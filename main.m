//
//  main.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010, BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCAppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BCAppDelegate class]));
    }
}
