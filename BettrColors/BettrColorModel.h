//
//  BettrColorModel.h
//  BettrColors
//
//  Created by Eric on 8/27/14.
//
//

#import <Foundation/Foundation.h>

@interface BettrColorModel : NSObject

@property (retain) NSColor	*color;
@property (retain) NSString	*name;
@property (retain) NSString	*hex;

- (id) initWithColor:(NSColor*)color name:(NSString*)name;

@end
