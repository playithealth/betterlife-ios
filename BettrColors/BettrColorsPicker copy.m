#import "BettrColorsPicker.h"
#import <objc/runtime.h>
#import "UIColor+Additions.h"
#import "BettrColorModel.h"

@implementation BettrColorsPicker


// Picker Proctocol Methods
- (NSImage*)provideNewButtonImage
{
	NSImage* image;
	
	image = [[NSImage alloc] initWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:@"icon" ofType:@"png"]];
	[image setScalesWhenResized:YES];
	[image setSize:NSMakeSize(32.0,32.0)];
	
	return image;
}

- (NSString *)_buttonToolTip
{
	return @"BettrColors";
}

- (NSString *)description
{
	return @"Bettr than all the other colors.";
}

- (void) setupColors
{
	NSMutableArray	*theColors = [NSMutableArray arrayWithCapacity:30];
	
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_darkGreenColor] name:@"BC_darkGreenColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_mediumDarkGreenColor] name:@"BC_mediumDarkGreenColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_mediumLightGreenColor] name:@"BC_mediumLightGreenColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_lightGreenColor] name:@"BC_lightGreenColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_blueColor] name:@"BC_blueColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_redColor] name:@"BC_redColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_goldColor] name:@"BC_goldColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_backgroundColor] name:@"BC_backgroundColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_lightGrayColor] name:@"BC_lightGrayColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_mediumLightGrayColor] name:@"BC_mediumLightGrayColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_mediumDarkGrayColor] name:@"BC_mediumDarkGrayColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_darkGrayColor] name:@"BC_darkGrayColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_groupedTableViewCellBackgroundColor] name:@"BC_groupedTableViewCellBackgroundColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_tableViewHeaderColor] name:@"BC_tableViewHeaderColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_tableViewHeaderTextColor] name:@"BC_tableViewHeaderTextColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BC_nonEditableTextColor] name:@"BC_nonEditableTextColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BL_goalRangeColor] name:@"BL_goalRangeColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BL_mehTextColor] name:@"BL_mehTextColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BL_darkLineColor] name:@"BL_darkLineColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BL_lightLineColor] name:@"BL_lightLineColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BL_tabColor] name:@"BL_tabColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BL_linkColor] name:@"BL_linkColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BL_happySmileyColor] name:@"BL_happySmileyColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BL_mehSmileyColor] name:@"BL_mehSmileyColor"]];
	[theColors addObject:[[BettrColorModel alloc] initWithColor:[NSColor BL_sadSmileyColor] name:@"BL_sadSmileyColor"]];

	[self setBettrColorModelArray:theColors];
}

- (NSView*)provideNewView:(BOOL)initialRequest
{
	if (initialRequest)
		[[NSBundle bundleForClass:[self class]] loadNibNamed:@"BettrColorsPicker" owner:self topLevelObjects:NULL];
	
	[self setupColors];
	
	return colorPickerView;
}

- (BOOL)supportsMode:(NSColorPanelMode)mode
{
	switch (mode)
	{
		case NSColorPanelAllModesMask:	// we support all modes
			return YES;
	}
	return NO;
}

- (NSColorPanelMode)currentMode;
{
	return NSColorPanelAllModesMask;
}



// Interaction Methods

- (void)setColor:(NSColor *)color
{	
	self.currColor = color;
}

// the user chose a particular color
- (IBAction)colorChanged:(id)sender
{
	if ([sender isKindOfClass:[BettrColorModel class]])
	{
		BettrColorModel	*cm = (BettrColorModel*)sender;
		[[self colorPanel] setColor:cm.color];
	}
}


-(void)insertObject:(BettrColorModel *)p inBettrColorModelArrayAtIndex:(NSUInteger)index {
    [self.colors insertObject:p atIndex:index];
}

-(void)removeObjectFromBettrColorModelArrayAtIndex:(NSUInteger)index {
    [self.colors removeObjectAtIndex:index];
}

-(void)setBettrColorModelArray:(NSMutableArray *)a {
    self.colors = a;
}

-(NSArray*)bettrColorModelArray {
    return self.colors;
}

@end
