//
//  BettrColorModel.m
//  BettrColors
//
//  Created by Eric on 8/27/14.
//
//

#import "BettrColorModel.h"

@implementation BettrColorModel

- (id) initWithColor:(NSColor*)color name:(NSString*)name
{
	self = [super init];
	if (self)
	{
		_color = color;
		_name = name;
		
		CGFloat red, green, blue, alpha;
		NSColor *rgbColor = [color colorUsingColorSpace:[NSColorSpace deviceRGBColorSpace]];
		if (rgbColor != nil)
		{
			[rgbColor getRed:&red green:&green blue:&blue alpha:&alpha];
			NSString *hexString = [NSString stringWithFormat:@"#%02X%02X%02X", (unsigned int)(red*255), (unsigned int)(green*255), (unsigned int)(blue*255)];
			if (alpha < 1.0)
				hexString = [NSString stringWithFormat:@"%@, %0.2f", hexString, alpha];
			_hex = hexString;
		}
	}
	
	return self;
}

@end
