#import <Cocoa/Cocoa.h>

@interface BettrColorsPicker : NSColorPicker <NSColorPickingCustom>
{
	IBOutlet NSView*	colorPickerView;
}

@property (retain, readwrite) NSMutableArray	*colors;
@property (retain, readwrite) NSColor			*currColor;

- (IBAction)colorChanged:(id)sender;

@end
