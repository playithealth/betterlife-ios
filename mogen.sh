#!/bin/sh
# Data Model version directory naming pattern:  buyercompass <number>.xcdatamodel

echo "mogenerator -m buyercompass.xcdatamodeld/ --template-var arc=true -M Classes/CoreData/Machine/ -H Classes/CoreData/Human/ --template-path=."
mogenerator -m buyercompass.xcdatamodeld/ --template-var arc=true -M Classes/CoreData/Machine/ -H Classes/CoreData/Human/ --template-path=.
