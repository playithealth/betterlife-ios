//
//  BLOnboardNutrientGoalViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 3/29/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCDetailViewDelegate.h"

@class UserProfileMO;

@interface BLOnboardNutrientGoalViewController : UIViewController
@property (strong, nonatomic) UserProfileMO *userProfileMO;
@property (strong, nonatomic) NSString *nutrientName;
@property (strong, nonatomic) NSString *nutrientDisplayName;
@property (strong, nonatomic) NSString *nutrientUnits;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;
@property (strong, nonatomic) NSNumber *calorieCalculatorGoal;
@end
