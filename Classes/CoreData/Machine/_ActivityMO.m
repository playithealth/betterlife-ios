// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityMO.m instead.

#import "_ActivityMO.h"

const struct ActivityMOAttributes ActivityMOAttributes = {
	.activityDescription = @"activityDescription",
	.activityName = @"activityName",
	.basedOnId = @"basedOnId",
	.cloudId = @"cloudId",
	.factor = @"factor",
	.isTimedActivity = @"isTimedActivity",
	.link = @"link",
	.loginId = @"loginId",
	.notes = @"notes",
	.routineId = @"routineId",
	.sortOrder = @"sortOrder",
	.source = @"source",
};

const struct ActivityMORelationships ActivityMORelationships = {
	.basedOnActivity = @"basedOnActivity",
	.childActivities = @"childActivities",
	.sets = @"sets",
	.workout = @"workout",
};

@implementation ActivityMOID
@end

@implementation _ActivityMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AbstractActivity" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AbstractActivity";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AbstractActivity" inManagedObjectContext:moc_];
}

- (ActivityMOID*)objectID {
	return (ActivityMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"basedOnIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"basedOnId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"factorValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"factor"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isTimedActivityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isTimedActivity"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"routineIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"routineId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sourceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"source"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic activityDescription;

@dynamic activityName;

@dynamic basedOnId;

- (int32_t)basedOnIdValue {
	NSNumber *result = [self basedOnId];
	return [result intValue];
}

- (void)setBasedOnIdValue:(int32_t)value_ {
	[self setBasedOnId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveBasedOnIdValue {
	NSNumber *result = [self primitiveBasedOnId];
	return [result intValue];
}

- (void)setPrimitiveBasedOnIdValue:(int32_t)value_ {
	[self setPrimitiveBasedOnId:[NSNumber numberWithInt:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic factor;

- (double)factorValue {
	NSNumber *result = [self factor];
	return [result doubleValue];
}

- (void)setFactorValue:(double)value_ {
	[self setFactor:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveFactorValue {
	NSNumber *result = [self primitiveFactor];
	return [result doubleValue];
}

- (void)setPrimitiveFactorValue:(double)value_ {
	[self setPrimitiveFactor:[NSNumber numberWithDouble:value_]];
}

@dynamic isTimedActivity;

- (BOOL)isTimedActivityValue {
	NSNumber *result = [self isTimedActivity];
	return [result boolValue];
}

- (void)setIsTimedActivityValue:(BOOL)value_ {
	[self setIsTimedActivity:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsTimedActivityValue {
	NSNumber *result = [self primitiveIsTimedActivity];
	return [result boolValue];
}

- (void)setPrimitiveIsTimedActivityValue:(BOOL)value_ {
	[self setPrimitiveIsTimedActivity:[NSNumber numberWithBool:value_]];
}

@dynamic link;

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic notes;

@dynamic routineId;

- (int32_t)routineIdValue {
	NSNumber *result = [self routineId];
	return [result intValue];
}

- (void)setRoutineIdValue:(int32_t)value_ {
	[self setRoutineId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRoutineIdValue {
	NSNumber *result = [self primitiveRoutineId];
	return [result intValue];
}

- (void)setPrimitiveRoutineIdValue:(int32_t)value_ {
	[self setPrimitiveRoutineId:[NSNumber numberWithInt:value_]];
}

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic source;

- (int16_t)sourceValue {
	NSNumber *result = [self source];
	return [result shortValue];
}

- (void)setSourceValue:(int16_t)value_ {
	[self setSource:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSourceValue {
	NSNumber *result = [self primitiveSource];
	return [result shortValue];
}

- (void)setPrimitiveSourceValue:(int16_t)value_ {
	[self setPrimitiveSource:[NSNumber numberWithShort:value_]];
}

@dynamic basedOnActivity;

@dynamic childActivities;

- (NSMutableSet*)childActivitiesSet {
	[self willAccessValueForKey:@"childActivities"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"childActivities"];

	[self didAccessValueForKey:@"childActivities"];
	return result;
}

@dynamic sets;

- (NSMutableSet*)setsSet {
	[self willAccessValueForKey:@"sets"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"sets"];

	[self didAccessValueForKey:@"sets"];
	return result;
}

@dynamic workout;

@end

