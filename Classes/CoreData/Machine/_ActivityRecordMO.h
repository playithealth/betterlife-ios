// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityRecordMO.h instead.

#import <CoreData/CoreData.h>
#import "ActivityMO.h"

extern const struct ActivityRecordMOAttributes {
	__unsafe_unretained NSString *activityId;
	__unsafe_unretained NSString *isRemoved;
	__unsafe_unretained NSString *recordId;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *targetDate;
	__unsafe_unretained NSString *targetDateString;
	__unsafe_unretained NSString *updatedOn;
} ActivityRecordMOAttributes;

extern const struct ActivityRecordMORelationships {
	__unsafe_unretained NSString *linkedWorkout;
} ActivityRecordMORelationships;

@class AdvisorActivityPlanWorkoutMO;

@interface ActivityRecordMOID : ActivityMOID {}
@end

@interface _ActivityRecordMO : ActivityMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ActivityRecordMOID* objectID;

@property (nonatomic, strong) NSNumber* activityId;

@property (atomic) int32_t activityIdValue;
- (int32_t)activityIdValue;
- (void)setActivityIdValue:(int32_t)value_;

//- (BOOL)validateActivityId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRemoved;

@property (atomic) BOOL isRemovedValue;
- (BOOL)isRemovedValue;
- (void)setIsRemovedValue:(BOOL)value_;

//- (BOOL)validateIsRemoved:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* recordId;

@property (atomic) int32_t recordIdValue;
- (int32_t)recordIdValue;
- (void)setRecordIdValue:(int32_t)value_;

//- (BOOL)validateRecordId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* targetDate;

//- (BOOL)validateTargetDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* targetDateString;

//- (BOOL)validateTargetDateString:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AdvisorActivityPlanWorkoutMO *linkedWorkout;

//- (BOOL)validateLinkedWorkout:(id*)value_ error:(NSError**)error_;

@end

@interface _ActivityRecordMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveActivityId;
- (void)setPrimitiveActivityId:(NSNumber*)value;

- (int32_t)primitiveActivityIdValue;
- (void)setPrimitiveActivityIdValue:(int32_t)value_;

- (NSNumber*)primitiveIsRemoved;
- (void)setPrimitiveIsRemoved:(NSNumber*)value;

- (BOOL)primitiveIsRemovedValue;
- (void)setPrimitiveIsRemovedValue:(BOOL)value_;

- (NSNumber*)primitiveRecordId;
- (void)setPrimitiveRecordId:(NSNumber*)value;

- (int32_t)primitiveRecordIdValue;
- (void)setPrimitiveRecordIdValue:(int32_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSDate*)primitiveTargetDate;
- (void)setPrimitiveTargetDate:(NSDate*)value;

- (NSString*)primitiveTargetDateString;
- (void)setPrimitiveTargetDateString:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (AdvisorActivityPlanWorkoutMO*)primitiveLinkedWorkout;
- (void)setPrimitiveLinkedWorkout:(AdvisorActivityPlanWorkoutMO*)value;

@end
