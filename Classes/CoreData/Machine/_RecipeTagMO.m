// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RecipeTagMO.m instead.

#import "_RecipeTagMO.h"

const struct RecipeTagMOAttributes RecipeTagMOAttributes = {
	.name = @"name",
};

const struct RecipeTagMORelationships RecipeTagMORelationships = {
	.recipes = @"recipes",
};

@implementation RecipeTagMOID
@end

@implementation _RecipeTagMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"RecipeTag" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"RecipeTag";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"RecipeTag" inManagedObjectContext:moc_];
}

- (RecipeTagMOID*)objectID {
	return (RecipeTagMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic recipes;

- (NSMutableSet*)recipesSet {
	[self willAccessValueForKey:@"recipes"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"recipes"];

	[self didAccessValueForKey:@"recipes"];
	return result;
}

@end

