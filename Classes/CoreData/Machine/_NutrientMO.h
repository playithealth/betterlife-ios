// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to NutrientMO.h instead.

#import <CoreData/CoreData.h>

extern const struct NutrientMOAttributes {
	__unsafe_unretained NSString *cloudKey;
	__unsafe_unretained NSString *displayName;
	__unsafe_unretained NSString *isSubNutrient;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *precision;
	__unsafe_unretained NSString *sortOrder;
	__unsafe_unretained NSString *units;
} NutrientMOAttributes;

extern const struct NutrientMORelationships {
	__unsafe_unretained NSString *userProfilesShowing;
} NutrientMORelationships;

@class UserProfileMO;

@interface NutrientMOID : NSManagedObjectID {}
@end

@interface _NutrientMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) NutrientMOID* objectID;

@property (nonatomic, strong) NSString* cloudKey;

//- (BOOL)validateCloudKey:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* displayName;

//- (BOOL)validateDisplayName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isSubNutrient;

@property (atomic) BOOL isSubNutrientValue;
- (BOOL)isSubNutrientValue;
- (void)setIsSubNutrientValue:(BOOL)value_;

//- (BOOL)validateIsSubNutrient:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* precision;

@property (atomic) int16_t precisionValue;
- (int16_t)precisionValue;
- (void)setPrecisionValue:(int16_t)value_;

//- (BOOL)validatePrecision:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* units;

//- (BOOL)validateUnits:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *userProfilesShowing;

- (NSMutableSet*)userProfilesShowingSet;

@end

@interface _NutrientMO (UserProfilesShowingCoreDataGeneratedAccessors)
- (void)addUserProfilesShowing:(NSSet*)value_;
- (void)removeUserProfilesShowing:(NSSet*)value_;
- (void)addUserProfilesShowingObject:(UserProfileMO*)value_;
- (void)removeUserProfilesShowingObject:(UserProfileMO*)value_;

@end

@interface _NutrientMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveCloudKey;
- (void)setPrimitiveCloudKey:(NSString*)value;

- (NSString*)primitiveDisplayName;
- (void)setPrimitiveDisplayName:(NSString*)value;

- (NSNumber*)primitiveIsSubNutrient;
- (void)setPrimitiveIsSubNutrient:(NSNumber*)value;

- (BOOL)primitiveIsSubNutrientValue;
- (void)setPrimitiveIsSubNutrientValue:(BOOL)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitivePrecision;
- (void)setPrimitivePrecision:(NSNumber*)value;

- (int16_t)primitivePrecisionValue;
- (void)setPrimitivePrecisionValue:(int16_t)value_;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSString*)primitiveUnits;
- (void)setPrimitiveUnits:(NSString*)value;

- (NSMutableSet*)primitiveUserProfilesShowing;
- (void)setPrimitiveUserProfilesShowing:(NSMutableSet*)value;

@end
