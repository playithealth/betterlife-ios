// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RecipeTagMO.h instead.

#import <CoreData/CoreData.h>

extern const struct RecipeTagMOAttributes {
	__unsafe_unretained NSString *name;
} RecipeTagMOAttributes;

extern const struct RecipeTagMORelationships {
	__unsafe_unretained NSString *recipes;
} RecipeTagMORelationships;

@class RecipeMO;

@interface RecipeTagMOID : NSManagedObjectID {}
@end

@interface _RecipeTagMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) RecipeTagMOID* objectID;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *recipes;

- (NSMutableSet*)recipesSet;

@end

@interface _RecipeTagMO (RecipesCoreDataGeneratedAccessors)
- (void)addRecipes:(NSSet*)value_;
- (void)removeRecipes:(NSSet*)value_;
- (void)addRecipesObject:(RecipeMO*)value_;
- (void)removeRecipesObject:(RecipeMO*)value_;

@end

@interface _RecipeTagMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableSet*)primitiveRecipes;
- (void)setPrimitiveRecipes:(NSMutableSet*)value;

@end
