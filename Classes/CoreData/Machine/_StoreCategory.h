// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to StoreCategory.h instead.

#import <CoreData/CoreData.h>

extern const struct StoreCategoryAttributes {
	__unsafe_unretained NSString *hidden;
	__unsafe_unretained NSString *sortOrder;
} StoreCategoryAttributes;

extern const struct StoreCategoryRelationships {
	__unsafe_unretained NSString *category;
	__unsafe_unretained NSString *shoppingListItems;
	__unsafe_unretained NSString *store;
} StoreCategoryRelationships;

@class CategoryMO;
@class ShoppingListItem;
@class UserStoreMO;

@interface StoreCategoryID : NSManagedObjectID {}
@end

@interface _StoreCategory : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) StoreCategoryID* objectID;

@property (nonatomic, strong) NSNumber* hidden;

@property (atomic) BOOL hiddenValue;
- (BOOL)hiddenValue;
- (void)setHiddenValue:(BOOL)value_;

//- (BOOL)validateHidden:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int32_t sortOrderValue;
- (int32_t)sortOrderValue;
- (void)setSortOrderValue:(int32_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CategoryMO *category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *shoppingListItems;

- (NSMutableSet*)shoppingListItemsSet;

@property (nonatomic, strong) UserStoreMO *store;

//- (BOOL)validateStore:(id*)value_ error:(NSError**)error_;

@end

@interface _StoreCategory (ShoppingListItemsCoreDataGeneratedAccessors)
- (void)addShoppingListItems:(NSSet*)value_;
- (void)removeShoppingListItems:(NSSet*)value_;
- (void)addShoppingListItemsObject:(ShoppingListItem*)value_;
- (void)removeShoppingListItemsObject:(ShoppingListItem*)value_;

@end

@interface _StoreCategory (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveHidden;
- (void)setPrimitiveHidden:(NSNumber*)value;

- (BOOL)primitiveHiddenValue;
- (void)setPrimitiveHiddenValue:(BOOL)value_;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int32_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int32_t)value_;

- (CategoryMO*)primitiveCategory;
- (void)setPrimitiveCategory:(CategoryMO*)value;

- (NSMutableSet*)primitiveShoppingListItems;
- (void)setPrimitiveShoppingListItems:(NSMutableSet*)value;

- (UserStoreMO*)primitiveStore;
- (void)setPrimitiveStore:(UserStoreMO*)value;

@end
