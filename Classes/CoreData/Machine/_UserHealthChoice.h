// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserHealthChoice.h instead.

#import <CoreData/CoreData.h>

extern const struct UserHealthChoiceAttributes {
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *value;
} UserHealthChoiceAttributes;

extern const struct UserHealthChoiceRelationships {
	__unsafe_unretained NSString *choice;
} UserHealthChoiceRelationships;

@class HealthChoiceMO;

@interface UserHealthChoiceID : NSManagedObjectID {}
@end

@interface _UserHealthChoice : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserHealthChoiceID* objectID;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* value;

@property (atomic) int16_t valueValue;
- (int16_t)valueValue;
- (void)setValueValue:(int16_t)value_;

//- (BOOL)validateValue:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) HealthChoiceMO *choice;

//- (BOOL)validateChoice:(id*)value_ error:(NSError**)error_;

@end

@interface _UserHealthChoice (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveValue;
- (void)setPrimitiveValue:(NSNumber*)value;

- (int16_t)primitiveValueValue;
- (void)setPrimitiveValueValue:(int16_t)value_;

- (HealthChoiceMO*)primitiveChoice;
- (void)setPrimitiveChoice:(HealthChoiceMO*)value;

@end
