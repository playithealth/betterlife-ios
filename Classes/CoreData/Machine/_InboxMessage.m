// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to InboxMessage.m instead.

#import "_InboxMessage.h"

const struct InboxMessageAttributes InboxMessageAttributes = {
	.accountId = @"accountId",
	.cloudId = @"cloudId",
	.createdOn = @"createdOn",
	.deliveryId = @"deliveryId",
	.isPrivate = @"isPrivate",
	.isRead = @"isRead",
	.isRemoved = @"isRemoved",
	.loginId = @"loginId",
	.sender = @"sender",
	.senderId = @"senderId",
	.status = @"status",
	.subject = @"subject",
	.summary = @"summary",
	.text = @"text",
	.updatedOn = @"updatedOn",
};

const struct InboxMessageRelationships InboxMessageRelationships = {
	.replies = @"replies",
	.replyTo = @"replyTo",
};

@implementation InboxMessageID
@end

@implementation _InboxMessage

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"InboxMessage" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"InboxMessage";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"InboxMessage" inManagedObjectContext:moc_];
}

- (InboxMessageID*)objectID {
	return (InboxMessageID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"createdOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"createdOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"deliveryIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"deliveryId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isPrivateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isPrivate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isReadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRead"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isRemovedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRemoved"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"senderIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"senderId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic createdOn;

- (int32_t)createdOnValue {
	NSNumber *result = [self createdOn];
	return [result intValue];
}

- (void)setCreatedOnValue:(int32_t)value_ {
	[self setCreatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCreatedOnValue {
	NSNumber *result = [self primitiveCreatedOn];
	return [result intValue];
}

- (void)setPrimitiveCreatedOnValue:(int32_t)value_ {
	[self setPrimitiveCreatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic deliveryId;

- (int32_t)deliveryIdValue {
	NSNumber *result = [self deliveryId];
	return [result intValue];
}

- (void)setDeliveryIdValue:(int32_t)value_ {
	[self setDeliveryId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDeliveryIdValue {
	NSNumber *result = [self primitiveDeliveryId];
	return [result intValue];
}

- (void)setPrimitiveDeliveryIdValue:(int32_t)value_ {
	[self setPrimitiveDeliveryId:[NSNumber numberWithInt:value_]];
}

@dynamic isPrivate;

- (BOOL)isPrivateValue {
	NSNumber *result = [self isPrivate];
	return [result boolValue];
}

- (void)setIsPrivateValue:(BOOL)value_ {
	[self setIsPrivate:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsPrivateValue {
	NSNumber *result = [self primitiveIsPrivate];
	return [result boolValue];
}

- (void)setPrimitiveIsPrivateValue:(BOOL)value_ {
	[self setPrimitiveIsPrivate:[NSNumber numberWithBool:value_]];
}

@dynamic isRead;

- (BOOL)isReadValue {
	NSNumber *result = [self isRead];
	return [result boolValue];
}

- (void)setIsReadValue:(BOOL)value_ {
	[self setIsRead:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsReadValue {
	NSNumber *result = [self primitiveIsRead];
	return [result boolValue];
}

- (void)setPrimitiveIsReadValue:(BOOL)value_ {
	[self setPrimitiveIsRead:[NSNumber numberWithBool:value_]];
}

@dynamic isRemoved;

- (BOOL)isRemovedValue {
	NSNumber *result = [self isRemoved];
	return [result boolValue];
}

- (void)setIsRemovedValue:(BOOL)value_ {
	[self setIsRemoved:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsRemovedValue {
	NSNumber *result = [self primitiveIsRemoved];
	return [result boolValue];
}

- (void)setPrimitiveIsRemovedValue:(BOOL)value_ {
	[self setPrimitiveIsRemoved:[NSNumber numberWithBool:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic sender;

@dynamic senderId;

- (int32_t)senderIdValue {
	NSNumber *result = [self senderId];
	return [result intValue];
}

- (void)setSenderIdValue:(int32_t)value_ {
	[self setSenderId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSenderIdValue {
	NSNumber *result = [self primitiveSenderId];
	return [result intValue];
}

- (void)setPrimitiveSenderIdValue:(int32_t)value_ {
	[self setPrimitiveSenderId:[NSNumber numberWithInt:value_]];
}

@dynamic status;

@dynamic subject;

@dynamic summary;

@dynamic text;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic replies;

- (NSMutableSet*)repliesSet {
	[self willAccessValueForKey:@"replies"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"replies"];

	[self didAccessValueForKey:@"replies"];
	return result;
}

@dynamic replyTo;

@end

