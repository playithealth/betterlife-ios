// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanTagMO.m instead.

#import "_AdvisorMealPlanTagMO.h"

const struct AdvisorMealPlanTagMOAttributes AdvisorMealPlanTagMOAttributes = {
	.cloudId = @"cloudId",
	.loginId = @"loginId",
	.name = @"name",
	.servingTargetQuantity = @"servingTargetQuantity",
	.servingTargetUnit = @"servingTargetUnit",
};

const struct AdvisorMealPlanTagMORelationships AdvisorMealPlanTagMORelationships = {
	.entry = @"entry",
	.foodLogs = @"foodLogs",
	.mealPlannerDays = @"mealPlannerDays",
	.mealPredictions = @"mealPredictions",
};

@implementation AdvisorMealPlanTagMOID
@end

@implementation _AdvisorMealPlanTagMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorMealPlanTag" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorMealPlanTag";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorMealPlanTag" inManagedObjectContext:moc_];
}

- (AdvisorMealPlanTagMOID*)objectID {
	return (AdvisorMealPlanTagMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"servingTargetQuantityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"servingTargetQuantity"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic servingTargetQuantity;

- (int16_t)servingTargetQuantityValue {
	NSNumber *result = [self servingTargetQuantity];
	return [result shortValue];
}

- (void)setServingTargetQuantityValue:(int16_t)value_ {
	[self setServingTargetQuantity:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveServingTargetQuantityValue {
	NSNumber *result = [self primitiveServingTargetQuantity];
	return [result shortValue];
}

- (void)setPrimitiveServingTargetQuantityValue:(int16_t)value_ {
	[self setPrimitiveServingTargetQuantity:[NSNumber numberWithShort:value_]];
}

@dynamic servingTargetUnit;

@dynamic entry;

@dynamic foodLogs;

- (NSMutableSet*)foodLogsSet {
	[self willAccessValueForKey:@"foodLogs"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"foodLogs"];

	[self didAccessValueForKey:@"foodLogs"];
	return result;
}

@dynamic mealPlannerDays;

- (NSMutableSet*)mealPlannerDaysSet {
	[self willAccessValueForKey:@"mealPlannerDays"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mealPlannerDays"];

	[self didAccessValueForKey:@"mealPlannerDays"];
	return result;
}

@dynamic mealPredictions;

- (NSMutableSet*)mealPredictionsSet {
	[self willAccessValueForKey:@"mealPredictions"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mealPredictions"];

	[self didAccessValueForKey:@"mealPredictions"];
	return result;
}

@end

