// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrackingTypeMO.h instead.

#import <CoreData/CoreData.h>

extern const struct TrackingTypeMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *readOnly;
	__unsafe_unretained NSString *sortOrder;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *view;
} TrackingTypeMOAttributes;

extern const struct TrackingTypeMORelationships {
	__unsafe_unretained NSString *trackingValues;
} TrackingTypeMORelationships;

@class TrackingValueMO;

@interface TrackingTypeMOID : NSManagedObjectID {}
@end

@interface _TrackingTypeMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TrackingTypeMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* readOnly;

@property (atomic) BOOL readOnlyValue;
- (BOOL)readOnlyValue;
- (void)setReadOnlyValue:(BOOL)value_;

//- (BOOL)validateReadOnly:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* view;

//- (BOOL)validateView:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *trackingValues;

- (NSMutableSet*)trackingValuesSet;

@end

@interface _TrackingTypeMO (TrackingValuesCoreDataGeneratedAccessors)
- (void)addTrackingValues:(NSSet*)value_;
- (void)removeTrackingValues:(NSSet*)value_;
- (void)addTrackingValuesObject:(TrackingValueMO*)value_;
- (void)removeTrackingValuesObject:(TrackingValueMO*)value_;

@end

@interface _TrackingTypeMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSNumber*)primitiveReadOnly;
- (void)setPrimitiveReadOnly:(NSNumber*)value;

- (BOOL)primitiveReadOnlyValue;
- (void)setPrimitiveReadOnlyValue:(BOOL)value_;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (NSString*)primitiveView;
- (void)setPrimitiveView:(NSString*)value;

- (NSMutableSet*)primitiveTrackingValues;
- (void)setPrimitiveTrackingValues:(NSMutableSet*)value;

@end
