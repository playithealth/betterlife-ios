// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanEntryMO.h instead.

#import <CoreData/CoreData.h>

extern const struct AdvisorMealPlanEntryMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *logTime;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *nutritionPercent;
	__unsafe_unretained NSString *unplannedMealOption;
} AdvisorMealPlanEntryMOAttributes;

extern const struct AdvisorMealPlanEntryMORelationships {
	__unsafe_unretained NSString *phase;
	__unsafe_unretained NSString *tags;
} AdvisorMealPlanEntryMORelationships;

@class AdvisorMealPlanPhaseMO;
@class AdvisorMealPlanTagMO;

@interface AdvisorMealPlanEntryMOID : NSManagedObjectID {}
@end

@interface _AdvisorMealPlanEntryMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorMealPlanEntryMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* logTime;

@property (atomic) int16_t logTimeValue;
- (int16_t)logTimeValue;
- (void)setLogTimeValue:(int16_t)value_;

//- (BOOL)validateLogTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* nutritionPercent;

@property (atomic) int16_t nutritionPercentValue;
- (int16_t)nutritionPercentValue;
- (void)setNutritionPercentValue:(int16_t)value_;

//- (BOOL)validateNutritionPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* unplannedMealOption;

//- (BOOL)validateUnplannedMealOption:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AdvisorMealPlanPhaseMO *phase;

//- (BOOL)validatePhase:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *tags;

- (NSMutableSet*)tagsSet;

@end

@interface _AdvisorMealPlanEntryMO (TagsCoreDataGeneratedAccessors)
- (void)addTags:(NSSet*)value_;
- (void)removeTags:(NSSet*)value_;
- (void)addTagsObject:(AdvisorMealPlanTagMO*)value_;
- (void)removeTagsObject:(AdvisorMealPlanTagMO*)value_;

@end

@interface _AdvisorMealPlanEntryMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveLogTime;
- (void)setPrimitiveLogTime:(NSNumber*)value;

- (int16_t)primitiveLogTimeValue;
- (void)setPrimitiveLogTimeValue:(int16_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSNumber*)primitiveNutritionPercent;
- (void)setPrimitiveNutritionPercent:(NSNumber*)value;

- (int16_t)primitiveNutritionPercentValue;
- (void)setPrimitiveNutritionPercentValue:(int16_t)value_;

- (NSString*)primitiveUnplannedMealOption;
- (void)setPrimitiveUnplannedMealOption:(NSString*)value;

- (AdvisorMealPlanPhaseMO*)primitivePhase;
- (void)setPrimitivePhase:(AdvisorMealPlanPhaseMO*)value;

- (NSMutableSet*)primitiveTags;
- (void)setPrimitiveTags:(NSMutableSet*)value;

@end
