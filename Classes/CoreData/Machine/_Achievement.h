// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Achievement.h instead.

#import <CoreData/CoreData.h>

extern const struct AchievementAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *image;
	__unsafe_unretained NSString *message;
	__unsafe_unretained NSString *name;
} AchievementAttributes;

extern const struct AchievementRelationships {
	__unsafe_unretained NSString *userAchievements;
} AchievementRelationships;

@class UserAchievement;

@interface AchievementID : NSManagedObjectID {}
@end

@interface _Achievement : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AchievementID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* image;

//- (BOOL)validateImage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* message;

//- (BOOL)validateMessage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *userAchievements;

- (NSMutableSet*)userAchievementsSet;

@end

@interface _Achievement (UserAchievementsCoreDataGeneratedAccessors)
- (void)addUserAchievements:(NSSet*)value_;
- (void)removeUserAchievements:(NSSet*)value_;
- (void)addUserAchievementsObject:(UserAchievement*)value_;
- (void)removeUserAchievementsObject:(UserAchievement*)value_;

@end

@interface _Achievement (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSString*)primitiveImage;
- (void)setPrimitiveImage:(NSString*)value;

- (NSString*)primitiveMessage;
- (void)setPrimitiveMessage:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableSet*)primitiveUserAchievements;
- (void)setPrimitiveUserAchievements:(NSMutableSet*)value;

@end
