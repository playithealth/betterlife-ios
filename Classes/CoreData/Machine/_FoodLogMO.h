// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FoodLogMO.h instead.

#import <CoreData/CoreData.h>

extern const struct FoodLogMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *image;
	__unsafe_unretained NSString *imageId;
	__unsafe_unretained NSString *itemType;
	__unsafe_unretained NSString *logTime;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *menuId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *nutritionFactor;
	__unsafe_unretained NSString *nutritionId;
	__unsafe_unretained NSString *productId;
	__unsafe_unretained NSString *servingSize;
	__unsafe_unretained NSString *servings;
	__unsafe_unretained NSString *servingsUnit;
	__unsafe_unretained NSString *source;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *tagId;
	__unsafe_unretained NSString *updatedOn;
} FoodLogMOAttributes;

extern const struct FoodLogMORelationships {
	__unsafe_unretained NSString *customFood;
	__unsafe_unretained NSString *nutrition;
	__unsafe_unretained NSString *recipe;
	__unsafe_unretained NSString *tag;
} FoodLogMORelationships;

@class CustomFoodMO;
@class NutritionMO;
@class RecipeMO;
@class AdvisorMealPlanTagMO;

@interface FoodLogMOID : NSManagedObjectID {}
@end

@interface _FoodLogMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) FoodLogMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSData* image;

//- (BOOL)validateImage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* imageId;

@property (atomic) int32_t imageIdValue;
- (int32_t)imageIdValue;
- (void)setImageIdValue:(int32_t)value_;

//- (BOOL)validateImageId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* itemType;

//- (BOOL)validateItemType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* logTime;

@property (atomic) int16_t logTimeValue;
- (int16_t)logTimeValue;
- (void)setLogTimeValue:(int16_t)value_;

//- (BOOL)validateLogTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* menuId;

@property (atomic) int32_t menuIdValue;
- (int32_t)menuIdValue;
- (void)setMenuIdValue:(int32_t)value_;

//- (BOOL)validateMenuId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* nutritionFactor;

@property (atomic) double nutritionFactorValue;
- (double)nutritionFactorValue;
- (void)setNutritionFactorValue:(double)value_;

//- (BOOL)validateNutritionFactor:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* nutritionId;

@property (atomic) int32_t nutritionIdValue;
- (int32_t)nutritionIdValue;
- (void)setNutritionIdValue:(int32_t)value_;

//- (BOOL)validateNutritionId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* productId;

@property (atomic) int32_t productIdValue;
- (int32_t)productIdValue;
- (void)setProductIdValue:(int32_t)value_;

//- (BOOL)validateProductId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* servingSize;

//- (BOOL)validateServingSize:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* servings;

@property (atomic) double servingsValue;
- (double)servingsValue;
- (void)setServingsValue:(double)value_;

//- (BOOL)validateServings:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* servingsUnit;

//- (BOOL)validateServingsUnit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* source;

@property (atomic) int32_t sourceValue;
- (int32_t)sourceValue;
- (void)setSourceValue:(int32_t)value_;

//- (BOOL)validateSource:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* tagId;

@property (atomic) int32_t tagIdValue;
- (int32_t)tagIdValue;
- (void)setTagIdValue:(int32_t)value_;

//- (BOOL)validateTagId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CustomFoodMO *customFood;

//- (BOOL)validateCustomFood:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NutritionMO *nutrition;

//- (BOOL)validateNutrition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) RecipeMO *recipe;

//- (BOOL)validateRecipe:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AdvisorMealPlanTagMO *tag;

//- (BOOL)validateTag:(id*)value_ error:(NSError**)error_;

@end

@interface _FoodLogMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSDate*)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(NSDate*)value;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSData*)primitiveImage;
- (void)setPrimitiveImage:(NSData*)value;

- (NSNumber*)primitiveImageId;
- (void)setPrimitiveImageId:(NSNumber*)value;

- (int32_t)primitiveImageIdValue;
- (void)setPrimitiveImageIdValue:(int32_t)value_;

- (NSString*)primitiveItemType;
- (void)setPrimitiveItemType:(NSString*)value;

- (NSNumber*)primitiveLogTime;
- (void)setPrimitiveLogTime:(NSNumber*)value;

- (int16_t)primitiveLogTimeValue;
- (void)setPrimitiveLogTimeValue:(int16_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSNumber*)primitiveMenuId;
- (void)setPrimitiveMenuId:(NSNumber*)value;

- (int32_t)primitiveMenuIdValue;
- (void)setPrimitiveMenuIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveNutritionFactor;
- (void)setPrimitiveNutritionFactor:(NSNumber*)value;

- (double)primitiveNutritionFactorValue;
- (void)setPrimitiveNutritionFactorValue:(double)value_;

- (NSNumber*)primitiveNutritionId;
- (void)setPrimitiveNutritionId:(NSNumber*)value;

- (int32_t)primitiveNutritionIdValue;
- (void)setPrimitiveNutritionIdValue:(int32_t)value_;

- (NSNumber*)primitiveProductId;
- (void)setPrimitiveProductId:(NSNumber*)value;

- (int32_t)primitiveProductIdValue;
- (void)setPrimitiveProductIdValue:(int32_t)value_;

- (NSString*)primitiveServingSize;
- (void)setPrimitiveServingSize:(NSString*)value;

- (NSNumber*)primitiveServings;
- (void)setPrimitiveServings:(NSNumber*)value;

- (double)primitiveServingsValue;
- (void)setPrimitiveServingsValue:(double)value_;

- (NSString*)primitiveServingsUnit;
- (void)setPrimitiveServingsUnit:(NSString*)value;

- (NSNumber*)primitiveSource;
- (void)setPrimitiveSource:(NSNumber*)value;

- (int32_t)primitiveSourceValue;
- (void)setPrimitiveSourceValue:(int32_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveTagId;
- (void)setPrimitiveTagId:(NSNumber*)value;

- (int32_t)primitiveTagIdValue;
- (void)setPrimitiveTagIdValue:(int32_t)value_;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (CustomFoodMO*)primitiveCustomFood;
- (void)setPrimitiveCustomFood:(CustomFoodMO*)value;

- (NutritionMO*)primitiveNutrition;
- (void)setPrimitiveNutrition:(NutritionMO*)value;

- (RecipeMO*)primitiveRecipe;
- (void)setPrimitiveRecipe:(RecipeMO*)value;

- (AdvisorMealPlanTagMO*)primitiveTag;
- (void)setPrimitiveTag:(AdvisorMealPlanTagMO*)value;

@end
