// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanTimelineMO.h instead.

#import <CoreData/CoreData.h>
#import "AdvisorPlanTimelineMO.h"

@interface AdvisorMealPlanTimelineMOID : AdvisorPlanTimelineMOID {}
@end

@interface _AdvisorMealPlanTimelineMO : AdvisorPlanTimelineMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorMealPlanTimelineMOID* objectID;

@end

@interface _AdvisorMealPlanTimelineMO (CoreDataGeneratedPrimitiveAccessors)

@end
