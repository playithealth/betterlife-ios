// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrainingActivityMO.m instead.

#import "_TrainingActivityMO.h"

const struct TrainingActivityMOAttributes TrainingActivityMOAttributes = {
	.activity = @"activity",
	.calories = @"calories",
	.cloudId = @"cloudId",
	.date = @"date",
	.distance = @"distance",
	.factor = @"factor",
	.loginId = @"loginId",
	.source = @"source",
	.status = @"status",
	.time = @"time",
	.updatedOn = @"updatedOn",
	.weight = @"weight",
};

const struct TrainingActivityMORelationships TrainingActivityMORelationships = {
	.exerciseRoutine = @"exerciseRoutine",
};

@implementation TrainingActivityMOID
@end

@implementation _TrainingActivityMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TrainingActivity" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TrainingActivity";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TrainingActivity" inManagedObjectContext:moc_];
}

- (TrainingActivityMOID*)objectID {
	return (TrainingActivityMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"caloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"calories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"distanceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"distance"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"factorValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"factor"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sourceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"source"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"weightValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"weight"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic activity;

@dynamic calories;

- (int16_t)caloriesValue {
	NSNumber *result = [self calories];
	return [result shortValue];
}

- (void)setCaloriesValue:(int16_t)value_ {
	[self setCalories:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCaloriesValue {
	NSNumber *result = [self primitiveCalories];
	return [result shortValue];
}

- (void)setPrimitiveCaloriesValue:(int16_t)value_ {
	[self setPrimitiveCalories:[NSNumber numberWithShort:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic date;

@dynamic distance;

- (double)distanceValue {
	NSNumber *result = [self distance];
	return [result doubleValue];
}

- (void)setDistanceValue:(double)value_ {
	[self setDistance:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveDistanceValue {
	NSNumber *result = [self primitiveDistance];
	return [result doubleValue];
}

- (void)setPrimitiveDistanceValue:(double)value_ {
	[self setPrimitiveDistance:[NSNumber numberWithDouble:value_]];
}

@dynamic factor;

- (double)factorValue {
	NSNumber *result = [self factor];
	return [result doubleValue];
}

- (void)setFactorValue:(double)value_ {
	[self setFactor:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveFactorValue {
	NSNumber *result = [self primitiveFactor];
	return [result doubleValue];
}

- (void)setPrimitiveFactorValue:(double)value_ {
	[self setPrimitiveFactor:[NSNumber numberWithDouble:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic source;

- (int32_t)sourceValue {
	NSNumber *result = [self source];
	return [result intValue];
}

- (void)setSourceValue:(int32_t)value_ {
	[self setSource:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSourceValue {
	NSNumber *result = [self primitiveSource];
	return [result intValue];
}

- (void)setPrimitiveSourceValue:(int32_t)value_ {
	[self setPrimitiveSource:[NSNumber numberWithInt:value_]];
}

@dynamic status;

@dynamic time;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic weight;

- (double)weightValue {
	NSNumber *result = [self weight];
	return [result doubleValue];
}

- (void)setWeightValue:(double)value_ {
	[self setWeight:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveWeightValue {
	NSNumber *result = [self primitiveWeight];
	return [result doubleValue];
}

- (void)setPrimitiveWeightValue:(double)value_ {
	[self setPrimitiveWeight:[NSNumber numberWithDouble:value_]];
}

@dynamic exerciseRoutine;

@end

