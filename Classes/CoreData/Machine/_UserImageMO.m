// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserImageMO.m instead.

#import "_UserImageMO.h"

const struct UserImageMOAttributes UserImageMOAttributes = {
	.accountId = @"accountId",
	.imageData = @"imageData",
	.imageId = @"imageId",
	.imageType = @"imageType",
	.loginId = @"loginId",
	.status = @"status",
};

const struct UserImageMORelationships UserImageMORelationships = {
	.customFood = @"customFood",
};

@implementation UserImageMOID
@end

@implementation _UserImageMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserImage" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserImage";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserImage" inManagedObjectContext:moc_];
}

- (UserImageMOID*)objectID {
	return (UserImageMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"imageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"imageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"imageTypeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"imageType"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic imageData;

@dynamic imageId;

- (int32_t)imageIdValue {
	NSNumber *result = [self imageId];
	return [result intValue];
}

- (void)setImageIdValue:(int32_t)value_ {
	[self setImageId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImageIdValue {
	NSNumber *result = [self primitiveImageId];
	return [result intValue];
}

- (void)setPrimitiveImageIdValue:(int32_t)value_ {
	[self setPrimitiveImageId:[NSNumber numberWithInt:value_]];
}

@dynamic imageType;

- (int16_t)imageTypeValue {
	NSNumber *result = [self imageType];
	return [result shortValue];
}

- (void)setImageTypeValue:(int16_t)value_ {
	[self setImageType:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveImageTypeValue {
	NSNumber *result = [self primitiveImageType];
	return [result shortValue];
}

- (void)setPrimitiveImageTypeValue:(int16_t)value_ {
	[self setPrimitiveImageType:[NSNumber numberWithShort:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic status;

@dynamic customFood;

@end

