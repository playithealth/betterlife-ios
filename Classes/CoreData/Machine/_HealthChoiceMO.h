// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to HealthChoiceMO.h instead.

#import <CoreData/CoreData.h>

extern const struct HealthChoiceMOAttributes {
	__unsafe_unretained NSString *choiceType;
	__unsafe_unretained NSString *detailText;
	__unsafe_unretained NSString *filter;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *sortOrder;
} HealthChoiceMOAttributes;

extern const struct HealthChoiceMORelationships {
	__unsafe_unretained NSString *userHealthChoices;
} HealthChoiceMORelationships;

@class UserHealthChoice;

@interface HealthChoiceMOID : NSManagedObjectID {}
@end

@interface _HealthChoiceMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) HealthChoiceMOID* objectID;

@property (nonatomic, strong) NSNumber* choiceType;

@property (atomic) int16_t choiceTypeValue;
- (int16_t)choiceTypeValue;
- (void)setChoiceTypeValue:(int16_t)value_;

//- (BOOL)validateChoiceType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* detailText;

//- (BOOL)validateDetailText:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* filter;

//- (BOOL)validateFilter:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *userHealthChoices;

- (NSMutableSet*)userHealthChoicesSet;

@end

@interface _HealthChoiceMO (UserHealthChoicesCoreDataGeneratedAccessors)
- (void)addUserHealthChoices:(NSSet*)value_;
- (void)removeUserHealthChoices:(NSSet*)value_;
- (void)addUserHealthChoicesObject:(UserHealthChoice*)value_;
- (void)removeUserHealthChoicesObject:(UserHealthChoice*)value_;

@end

@interface _HealthChoiceMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveChoiceType;
- (void)setPrimitiveChoiceType:(NSNumber*)value;

- (int16_t)primitiveChoiceTypeValue;
- (void)setPrimitiveChoiceTypeValue:(int16_t)value_;

- (NSString*)primitiveDetailText;
- (void)setPrimitiveDetailText:(NSString*)value;

- (NSString*)primitiveFilter;
- (void)setPrimitiveFilter:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSMutableSet*)primitiveUserHealthChoices;
- (void)setPrimitiveUserHealthChoices:(NSMutableSet*)value;

@end
