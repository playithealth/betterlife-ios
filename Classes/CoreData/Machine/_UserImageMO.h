// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserImageMO.h instead.

#import <CoreData/CoreData.h>

extern const struct UserImageMOAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *imageData;
	__unsafe_unretained NSString *imageId;
	__unsafe_unretained NSString *imageType;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *status;
} UserImageMOAttributes;

extern const struct UserImageMORelationships {
	__unsafe_unretained NSString *customFood;
} UserImageMORelationships;

@class CustomFoodMO;

@interface UserImageMOID : NSManagedObjectID {}
@end

@interface _UserImageMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserImageMOID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSData* imageData;

//- (BOOL)validateImageData:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* imageId;

@property (atomic) int32_t imageIdValue;
- (int32_t)imageIdValue;
- (void)setImageIdValue:(int32_t)value_;

//- (BOOL)validateImageId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* imageType;

@property (atomic) int16_t imageTypeValue;
- (int16_t)imageTypeValue;
- (void)setImageTypeValue:(int16_t)value_;

//- (BOOL)validateImageType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CustomFoodMO *customFood;

//- (BOOL)validateCustomFood:(id*)value_ error:(NSError**)error_;

@end

@interface _UserImageMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSData*)primitiveImageData;
- (void)setPrimitiveImageData:(NSData*)value;

- (NSNumber*)primitiveImageId;
- (void)setPrimitiveImageId:(NSNumber*)value;

- (int32_t)primitiveImageIdValue;
- (void)setPrimitiveImageIdValue:(int32_t)value_;

- (NSNumber*)primitiveImageType;
- (void)setPrimitiveImageType:(NSNumber*)value;

- (int16_t)primitiveImageTypeValue;
- (void)setPrimitiveImageTypeValue:(int16_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (CustomFoodMO*)primitiveCustomFood;
- (void)setPrimitiveCustomFood:(CustomFoodMO*)value;

@end
