// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SyncUpdateMO.m instead.

#import "_SyncUpdateMO.h"

const struct SyncUpdateMOAttributes SyncUpdateMOAttributes = {
	.lastUpdate = @"lastUpdate",
	.loginId = @"loginId",
	.tableName = @"tableName",
};

@implementation SyncUpdateMOID
@end

@implementation _SyncUpdateMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SyncUpdate" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SyncUpdate";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SyncUpdate" inManagedObjectContext:moc_];
}

- (SyncUpdateMOID*)objectID {
	return (SyncUpdateMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"lastUpdateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"lastUpdate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic lastUpdate;

- (int32_t)lastUpdateValue {
	NSNumber *result = [self lastUpdate];
	return [result intValue];
}

- (void)setLastUpdateValue:(int32_t)value_ {
	[self setLastUpdate:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLastUpdateValue {
	NSNumber *result = [self primitiveLastUpdate];
	return [result intValue];
}

- (void)setPrimitiveLastUpdateValue:(int32_t)value_ {
	[self setPrimitiveLastUpdate:[NSNumber numberWithInt:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic tableName;

@end

