// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityLogMO.m instead.

#import "_ActivityLogMO.h"

const struct ActivityLogMOAttributes ActivityLogMOAttributes = {
	.elapsedDuration = @"elapsedDuration",
};

@implementation ActivityLogMOID
@end

@implementation _ActivityLogMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ActivityLog" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ActivityLog";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ActivityLog" inManagedObjectContext:moc_];
}

- (ActivityLogMOID*)objectID {
	return (ActivityLogMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"elapsedDurationValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"elapsedDuration"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic elapsedDuration;

- (double)elapsedDurationValue {
	NSNumber *result = [self elapsedDuration];
	return [result doubleValue];
}

- (void)setElapsedDurationValue:(double)value_ {
	[self setElapsedDuration:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveElapsedDurationValue {
	NSNumber *result = [self primitiveElapsedDuration];
	return [result doubleValue];
}

- (void)setPrimitiveElapsedDurationValue:(double)value_ {
	[self setPrimitiveElapsedDuration:[NSNumber numberWithDouble:value_]];
}

@end

