// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanPhaseMO.m instead.

#import "_AdvisorMealPlanPhaseMO.h"

const struct AdvisorMealPlanPhaseMOAttributes AdvisorMealPlanPhaseMOAttributes = {
	.bmr = @"bmr",
	.proteinRatio = @"proteinRatio",
	.totalCarbohydrateRatio = @"totalCarbohydrateRatio",
	.totalFatRatio = @"totalFatRatio",
	.useAutoRatios = @"useAutoRatios",
};

const struct AdvisorMealPlanPhaseMORelationships AdvisorMealPlanPhaseMORelationships = {
	.entries = @"entries",
	.maxNutrition = @"maxNutrition",
	.minNutrition = @"minNutrition",
};

@implementation AdvisorMealPlanPhaseMOID
@end

@implementation _AdvisorMealPlanPhaseMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorMealPlanPhase" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorMealPlanPhase";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorMealPlanPhase" inManagedObjectContext:moc_];
}

- (AdvisorMealPlanPhaseMOID*)objectID {
	return (AdvisorMealPlanPhaseMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"bmrValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"bmr"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"proteinRatioValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"proteinRatio"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalCarbohydrateRatioValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalCarbohydrateRatio"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalFatRatioValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalFatRatio"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"useAutoRatiosValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"useAutoRatios"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic bmr;

- (double)bmrValue {
	NSNumber *result = [self bmr];
	return [result doubleValue];
}

- (void)setBmrValue:(double)value_ {
	[self setBmr:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveBmrValue {
	NSNumber *result = [self primitiveBmr];
	return [result doubleValue];
}

- (void)setPrimitiveBmrValue:(double)value_ {
	[self setPrimitiveBmr:[NSNumber numberWithDouble:value_]];
}

@dynamic proteinRatio;

- (int16_t)proteinRatioValue {
	NSNumber *result = [self proteinRatio];
	return [result shortValue];
}

- (void)setProteinRatioValue:(int16_t)value_ {
	[self setProteinRatio:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveProteinRatioValue {
	NSNumber *result = [self primitiveProteinRatio];
	return [result shortValue];
}

- (void)setPrimitiveProteinRatioValue:(int16_t)value_ {
	[self setPrimitiveProteinRatio:[NSNumber numberWithShort:value_]];
}

@dynamic totalCarbohydrateRatio;

- (int16_t)totalCarbohydrateRatioValue {
	NSNumber *result = [self totalCarbohydrateRatio];
	return [result shortValue];
}

- (void)setTotalCarbohydrateRatioValue:(int16_t)value_ {
	[self setTotalCarbohydrateRatio:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveTotalCarbohydrateRatioValue {
	NSNumber *result = [self primitiveTotalCarbohydrateRatio];
	return [result shortValue];
}

- (void)setPrimitiveTotalCarbohydrateRatioValue:(int16_t)value_ {
	[self setPrimitiveTotalCarbohydrateRatio:[NSNumber numberWithShort:value_]];
}

@dynamic totalFatRatio;

- (int16_t)totalFatRatioValue {
	NSNumber *result = [self totalFatRatio];
	return [result shortValue];
}

- (void)setTotalFatRatioValue:(int16_t)value_ {
	[self setTotalFatRatio:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveTotalFatRatioValue {
	NSNumber *result = [self primitiveTotalFatRatio];
	return [result shortValue];
}

- (void)setPrimitiveTotalFatRatioValue:(int16_t)value_ {
	[self setPrimitiveTotalFatRatio:[NSNumber numberWithShort:value_]];
}

@dynamic useAutoRatios;

- (BOOL)useAutoRatiosValue {
	NSNumber *result = [self useAutoRatios];
	return [result boolValue];
}

- (void)setUseAutoRatiosValue:(BOOL)value_ {
	[self setUseAutoRatios:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUseAutoRatiosValue {
	NSNumber *result = [self primitiveUseAutoRatios];
	return [result boolValue];
}

- (void)setPrimitiveUseAutoRatiosValue:(BOOL)value_ {
	[self setPrimitiveUseAutoRatios:[NSNumber numberWithBool:value_]];
}

@dynamic entries;

- (NSMutableSet*)entriesSet {
	[self willAccessValueForKey:@"entries"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"entries"];

	[self didAccessValueForKey:@"entries"];
	return result;
}

@dynamic maxNutrition;

@dynamic minNutrition;

@end

