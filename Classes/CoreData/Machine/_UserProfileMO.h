// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserProfileMO.h instead.

#import <CoreData/CoreData.h>

extern const struct UserProfileMOAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *activityLevel;
	__unsafe_unretained NSString *birthday;
	__unsafe_unretained NSString *currentWeight;
	__unsafe_unretained NSString *customNutritionTimeframe;
	__unsafe_unretained NSString *customTrackingTimeframe;
	__unsafe_unretained NSString *email;
	__unsafe_unretained NSString *gender;
	__unsafe_unretained NSString *goalWeight;
	__unsafe_unretained NSString *hasMyZone;
	__unsafe_unretained NSString *height;
	__unsafe_unretained NSString *imageId;
	__unsafe_unretained NSString *isPaidCustomer;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *nutritionEffort;
	__unsafe_unretained NSString *onboardingComplete;
	__unsafe_unretained NSString *proteinRatio;
	__unsafe_unretained NSString *skin;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *syncToken;
	__unsafe_unretained NSString *totalCarbohydrateRatio;
	__unsafe_unretained NSString *totalFatRatio;
	__unsafe_unretained NSString *useAutoRatios;
	__unsafe_unretained NSString *useBMR;
	__unsafe_unretained NSString *zipCode;
} UserProfileMOAttributes;

extern const struct UserProfileMORelationships {
	__unsafe_unretained NSString *alwaysShowNutrients;
	__unsafe_unretained NSString *communities;
	__unsafe_unretained NSString *maxNutrition;
	__unsafe_unretained NSString *minNutrition;
	__unsafe_unretained NSString *permissions;
	__unsafe_unretained NSString *threadMessages;
	__unsafe_unretained NSString *threads;
} UserProfileMORelationships;

@class NutrientMO;
@class CommunityMO;
@class NutritionMO;
@class NutritionMO;
@class UserPermissionMO;
@class ThreadMessageMO;
@class ThreadMO;

@interface UserProfileMOID : NSManagedObjectID {}
@end

@interface _UserProfileMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserProfileMOID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* activityLevel;

@property (atomic) double activityLevelValue;
- (double)activityLevelValue;
- (void)setActivityLevelValue:(double)value_;

//- (BOOL)validateActivityLevel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* birthday;

//- (BOOL)validateBirthday:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* currentWeight;

@property (atomic) double currentWeightValue;
- (double)currentWeightValue;
- (void)setCurrentWeightValue:(double)value_;

//- (BOOL)validateCurrentWeight:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* customNutritionTimeframe;

@property (atomic) int16_t customNutritionTimeframeValue;
- (int16_t)customNutritionTimeframeValue;
- (void)setCustomNutritionTimeframeValue:(int16_t)value_;

//- (BOOL)validateCustomNutritionTimeframe:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* customTrackingTimeframe;

@property (atomic) int16_t customTrackingTimeframeValue;
- (int16_t)customTrackingTimeframeValue;
- (void)setCustomTrackingTimeframeValue:(int16_t)value_;

//- (BOOL)validateCustomTrackingTimeframe:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* email;

//- (BOOL)validateEmail:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gender;

@property (atomic) int16_t genderValue;
- (int16_t)genderValue;
- (void)setGenderValue:(int16_t)value_;

//- (BOOL)validateGender:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* goalWeight;

@property (atomic) int16_t goalWeightValue;
- (int16_t)goalWeightValue;
- (void)setGoalWeightValue:(int16_t)value_;

//- (BOOL)validateGoalWeight:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* hasMyZone;

@property (atomic) BOOL hasMyZoneValue;
- (BOOL)hasMyZoneValue;
- (void)setHasMyZoneValue:(BOOL)value_;

//- (BOOL)validateHasMyZone:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* height;

@property (atomic) int16_t heightValue;
- (int16_t)heightValue;
- (void)setHeightValue:(int16_t)value_;

//- (BOOL)validateHeight:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* imageId;

@property (atomic) int32_t imageIdValue;
- (int32_t)imageIdValue;
- (void)setImageIdValue:(int32_t)value_;

//- (BOOL)validateImageId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isPaidCustomer;

@property (atomic) BOOL isPaidCustomerValue;
- (BOOL)isPaidCustomerValue;
- (void)setIsPaidCustomerValue:(BOOL)value_;

//- (BOOL)validateIsPaidCustomer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* nutritionEffort;

@property (atomic) int16_t nutritionEffortValue;
- (int16_t)nutritionEffortValue;
- (void)setNutritionEffortValue:(int16_t)value_;

//- (BOOL)validateNutritionEffort:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* onboardingComplete;

@property (atomic) BOOL onboardingCompleteValue;
- (BOOL)onboardingCompleteValue;
- (void)setOnboardingCompleteValue:(BOOL)value_;

//- (BOOL)validateOnboardingComplete:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* proteinRatio;

@property (atomic) int16_t proteinRatioValue;
- (int16_t)proteinRatioValue;
- (void)setProteinRatioValue:(int16_t)value_;

//- (BOOL)validateProteinRatio:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* skin;

//- (BOOL)validateSkin:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* syncToken;

@property (atomic) int32_t syncTokenValue;
- (int32_t)syncTokenValue;
- (void)setSyncTokenValue:(int32_t)value_;

//- (BOOL)validateSyncToken:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* totalCarbohydrateRatio;

@property (atomic) int16_t totalCarbohydrateRatioValue;
- (int16_t)totalCarbohydrateRatioValue;
- (void)setTotalCarbohydrateRatioValue:(int16_t)value_;

//- (BOOL)validateTotalCarbohydrateRatio:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* totalFatRatio;

@property (atomic) int16_t totalFatRatioValue;
- (int16_t)totalFatRatioValue;
- (void)setTotalFatRatioValue:(int16_t)value_;

//- (BOOL)validateTotalFatRatio:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* useAutoRatios;

@property (atomic) BOOL useAutoRatiosValue;
- (BOOL)useAutoRatiosValue;
- (void)setUseAutoRatiosValue:(BOOL)value_;

//- (BOOL)validateUseAutoRatios:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* useBMR;

@property (atomic) BOOL useBMRValue;
- (BOOL)useBMRValue;
- (void)setUseBMRValue:(BOOL)value_;

//- (BOOL)validateUseBMR:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* zipCode;

//- (BOOL)validateZipCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *alwaysShowNutrients;

- (NSMutableSet*)alwaysShowNutrientsSet;

@property (nonatomic, strong) NSSet *communities;

- (NSMutableSet*)communitiesSet;

@property (nonatomic, strong) NutritionMO *maxNutrition;

//- (BOOL)validateMaxNutrition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NutritionMO *minNutrition;

//- (BOOL)validateMinNutrition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *permissions;

- (NSMutableSet*)permissionsSet;

@property (nonatomic, strong) NSSet *threadMessages;

- (NSMutableSet*)threadMessagesSet;

@property (nonatomic, strong) NSSet *threads;

- (NSMutableSet*)threadsSet;

@end

@interface _UserProfileMO (AlwaysShowNutrientsCoreDataGeneratedAccessors)
- (void)addAlwaysShowNutrients:(NSSet*)value_;
- (void)removeAlwaysShowNutrients:(NSSet*)value_;
- (void)addAlwaysShowNutrientsObject:(NutrientMO*)value_;
- (void)removeAlwaysShowNutrientsObject:(NutrientMO*)value_;

@end

@interface _UserProfileMO (CommunitiesCoreDataGeneratedAccessors)
- (void)addCommunities:(NSSet*)value_;
- (void)removeCommunities:(NSSet*)value_;
- (void)addCommunitiesObject:(CommunityMO*)value_;
- (void)removeCommunitiesObject:(CommunityMO*)value_;

@end

@interface _UserProfileMO (PermissionsCoreDataGeneratedAccessors)
- (void)addPermissions:(NSSet*)value_;
- (void)removePermissions:(NSSet*)value_;
- (void)addPermissionsObject:(UserPermissionMO*)value_;
- (void)removePermissionsObject:(UserPermissionMO*)value_;

@end

@interface _UserProfileMO (ThreadMessagesCoreDataGeneratedAccessors)
- (void)addThreadMessages:(NSSet*)value_;
- (void)removeThreadMessages:(NSSet*)value_;
- (void)addThreadMessagesObject:(ThreadMessageMO*)value_;
- (void)removeThreadMessagesObject:(ThreadMessageMO*)value_;

@end

@interface _UserProfileMO (ThreadsCoreDataGeneratedAccessors)
- (void)addThreads:(NSSet*)value_;
- (void)removeThreads:(NSSet*)value_;
- (void)addThreadsObject:(ThreadMO*)value_;
- (void)removeThreadsObject:(ThreadMO*)value_;

@end

@interface _UserProfileMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSNumber*)primitiveActivityLevel;
- (void)setPrimitiveActivityLevel:(NSNumber*)value;

- (double)primitiveActivityLevelValue;
- (void)setPrimitiveActivityLevelValue:(double)value_;

- (NSDate*)primitiveBirthday;
- (void)setPrimitiveBirthday:(NSDate*)value;

- (NSNumber*)primitiveCurrentWeight;
- (void)setPrimitiveCurrentWeight:(NSNumber*)value;

- (double)primitiveCurrentWeightValue;
- (void)setPrimitiveCurrentWeightValue:(double)value_;

- (NSNumber*)primitiveCustomNutritionTimeframe;
- (void)setPrimitiveCustomNutritionTimeframe:(NSNumber*)value;

- (int16_t)primitiveCustomNutritionTimeframeValue;
- (void)setPrimitiveCustomNutritionTimeframeValue:(int16_t)value_;

- (NSNumber*)primitiveCustomTrackingTimeframe;
- (void)setPrimitiveCustomTrackingTimeframe:(NSNumber*)value;

- (int16_t)primitiveCustomTrackingTimeframeValue;
- (void)setPrimitiveCustomTrackingTimeframeValue:(int16_t)value_;

- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;

- (NSNumber*)primitiveGender;
- (void)setPrimitiveGender:(NSNumber*)value;

- (int16_t)primitiveGenderValue;
- (void)setPrimitiveGenderValue:(int16_t)value_;

- (NSNumber*)primitiveGoalWeight;
- (void)setPrimitiveGoalWeight:(NSNumber*)value;

- (int16_t)primitiveGoalWeightValue;
- (void)setPrimitiveGoalWeightValue:(int16_t)value_;

- (NSNumber*)primitiveHasMyZone;
- (void)setPrimitiveHasMyZone:(NSNumber*)value;

- (BOOL)primitiveHasMyZoneValue;
- (void)setPrimitiveHasMyZoneValue:(BOOL)value_;

- (NSNumber*)primitiveHeight;
- (void)setPrimitiveHeight:(NSNumber*)value;

- (int16_t)primitiveHeightValue;
- (void)setPrimitiveHeightValue:(int16_t)value_;

- (NSNumber*)primitiveImageId;
- (void)setPrimitiveImageId:(NSNumber*)value;

- (int32_t)primitiveImageIdValue;
- (void)setPrimitiveImageIdValue:(int32_t)value_;

- (NSNumber*)primitiveIsPaidCustomer;
- (void)setPrimitiveIsPaidCustomer:(NSNumber*)value;

- (BOOL)primitiveIsPaidCustomerValue;
- (void)setPrimitiveIsPaidCustomerValue:(BOOL)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveNutritionEffort;
- (void)setPrimitiveNutritionEffort:(NSNumber*)value;

- (int16_t)primitiveNutritionEffortValue;
- (void)setPrimitiveNutritionEffortValue:(int16_t)value_;

- (NSNumber*)primitiveOnboardingComplete;
- (void)setPrimitiveOnboardingComplete:(NSNumber*)value;

- (BOOL)primitiveOnboardingCompleteValue;
- (void)setPrimitiveOnboardingCompleteValue:(BOOL)value_;

- (NSNumber*)primitiveProteinRatio;
- (void)setPrimitiveProteinRatio:(NSNumber*)value;

- (int16_t)primitiveProteinRatioValue;
- (void)setPrimitiveProteinRatioValue:(int16_t)value_;

- (NSString*)primitiveSkin;
- (void)setPrimitiveSkin:(NSString*)value;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveSyncToken;
- (void)setPrimitiveSyncToken:(NSNumber*)value;

- (int32_t)primitiveSyncTokenValue;
- (void)setPrimitiveSyncTokenValue:(int32_t)value_;

- (NSNumber*)primitiveTotalCarbohydrateRatio;
- (void)setPrimitiveTotalCarbohydrateRatio:(NSNumber*)value;

- (int16_t)primitiveTotalCarbohydrateRatioValue;
- (void)setPrimitiveTotalCarbohydrateRatioValue:(int16_t)value_;

- (NSNumber*)primitiveTotalFatRatio;
- (void)setPrimitiveTotalFatRatio:(NSNumber*)value;

- (int16_t)primitiveTotalFatRatioValue;
- (void)setPrimitiveTotalFatRatioValue:(int16_t)value_;

- (NSNumber*)primitiveUseAutoRatios;
- (void)setPrimitiveUseAutoRatios:(NSNumber*)value;

- (BOOL)primitiveUseAutoRatiosValue;
- (void)setPrimitiveUseAutoRatiosValue:(BOOL)value_;

- (NSNumber*)primitiveUseBMR;
- (void)setPrimitiveUseBMR:(NSNumber*)value;

- (BOOL)primitiveUseBMRValue;
- (void)setPrimitiveUseBMRValue:(BOOL)value_;

- (NSString*)primitiveZipCode;
- (void)setPrimitiveZipCode:(NSString*)value;

- (NSMutableSet*)primitiveAlwaysShowNutrients;
- (void)setPrimitiveAlwaysShowNutrients:(NSMutableSet*)value;

- (NSMutableSet*)primitiveCommunities;
- (void)setPrimitiveCommunities:(NSMutableSet*)value;

- (NutritionMO*)primitiveMaxNutrition;
- (void)setPrimitiveMaxNutrition:(NutritionMO*)value;

- (NutritionMO*)primitiveMinNutrition;
- (void)setPrimitiveMinNutrition:(NutritionMO*)value;

- (NSMutableSet*)primitivePermissions;
- (void)setPrimitivePermissions:(NSMutableSet*)value;

- (NSMutableSet*)primitiveThreadMessages;
- (void)setPrimitiveThreadMessages:(NSMutableSet*)value;

- (NSMutableSet*)primitiveThreads;
- (void)setPrimitiveThreads:(NSMutableSet*)value;

@end
