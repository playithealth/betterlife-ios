// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealPredictionMO.m instead.

#import "_MealPredictionMO.h"

const struct MealPredictionMOAttributes MealPredictionMOAttributes = {
	.date = @"date",
	.image_id = @"image_id",
	.item_id = @"item_id",
	.item_type = @"item_type",
	.logTime = @"logTime",
	.login_id = @"login_id",
	.name = @"name",
	.serving_size_text = @"serving_size_text",
	.serving_size_uom = @"serving_size_uom",
	.servings = @"servings",
	.status = @"status",
	.updatedOn = @"updatedOn",
	.usageCount = @"usageCount",
};

const struct MealPredictionMORelationships MealPredictionMORelationships = {
	.customFood = @"customFood",
	.entree = @"entree",
	.nutrition = @"nutrition",
	.recipe = @"recipe",
	.sides = @"sides",
	.tag = @"tag",
};

@implementation MealPredictionMOID
@end

@implementation _MealPredictionMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MealPrediction" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MealPrediction";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MealPrediction" inManagedObjectContext:moc_];
}

- (MealPredictionMOID*)objectID {
	return (MealPredictionMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"image_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"image_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"item_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"item_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"logTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"logTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"login_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"login_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"servingsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"servings"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"usageCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"usageCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic date;

@dynamic image_id;

- (int32_t)image_idValue {
	NSNumber *result = [self image_id];
	return [result intValue];
}

- (void)setImage_idValue:(int32_t)value_ {
	[self setImage_id:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImage_idValue {
	NSNumber *result = [self primitiveImage_id];
	return [result intValue];
}

- (void)setPrimitiveImage_idValue:(int32_t)value_ {
	[self setPrimitiveImage_id:[NSNumber numberWithInt:value_]];
}

@dynamic item_id;

- (int32_t)item_idValue {
	NSNumber *result = [self item_id];
	return [result intValue];
}

- (void)setItem_idValue:(int32_t)value_ {
	[self setItem_id:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveItem_idValue {
	NSNumber *result = [self primitiveItem_id];
	return [result intValue];
}

- (void)setPrimitiveItem_idValue:(int32_t)value_ {
	[self setPrimitiveItem_id:[NSNumber numberWithInt:value_]];
}

@dynamic item_type;

@dynamic logTime;

- (int16_t)logTimeValue {
	NSNumber *result = [self logTime];
	return [result shortValue];
}

- (void)setLogTimeValue:(int16_t)value_ {
	[self setLogTime:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLogTimeValue {
	NSNumber *result = [self primitiveLogTime];
	return [result shortValue];
}

- (void)setPrimitiveLogTimeValue:(int16_t)value_ {
	[self setPrimitiveLogTime:[NSNumber numberWithShort:value_]];
}

@dynamic login_id;

- (int16_t)login_idValue {
	NSNumber *result = [self login_id];
	return [result shortValue];
}

- (void)setLogin_idValue:(int16_t)value_ {
	[self setLogin_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLogin_idValue {
	NSNumber *result = [self primitiveLogin_id];
	return [result shortValue];
}

- (void)setPrimitiveLogin_idValue:(int16_t)value_ {
	[self setPrimitiveLogin_id:[NSNumber numberWithShort:value_]];
}

@dynamic name;

@dynamic serving_size_text;

@dynamic serving_size_uom;

@dynamic servings;

- (double)servingsValue {
	NSNumber *result = [self servings];
	return [result doubleValue];
}

- (void)setServingsValue:(double)value_ {
	[self setServings:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveServingsValue {
	NSNumber *result = [self primitiveServings];
	return [result doubleValue];
}

- (void)setPrimitiveServingsValue:(double)value_ {
	[self setPrimitiveServings:[NSNumber numberWithDouble:value_]];
}

@dynamic status;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic usageCount;

- (int16_t)usageCountValue {
	NSNumber *result = [self usageCount];
	return [result shortValue];
}

- (void)setUsageCountValue:(int16_t)value_ {
	[self setUsageCount:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveUsageCountValue {
	NSNumber *result = [self primitiveUsageCount];
	return [result shortValue];
}

- (void)setPrimitiveUsageCountValue:(int16_t)value_ {
	[self setPrimitiveUsageCount:[NSNumber numberWithShort:value_]];
}

@dynamic customFood;

@dynamic entree;

@dynamic nutrition;

@dynamic recipe;

@dynamic sides;

- (NSMutableSet*)sidesSet {
	[self willAccessValueForKey:@"sides"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"sides"];

	[self didAccessValueForKey:@"sides"];
	return result;
}

@dynamic tag;

@end

