// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ExerciseRoutineMO.m instead.

#import "_ExerciseRoutineMO.h"

const struct ExerciseRoutineMOAttributes ExerciseRoutineMOAttributes = {
	.activity = @"activity",
	.cloudId = @"cloudId",
	.factor = @"factor",
};

const struct ExerciseRoutineMORelationships ExerciseRoutineMORelationships = {
	.trainingActivities = @"trainingActivities",
};

@implementation ExerciseRoutineMOID
@end

@implementation _ExerciseRoutineMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ExerciseRoutine" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ExerciseRoutine";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ExerciseRoutine" inManagedObjectContext:moc_];
}

- (ExerciseRoutineMOID*)objectID {
	return (ExerciseRoutineMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"factorValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"factor"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic activity;

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic factor;

- (double)factorValue {
	NSNumber *result = [self factor];
	return [result doubleValue];
}

- (void)setFactorValue:(double)value_ {
	[self setFactor:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveFactorValue {
	NSNumber *result = [self primitiveFactor];
	return [result doubleValue];
}

- (void)setPrimitiveFactorValue:(double)value_ {
	[self setPrimitiveFactor:[NSNumber numberWithDouble:value_]];
}

@dynamic trainingActivities;

- (NSMutableSet*)trainingActivitiesSet {
	[self willAccessValueForKey:@"trainingActivities"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"trainingActivities"];

	[self didAccessValueForKey:@"trainingActivities"];
	return result;
}

@end

