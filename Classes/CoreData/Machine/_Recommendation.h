// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Recommendation.h instead.

#import <CoreData/CoreData.h>

extern const struct RecommendationAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *days;
	__unsafe_unretained NSString *hidden;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *productId;
	__unsafe_unretained NSString *quantity;
	__unsafe_unretained NSString *status;
} RecommendationAttributes;

extern const struct RecommendationRelationships {
	__unsafe_unretained NSString *category;
} RecommendationRelationships;

@class CategoryMO;

@interface RecommendationID : NSManagedObjectID {}
@end

@interface _Recommendation : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) RecommendationID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* days;

@property (atomic) int16_t daysValue;
- (int16_t)daysValue;
- (void)setDaysValue:(int16_t)value_;

//- (BOOL)validateDays:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* hidden;

@property (atomic) BOOL hiddenValue;
- (BOOL)hiddenValue;
- (void)setHiddenValue:(BOOL)value_;

//- (BOOL)validateHidden:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* productId;

@property (atomic) int32_t productIdValue;
- (int32_t)productIdValue;
- (void)setProductIdValue:(int32_t)value_;

//- (BOOL)validateProductId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* quantity;

@property (atomic) int16_t quantityValue;
- (int16_t)quantityValue;
- (void)setQuantityValue:(int16_t)value_;

//- (BOOL)validateQuantity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CategoryMO *category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;

@end

@interface _Recommendation (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSNumber*)primitiveDays;
- (void)setPrimitiveDays:(NSNumber*)value;

- (int16_t)primitiveDaysValue;
- (void)setPrimitiveDaysValue:(int16_t)value_;

- (NSNumber*)primitiveHidden;
- (void)setPrimitiveHidden:(NSNumber*)value;

- (BOOL)primitiveHiddenValue;
- (void)setPrimitiveHiddenValue:(BOOL)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveProductId;
- (void)setPrimitiveProductId:(NSNumber*)value;

- (int32_t)primitiveProductIdValue;
- (void)setPrimitiveProductIdValue:(int32_t)value_;

- (NSNumber*)primitiveQuantity;
- (void)setPrimitiveQuantity:(NSNumber*)value;

- (int16_t)primitiveQuantityValue;
- (void)setPrimitiveQuantityValue:(int16_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (CategoryMO*)primitiveCategory;
- (void)setPrimitiveCategory:(CategoryMO*)value;

@end
