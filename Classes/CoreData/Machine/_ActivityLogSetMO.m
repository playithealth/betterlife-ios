// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityLogSetMO.m instead.

#import "_ActivityLogSetMO.h"

@implementation ActivityLogSetMOID
@end

@implementation _ActivityLogSetMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ActivityLogSet" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ActivityLogSet";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ActivityLogSet" inManagedObjectContext:moc_];
}

- (ActivityLogSetMOID*)objectID {
	return (ActivityLogSetMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@end

