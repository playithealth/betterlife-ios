// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorPlanMO.h instead.

#import <CoreData/CoreData.h>

extern const struct AdvisorPlanMOAttributes {
	__unsafe_unretained NSString *advisorId;
	__unsafe_unretained NSString *advisorName;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *isNew;
	__unsafe_unretained NSString *isRepeating;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *planDescription;
	__unsafe_unretained NSString *planName;
	__unsafe_unretained NSString *status;
} AdvisorPlanMOAttributes;

extern const struct AdvisorPlanMORelationships {
	__unsafe_unretained NSString *phases;
	__unsafe_unretained NSString *timelines;
} AdvisorPlanMORelationships;

@class AdvisorPlanPhaseMO;
@class AdvisorPlanTimelineMO;

@interface AdvisorPlanMOID : NSManagedObjectID {}
@end

@interface _AdvisorPlanMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorPlanMOID* objectID;

@property (nonatomic, strong) NSNumber* advisorId;

@property (atomic) int32_t advisorIdValue;
- (int32_t)advisorIdValue;
- (void)setAdvisorIdValue:(int32_t)value_;

//- (BOOL)validateAdvisorId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* advisorName;

//- (BOOL)validateAdvisorName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isNew;

@property (atomic) BOOL isNewValue;
- (BOOL)isNewValue;
- (void)setIsNewValue:(BOOL)value_;

//- (BOOL)validateIsNew:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRepeating;

@property (atomic) BOOL isRepeatingValue;
- (BOOL)isRepeatingValue;
- (void)setIsRepeatingValue:(BOOL)value_;

//- (BOOL)validateIsRepeating:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* planDescription;

//- (BOOL)validatePlanDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* planName;

//- (BOOL)validatePlanName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *phases;

- (NSMutableSet*)phasesSet;

@property (nonatomic, strong) NSSet *timelines;

- (NSMutableSet*)timelinesSet;

@end

@interface _AdvisorPlanMO (PhasesCoreDataGeneratedAccessors)
- (void)addPhases:(NSSet*)value_;
- (void)removePhases:(NSSet*)value_;
- (void)addPhasesObject:(AdvisorPlanPhaseMO*)value_;
- (void)removePhasesObject:(AdvisorPlanPhaseMO*)value_;

@end

@interface _AdvisorPlanMO (TimelinesCoreDataGeneratedAccessors)
- (void)addTimelines:(NSSet*)value_;
- (void)removeTimelines:(NSSet*)value_;
- (void)addTimelinesObject:(AdvisorPlanTimelineMO*)value_;
- (void)removeTimelinesObject:(AdvisorPlanTimelineMO*)value_;

@end

@interface _AdvisorPlanMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAdvisorId;
- (void)setPrimitiveAdvisorId:(NSNumber*)value;

- (int32_t)primitiveAdvisorIdValue;
- (void)setPrimitiveAdvisorIdValue:(int32_t)value_;

- (NSString*)primitiveAdvisorName;
- (void)setPrimitiveAdvisorName:(NSString*)value;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveIsNew;
- (void)setPrimitiveIsNew:(NSNumber*)value;

- (BOOL)primitiveIsNewValue;
- (void)setPrimitiveIsNewValue:(BOOL)value_;

- (NSNumber*)primitiveIsRepeating;
- (void)setPrimitiveIsRepeating:(NSNumber*)value;

- (BOOL)primitiveIsRepeatingValue;
- (void)setPrimitiveIsRepeatingValue:(BOOL)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitivePlanDescription;
- (void)setPrimitivePlanDescription:(NSString*)value;

- (NSString*)primitivePlanName;
- (void)setPrimitivePlanName:(NSString*)value;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSMutableSet*)primitivePhases;
- (void)setPrimitivePhases:(NSMutableSet*)value;

- (NSMutableSet*)primitiveTimelines;
- (void)setPrimitiveTimelines:(NSMutableSet*)value;

@end
