// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to QuickListItem.m instead.

#import "_QuickListItem.h"

const struct QuickListItemAttributes QuickListItemAttributes = {
	.item = @"item",
	.productId = @"productId",
};

const struct QuickListItemRelationships QuickListItemRelationships = {
	.QuickList = @"QuickList",
};

const struct QuickListItemFetchedProperties QuickListItemFetchedProperties = {
};

@implementation QuickListItemID
@end

@implementation _QuickListItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"QuickListItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"QuickListItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"QuickListItem" inManagedObjectContext:moc_];
}

- (QuickListItemID*)objectID {
	return (QuickListItemID*)[super objectID];
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"productIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"productId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}

	return keyPaths;
}




@dynamic item;






@dynamic productId;



- (int32_t)productIdValue {
	NSNumber *result = [self productId];
	return [result intValue];
}

- (void)setProductIdValue:(int32_t)value_ {
	[self setProductId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveProductIdValue {
	NSNumber *result = [self primitiveProductId];
	return [result intValue];
}

- (void)setPrimitiveProductIdValue:(int32_t)value_ {
	[self setPrimitiveProductId:[NSNumber numberWithInt:value_]];
}





@dynamic QuickList;

	






@end
