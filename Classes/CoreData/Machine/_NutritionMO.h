// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to NutritionMO.h instead.

#import <CoreData/CoreData.h>

extern const struct NutritionMOAttributes {
	__unsafe_unretained NSString *calciumPercent;
	__unsafe_unretained NSString *calories;
	__unsafe_unretained NSString *caloriesFromFat;
	__unsafe_unretained NSString *cholesterol;
	__unsafe_unretained NSString *dietaryFiber;
	__unsafe_unretained NSString *insolubleFiber;
	__unsafe_unretained NSString *ironPercent;
	__unsafe_unretained NSString *monounsaturatedFat;
	__unsafe_unretained NSString *nutritionId;
	__unsafe_unretained NSString *otherCarbohydrates;
	__unsafe_unretained NSString *polyunsaturatedFat;
	__unsafe_unretained NSString *potassium;
	__unsafe_unretained NSString *protein;
	__unsafe_unretained NSString *saturatedFat;
	__unsafe_unretained NSString *saturatedFatCalories;
	__unsafe_unretained NSString *sodium;
	__unsafe_unretained NSString *solubleFiber;
	__unsafe_unretained NSString *sugars;
	__unsafe_unretained NSString *sugarsAlcohol;
	__unsafe_unretained NSString *totalCarbohydrates;
	__unsafe_unretained NSString *totalFat;
	__unsafe_unretained NSString *transFat;
	__unsafe_unretained NSString *vitaminAPercent;
	__unsafe_unretained NSString *vitaminCPercent;
} NutritionMOAttributes;

extern const struct NutritionMORelationships {
	__unsafe_unretained NSString *customFood;
	__unsafe_unretained NSString *foodLog;
	__unsafe_unretained NSString *maxPhase;
	__unsafe_unretained NSString *maxUserProfile;
	__unsafe_unretained NSString *mealPlannerDay;
	__unsafe_unretained NSString *mealPredictions;
	__unsafe_unretained NSString *minPhase;
	__unsafe_unretained NSString *minUserProfile;
	__unsafe_unretained NSString *recipe;
} NutritionMORelationships;

@class CustomFoodMO;
@class FoodLogMO;
@class AdvisorMealPlanPhaseMO;
@class UserProfileMO;
@class MealPlannerDay;
@class MealPredictionMO;
@class AdvisorMealPlanPhaseMO;
@class UserProfileMO;
@class RecipeMO;

@interface NutritionMOID : NSManagedObjectID {}
@end

@interface _NutritionMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) NutritionMOID* objectID;

@property (nonatomic, strong) NSNumber* calciumPercent;

@property (atomic) double calciumPercentValue;
- (double)calciumPercentValue;
- (void)setCalciumPercentValue:(double)value_;

//- (BOOL)validateCalciumPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* calories;

@property (atomic) double caloriesValue;
- (double)caloriesValue;
- (void)setCaloriesValue:(double)value_;

//- (BOOL)validateCalories:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* caloriesFromFat;

@property (atomic) double caloriesFromFatValue;
- (double)caloriesFromFatValue;
- (void)setCaloriesFromFatValue:(double)value_;

//- (BOOL)validateCaloriesFromFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cholesterol;

@property (atomic) double cholesterolValue;
- (double)cholesterolValue;
- (void)setCholesterolValue:(double)value_;

//- (BOOL)validateCholesterol:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* dietaryFiber;

@property (atomic) double dietaryFiberValue;
- (double)dietaryFiberValue;
- (void)setDietaryFiberValue:(double)value_;

//- (BOOL)validateDietaryFiber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* insolubleFiber;

@property (atomic) double insolubleFiberValue;
- (double)insolubleFiberValue;
- (void)setInsolubleFiberValue:(double)value_;

//- (BOOL)validateInsolubleFiber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ironPercent;

@property (atomic) double ironPercentValue;
- (double)ironPercentValue;
- (void)setIronPercentValue:(double)value_;

//- (BOOL)validateIronPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* monounsaturatedFat;

@property (atomic) double monounsaturatedFatValue;
- (double)monounsaturatedFatValue;
- (void)setMonounsaturatedFatValue:(double)value_;

//- (BOOL)validateMonounsaturatedFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* nutritionId;

@property (atomic) int32_t nutritionIdValue;
- (int32_t)nutritionIdValue;
- (void)setNutritionIdValue:(int32_t)value_;

//- (BOOL)validateNutritionId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* otherCarbohydrates;

@property (atomic) double otherCarbohydratesValue;
- (double)otherCarbohydratesValue;
- (void)setOtherCarbohydratesValue:(double)value_;

//- (BOOL)validateOtherCarbohydrates:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* polyunsaturatedFat;

@property (atomic) double polyunsaturatedFatValue;
- (double)polyunsaturatedFatValue;
- (void)setPolyunsaturatedFatValue:(double)value_;

//- (BOOL)validatePolyunsaturatedFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* potassium;

@property (atomic) double potassiumValue;
- (double)potassiumValue;
- (void)setPotassiumValue:(double)value_;

//- (BOOL)validatePotassium:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* protein;

@property (atomic) double proteinValue;
- (double)proteinValue;
- (void)setProteinValue:(double)value_;

//- (BOOL)validateProtein:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* saturatedFat;

@property (atomic) double saturatedFatValue;
- (double)saturatedFatValue;
- (void)setSaturatedFatValue:(double)value_;

//- (BOOL)validateSaturatedFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* saturatedFatCalories;

@property (atomic) double saturatedFatCaloriesValue;
- (double)saturatedFatCaloriesValue;
- (void)setSaturatedFatCaloriesValue:(double)value_;

//- (BOOL)validateSaturatedFatCalories:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sodium;

@property (atomic) double sodiumValue;
- (double)sodiumValue;
- (void)setSodiumValue:(double)value_;

//- (BOOL)validateSodium:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* solubleFiber;

@property (atomic) double solubleFiberValue;
- (double)solubleFiberValue;
- (void)setSolubleFiberValue:(double)value_;

//- (BOOL)validateSolubleFiber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sugars;

@property (atomic) double sugarsValue;
- (double)sugarsValue;
- (void)setSugarsValue:(double)value_;

//- (BOOL)validateSugars:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sugarsAlcohol;

@property (atomic) double sugarsAlcoholValue;
- (double)sugarsAlcoholValue;
- (void)setSugarsAlcoholValue:(double)value_;

//- (BOOL)validateSugarsAlcohol:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* totalCarbohydrates;

@property (atomic) double totalCarbohydratesValue;
- (double)totalCarbohydratesValue;
- (void)setTotalCarbohydratesValue:(double)value_;

//- (BOOL)validateTotalCarbohydrates:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* totalFat;

@property (atomic) double totalFatValue;
- (double)totalFatValue;
- (void)setTotalFatValue:(double)value_;

//- (BOOL)validateTotalFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* transFat;

@property (atomic) double transFatValue;
- (double)transFatValue;
- (void)setTransFatValue:(double)value_;

//- (BOOL)validateTransFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* vitaminAPercent;

@property (atomic) double vitaminAPercentValue;
- (double)vitaminAPercentValue;
- (void)setVitaminAPercentValue:(double)value_;

//- (BOOL)validateVitaminAPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* vitaminCPercent;

@property (atomic) double vitaminCPercentValue;
- (double)vitaminCPercentValue;
- (void)setVitaminCPercentValue:(double)value_;

//- (BOOL)validateVitaminCPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CustomFoodMO *customFood;

//- (BOOL)validateCustomFood:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) FoodLogMO *foodLog;

//- (BOOL)validateFoodLog:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AdvisorMealPlanPhaseMO *maxPhase;

//- (BOOL)validateMaxPhase:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UserProfileMO *maxUserProfile;

//- (BOOL)validateMaxUserProfile:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MealPlannerDay *mealPlannerDay;

//- (BOOL)validateMealPlannerDay:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MealPredictionMO *mealPredictions;

//- (BOOL)validateMealPredictions:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AdvisorMealPlanPhaseMO *minPhase;

//- (BOOL)validateMinPhase:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UserProfileMO *minUserProfile;

//- (BOOL)validateMinUserProfile:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) RecipeMO *recipe;

//- (BOOL)validateRecipe:(id*)value_ error:(NSError**)error_;

@end

@interface _NutritionMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCalciumPercent;
- (void)setPrimitiveCalciumPercent:(NSNumber*)value;

- (double)primitiveCalciumPercentValue;
- (void)setPrimitiveCalciumPercentValue:(double)value_;

- (NSNumber*)primitiveCalories;
- (void)setPrimitiveCalories:(NSNumber*)value;

- (double)primitiveCaloriesValue;
- (void)setPrimitiveCaloriesValue:(double)value_;

- (NSNumber*)primitiveCaloriesFromFat;
- (void)setPrimitiveCaloriesFromFat:(NSNumber*)value;

- (double)primitiveCaloriesFromFatValue;
- (void)setPrimitiveCaloriesFromFatValue:(double)value_;

- (NSNumber*)primitiveCholesterol;
- (void)setPrimitiveCholesterol:(NSNumber*)value;

- (double)primitiveCholesterolValue;
- (void)setPrimitiveCholesterolValue:(double)value_;

- (NSNumber*)primitiveDietaryFiber;
- (void)setPrimitiveDietaryFiber:(NSNumber*)value;

- (double)primitiveDietaryFiberValue;
- (void)setPrimitiveDietaryFiberValue:(double)value_;

- (NSNumber*)primitiveInsolubleFiber;
- (void)setPrimitiveInsolubleFiber:(NSNumber*)value;

- (double)primitiveInsolubleFiberValue;
- (void)setPrimitiveInsolubleFiberValue:(double)value_;

- (NSNumber*)primitiveIronPercent;
- (void)setPrimitiveIronPercent:(NSNumber*)value;

- (double)primitiveIronPercentValue;
- (void)setPrimitiveIronPercentValue:(double)value_;

- (NSNumber*)primitiveMonounsaturatedFat;
- (void)setPrimitiveMonounsaturatedFat:(NSNumber*)value;

- (double)primitiveMonounsaturatedFatValue;
- (void)setPrimitiveMonounsaturatedFatValue:(double)value_;

- (NSNumber*)primitiveNutritionId;
- (void)setPrimitiveNutritionId:(NSNumber*)value;

- (int32_t)primitiveNutritionIdValue;
- (void)setPrimitiveNutritionIdValue:(int32_t)value_;

- (NSNumber*)primitiveOtherCarbohydrates;
- (void)setPrimitiveOtherCarbohydrates:(NSNumber*)value;

- (double)primitiveOtherCarbohydratesValue;
- (void)setPrimitiveOtherCarbohydratesValue:(double)value_;

- (NSNumber*)primitivePolyunsaturatedFat;
- (void)setPrimitivePolyunsaturatedFat:(NSNumber*)value;

- (double)primitivePolyunsaturatedFatValue;
- (void)setPrimitivePolyunsaturatedFatValue:(double)value_;

- (NSNumber*)primitivePotassium;
- (void)setPrimitivePotassium:(NSNumber*)value;

- (double)primitivePotassiumValue;
- (void)setPrimitivePotassiumValue:(double)value_;

- (NSNumber*)primitiveProtein;
- (void)setPrimitiveProtein:(NSNumber*)value;

- (double)primitiveProteinValue;
- (void)setPrimitiveProteinValue:(double)value_;

- (NSNumber*)primitiveSaturatedFat;
- (void)setPrimitiveSaturatedFat:(NSNumber*)value;

- (double)primitiveSaturatedFatValue;
- (void)setPrimitiveSaturatedFatValue:(double)value_;

- (NSNumber*)primitiveSaturatedFatCalories;
- (void)setPrimitiveSaturatedFatCalories:(NSNumber*)value;

- (double)primitiveSaturatedFatCaloriesValue;
- (void)setPrimitiveSaturatedFatCaloriesValue:(double)value_;

- (NSNumber*)primitiveSodium;
- (void)setPrimitiveSodium:(NSNumber*)value;

- (double)primitiveSodiumValue;
- (void)setPrimitiveSodiumValue:(double)value_;

- (NSNumber*)primitiveSolubleFiber;
- (void)setPrimitiveSolubleFiber:(NSNumber*)value;

- (double)primitiveSolubleFiberValue;
- (void)setPrimitiveSolubleFiberValue:(double)value_;

- (NSNumber*)primitiveSugars;
- (void)setPrimitiveSugars:(NSNumber*)value;

- (double)primitiveSugarsValue;
- (void)setPrimitiveSugarsValue:(double)value_;

- (NSNumber*)primitiveSugarsAlcohol;
- (void)setPrimitiveSugarsAlcohol:(NSNumber*)value;

- (double)primitiveSugarsAlcoholValue;
- (void)setPrimitiveSugarsAlcoholValue:(double)value_;

- (NSNumber*)primitiveTotalCarbohydrates;
- (void)setPrimitiveTotalCarbohydrates:(NSNumber*)value;

- (double)primitiveTotalCarbohydratesValue;
- (void)setPrimitiveTotalCarbohydratesValue:(double)value_;

- (NSNumber*)primitiveTotalFat;
- (void)setPrimitiveTotalFat:(NSNumber*)value;

- (double)primitiveTotalFatValue;
- (void)setPrimitiveTotalFatValue:(double)value_;

- (NSNumber*)primitiveTransFat;
- (void)setPrimitiveTransFat:(NSNumber*)value;

- (double)primitiveTransFatValue;
- (void)setPrimitiveTransFatValue:(double)value_;

- (NSNumber*)primitiveVitaminAPercent;
- (void)setPrimitiveVitaminAPercent:(NSNumber*)value;

- (double)primitiveVitaminAPercentValue;
- (void)setPrimitiveVitaminAPercentValue:(double)value_;

- (NSNumber*)primitiveVitaminCPercent;
- (void)setPrimitiveVitaminCPercent:(NSNumber*)value;

- (double)primitiveVitaminCPercentValue;
- (void)setPrimitiveVitaminCPercentValue:(double)value_;

- (CustomFoodMO*)primitiveCustomFood;
- (void)setPrimitiveCustomFood:(CustomFoodMO*)value;

- (FoodLogMO*)primitiveFoodLog;
- (void)setPrimitiveFoodLog:(FoodLogMO*)value;

- (AdvisorMealPlanPhaseMO*)primitiveMaxPhase;
- (void)setPrimitiveMaxPhase:(AdvisorMealPlanPhaseMO*)value;

- (UserProfileMO*)primitiveMaxUserProfile;
- (void)setPrimitiveMaxUserProfile:(UserProfileMO*)value;

- (MealPlannerDay*)primitiveMealPlannerDay;
- (void)setPrimitiveMealPlannerDay:(MealPlannerDay*)value;

- (MealPredictionMO*)primitiveMealPredictions;
- (void)setPrimitiveMealPredictions:(MealPredictionMO*)value;

- (AdvisorMealPlanPhaseMO*)primitiveMinPhase;
- (void)setPrimitiveMinPhase:(AdvisorMealPlanPhaseMO*)value;

- (UserProfileMO*)primitiveMinUserProfile;
- (void)setPrimitiveMinUserProfile:(UserProfileMO*)value;

- (RecipeMO*)primitiveRecipe;
- (void)setPrimitiveRecipe:(RecipeMO*)value;

@end
