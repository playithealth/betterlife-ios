// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealPlannerDay.m instead.

#import "_MealPlannerDay.h"

const struct MealPlannerDayAttributes MealPlannerDayAttributes = {
	.accountId = @"accountId",
	.cloudId = @"cloudId",
	.date = @"date",
	.imageId = @"imageId",
	.itemType = @"itemType",
	.logTime = @"logTime",
	.menuId = @"menuId",
	.name = @"name",
	.nutritionId = @"nutritionId",
	.productId = @"productId",
	.recipeId = @"recipeId",
	.servingSize = @"servingSize",
	.servingSizeUnit = @"servingSizeUnit",
	.servingSizeUnitAbbreviation = @"servingSizeUnitAbbreviation",
	.servings = @"servings",
	.sortOrder = @"sortOrder",
	.status = @"status",
	.tagId = @"tagId",
	.updatedOn = @"updatedOn",
};

const struct MealPlannerDayRelationships MealPlannerDayRelationships = {
	.nutrition = @"nutrition",
	.recipe = @"recipe",
	.shoppingListItems = @"shoppingListItems",
	.tag = @"tag",
};

@implementation MealPlannerDayID
@end

@implementation _MealPlannerDay

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MealPlannerDay" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MealPlannerDay";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MealPlannerDay" inManagedObjectContext:moc_];
}

- (MealPlannerDayID*)objectID {
	return (MealPlannerDayID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"imageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"imageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"logTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"logTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"menuIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"menuId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"nutritionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"nutritionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"productIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"productId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"recipeIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"recipeId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"servingsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"servings"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"tagIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"tagId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic date;

@dynamic imageId;

- (int32_t)imageIdValue {
	NSNumber *result = [self imageId];
	return [result intValue];
}

- (void)setImageIdValue:(int32_t)value_ {
	[self setImageId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImageIdValue {
	NSNumber *result = [self primitiveImageId];
	return [result intValue];
}

- (void)setPrimitiveImageIdValue:(int32_t)value_ {
	[self setPrimitiveImageId:[NSNumber numberWithInt:value_]];
}

@dynamic itemType;

@dynamic logTime;

- (int16_t)logTimeValue {
	NSNumber *result = [self logTime];
	return [result shortValue];
}

- (void)setLogTimeValue:(int16_t)value_ {
	[self setLogTime:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLogTimeValue {
	NSNumber *result = [self primitiveLogTime];
	return [result shortValue];
}

- (void)setPrimitiveLogTimeValue:(int16_t)value_ {
	[self setPrimitiveLogTime:[NSNumber numberWithShort:value_]];
}

@dynamic menuId;

- (int32_t)menuIdValue {
	NSNumber *result = [self menuId];
	return [result intValue];
}

- (void)setMenuIdValue:(int32_t)value_ {
	[self setMenuId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMenuIdValue {
	NSNumber *result = [self primitiveMenuId];
	return [result intValue];
}

- (void)setPrimitiveMenuIdValue:(int32_t)value_ {
	[self setPrimitiveMenuId:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic nutritionId;

- (int32_t)nutritionIdValue {
	NSNumber *result = [self nutritionId];
	return [result intValue];
}

- (void)setNutritionIdValue:(int32_t)value_ {
	[self setNutritionId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveNutritionIdValue {
	NSNumber *result = [self primitiveNutritionId];
	return [result intValue];
}

- (void)setPrimitiveNutritionIdValue:(int32_t)value_ {
	[self setPrimitiveNutritionId:[NSNumber numberWithInt:value_]];
}

@dynamic productId;

- (int32_t)productIdValue {
	NSNumber *result = [self productId];
	return [result intValue];
}

- (void)setProductIdValue:(int32_t)value_ {
	[self setProductId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveProductIdValue {
	NSNumber *result = [self primitiveProductId];
	return [result intValue];
}

- (void)setPrimitiveProductIdValue:(int32_t)value_ {
	[self setPrimitiveProductId:[NSNumber numberWithInt:value_]];
}

@dynamic recipeId;

- (int32_t)recipeIdValue {
	NSNumber *result = [self recipeId];
	return [result intValue];
}

- (void)setRecipeIdValue:(int32_t)value_ {
	[self setRecipeId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRecipeIdValue {
	NSNumber *result = [self primitiveRecipeId];
	return [result intValue];
}

- (void)setPrimitiveRecipeIdValue:(int32_t)value_ {
	[self setPrimitiveRecipeId:[NSNumber numberWithInt:value_]];
}

@dynamic servingSize;

@dynamic servingSizeUnit;

@dynamic servingSizeUnitAbbreviation;

@dynamic servings;

- (double)servingsValue {
	NSNumber *result = [self servings];
	return [result doubleValue];
}

- (void)setServingsValue:(double)value_ {
	[self setServings:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveServingsValue {
	NSNumber *result = [self primitiveServings];
	return [result doubleValue];
}

- (void)setPrimitiveServingsValue:(double)value_ {
	[self setPrimitiveServings:[NSNumber numberWithDouble:value_]];
}

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic status;

@dynamic tagId;

- (int32_t)tagIdValue {
	NSNumber *result = [self tagId];
	return [result intValue];
}

- (void)setTagIdValue:(int32_t)value_ {
	[self setTagId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveTagIdValue {
	NSNumber *result = [self primitiveTagId];
	return [result intValue];
}

- (void)setPrimitiveTagIdValue:(int32_t)value_ {
	[self setPrimitiveTagId:[NSNumber numberWithInt:value_]];
}

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic nutrition;

@dynamic recipe;

@dynamic shoppingListItems;

- (NSMutableSet*)shoppingListItemsSet {
	[self willAccessValueForKey:@"shoppingListItems"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"shoppingListItems"];

	[self didAccessValueForKey:@"shoppingListItems"];
	return result;
}

@dynamic tag;

@end

