// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrackingMO.h instead.

#import <CoreData/CoreData.h>

extern const struct TrackingMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *source;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *trackingType;
	__unsafe_unretained NSString *updatedOn;
	__unsafe_unretained NSString *valueJSON;
} TrackingMOAttributes;

extern const struct TrackingMORelationships {
	__unsafe_unretained NSString *trainingAssessment;
} TrackingMORelationships;

@class TrainingAssessmentMO;

@interface TrackingMOID : NSManagedObjectID {}
@end

@interface _TrackingMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TrackingMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* source;

@property (atomic) int32_t sourceValue;
- (int32_t)sourceValue;
- (void)setSourceValue:(int32_t)value_;

//- (BOOL)validateSource:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* trackingType;

@property (atomic) int16_t trackingTypeValue;
- (int16_t)trackingTypeValue;
- (void)setTrackingTypeValue:(int16_t)value_;

//- (BOOL)validateTrackingType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* valueJSON;

//- (BOOL)validateValueJSON:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TrainingAssessmentMO *trainingAssessment;

//- (BOOL)validateTrainingAssessment:(id*)value_ error:(NSError**)error_;

@end

@interface _TrackingMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSNumber*)primitiveSource;
- (void)setPrimitiveSource:(NSNumber*)value;

- (int32_t)primitiveSourceValue;
- (void)setPrimitiveSourceValue:(int32_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveTrackingType;
- (void)setPrimitiveTrackingType:(NSNumber*)value;

- (int16_t)primitiveTrackingTypeValue;
- (void)setPrimitiveTrackingTypeValue:(int16_t)value_;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSString*)primitiveValueJSON;
- (void)setPrimitiveValueJSON:(NSString*)value;

- (TrainingAssessmentMO*)primitiveTrainingAssessment;
- (void)setPrimitiveTrainingAssessment:(TrainingAssessmentMO*)value;

@end
