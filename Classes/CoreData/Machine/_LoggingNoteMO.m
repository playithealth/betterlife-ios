// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LoggingNoteMO.m instead.

#import "_LoggingNoteMO.h"

const struct LoggingNoteMOAttributes LoggingNoteMOAttributes = {
	.cloudId = @"cloudId",
	.date = @"date",
	.loginId = @"loginId",
	.note = @"note",
	.noteType = @"noteType",
	.source = @"source",
	.status = @"status",
	.updatedOn = @"updatedOn",
};

@implementation LoggingNoteMOID
@end

@implementation _LoggingNoteMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LoggingNote" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LoggingNote";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LoggingNote" inManagedObjectContext:moc_];
}

- (LoggingNoteMOID*)objectID {
	return (LoggingNoteMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sourceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"source"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic date;

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic note;

@dynamic noteType;

@dynamic source;

- (int16_t)sourceValue {
	NSNumber *result = [self source];
	return [result shortValue];
}

- (void)setSourceValue:(int16_t)value_ {
	[self setSource:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSourceValue {
	NSNumber *result = [self primitiveSource];
	return [result shortValue];
}

- (void)setPrimitiveSourceValue:(int16_t)value_ {
	[self setPrimitiveSource:[NSNumber numberWithShort:value_]];
}

@dynamic status;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@end

