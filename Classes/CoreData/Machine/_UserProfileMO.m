// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserProfileMO.m instead.

#import "_UserProfileMO.h"

const struct UserProfileMOAttributes UserProfileMOAttributes = {
	.accountId = @"accountId",
	.activityLevel = @"activityLevel",
	.birthday = @"birthday",
	.currentWeight = @"currentWeight",
	.customNutritionTimeframe = @"customNutritionTimeframe",
	.customTrackingTimeframe = @"customTrackingTimeframe",
	.email = @"email",
	.gender = @"gender",
	.goalWeight = @"goalWeight",
	.hasMyZone = @"hasMyZone",
	.height = @"height",
	.imageId = @"imageId",
	.isPaidCustomer = @"isPaidCustomer",
	.loginId = @"loginId",
	.name = @"name",
	.nutritionEffort = @"nutritionEffort",
	.onboardingComplete = @"onboardingComplete",
	.proteinRatio = @"proteinRatio",
	.skin = @"skin",
	.status = @"status",
	.syncToken = @"syncToken",
	.totalCarbohydrateRatio = @"totalCarbohydrateRatio",
	.totalFatRatio = @"totalFatRatio",
	.useAutoRatios = @"useAutoRatios",
	.useBMR = @"useBMR",
	.zipCode = @"zipCode",
};

const struct UserProfileMORelationships UserProfileMORelationships = {
	.alwaysShowNutrients = @"alwaysShowNutrients",
	.communities = @"communities",
	.maxNutrition = @"maxNutrition",
	.minNutrition = @"minNutrition",
	.permissions = @"permissions",
	.threadMessages = @"threadMessages",
	.threads = @"threads",
};

@implementation UserProfileMOID
@end

@implementation _UserProfileMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserProfile" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserProfile";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserProfile" inManagedObjectContext:moc_];
}

- (UserProfileMOID*)objectID {
	return (UserProfileMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"activityLevelValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"activityLevel"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"currentWeightValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"currentWeight"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"customNutritionTimeframeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"customNutritionTimeframe"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"customTrackingTimeframeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"customTrackingTimeframe"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"genderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gender"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"goalWeightValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"goalWeight"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"hasMyZoneValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"hasMyZone"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"heightValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"height"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"imageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"imageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isPaidCustomerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isPaidCustomer"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"nutritionEffortValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"nutritionEffort"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"onboardingCompleteValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"onboardingComplete"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"proteinRatioValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"proteinRatio"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"syncTokenValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"syncToken"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalCarbohydrateRatioValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalCarbohydrateRatio"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalFatRatioValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalFatRatio"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"useAutoRatiosValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"useAutoRatios"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"useBMRValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"useBMR"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic activityLevel;

- (double)activityLevelValue {
	NSNumber *result = [self activityLevel];
	return [result doubleValue];
}

- (void)setActivityLevelValue:(double)value_ {
	[self setActivityLevel:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveActivityLevelValue {
	NSNumber *result = [self primitiveActivityLevel];
	return [result doubleValue];
}

- (void)setPrimitiveActivityLevelValue:(double)value_ {
	[self setPrimitiveActivityLevel:[NSNumber numberWithDouble:value_]];
}

@dynamic birthday;

@dynamic currentWeight;

- (double)currentWeightValue {
	NSNumber *result = [self currentWeight];
	return [result doubleValue];
}

- (void)setCurrentWeightValue:(double)value_ {
	[self setCurrentWeight:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCurrentWeightValue {
	NSNumber *result = [self primitiveCurrentWeight];
	return [result doubleValue];
}

- (void)setPrimitiveCurrentWeightValue:(double)value_ {
	[self setPrimitiveCurrentWeight:[NSNumber numberWithDouble:value_]];
}

@dynamic customNutritionTimeframe;

- (int16_t)customNutritionTimeframeValue {
	NSNumber *result = [self customNutritionTimeframe];
	return [result shortValue];
}

- (void)setCustomNutritionTimeframeValue:(int16_t)value_ {
	[self setCustomNutritionTimeframe:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCustomNutritionTimeframeValue {
	NSNumber *result = [self primitiveCustomNutritionTimeframe];
	return [result shortValue];
}

- (void)setPrimitiveCustomNutritionTimeframeValue:(int16_t)value_ {
	[self setPrimitiveCustomNutritionTimeframe:[NSNumber numberWithShort:value_]];
}

@dynamic customTrackingTimeframe;

- (int16_t)customTrackingTimeframeValue {
	NSNumber *result = [self customTrackingTimeframe];
	return [result shortValue];
}

- (void)setCustomTrackingTimeframeValue:(int16_t)value_ {
	[self setCustomTrackingTimeframe:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCustomTrackingTimeframeValue {
	NSNumber *result = [self primitiveCustomTrackingTimeframe];
	return [result shortValue];
}

- (void)setPrimitiveCustomTrackingTimeframeValue:(int16_t)value_ {
	[self setPrimitiveCustomTrackingTimeframe:[NSNumber numberWithShort:value_]];
}

@dynamic email;

@dynamic gender;

- (int16_t)genderValue {
	NSNumber *result = [self gender];
	return [result shortValue];
}

- (void)setGenderValue:(int16_t)value_ {
	[self setGender:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveGenderValue {
	NSNumber *result = [self primitiveGender];
	return [result shortValue];
}

- (void)setPrimitiveGenderValue:(int16_t)value_ {
	[self setPrimitiveGender:[NSNumber numberWithShort:value_]];
}

@dynamic goalWeight;

- (int16_t)goalWeightValue {
	NSNumber *result = [self goalWeight];
	return [result shortValue];
}

- (void)setGoalWeightValue:(int16_t)value_ {
	[self setGoalWeight:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveGoalWeightValue {
	NSNumber *result = [self primitiveGoalWeight];
	return [result shortValue];
}

- (void)setPrimitiveGoalWeightValue:(int16_t)value_ {
	[self setPrimitiveGoalWeight:[NSNumber numberWithShort:value_]];
}

@dynamic hasMyZone;

- (BOOL)hasMyZoneValue {
	NSNumber *result = [self hasMyZone];
	return [result boolValue];
}

- (void)setHasMyZoneValue:(BOOL)value_ {
	[self setHasMyZone:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveHasMyZoneValue {
	NSNumber *result = [self primitiveHasMyZone];
	return [result boolValue];
}

- (void)setPrimitiveHasMyZoneValue:(BOOL)value_ {
	[self setPrimitiveHasMyZone:[NSNumber numberWithBool:value_]];
}

@dynamic height;

- (int16_t)heightValue {
	NSNumber *result = [self height];
	return [result shortValue];
}

- (void)setHeightValue:(int16_t)value_ {
	[self setHeight:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveHeightValue {
	NSNumber *result = [self primitiveHeight];
	return [result shortValue];
}

- (void)setPrimitiveHeightValue:(int16_t)value_ {
	[self setPrimitiveHeight:[NSNumber numberWithShort:value_]];
}

@dynamic imageId;

- (int32_t)imageIdValue {
	NSNumber *result = [self imageId];
	return [result intValue];
}

- (void)setImageIdValue:(int32_t)value_ {
	[self setImageId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImageIdValue {
	NSNumber *result = [self primitiveImageId];
	return [result intValue];
}

- (void)setPrimitiveImageIdValue:(int32_t)value_ {
	[self setPrimitiveImageId:[NSNumber numberWithInt:value_]];
}

@dynamic isPaidCustomer;

- (BOOL)isPaidCustomerValue {
	NSNumber *result = [self isPaidCustomer];
	return [result boolValue];
}

- (void)setIsPaidCustomerValue:(BOOL)value_ {
	[self setIsPaidCustomer:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsPaidCustomerValue {
	NSNumber *result = [self primitiveIsPaidCustomer];
	return [result boolValue];
}

- (void)setPrimitiveIsPaidCustomerValue:(BOOL)value_ {
	[self setPrimitiveIsPaidCustomer:[NSNumber numberWithBool:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic nutritionEffort;

- (int16_t)nutritionEffortValue {
	NSNumber *result = [self nutritionEffort];
	return [result shortValue];
}

- (void)setNutritionEffortValue:(int16_t)value_ {
	[self setNutritionEffort:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveNutritionEffortValue {
	NSNumber *result = [self primitiveNutritionEffort];
	return [result shortValue];
}

- (void)setPrimitiveNutritionEffortValue:(int16_t)value_ {
	[self setPrimitiveNutritionEffort:[NSNumber numberWithShort:value_]];
}

@dynamic onboardingComplete;

- (BOOL)onboardingCompleteValue {
	NSNumber *result = [self onboardingComplete];
	return [result boolValue];
}

- (void)setOnboardingCompleteValue:(BOOL)value_ {
	[self setOnboardingComplete:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveOnboardingCompleteValue {
	NSNumber *result = [self primitiveOnboardingComplete];
	return [result boolValue];
}

- (void)setPrimitiveOnboardingCompleteValue:(BOOL)value_ {
	[self setPrimitiveOnboardingComplete:[NSNumber numberWithBool:value_]];
}

@dynamic proteinRatio;

- (int16_t)proteinRatioValue {
	NSNumber *result = [self proteinRatio];
	return [result shortValue];
}

- (void)setProteinRatioValue:(int16_t)value_ {
	[self setProteinRatio:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveProteinRatioValue {
	NSNumber *result = [self primitiveProteinRatio];
	return [result shortValue];
}

- (void)setPrimitiveProteinRatioValue:(int16_t)value_ {
	[self setPrimitiveProteinRatio:[NSNumber numberWithShort:value_]];
}

@dynamic skin;

@dynamic status;

@dynamic syncToken;

- (int32_t)syncTokenValue {
	NSNumber *result = [self syncToken];
	return [result intValue];
}

- (void)setSyncTokenValue:(int32_t)value_ {
	[self setSyncToken:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSyncTokenValue {
	NSNumber *result = [self primitiveSyncToken];
	return [result intValue];
}

- (void)setPrimitiveSyncTokenValue:(int32_t)value_ {
	[self setPrimitiveSyncToken:[NSNumber numberWithInt:value_]];
}

@dynamic totalCarbohydrateRatio;

- (int16_t)totalCarbohydrateRatioValue {
	NSNumber *result = [self totalCarbohydrateRatio];
	return [result shortValue];
}

- (void)setTotalCarbohydrateRatioValue:(int16_t)value_ {
	[self setTotalCarbohydrateRatio:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveTotalCarbohydrateRatioValue {
	NSNumber *result = [self primitiveTotalCarbohydrateRatio];
	return [result shortValue];
}

- (void)setPrimitiveTotalCarbohydrateRatioValue:(int16_t)value_ {
	[self setPrimitiveTotalCarbohydrateRatio:[NSNumber numberWithShort:value_]];
}

@dynamic totalFatRatio;

- (int16_t)totalFatRatioValue {
	NSNumber *result = [self totalFatRatio];
	return [result shortValue];
}

- (void)setTotalFatRatioValue:(int16_t)value_ {
	[self setTotalFatRatio:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveTotalFatRatioValue {
	NSNumber *result = [self primitiveTotalFatRatio];
	return [result shortValue];
}

- (void)setPrimitiveTotalFatRatioValue:(int16_t)value_ {
	[self setPrimitiveTotalFatRatio:[NSNumber numberWithShort:value_]];
}

@dynamic useAutoRatios;

- (BOOL)useAutoRatiosValue {
	NSNumber *result = [self useAutoRatios];
	return [result boolValue];
}

- (void)setUseAutoRatiosValue:(BOOL)value_ {
	[self setUseAutoRatios:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUseAutoRatiosValue {
	NSNumber *result = [self primitiveUseAutoRatios];
	return [result boolValue];
}

- (void)setPrimitiveUseAutoRatiosValue:(BOOL)value_ {
	[self setPrimitiveUseAutoRatios:[NSNumber numberWithBool:value_]];
}

@dynamic useBMR;

- (BOOL)useBMRValue {
	NSNumber *result = [self useBMR];
	return [result boolValue];
}

- (void)setUseBMRValue:(BOOL)value_ {
	[self setUseBMR:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUseBMRValue {
	NSNumber *result = [self primitiveUseBMR];
	return [result boolValue];
}

- (void)setPrimitiveUseBMRValue:(BOOL)value_ {
	[self setPrimitiveUseBMR:[NSNumber numberWithBool:value_]];
}

@dynamic zipCode;

@dynamic alwaysShowNutrients;

- (NSMutableSet*)alwaysShowNutrientsSet {
	[self willAccessValueForKey:@"alwaysShowNutrients"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"alwaysShowNutrients"];

	[self didAccessValueForKey:@"alwaysShowNutrients"];
	return result;
}

@dynamic communities;

- (NSMutableSet*)communitiesSet {
	[self willAccessValueForKey:@"communities"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"communities"];

	[self didAccessValueForKey:@"communities"];
	return result;
}

@dynamic maxNutrition;

@dynamic minNutrition;

@dynamic permissions;

- (NSMutableSet*)permissionsSet {
	[self willAccessValueForKey:@"permissions"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"permissions"];

	[self didAccessValueForKey:@"permissions"];
	return result;
}

@dynamic threadMessages;

- (NSMutableSet*)threadMessagesSet {
	[self willAccessValueForKey:@"threadMessages"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"threadMessages"];

	[self didAccessValueForKey:@"threadMessages"];
	return result;
}

@dynamic threads;

- (NSMutableSet*)threadsSet {
	[self willAccessValueForKey:@"threads"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"threads"];

	[self didAccessValueForKey:@"threads"];
	return result;
}

@end

