// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LoggingNoteMO.h instead.

#import <CoreData/CoreData.h>

extern const struct LoggingNoteMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *note;
	__unsafe_unretained NSString *noteType;
	__unsafe_unretained NSString *source;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
} LoggingNoteMOAttributes;

@interface LoggingNoteMOID : NSManagedObjectID {}
@end

@interface _LoggingNoteMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LoggingNoteMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* note;

//- (BOOL)validateNote:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* noteType;

//- (BOOL)validateNoteType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* source;

@property (atomic) int16_t sourceValue;
- (int16_t)sourceValue;
- (void)setSourceValue:(int16_t)value_;

//- (BOOL)validateSource:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@end

@interface _LoggingNoteMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveNote;
- (void)setPrimitiveNote:(NSString*)value;

- (NSString*)primitiveNoteType;
- (void)setPrimitiveNoteType:(NSString*)value;

- (NSNumber*)primitiveSource;
- (void)setPrimitiveSource:(NSNumber*)value;

- (int16_t)primitiveSourceValue;
- (void)setPrimitiveSourceValue:(int16_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

@end
