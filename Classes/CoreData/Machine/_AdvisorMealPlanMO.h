// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanMO.h instead.

#import <CoreData/CoreData.h>
#import "AdvisorPlanMO.h"

extern const struct AdvisorMealPlanMOAttributes {
	__unsafe_unretained NSString *trackFruitsVeggies;
	__unsafe_unretained NSString *trackSugaryDrinks;
} AdvisorMealPlanMOAttributes;

@interface AdvisorMealPlanMOID : AdvisorPlanMOID {}
@end

@interface _AdvisorMealPlanMO : AdvisorPlanMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorMealPlanMOID* objectID;

@property (nonatomic, strong) NSNumber* trackFruitsVeggies;

@property (atomic) BOOL trackFruitsVeggiesValue;
- (BOOL)trackFruitsVeggiesValue;
- (void)setTrackFruitsVeggiesValue:(BOOL)value_;

//- (BOOL)validateTrackFruitsVeggies:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* trackSugaryDrinks;

@property (atomic) BOOL trackSugaryDrinksValue;
- (BOOL)trackSugaryDrinksValue;
- (void)setTrackSugaryDrinksValue:(BOOL)value_;

//- (BOOL)validateTrackSugaryDrinks:(id*)value_ error:(NSError**)error_;

@end

@interface _AdvisorMealPlanMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveTrackFruitsVeggies;
- (void)setPrimitiveTrackFruitsVeggies:(NSNumber*)value;

- (BOOL)primitiveTrackFruitsVeggiesValue;
- (void)setPrimitiveTrackFruitsVeggiesValue:(BOOL)value_;

- (NSNumber*)primitiveTrackSugaryDrinks;
- (void)setPrimitiveTrackSugaryDrinks:(NSNumber*)value;

- (BOOL)primitiveTrackSugaryDrinksValue;
- (void)setPrimitiveTrackSugaryDrinksValue:(BOOL)value_;

@end
