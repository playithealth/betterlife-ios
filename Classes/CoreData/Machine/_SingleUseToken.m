// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SingleUseToken.m instead.

#import "_SingleUseToken.h"

const struct SingleUseTokenAttributes SingleUseTokenAttributes = {
	.accountId = @"accountId",
	.timeViewed = @"timeViewed",
	.token = @"token",
};

@implementation SingleUseTokenID
@end

@implementation _SingleUseToken

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SingleUseToken" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SingleUseToken";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SingleUseToken" inManagedObjectContext:moc_];
}

- (SingleUseTokenID*)objectID {
	return (SingleUseTokenID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"timeViewedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"timeViewed"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic timeViewed;

- (int32_t)timeViewedValue {
	NSNumber *result = [self timeViewed];
	return [result intValue];
}

- (void)setTimeViewedValue:(int32_t)value_ {
	[self setTimeViewed:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveTimeViewedValue {
	NSNumber *result = [self primitiveTimeViewed];
	return [result intValue];
}

- (void)setPrimitiveTimeViewedValue:(int32_t)value_ {
	[self setPrimitiveTimeViewed:[NSNumber numberWithInt:value_]];
}

@dynamic token;

@end

