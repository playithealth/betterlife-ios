// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserStoreMO.m instead.

#import "_UserStoreMO.h"

const struct UserStoreMOAttributes UserStoreMOAttributes = {
	.accountId = @"accountId",
	.address = @"address",
	.address2 = @"address2",
	.autoSort = @"autoSort",
	.buyerCompassClient = @"buyerCompassClient",
	.city = @"city",
	.cloudId = @"cloudId",
	.latitude = @"latitude",
	.longitude = @"longitude",
	.name = @"name",
	.phone = @"phone",
	.state = @"state",
	.status = @"status",
	.storeCategoryStatus = @"storeCategoryStatus",
	.storeEntityId = @"storeEntityId",
	.updatedOn = @"updatedOn",
	.zip = @"zip",
};

const struct UserStoreMORelationships UserStoreMORelationships = {
	.categories = @"categories",
};

@implementation UserStoreMOID
@end

@implementation _UserStoreMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserStore" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserStore";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserStore" inManagedObjectContext:moc_];
}

- (UserStoreMOID*)objectID {
	return (UserStoreMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"autoSortValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"autoSort"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"buyerCompassClientValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"buyerCompassClient"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"latitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"latitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"storeEntityIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"storeEntityId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic address;

@dynamic address2;

@dynamic autoSort;

- (BOOL)autoSortValue {
	NSNumber *result = [self autoSort];
	return [result boolValue];
}

- (void)setAutoSortValue:(BOOL)value_ {
	[self setAutoSort:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAutoSortValue {
	NSNumber *result = [self primitiveAutoSort];
	return [result boolValue];
}

- (void)setPrimitiveAutoSortValue:(BOOL)value_ {
	[self setPrimitiveAutoSort:[NSNumber numberWithBool:value_]];
}

@dynamic buyerCompassClient;

- (BOOL)buyerCompassClientValue {
	NSNumber *result = [self buyerCompassClient];
	return [result boolValue];
}

- (void)setBuyerCompassClientValue:(BOOL)value_ {
	[self setBuyerCompassClient:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveBuyerCompassClientValue {
	NSNumber *result = [self primitiveBuyerCompassClient];
	return [result boolValue];
}

- (void)setPrimitiveBuyerCompassClientValue:(BOOL)value_ {
	[self setPrimitiveBuyerCompassClient:[NSNumber numberWithBool:value_]];
}

@dynamic city;

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic latitude;

- (float)latitudeValue {
	NSNumber *result = [self latitude];
	return [result floatValue];
}

- (void)setLatitudeValue:(float)value_ {
	[self setLatitude:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveLatitudeValue {
	NSNumber *result = [self primitiveLatitude];
	return [result floatValue];
}

- (void)setPrimitiveLatitudeValue:(float)value_ {
	[self setPrimitiveLatitude:[NSNumber numberWithFloat:value_]];
}

@dynamic longitude;

- (float)longitudeValue {
	NSNumber *result = [self longitude];
	return [result floatValue];
}

- (void)setLongitudeValue:(float)value_ {
	[self setLongitude:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveLongitudeValue {
	NSNumber *result = [self primitiveLongitude];
	return [result floatValue];
}

- (void)setPrimitiveLongitudeValue:(float)value_ {
	[self setPrimitiveLongitude:[NSNumber numberWithFloat:value_]];
}

@dynamic name;

@dynamic phone;

@dynamic state;

@dynamic status;

@dynamic storeCategoryStatus;

@dynamic storeEntityId;

- (int32_t)storeEntityIdValue {
	NSNumber *result = [self storeEntityId];
	return [result intValue];
}

- (void)setStoreEntityIdValue:(int32_t)value_ {
	[self setStoreEntityId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStoreEntityIdValue {
	NSNumber *result = [self primitiveStoreEntityId];
	return [result intValue];
}

- (void)setPrimitiveStoreEntityIdValue:(int32_t)value_ {
	[self setPrimitiveStoreEntityId:[NSNumber numberWithInt:value_]];
}

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic zip;

@dynamic categories;

- (NSMutableSet*)categoriesSet {
	[self willAccessValueForKey:@"categories"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"categories"];

	[self didAccessValueForKey:@"categories"];
	return result;
}

@end

