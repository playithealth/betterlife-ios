// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to InboxMessage.h instead.

#import <CoreData/CoreData.h>

extern const struct InboxMessageAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *createdOn;
	__unsafe_unretained NSString *deliveryId;
	__unsafe_unretained NSString *isPrivate;
	__unsafe_unretained NSString *isRead;
	__unsafe_unretained NSString *isRemoved;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *sender;
	__unsafe_unretained NSString *senderId;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *subject;
	__unsafe_unretained NSString *summary;
	__unsafe_unretained NSString *text;
	__unsafe_unretained NSString *updatedOn;
} InboxMessageAttributes;

extern const struct InboxMessageRelationships {
	__unsafe_unretained NSString *replies;
	__unsafe_unretained NSString *replyTo;
} InboxMessageRelationships;

@class InboxMessage;
@class InboxMessage;

@interface InboxMessageID : NSManagedObjectID {}
@end

@interface _InboxMessage : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) InboxMessageID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* createdOn;

@property (atomic) int32_t createdOnValue;
- (int32_t)createdOnValue;
- (void)setCreatedOnValue:(int32_t)value_;

//- (BOOL)validateCreatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* deliveryId;

@property (atomic) int32_t deliveryIdValue;
- (int32_t)deliveryIdValue;
- (void)setDeliveryIdValue:(int32_t)value_;

//- (BOOL)validateDeliveryId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isPrivate;

@property (atomic) BOOL isPrivateValue;
- (BOOL)isPrivateValue;
- (void)setIsPrivateValue:(BOOL)value_;

//- (BOOL)validateIsPrivate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRead;

@property (atomic) BOOL isReadValue;
- (BOOL)isReadValue;
- (void)setIsReadValue:(BOOL)value_;

//- (BOOL)validateIsRead:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRemoved;

@property (atomic) BOOL isRemovedValue;
- (BOOL)isRemovedValue;
- (void)setIsRemovedValue:(BOOL)value_;

//- (BOOL)validateIsRemoved:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sender;

//- (BOOL)validateSender:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* senderId;

@property (atomic) int32_t senderIdValue;
- (int32_t)senderIdValue;
- (void)setSenderIdValue:(int32_t)value_;

//- (BOOL)validateSenderId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* subject;

//- (BOOL)validateSubject:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* summary;

//- (BOOL)validateSummary:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* text;

//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *replies;

- (NSMutableSet*)repliesSet;

@property (nonatomic, strong) InboxMessage *replyTo;

//- (BOOL)validateReplyTo:(id*)value_ error:(NSError**)error_;

@end

@interface _InboxMessage (RepliesCoreDataGeneratedAccessors)
- (void)addReplies:(NSSet*)value_;
- (void)removeReplies:(NSSet*)value_;
- (void)addRepliesObject:(InboxMessage*)value_;
- (void)removeRepliesObject:(InboxMessage*)value_;

@end

@interface _InboxMessage (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveCreatedOn;
- (void)setPrimitiveCreatedOn:(NSNumber*)value;

- (int32_t)primitiveCreatedOnValue;
- (void)setPrimitiveCreatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveDeliveryId;
- (void)setPrimitiveDeliveryId:(NSNumber*)value;

- (int32_t)primitiveDeliveryIdValue;
- (void)setPrimitiveDeliveryIdValue:(int32_t)value_;

- (NSNumber*)primitiveIsPrivate;
- (void)setPrimitiveIsPrivate:(NSNumber*)value;

- (BOOL)primitiveIsPrivateValue;
- (void)setPrimitiveIsPrivateValue:(BOOL)value_;

- (NSNumber*)primitiveIsRead;
- (void)setPrimitiveIsRead:(NSNumber*)value;

- (BOOL)primitiveIsReadValue;
- (void)setPrimitiveIsReadValue:(BOOL)value_;

- (NSNumber*)primitiveIsRemoved;
- (void)setPrimitiveIsRemoved:(NSNumber*)value;

- (BOOL)primitiveIsRemovedValue;
- (void)setPrimitiveIsRemovedValue:(BOOL)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveSender;
- (void)setPrimitiveSender:(NSString*)value;

- (NSNumber*)primitiveSenderId;
- (void)setPrimitiveSenderId:(NSNumber*)value;

- (int32_t)primitiveSenderIdValue;
- (void)setPrimitiveSenderIdValue:(int32_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSString*)primitiveSubject;
- (void)setPrimitiveSubject:(NSString*)value;

- (NSString*)primitiveSummary;
- (void)setPrimitiveSummary:(NSString*)value;

- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSMutableSet*)primitiveReplies;
- (void)setPrimitiveReplies:(NSMutableSet*)value;

- (InboxMessage*)primitiveReplyTo;
- (void)setPrimitiveReplyTo:(InboxMessage*)value;

@end
