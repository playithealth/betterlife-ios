// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to StoreCategory.m instead.

#import "_StoreCategory.h"

const struct StoreCategoryAttributes StoreCategoryAttributes = {
	.hidden = @"hidden",
	.sortOrder = @"sortOrder",
};

const struct StoreCategoryRelationships StoreCategoryRelationships = {
	.category = @"category",
	.shoppingListItems = @"shoppingListItems",
	.store = @"store",
};

@implementation StoreCategoryID
@end

@implementation _StoreCategory

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"StoreCategory" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"StoreCategory";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"StoreCategory" inManagedObjectContext:moc_];
}

- (StoreCategoryID*)objectID {
	return (StoreCategoryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"hiddenValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"hidden"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic hidden;

- (BOOL)hiddenValue {
	NSNumber *result = [self hidden];
	return [result boolValue];
}

- (void)setHiddenValue:(BOOL)value_ {
	[self setHidden:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveHiddenValue {
	NSNumber *result = [self primitiveHidden];
	return [result boolValue];
}

- (void)setPrimitiveHiddenValue:(BOOL)value_ {
	[self setPrimitiveHidden:[NSNumber numberWithBool:value_]];
}

@dynamic sortOrder;

- (int32_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result intValue];
}

- (void)setSortOrderValue:(int32_t)value_ {
	[self setSortOrder:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result intValue];
}

- (void)setPrimitiveSortOrderValue:(int32_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithInt:value_]];
}

@dynamic category;

@dynamic shoppingListItems;

- (NSMutableSet*)shoppingListItemsSet {
	[self willAccessValueForKey:@"shoppingListItems"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"shoppingListItems"];

	[self didAccessValueForKey:@"shoppingListItems"];
	return result;
}

@dynamic store;

@end

