// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrainingStrengthSetMO.m instead.

#import "_TrainingStrengthSetMO.h"

const struct TrainingStrengthSetMOAttributes TrainingStrengthSetMOAttributes = {
	.reps = @"reps",
	.sortOrder = @"sortOrder",
	.weight = @"weight",
};

const struct TrainingStrengthSetMORelationships TrainingStrengthSetMORelationships = {
	.trainingStrength = @"trainingStrength",
};

@implementation TrainingStrengthSetMOID
@end

@implementation _TrainingStrengthSetMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TrainingStrengthSet" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TrainingStrengthSet";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TrainingStrengthSet" inManagedObjectContext:moc_];
}

- (TrainingStrengthSetMOID*)objectID {
	return (TrainingStrengthSetMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"repsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"reps"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"weightValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"weight"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic reps;

- (int16_t)repsValue {
	NSNumber *result = [self reps];
	return [result shortValue];
}

- (void)setRepsValue:(int16_t)value_ {
	[self setReps:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveRepsValue {
	NSNumber *result = [self primitiveReps];
	return [result shortValue];
}

- (void)setPrimitiveRepsValue:(int16_t)value_ {
	[self setPrimitiveReps:[NSNumber numberWithShort:value_]];
}

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic weight;

- (int16_t)weightValue {
	NSNumber *result = [self weight];
	return [result shortValue];
}

- (void)setWeightValue:(int16_t)value_ {
	[self setWeight:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveWeightValue {
	NSNumber *result = [self primitiveWeight];
	return [result shortValue];
}

- (void)setPrimitiveWeightValue:(int16_t)value_ {
	[self setPrimitiveWeight:[NSNumber numberWithShort:value_]];
}

@dynamic trainingStrength;

@end

