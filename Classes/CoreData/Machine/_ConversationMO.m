// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ConversationMO.m instead.

#import "_ConversationMO.h"

const struct ConversationMOAttributes ConversationMOAttributes = {
	.conversationUserGender = @"conversationUserGender",
	.conversationUserId = @"conversationUserId",
	.conversationUserImageId = @"conversationUserImageId",
	.conversationUserName = @"conversationUserName",
	.isRead = @"isRead",
	.loginId = @"loginId",
};

const struct ConversationMORelationships ConversationMORelationships = {
	.messageDeliveries = @"messageDeliveries",
};

const struct ConversationMOFetchedProperties ConversationMOFetchedProperties = {
	.newestMessage = @"newestMessage",
};

@implementation ConversationMOID
@end

@implementation _ConversationMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Conversation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Conversation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Conversation" inManagedObjectContext:moc_];
}

- (ConversationMOID*)objectID {
	return (ConversationMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"conversationUserGenderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"conversationUserGender"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"conversationUserIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"conversationUserId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"conversationUserImageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"conversationUserImageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isReadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRead"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic conversationUserGender;

- (int16_t)conversationUserGenderValue {
	NSNumber *result = [self conversationUserGender];
	return [result shortValue];
}

- (void)setConversationUserGenderValue:(int16_t)value_ {
	[self setConversationUserGender:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveConversationUserGenderValue {
	NSNumber *result = [self primitiveConversationUserGender];
	return [result shortValue];
}

- (void)setPrimitiveConversationUserGenderValue:(int16_t)value_ {
	[self setPrimitiveConversationUserGender:[NSNumber numberWithShort:value_]];
}

@dynamic conversationUserId;

- (int32_t)conversationUserIdValue {
	NSNumber *result = [self conversationUserId];
	return [result intValue];
}

- (void)setConversationUserIdValue:(int32_t)value_ {
	[self setConversationUserId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveConversationUserIdValue {
	NSNumber *result = [self primitiveConversationUserId];
	return [result intValue];
}

- (void)setPrimitiveConversationUserIdValue:(int32_t)value_ {
	[self setPrimitiveConversationUserId:[NSNumber numberWithInt:value_]];
}

@dynamic conversationUserImageId;

- (int32_t)conversationUserImageIdValue {
	NSNumber *result = [self conversationUserImageId];
	return [result intValue];
}

- (void)setConversationUserImageIdValue:(int32_t)value_ {
	[self setConversationUserImageId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveConversationUserImageIdValue {
	NSNumber *result = [self primitiveConversationUserImageId];
	return [result intValue];
}

- (void)setPrimitiveConversationUserImageIdValue:(int32_t)value_ {
	[self setPrimitiveConversationUserImageId:[NSNumber numberWithInt:value_]];
}

@dynamic conversationUserName;

@dynamic isRead;

- (BOOL)isReadValue {
	NSNumber *result = [self isRead];
	return [result boolValue];
}

- (void)setIsReadValue:(BOOL)value_ {
	[self setIsRead:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsReadValue {
	NSNumber *result = [self primitiveIsRead];
	return [result boolValue];
}

- (void)setPrimitiveIsReadValue:(BOOL)value_ {
	[self setPrimitiveIsRead:[NSNumber numberWithBool:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic messageDeliveries;

- (NSMutableSet*)messageDeliveriesSet {
	[self willAccessValueForKey:@"messageDeliveries"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"messageDeliveries"];

	[self didAccessValueForKey:@"messageDeliveries"];
	return result;
}

@dynamic newestMessage;

@end

