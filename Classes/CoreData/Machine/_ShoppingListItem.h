// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ShoppingListItem.h instead.

#import <CoreData/CoreData.h>

extern const struct ShoppingListItemAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *allergyCount;
	__unsafe_unretained NSString *barcode;
	__unsafe_unretained NSString *calciumPercent;
	__unsafe_unretained NSString *calories;
	__unsafe_unretained NSString *caloriesFromFat;
	__unsafe_unretained NSString *cholesterol;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *dietaryFiber;
	__unsafe_unretained NSString *imageId;
	__unsafe_unretained NSString *inCart;
	__unsafe_unretained NSString *inCartTime;
	__unsafe_unretained NSString *ingredientCount;
	__unsafe_unretained NSString *insolubleFiber;
	__unsafe_unretained NSString *ironPercent;
	__unsafe_unretained NSString *isRecommendation;
	__unsafe_unretained NSString *itemDescription;
	__unsafe_unretained NSString *lifestyleCount;
	__unsafe_unretained NSString *monounsaturatedFat;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *nameFirstCharacter;
	__unsafe_unretained NSString *nutritionHtml;
	__unsafe_unretained NSString *otherCarbohydrates;
	__unsafe_unretained NSString *polyunsaturatedFat;
	__unsafe_unretained NSString *potassium;
	__unsafe_unretained NSString *private;
	__unsafe_unretained NSString *productId;
	__unsafe_unretained NSString *promoAvailable;
	__unsafe_unretained NSString *promoSelectedCount;
	__unsafe_unretained NSString *protein;
	__unsafe_unretained NSString *purchased;
	__unsafe_unretained NSString *quantity;
	__unsafe_unretained NSString *recommendationDays;
	__unsafe_unretained NSString *saturatedFat;
	__unsafe_unretained NSString *saturatedFatCalories;
	__unsafe_unretained NSString *sodium;
	__unsafe_unretained NSString *solubleFiber;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *storeId;
	__unsafe_unretained NSString *sugars;
	__unsafe_unretained NSString *sugarsAlcohol;
	__unsafe_unretained NSString *totalCarbohydrates;
	__unsafe_unretained NSString *totalFat;
	__unsafe_unretained NSString *transFat;
	__unsafe_unretained NSString *updatedOn;
	__unsafe_unretained NSString *vitaminAPercent;
	__unsafe_unretained NSString *vitaminCPercent;
} ShoppingListItemAttributes;

extern const struct ShoppingListItemRelationships {
	__unsafe_unretained NSString *category;
	__unsafe_unretained NSString *customFood;
	__unsafe_unretained NSString *mealPlannerDays;
	__unsafe_unretained NSString *promotions;
	__unsafe_unretained NSString *storeCategory;
} ShoppingListItemRelationships;

@class CategoryMO;
@class CustomFoodMO;
@class MealPlannerShoppingListItem;
@class Promotion;
@class StoreCategory;

@interface ShoppingListItemID : NSManagedObjectID {}
@end

@interface _ShoppingListItem : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ShoppingListItemID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* allergyCount;

@property (atomic) int16_t allergyCountValue;
- (int16_t)allergyCountValue;
- (void)setAllergyCountValue:(int16_t)value_;

//- (BOOL)validateAllergyCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* barcode;

//- (BOOL)validateBarcode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* calciumPercent;

@property (atomic) double calciumPercentValue;
- (double)calciumPercentValue;
- (void)setCalciumPercentValue:(double)value_;

//- (BOOL)validateCalciumPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* calories;

@property (atomic) int16_t caloriesValue;
- (int16_t)caloriesValue;
- (void)setCaloriesValue:(int16_t)value_;

//- (BOOL)validateCalories:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* caloriesFromFat;

@property (atomic) int16_t caloriesFromFatValue;
- (int16_t)caloriesFromFatValue;
- (void)setCaloriesFromFatValue:(int16_t)value_;

//- (BOOL)validateCaloriesFromFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cholesterol;

@property (atomic) double cholesterolValue;
- (double)cholesterolValue;
- (void)setCholesterolValue:(double)value_;

//- (BOOL)validateCholesterol:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* dietaryFiber;

@property (atomic) double dietaryFiberValue;
- (double)dietaryFiberValue;
- (void)setDietaryFiberValue:(double)value_;

//- (BOOL)validateDietaryFiber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* imageId;

@property (atomic) int32_t imageIdValue;
- (int32_t)imageIdValue;
- (void)setImageIdValue:(int32_t)value_;

//- (BOOL)validateImageId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* inCart;

@property (atomic) BOOL inCartValue;
- (BOOL)inCartValue;
- (void)setInCartValue:(BOOL)value_;

//- (BOOL)validateInCart:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* inCartTime;

//- (BOOL)validateInCartTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ingredientCount;

@property (atomic) int16_t ingredientCountValue;
- (int16_t)ingredientCountValue;
- (void)setIngredientCountValue:(int16_t)value_;

//- (BOOL)validateIngredientCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* insolubleFiber;

@property (atomic) double insolubleFiberValue;
- (double)insolubleFiberValue;
- (void)setInsolubleFiberValue:(double)value_;

//- (BOOL)validateInsolubleFiber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ironPercent;

@property (atomic) double ironPercentValue;
- (double)ironPercentValue;
- (void)setIronPercentValue:(double)value_;

//- (BOOL)validateIronPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRecommendation;

@property (atomic) BOOL isRecommendationValue;
- (BOOL)isRecommendationValue;
- (void)setIsRecommendationValue:(BOOL)value_;

//- (BOOL)validateIsRecommendation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* itemDescription;

//- (BOOL)validateItemDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* lifestyleCount;

@property (atomic) int16_t lifestyleCountValue;
- (int16_t)lifestyleCountValue;
- (void)setLifestyleCountValue:(int16_t)value_;

//- (BOOL)validateLifestyleCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* monounsaturatedFat;

@property (atomic) double monounsaturatedFatValue;
- (double)monounsaturatedFatValue;
- (void)setMonounsaturatedFatValue:(double)value_;

//- (BOOL)validateMonounsaturatedFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nameFirstCharacter;

//- (BOOL)validateNameFirstCharacter:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nutritionHtml;

//- (BOOL)validateNutritionHtml:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* otherCarbohydrates;

@property (atomic) double otherCarbohydratesValue;
- (double)otherCarbohydratesValue;
- (void)setOtherCarbohydratesValue:(double)value_;

//- (BOOL)validateOtherCarbohydrates:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* polyunsaturatedFat;

@property (atomic) double polyunsaturatedFatValue;
- (double)polyunsaturatedFatValue;
- (void)setPolyunsaturatedFatValue:(double)value_;

//- (BOOL)validatePolyunsaturatedFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* potassium;

@property (atomic) double potassiumValue;
- (double)potassiumValue;
- (void)setPotassiumValue:(double)value_;

//- (BOOL)validatePotassium:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* private;

@property (atomic) BOOL privateValue;
- (BOOL)privateValue;
- (void)setPrivateValue:(BOOL)value_;

//- (BOOL)validatePrivate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* productId;

@property (atomic) int32_t productIdValue;
- (int32_t)productIdValue;
- (void)setProductIdValue:(int32_t)value_;

//- (BOOL)validateProductId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* promoAvailable;

@property (atomic) BOOL promoAvailableValue;
- (BOOL)promoAvailableValue;
- (void)setPromoAvailableValue:(BOOL)value_;

//- (BOOL)validatePromoAvailable:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* promoSelectedCount;

@property (atomic) int16_t promoSelectedCountValue;
- (int16_t)promoSelectedCountValue;
- (void)setPromoSelectedCountValue:(int16_t)value_;

//- (BOOL)validatePromoSelectedCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* protein;

@property (atomic) double proteinValue;
- (double)proteinValue;
- (void)setProteinValue:(double)value_;

//- (BOOL)validateProtein:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* purchased;

@property (atomic) BOOL purchasedValue;
- (BOOL)purchasedValue;
- (void)setPurchasedValue:(BOOL)value_;

//- (BOOL)validatePurchased:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* quantity;

@property (atomic) double quantityValue;
- (double)quantityValue;
- (void)setQuantityValue:(double)value_;

//- (BOOL)validateQuantity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* recommendationDays;

@property (atomic) int16_t recommendationDaysValue;
- (int16_t)recommendationDaysValue;
- (void)setRecommendationDaysValue:(int16_t)value_;

//- (BOOL)validateRecommendationDays:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* saturatedFat;

@property (atomic) double saturatedFatValue;
- (double)saturatedFatValue;
- (void)setSaturatedFatValue:(double)value_;

//- (BOOL)validateSaturatedFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* saturatedFatCalories;

@property (atomic) int16_t saturatedFatCaloriesValue;
- (int16_t)saturatedFatCaloriesValue;
- (void)setSaturatedFatCaloriesValue:(int16_t)value_;

//- (BOOL)validateSaturatedFatCalories:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sodium;

@property (atomic) double sodiumValue;
- (double)sodiumValue;
- (void)setSodiumValue:(double)value_;

//- (BOOL)validateSodium:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* solubleFiber;

@property (atomic) double solubleFiberValue;
- (double)solubleFiberValue;
- (void)setSolubleFiberValue:(double)value_;

//- (BOOL)validateSolubleFiber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* storeId;

@property (atomic) int32_t storeIdValue;
- (int32_t)storeIdValue;
- (void)setStoreIdValue:(int32_t)value_;

//- (BOOL)validateStoreId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sugars;

@property (atomic) double sugarsValue;
- (double)sugarsValue;
- (void)setSugarsValue:(double)value_;

//- (BOOL)validateSugars:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sugarsAlcohol;

@property (atomic) double sugarsAlcoholValue;
- (double)sugarsAlcoholValue;
- (void)setSugarsAlcoholValue:(double)value_;

//- (BOOL)validateSugarsAlcohol:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* totalCarbohydrates;

@property (atomic) double totalCarbohydratesValue;
- (double)totalCarbohydratesValue;
- (void)setTotalCarbohydratesValue:(double)value_;

//- (BOOL)validateTotalCarbohydrates:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* totalFat;

@property (atomic) double totalFatValue;
- (double)totalFatValue;
- (void)setTotalFatValue:(double)value_;

//- (BOOL)validateTotalFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* transFat;

@property (atomic) double transFatValue;
- (double)transFatValue;
- (void)setTransFatValue:(double)value_;

//- (BOOL)validateTransFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* vitaminAPercent;

@property (atomic) double vitaminAPercentValue;
- (double)vitaminAPercentValue;
- (void)setVitaminAPercentValue:(double)value_;

//- (BOOL)validateVitaminAPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* vitaminCPercent;

@property (atomic) double vitaminCPercentValue;
- (double)vitaminCPercentValue;
- (void)setVitaminCPercentValue:(double)value_;

//- (BOOL)validateVitaminCPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CategoryMO *category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CustomFoodMO *customFood;

//- (BOOL)validateCustomFood:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *mealPlannerDays;

- (NSMutableSet*)mealPlannerDaysSet;

@property (nonatomic, strong) NSSet *promotions;

- (NSMutableSet*)promotionsSet;

@property (nonatomic, strong) StoreCategory *storeCategory;

//- (BOOL)validateStoreCategory:(id*)value_ error:(NSError**)error_;

@end

@interface _ShoppingListItem (MealPlannerDaysCoreDataGeneratedAccessors)
- (void)addMealPlannerDays:(NSSet*)value_;
- (void)removeMealPlannerDays:(NSSet*)value_;
- (void)addMealPlannerDaysObject:(MealPlannerShoppingListItem*)value_;
- (void)removeMealPlannerDaysObject:(MealPlannerShoppingListItem*)value_;

@end

@interface _ShoppingListItem (PromotionsCoreDataGeneratedAccessors)
- (void)addPromotions:(NSSet*)value_;
- (void)removePromotions:(NSSet*)value_;
- (void)addPromotionsObject:(Promotion*)value_;
- (void)removePromotionsObject:(Promotion*)value_;

@end

@interface _ShoppingListItem (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSNumber*)primitiveAllergyCount;
- (void)setPrimitiveAllergyCount:(NSNumber*)value;

- (int16_t)primitiveAllergyCountValue;
- (void)setPrimitiveAllergyCountValue:(int16_t)value_;

- (NSString*)primitiveBarcode;
- (void)setPrimitiveBarcode:(NSString*)value;

- (NSNumber*)primitiveCalciumPercent;
- (void)setPrimitiveCalciumPercent:(NSNumber*)value;

- (double)primitiveCalciumPercentValue;
- (void)setPrimitiveCalciumPercentValue:(double)value_;

- (NSNumber*)primitiveCalories;
- (void)setPrimitiveCalories:(NSNumber*)value;

- (int16_t)primitiveCaloriesValue;
- (void)setPrimitiveCaloriesValue:(int16_t)value_;

- (NSNumber*)primitiveCaloriesFromFat;
- (void)setPrimitiveCaloriesFromFat:(NSNumber*)value;

- (int16_t)primitiveCaloriesFromFatValue;
- (void)setPrimitiveCaloriesFromFatValue:(int16_t)value_;

- (NSNumber*)primitiveCholesterol;
- (void)setPrimitiveCholesterol:(NSNumber*)value;

- (double)primitiveCholesterolValue;
- (void)setPrimitiveCholesterolValue:(double)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveDietaryFiber;
- (void)setPrimitiveDietaryFiber:(NSNumber*)value;

- (double)primitiveDietaryFiberValue;
- (void)setPrimitiveDietaryFiberValue:(double)value_;

- (NSNumber*)primitiveImageId;
- (void)setPrimitiveImageId:(NSNumber*)value;

- (int32_t)primitiveImageIdValue;
- (void)setPrimitiveImageIdValue:(int32_t)value_;

- (NSNumber*)primitiveInCart;
- (void)setPrimitiveInCart:(NSNumber*)value;

- (BOOL)primitiveInCartValue;
- (void)setPrimitiveInCartValue:(BOOL)value_;

- (NSDate*)primitiveInCartTime;
- (void)setPrimitiveInCartTime:(NSDate*)value;

- (NSNumber*)primitiveIngredientCount;
- (void)setPrimitiveIngredientCount:(NSNumber*)value;

- (int16_t)primitiveIngredientCountValue;
- (void)setPrimitiveIngredientCountValue:(int16_t)value_;

- (NSNumber*)primitiveInsolubleFiber;
- (void)setPrimitiveInsolubleFiber:(NSNumber*)value;

- (double)primitiveInsolubleFiberValue;
- (void)setPrimitiveInsolubleFiberValue:(double)value_;

- (NSNumber*)primitiveIronPercent;
- (void)setPrimitiveIronPercent:(NSNumber*)value;

- (double)primitiveIronPercentValue;
- (void)setPrimitiveIronPercentValue:(double)value_;

- (NSNumber*)primitiveIsRecommendation;
- (void)setPrimitiveIsRecommendation:(NSNumber*)value;

- (BOOL)primitiveIsRecommendationValue;
- (void)setPrimitiveIsRecommendationValue:(BOOL)value_;

- (NSString*)primitiveItemDescription;
- (void)setPrimitiveItemDescription:(NSString*)value;

- (NSNumber*)primitiveLifestyleCount;
- (void)setPrimitiveLifestyleCount:(NSNumber*)value;

- (int16_t)primitiveLifestyleCountValue;
- (void)setPrimitiveLifestyleCountValue:(int16_t)value_;

- (NSNumber*)primitiveMonounsaturatedFat;
- (void)setPrimitiveMonounsaturatedFat:(NSNumber*)value;

- (double)primitiveMonounsaturatedFatValue;
- (void)setPrimitiveMonounsaturatedFatValue:(double)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveNameFirstCharacter;
- (void)setPrimitiveNameFirstCharacter:(NSString*)value;

- (NSString*)primitiveNutritionHtml;
- (void)setPrimitiveNutritionHtml:(NSString*)value;

- (NSNumber*)primitiveOtherCarbohydrates;
- (void)setPrimitiveOtherCarbohydrates:(NSNumber*)value;

- (double)primitiveOtherCarbohydratesValue;
- (void)setPrimitiveOtherCarbohydratesValue:(double)value_;

- (NSNumber*)primitivePolyunsaturatedFat;
- (void)setPrimitivePolyunsaturatedFat:(NSNumber*)value;

- (double)primitivePolyunsaturatedFatValue;
- (void)setPrimitivePolyunsaturatedFatValue:(double)value_;

- (NSNumber*)primitivePotassium;
- (void)setPrimitivePotassium:(NSNumber*)value;

- (double)primitivePotassiumValue;
- (void)setPrimitivePotassiumValue:(double)value_;

- (NSNumber*)primitivePrivate;
- (void)setPrimitivePrivate:(NSNumber*)value;

- (BOOL)primitivePrivateValue;
- (void)setPrimitivePrivateValue:(BOOL)value_;

- (NSNumber*)primitiveProductId;
- (void)setPrimitiveProductId:(NSNumber*)value;

- (int32_t)primitiveProductIdValue;
- (void)setPrimitiveProductIdValue:(int32_t)value_;

- (NSNumber*)primitivePromoAvailable;
- (void)setPrimitivePromoAvailable:(NSNumber*)value;

- (BOOL)primitivePromoAvailableValue;
- (void)setPrimitivePromoAvailableValue:(BOOL)value_;

- (NSNumber*)primitivePromoSelectedCount;
- (void)setPrimitivePromoSelectedCount:(NSNumber*)value;

- (int16_t)primitivePromoSelectedCountValue;
- (void)setPrimitivePromoSelectedCountValue:(int16_t)value_;

- (NSNumber*)primitiveProtein;
- (void)setPrimitiveProtein:(NSNumber*)value;

- (double)primitiveProteinValue;
- (void)setPrimitiveProteinValue:(double)value_;

- (NSNumber*)primitivePurchased;
- (void)setPrimitivePurchased:(NSNumber*)value;

- (BOOL)primitivePurchasedValue;
- (void)setPrimitivePurchasedValue:(BOOL)value_;

- (NSNumber*)primitiveQuantity;
- (void)setPrimitiveQuantity:(NSNumber*)value;

- (double)primitiveQuantityValue;
- (void)setPrimitiveQuantityValue:(double)value_;

- (NSNumber*)primitiveRecommendationDays;
- (void)setPrimitiveRecommendationDays:(NSNumber*)value;

- (int16_t)primitiveRecommendationDaysValue;
- (void)setPrimitiveRecommendationDaysValue:(int16_t)value_;

- (NSNumber*)primitiveSaturatedFat;
- (void)setPrimitiveSaturatedFat:(NSNumber*)value;

- (double)primitiveSaturatedFatValue;
- (void)setPrimitiveSaturatedFatValue:(double)value_;

- (NSNumber*)primitiveSaturatedFatCalories;
- (void)setPrimitiveSaturatedFatCalories:(NSNumber*)value;

- (int16_t)primitiveSaturatedFatCaloriesValue;
- (void)setPrimitiveSaturatedFatCaloriesValue:(int16_t)value_;

- (NSNumber*)primitiveSodium;
- (void)setPrimitiveSodium:(NSNumber*)value;

- (double)primitiveSodiumValue;
- (void)setPrimitiveSodiumValue:(double)value_;

- (NSNumber*)primitiveSolubleFiber;
- (void)setPrimitiveSolubleFiber:(NSNumber*)value;

- (double)primitiveSolubleFiberValue;
- (void)setPrimitiveSolubleFiberValue:(double)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveStoreId;
- (void)setPrimitiveStoreId:(NSNumber*)value;

- (int32_t)primitiveStoreIdValue;
- (void)setPrimitiveStoreIdValue:(int32_t)value_;

- (NSNumber*)primitiveSugars;
- (void)setPrimitiveSugars:(NSNumber*)value;

- (double)primitiveSugarsValue;
- (void)setPrimitiveSugarsValue:(double)value_;

- (NSNumber*)primitiveSugarsAlcohol;
- (void)setPrimitiveSugarsAlcohol:(NSNumber*)value;

- (double)primitiveSugarsAlcoholValue;
- (void)setPrimitiveSugarsAlcoholValue:(double)value_;

- (NSNumber*)primitiveTotalCarbohydrates;
- (void)setPrimitiveTotalCarbohydrates:(NSNumber*)value;

- (double)primitiveTotalCarbohydratesValue;
- (void)setPrimitiveTotalCarbohydratesValue:(double)value_;

- (NSNumber*)primitiveTotalFat;
- (void)setPrimitiveTotalFat:(NSNumber*)value;

- (double)primitiveTotalFatValue;
- (void)setPrimitiveTotalFatValue:(double)value_;

- (NSNumber*)primitiveTransFat;
- (void)setPrimitiveTransFat:(NSNumber*)value;

- (double)primitiveTransFatValue;
- (void)setPrimitiveTransFatValue:(double)value_;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveVitaminAPercent;
- (void)setPrimitiveVitaminAPercent:(NSNumber*)value;

- (double)primitiveVitaminAPercentValue;
- (void)setPrimitiveVitaminAPercentValue:(double)value_;

- (NSNumber*)primitiveVitaminCPercent;
- (void)setPrimitiveVitaminCPercent:(NSNumber*)value;

- (double)primitiveVitaminCPercentValue;
- (void)setPrimitiveVitaminCPercentValue:(double)value_;

- (CategoryMO*)primitiveCategory;
- (void)setPrimitiveCategory:(CategoryMO*)value;

- (CustomFoodMO*)primitiveCustomFood;
- (void)setPrimitiveCustomFood:(CustomFoodMO*)value;

- (NSMutableSet*)primitiveMealPlannerDays;
- (void)setPrimitiveMealPlannerDays:(NSMutableSet*)value;

- (NSMutableSet*)primitivePromotions;
- (void)setPrimitivePromotions:(NSMutableSet*)value;

- (StoreCategory*)primitiveStoreCategory;
- (void)setPrimitiveStoreCategory:(StoreCategory*)value;

@end
