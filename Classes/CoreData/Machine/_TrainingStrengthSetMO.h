// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrainingStrengthSetMO.h instead.

#import <CoreData/CoreData.h>

extern const struct TrainingStrengthSetMOAttributes {
	__unsafe_unretained NSString *reps;
	__unsafe_unretained NSString *sortOrder;
	__unsafe_unretained NSString *weight;
} TrainingStrengthSetMOAttributes;

extern const struct TrainingStrengthSetMORelationships {
	__unsafe_unretained NSString *trainingStrength;
} TrainingStrengthSetMORelationships;

@class TrainingStrengthMO;

@interface TrainingStrengthSetMOID : NSManagedObjectID {}
@end

@interface _TrainingStrengthSetMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TrainingStrengthSetMOID* objectID;

@property (nonatomic, strong) NSNumber* reps;

@property (atomic) int16_t repsValue;
- (int16_t)repsValue;
- (void)setRepsValue:(int16_t)value_;

//- (BOOL)validateReps:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* weight;

@property (atomic) int16_t weightValue;
- (int16_t)weightValue;
- (void)setWeightValue:(int16_t)value_;

//- (BOOL)validateWeight:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TrainingStrengthMO *trainingStrength;

//- (BOOL)validateTrainingStrength:(id*)value_ error:(NSError**)error_;

@end

@interface _TrainingStrengthSetMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveReps;
- (void)setPrimitiveReps:(NSNumber*)value;

- (int16_t)primitiveRepsValue;
- (void)setPrimitiveRepsValue:(int16_t)value_;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSNumber*)primitiveWeight;
- (void)setPrimitiveWeight:(NSNumber*)value;

- (int16_t)primitiveWeightValue;
- (void)setPrimitiveWeightValue:(int16_t)value_;

- (TrainingStrengthMO*)primitiveTrainingStrength;
- (void)setPrimitiveTrainingStrength:(TrainingStrengthMO*)value;

@end
