// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityPlanMO.m instead.

#import "_ActivityPlanMO.h"

@implementation ActivityPlanMOID
@end

@implementation _ActivityPlanMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ActivityPlan" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ActivityPlan";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ActivityPlan" inManagedObjectContext:moc_];
}

- (ActivityPlanMOID*)objectID {
	return (ActivityPlanMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@end

