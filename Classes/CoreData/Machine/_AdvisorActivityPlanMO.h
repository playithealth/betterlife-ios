// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanMO.h instead.

#import <CoreData/CoreData.h>
#import "AdvisorPlanMO.h"

@interface AdvisorActivityPlanMOID : AdvisorPlanMOID {}
@end

@interface _AdvisorActivityPlanMO : AdvisorPlanMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorActivityPlanMOID* objectID;

@end

@interface _AdvisorActivityPlanMO (CoreDataGeneratedPrimitiveAccessors)

@end
