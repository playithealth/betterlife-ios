// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to QuickListItem.h instead.

#import <CoreData/CoreData.h>


extern const struct QuickListItemAttributes {
	__unsafe_unretained NSString *item;
	__unsafe_unretained NSString *productId;
} QuickListItemAttributes;

extern const struct QuickListItemRelationships {
	__unsafe_unretained NSString *QuickList;
} QuickListItemRelationships;

extern const struct QuickListItemFetchedProperties {
} QuickListItemFetchedProperties;

@class QuickList;




@interface QuickListItemID : NSManagedObjectID {}
@end

@interface _QuickListItem : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (QuickListItemID*)objectID;




@property (nonatomic, strong) NSString* item;


//- (BOOL)validateItem:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* productId;


@property int32_t productIdValue;
- (int32_t)productIdValue;
- (void)setProductIdValue:(int32_t)value_;

//- (BOOL)validateProductId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) QuickList* QuickList;

//- (BOOL)validateQuickList:(id*)value_ error:(NSError**)error_;





@end

@interface _QuickListItem (CoreDataGeneratedAccessors)

@end

@interface _QuickListItem (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveItem;
- (void)setPrimitiveItem:(NSString*)value;




- (NSNumber*)primitiveProductId;
- (void)setPrimitiveProductId:(NSNumber*)value;

- (int32_t)primitiveProductIdValue;
- (void)setPrimitiveProductIdValue:(int32_t)value_;





- (QuickList*)primitiveQuickList;
- (void)setPrimitiveQuickList:(QuickList*)value;


@end
