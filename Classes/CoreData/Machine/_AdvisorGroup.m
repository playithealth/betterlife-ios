// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorGroup.m instead.

#import "_AdvisorGroup.h"

const struct AdvisorGroupAttributes AdvisorGroupAttributes = {
	.cloudId = @"cloudId",
};

const struct AdvisorGroupRelationships AdvisorGroupRelationships = {
	.advisor = @"advisor",
	.recipes = @"recipes",
};

@implementation AdvisorGroupID
@end

@implementation _AdvisorGroup

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorGroup" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorGroup";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorGroup" inManagedObjectContext:moc_];
}

- (AdvisorGroupID*)objectID {
	return (AdvisorGroupID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic advisor;

@dynamic recipes;

- (NSMutableSet*)recipesSet {
	[self willAccessValueForKey:@"recipes"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"recipes"];

	[self didAccessValueForKey:@"recipes"];
	return result;
}

@end

