// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserAchievement.m instead.

#import "_UserAchievement.h"

const struct UserAchievementAttributes UserAchievementAttributes = {
	.date = @"date",
	.loginId = @"loginId",
};

const struct UserAchievementRelationships UserAchievementRelationships = {
	.achievement = @"achievement",
};

@implementation UserAchievementID
@end

@implementation _UserAchievement

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserAchievement" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserAchievement";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserAchievement" inManagedObjectContext:moc_];
}

- (UserAchievementID*)objectID {
	return (UserAchievementID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic date;

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic achievement;

@end

