// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to HelpView.h instead.

#import <CoreData/CoreData.h>

extern const struct HelpViewAttributes {
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *viewed;
} HelpViewAttributes;

@interface HelpViewID : NSManagedObjectID {}
@end

@interface _HelpView : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) HelpViewID* objectID;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* viewed;

@property (atomic) BOOL viewedValue;
- (BOOL)viewedValue;
- (void)setViewedValue:(BOOL)value_;

//- (BOOL)validateViewed:(id*)value_ error:(NSError**)error_;

@end

@interface _HelpView (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveViewed;
- (void)setPrimitiveViewed:(NSNumber*)value;

- (BOOL)primitiveViewedValue;
- (void)setPrimitiveViewedValue:(BOOL)value_;

@end
