// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RestaurantChainMO.m instead.

#import "_RestaurantChainMO.h"

const struct RestaurantChainMOAttributes RestaurantChainMOAttributes = {
	.cloudId = @"cloudId",
	.date = @"date",
	.loginId = @"loginId",
	.name = @"name",
};

@implementation RestaurantChainMOID
@end

@implementation _RestaurantChainMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"RestaurantChain" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"RestaurantChain";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"RestaurantChain" inManagedObjectContext:moc_];
}

- (RestaurantChainMOID*)objectID {
	return (RestaurantChainMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic date;

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@end

