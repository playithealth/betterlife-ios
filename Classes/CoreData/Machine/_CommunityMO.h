// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CommunityMO.h instead.

#import <CoreData/CoreData.h>

extern const struct CommunityMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *communityStatus;
	__unsafe_unretained NSString *createdOn;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *ownerId;
	__unsafe_unretained NSString *ownerName;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *type;
	__unsafe_unretained NSString *updatedOn;
	__unsafe_unretained NSString *usePhoto;
	__unsafe_unretained NSString *visibility;
} CommunityMOAttributes;

extern const struct CommunityMORelationships {
	__unsafe_unretained NSString *threads;
	__unsafe_unretained NSString *userProfile;
} CommunityMORelationships;

@class ThreadMO;
@class UserProfileMO;

@interface CommunityMOID : NSManagedObjectID {}
@end

@interface _CommunityMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CommunityMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* communityStatus;

//- (BOOL)validateCommunityStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* createdOn;

@property (atomic) int32_t createdOnValue;
- (int32_t)createdOnValue;
- (void)setCreatedOnValue:(int32_t)value_;

//- (BOOL)validateCreatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ownerId;

@property (atomic) int32_t ownerIdValue;
- (int32_t)ownerIdValue;
- (void)setOwnerIdValue:(int32_t)value_;

//- (BOOL)validateOwnerId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ownerName;

//- (BOOL)validateOwnerName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* type;

//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* usePhoto;

@property (atomic) BOOL usePhotoValue;
- (BOOL)usePhotoValue;
- (void)setUsePhotoValue:(BOOL)value_;

//- (BOOL)validateUsePhoto:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* visibility;

//- (BOOL)validateVisibility:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *threads;

- (NSMutableSet*)threadsSet;

@property (nonatomic, strong) UserProfileMO *userProfile;

//- (BOOL)validateUserProfile:(id*)value_ error:(NSError**)error_;

@end

@interface _CommunityMO (ThreadsCoreDataGeneratedAccessors)
- (void)addThreads:(NSSet*)value_;
- (void)removeThreads:(NSSet*)value_;
- (void)addThreadsObject:(ThreadMO*)value_;
- (void)removeThreadsObject:(ThreadMO*)value_;

@end

@interface _CommunityMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSString*)primitiveCommunityStatus;
- (void)setPrimitiveCommunityStatus:(NSString*)value;

- (NSNumber*)primitiveCreatedOn;
- (void)setPrimitiveCreatedOn:(NSNumber*)value;

- (int32_t)primitiveCreatedOnValue;
- (void)setPrimitiveCreatedOnValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveOwnerId;
- (void)setPrimitiveOwnerId:(NSNumber*)value;

- (int32_t)primitiveOwnerIdValue;
- (void)setPrimitiveOwnerIdValue:(int32_t)value_;

- (NSString*)primitiveOwnerName;
- (void)setPrimitiveOwnerName:(NSString*)value;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveUsePhoto;
- (void)setPrimitiveUsePhoto:(NSNumber*)value;

- (BOOL)primitiveUsePhotoValue;
- (void)setPrimitiveUsePhotoValue:(BOOL)value_;

- (NSString*)primitiveVisibility;
- (void)setPrimitiveVisibility:(NSString*)value;

- (NSMutableSet*)primitiveThreads;
- (void)setPrimitiveThreads:(NSMutableSet*)value;

- (UserProfileMO*)primitiveUserProfile;
- (void)setPrimitiveUserProfile:(UserProfileMO*)value;

@end
