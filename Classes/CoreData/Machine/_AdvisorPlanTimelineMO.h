// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorPlanTimelineMO.h instead.

#import <CoreData/CoreData.h>

extern const struct AdvisorPlanTimelineMOAttributes {
	__unsafe_unretained NSString *startDate;
	__unsafe_unretained NSString *stopDate;
} AdvisorPlanTimelineMOAttributes;

extern const struct AdvisorPlanTimelineMORelationships {
	__unsafe_unretained NSString *plan;
} AdvisorPlanTimelineMORelationships;

@class AdvisorPlanMO;

@interface AdvisorPlanTimelineMOID : NSManagedObjectID {}
@end

@interface _AdvisorPlanTimelineMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorPlanTimelineMOID* objectID;

@property (nonatomic, strong) NSDate* startDate;

//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* stopDate;

//- (BOOL)validateStopDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AdvisorPlanMO *plan;

//- (BOOL)validatePlan:(id*)value_ error:(NSError**)error_;

@end

@interface _AdvisorPlanTimelineMO (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveStartDate;
- (void)setPrimitiveStartDate:(NSDate*)value;

- (NSDate*)primitiveStopDate;
- (void)setPrimitiveStopDate:(NSDate*)value;

- (AdvisorPlanMO*)primitivePlan;
- (void)setPrimitivePlan:(AdvisorPlanMO*)value;

@end
