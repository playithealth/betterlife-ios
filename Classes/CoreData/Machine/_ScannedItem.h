// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ScannedItem.h instead.

#import <CoreData/CoreData.h>

extern const struct ScannedItemAttributes {
	__unsafe_unretained NSString *barcode;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *name;
} ScannedItemAttributes;

extern const struct ScannedItemRelationships {
	__unsafe_unretained NSString *category;
} ScannedItemRelationships;

@class CategoryMO;

@interface ScannedItemID : NSManagedObjectID {}
@end

@interface _ScannedItem : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ScannedItemID* objectID;

@property (nonatomic, strong) NSString* barcode;

//- (BOOL)validateBarcode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CategoryMO *category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;

@end

@interface _ScannedItem (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveBarcode;
- (void)setPrimitiveBarcode:(NSString*)value;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (CategoryMO*)primitiveCategory;
- (void)setPrimitiveCategory:(CategoryMO*)value;

@end
