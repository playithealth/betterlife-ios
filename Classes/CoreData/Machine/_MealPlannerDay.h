// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealPlannerDay.h instead.

#import <CoreData/CoreData.h>

extern const struct MealPlannerDayAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *imageId;
	__unsafe_unretained NSString *itemType;
	__unsafe_unretained NSString *logTime;
	__unsafe_unretained NSString *menuId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *nutritionId;
	__unsafe_unretained NSString *productId;
	__unsafe_unretained NSString *recipeId;
	__unsafe_unretained NSString *servingSize;
	__unsafe_unretained NSString *servingSizeUnit;
	__unsafe_unretained NSString *servingSizeUnitAbbreviation;
	__unsafe_unretained NSString *servings;
	__unsafe_unretained NSString *sortOrder;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *tagId;
	__unsafe_unretained NSString *updatedOn;
} MealPlannerDayAttributes;

extern const struct MealPlannerDayRelationships {
	__unsafe_unretained NSString *nutrition;
	__unsafe_unretained NSString *recipe;
	__unsafe_unretained NSString *shoppingListItems;
	__unsafe_unretained NSString *tag;
} MealPlannerDayRelationships;

@class NutritionMO;
@class RecipeMO;
@class MealPlannerShoppingListItem;
@class AdvisorMealPlanTagMO;

@interface MealPlannerDayID : NSManagedObjectID {}
@end

@interface _MealPlannerDay : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MealPlannerDayID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* imageId;

@property (atomic) int32_t imageIdValue;
- (int32_t)imageIdValue;
- (void)setImageIdValue:(int32_t)value_;

//- (BOOL)validateImageId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* itemType;

//- (BOOL)validateItemType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* logTime;

@property (atomic) int16_t logTimeValue;
- (int16_t)logTimeValue;
- (void)setLogTimeValue:(int16_t)value_;

//- (BOOL)validateLogTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* menuId;

@property (atomic) int32_t menuIdValue;
- (int32_t)menuIdValue;
- (void)setMenuIdValue:(int32_t)value_;

//- (BOOL)validateMenuId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* nutritionId;

@property (atomic) int32_t nutritionIdValue;
- (int32_t)nutritionIdValue;
- (void)setNutritionIdValue:(int32_t)value_;

//- (BOOL)validateNutritionId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* productId;

@property (atomic) int32_t productIdValue;
- (int32_t)productIdValue;
- (void)setProductIdValue:(int32_t)value_;

//- (BOOL)validateProductId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* recipeId;

@property (atomic) int32_t recipeIdValue;
- (int32_t)recipeIdValue;
- (void)setRecipeIdValue:(int32_t)value_;

//- (BOOL)validateRecipeId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* servingSize;

//- (BOOL)validateServingSize:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* servingSizeUnit;

//- (BOOL)validateServingSizeUnit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* servingSizeUnitAbbreviation;

//- (BOOL)validateServingSizeUnitAbbreviation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* servings;

@property (atomic) double servingsValue;
- (double)servingsValue;
- (void)setServingsValue:(double)value_;

//- (BOOL)validateServings:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* tagId;

@property (atomic) int32_t tagIdValue;
- (int32_t)tagIdValue;
- (void)setTagIdValue:(int32_t)value_;

//- (BOOL)validateTagId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NutritionMO *nutrition;

//- (BOOL)validateNutrition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) RecipeMO *recipe;

//- (BOOL)validateRecipe:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *shoppingListItems;

- (NSMutableSet*)shoppingListItemsSet;

@property (nonatomic, strong) AdvisorMealPlanTagMO *tag;

//- (BOOL)validateTag:(id*)value_ error:(NSError**)error_;

@end

@interface _MealPlannerDay (ShoppingListItemsCoreDataGeneratedAccessors)
- (void)addShoppingListItems:(NSSet*)value_;
- (void)removeShoppingListItems:(NSSet*)value_;
- (void)addShoppingListItemsObject:(MealPlannerShoppingListItem*)value_;
- (void)removeShoppingListItemsObject:(MealPlannerShoppingListItem*)value_;

@end

@interface _MealPlannerDay (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSNumber*)primitiveImageId;
- (void)setPrimitiveImageId:(NSNumber*)value;

- (int32_t)primitiveImageIdValue;
- (void)setPrimitiveImageIdValue:(int32_t)value_;

- (NSString*)primitiveItemType;
- (void)setPrimitiveItemType:(NSString*)value;

- (NSNumber*)primitiveLogTime;
- (void)setPrimitiveLogTime:(NSNumber*)value;

- (int16_t)primitiveLogTimeValue;
- (void)setPrimitiveLogTimeValue:(int16_t)value_;

- (NSNumber*)primitiveMenuId;
- (void)setPrimitiveMenuId:(NSNumber*)value;

- (int32_t)primitiveMenuIdValue;
- (void)setPrimitiveMenuIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveNutritionId;
- (void)setPrimitiveNutritionId:(NSNumber*)value;

- (int32_t)primitiveNutritionIdValue;
- (void)setPrimitiveNutritionIdValue:(int32_t)value_;

- (NSNumber*)primitiveProductId;
- (void)setPrimitiveProductId:(NSNumber*)value;

- (int32_t)primitiveProductIdValue;
- (void)setPrimitiveProductIdValue:(int32_t)value_;

- (NSNumber*)primitiveRecipeId;
- (void)setPrimitiveRecipeId:(NSNumber*)value;

- (int32_t)primitiveRecipeIdValue;
- (void)setPrimitiveRecipeIdValue:(int32_t)value_;

- (NSString*)primitiveServingSize;
- (void)setPrimitiveServingSize:(NSString*)value;

- (NSString*)primitiveServingSizeUnit;
- (void)setPrimitiveServingSizeUnit:(NSString*)value;

- (NSString*)primitiveServingSizeUnitAbbreviation;
- (void)setPrimitiveServingSizeUnitAbbreviation:(NSString*)value;

- (NSNumber*)primitiveServings;
- (void)setPrimitiveServings:(NSNumber*)value;

- (double)primitiveServingsValue;
- (void)setPrimitiveServingsValue:(double)value_;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveTagId;
- (void)setPrimitiveTagId:(NSNumber*)value;

- (int32_t)primitiveTagIdValue;
- (void)setPrimitiveTagIdValue:(int32_t)value_;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NutritionMO*)primitiveNutrition;
- (void)setPrimitiveNutrition:(NutritionMO*)value;

- (RecipeMO*)primitiveRecipe;
- (void)setPrimitiveRecipe:(RecipeMO*)value;

- (NSMutableSet*)primitiveShoppingListItems;
- (void)setPrimitiveShoppingListItems:(NSMutableSet*)value;

- (AdvisorMealPlanTagMO*)primitiveTag;
- (void)setPrimitiveTag:(AdvisorMealPlanTagMO*)value;

@end
