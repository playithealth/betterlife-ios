// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrainingStrengthMO.h instead.

#import <CoreData/CoreData.h>

extern const struct TrainingStrengthMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *resistance;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
} TrainingStrengthMOAttributes;

extern const struct TrainingStrengthMORelationships {
	__unsafe_unretained NSString *sets;
} TrainingStrengthMORelationships;

@class TrainingStrengthSetMO;

@interface TrainingStrengthMOID : NSManagedObjectID {}
@end

@interface _TrainingStrengthMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TrainingStrengthMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* resistance;

//- (BOOL)validateResistance:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *sets;

- (NSMutableSet*)setsSet;

@end

@interface _TrainingStrengthMO (SetsCoreDataGeneratedAccessors)
- (void)addSets:(NSSet*)value_;
- (void)removeSets:(NSSet*)value_;
- (void)addSetsObject:(TrainingStrengthSetMO*)value_;
- (void)removeSetsObject:(TrainingStrengthSetMO*)value_;

@end

@interface _TrainingStrengthMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveResistance;
- (void)setPrimitiveResistance:(NSString*)value;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSMutableSet*)primitiveSets;
- (void)setPrimitiveSets:(NSMutableSet*)value;

@end
