// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrackingTypeMO.m instead.

#import "_TrackingTypeMO.h"

const struct TrackingTypeMOAttributes TrackingTypeMOAttributes = {
	.cloudId = @"cloudId",
	.loginId = @"loginId",
	.readOnly = @"readOnly",
	.sortOrder = @"sortOrder",
	.title = @"title",
	.view = @"view",
};

const struct TrackingTypeMORelationships TrackingTypeMORelationships = {
	.trackingValues = @"trackingValues",
};

@implementation TrackingTypeMOID
@end

@implementation _TrackingTypeMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TrackingType" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TrackingType";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TrackingType" inManagedObjectContext:moc_];
}

- (TrackingTypeMOID*)objectID {
	return (TrackingTypeMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"readOnlyValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"readOnly"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic readOnly;

- (BOOL)readOnlyValue {
	NSNumber *result = [self readOnly];
	return [result boolValue];
}

- (void)setReadOnlyValue:(BOOL)value_ {
	[self setReadOnly:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveReadOnlyValue {
	NSNumber *result = [self primitiveReadOnly];
	return [result boolValue];
}

- (void)setPrimitiveReadOnlyValue:(BOOL)value_ {
	[self setPrimitiveReadOnly:[NSNumber numberWithBool:value_]];
}

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic title;

@dynamic view;

@dynamic trackingValues;

- (NSMutableSet*)trackingValuesSet {
	[self willAccessValueForKey:@"trackingValues"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"trackingValues"];

	[self didAccessValueForKey:@"trackingValues"];
	return result;
}

@end

