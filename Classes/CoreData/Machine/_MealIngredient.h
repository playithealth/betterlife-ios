// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealIngredient.h instead.

#import <CoreData/CoreData.h>

extern const struct MealIngredientAttributes {
	__unsafe_unretained NSString *addToShoppingList;
	__unsafe_unretained NSString *answer;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *fullText;
	__unsafe_unretained NSString *measure;
	__unsafe_unretained NSString *measureUnits;
	__unsafe_unretained NSString *measureUnitsId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *productId;
	__unsafe_unretained NSString *shoppingListQuantity;
	__unsafe_unretained NSString *shoppingListText;
	__unsafe_unretained NSString *sortOrder;
	__unsafe_unretained NSString *status;
} MealIngredientAttributes;

extern const struct MealIngredientRelationships {
	__unsafe_unretained NSString *mealPlannerShoppingListItems;
	__unsafe_unretained NSString *recipe;
	__unsafe_unretained NSString *shoppingListCategory;
} MealIngredientRelationships;

@class MealPlannerShoppingListItem;
@class RecipeMO;
@class CategoryMO;

@interface MealIngredientID : NSManagedObjectID {}
@end

@interface _MealIngredient : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MealIngredientID* objectID;

@property (nonatomic, strong) NSNumber* addToShoppingList;

@property (atomic) int16_t addToShoppingListValue;
- (int16_t)addToShoppingListValue;
- (void)setAddToShoppingListValue:(int16_t)value_;

//- (BOOL)validateAddToShoppingList:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* answer;

@property (atomic) BOOL answerValue;
- (BOOL)answerValue;
- (void)setAnswerValue:(BOOL)value_;

//- (BOOL)validateAnswer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* fullText;

//- (BOOL)validateFullText:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* measure;

//- (BOOL)validateMeasure:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* measureUnits;

//- (BOOL)validateMeasureUnits:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* measureUnitsId;

@property (atomic) int32_t measureUnitsIdValue;
- (int32_t)measureUnitsIdValue;
- (void)setMeasureUnitsIdValue:(int32_t)value_;

//- (BOOL)validateMeasureUnitsId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* notes;

//- (BOOL)validateNotes:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* productId;

@property (atomic) int32_t productIdValue;
- (int32_t)productIdValue;
- (void)setProductIdValue:(int32_t)value_;

//- (BOOL)validateProductId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* shoppingListQuantity;

@property (atomic) int16_t shoppingListQuantityValue;
- (int16_t)shoppingListQuantityValue;
- (void)setShoppingListQuantityValue:(int16_t)value_;

//- (BOOL)validateShoppingListQuantity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* shoppingListText;

//- (BOOL)validateShoppingListText:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *mealPlannerShoppingListItems;

- (NSMutableSet*)mealPlannerShoppingListItemsSet;

@property (nonatomic, strong) RecipeMO *recipe;

//- (BOOL)validateRecipe:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CategoryMO *shoppingListCategory;

//- (BOOL)validateShoppingListCategory:(id*)value_ error:(NSError**)error_;

@end

@interface _MealIngredient (MealPlannerShoppingListItemsCoreDataGeneratedAccessors)
- (void)addMealPlannerShoppingListItems:(NSSet*)value_;
- (void)removeMealPlannerShoppingListItems:(NSSet*)value_;
- (void)addMealPlannerShoppingListItemsObject:(MealPlannerShoppingListItem*)value_;
- (void)removeMealPlannerShoppingListItemsObject:(MealPlannerShoppingListItem*)value_;

@end

@interface _MealIngredient (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAddToShoppingList;
- (void)setPrimitiveAddToShoppingList:(NSNumber*)value;

- (int16_t)primitiveAddToShoppingListValue;
- (void)setPrimitiveAddToShoppingListValue:(int16_t)value_;

- (NSNumber*)primitiveAnswer;
- (void)setPrimitiveAnswer:(NSNumber*)value;

- (BOOL)primitiveAnswerValue;
- (void)setPrimitiveAnswerValue:(BOOL)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSString*)primitiveFullText;
- (void)setPrimitiveFullText:(NSString*)value;

- (NSString*)primitiveMeasure;
- (void)setPrimitiveMeasure:(NSString*)value;

- (NSString*)primitiveMeasureUnits;
- (void)setPrimitiveMeasureUnits:(NSString*)value;

- (NSNumber*)primitiveMeasureUnitsId;
- (void)setPrimitiveMeasureUnitsId:(NSNumber*)value;

- (int32_t)primitiveMeasureUnitsIdValue;
- (void)setPrimitiveMeasureUnitsIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveNotes;
- (void)setPrimitiveNotes:(NSString*)value;

- (NSNumber*)primitiveProductId;
- (void)setPrimitiveProductId:(NSNumber*)value;

- (int32_t)primitiveProductIdValue;
- (void)setPrimitiveProductIdValue:(int32_t)value_;

- (NSNumber*)primitiveShoppingListQuantity;
- (void)setPrimitiveShoppingListQuantity:(NSNumber*)value;

- (int16_t)primitiveShoppingListQuantityValue;
- (void)setPrimitiveShoppingListQuantityValue:(int16_t)value_;

- (NSString*)primitiveShoppingListText;
- (void)setPrimitiveShoppingListText:(NSString*)value;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSMutableSet*)primitiveMealPlannerShoppingListItems;
- (void)setPrimitiveMealPlannerShoppingListItems:(NSMutableSet*)value;

- (RecipeMO*)primitiveRecipe;
- (void)setPrimitiveRecipe:(RecipeMO*)value;

- (CategoryMO*)primitiveShoppingListCategory;
- (void)setPrimitiveShoppingListCategory:(CategoryMO*)value;

@end
