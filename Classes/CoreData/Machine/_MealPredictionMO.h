// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealPredictionMO.h instead.

#import <CoreData/CoreData.h>

extern const struct MealPredictionMOAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *image_id;
	__unsafe_unretained NSString *item_id;
	__unsafe_unretained NSString *item_type;
	__unsafe_unretained NSString *logTime;
	__unsafe_unretained NSString *login_id;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *serving_size_text;
	__unsafe_unretained NSString *serving_size_uom;
	__unsafe_unretained NSString *servings;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
	__unsafe_unretained NSString *usageCount;
} MealPredictionMOAttributes;

extern const struct MealPredictionMORelationships {
	__unsafe_unretained NSString *customFood;
	__unsafe_unretained NSString *entree;
	__unsafe_unretained NSString *nutrition;
	__unsafe_unretained NSString *recipe;
	__unsafe_unretained NSString *sides;
	__unsafe_unretained NSString *tag;
} MealPredictionMORelationships;

@class CustomFoodMO;
@class MealPredictionMO;
@class NutritionMO;
@class RecipeMO;
@class MealPredictionMO;
@class AdvisorMealPlanTagMO;

@interface MealPredictionMOID : NSManagedObjectID {}
@end

@interface _MealPredictionMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MealPredictionMOID* objectID;

@property (nonatomic, strong) NSString* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* image_id;

@property (atomic) int32_t image_idValue;
- (int32_t)image_idValue;
- (void)setImage_idValue:(int32_t)value_;

//- (BOOL)validateImage_id:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* item_id;

@property (atomic) int32_t item_idValue;
- (int32_t)item_idValue;
- (void)setItem_idValue:(int32_t)value_;

//- (BOOL)validateItem_id:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* item_type;

//- (BOOL)validateItem_type:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* logTime;

@property (atomic) int16_t logTimeValue;
- (int16_t)logTimeValue;
- (void)setLogTimeValue:(int16_t)value_;

//- (BOOL)validateLogTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* login_id;

@property (atomic) int16_t login_idValue;
- (int16_t)login_idValue;
- (void)setLogin_idValue:(int16_t)value_;

//- (BOOL)validateLogin_id:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* serving_size_text;

//- (BOOL)validateServing_size_text:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* serving_size_uom;

//- (BOOL)validateServing_size_uom:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* servings;

@property (atomic) double servingsValue;
- (double)servingsValue;
- (void)setServingsValue:(double)value_;

//- (BOOL)validateServings:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* usageCount;

@property (atomic) int16_t usageCountValue;
- (int16_t)usageCountValue;
- (void)setUsageCountValue:(int16_t)value_;

//- (BOOL)validateUsageCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CustomFoodMO *customFood;

//- (BOOL)validateCustomFood:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MealPredictionMO *entree;

//- (BOOL)validateEntree:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NutritionMO *nutrition;

//- (BOOL)validateNutrition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) RecipeMO *recipe;

//- (BOOL)validateRecipe:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *sides;

- (NSMutableSet*)sidesSet;

@property (nonatomic, strong) AdvisorMealPlanTagMO *tag;

//- (BOOL)validateTag:(id*)value_ error:(NSError**)error_;

@end

@interface _MealPredictionMO (SidesCoreDataGeneratedAccessors)
- (void)addSides:(NSSet*)value_;
- (void)removeSides:(NSSet*)value_;
- (void)addSidesObject:(MealPredictionMO*)value_;
- (void)removeSidesObject:(MealPredictionMO*)value_;

@end

@interface _MealPredictionMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveDate;
- (void)setPrimitiveDate:(NSString*)value;

- (NSNumber*)primitiveImage_id;
- (void)setPrimitiveImage_id:(NSNumber*)value;

- (int32_t)primitiveImage_idValue;
- (void)setPrimitiveImage_idValue:(int32_t)value_;

- (NSNumber*)primitiveItem_id;
- (void)setPrimitiveItem_id:(NSNumber*)value;

- (int32_t)primitiveItem_idValue;
- (void)setPrimitiveItem_idValue:(int32_t)value_;

- (NSString*)primitiveItem_type;
- (void)setPrimitiveItem_type:(NSString*)value;

- (NSNumber*)primitiveLogTime;
- (void)setPrimitiveLogTime:(NSNumber*)value;

- (int16_t)primitiveLogTimeValue;
- (void)setPrimitiveLogTimeValue:(int16_t)value_;

- (NSNumber*)primitiveLogin_id;
- (void)setPrimitiveLogin_id:(NSNumber*)value;

- (int16_t)primitiveLogin_idValue;
- (void)setPrimitiveLogin_idValue:(int16_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveServing_size_text;
- (void)setPrimitiveServing_size_text:(NSString*)value;

- (NSString*)primitiveServing_size_uom;
- (void)setPrimitiveServing_size_uom:(NSString*)value;

- (NSNumber*)primitiveServings;
- (void)setPrimitiveServings:(NSNumber*)value;

- (double)primitiveServingsValue;
- (void)setPrimitiveServingsValue:(double)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveUsageCount;
- (void)setPrimitiveUsageCount:(NSNumber*)value;

- (int16_t)primitiveUsageCountValue;
- (void)setPrimitiveUsageCountValue:(int16_t)value_;

- (CustomFoodMO*)primitiveCustomFood;
- (void)setPrimitiveCustomFood:(CustomFoodMO*)value;

- (MealPredictionMO*)primitiveEntree;
- (void)setPrimitiveEntree:(MealPredictionMO*)value;

- (NutritionMO*)primitiveNutrition;
- (void)setPrimitiveNutrition:(NutritionMO*)value;

- (RecipeMO*)primitiveRecipe;
- (void)setPrimitiveRecipe:(RecipeMO*)value;

- (NSMutableSet*)primitiveSides;
- (void)setPrimitiveSides:(NSMutableSet*)value;

- (AdvisorMealPlanTagMO*)primitiveTag;
- (void)setPrimitiveTag:(AdvisorMealPlanTagMO*)value;

@end
