// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserAchievement.h instead.

#import <CoreData/CoreData.h>

extern const struct UserAchievementAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *loginId;
} UserAchievementAttributes;

extern const struct UserAchievementRelationships {
	__unsafe_unretained NSString *achievement;
} UserAchievementRelationships;

@class Achievement;

@interface UserAchievementID : NSManagedObjectID {}
@end

@interface _UserAchievement : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserAchievementID* objectID;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Achievement *achievement;

//- (BOOL)validateAchievement:(id*)value_ error:(NSError**)error_;

@end

@interface _UserAchievement (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (Achievement*)primitiveAchievement;
- (void)setPrimitiveAchievement:(Achievement*)value;

@end
