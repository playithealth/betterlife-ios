// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitMO.h instead.

#import <CoreData/CoreData.h>

extern const struct UnitMOAttributes {
	__unsafe_unretained NSString *abbrev;
	__unsafe_unretained NSString *plural;
	__unsafe_unretained NSString *singular;
	__unsafe_unretained NSString *sortOrder;
} UnitMOAttributes;

@interface UnitMOID : NSManagedObjectID {}
@end

@interface _UnitMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UnitMOID* objectID;

@property (nonatomic, strong) NSString* abbrev;

//- (BOOL)validateAbbrev:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* plural;

//- (BOOL)validatePlural:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* singular;

//- (BOOL)validateSingular:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@end

@interface _UnitMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAbbrev;
- (void)setPrimitiveAbbrev:(NSString*)value;

- (NSString*)primitivePlural;
- (void)setPrimitivePlural:(NSString*)value;

- (NSString*)primitiveSingular;
- (void)setPrimitiveSingular:(NSString*)value;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

@end
