// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CoachInviteMO.h instead.

#import <CoreData/CoreData.h>

extern const struct CoachInviteMOAttributes {
	__unsafe_unretained NSString *advisorId;
	__unsafe_unretained NSString *advisorName;
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *createdOn;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *rootEntityName;
	__unsafe_unretained NSString *status;
} CoachInviteMOAttributes;

@interface CoachInviteMOID : NSManagedObjectID {}
@end

@interface _CoachInviteMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CoachInviteMOID* objectID;

@property (nonatomic, strong) NSNumber* advisorId;

@property (atomic) int32_t advisorIdValue;
- (int32_t)advisorIdValue;
- (void)setAdvisorIdValue:(int32_t)value_;

//- (BOOL)validateAdvisorId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* advisorName;

//- (BOOL)validateAdvisorName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* code;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* createdOn;

@property (atomic) int32_t createdOnValue;
- (int32_t)createdOnValue;
- (void)setCreatedOnValue:(int32_t)value_;

//- (BOOL)validateCreatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* rootEntityName;

//- (BOOL)validateRootEntityName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@end

@interface _CoachInviteMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAdvisorId;
- (void)setPrimitiveAdvisorId:(NSNumber*)value;

- (int32_t)primitiveAdvisorIdValue;
- (void)setPrimitiveAdvisorIdValue:(int32_t)value_;

- (NSString*)primitiveAdvisorName;
- (void)setPrimitiveAdvisorName:(NSString*)value;

- (NSString*)primitiveCode;
- (void)setPrimitiveCode:(NSString*)value;

- (NSNumber*)primitiveCreatedOn;
- (void)setPrimitiveCreatedOn:(NSNumber*)value;

- (int32_t)primitiveCreatedOnValue;
- (void)setPrimitiveCreatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveRootEntityName;
- (void)setPrimitiveRootEntityName:(NSString*)value;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

@end
