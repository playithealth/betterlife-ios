// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Achievement.m instead.

#import "_Achievement.h"

const struct AchievementAttributes AchievementAttributes = {
	.cloudId = @"cloudId",
	.image = @"image",
	.message = @"message",
	.name = @"name",
};

const struct AchievementRelationships AchievementRelationships = {
	.userAchievements = @"userAchievements",
};

@implementation AchievementID
@end

@implementation _Achievement

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Achievement" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Achievement";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Achievement" inManagedObjectContext:moc_];
}

- (AchievementID*)objectID {
	return (AchievementID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic image;

@dynamic message;

@dynamic name;

@dynamic userAchievements;

- (NSMutableSet*)userAchievementsSet {
	[self willAccessValueForKey:@"userAchievements"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userAchievements"];

	[self didAccessValueForKey:@"userAchievements"];
	return result;
}

@end

