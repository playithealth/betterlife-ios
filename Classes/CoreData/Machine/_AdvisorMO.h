// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMO.h instead.

#import <CoreData/CoreData.h>

extern const struct AdvisorMOAttributes {
	__unsafe_unretained NSString *advisorId;
	__unsafe_unretained NSString *canAddToShoppingList;
	__unsafe_unretained NSString *canDeleteFromShoppingList;
	__unsafe_unretained NSString *canMessage;
	__unsafe_unretained NSString *canRecommendExercise;
	__unsafe_unretained NSString *canRecommendMealPlans;
	__unsafe_unretained NSString *canRecommendMeals;
	__unsafe_unretained NSString *canViewExercise;
	__unsafe_unretained NSString *canViewMeals;
	__unsafe_unretained NSString *canViewNutrition;
	__unsafe_unretained NSString *canViewPantry;
	__unsafe_unretained NSString *canViewShoppingList;
	__unsafe_unretained NSString *confirmation;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
} AdvisorMOAttributes;

extern const struct AdvisorMORelationships {
	__unsafe_unretained NSString *groups;
} AdvisorMORelationships;

@class AdvisorGroup;

@interface AdvisorMOID : NSManagedObjectID {}
@end

@interface _AdvisorMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorMOID* objectID;

@property (nonatomic, strong) NSNumber* advisorId;

@property (atomic) int32_t advisorIdValue;
- (int32_t)advisorIdValue;
- (void)setAdvisorIdValue:(int32_t)value_;

//- (BOOL)validateAdvisorId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canAddToShoppingList;

@property (atomic) BOOL canAddToShoppingListValue;
- (BOOL)canAddToShoppingListValue;
- (void)setCanAddToShoppingListValue:(BOOL)value_;

//- (BOOL)validateCanAddToShoppingList:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canDeleteFromShoppingList;

@property (atomic) BOOL canDeleteFromShoppingListValue;
- (BOOL)canDeleteFromShoppingListValue;
- (void)setCanDeleteFromShoppingListValue:(BOOL)value_;

//- (BOOL)validateCanDeleteFromShoppingList:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canMessage;

@property (atomic) BOOL canMessageValue;
- (BOOL)canMessageValue;
- (void)setCanMessageValue:(BOOL)value_;

//- (BOOL)validateCanMessage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canRecommendExercise;

@property (atomic) BOOL canRecommendExerciseValue;
- (BOOL)canRecommendExerciseValue;
- (void)setCanRecommendExerciseValue:(BOOL)value_;

//- (BOOL)validateCanRecommendExercise:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canRecommendMealPlans;

@property (atomic) BOOL canRecommendMealPlansValue;
- (BOOL)canRecommendMealPlansValue;
- (void)setCanRecommendMealPlansValue:(BOOL)value_;

//- (BOOL)validateCanRecommendMealPlans:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canRecommendMeals;

@property (atomic) BOOL canRecommendMealsValue;
- (BOOL)canRecommendMealsValue;
- (void)setCanRecommendMealsValue:(BOOL)value_;

//- (BOOL)validateCanRecommendMeals:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canViewExercise;

@property (atomic) BOOL canViewExerciseValue;
- (BOOL)canViewExerciseValue;
- (void)setCanViewExerciseValue:(BOOL)value_;

//- (BOOL)validateCanViewExercise:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canViewMeals;

@property (atomic) BOOL canViewMealsValue;
- (BOOL)canViewMealsValue;
- (void)setCanViewMealsValue:(BOOL)value_;

//- (BOOL)validateCanViewMeals:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canViewNutrition;

@property (atomic) BOOL canViewNutritionValue;
- (BOOL)canViewNutritionValue;
- (void)setCanViewNutritionValue:(BOOL)value_;

//- (BOOL)validateCanViewNutrition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canViewPantry;

@property (atomic) BOOL canViewPantryValue;
- (BOOL)canViewPantryValue;
- (void)setCanViewPantryValue:(BOOL)value_;

//- (BOOL)validateCanViewPantry:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* canViewShoppingList;

@property (atomic) BOOL canViewShoppingListValue;
- (BOOL)canViewShoppingListValue;
- (void)setCanViewShoppingListValue:(BOOL)value_;

//- (BOOL)validateCanViewShoppingList:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* confirmation;

//- (BOOL)validateConfirmation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *groups;

- (NSMutableSet*)groupsSet;

@end

@interface _AdvisorMO (GroupsCoreDataGeneratedAccessors)
- (void)addGroups:(NSSet*)value_;
- (void)removeGroups:(NSSet*)value_;
- (void)addGroupsObject:(AdvisorGroup*)value_;
- (void)removeGroupsObject:(AdvisorGroup*)value_;

@end

@interface _AdvisorMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAdvisorId;
- (void)setPrimitiveAdvisorId:(NSNumber*)value;

- (int32_t)primitiveAdvisorIdValue;
- (void)setPrimitiveAdvisorIdValue:(int32_t)value_;

- (NSNumber*)primitiveCanAddToShoppingList;
- (void)setPrimitiveCanAddToShoppingList:(NSNumber*)value;

- (BOOL)primitiveCanAddToShoppingListValue;
- (void)setPrimitiveCanAddToShoppingListValue:(BOOL)value_;

- (NSNumber*)primitiveCanDeleteFromShoppingList;
- (void)setPrimitiveCanDeleteFromShoppingList:(NSNumber*)value;

- (BOOL)primitiveCanDeleteFromShoppingListValue;
- (void)setPrimitiveCanDeleteFromShoppingListValue:(BOOL)value_;

- (NSNumber*)primitiveCanMessage;
- (void)setPrimitiveCanMessage:(NSNumber*)value;

- (BOOL)primitiveCanMessageValue;
- (void)setPrimitiveCanMessageValue:(BOOL)value_;

- (NSNumber*)primitiveCanRecommendExercise;
- (void)setPrimitiveCanRecommendExercise:(NSNumber*)value;

- (BOOL)primitiveCanRecommendExerciseValue;
- (void)setPrimitiveCanRecommendExerciseValue:(BOOL)value_;

- (NSNumber*)primitiveCanRecommendMealPlans;
- (void)setPrimitiveCanRecommendMealPlans:(NSNumber*)value;

- (BOOL)primitiveCanRecommendMealPlansValue;
- (void)setPrimitiveCanRecommendMealPlansValue:(BOOL)value_;

- (NSNumber*)primitiveCanRecommendMeals;
- (void)setPrimitiveCanRecommendMeals:(NSNumber*)value;

- (BOOL)primitiveCanRecommendMealsValue;
- (void)setPrimitiveCanRecommendMealsValue:(BOOL)value_;

- (NSNumber*)primitiveCanViewExercise;
- (void)setPrimitiveCanViewExercise:(NSNumber*)value;

- (BOOL)primitiveCanViewExerciseValue;
- (void)setPrimitiveCanViewExerciseValue:(BOOL)value_;

- (NSNumber*)primitiveCanViewMeals;
- (void)setPrimitiveCanViewMeals:(NSNumber*)value;

- (BOOL)primitiveCanViewMealsValue;
- (void)setPrimitiveCanViewMealsValue:(BOOL)value_;

- (NSNumber*)primitiveCanViewNutrition;
- (void)setPrimitiveCanViewNutrition:(NSNumber*)value;

- (BOOL)primitiveCanViewNutritionValue;
- (void)setPrimitiveCanViewNutritionValue:(BOOL)value_;

- (NSNumber*)primitiveCanViewPantry;
- (void)setPrimitiveCanViewPantry:(NSNumber*)value;

- (BOOL)primitiveCanViewPantryValue;
- (void)setPrimitiveCanViewPantryValue:(BOOL)value_;

- (NSNumber*)primitiveCanViewShoppingList;
- (void)setPrimitiveCanViewShoppingList:(NSNumber*)value;

- (BOOL)primitiveCanViewShoppingListValue;
- (void)setPrimitiveCanViewShoppingListValue:(BOOL)value_;

- (NSString*)primitiveConfirmation;
- (void)setPrimitiveConfirmation:(NSString*)value;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSMutableSet*)primitiveGroups;
- (void)setPrimitiveGroups:(NSMutableSet*)value;

@end
