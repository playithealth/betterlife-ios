// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanWorkoutActivitySetMO.m instead.

#import "_AdvisorActivityPlanWorkoutActivitySetMO.h"

@implementation AdvisorActivityPlanWorkoutActivitySetMOID
@end

@implementation _AdvisorActivityPlanWorkoutActivitySetMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorActivityPlanWorkoutActivitySet" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorActivityPlanWorkoutActivitySet";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorActivityPlanWorkoutActivitySet" inManagedObjectContext:moc_];
}

- (AdvisorActivityPlanWorkoutActivitySetMOID*)objectID {
	return (AdvisorActivityPlanWorkoutActivitySetMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@end

