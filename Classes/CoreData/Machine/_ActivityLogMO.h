// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityLogMO.h instead.

#import <CoreData/CoreData.h>
#import "ActivityRecordMO.h"

extern const struct ActivityLogMOAttributes {
	__unsafe_unretained NSString *elapsedDuration;
} ActivityLogMOAttributes;

@interface ActivityLogMOID : ActivityRecordMOID {}
@end

@interface _ActivityLogMO : ActivityRecordMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ActivityLogMOID* objectID;

@property (nonatomic, strong) NSNumber* elapsedDuration;

@property (atomic) double elapsedDurationValue;
- (double)elapsedDurationValue;
- (void)setElapsedDurationValue:(double)value_;

//- (BOOL)validateElapsedDuration:(id*)value_ error:(NSError**)error_;

@end

@interface _ActivityLogMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveElapsedDuration;
- (void)setPrimitiveElapsedDuration:(NSNumber*)value;

- (double)primitiveElapsedDurationValue;
- (void)setPrimitiveElapsedDurationValue:(double)value_;

@end
