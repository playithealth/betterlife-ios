// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserPermissionMO.m instead.

#import "_UserPermissionMO.h"

const struct UserPermissionMOAttributes UserPermissionMOAttributes = {
	.enabled = @"enabled",
	.name = @"name",
};

const struct UserPermissionMORelationships UserPermissionMORelationships = {
	.user = @"user",
};

@implementation UserPermissionMOID
@end

@implementation _UserPermissionMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserPermission" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserPermission";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserPermission" inManagedObjectContext:moc_];
}

- (UserPermissionMOID*)objectID {
	return (UserPermissionMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"enabledValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"enabled"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic enabled;

- (BOOL)enabledValue {
	NSNumber *result = [self enabled];
	return [result boolValue];
}

- (void)setEnabledValue:(BOOL)value_ {
	[self setEnabled:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveEnabledValue {
	NSNumber *result = [self primitiveEnabled];
	return [result boolValue];
}

- (void)setPrimitiveEnabledValue:(BOOL)value_ {
	[self setPrimitiveEnabled:[NSNumber numberWithBool:value_]];
}

@dynamic name;

@dynamic user;

@end

