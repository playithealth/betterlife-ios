// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrackingValueMO.h instead.

#import <CoreData/CoreData.h>

extern const struct TrackingValueMOAttributes {
	__unsafe_unretained NSString *dataType;
	__unsafe_unretained NSString *defaultValue;
	__unsafe_unretained NSString *field;
	__unsafe_unretained NSString *optional;
	__unsafe_unretained NSString *precision;
	__unsafe_unretained NSString *rangeMax;
	__unsafe_unretained NSString *rangeMin;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *unitAbbrev;
	__unsafe_unretained NSString *units;
} TrackingValueMOAttributes;

extern const struct TrackingValueMORelationships {
	__unsafe_unretained NSString *trackingType;
} TrackingValueMORelationships;

@class TrackingTypeMO;

@interface TrackingValueMOID : NSManagedObjectID {}
@end

@interface _TrackingValueMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TrackingValueMOID* objectID;

@property (nonatomic, strong) NSString* dataType;

//- (BOOL)validateDataType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* defaultValue;

@property (atomic) double defaultValueValue;
- (double)defaultValueValue;
- (void)setDefaultValueValue:(double)value_;

//- (BOOL)validateDefaultValue:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* field;

//- (BOOL)validateField:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* optional;

@property (atomic) BOOL optionalValue;
- (BOOL)optionalValue;
- (void)setOptionalValue:(BOOL)value_;

//- (BOOL)validateOptional:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* precision;

@property (atomic) int16_t precisionValue;
- (int16_t)precisionValue;
- (void)setPrecisionValue:(int16_t)value_;

//- (BOOL)validatePrecision:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rangeMax;

@property (atomic) double rangeMaxValue;
- (double)rangeMaxValue;
- (void)setRangeMaxValue:(double)value_;

//- (BOOL)validateRangeMax:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rangeMin;

@property (atomic) double rangeMinValue;
- (double)rangeMinValue;
- (void)setRangeMinValue:(double)value_;

//- (BOOL)validateRangeMin:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* unitAbbrev;

//- (BOOL)validateUnitAbbrev:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* units;

//- (BOOL)validateUnits:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TrackingTypeMO *trackingType;

//- (BOOL)validateTrackingType:(id*)value_ error:(NSError**)error_;

@end

@interface _TrackingValueMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveDataType;
- (void)setPrimitiveDataType:(NSString*)value;

- (NSNumber*)primitiveDefaultValue;
- (void)setPrimitiveDefaultValue:(NSNumber*)value;

- (double)primitiveDefaultValueValue;
- (void)setPrimitiveDefaultValueValue:(double)value_;

- (NSString*)primitiveField;
- (void)setPrimitiveField:(NSString*)value;

- (NSNumber*)primitiveOptional;
- (void)setPrimitiveOptional:(NSNumber*)value;

- (BOOL)primitiveOptionalValue;
- (void)setPrimitiveOptionalValue:(BOOL)value_;

- (NSNumber*)primitivePrecision;
- (void)setPrimitivePrecision:(NSNumber*)value;

- (int16_t)primitivePrecisionValue;
- (void)setPrimitivePrecisionValue:(int16_t)value_;

- (NSNumber*)primitiveRangeMax;
- (void)setPrimitiveRangeMax:(NSNumber*)value;

- (double)primitiveRangeMaxValue;
- (void)setPrimitiveRangeMaxValue:(double)value_;

- (NSNumber*)primitiveRangeMin;
- (void)setPrimitiveRangeMin:(NSNumber*)value;

- (double)primitiveRangeMinValue;
- (void)setPrimitiveRangeMinValue:(double)value_;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (NSString*)primitiveUnitAbbrev;
- (void)setPrimitiveUnitAbbrev:(NSString*)value;

- (NSString*)primitiveUnits;
- (void)setPrimitiveUnits:(NSString*)value;

- (TrackingTypeMO*)primitiveTrackingType;
- (void)setPrimitiveTrackingType:(TrackingTypeMO*)value;

@end
