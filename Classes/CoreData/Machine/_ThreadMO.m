// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ThreadMO.m instead.

#import "_ThreadMO.h"

const struct ThreadMOAttributes ThreadMOAttributes = {
	.cloudId = @"cloudId",
	.createdOn = @"createdOn",
	.name = @"name",
	.readOn = @"readOn",
	.status = @"status",
	.threadUpdatedOn = @"threadUpdatedOn",
	.type = @"type",
	.unsubscribed = @"unsubscribed",
	.updatedOn = @"updatedOn",
};

const struct ThreadMORelationships ThreadMORelationships = {
	.community = @"community",
	.messages = @"messages",
	.userProfile = @"userProfile",
};

@implementation ThreadMOID
@end

@implementation _ThreadMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Thread" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Thread";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Thread" inManagedObjectContext:moc_];
}

- (ThreadMOID*)objectID {
	return (ThreadMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"createdOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"createdOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"readOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"readOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"threadUpdatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"threadUpdatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"unsubscribedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"unsubscribed"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic createdOn;

- (int32_t)createdOnValue {
	NSNumber *result = [self createdOn];
	return [result intValue];
}

- (void)setCreatedOnValue:(int32_t)value_ {
	[self setCreatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCreatedOnValue {
	NSNumber *result = [self primitiveCreatedOn];
	return [result intValue];
}

- (void)setPrimitiveCreatedOnValue:(int32_t)value_ {
	[self setPrimitiveCreatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic readOn;

- (int32_t)readOnValue {
	NSNumber *result = [self readOn];
	return [result intValue];
}

- (void)setReadOnValue:(int32_t)value_ {
	[self setReadOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveReadOnValue {
	NSNumber *result = [self primitiveReadOn];
	return [result intValue];
}

- (void)setPrimitiveReadOnValue:(int32_t)value_ {
	[self setPrimitiveReadOn:[NSNumber numberWithInt:value_]];
}

@dynamic status;

@dynamic threadUpdatedOn;

- (int32_t)threadUpdatedOnValue {
	NSNumber *result = [self threadUpdatedOn];
	return [result intValue];
}

- (void)setThreadUpdatedOnValue:(int32_t)value_ {
	[self setThreadUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveThreadUpdatedOnValue {
	NSNumber *result = [self primitiveThreadUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveThreadUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveThreadUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic type;

@dynamic unsubscribed;

- (BOOL)unsubscribedValue {
	NSNumber *result = [self unsubscribed];
	return [result boolValue];
}

- (void)setUnsubscribedValue:(BOOL)value_ {
	[self setUnsubscribed:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUnsubscribedValue {
	NSNumber *result = [self primitiveUnsubscribed];
	return [result boolValue];
}

- (void)setPrimitiveUnsubscribedValue:(BOOL)value_ {
	[self setPrimitiveUnsubscribed:[NSNumber numberWithBool:value_]];
}

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic community;

@dynamic messages;

- (NSMutableSet*)messagesSet {
	[self willAccessValueForKey:@"messages"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"messages"];

	[self didAccessValueForKey:@"messages"];
	return result;
}

@dynamic userProfile;

@end

