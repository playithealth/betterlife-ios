// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrackingMO.m instead.

#import "_TrackingMO.h"

const struct TrackingMOAttributes TrackingMOAttributes = {
	.cloudId = @"cloudId",
	.date = @"date",
	.loginId = @"loginId",
	.source = @"source",
	.status = @"status",
	.trackingType = @"trackingType",
	.updatedOn = @"updatedOn",
	.valueJSON = @"valueJSON",
};

const struct TrackingMORelationships TrackingMORelationships = {
	.trainingAssessment = @"trainingAssessment",
};

@implementation TrackingMOID
@end

@implementation _TrackingMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Tracking" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Tracking";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Tracking" inManagedObjectContext:moc_];
}

- (TrackingMOID*)objectID {
	return (TrackingMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sourceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"source"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"trackingTypeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"trackingType"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic date;

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic source;

- (int32_t)sourceValue {
	NSNumber *result = [self source];
	return [result intValue];
}

- (void)setSourceValue:(int32_t)value_ {
	[self setSource:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSourceValue {
	NSNumber *result = [self primitiveSource];
	return [result intValue];
}

- (void)setPrimitiveSourceValue:(int32_t)value_ {
	[self setPrimitiveSource:[NSNumber numberWithInt:value_]];
}

@dynamic status;

@dynamic trackingType;

- (int16_t)trackingTypeValue {
	NSNumber *result = [self trackingType];
	return [result shortValue];
}

- (void)setTrackingTypeValue:(int16_t)value_ {
	[self setTrackingType:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveTrackingTypeValue {
	NSNumber *result = [self primitiveTrackingType];
	return [result shortValue];
}

- (void)setPrimitiveTrackingTypeValue:(int16_t)value_ {
	[self setPrimitiveTrackingType:[NSNumber numberWithShort:value_]];
}

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic valueJSON;

@dynamic trainingAssessment;

@end

