// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanPhaseMO.h instead.

#import <CoreData/CoreData.h>
#import "AdvisorPlanPhaseMO.h"

extern const struct AdvisorMealPlanPhaseMOAttributes {
	__unsafe_unretained NSString *bmr;
	__unsafe_unretained NSString *proteinRatio;
	__unsafe_unretained NSString *totalCarbohydrateRatio;
	__unsafe_unretained NSString *totalFatRatio;
	__unsafe_unretained NSString *useAutoRatios;
} AdvisorMealPlanPhaseMOAttributes;

extern const struct AdvisorMealPlanPhaseMORelationships {
	__unsafe_unretained NSString *entries;
	__unsafe_unretained NSString *maxNutrition;
	__unsafe_unretained NSString *minNutrition;
} AdvisorMealPlanPhaseMORelationships;

@class AdvisorMealPlanEntryMO;
@class NutritionMO;
@class NutritionMO;

@interface AdvisorMealPlanPhaseMOID : AdvisorPlanPhaseMOID {}
@end

@interface _AdvisorMealPlanPhaseMO : AdvisorPlanPhaseMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorMealPlanPhaseMOID* objectID;

@property (nonatomic, strong) NSNumber* bmr;

@property (atomic) double bmrValue;
- (double)bmrValue;
- (void)setBmrValue:(double)value_;

//- (BOOL)validateBmr:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* proteinRatio;

@property (atomic) int16_t proteinRatioValue;
- (int16_t)proteinRatioValue;
- (void)setProteinRatioValue:(int16_t)value_;

//- (BOOL)validateProteinRatio:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* totalCarbohydrateRatio;

@property (atomic) int16_t totalCarbohydrateRatioValue;
- (int16_t)totalCarbohydrateRatioValue;
- (void)setTotalCarbohydrateRatioValue:(int16_t)value_;

//- (BOOL)validateTotalCarbohydrateRatio:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* totalFatRatio;

@property (atomic) int16_t totalFatRatioValue;
- (int16_t)totalFatRatioValue;
- (void)setTotalFatRatioValue:(int16_t)value_;

//- (BOOL)validateTotalFatRatio:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* useAutoRatios;

@property (atomic) BOOL useAutoRatiosValue;
- (BOOL)useAutoRatiosValue;
- (void)setUseAutoRatiosValue:(BOOL)value_;

//- (BOOL)validateUseAutoRatios:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *entries;

- (NSMutableSet*)entriesSet;

@property (nonatomic, strong) NutritionMO *maxNutrition;

//- (BOOL)validateMaxNutrition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NutritionMO *minNutrition;

//- (BOOL)validateMinNutrition:(id*)value_ error:(NSError**)error_;

@end

@interface _AdvisorMealPlanPhaseMO (EntriesCoreDataGeneratedAccessors)
- (void)addEntries:(NSSet*)value_;
- (void)removeEntries:(NSSet*)value_;
- (void)addEntriesObject:(AdvisorMealPlanEntryMO*)value_;
- (void)removeEntriesObject:(AdvisorMealPlanEntryMO*)value_;

@end

@interface _AdvisorMealPlanPhaseMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveBmr;
- (void)setPrimitiveBmr:(NSNumber*)value;

- (double)primitiveBmrValue;
- (void)setPrimitiveBmrValue:(double)value_;

- (NSNumber*)primitiveProteinRatio;
- (void)setPrimitiveProteinRatio:(NSNumber*)value;

- (int16_t)primitiveProteinRatioValue;
- (void)setPrimitiveProteinRatioValue:(int16_t)value_;

- (NSNumber*)primitiveTotalCarbohydrateRatio;
- (void)setPrimitiveTotalCarbohydrateRatio:(NSNumber*)value;

- (int16_t)primitiveTotalCarbohydrateRatioValue;
- (void)setPrimitiveTotalCarbohydrateRatioValue:(int16_t)value_;

- (NSNumber*)primitiveTotalFatRatio;
- (void)setPrimitiveTotalFatRatio:(NSNumber*)value;

- (int16_t)primitiveTotalFatRatioValue;
- (void)setPrimitiveTotalFatRatioValue:(int16_t)value_;

- (NSNumber*)primitiveUseAutoRatios;
- (void)setPrimitiveUseAutoRatios:(NSNumber*)value;

- (BOOL)primitiveUseAutoRatiosValue;
- (void)setPrimitiveUseAutoRatiosValue:(BOOL)value_;

- (NSMutableSet*)primitiveEntries;
- (void)setPrimitiveEntries:(NSMutableSet*)value;

- (NutritionMO*)primitiveMaxNutrition;
- (void)setPrimitiveMaxNutrition:(NutritionMO*)value;

- (NutritionMO*)primitiveMinNutrition;
- (void)setPrimitiveMinNutrition:(NutritionMO*)value;

@end
