// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivitySetMO.h instead.

#import <CoreData/CoreData.h>

extern const struct ActivitySetMOAttributes {
	__unsafe_unretained NSString *activityType;
	__unsafe_unretained NSString *calories;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *factor;
	__unsafe_unretained NSString *heartZone;
	__unsafe_unretained NSString *miles;
	__unsafe_unretained NSString *mph;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *pounds;
	__unsafe_unretained NSString *reps;
	__unsafe_unretained NSString *seconds;
	__unsafe_unretained NSString *sortOrder;
	__unsafe_unretained NSString *watts;
} ActivitySetMOAttributes;

extern const struct ActivitySetMORelationships {
	__unsafe_unretained NSString *activity;
	__unsafe_unretained NSString *basedOnSet;
	__unsafe_unretained NSString *derivedSets;
} ActivitySetMORelationships;

@class ActivityMO;
@class ActivitySetMO;
@class ActivitySetMO;

@interface ActivitySetMOID : NSManagedObjectID {}
@end

@interface _ActivitySetMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ActivitySetMOID* objectID;

@property (nonatomic, strong) NSNumber* activityType;

@property (atomic) int16_t activityTypeValue;
- (int16_t)activityTypeValue;
- (void)setActivityTypeValue:(int16_t)value_;

//- (BOOL)validateActivityType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* calories;

@property (atomic) double caloriesValue;
- (double)caloriesValue;
- (void)setCaloriesValue:(double)value_;

//- (BOOL)validateCalories:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* factor;

@property (atomic) double factorValue;
- (double)factorValue;
- (void)setFactorValue:(double)value_;

//- (BOOL)validateFactor:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* heartZone;

@property (atomic) int16_t heartZoneValue;
- (int16_t)heartZoneValue;
- (void)setHeartZoneValue:(int16_t)value_;

//- (BOOL)validateHeartZone:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* miles;

@property (atomic) double milesValue;
- (double)milesValue;
- (void)setMilesValue:(double)value_;

//- (BOOL)validateMiles:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* mph;

@property (atomic) double mphValue;
- (double)mphValue;
- (void)setMphValue:(double)value_;

//- (BOOL)validateMph:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* pounds;

@property (atomic) double poundsValue;
- (double)poundsValue;
- (void)setPoundsValue:(double)value_;

//- (BOOL)validatePounds:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* reps;

@property (atomic) int16_t repsValue;
- (int16_t)repsValue;
- (void)setRepsValue:(int16_t)value_;

//- (BOOL)validateReps:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* seconds;

@property (atomic) int16_t secondsValue;
- (int16_t)secondsValue;
- (void)setSecondsValue:(int16_t)value_;

//- (BOOL)validateSeconds:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* watts;

@property (atomic) int16_t wattsValue;
- (int16_t)wattsValue;
- (void)setWattsValue:(int16_t)value_;

//- (BOOL)validateWatts:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) ActivityMO *activity;

//- (BOOL)validateActivity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) ActivitySetMO *basedOnSet;

//- (BOOL)validateBasedOnSet:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *derivedSets;

- (NSMutableSet*)derivedSetsSet;

@end

@interface _ActivitySetMO (DerivedSetsCoreDataGeneratedAccessors)
- (void)addDerivedSets:(NSSet*)value_;
- (void)removeDerivedSets:(NSSet*)value_;
- (void)addDerivedSetsObject:(ActivitySetMO*)value_;
- (void)removeDerivedSetsObject:(ActivitySetMO*)value_;

@end

@interface _ActivitySetMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveActivityType;
- (void)setPrimitiveActivityType:(NSNumber*)value;

- (int16_t)primitiveActivityTypeValue;
- (void)setPrimitiveActivityTypeValue:(int16_t)value_;

- (NSNumber*)primitiveCalories;
- (void)setPrimitiveCalories:(NSNumber*)value;

- (double)primitiveCaloriesValue;
- (void)setPrimitiveCaloriesValue:(double)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveFactor;
- (void)setPrimitiveFactor:(NSNumber*)value;

- (double)primitiveFactorValue;
- (void)setPrimitiveFactorValue:(double)value_;

- (NSNumber*)primitiveHeartZone;
- (void)setPrimitiveHeartZone:(NSNumber*)value;

- (int16_t)primitiveHeartZoneValue;
- (void)setPrimitiveHeartZoneValue:(int16_t)value_;

- (NSNumber*)primitiveMiles;
- (void)setPrimitiveMiles:(NSNumber*)value;

- (double)primitiveMilesValue;
- (void)setPrimitiveMilesValue:(double)value_;

- (NSNumber*)primitiveMph;
- (void)setPrimitiveMph:(NSNumber*)value;

- (double)primitiveMphValue;
- (void)setPrimitiveMphValue:(double)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitivePounds;
- (void)setPrimitivePounds:(NSNumber*)value;

- (double)primitivePoundsValue;
- (void)setPrimitivePoundsValue:(double)value_;

- (NSNumber*)primitiveReps;
- (void)setPrimitiveReps:(NSNumber*)value;

- (int16_t)primitiveRepsValue;
- (void)setPrimitiveRepsValue:(int16_t)value_;

- (NSNumber*)primitiveSeconds;
- (void)setPrimitiveSeconds:(NSNumber*)value;

- (int16_t)primitiveSecondsValue;
- (void)setPrimitiveSecondsValue:(int16_t)value_;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSNumber*)primitiveWatts;
- (void)setPrimitiveWatts:(NSNumber*)value;

- (int16_t)primitiveWattsValue;
- (void)setPrimitiveWattsValue:(int16_t)value_;

- (ActivityMO*)primitiveActivity;
- (void)setPrimitiveActivity:(ActivityMO*)value;

- (ActivitySetMO*)primitiveBasedOnSet;
- (void)setPrimitiveBasedOnSet:(ActivitySetMO*)value;

- (NSMutableSet*)primitiveDerivedSets;
- (void)setPrimitiveDerivedSets:(NSMutableSet*)value;

@end
