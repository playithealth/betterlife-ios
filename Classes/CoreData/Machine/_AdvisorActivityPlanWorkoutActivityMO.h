// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanWorkoutActivityMO.h instead.

#import <CoreData/CoreData.h>
#import "ActivityMO.h"

@interface AdvisorActivityPlanWorkoutActivityMOID : ActivityMOID {}
@end

@interface _AdvisorActivityPlanWorkoutActivityMO : ActivityMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorActivityPlanWorkoutActivityMOID* objectID;

@end

@interface _AdvisorActivityPlanWorkoutActivityMO (CoreDataGeneratedPrimitiveAccessors)

@end
