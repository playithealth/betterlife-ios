// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorPlanPhaseMO.h instead.

#import <CoreData/CoreData.h>

extern const struct AdvisorPlanPhaseMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *durationInDays;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *phaseNumber;
} AdvisorPlanPhaseMOAttributes;

extern const struct AdvisorPlanPhaseMORelationships {
	__unsafe_unretained NSString *plan;
} AdvisorPlanPhaseMORelationships;

@class AdvisorPlanMO;

@interface AdvisorPlanPhaseMOID : NSManagedObjectID {}
@end

@interface _AdvisorPlanPhaseMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorPlanPhaseMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* durationInDays;

@property (atomic) int16_t durationInDaysValue;
- (int16_t)durationInDaysValue;
- (void)setDurationInDaysValue:(int16_t)value_;

//- (BOOL)validateDurationInDays:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* phaseNumber;

@property (atomic) int16_t phaseNumberValue;
- (int16_t)phaseNumberValue;
- (void)setPhaseNumberValue:(int16_t)value_;

//- (BOOL)validatePhaseNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AdvisorPlanMO *plan;

//- (BOOL)validatePlan:(id*)value_ error:(NSError**)error_;

@end

@interface _AdvisorPlanPhaseMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveDurationInDays;
- (void)setPrimitiveDurationInDays:(NSNumber*)value;

- (int16_t)primitiveDurationInDaysValue;
- (void)setPrimitiveDurationInDaysValue:(int16_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSNumber*)primitivePhaseNumber;
- (void)setPrimitivePhaseNumber:(NSNumber*)value;

- (int16_t)primitivePhaseNumberValue;
- (void)setPrimitivePhaseNumberValue:(int16_t)value_;

- (AdvisorPlanMO*)primitivePlan;
- (void)setPrimitivePlan:(AdvisorPlanMO*)value;

@end
