// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ThreadMessageMO.h instead.

#import <CoreData/CoreData.h>

extern const struct ThreadMessageMOAttributes {
	__unsafe_unretained NSString *body;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *createdOn;
	__unsafe_unretained NSString *senderId;
	__unsafe_unretained NSString *senderName;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
} ThreadMessageMOAttributes;

extern const struct ThreadMessageMORelationships {
	__unsafe_unretained NSString *thread;
	__unsafe_unretained NSString *userProfile;
} ThreadMessageMORelationships;

@class ThreadMO;
@class UserProfileMO;

@interface ThreadMessageMOID : NSManagedObjectID {}
@end

@interface _ThreadMessageMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ThreadMessageMOID* objectID;

@property (nonatomic, strong) NSString* body;

//- (BOOL)validateBody:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* createdOn;

@property (atomic) int32_t createdOnValue;
- (int32_t)createdOnValue;
- (void)setCreatedOnValue:(int32_t)value_;

//- (BOOL)validateCreatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* senderId;

@property (atomic) int32_t senderIdValue;
- (int32_t)senderIdValue;
- (void)setSenderIdValue:(int32_t)value_;

//- (BOOL)validateSenderId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* senderName;

//- (BOOL)validateSenderName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) ThreadMO *thread;

//- (BOOL)validateThread:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UserProfileMO *userProfile;

//- (BOOL)validateUserProfile:(id*)value_ error:(NSError**)error_;

@end

@interface _ThreadMessageMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveBody;
- (void)setPrimitiveBody:(NSString*)value;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveCreatedOn;
- (void)setPrimitiveCreatedOn:(NSNumber*)value;

- (int32_t)primitiveCreatedOnValue;
- (void)setPrimitiveCreatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveSenderId;
- (void)setPrimitiveSenderId:(NSNumber*)value;

- (int32_t)primitiveSenderIdValue;
- (void)setPrimitiveSenderIdValue:(int32_t)value_;

- (NSString*)primitiveSenderName;
- (void)setPrimitiveSenderName:(NSString*)value;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (ThreadMO*)primitiveThread;
- (void)setPrimitiveThread:(ThreadMO*)value;

- (UserProfileMO*)primitiveUserProfile;
- (void)setPrimitiveUserProfile:(UserProfileMO*)value;

@end
