// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealPlannerShoppingListItem.m instead.

#import "_MealPlannerShoppingListItem.h"

const struct MealPlannerShoppingListItemAttributes MealPlannerShoppingListItemAttributes = {
	.dateAdded = @"dateAdded",
	.quantity = @"quantity",
};

const struct MealPlannerShoppingListItemRelationships MealPlannerShoppingListItemRelationships = {
	.ingredient = @"ingredient",
	.mealPlannerDay = @"mealPlannerDay",
	.shoppingListItem = @"shoppingListItem",
};

@implementation MealPlannerShoppingListItemID
@end

@implementation _MealPlannerShoppingListItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MealPlannerShoppingListItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MealPlannerShoppingListItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MealPlannerShoppingListItem" inManagedObjectContext:moc_];
}

- (MealPlannerShoppingListItemID*)objectID {
	return (MealPlannerShoppingListItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"dateAddedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dateAdded"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"quantityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"quantity"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic dateAdded;

- (int32_t)dateAddedValue {
	NSNumber *result = [self dateAdded];
	return [result intValue];
}

- (void)setDateAddedValue:(int32_t)value_ {
	[self setDateAdded:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDateAddedValue {
	NSNumber *result = [self primitiveDateAdded];
	return [result intValue];
}

- (void)setPrimitiveDateAddedValue:(int32_t)value_ {
	[self setPrimitiveDateAdded:[NSNumber numberWithInt:value_]];
}

@dynamic quantity;

- (int16_t)quantityValue {
	NSNumber *result = [self quantity];
	return [result shortValue];
}

- (void)setQuantityValue:(int16_t)value_ {
	[self setQuantity:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveQuantityValue {
	NSNumber *result = [self primitiveQuantity];
	return [result shortValue];
}

- (void)setPrimitiveQuantityValue:(int16_t)value_ {
	[self setPrimitiveQuantity:[NSNumber numberWithShort:value_]];
}

@dynamic ingredient;

@dynamic mealPlannerDay;

@dynamic shoppingListItem;

@end

