// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanWorkoutMO.h instead.

#import <CoreData/CoreData.h>

#import "AdvisorActivityPlanWorkoutEnums.h"

extern const struct AdvisorActivityPlanWorkoutMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *numberToPerform;
	__unsafe_unretained NSString *performOp;
	__unsafe_unretained NSString *sortOrder;
	__unsafe_unretained NSString *workoutName;
	__unsafe_unretained NSString *workoutTime;
} AdvisorActivityPlanWorkoutMOAttributes;

extern const struct AdvisorActivityPlanWorkoutMORelationships {
	__unsafe_unretained NSString *activities;
	__unsafe_unretained NSString *activityRecords;
	__unsafe_unretained NSString *phase;
} AdvisorActivityPlanWorkoutMORelationships;

extern const struct AdvisorActivityPlanWorkoutMOUserInfo {
	__unsafe_unretained NSString *additionalHeaderFileName;
} AdvisorActivityPlanWorkoutMOUserInfo;

@class ActivityMO;
@class ActivityRecordMO;
@class AdvisorActivityPlanPhaseMO;

@interface AdvisorActivityPlanWorkoutMOID : NSManagedObjectID {}
@end

@interface _AdvisorActivityPlanWorkoutMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorActivityPlanWorkoutMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int16_t loginIdValue;
- (int16_t)loginIdValue;
- (void)setLoginIdValue:(int16_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* numberToPerform;

@property (atomic) int16_t numberToPerformValue;
- (int16_t)numberToPerformValue;
- (void)setNumberToPerformValue:(int16_t)value_;

//- (BOOL)validateNumberToPerform:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* performOp;

@property (atomic) PerformOperator performOpValue;
- (PerformOperator)performOpValue;
- (void)setPerformOpValue:(PerformOperator)value_;

//- (BOOL)validatePerformOp:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* workoutName;

//- (BOOL)validateWorkoutName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* workoutTime;

@property (atomic) WorkoutTime workoutTimeValue;
- (WorkoutTime)workoutTimeValue;
- (void)setWorkoutTimeValue:(WorkoutTime)value_;

//- (BOOL)validateWorkoutTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *activities;

- (NSMutableSet*)activitiesSet;

@property (nonatomic, strong) NSSet *activityRecords;

- (NSMutableSet*)activityRecordsSet;

@property (nonatomic, strong) AdvisorActivityPlanPhaseMO *phase;

//- (BOOL)validatePhase:(id*)value_ error:(NSError**)error_;

@end

@interface _AdvisorActivityPlanWorkoutMO (ActivitiesCoreDataGeneratedAccessors)
- (void)addActivities:(NSSet*)value_;
- (void)removeActivities:(NSSet*)value_;
- (void)addActivitiesObject:(ActivityMO*)value_;
- (void)removeActivitiesObject:(ActivityMO*)value_;

@end

@interface _AdvisorActivityPlanWorkoutMO (ActivityRecordsCoreDataGeneratedAccessors)
- (void)addActivityRecords:(NSSet*)value_;
- (void)removeActivityRecords:(NSSet*)value_;
- (void)addActivityRecordsObject:(ActivityRecordMO*)value_;
- (void)removeActivityRecordsObject:(ActivityRecordMO*)value_;

@end

@interface _AdvisorActivityPlanWorkoutMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int16_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int16_t)value_;

- (NSNumber*)primitiveNumberToPerform;
- (void)setPrimitiveNumberToPerform:(NSNumber*)value;

- (int16_t)primitiveNumberToPerformValue;
- (void)setPrimitiveNumberToPerformValue:(int16_t)value_;

- (NSNumber*)primitivePerformOp;
- (void)setPrimitivePerformOp:(NSNumber*)value;

- (PerformOperator)primitivePerformOpValue;
- (void)setPrimitivePerformOpValue:(PerformOperator)value_;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSString*)primitiveWorkoutName;
- (void)setPrimitiveWorkoutName:(NSString*)value;

- (NSNumber*)primitiveWorkoutTime;
- (void)setPrimitiveWorkoutTime:(NSNumber*)value;

- (WorkoutTime)primitiveWorkoutTimeValue;
- (void)setPrimitiveWorkoutTimeValue:(WorkoutTime)value_;

- (NSMutableSet*)primitiveActivities;
- (void)setPrimitiveActivities:(NSMutableSet*)value;

- (NSMutableSet*)primitiveActivityRecords;
- (void)setPrimitiveActivityRecords:(NSMutableSet*)value;

- (AdvisorActivityPlanPhaseMO*)primitivePhase;
- (void)setPrimitivePhase:(AdvisorActivityPlanPhaseMO*)value;

@end
