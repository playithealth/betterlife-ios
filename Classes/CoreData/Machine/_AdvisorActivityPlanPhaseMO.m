// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanPhaseMO.m instead.

#import "_AdvisorActivityPlanPhaseMO.h"

const struct AdvisorActivityPlanPhaseMORelationships AdvisorActivityPlanPhaseMORelationships = {
	.workouts = @"workouts",
};

@implementation AdvisorActivityPlanPhaseMOID
@end

@implementation _AdvisorActivityPlanPhaseMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorActivityPlanPhase" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorActivityPlanPhase";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorActivityPlanPhase" inManagedObjectContext:moc_];
}

- (AdvisorActivityPlanPhaseMOID*)objectID {
	return (AdvisorActivityPlanPhaseMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic workouts;

- (NSMutableSet*)workoutsSet {
	[self willAccessValueForKey:@"workouts"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"workouts"];

	[self didAccessValueForKey:@"workouts"];
	return result;
}

@end

