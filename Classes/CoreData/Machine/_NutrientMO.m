// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to NutrientMO.m instead.

#import "_NutrientMO.h"

const struct NutrientMOAttributes NutrientMOAttributes = {
	.cloudKey = @"cloudKey",
	.displayName = @"displayName",
	.isSubNutrient = @"isSubNutrient",
	.name = @"name",
	.precision = @"precision",
	.sortOrder = @"sortOrder",
	.units = @"units",
};

const struct NutrientMORelationships NutrientMORelationships = {
	.userProfilesShowing = @"userProfilesShowing",
};

@implementation NutrientMOID
@end

@implementation _NutrientMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Nutrient" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Nutrient";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Nutrient" inManagedObjectContext:moc_];
}

- (NutrientMOID*)objectID {
	return (NutrientMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isSubNutrientValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isSubNutrient"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"precisionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"precision"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudKey;

@dynamic displayName;

@dynamic isSubNutrient;

- (BOOL)isSubNutrientValue {
	NSNumber *result = [self isSubNutrient];
	return [result boolValue];
}

- (void)setIsSubNutrientValue:(BOOL)value_ {
	[self setIsSubNutrient:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsSubNutrientValue {
	NSNumber *result = [self primitiveIsSubNutrient];
	return [result boolValue];
}

- (void)setPrimitiveIsSubNutrientValue:(BOOL)value_ {
	[self setPrimitiveIsSubNutrient:[NSNumber numberWithBool:value_]];
}

@dynamic name;

@dynamic precision;

- (int16_t)precisionValue {
	NSNumber *result = [self precision];
	return [result shortValue];
}

- (void)setPrecisionValue:(int16_t)value_ {
	[self setPrecision:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePrecisionValue {
	NSNumber *result = [self primitivePrecision];
	return [result shortValue];
}

- (void)setPrimitivePrecisionValue:(int16_t)value_ {
	[self setPrimitivePrecision:[NSNumber numberWithShort:value_]];
}

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic units;

@dynamic userProfilesShowing;

- (NSMutableSet*)userProfilesShowingSet {
	[self willAccessValueForKey:@"userProfilesShowing"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userProfilesShowing"];

	[self didAccessValueForKey:@"userProfilesShowing"];
	return result;
}

@end

