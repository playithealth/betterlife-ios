// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitMO.m instead.

#import "_UnitMO.h"

const struct UnitMOAttributes UnitMOAttributes = {
	.abbrev = @"abbrev",
	.plural = @"plural",
	.singular = @"singular",
	.sortOrder = @"sortOrder",
};

@implementation UnitMOID
@end

@implementation _UnitMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Unit" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Unit";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Unit" inManagedObjectContext:moc_];
}

- (UnitMOID*)objectID {
	return (UnitMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic abbrev;

@dynamic plural;

@dynamic singular;

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@end

