// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PantryItem.m instead.

#import "_PantryItem.h"

const struct PantryItemAttributes PantryItemAttributes = {
	.accountId = @"accountId",
	.barcode = @"barcode",
	.calories = @"calories",
	.caloriesFromFat = @"caloriesFromFat",
	.carbohydrates = @"carbohydrates",
	.cholesterol = @"cholesterol",
	.cloudId = @"cloudId",
	.createdOn = @"createdOn",
	.fat = @"fat",
	.imageId = @"imageId",
	.name = @"name",
	.productId = @"productId",
	.protein = @"protein",
	.purchasedOn = @"purchasedOn",
	.quantity = @"quantity",
	.reorderPoint = @"reorderPoint",
	.saturatedFat = @"saturatedFat",
	.sodium = @"sodium",
	.sourceId = @"sourceId",
	.status = @"status",
	.updatedOn = @"updatedOn",
};

const struct PantryItemRelationships PantryItemRelationships = {
	.category = @"category",
};

@implementation PantryItemID
@end

@implementation _PantryItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PantryItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PantryItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PantryItem" inManagedObjectContext:moc_];
}

- (PantryItemID*)objectID {
	return (PantryItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"calories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesFromFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"caloriesFromFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"carbohydratesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"carbohydrates"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cholesterolValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cholesterol"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"createdOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"createdOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"fatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"fat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"imageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"imageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"productIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"productId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"proteinValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"protein"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"purchasedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"purchasedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"quantityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"quantity"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"reorderPointValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"reorderPoint"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"saturatedFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"saturatedFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sodiumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sodium"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sourceIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sourceId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic barcode;

@dynamic calories;

- (int16_t)caloriesValue {
	NSNumber *result = [self calories];
	return [result shortValue];
}

- (void)setCaloriesValue:(int16_t)value_ {
	[self setCalories:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCaloriesValue {
	NSNumber *result = [self primitiveCalories];
	return [result shortValue];
}

- (void)setPrimitiveCaloriesValue:(int16_t)value_ {
	[self setPrimitiveCalories:[NSNumber numberWithShort:value_]];
}

@dynamic caloriesFromFat;

- (int16_t)caloriesFromFatValue {
	NSNumber *result = [self caloriesFromFat];
	return [result shortValue];
}

- (void)setCaloriesFromFatValue:(int16_t)value_ {
	[self setCaloriesFromFat:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCaloriesFromFatValue {
	NSNumber *result = [self primitiveCaloriesFromFat];
	return [result shortValue];
}

- (void)setPrimitiveCaloriesFromFatValue:(int16_t)value_ {
	[self setPrimitiveCaloriesFromFat:[NSNumber numberWithShort:value_]];
}

@dynamic carbohydrates;

- (double)carbohydratesValue {
	NSNumber *result = [self carbohydrates];
	return [result doubleValue];
}

- (void)setCarbohydratesValue:(double)value_ {
	[self setCarbohydrates:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCarbohydratesValue {
	NSNumber *result = [self primitiveCarbohydrates];
	return [result doubleValue];
}

- (void)setPrimitiveCarbohydratesValue:(double)value_ {
	[self setPrimitiveCarbohydrates:[NSNumber numberWithDouble:value_]];
}

@dynamic cholesterol;

- (double)cholesterolValue {
	NSNumber *result = [self cholesterol];
	return [result doubleValue];
}

- (void)setCholesterolValue:(double)value_ {
	[self setCholesterol:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCholesterolValue {
	NSNumber *result = [self primitiveCholesterol];
	return [result doubleValue];
}

- (void)setPrimitiveCholesterolValue:(double)value_ {
	[self setPrimitiveCholesterol:[NSNumber numberWithDouble:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic createdOn;

- (int32_t)createdOnValue {
	NSNumber *result = [self createdOn];
	return [result intValue];
}

- (void)setCreatedOnValue:(int32_t)value_ {
	[self setCreatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCreatedOnValue {
	NSNumber *result = [self primitiveCreatedOn];
	return [result intValue];
}

- (void)setPrimitiveCreatedOnValue:(int32_t)value_ {
	[self setPrimitiveCreatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic fat;

- (double)fatValue {
	NSNumber *result = [self fat];
	return [result doubleValue];
}

- (void)setFatValue:(double)value_ {
	[self setFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveFatValue {
	NSNumber *result = [self primitiveFat];
	return [result doubleValue];
}

- (void)setPrimitiveFatValue:(double)value_ {
	[self setPrimitiveFat:[NSNumber numberWithDouble:value_]];
}

@dynamic imageId;

- (int32_t)imageIdValue {
	NSNumber *result = [self imageId];
	return [result intValue];
}

- (void)setImageIdValue:(int32_t)value_ {
	[self setImageId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImageIdValue {
	NSNumber *result = [self primitiveImageId];
	return [result intValue];
}

- (void)setPrimitiveImageIdValue:(int32_t)value_ {
	[self setPrimitiveImageId:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic productId;

- (int32_t)productIdValue {
	NSNumber *result = [self productId];
	return [result intValue];
}

- (void)setProductIdValue:(int32_t)value_ {
	[self setProductId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveProductIdValue {
	NSNumber *result = [self primitiveProductId];
	return [result intValue];
}

- (void)setPrimitiveProductIdValue:(int32_t)value_ {
	[self setPrimitiveProductId:[NSNumber numberWithInt:value_]];
}

@dynamic protein;

- (double)proteinValue {
	NSNumber *result = [self protein];
	return [result doubleValue];
}

- (void)setProteinValue:(double)value_ {
	[self setProtein:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveProteinValue {
	NSNumber *result = [self primitiveProtein];
	return [result doubleValue];
}

- (void)setPrimitiveProteinValue:(double)value_ {
	[self setPrimitiveProtein:[NSNumber numberWithDouble:value_]];
}

@dynamic purchasedOn;

- (int32_t)purchasedOnValue {
	NSNumber *result = [self purchasedOn];
	return [result intValue];
}

- (void)setPurchasedOnValue:(int32_t)value_ {
	[self setPurchasedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePurchasedOnValue {
	NSNumber *result = [self primitivePurchasedOn];
	return [result intValue];
}

- (void)setPrimitivePurchasedOnValue:(int32_t)value_ {
	[self setPrimitivePurchasedOn:[NSNumber numberWithInt:value_]];
}

@dynamic quantity;

- (double)quantityValue {
	NSNumber *result = [self quantity];
	return [result doubleValue];
}

- (void)setQuantityValue:(double)value_ {
	[self setQuantity:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveQuantityValue {
	NSNumber *result = [self primitiveQuantity];
	return [result doubleValue];
}

- (void)setPrimitiveQuantityValue:(double)value_ {
	[self setPrimitiveQuantity:[NSNumber numberWithDouble:value_]];
}

@dynamic reorderPoint;

- (double)reorderPointValue {
	NSNumber *result = [self reorderPoint];
	return [result doubleValue];
}

- (void)setReorderPointValue:(double)value_ {
	[self setReorderPoint:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveReorderPointValue {
	NSNumber *result = [self primitiveReorderPoint];
	return [result doubleValue];
}

- (void)setPrimitiveReorderPointValue:(double)value_ {
	[self setPrimitiveReorderPoint:[NSNumber numberWithDouble:value_]];
}

@dynamic saturatedFat;

- (double)saturatedFatValue {
	NSNumber *result = [self saturatedFat];
	return [result doubleValue];
}

- (void)setSaturatedFatValue:(double)value_ {
	[self setSaturatedFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSaturatedFatValue {
	NSNumber *result = [self primitiveSaturatedFat];
	return [result doubleValue];
}

- (void)setPrimitiveSaturatedFatValue:(double)value_ {
	[self setPrimitiveSaturatedFat:[NSNumber numberWithDouble:value_]];
}

@dynamic sodium;

- (double)sodiumValue {
	NSNumber *result = [self sodium];
	return [result doubleValue];
}

- (void)setSodiumValue:(double)value_ {
	[self setSodium:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSodiumValue {
	NSNumber *result = [self primitiveSodium];
	return [result doubleValue];
}

- (void)setPrimitiveSodiumValue:(double)value_ {
	[self setPrimitiveSodium:[NSNumber numberWithDouble:value_]];
}

@dynamic sourceId;

- (int32_t)sourceIdValue {
	NSNumber *result = [self sourceId];
	return [result intValue];
}

- (void)setSourceIdValue:(int32_t)value_ {
	[self setSourceId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSourceIdValue {
	NSNumber *result = [self primitiveSourceId];
	return [result intValue];
}

- (void)setPrimitiveSourceIdValue:(int32_t)value_ {
	[self setPrimitiveSourceId:[NSNumber numberWithInt:value_]];
}

@dynamic status;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic category;

@end

