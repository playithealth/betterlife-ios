// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserPermissionMO.h instead.

#import <CoreData/CoreData.h>

extern const struct UserPermissionMOAttributes {
	__unsafe_unretained NSString *enabled;
	__unsafe_unretained NSString *name;
} UserPermissionMOAttributes;

extern const struct UserPermissionMORelationships {
	__unsafe_unretained NSString *user;
} UserPermissionMORelationships;

@class UserProfileMO;

@interface UserPermissionMOID : NSManagedObjectID {}
@end

@interface _UserPermissionMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserPermissionMOID* objectID;

@property (nonatomic, strong) NSNumber* enabled;

@property (atomic) BOOL enabledValue;
- (BOOL)enabledValue;
- (void)setEnabledValue:(BOOL)value_;

//- (BOOL)validateEnabled:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UserProfileMO *user;

//- (BOOL)validateUser:(id*)value_ error:(NSError**)error_;

@end

@interface _UserPermissionMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveEnabled;
- (void)setPrimitiveEnabled:(NSNumber*)value;

- (BOOL)primitiveEnabledValue;
- (void)setPrimitiveEnabledValue:(BOOL)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (UserProfileMO*)primitiveUser;
- (void)setPrimitiveUser:(UserProfileMO*)value;

@end
