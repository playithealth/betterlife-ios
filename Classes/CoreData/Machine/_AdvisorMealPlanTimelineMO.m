// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanTimelineMO.m instead.

#import "_AdvisorMealPlanTimelineMO.h"

@implementation AdvisorMealPlanTimelineMOID
@end

@implementation _AdvisorMealPlanTimelineMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorMealPlanTimeline" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorMealPlanTimeline";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorMealPlanTimeline" inManagedObjectContext:moc_];
}

- (AdvisorMealPlanTimelineMOID*)objectID {
	return (AdvisorMealPlanTimelineMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@end

