// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorPlanPhaseMO.m instead.

#import "_AdvisorPlanPhaseMO.h"

const struct AdvisorPlanPhaseMOAttributes AdvisorPlanPhaseMOAttributes = {
	.cloudId = @"cloudId",
	.durationInDays = @"durationInDays",
	.loginId = @"loginId",
	.phaseNumber = @"phaseNumber",
};

const struct AdvisorPlanPhaseMORelationships AdvisorPlanPhaseMORelationships = {
	.plan = @"plan",
};

@implementation AdvisorPlanPhaseMOID
@end

@implementation _AdvisorPlanPhaseMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AbstractAdvisorPlanPhase" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AbstractAdvisorPlanPhase";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AbstractAdvisorPlanPhase" inManagedObjectContext:moc_];
}

- (AdvisorPlanPhaseMOID*)objectID {
	return (AdvisorPlanPhaseMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"durationInDaysValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"durationInDays"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"phaseNumberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"phaseNumber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic durationInDays;

- (int16_t)durationInDaysValue {
	NSNumber *result = [self durationInDays];
	return [result shortValue];
}

- (void)setDurationInDaysValue:(int16_t)value_ {
	[self setDurationInDays:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveDurationInDaysValue {
	NSNumber *result = [self primitiveDurationInDays];
	return [result shortValue];
}

- (void)setPrimitiveDurationInDaysValue:(int16_t)value_ {
	[self setPrimitiveDurationInDays:[NSNumber numberWithShort:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic phaseNumber;

- (int16_t)phaseNumberValue {
	NSNumber *result = [self phaseNumber];
	return [result shortValue];
}

- (void)setPhaseNumberValue:(int16_t)value_ {
	[self setPhaseNumber:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePhaseNumberValue {
	NSNumber *result = [self primitivePhaseNumber];
	return [result shortValue];
}

- (void)setPrimitivePhaseNumberValue:(int16_t)value_ {
	[self setPrimitivePhaseNumber:[NSNumber numberWithShort:value_]];
}

@dynamic plan;

@end

