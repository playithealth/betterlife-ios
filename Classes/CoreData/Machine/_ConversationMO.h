// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ConversationMO.h instead.

#import <CoreData/CoreData.h>

extern const struct ConversationMOAttributes {
	__unsafe_unretained NSString *conversationUserGender;
	__unsafe_unretained NSString *conversationUserId;
	__unsafe_unretained NSString *conversationUserImageId;
	__unsafe_unretained NSString *conversationUserName;
	__unsafe_unretained NSString *isRead;
	__unsafe_unretained NSString *loginId;
} ConversationMOAttributes;

extern const struct ConversationMORelationships {
	__unsafe_unretained NSString *messageDeliveries;
} ConversationMORelationships;

extern const struct ConversationMOFetchedProperties {
	__unsafe_unretained NSString *newestMessage;
} ConversationMOFetchedProperties;

@class MessageDeliveryMO;

@interface ConversationMOID : NSManagedObjectID {}
@end

@interface _ConversationMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ConversationMOID* objectID;

@property (nonatomic, strong) NSNumber* conversationUserGender;

@property (atomic) int16_t conversationUserGenderValue;
- (int16_t)conversationUserGenderValue;
- (void)setConversationUserGenderValue:(int16_t)value_;

//- (BOOL)validateConversationUserGender:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* conversationUserId;

@property (atomic) int32_t conversationUserIdValue;
- (int32_t)conversationUserIdValue;
- (void)setConversationUserIdValue:(int32_t)value_;

//- (BOOL)validateConversationUserId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* conversationUserImageId;

@property (atomic) int32_t conversationUserImageIdValue;
- (int32_t)conversationUserImageIdValue;
- (void)setConversationUserImageIdValue:(int32_t)value_;

//- (BOOL)validateConversationUserImageId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* conversationUserName;

//- (BOOL)validateConversationUserName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRead;

@property (atomic) BOOL isReadValue;
- (BOOL)isReadValue;
- (void)setIsReadValue:(BOOL)value_;

//- (BOOL)validateIsRead:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *messageDeliveries;

- (NSMutableSet*)messageDeliveriesSet;

@property (nonatomic, readonly) NSArray *newestMessage;

@end

@interface _ConversationMO (MessageDeliveriesCoreDataGeneratedAccessors)
- (void)addMessageDeliveries:(NSSet*)value_;
- (void)removeMessageDeliveries:(NSSet*)value_;
- (void)addMessageDeliveriesObject:(MessageDeliveryMO*)value_;
- (void)removeMessageDeliveriesObject:(MessageDeliveryMO*)value_;

@end

@interface _ConversationMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveConversationUserGender;
- (void)setPrimitiveConversationUserGender:(NSNumber*)value;

- (int16_t)primitiveConversationUserGenderValue;
- (void)setPrimitiveConversationUserGenderValue:(int16_t)value_;

- (NSNumber*)primitiveConversationUserId;
- (void)setPrimitiveConversationUserId:(NSNumber*)value;

- (int32_t)primitiveConversationUserIdValue;
- (void)setPrimitiveConversationUserIdValue:(int32_t)value_;

- (NSNumber*)primitiveConversationUserImageId;
- (void)setPrimitiveConversationUserImageId:(NSNumber*)value;

- (int32_t)primitiveConversationUserImageIdValue;
- (void)setPrimitiveConversationUserImageIdValue:(int32_t)value_;

- (NSString*)primitiveConversationUserName;
- (void)setPrimitiveConversationUserName:(NSString*)value;

- (NSNumber*)primitiveIsRead;
- (void)setPrimitiveIsRead:(NSNumber*)value;

- (BOOL)primitiveIsReadValue;
- (void)setPrimitiveIsReadValue:(BOOL)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSMutableSet*)primitiveMessageDeliveries;
- (void)setPrimitiveMessageDeliveries:(NSMutableSet*)value;

@end
