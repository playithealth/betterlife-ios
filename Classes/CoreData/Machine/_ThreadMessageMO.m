// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ThreadMessageMO.m instead.

#import "_ThreadMessageMO.h"

const struct ThreadMessageMOAttributes ThreadMessageMOAttributes = {
	.body = @"body",
	.cloudId = @"cloudId",
	.createdOn = @"createdOn",
	.senderId = @"senderId",
	.senderName = @"senderName",
	.status = @"status",
	.updatedOn = @"updatedOn",
};

const struct ThreadMessageMORelationships ThreadMessageMORelationships = {
	.thread = @"thread",
	.userProfile = @"userProfile",
};

@implementation ThreadMessageMOID
@end

@implementation _ThreadMessageMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ThreadMessage" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ThreadMessage";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ThreadMessage" inManagedObjectContext:moc_];
}

- (ThreadMessageMOID*)objectID {
	return (ThreadMessageMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"createdOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"createdOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"senderIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"senderId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic body;

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic createdOn;

- (int32_t)createdOnValue {
	NSNumber *result = [self createdOn];
	return [result intValue];
}

- (void)setCreatedOnValue:(int32_t)value_ {
	[self setCreatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCreatedOnValue {
	NSNumber *result = [self primitiveCreatedOn];
	return [result intValue];
}

- (void)setPrimitiveCreatedOnValue:(int32_t)value_ {
	[self setPrimitiveCreatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic senderId;

- (int32_t)senderIdValue {
	NSNumber *result = [self senderId];
	return [result intValue];
}

- (void)setSenderIdValue:(int32_t)value_ {
	[self setSenderId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSenderIdValue {
	NSNumber *result = [self primitiveSenderId];
	return [result intValue];
}

- (void)setPrimitiveSenderIdValue:(int32_t)value_ {
	[self setPrimitiveSenderId:[NSNumber numberWithInt:value_]];
}

@dynamic senderName;

@dynamic status;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic thread;

@dynamic userProfile;

@end

