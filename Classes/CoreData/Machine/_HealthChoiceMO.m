// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to HealthChoiceMO.m instead.

#import "_HealthChoiceMO.h"

const struct HealthChoiceMOAttributes HealthChoiceMOAttributes = {
	.choiceType = @"choiceType",
	.detailText = @"detailText",
	.filter = @"filter",
	.name = @"name",
	.sortOrder = @"sortOrder",
};

const struct HealthChoiceMORelationships HealthChoiceMORelationships = {
	.userHealthChoices = @"userHealthChoices",
};

@implementation HealthChoiceMOID
@end

@implementation _HealthChoiceMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"HealthChoice" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"HealthChoice";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"HealthChoice" inManagedObjectContext:moc_];
}

- (HealthChoiceMOID*)objectID {
	return (HealthChoiceMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"choiceTypeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"choiceType"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic choiceType;

- (int16_t)choiceTypeValue {
	NSNumber *result = [self choiceType];
	return [result shortValue];
}

- (void)setChoiceTypeValue:(int16_t)value_ {
	[self setChoiceType:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveChoiceTypeValue {
	NSNumber *result = [self primitiveChoiceType];
	return [result shortValue];
}

- (void)setPrimitiveChoiceTypeValue:(int16_t)value_ {
	[self setPrimitiveChoiceType:[NSNumber numberWithShort:value_]];
}

@dynamic detailText;

@dynamic filter;

@dynamic name;

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic userHealthChoices;

- (NSMutableSet*)userHealthChoicesSet {
	[self willAccessValueForKey:@"userHealthChoices"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userHealthChoices"];

	[self didAccessValueForKey:@"userHealthChoices"];
	return result;
}

@end

