// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CategoryMO.m instead.

#import "_CategoryMO.h"

const struct CategoryMOAttributes CategoryMOAttributes = {
	.cloudId = @"cloudId",
	.name = @"name",
	.nameFirstCharacter = @"nameFirstCharacter",
};

const struct CategoryMORelationships CategoryMORelationships = {
	.customFoods = @"customFoods",
	.mealIngredients = @"mealIngredients",
	.pantryItems = @"pantryItems",
	.purchaseHxItems = @"purchaseHxItems",
	.recommendations = @"recommendations",
	.scannedItems = @"scannedItems",
	.shoppingListItems = @"shoppingListItems",
	.storeCategories = @"storeCategories",
};

@implementation CategoryMOID
@end

@implementation _CategoryMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Category";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Category" inManagedObjectContext:moc_];
}

- (CategoryMOID*)objectID {
	return (CategoryMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic nameFirstCharacter;

@dynamic customFoods;

- (NSMutableSet*)customFoodsSet {
	[self willAccessValueForKey:@"customFoods"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"customFoods"];

	[self didAccessValueForKey:@"customFoods"];
	return result;
}

@dynamic mealIngredients;

- (NSMutableSet*)mealIngredientsSet {
	[self willAccessValueForKey:@"mealIngredients"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mealIngredients"];

	[self didAccessValueForKey:@"mealIngredients"];
	return result;
}

@dynamic pantryItems;

- (NSMutableSet*)pantryItemsSet {
	[self willAccessValueForKey:@"pantryItems"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"pantryItems"];

	[self didAccessValueForKey:@"pantryItems"];
	return result;
}

@dynamic purchaseHxItems;

- (NSMutableSet*)purchaseHxItemsSet {
	[self willAccessValueForKey:@"purchaseHxItems"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"purchaseHxItems"];

	[self didAccessValueForKey:@"purchaseHxItems"];
	return result;
}

@dynamic recommendations;

- (NSMutableSet*)recommendationsSet {
	[self willAccessValueForKey:@"recommendations"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"recommendations"];

	[self didAccessValueForKey:@"recommendations"];
	return result;
}

@dynamic scannedItems;

- (NSMutableSet*)scannedItemsSet {
	[self willAccessValueForKey:@"scannedItems"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"scannedItems"];

	[self didAccessValueForKey:@"scannedItems"];
	return result;
}

@dynamic shoppingListItems;

- (NSMutableSet*)shoppingListItemsSet {
	[self willAccessValueForKey:@"shoppingListItems"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"shoppingListItems"];

	[self didAccessValueForKey:@"shoppingListItems"];
	return result;
}

@dynamic storeCategories;

- (NSMutableSet*)storeCategoriesSet {
	[self willAccessValueForKey:@"storeCategories"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"storeCategories"];

	[self didAccessValueForKey:@"storeCategories"];
	return result;
}

@end

