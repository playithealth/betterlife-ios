// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrainingActivityMO.h instead.

#import <CoreData/CoreData.h>

extern const struct TrainingActivityMOAttributes {
	__unsafe_unretained NSString *activity;
	__unsafe_unretained NSString *calories;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *distance;
	__unsafe_unretained NSString *factor;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *source;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *time;
	__unsafe_unretained NSString *updatedOn;
	__unsafe_unretained NSString *weight;
} TrainingActivityMOAttributes;

extern const struct TrainingActivityMORelationships {
	__unsafe_unretained NSString *exerciseRoutine;
} TrainingActivityMORelationships;

@class ExerciseRoutineMO;

@interface TrainingActivityMOID : NSManagedObjectID {}
@end

@interface _TrainingActivityMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TrainingActivityMOID* objectID;

@property (nonatomic, strong) NSString* activity;

//- (BOOL)validateActivity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* calories;

@property (atomic) int16_t caloriesValue;
- (int16_t)caloriesValue;
- (void)setCaloriesValue:(int16_t)value_;

//- (BOOL)validateCalories:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* distance;

@property (atomic) double distanceValue;
- (double)distanceValue;
- (void)setDistanceValue:(double)value_;

//- (BOOL)validateDistance:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* factor;

@property (atomic) double factorValue;
- (double)factorValue;
- (void)setFactorValue:(double)value_;

//- (BOOL)validateFactor:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* source;

@property (atomic) int32_t sourceValue;
- (int32_t)sourceValue;
- (void)setSourceValue:(int32_t)value_;

//- (BOOL)validateSource:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* time;

//- (BOOL)validateTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* weight;

@property (atomic) double weightValue;
- (double)weightValue;
- (void)setWeightValue:(double)value_;

//- (BOOL)validateWeight:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) ExerciseRoutineMO *exerciseRoutine;

//- (BOOL)validateExerciseRoutine:(id*)value_ error:(NSError**)error_;

@end

@interface _TrainingActivityMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveActivity;
- (void)setPrimitiveActivity:(NSString*)value;

- (NSNumber*)primitiveCalories;
- (void)setPrimitiveCalories:(NSNumber*)value;

- (int16_t)primitiveCaloriesValue;
- (void)setPrimitiveCaloriesValue:(int16_t)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSNumber*)primitiveDistance;
- (void)setPrimitiveDistance:(NSNumber*)value;

- (double)primitiveDistanceValue;
- (void)setPrimitiveDistanceValue:(double)value_;

- (NSNumber*)primitiveFactor;
- (void)setPrimitiveFactor:(NSNumber*)value;

- (double)primitiveFactorValue;
- (void)setPrimitiveFactorValue:(double)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSNumber*)primitiveSource;
- (void)setPrimitiveSource:(NSNumber*)value;

- (int32_t)primitiveSourceValue;
- (void)setPrimitiveSourceValue:(int32_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSString*)primitiveTime;
- (void)setPrimitiveTime:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveWeight;
- (void)setPrimitiveWeight:(NSNumber*)value;

- (double)primitiveWeightValue;
- (void)setPrimitiveWeightValue:(double)value_;

- (ExerciseRoutineMO*)primitiveExerciseRoutine;
- (void)setPrimitiveExerciseRoutine:(ExerciseRoutineMO*)value;

@end
