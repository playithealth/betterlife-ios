// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FoodLogMO.m instead.

#import "_FoodLogMO.h"

const struct FoodLogMOAttributes FoodLogMOAttributes = {
	.cloudId = @"cloudId",
	.creationDate = @"creationDate",
	.date = @"date",
	.image = @"image",
	.imageId = @"imageId",
	.itemType = @"itemType",
	.logTime = @"logTime",
	.loginId = @"loginId",
	.menuId = @"menuId",
	.name = @"name",
	.nutritionFactor = @"nutritionFactor",
	.nutritionId = @"nutritionId",
	.productId = @"productId",
	.servingSize = @"servingSize",
	.servings = @"servings",
	.servingsUnit = @"servingsUnit",
	.source = @"source",
	.status = @"status",
	.tagId = @"tagId",
	.updatedOn = @"updatedOn",
};

const struct FoodLogMORelationships FoodLogMORelationships = {
	.customFood = @"customFood",
	.nutrition = @"nutrition",
	.recipe = @"recipe",
	.tag = @"tag",
};

@implementation FoodLogMOID
@end

@implementation _FoodLogMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"FoodLog" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"FoodLog";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"FoodLog" inManagedObjectContext:moc_];
}

- (FoodLogMOID*)objectID {
	return (FoodLogMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"imageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"imageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"logTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"logTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"menuIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"menuId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"nutritionFactorValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"nutritionFactor"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"nutritionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"nutritionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"productIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"productId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"servingsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"servings"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sourceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"source"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"tagIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"tagId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic creationDate;

@dynamic date;

@dynamic image;

@dynamic imageId;

- (int32_t)imageIdValue {
	NSNumber *result = [self imageId];
	return [result intValue];
}

- (void)setImageIdValue:(int32_t)value_ {
	[self setImageId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImageIdValue {
	NSNumber *result = [self primitiveImageId];
	return [result intValue];
}

- (void)setPrimitiveImageIdValue:(int32_t)value_ {
	[self setPrimitiveImageId:[NSNumber numberWithInt:value_]];
}

@dynamic itemType;

@dynamic logTime;

- (int16_t)logTimeValue {
	NSNumber *result = [self logTime];
	return [result shortValue];
}

- (void)setLogTimeValue:(int16_t)value_ {
	[self setLogTime:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLogTimeValue {
	NSNumber *result = [self primitiveLogTime];
	return [result shortValue];
}

- (void)setPrimitiveLogTimeValue:(int16_t)value_ {
	[self setPrimitiveLogTime:[NSNumber numberWithShort:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic menuId;

- (int32_t)menuIdValue {
	NSNumber *result = [self menuId];
	return [result intValue];
}

- (void)setMenuIdValue:(int32_t)value_ {
	[self setMenuId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMenuIdValue {
	NSNumber *result = [self primitiveMenuId];
	return [result intValue];
}

- (void)setPrimitiveMenuIdValue:(int32_t)value_ {
	[self setPrimitiveMenuId:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic nutritionFactor;

- (double)nutritionFactorValue {
	NSNumber *result = [self nutritionFactor];
	return [result doubleValue];
}

- (void)setNutritionFactorValue:(double)value_ {
	[self setNutritionFactor:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveNutritionFactorValue {
	NSNumber *result = [self primitiveNutritionFactor];
	return [result doubleValue];
}

- (void)setPrimitiveNutritionFactorValue:(double)value_ {
	[self setPrimitiveNutritionFactor:[NSNumber numberWithDouble:value_]];
}

@dynamic nutritionId;

- (int32_t)nutritionIdValue {
	NSNumber *result = [self nutritionId];
	return [result intValue];
}

- (void)setNutritionIdValue:(int32_t)value_ {
	[self setNutritionId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveNutritionIdValue {
	NSNumber *result = [self primitiveNutritionId];
	return [result intValue];
}

- (void)setPrimitiveNutritionIdValue:(int32_t)value_ {
	[self setPrimitiveNutritionId:[NSNumber numberWithInt:value_]];
}

@dynamic productId;

- (int32_t)productIdValue {
	NSNumber *result = [self productId];
	return [result intValue];
}

- (void)setProductIdValue:(int32_t)value_ {
	[self setProductId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveProductIdValue {
	NSNumber *result = [self primitiveProductId];
	return [result intValue];
}

- (void)setPrimitiveProductIdValue:(int32_t)value_ {
	[self setPrimitiveProductId:[NSNumber numberWithInt:value_]];
}

@dynamic servingSize;

@dynamic servings;

- (double)servingsValue {
	NSNumber *result = [self servings];
	return [result doubleValue];
}

- (void)setServingsValue:(double)value_ {
	[self setServings:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveServingsValue {
	NSNumber *result = [self primitiveServings];
	return [result doubleValue];
}

- (void)setPrimitiveServingsValue:(double)value_ {
	[self setPrimitiveServings:[NSNumber numberWithDouble:value_]];
}

@dynamic servingsUnit;

@dynamic source;

- (int32_t)sourceValue {
	NSNumber *result = [self source];
	return [result intValue];
}

- (void)setSourceValue:(int32_t)value_ {
	[self setSource:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSourceValue {
	NSNumber *result = [self primitiveSource];
	return [result intValue];
}

- (void)setPrimitiveSourceValue:(int32_t)value_ {
	[self setPrimitiveSource:[NSNumber numberWithInt:value_]];
}

@dynamic status;

@dynamic tagId;

- (int32_t)tagIdValue {
	NSNumber *result = [self tagId];
	return [result intValue];
}

- (void)setTagIdValue:(int32_t)value_ {
	[self setTagId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveTagIdValue {
	NSNumber *result = [self primitiveTagId];
	return [result intValue];
}

- (void)setPrimitiveTagIdValue:(int32_t)value_ {
	[self setPrimitiveTagId:[NSNumber numberWithInt:value_]];
}

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic customFood;

@dynamic nutrition;

@dynamic recipe;

@dynamic tag;

@end

