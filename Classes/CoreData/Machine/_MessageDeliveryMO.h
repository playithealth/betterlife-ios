// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MessageDeliveryMO.h instead.

#import <CoreData/CoreData.h>

extern const struct MessageDeliveryMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *isRead;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *receiverRm;
	__unsafe_unretained NSString *senderRm;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
} MessageDeliveryMOAttributes;

extern const struct MessageDeliveryMORelationships {
	__unsafe_unretained NSString *conversation;
	__unsafe_unretained NSString *message;
} MessageDeliveryMORelationships;

@class ConversationMO;
@class MessageMO;

@interface MessageDeliveryMOID : NSManagedObjectID {}
@end

@interface _MessageDeliveryMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MessageDeliveryMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isRead;

@property (atomic) BOOL isReadValue;
- (BOOL)isReadValue;
- (void)setIsReadValue:(BOOL)value_;

//- (BOOL)validateIsRead:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* receiverRm;

@property (atomic) BOOL receiverRmValue;
- (BOOL)receiverRmValue;
- (void)setReceiverRmValue:(BOOL)value_;

//- (BOOL)validateReceiverRm:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* senderRm;

@property (atomic) BOOL senderRmValue;
- (BOOL)senderRmValue;
- (void)setSenderRmValue:(BOOL)value_;

//- (BOOL)validateSenderRm:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) ConversationMO *conversation;

//- (BOOL)validateConversation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MessageMO *message;

//- (BOOL)validateMessage:(id*)value_ error:(NSError**)error_;

@end

@interface _MessageDeliveryMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveIsRead;
- (void)setPrimitiveIsRead:(NSNumber*)value;

- (BOOL)primitiveIsReadValue;
- (void)setPrimitiveIsReadValue:(BOOL)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSNumber*)primitiveReceiverRm;
- (void)setPrimitiveReceiverRm:(NSNumber*)value;

- (BOOL)primitiveReceiverRmValue;
- (void)setPrimitiveReceiverRmValue:(BOOL)value_;

- (NSNumber*)primitiveSenderRm;
- (void)setPrimitiveSenderRm:(NSNumber*)value;

- (BOOL)primitiveSenderRmValue;
- (void)setPrimitiveSenderRmValue:(BOOL)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (ConversationMO*)primitiveConversation;
- (void)setPrimitiveConversation:(ConversationMO*)value;

- (MessageMO*)primitiveMessage;
- (void)setPrimitiveMessage:(MessageMO*)value;

@end
