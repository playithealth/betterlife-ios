// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Promotion.h instead.

#import <CoreData/CoreData.h>

extern const struct PromotionAttributes {
	__unsafe_unretained NSString *accepted;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *disclaimer;
	__unsafe_unretained NSString *discount;
	__unsafe_unretained NSString *entityId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *promoDescription;
	__unsafe_unretained NSString *status;
} PromotionAttributes;

extern const struct PromotionRelationships {
	__unsafe_unretained NSString *shoppingListItem;
} PromotionRelationships;

@class ShoppingListItem;

@interface PromotionID : NSManagedObjectID {}
@end

@interface _Promotion : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PromotionID* objectID;

@property (nonatomic, strong) NSNumber* accepted;

@property (atomic) BOOL acceptedValue;
- (BOOL)acceptedValue;
- (void)setAcceptedValue:(BOOL)value_;

//- (BOOL)validateAccepted:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* disclaimer;

//- (BOOL)validateDisclaimer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* discount;

//- (BOOL)validateDiscount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* entityId;

@property (atomic) int32_t entityIdValue;
- (int32_t)entityIdValue;
- (void)setEntityIdValue:(int32_t)value_;

//- (BOOL)validateEntityId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* promoDescription;

//- (BOOL)validatePromoDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) ShoppingListItem *shoppingListItem;

//- (BOOL)validateShoppingListItem:(id*)value_ error:(NSError**)error_;

@end

@interface _Promotion (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccepted;
- (void)setPrimitiveAccepted:(NSNumber*)value;

- (BOOL)primitiveAcceptedValue;
- (void)setPrimitiveAcceptedValue:(BOOL)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSString*)primitiveDisclaimer;
- (void)setPrimitiveDisclaimer:(NSString*)value;

- (NSString*)primitiveDiscount;
- (void)setPrimitiveDiscount:(NSString*)value;

- (NSNumber*)primitiveEntityId;
- (void)setPrimitiveEntityId:(NSNumber*)value;

- (int32_t)primitiveEntityIdValue;
- (void)setPrimitiveEntityIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitivePromoDescription;
- (void)setPrimitivePromoDescription:(NSString*)value;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (ShoppingListItem*)primitiveShoppingListItem;
- (void)setPrimitiveShoppingListItem:(ShoppingListItem*)value;

@end
