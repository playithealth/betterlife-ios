// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanWorkoutMO.m instead.

#import "_AdvisorActivityPlanWorkoutMO.h"

const struct AdvisorActivityPlanWorkoutMOAttributes AdvisorActivityPlanWorkoutMOAttributes = {
	.cloudId = @"cloudId",
	.loginId = @"loginId",
	.numberToPerform = @"numberToPerform",
	.performOp = @"performOp",
	.sortOrder = @"sortOrder",
	.workoutName = @"workoutName",
	.workoutTime = @"workoutTime",
};

const struct AdvisorActivityPlanWorkoutMORelationships AdvisorActivityPlanWorkoutMORelationships = {
	.activities = @"activities",
	.activityRecords = @"activityRecords",
	.phase = @"phase",
};

const struct AdvisorActivityPlanWorkoutMOUserInfo AdvisorActivityPlanWorkoutMOUserInfo = {
	.additionalHeaderFileName = @"AdvisorActivityPlanWorkoutEnums.h",
};

@implementation AdvisorActivityPlanWorkoutMOID
@end

@implementation _AdvisorActivityPlanWorkoutMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorActivityPlanWorkout" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorActivityPlanWorkout";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorActivityPlanWorkout" inManagedObjectContext:moc_];
}

- (AdvisorActivityPlanWorkoutMOID*)objectID {
	return (AdvisorActivityPlanWorkoutMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"numberToPerformValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"numberToPerform"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"performOpValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"performOp"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"workoutTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"workoutTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic loginId;

- (int16_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result shortValue];
}

- (void)setLoginIdValue:(int16_t)value_ {
	[self setLoginId:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result shortValue];
}

- (void)setPrimitiveLoginIdValue:(int16_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithShort:value_]];
}

@dynamic numberToPerform;

- (int16_t)numberToPerformValue {
	NSNumber *result = [self numberToPerform];
	return [result shortValue];
}

- (void)setNumberToPerformValue:(int16_t)value_ {
	[self setNumberToPerform:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveNumberToPerformValue {
	NSNumber *result = [self primitiveNumberToPerform];
	return [result shortValue];
}

- (void)setPrimitiveNumberToPerformValue:(int16_t)value_ {
	[self setPrimitiveNumberToPerform:[NSNumber numberWithShort:value_]];
}

@dynamic performOp;

- (PerformOperator)performOpValue {
	NSNumber *result = [self performOp];
	return [result shortValue];
}

- (void)setPerformOpValue:(PerformOperator)value_ {
	[self setPerformOp:[NSNumber numberWithShort:value_]];
}

- (PerformOperator)primitivePerformOpValue {
	NSNumber *result = [self primitivePerformOp];
	return [result shortValue];
}

- (void)setPrimitivePerformOpValue:(PerformOperator)value_ {
	[self setPrimitivePerformOp:[NSNumber numberWithShort:value_]];
}

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic workoutName;

@dynamic workoutTime;

- (WorkoutTime)workoutTimeValue {
	NSNumber *result = [self workoutTime];
	return [result shortValue];
}

- (void)setWorkoutTimeValue:(WorkoutTime)value_ {
	[self setWorkoutTime:[NSNumber numberWithShort:value_]];
}

- (WorkoutTime)primitiveWorkoutTimeValue {
	NSNumber *result = [self primitiveWorkoutTime];
	return [result shortValue];
}

- (void)setPrimitiveWorkoutTimeValue:(WorkoutTime)value_ {
	[self setPrimitiveWorkoutTime:[NSNumber numberWithShort:value_]];
}

@dynamic activities;

- (NSMutableSet*)activitiesSet {
	[self willAccessValueForKey:@"activities"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"activities"];

	[self didAccessValueForKey:@"activities"];
	return result;
}

@dynamic activityRecords;

- (NSMutableSet*)activityRecordsSet {
	[self willAccessValueForKey:@"activityRecords"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"activityRecords"];

	[self didAccessValueForKey:@"activityRecords"];
	return result;
}

@dynamic phase;

@end

