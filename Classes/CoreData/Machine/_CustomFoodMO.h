// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CustomFoodMO.h instead.

#import <CoreData/CoreData.h>

extern const struct CustomFoodMOAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *barcode;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *image;
	__unsafe_unretained NSString *imageId;
	__unsafe_unretained NSString *ingredients;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *servingSize;
	__unsafe_unretained NSString *servingSizeUnit;
	__unsafe_unretained NSString *servingsPerContainer;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
} CustomFoodMOAttributes;

extern const struct CustomFoodMORelationships {
	__unsafe_unretained NSString *category;
	__unsafe_unretained NSString *foodLogs;
	__unsafe_unretained NSString *mealPredictions;
	__unsafe_unretained NSString *nutrition;
	__unsafe_unretained NSString *shoppingListItems;
	__unsafe_unretained NSString *userImage;
} CustomFoodMORelationships;

@class CategoryMO;
@class FoodLogMO;
@class MealPredictionMO;
@class NutritionMO;
@class ShoppingListItem;
@class UserImageMO;

@interface CustomFoodMOID : NSManagedObjectID {}
@end

@interface _CustomFoodMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CustomFoodMOID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* barcode;

//- (BOOL)validateBarcode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSData* image;

//- (BOOL)validateImage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* imageId;

@property (atomic) int32_t imageIdValue;
- (int32_t)imageIdValue;
- (void)setImageIdValue:(int32_t)value_;

//- (BOOL)validateImageId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ingredients;

//- (BOOL)validateIngredients:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* servingSize;

@property (atomic) double servingSizeValue;
- (double)servingSizeValue;
- (void)setServingSizeValue:(double)value_;

//- (BOOL)validateServingSize:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* servingSizeUnit;

//- (BOOL)validateServingSizeUnit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* servingsPerContainer;

@property (atomic) double servingsPerContainerValue;
- (double)servingsPerContainerValue;
- (void)setServingsPerContainerValue:(double)value_;

//- (BOOL)validateServingsPerContainer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CategoryMO *category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *foodLogs;

- (NSMutableSet*)foodLogsSet;

@property (nonatomic, strong) NSSet *mealPredictions;

- (NSMutableSet*)mealPredictionsSet;

@property (nonatomic, strong) NutritionMO *nutrition;

//- (BOOL)validateNutrition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *shoppingListItems;

- (NSMutableSet*)shoppingListItemsSet;

@property (nonatomic, strong) UserImageMO *userImage;

//- (BOOL)validateUserImage:(id*)value_ error:(NSError**)error_;

@end

@interface _CustomFoodMO (FoodLogsCoreDataGeneratedAccessors)
- (void)addFoodLogs:(NSSet*)value_;
- (void)removeFoodLogs:(NSSet*)value_;
- (void)addFoodLogsObject:(FoodLogMO*)value_;
- (void)removeFoodLogsObject:(FoodLogMO*)value_;

@end

@interface _CustomFoodMO (MealPredictionsCoreDataGeneratedAccessors)
- (void)addMealPredictions:(NSSet*)value_;
- (void)removeMealPredictions:(NSSet*)value_;
- (void)addMealPredictionsObject:(MealPredictionMO*)value_;
- (void)removeMealPredictionsObject:(MealPredictionMO*)value_;

@end

@interface _CustomFoodMO (ShoppingListItemsCoreDataGeneratedAccessors)
- (void)addShoppingListItems:(NSSet*)value_;
- (void)removeShoppingListItems:(NSSet*)value_;
- (void)addShoppingListItemsObject:(ShoppingListItem*)value_;
- (void)removeShoppingListItemsObject:(ShoppingListItem*)value_;

@end

@interface _CustomFoodMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSString*)primitiveBarcode;
- (void)setPrimitiveBarcode:(NSString*)value;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSData*)primitiveImage;
- (void)setPrimitiveImage:(NSData*)value;

- (NSNumber*)primitiveImageId;
- (void)setPrimitiveImageId:(NSNumber*)value;

- (int32_t)primitiveImageIdValue;
- (void)setPrimitiveImageIdValue:(int32_t)value_;

- (NSString*)primitiveIngredients;
- (void)setPrimitiveIngredients:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveServingSize;
- (void)setPrimitiveServingSize:(NSNumber*)value;

- (double)primitiveServingSizeValue;
- (void)setPrimitiveServingSizeValue:(double)value_;

- (NSString*)primitiveServingSizeUnit;
- (void)setPrimitiveServingSizeUnit:(NSString*)value;

- (NSNumber*)primitiveServingsPerContainer;
- (void)setPrimitiveServingsPerContainer:(NSNumber*)value;

- (double)primitiveServingsPerContainerValue;
- (void)setPrimitiveServingsPerContainerValue:(double)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (CategoryMO*)primitiveCategory;
- (void)setPrimitiveCategory:(CategoryMO*)value;

- (NSMutableSet*)primitiveFoodLogs;
- (void)setPrimitiveFoodLogs:(NSMutableSet*)value;

- (NSMutableSet*)primitiveMealPredictions;
- (void)setPrimitiveMealPredictions:(NSMutableSet*)value;

- (NutritionMO*)primitiveNutrition;
- (void)setPrimitiveNutrition:(NutritionMO*)value;

- (NSMutableSet*)primitiveShoppingListItems;
- (void)setPrimitiveShoppingListItems:(NSMutableSet*)value;

- (UserImageMO*)primitiveUserImage;
- (void)setPrimitiveUserImage:(UserImageMO*)value;

@end
