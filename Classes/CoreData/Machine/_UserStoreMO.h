// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserStoreMO.h instead.

#import <CoreData/CoreData.h>

extern const struct UserStoreMOAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *address;
	__unsafe_unretained NSString *address2;
	__unsafe_unretained NSString *autoSort;
	__unsafe_unretained NSString *buyerCompassClient;
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *latitude;
	__unsafe_unretained NSString *longitude;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *phone;
	__unsafe_unretained NSString *state;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *storeCategoryStatus;
	__unsafe_unretained NSString *storeEntityId;
	__unsafe_unretained NSString *updatedOn;
	__unsafe_unretained NSString *zip;
} UserStoreMOAttributes;

extern const struct UserStoreMORelationships {
	__unsafe_unretained NSString *categories;
} UserStoreMORelationships;

@class StoreCategory;

@interface UserStoreMOID : NSManagedObjectID {}
@end

@interface _UserStoreMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserStoreMOID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* address;

//- (BOOL)validateAddress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* address2;

//- (BOOL)validateAddress2:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* autoSort;

@property (atomic) BOOL autoSortValue;
- (BOOL)autoSortValue;
- (void)setAutoSortValue:(BOOL)value_;

//- (BOOL)validateAutoSort:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* buyerCompassClient;

@property (atomic) BOOL buyerCompassClientValue;
- (BOOL)buyerCompassClientValue;
- (void)setBuyerCompassClientValue:(BOOL)value_;

//- (BOOL)validateBuyerCompassClient:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* city;

//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* latitude;

@property (atomic) float latitudeValue;
- (float)latitudeValue;
- (void)setLatitudeValue:(float)value_;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* longitude;

@property (atomic) float longitudeValue;
- (float)longitudeValue;
- (void)setLongitudeValue:(float)value_;

//- (BOOL)validateLongitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* phone;

//- (BOOL)validatePhone:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* state;

//- (BOOL)validateState:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* storeCategoryStatus;

//- (BOOL)validateStoreCategoryStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* storeEntityId;

@property (atomic) int32_t storeEntityIdValue;
- (int32_t)storeEntityIdValue;
- (void)setStoreEntityIdValue:(int32_t)value_;

//- (BOOL)validateStoreEntityId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* zip;

//- (BOOL)validateZip:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *categories;

- (NSMutableSet*)categoriesSet;

@end

@interface _UserStoreMO (CategoriesCoreDataGeneratedAccessors)
- (void)addCategories:(NSSet*)value_;
- (void)removeCategories:(NSSet*)value_;
- (void)addCategoriesObject:(StoreCategory*)value_;
- (void)removeCategoriesObject:(StoreCategory*)value_;

@end

@interface _UserStoreMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;

- (NSString*)primitiveAddress2;
- (void)setPrimitiveAddress2:(NSString*)value;

- (NSNumber*)primitiveAutoSort;
- (void)setPrimitiveAutoSort:(NSNumber*)value;

- (BOOL)primitiveAutoSortValue;
- (void)setPrimitiveAutoSortValue:(BOOL)value_;

- (NSNumber*)primitiveBuyerCompassClient;
- (void)setPrimitiveBuyerCompassClient:(NSNumber*)value;

- (BOOL)primitiveBuyerCompassClientValue;
- (void)setPrimitiveBuyerCompassClientValue:(BOOL)value_;

- (NSString*)primitiveCity;
- (void)setPrimitiveCity:(NSString*)value;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (float)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(float)value_;

- (NSNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSNumber*)value;

- (float)primitiveLongitudeValue;
- (void)setPrimitiveLongitudeValue:(float)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitivePhone;
- (void)setPrimitivePhone:(NSString*)value;

- (NSString*)primitiveState;
- (void)setPrimitiveState:(NSString*)value;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSString*)primitiveStoreCategoryStatus;
- (void)setPrimitiveStoreCategoryStatus:(NSString*)value;

- (NSNumber*)primitiveStoreEntityId;
- (void)setPrimitiveStoreEntityId:(NSNumber*)value;

- (int32_t)primitiveStoreEntityIdValue;
- (void)setPrimitiveStoreEntityIdValue:(int32_t)value_;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSString*)primitiveZip;
- (void)setPrimitiveZip:(NSString*)value;

- (NSMutableSet*)primitiveCategories;
- (void)setPrimitiveCategories:(NSMutableSet*)value;

@end
