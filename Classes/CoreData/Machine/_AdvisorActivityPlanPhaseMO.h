// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanPhaseMO.h instead.

#import <CoreData/CoreData.h>
#import "AdvisorPlanPhaseMO.h"

extern const struct AdvisorActivityPlanPhaseMORelationships {
	__unsafe_unretained NSString *workouts;
} AdvisorActivityPlanPhaseMORelationships;

@class AdvisorActivityPlanWorkoutMO;

@interface AdvisorActivityPlanPhaseMOID : AdvisorPlanPhaseMOID {}
@end

@interface _AdvisorActivityPlanPhaseMO : AdvisorPlanPhaseMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorActivityPlanPhaseMOID* objectID;

@property (nonatomic, strong) NSSet *workouts;

- (NSMutableSet*)workoutsSet;

@end

@interface _AdvisorActivityPlanPhaseMO (WorkoutsCoreDataGeneratedAccessors)
- (void)addWorkouts:(NSSet*)value_;
- (void)removeWorkouts:(NSSet*)value_;
- (void)addWorkoutsObject:(AdvisorActivityPlanWorkoutMO*)value_;
- (void)removeWorkoutsObject:(AdvisorActivityPlanWorkoutMO*)value_;

@end

@interface _AdvisorActivityPlanPhaseMO (CoreDataGeneratedPrimitiveAccessors)

- (NSMutableSet*)primitiveWorkouts;
- (void)setPrimitiveWorkouts:(NSMutableSet*)value;

@end
