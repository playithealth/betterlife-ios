// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ThreadMO.h instead.

#import <CoreData/CoreData.h>

extern const struct ThreadMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *createdOn;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *readOn;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *threadUpdatedOn;
	__unsafe_unretained NSString *type;
	__unsafe_unretained NSString *unsubscribed;
	__unsafe_unretained NSString *updatedOn;
} ThreadMOAttributes;

extern const struct ThreadMORelationships {
	__unsafe_unretained NSString *community;
	__unsafe_unretained NSString *messages;
	__unsafe_unretained NSString *userProfile;
} ThreadMORelationships;

@class CommunityMO;
@class ThreadMessageMO;
@class UserProfileMO;

@interface ThreadMOID : NSManagedObjectID {}
@end

@interface _ThreadMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ThreadMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* createdOn;

@property (atomic) int32_t createdOnValue;
- (int32_t)createdOnValue;
- (void)setCreatedOnValue:(int32_t)value_;

//- (BOOL)validateCreatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* readOn;

@property (atomic) int32_t readOnValue;
- (int32_t)readOnValue;
- (void)setReadOnValue:(int32_t)value_;

//- (BOOL)validateReadOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* threadUpdatedOn;

@property (atomic) int32_t threadUpdatedOnValue;
- (int32_t)threadUpdatedOnValue;
- (void)setThreadUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateThreadUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* type;

//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* unsubscribed;

@property (atomic) BOOL unsubscribedValue;
- (BOOL)unsubscribedValue;
- (void)setUnsubscribedValue:(BOOL)value_;

//- (BOOL)validateUnsubscribed:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CommunityMO *community;

//- (BOOL)validateCommunity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *messages;

- (NSMutableSet*)messagesSet;

@property (nonatomic, strong) UserProfileMO *userProfile;

//- (BOOL)validateUserProfile:(id*)value_ error:(NSError**)error_;

@end

@interface _ThreadMO (MessagesCoreDataGeneratedAccessors)
- (void)addMessages:(NSSet*)value_;
- (void)removeMessages:(NSSet*)value_;
- (void)addMessagesObject:(ThreadMessageMO*)value_;
- (void)removeMessagesObject:(ThreadMessageMO*)value_;

@end

@interface _ThreadMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveCreatedOn;
- (void)setPrimitiveCreatedOn:(NSNumber*)value;

- (int32_t)primitiveCreatedOnValue;
- (void)setPrimitiveCreatedOnValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveReadOn;
- (void)setPrimitiveReadOn:(NSNumber*)value;

- (int32_t)primitiveReadOnValue;
- (void)setPrimitiveReadOnValue:(int32_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveThreadUpdatedOn;
- (void)setPrimitiveThreadUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveThreadUpdatedOnValue;
- (void)setPrimitiveThreadUpdatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveUnsubscribed;
- (void)setPrimitiveUnsubscribed:(NSNumber*)value;

- (BOOL)primitiveUnsubscribedValue;
- (void)setPrimitiveUnsubscribedValue:(BOOL)value_;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (CommunityMO*)primitiveCommunity;
- (void)setPrimitiveCommunity:(CommunityMO*)value;

- (NSMutableSet*)primitiveMessages;
- (void)setPrimitiveMessages:(NSMutableSet*)value;

- (UserProfileMO*)primitiveUserProfile;
- (void)setPrimitiveUserProfile:(UserProfileMO*)value;

@end
