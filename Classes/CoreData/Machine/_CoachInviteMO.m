// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CoachInviteMO.m instead.

#import "_CoachInviteMO.h"

const struct CoachInviteMOAttributes CoachInviteMOAttributes = {
	.advisorId = @"advisorId",
	.advisorName = @"advisorName",
	.code = @"code",
	.createdOn = @"createdOn",
	.loginId = @"loginId",
	.rootEntityName = @"rootEntityName",
	.status = @"status",
};

@implementation CoachInviteMOID
@end

@implementation _CoachInviteMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CoachInvite" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CoachInvite";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CoachInvite" inManagedObjectContext:moc_];
}

- (CoachInviteMOID*)objectID {
	return (CoachInviteMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"advisorIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"advisorId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"createdOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"createdOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic advisorId;

- (int32_t)advisorIdValue {
	NSNumber *result = [self advisorId];
	return [result intValue];
}

- (void)setAdvisorIdValue:(int32_t)value_ {
	[self setAdvisorId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAdvisorIdValue {
	NSNumber *result = [self primitiveAdvisorId];
	return [result intValue];
}

- (void)setPrimitiveAdvisorIdValue:(int32_t)value_ {
	[self setPrimitiveAdvisorId:[NSNumber numberWithInt:value_]];
}

@dynamic advisorName;

@dynamic code;

@dynamic createdOn;

- (int32_t)createdOnValue {
	NSNumber *result = [self createdOn];
	return [result intValue];
}

- (void)setCreatedOnValue:(int32_t)value_ {
	[self setCreatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCreatedOnValue {
	NSNumber *result = [self primitiveCreatedOn];
	return [result intValue];
}

- (void)setPrimitiveCreatedOnValue:(int32_t)value_ {
	[self setPrimitiveCreatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic rootEntityName;

@dynamic status;

@end

