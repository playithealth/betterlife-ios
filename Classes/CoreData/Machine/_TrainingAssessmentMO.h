// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrainingAssessmentMO.h instead.

#import <CoreData/CoreData.h>

extern const struct TrainingAssessmentMOAttributes {
	__unsafe_unretained NSString *activityLevel;
	__unsafe_unretained NSString *age;
	__unsafe_unretained NSString *bmi;
	__unsafe_unretained NSString *bmr;
	__unsafe_unretained NSString *bodyFatPercent;
	__unsafe_unretained NSString *chest;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *crunches;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *height;
	__unsafe_unretained NSString *hips;
	__unsafe_unretained NSString *leftBicep;
	__unsafe_unretained NSString *leftThigh;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *mileRun;
	__unsafe_unretained NSString *neck;
	__unsafe_unretained NSString *pullups;
	__unsafe_unretained NSString *pushups;
	__unsafe_unretained NSString *rightBicep;
	__unsafe_unretained NSString *rightThigh;
	__unsafe_unretained NSString *sex;
	__unsafe_unretained NSString *squats;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *stomach;
	__unsafe_unretained NSString *updatedOn;
	__unsafe_unretained NSString *waist;
	__unsafe_unretained NSString *weight;
} TrainingAssessmentMOAttributes;

extern const struct TrainingAssessmentMORelationships {
	__unsafe_unretained NSString *tracking;
} TrainingAssessmentMORelationships;

@class TrackingMO;

@interface TrainingAssessmentMOID : NSManagedObjectID {}
@end

@interface _TrainingAssessmentMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TrainingAssessmentMOID* objectID;

@property (nonatomic, strong) NSNumber* activityLevel;

@property (atomic) double activityLevelValue;
- (double)activityLevelValue;
- (void)setActivityLevelValue:(double)value_;

//- (BOOL)validateActivityLevel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* age;

@property (atomic) int16_t ageValue;
- (int16_t)ageValue;
- (void)setAgeValue:(int16_t)value_;

//- (BOOL)validateAge:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* bmi;

@property (atomic) double bmiValue;
- (double)bmiValue;
- (void)setBmiValue:(double)value_;

//- (BOOL)validateBmi:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* bmr;

@property (atomic) double bmrValue;
- (double)bmrValue;
- (void)setBmrValue:(double)value_;

//- (BOOL)validateBmr:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* bodyFatPercent;

@property (atomic) int16_t bodyFatPercentValue;
- (int16_t)bodyFatPercentValue;
- (void)setBodyFatPercentValue:(int16_t)value_;

//- (BOOL)validateBodyFatPercent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* chest;

@property (atomic) double chestValue;
- (double)chestValue;
- (void)setChestValue:(double)value_;

//- (BOOL)validateChest:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* crunches;

@property (atomic) int16_t crunchesValue;
- (int16_t)crunchesValue;
- (void)setCrunchesValue:(int16_t)value_;

//- (BOOL)validateCrunches:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* height;

@property (atomic) double heightValue;
- (double)heightValue;
- (void)setHeightValue:(double)value_;

//- (BOOL)validateHeight:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* hips;

@property (atomic) double hipsValue;
- (double)hipsValue;
- (void)setHipsValue:(double)value_;

//- (BOOL)validateHips:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* leftBicep;

@property (atomic) double leftBicepValue;
- (double)leftBicepValue;
- (void)setLeftBicepValue:(double)value_;

//- (BOOL)validateLeftBicep:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* leftThigh;

@property (atomic) double leftThighValue;
- (double)leftThighValue;
- (void)setLeftThighValue:(double)value_;

//- (BOOL)validateLeftThigh:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* mileRun;

@property (atomic) double mileRunValue;
- (double)mileRunValue;
- (void)setMileRunValue:(double)value_;

//- (BOOL)validateMileRun:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* neck;

@property (atomic) double neckValue;
- (double)neckValue;
- (void)setNeckValue:(double)value_;

//- (BOOL)validateNeck:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* pullups;

@property (atomic) int16_t pullupsValue;
- (int16_t)pullupsValue;
- (void)setPullupsValue:(int16_t)value_;

//- (BOOL)validatePullups:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* pushups;

@property (atomic) int16_t pushupsValue;
- (int16_t)pushupsValue;
- (void)setPushupsValue:(int16_t)value_;

//- (BOOL)validatePushups:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rightBicep;

@property (atomic) double rightBicepValue;
- (double)rightBicepValue;
- (void)setRightBicepValue:(double)value_;

//- (BOOL)validateRightBicep:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rightThigh;

@property (atomic) double rightThighValue;
- (double)rightThighValue;
- (void)setRightThighValue:(double)value_;

//- (BOOL)validateRightThigh:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sex;

@property (atomic) int16_t sexValue;
- (int16_t)sexValue;
- (void)setSexValue:(int16_t)value_;

//- (BOOL)validateSex:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* squats;

@property (atomic) int16_t squatsValue;
- (int16_t)squatsValue;
- (void)setSquatsValue:(int16_t)value_;

//- (BOOL)validateSquats:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stomach;

@property (atomic) double stomachValue;
- (double)stomachValue;
- (void)setStomachValue:(double)value_;

//- (BOOL)validateStomach:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* waist;

@property (atomic) double waistValue;
- (double)waistValue;
- (void)setWaistValue:(double)value_;

//- (BOOL)validateWaist:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* weight;

@property (atomic) double weightValue;
- (double)weightValue;
- (void)setWeightValue:(double)value_;

//- (BOOL)validateWeight:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TrackingMO *tracking;

//- (BOOL)validateTracking:(id*)value_ error:(NSError**)error_;

@end

@interface _TrainingAssessmentMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveActivityLevel;
- (void)setPrimitiveActivityLevel:(NSNumber*)value;

- (double)primitiveActivityLevelValue;
- (void)setPrimitiveActivityLevelValue:(double)value_;

- (NSNumber*)primitiveAge;
- (void)setPrimitiveAge:(NSNumber*)value;

- (int16_t)primitiveAgeValue;
- (void)setPrimitiveAgeValue:(int16_t)value_;

- (NSNumber*)primitiveBmi;
- (void)setPrimitiveBmi:(NSNumber*)value;

- (double)primitiveBmiValue;
- (void)setPrimitiveBmiValue:(double)value_;

- (NSNumber*)primitiveBmr;
- (void)setPrimitiveBmr:(NSNumber*)value;

- (double)primitiveBmrValue;
- (void)setPrimitiveBmrValue:(double)value_;

- (NSNumber*)primitiveBodyFatPercent;
- (void)setPrimitiveBodyFatPercent:(NSNumber*)value;

- (int16_t)primitiveBodyFatPercentValue;
- (void)setPrimitiveBodyFatPercentValue:(int16_t)value_;

- (NSNumber*)primitiveChest;
- (void)setPrimitiveChest:(NSNumber*)value;

- (double)primitiveChestValue;
- (void)setPrimitiveChestValue:(double)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveCrunches;
- (void)setPrimitiveCrunches:(NSNumber*)value;

- (int16_t)primitiveCrunchesValue;
- (void)setPrimitiveCrunchesValue:(int16_t)value_;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSNumber*)primitiveHeight;
- (void)setPrimitiveHeight:(NSNumber*)value;

- (double)primitiveHeightValue;
- (void)setPrimitiveHeightValue:(double)value_;

- (NSNumber*)primitiveHips;
- (void)setPrimitiveHips:(NSNumber*)value;

- (double)primitiveHipsValue;
- (void)setPrimitiveHipsValue:(double)value_;

- (NSNumber*)primitiveLeftBicep;
- (void)setPrimitiveLeftBicep:(NSNumber*)value;

- (double)primitiveLeftBicepValue;
- (void)setPrimitiveLeftBicepValue:(double)value_;

- (NSNumber*)primitiveLeftThigh;
- (void)setPrimitiveLeftThigh:(NSNumber*)value;

- (double)primitiveLeftThighValue;
- (void)setPrimitiveLeftThighValue:(double)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSNumber*)primitiveMileRun;
- (void)setPrimitiveMileRun:(NSNumber*)value;

- (double)primitiveMileRunValue;
- (void)setPrimitiveMileRunValue:(double)value_;

- (NSNumber*)primitiveNeck;
- (void)setPrimitiveNeck:(NSNumber*)value;

- (double)primitiveNeckValue;
- (void)setPrimitiveNeckValue:(double)value_;

- (NSNumber*)primitivePullups;
- (void)setPrimitivePullups:(NSNumber*)value;

- (int16_t)primitivePullupsValue;
- (void)setPrimitivePullupsValue:(int16_t)value_;

- (NSNumber*)primitivePushups;
- (void)setPrimitivePushups:(NSNumber*)value;

- (int16_t)primitivePushupsValue;
- (void)setPrimitivePushupsValue:(int16_t)value_;

- (NSNumber*)primitiveRightBicep;
- (void)setPrimitiveRightBicep:(NSNumber*)value;

- (double)primitiveRightBicepValue;
- (void)setPrimitiveRightBicepValue:(double)value_;

- (NSNumber*)primitiveRightThigh;
- (void)setPrimitiveRightThigh:(NSNumber*)value;

- (double)primitiveRightThighValue;
- (void)setPrimitiveRightThighValue:(double)value_;

- (NSNumber*)primitiveSex;
- (void)setPrimitiveSex:(NSNumber*)value;

- (int16_t)primitiveSexValue;
- (void)setPrimitiveSexValue:(int16_t)value_;

- (NSNumber*)primitiveSquats;
- (void)setPrimitiveSquats:(NSNumber*)value;

- (int16_t)primitiveSquatsValue;
- (void)setPrimitiveSquatsValue:(int16_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveStomach;
- (void)setPrimitiveStomach:(NSNumber*)value;

- (double)primitiveStomachValue;
- (void)setPrimitiveStomachValue:(double)value_;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveWaist;
- (void)setPrimitiveWaist:(NSNumber*)value;

- (double)primitiveWaistValue;
- (void)setPrimitiveWaistValue:(double)value_;

- (NSNumber*)primitiveWeight;
- (void)setPrimitiveWeight:(NSNumber*)value;

- (double)primitiveWeightValue;
- (void)setPrimitiveWeightValue:(double)value_;

- (TrackingMO*)primitiveTracking;
- (void)setPrimitiveTracking:(TrackingMO*)value;

@end
