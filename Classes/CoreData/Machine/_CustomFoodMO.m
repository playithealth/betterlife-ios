// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CustomFoodMO.m instead.

#import "_CustomFoodMO.h"

const struct CustomFoodMOAttributes CustomFoodMOAttributes = {
	.accountId = @"accountId",
	.barcode = @"barcode",
	.cloudId = @"cloudId",
	.image = @"image",
	.imageId = @"imageId",
	.ingredients = @"ingredients",
	.name = @"name",
	.servingSize = @"servingSize",
	.servingSizeUnit = @"servingSizeUnit",
	.servingsPerContainer = @"servingsPerContainer",
	.status = @"status",
	.updatedOn = @"updatedOn",
};

const struct CustomFoodMORelationships CustomFoodMORelationships = {
	.category = @"category",
	.foodLogs = @"foodLogs",
	.mealPredictions = @"mealPredictions",
	.nutrition = @"nutrition",
	.shoppingListItems = @"shoppingListItems",
	.userImage = @"userImage",
};

@implementation CustomFoodMOID
@end

@implementation _CustomFoodMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CustomFood" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CustomFood";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CustomFood" inManagedObjectContext:moc_];
}

- (CustomFoodMOID*)objectID {
	return (CustomFoodMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"imageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"imageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"servingSizeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"servingSize"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"servingsPerContainerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"servingsPerContainer"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic barcode;

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic image;

@dynamic imageId;

- (int32_t)imageIdValue {
	NSNumber *result = [self imageId];
	return [result intValue];
}

- (void)setImageIdValue:(int32_t)value_ {
	[self setImageId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImageIdValue {
	NSNumber *result = [self primitiveImageId];
	return [result intValue];
}

- (void)setPrimitiveImageIdValue:(int32_t)value_ {
	[self setPrimitiveImageId:[NSNumber numberWithInt:value_]];
}

@dynamic ingredients;

@dynamic name;

@dynamic servingSize;

- (double)servingSizeValue {
	NSNumber *result = [self servingSize];
	return [result doubleValue];
}

- (void)setServingSizeValue:(double)value_ {
	[self setServingSize:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveServingSizeValue {
	NSNumber *result = [self primitiveServingSize];
	return [result doubleValue];
}

- (void)setPrimitiveServingSizeValue:(double)value_ {
	[self setPrimitiveServingSize:[NSNumber numberWithDouble:value_]];
}

@dynamic servingSizeUnit;

@dynamic servingsPerContainer;

- (double)servingsPerContainerValue {
	NSNumber *result = [self servingsPerContainer];
	return [result doubleValue];
}

- (void)setServingsPerContainerValue:(double)value_ {
	[self setServingsPerContainer:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveServingsPerContainerValue {
	NSNumber *result = [self primitiveServingsPerContainer];
	return [result doubleValue];
}

- (void)setPrimitiveServingsPerContainerValue:(double)value_ {
	[self setPrimitiveServingsPerContainer:[NSNumber numberWithDouble:value_]];
}

@dynamic status;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic category;

@dynamic foodLogs;

- (NSMutableSet*)foodLogsSet {
	[self willAccessValueForKey:@"foodLogs"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"foodLogs"];

	[self didAccessValueForKey:@"foodLogs"];
	return result;
}

@dynamic mealPredictions;

- (NSMutableSet*)mealPredictionsSet {
	[self willAccessValueForKey:@"mealPredictions"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mealPredictions"];

	[self didAccessValueForKey:@"mealPredictions"];
	return result;
}

@dynamic nutrition;

@dynamic shoppingListItems;

- (NSMutableSet*)shoppingListItemsSet {
	[self willAccessValueForKey:@"shoppingListItems"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"shoppingListItems"];

	[self didAccessValueForKey:@"shoppingListItems"];
	return result;
}

@dynamic userImage;

@end

