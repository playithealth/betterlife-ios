// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorPlanMO.m instead.

#import "_AdvisorPlanMO.h"

const struct AdvisorPlanMOAttributes AdvisorPlanMOAttributes = {
	.advisorId = @"advisorId",
	.advisorName = @"advisorName",
	.cloudId = @"cloudId",
	.isNew = @"isNew",
	.isRepeating = @"isRepeating",
	.loginId = @"loginId",
	.planDescription = @"planDescription",
	.planName = @"planName",
	.status = @"status",
};

const struct AdvisorPlanMORelationships AdvisorPlanMORelationships = {
	.phases = @"phases",
	.timelines = @"timelines",
};

@implementation AdvisorPlanMOID
@end

@implementation _AdvisorPlanMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AbstractAdvisorPlan" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AbstractAdvisorPlan";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AbstractAdvisorPlan" inManagedObjectContext:moc_];
}

- (AdvisorPlanMOID*)objectID {
	return (AdvisorPlanMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"advisorIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"advisorId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isNewValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isNew"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isRepeatingValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRepeating"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic advisorId;

- (int32_t)advisorIdValue {
	NSNumber *result = [self advisorId];
	return [result intValue];
}

- (void)setAdvisorIdValue:(int32_t)value_ {
	[self setAdvisorId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAdvisorIdValue {
	NSNumber *result = [self primitiveAdvisorId];
	return [result intValue];
}

- (void)setPrimitiveAdvisorIdValue:(int32_t)value_ {
	[self setPrimitiveAdvisorId:[NSNumber numberWithInt:value_]];
}

@dynamic advisorName;

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic isNew;

- (BOOL)isNewValue {
	NSNumber *result = [self isNew];
	return [result boolValue];
}

- (void)setIsNewValue:(BOOL)value_ {
	[self setIsNew:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsNewValue {
	NSNumber *result = [self primitiveIsNew];
	return [result boolValue];
}

- (void)setPrimitiveIsNewValue:(BOOL)value_ {
	[self setPrimitiveIsNew:[NSNumber numberWithBool:value_]];
}

@dynamic isRepeating;

- (BOOL)isRepeatingValue {
	NSNumber *result = [self isRepeating];
	return [result boolValue];
}

- (void)setIsRepeatingValue:(BOOL)value_ {
	[self setIsRepeating:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsRepeatingValue {
	NSNumber *result = [self primitiveIsRepeating];
	return [result boolValue];
}

- (void)setPrimitiveIsRepeatingValue:(BOOL)value_ {
	[self setPrimitiveIsRepeating:[NSNumber numberWithBool:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic planDescription;

@dynamic planName;

@dynamic status;

@dynamic phases;

- (NSMutableSet*)phasesSet {
	[self willAccessValueForKey:@"phases"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"phases"];

	[self didAccessValueForKey:@"phases"];
	return result;
}

@dynamic timelines;

- (NSMutableSet*)timelinesSet {
	[self willAccessValueForKey:@"timelines"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"timelines"];

	[self didAccessValueForKey:@"timelines"];
	return result;
}

@end

