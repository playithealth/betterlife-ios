// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RecipeMO.h instead.

#import <CoreData/CoreData.h>

extern const struct RecipeMOAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *advisorId;
	__unsafe_unretained NSString *advisorName;
	__unsafe_unretained NSString *calories;
	__unsafe_unretained NSString *caloriesFromFat;
	__unsafe_unretained NSString *carbs;
	__unsafe_unretained NSString *cholesterol;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *cookTime;
	__unsafe_unretained NSString *cookTimeUnit;
	__unsafe_unretained NSString *externalId;
	__unsafe_unretained NSString *fat;
	__unsafe_unretained NSString *frequency;
	__unsafe_unretained NSString *imageId;
	__unsafe_unretained NSString *imageUrl;
	__unsafe_unretained NSString *instructions;
	__unsafe_unretained NSString *isOwner;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *nutritionSummary;
	__unsafe_unretained NSString *prepTime;
	__unsafe_unretained NSString *protein;
	__unsafe_unretained NSString *rating;
	__unsafe_unretained NSString *recommendedDate;
	__unsafe_unretained NSString *removed;
	__unsafe_unretained NSString *satfat;
	__unsafe_unretained NSString *serves;
	__unsafe_unretained NSString *sodium;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
	__unsafe_unretained NSString *url;
} RecipeMOAttributes;

extern const struct RecipeMORelationships {
	__unsafe_unretained NSString *advisorGroups;
	__unsafe_unretained NSString *foodLog;
	__unsafe_unretained NSString *ingredients;
	__unsafe_unretained NSString *mealPredictions;
	__unsafe_unretained NSString *nutrition;
	__unsafe_unretained NSString *plannerDays;
	__unsafe_unretained NSString *tags;
} RecipeMORelationships;

@class AdvisorGroup;
@class FoodLogMO;
@class MealIngredient;
@class MealPredictionMO;
@class NutritionMO;
@class MealPlannerDay;
@class RecipeTagMO;

@interface RecipeMOID : NSManagedObjectID {}
@end

@interface _RecipeMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) RecipeMOID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* advisorId;

@property (atomic) int32_t advisorIdValue;
- (int32_t)advisorIdValue;
- (void)setAdvisorIdValue:(int32_t)value_;

//- (BOOL)validateAdvisorId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* advisorName;

//- (BOOL)validateAdvisorName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* calories;

@property (atomic) int16_t caloriesValue;
- (int16_t)caloriesValue;
- (void)setCaloriesValue:(int16_t)value_;

//- (BOOL)validateCalories:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* caloriesFromFat;

@property (atomic) int16_t caloriesFromFatValue;
- (int16_t)caloriesFromFatValue;
- (void)setCaloriesFromFatValue:(int16_t)value_;

//- (BOOL)validateCaloriesFromFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* carbs;

@property (atomic) double carbsValue;
- (double)carbsValue;
- (void)setCarbsValue:(double)value_;

//- (BOOL)validateCarbs:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cholesterol;

@property (atomic) double cholesterolValue;
- (double)cholesterolValue;
- (void)setCholesterolValue:(double)value_;

//- (BOOL)validateCholesterol:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cookTime;

@property (atomic) int32_t cookTimeValue;
- (int32_t)cookTimeValue;
- (void)setCookTimeValue:(int32_t)value_;

//- (BOOL)validateCookTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* cookTimeUnit;

//- (BOOL)validateCookTimeUnit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* externalId;

//- (BOOL)validateExternalId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* fat;

@property (atomic) double fatValue;
- (double)fatValue;
- (void)setFatValue:(double)value_;

//- (BOOL)validateFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* frequency;

@property (atomic) int16_t frequencyValue;
- (int16_t)frequencyValue;
- (void)setFrequencyValue:(int16_t)value_;

//- (BOOL)validateFrequency:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* imageId;

@property (atomic) int32_t imageIdValue;
- (int32_t)imageIdValue;
- (void)setImageIdValue:(int32_t)value_;

//- (BOOL)validateImageId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* imageUrl;

//- (BOOL)validateImageUrl:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* instructions;

//- (BOOL)validateInstructions:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isOwner;

@property (atomic) BOOL isOwnerValue;
- (BOOL)isOwnerValue;
- (void)setIsOwnerValue:(BOOL)value_;

//- (BOOL)validateIsOwner:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nutritionSummary;

//- (BOOL)validateNutritionSummary:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* prepTime;

@property (atomic) int32_t prepTimeValue;
- (int32_t)prepTimeValue;
- (void)setPrepTimeValue:(int32_t)value_;

//- (BOOL)validatePrepTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* protein;

@property (atomic) double proteinValue;
- (double)proteinValue;
- (void)setProteinValue:(double)value_;

//- (BOOL)validateProtein:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* rating;

@property (atomic) int16_t ratingValue;
- (int16_t)ratingValue;
- (void)setRatingValue:(int16_t)value_;

//- (BOOL)validateRating:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* recommendedDate;

//- (BOOL)validateRecommendedDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* removed;

@property (atomic) BOOL removedValue;
- (BOOL)removedValue;
- (void)setRemovedValue:(BOOL)value_;

//- (BOOL)validateRemoved:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* satfat;

@property (atomic) double satfatValue;
- (double)satfatValue;
- (void)setSatfatValue:(double)value_;

//- (BOOL)validateSatfat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* serves;

@property (atomic) int16_t servesValue;
- (int16_t)servesValue;
- (void)setServesValue:(int16_t)value_;

//- (BOOL)validateServes:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sodium;

@property (atomic) double sodiumValue;
- (double)sodiumValue;
- (void)setSodiumValue:(double)value_;

//- (BOOL)validateSodium:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* url;

//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *advisorGroups;

- (NSMutableSet*)advisorGroupsSet;

@property (nonatomic, strong) NSSet *foodLog;

- (NSMutableSet*)foodLogSet;

@property (nonatomic, strong) NSSet *ingredients;

- (NSMutableSet*)ingredientsSet;

@property (nonatomic, strong) NSSet *mealPredictions;

- (NSMutableSet*)mealPredictionsSet;

@property (nonatomic, strong) NutritionMO *nutrition;

//- (BOOL)validateNutrition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *plannerDays;

- (NSMutableSet*)plannerDaysSet;

@property (nonatomic, strong) NSSet *tags;

- (NSMutableSet*)tagsSet;

@end

@interface _RecipeMO (AdvisorGroupsCoreDataGeneratedAccessors)
- (void)addAdvisorGroups:(NSSet*)value_;
- (void)removeAdvisorGroups:(NSSet*)value_;
- (void)addAdvisorGroupsObject:(AdvisorGroup*)value_;
- (void)removeAdvisorGroupsObject:(AdvisorGroup*)value_;

@end

@interface _RecipeMO (FoodLogCoreDataGeneratedAccessors)
- (void)addFoodLog:(NSSet*)value_;
- (void)removeFoodLog:(NSSet*)value_;
- (void)addFoodLogObject:(FoodLogMO*)value_;
- (void)removeFoodLogObject:(FoodLogMO*)value_;

@end

@interface _RecipeMO (IngredientsCoreDataGeneratedAccessors)
- (void)addIngredients:(NSSet*)value_;
- (void)removeIngredients:(NSSet*)value_;
- (void)addIngredientsObject:(MealIngredient*)value_;
- (void)removeIngredientsObject:(MealIngredient*)value_;

@end

@interface _RecipeMO (MealPredictionsCoreDataGeneratedAccessors)
- (void)addMealPredictions:(NSSet*)value_;
- (void)removeMealPredictions:(NSSet*)value_;
- (void)addMealPredictionsObject:(MealPredictionMO*)value_;
- (void)removeMealPredictionsObject:(MealPredictionMO*)value_;

@end

@interface _RecipeMO (PlannerDaysCoreDataGeneratedAccessors)
- (void)addPlannerDays:(NSSet*)value_;
- (void)removePlannerDays:(NSSet*)value_;
- (void)addPlannerDaysObject:(MealPlannerDay*)value_;
- (void)removePlannerDaysObject:(MealPlannerDay*)value_;

@end

@interface _RecipeMO (TagsCoreDataGeneratedAccessors)
- (void)addTags:(NSSet*)value_;
- (void)removeTags:(NSSet*)value_;
- (void)addTagsObject:(RecipeTagMO*)value_;
- (void)removeTagsObject:(RecipeTagMO*)value_;

@end

@interface _RecipeMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSNumber*)primitiveAdvisorId;
- (void)setPrimitiveAdvisorId:(NSNumber*)value;

- (int32_t)primitiveAdvisorIdValue;
- (void)setPrimitiveAdvisorIdValue:(int32_t)value_;

- (NSString*)primitiveAdvisorName;
- (void)setPrimitiveAdvisorName:(NSString*)value;

- (NSNumber*)primitiveCalories;
- (void)setPrimitiveCalories:(NSNumber*)value;

- (int16_t)primitiveCaloriesValue;
- (void)setPrimitiveCaloriesValue:(int16_t)value_;

- (NSNumber*)primitiveCaloriesFromFat;
- (void)setPrimitiveCaloriesFromFat:(NSNumber*)value;

- (int16_t)primitiveCaloriesFromFatValue;
- (void)setPrimitiveCaloriesFromFatValue:(int16_t)value_;

- (NSNumber*)primitiveCarbs;
- (void)setPrimitiveCarbs:(NSNumber*)value;

- (double)primitiveCarbsValue;
- (void)setPrimitiveCarbsValue:(double)value_;

- (NSNumber*)primitiveCholesterol;
- (void)setPrimitiveCholesterol:(NSNumber*)value;

- (double)primitiveCholesterolValue;
- (void)setPrimitiveCholesterolValue:(double)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveCookTime;
- (void)setPrimitiveCookTime:(NSNumber*)value;

- (int32_t)primitiveCookTimeValue;
- (void)setPrimitiveCookTimeValue:(int32_t)value_;

- (NSString*)primitiveCookTimeUnit;
- (void)setPrimitiveCookTimeUnit:(NSString*)value;

- (NSString*)primitiveExternalId;
- (void)setPrimitiveExternalId:(NSString*)value;

- (NSNumber*)primitiveFat;
- (void)setPrimitiveFat:(NSNumber*)value;

- (double)primitiveFatValue;
- (void)setPrimitiveFatValue:(double)value_;

- (NSNumber*)primitiveFrequency;
- (void)setPrimitiveFrequency:(NSNumber*)value;

- (int16_t)primitiveFrequencyValue;
- (void)setPrimitiveFrequencyValue:(int16_t)value_;

- (NSNumber*)primitiveImageId;
- (void)setPrimitiveImageId:(NSNumber*)value;

- (int32_t)primitiveImageIdValue;
- (void)setPrimitiveImageIdValue:(int32_t)value_;

- (NSString*)primitiveImageUrl;
- (void)setPrimitiveImageUrl:(NSString*)value;

- (NSString*)primitiveInstructions;
- (void)setPrimitiveInstructions:(NSString*)value;

- (NSNumber*)primitiveIsOwner;
- (void)setPrimitiveIsOwner:(NSNumber*)value;

- (BOOL)primitiveIsOwnerValue;
- (void)setPrimitiveIsOwnerValue:(BOOL)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveNutritionSummary;
- (void)setPrimitiveNutritionSummary:(NSString*)value;

- (NSNumber*)primitivePrepTime;
- (void)setPrimitivePrepTime:(NSNumber*)value;

- (int32_t)primitivePrepTimeValue;
- (void)setPrimitivePrepTimeValue:(int32_t)value_;

- (NSNumber*)primitiveProtein;
- (void)setPrimitiveProtein:(NSNumber*)value;

- (double)primitiveProteinValue;
- (void)setPrimitiveProteinValue:(double)value_;

- (NSNumber*)primitiveRating;
- (void)setPrimitiveRating:(NSNumber*)value;

- (int16_t)primitiveRatingValue;
- (void)setPrimitiveRatingValue:(int16_t)value_;

- (NSDate*)primitiveRecommendedDate;
- (void)setPrimitiveRecommendedDate:(NSDate*)value;

- (NSNumber*)primitiveRemoved;
- (void)setPrimitiveRemoved:(NSNumber*)value;

- (BOOL)primitiveRemovedValue;
- (void)setPrimitiveRemovedValue:(BOOL)value_;

- (NSNumber*)primitiveSatfat;
- (void)setPrimitiveSatfat:(NSNumber*)value;

- (double)primitiveSatfatValue;
- (void)setPrimitiveSatfatValue:(double)value_;

- (NSNumber*)primitiveServes;
- (void)setPrimitiveServes:(NSNumber*)value;

- (int16_t)primitiveServesValue;
- (void)setPrimitiveServesValue:(int16_t)value_;

- (NSNumber*)primitiveSodium;
- (void)setPrimitiveSodium:(NSNumber*)value;

- (double)primitiveSodiumValue;
- (void)setPrimitiveSodiumValue:(double)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;

- (NSMutableSet*)primitiveAdvisorGroups;
- (void)setPrimitiveAdvisorGroups:(NSMutableSet*)value;

- (NSMutableSet*)primitiveFoodLog;
- (void)setPrimitiveFoodLog:(NSMutableSet*)value;

- (NSMutableSet*)primitiveIngredients;
- (void)setPrimitiveIngredients:(NSMutableSet*)value;

- (NSMutableSet*)primitiveMealPredictions;
- (void)setPrimitiveMealPredictions:(NSMutableSet*)value;

- (NutritionMO*)primitiveNutrition;
- (void)setPrimitiveNutrition:(NutritionMO*)value;

- (NSMutableSet*)primitivePlannerDays;
- (void)setPrimitivePlannerDays:(NSMutableSet*)value;

- (NSMutableSet*)primitiveTags;
- (void)setPrimitiveTags:(NSMutableSet*)value;

@end
