// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MessageMO.h instead.

#import <CoreData/CoreData.h>

extern const struct MessageMOAttributes {
	__unsafe_unretained NSString *body;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *createdOn;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *subject;
	__unsafe_unretained NSString *updatedOn;
} MessageMOAttributes;

extern const struct MessageMORelationships {
	__unsafe_unretained NSString *deliveries;
} MessageMORelationships;

@class MessageDeliveryMO;

@interface MessageMOID : NSManagedObjectID {}
@end

@interface _MessageMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MessageMOID* objectID;

@property (nonatomic, strong) NSString* body;

//- (BOOL)validateBody:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* createdOn;

@property (atomic) int32_t createdOnValue;
- (int32_t)createdOnValue;
- (void)setCreatedOnValue:(int32_t)value_;

//- (BOOL)validateCreatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* subject;

//- (BOOL)validateSubject:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *deliveries;

- (NSMutableSet*)deliveriesSet;

@end

@interface _MessageMO (DeliveriesCoreDataGeneratedAccessors)
- (void)addDeliveries:(NSSet*)value_;
- (void)removeDeliveries:(NSSet*)value_;
- (void)addDeliveriesObject:(MessageDeliveryMO*)value_;
- (void)removeDeliveriesObject:(MessageDeliveryMO*)value_;

@end

@interface _MessageMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveBody;
- (void)setPrimitiveBody:(NSString*)value;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveCreatedOn;
- (void)setPrimitiveCreatedOn:(NSNumber*)value;

- (int32_t)primitiveCreatedOnValue;
- (void)setPrimitiveCreatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveSubject;
- (void)setPrimitiveSubject:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (NSMutableSet*)primitiveDeliveries;
- (void)setPrimitiveDeliveries:(NSMutableSet*)value;

@end
