// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityMO.h instead.

#import <CoreData/CoreData.h>

extern const struct ActivityMOAttributes {
	__unsafe_unretained NSString *activityDescription;
	__unsafe_unretained NSString *activityName;
	__unsafe_unretained NSString *basedOnId;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *factor;
	__unsafe_unretained NSString *isTimedActivity;
	__unsafe_unretained NSString *link;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *routineId;
	__unsafe_unretained NSString *sortOrder;
	__unsafe_unretained NSString *source;
} ActivityMOAttributes;

extern const struct ActivityMORelationships {
	__unsafe_unretained NSString *basedOnActivity;
	__unsafe_unretained NSString *childActivities;
	__unsafe_unretained NSString *sets;
	__unsafe_unretained NSString *workout;
} ActivityMORelationships;

@class ActivityMO;
@class ActivityMO;
@class ActivitySetMO;
@class AdvisorActivityPlanWorkoutMO;

@interface ActivityMOID : NSManagedObjectID {}
@end

@interface _ActivityMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ActivityMOID* objectID;

@property (nonatomic, strong) NSString* activityDescription;

//- (BOOL)validateActivityDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* activityName;

//- (BOOL)validateActivityName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* basedOnId;

@property (atomic) int32_t basedOnIdValue;
- (int32_t)basedOnIdValue;
- (void)setBasedOnIdValue:(int32_t)value_;

//- (BOOL)validateBasedOnId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* factor;

@property (atomic) double factorValue;
- (double)factorValue;
- (void)setFactorValue:(double)value_;

//- (BOOL)validateFactor:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isTimedActivity;

@property (atomic) BOOL isTimedActivityValue;
- (BOOL)isTimedActivityValue;
- (void)setIsTimedActivityValue:(BOOL)value_;

//- (BOOL)validateIsTimedActivity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* link;

//- (BOOL)validateLink:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* notes;

//- (BOOL)validateNotes:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* routineId;

@property (atomic) int32_t routineIdValue;
- (int32_t)routineIdValue;
- (void)setRoutineIdValue:(int32_t)value_;

//- (BOOL)validateRoutineId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* source;

@property (atomic) int16_t sourceValue;
- (int16_t)sourceValue;
- (void)setSourceValue:(int16_t)value_;

//- (BOOL)validateSource:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) ActivityMO *basedOnActivity;

//- (BOOL)validateBasedOnActivity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *childActivities;

- (NSMutableSet*)childActivitiesSet;

@property (nonatomic, strong) NSSet *sets;

- (NSMutableSet*)setsSet;

@property (nonatomic, strong) AdvisorActivityPlanWorkoutMO *workout;

//- (BOOL)validateWorkout:(id*)value_ error:(NSError**)error_;

@end

@interface _ActivityMO (ChildActivitiesCoreDataGeneratedAccessors)
- (void)addChildActivities:(NSSet*)value_;
- (void)removeChildActivities:(NSSet*)value_;
- (void)addChildActivitiesObject:(ActivityMO*)value_;
- (void)removeChildActivitiesObject:(ActivityMO*)value_;

@end

@interface _ActivityMO (SetsCoreDataGeneratedAccessors)
- (void)addSets:(NSSet*)value_;
- (void)removeSets:(NSSet*)value_;
- (void)addSetsObject:(ActivitySetMO*)value_;
- (void)removeSetsObject:(ActivitySetMO*)value_;

@end

@interface _ActivityMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveActivityDescription;
- (void)setPrimitiveActivityDescription:(NSString*)value;

- (NSString*)primitiveActivityName;
- (void)setPrimitiveActivityName:(NSString*)value;

- (NSNumber*)primitiveBasedOnId;
- (void)setPrimitiveBasedOnId:(NSNumber*)value;

- (int32_t)primitiveBasedOnIdValue;
- (void)setPrimitiveBasedOnIdValue:(int32_t)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveFactor;
- (void)setPrimitiveFactor:(NSNumber*)value;

- (double)primitiveFactorValue;
- (void)setPrimitiveFactorValue:(double)value_;

- (NSNumber*)primitiveIsTimedActivity;
- (void)setPrimitiveIsTimedActivity:(NSNumber*)value;

- (BOOL)primitiveIsTimedActivityValue;
- (void)setPrimitiveIsTimedActivityValue:(BOOL)value_;

- (NSString*)primitiveLink;
- (void)setPrimitiveLink:(NSString*)value;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveNotes;
- (void)setPrimitiveNotes:(NSString*)value;

- (NSNumber*)primitiveRoutineId;
- (void)setPrimitiveRoutineId:(NSNumber*)value;

- (int32_t)primitiveRoutineIdValue;
- (void)setPrimitiveRoutineIdValue:(int32_t)value_;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSNumber*)primitiveSource;
- (void)setPrimitiveSource:(NSNumber*)value;

- (int16_t)primitiveSourceValue;
- (void)setPrimitiveSourceValue:(int16_t)value_;

- (ActivityMO*)primitiveBasedOnActivity;
- (void)setPrimitiveBasedOnActivity:(ActivityMO*)value;

- (NSMutableSet*)primitiveChildActivities;
- (void)setPrimitiveChildActivities:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSets;
- (void)setPrimitiveSets:(NSMutableSet*)value;

- (AdvisorActivityPlanWorkoutMO*)primitiveWorkout;
- (void)setPrimitiveWorkout:(AdvisorActivityPlanWorkoutMO*)value;

@end
