// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrackingValueMO.m instead.

#import "_TrackingValueMO.h"

const struct TrackingValueMOAttributes TrackingValueMOAttributes = {
	.dataType = @"dataType",
	.defaultValue = @"defaultValue",
	.field = @"field",
	.optional = @"optional",
	.precision = @"precision",
	.rangeMax = @"rangeMax",
	.rangeMin = @"rangeMin",
	.title = @"title",
	.unitAbbrev = @"unitAbbrev",
	.units = @"units",
};

const struct TrackingValueMORelationships TrackingValueMORelationships = {
	.trackingType = @"trackingType",
};

@implementation TrackingValueMOID
@end

@implementation _TrackingValueMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TrackingValue" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TrackingValue";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TrackingValue" inManagedObjectContext:moc_];
}

- (TrackingValueMOID*)objectID {
	return (TrackingValueMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"defaultValueValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"defaultValue"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"optionalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"optional"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"precisionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"precision"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rangeMaxValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rangeMax"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rangeMinValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rangeMin"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic dataType;

@dynamic defaultValue;

- (double)defaultValueValue {
	NSNumber *result = [self defaultValue];
	return [result doubleValue];
}

- (void)setDefaultValueValue:(double)value_ {
	[self setDefaultValue:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveDefaultValueValue {
	NSNumber *result = [self primitiveDefaultValue];
	return [result doubleValue];
}

- (void)setPrimitiveDefaultValueValue:(double)value_ {
	[self setPrimitiveDefaultValue:[NSNumber numberWithDouble:value_]];
}

@dynamic field;

@dynamic optional;

- (BOOL)optionalValue {
	NSNumber *result = [self optional];
	return [result boolValue];
}

- (void)setOptionalValue:(BOOL)value_ {
	[self setOptional:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveOptionalValue {
	NSNumber *result = [self primitiveOptional];
	return [result boolValue];
}

- (void)setPrimitiveOptionalValue:(BOOL)value_ {
	[self setPrimitiveOptional:[NSNumber numberWithBool:value_]];
}

@dynamic precision;

- (int16_t)precisionValue {
	NSNumber *result = [self precision];
	return [result shortValue];
}

- (void)setPrecisionValue:(int16_t)value_ {
	[self setPrecision:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePrecisionValue {
	NSNumber *result = [self primitivePrecision];
	return [result shortValue];
}

- (void)setPrimitivePrecisionValue:(int16_t)value_ {
	[self setPrimitivePrecision:[NSNumber numberWithShort:value_]];
}

@dynamic rangeMax;

- (double)rangeMaxValue {
	NSNumber *result = [self rangeMax];
	return [result doubleValue];
}

- (void)setRangeMaxValue:(double)value_ {
	[self setRangeMax:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveRangeMaxValue {
	NSNumber *result = [self primitiveRangeMax];
	return [result doubleValue];
}

- (void)setPrimitiveRangeMaxValue:(double)value_ {
	[self setPrimitiveRangeMax:[NSNumber numberWithDouble:value_]];
}

@dynamic rangeMin;

- (double)rangeMinValue {
	NSNumber *result = [self rangeMin];
	return [result doubleValue];
}

- (void)setRangeMinValue:(double)value_ {
	[self setRangeMin:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveRangeMinValue {
	NSNumber *result = [self primitiveRangeMin];
	return [result doubleValue];
}

- (void)setPrimitiveRangeMinValue:(double)value_ {
	[self setPrimitiveRangeMin:[NSNumber numberWithDouble:value_]];
}

@dynamic title;

@dynamic unitAbbrev;

@dynamic units;

@dynamic trackingType;

@end

