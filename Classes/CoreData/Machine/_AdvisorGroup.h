// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorGroup.h instead.

#import <CoreData/CoreData.h>

extern const struct AdvisorGroupAttributes {
	__unsafe_unretained NSString *cloudId;
} AdvisorGroupAttributes;

extern const struct AdvisorGroupRelationships {
	__unsafe_unretained NSString *advisor;
	__unsafe_unretained NSString *recipes;
} AdvisorGroupRelationships;

@class AdvisorMO;
@class RecipeMO;

@interface AdvisorGroupID : NSManagedObjectID {}
@end

@interface _AdvisorGroup : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorGroupID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AdvisorMO *advisor;

//- (BOOL)validateAdvisor:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *recipes;

- (NSMutableSet*)recipesSet;

@end

@interface _AdvisorGroup (RecipesCoreDataGeneratedAccessors)
- (void)addRecipes:(NSSet*)value_;
- (void)removeRecipes:(NSSet*)value_;
- (void)addRecipesObject:(RecipeMO*)value_;
- (void)removeRecipesObject:(RecipeMO*)value_;

@end

@interface _AdvisorGroup (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (AdvisorMO*)primitiveAdvisor;
- (void)setPrimitiveAdvisor:(AdvisorMO*)value;

- (NSMutableSet*)primitiveRecipes;
- (void)setPrimitiveRecipes:(NSMutableSet*)value;

@end
