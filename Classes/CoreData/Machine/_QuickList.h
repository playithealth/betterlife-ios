// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to QuickList.h instead.

#import <CoreData/CoreData.h>


extern const struct QuickListAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *createdOn;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
} QuickListAttributes;

extern const struct QuickListRelationships {
	__unsafe_unretained NSString *Items;
} QuickListRelationships;

extern const struct QuickListFetchedProperties {
} QuickListFetchedProperties;

@class QuickListItem;








@interface QuickListID : NSManagedObjectID {}
@end

@interface _QuickList : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (QuickListID*)objectID;




@property (nonatomic, strong) NSNumber* accountId;


@property int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* cloudId;


@property int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* createdOn;


@property int32_t createdOnValue;
- (int32_t)createdOnValue;
- (void)setCreatedOnValue:(int32_t)value_;

//- (BOOL)validateCreatedOn:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSString* name;


//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSString* status;


//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* updatedOn;


@property int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet* Items;

- (NSMutableSet*)ItemsSet;





@end

@interface _QuickList (CoreDataGeneratedAccessors)

- (void)addItems:(NSSet*)value_;
- (void)removeItems:(NSSet*)value_;
- (void)addItemsObject:(QuickListItem*)value_;
- (void)removeItemsObject:(QuickListItem*)value_;

@end

@interface _QuickList (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;




- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;




- (NSNumber*)primitiveCreatedOn;
- (void)setPrimitiveCreatedOn:(NSNumber*)value;

- (int32_t)primitiveCreatedOnValue;
- (void)setPrimitiveCreatedOnValue:(int32_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;




- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;





- (NSMutableSet*)primitiveItems;
- (void)setPrimitiveItems:(NSMutableSet*)value;


@end
