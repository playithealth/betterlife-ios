// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivitySetMO.m instead.

#import "_ActivitySetMO.h"

const struct ActivitySetMOAttributes ActivitySetMOAttributes = {
	.activityType = @"activityType",
	.calories = @"calories",
	.cloudId = @"cloudId",
	.factor = @"factor",
	.heartZone = @"heartZone",
	.miles = @"miles",
	.mph = @"mph",
	.name = @"name",
	.pounds = @"pounds",
	.reps = @"reps",
	.seconds = @"seconds",
	.sortOrder = @"sortOrder",
	.watts = @"watts",
};

const struct ActivitySetMORelationships ActivitySetMORelationships = {
	.activity = @"activity",
	.basedOnSet = @"basedOnSet",
	.derivedSets = @"derivedSets",
};

@implementation ActivitySetMOID
@end

@implementation _ActivitySetMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AbstractActivitySet" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AbstractActivitySet";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AbstractActivitySet" inManagedObjectContext:moc_];
}

- (ActivitySetMOID*)objectID {
	return (ActivitySetMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"activityTypeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"activityType"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"calories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"factorValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"factor"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"heartZoneValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"heartZone"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"milesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"miles"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"mphValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mph"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"poundsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"pounds"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"repsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"reps"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"secondsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"seconds"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"wattsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"watts"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic activityType;

- (int16_t)activityTypeValue {
	NSNumber *result = [self activityType];
	return [result shortValue];
}

- (void)setActivityTypeValue:(int16_t)value_ {
	[self setActivityType:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveActivityTypeValue {
	NSNumber *result = [self primitiveActivityType];
	return [result shortValue];
}

- (void)setPrimitiveActivityTypeValue:(int16_t)value_ {
	[self setPrimitiveActivityType:[NSNumber numberWithShort:value_]];
}

@dynamic calories;

- (double)caloriesValue {
	NSNumber *result = [self calories];
	return [result doubleValue];
}

- (void)setCaloriesValue:(double)value_ {
	[self setCalories:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCaloriesValue {
	NSNumber *result = [self primitiveCalories];
	return [result doubleValue];
}

- (void)setPrimitiveCaloriesValue:(double)value_ {
	[self setPrimitiveCalories:[NSNumber numberWithDouble:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic factor;

- (double)factorValue {
	NSNumber *result = [self factor];
	return [result doubleValue];
}

- (void)setFactorValue:(double)value_ {
	[self setFactor:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveFactorValue {
	NSNumber *result = [self primitiveFactor];
	return [result doubleValue];
}

- (void)setPrimitiveFactorValue:(double)value_ {
	[self setPrimitiveFactor:[NSNumber numberWithDouble:value_]];
}

@dynamic heartZone;

- (int16_t)heartZoneValue {
	NSNumber *result = [self heartZone];
	return [result shortValue];
}

- (void)setHeartZoneValue:(int16_t)value_ {
	[self setHeartZone:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveHeartZoneValue {
	NSNumber *result = [self primitiveHeartZone];
	return [result shortValue];
}

- (void)setPrimitiveHeartZoneValue:(int16_t)value_ {
	[self setPrimitiveHeartZone:[NSNumber numberWithShort:value_]];
}

@dynamic miles;

- (double)milesValue {
	NSNumber *result = [self miles];
	return [result doubleValue];
}

- (void)setMilesValue:(double)value_ {
	[self setMiles:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveMilesValue {
	NSNumber *result = [self primitiveMiles];
	return [result doubleValue];
}

- (void)setPrimitiveMilesValue:(double)value_ {
	[self setPrimitiveMiles:[NSNumber numberWithDouble:value_]];
}

@dynamic mph;

- (double)mphValue {
	NSNumber *result = [self mph];
	return [result doubleValue];
}

- (void)setMphValue:(double)value_ {
	[self setMph:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveMphValue {
	NSNumber *result = [self primitiveMph];
	return [result doubleValue];
}

- (void)setPrimitiveMphValue:(double)value_ {
	[self setPrimitiveMph:[NSNumber numberWithDouble:value_]];
}

@dynamic name;

@dynamic pounds;

- (double)poundsValue {
	NSNumber *result = [self pounds];
	return [result doubleValue];
}

- (void)setPoundsValue:(double)value_ {
	[self setPounds:[NSNumber numberWithDouble:value_]];
}

- (double)primitivePoundsValue {
	NSNumber *result = [self primitivePounds];
	return [result doubleValue];
}

- (void)setPrimitivePoundsValue:(double)value_ {
	[self setPrimitivePounds:[NSNumber numberWithDouble:value_]];
}

@dynamic reps;

- (int16_t)repsValue {
	NSNumber *result = [self reps];
	return [result shortValue];
}

- (void)setRepsValue:(int16_t)value_ {
	[self setReps:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveRepsValue {
	NSNumber *result = [self primitiveReps];
	return [result shortValue];
}

- (void)setPrimitiveRepsValue:(int16_t)value_ {
	[self setPrimitiveReps:[NSNumber numberWithShort:value_]];
}

@dynamic seconds;

- (int16_t)secondsValue {
	NSNumber *result = [self seconds];
	return [result shortValue];
}

- (void)setSecondsValue:(int16_t)value_ {
	[self setSeconds:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSecondsValue {
	NSNumber *result = [self primitiveSeconds];
	return [result shortValue];
}

- (void)setPrimitiveSecondsValue:(int16_t)value_ {
	[self setPrimitiveSeconds:[NSNumber numberWithShort:value_]];
}

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic watts;

- (int16_t)wattsValue {
	NSNumber *result = [self watts];
	return [result shortValue];
}

- (void)setWattsValue:(int16_t)value_ {
	[self setWatts:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveWattsValue {
	NSNumber *result = [self primitiveWatts];
	return [result shortValue];
}

- (void)setPrimitiveWattsValue:(int16_t)value_ {
	[self setPrimitiveWatts:[NSNumber numberWithShort:value_]];
}

@dynamic activity;

@dynamic basedOnSet;

@dynamic derivedSets;

- (NSMutableSet*)derivedSetsSet {
	[self willAccessValueForKey:@"derivedSets"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"derivedSets"];

	[self didAccessValueForKey:@"derivedSets"];
	return result;
}

@end

