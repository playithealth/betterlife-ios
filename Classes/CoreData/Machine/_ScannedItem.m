// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ScannedItem.m instead.

#import "_ScannedItem.h"

const struct ScannedItemAttributes ScannedItemAttributes = {
	.barcode = @"barcode",
	.date = @"date",
	.name = @"name",
};

const struct ScannedItemRelationships ScannedItemRelationships = {
	.category = @"category",
};

@implementation ScannedItemID
@end

@implementation _ScannedItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ScannedItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ScannedItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ScannedItem" inManagedObjectContext:moc_];
}

- (ScannedItemID*)objectID {
	return (ScannedItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic barcode;

@dynamic date;

@dynamic name;

@dynamic category;

@end

