// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CommunityMO.m instead.

#import "_CommunityMO.h"

const struct CommunityMOAttributes CommunityMOAttributes = {
	.cloudId = @"cloudId",
	.communityStatus = @"communityStatus",
	.createdOn = @"createdOn",
	.name = @"name",
	.ownerId = @"ownerId",
	.ownerName = @"ownerName",
	.status = @"status",
	.type = @"type",
	.updatedOn = @"updatedOn",
	.usePhoto = @"usePhoto",
	.visibility = @"visibility",
};

const struct CommunityMORelationships CommunityMORelationships = {
	.threads = @"threads",
	.userProfile = @"userProfile",
};

@implementation CommunityMOID
@end

@implementation _CommunityMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Community" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Community";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Community" inManagedObjectContext:moc_];
}

- (CommunityMOID*)objectID {
	return (CommunityMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"createdOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"createdOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ownerIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ownerId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"usePhotoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"usePhoto"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic communityStatus;

@dynamic createdOn;

- (int32_t)createdOnValue {
	NSNumber *result = [self createdOn];
	return [result intValue];
}

- (void)setCreatedOnValue:(int32_t)value_ {
	[self setCreatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCreatedOnValue {
	NSNumber *result = [self primitiveCreatedOn];
	return [result intValue];
}

- (void)setPrimitiveCreatedOnValue:(int32_t)value_ {
	[self setPrimitiveCreatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic ownerId;

- (int32_t)ownerIdValue {
	NSNumber *result = [self ownerId];
	return [result intValue];
}

- (void)setOwnerIdValue:(int32_t)value_ {
	[self setOwnerId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveOwnerIdValue {
	NSNumber *result = [self primitiveOwnerId];
	return [result intValue];
}

- (void)setPrimitiveOwnerIdValue:(int32_t)value_ {
	[self setPrimitiveOwnerId:[NSNumber numberWithInt:value_]];
}

@dynamic ownerName;

@dynamic status;

@dynamic type;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic usePhoto;

- (BOOL)usePhotoValue {
	NSNumber *result = [self usePhoto];
	return [result boolValue];
}

- (void)setUsePhotoValue:(BOOL)value_ {
	[self setUsePhoto:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUsePhotoValue {
	NSNumber *result = [self primitiveUsePhoto];
	return [result boolValue];
}

- (void)setPrimitiveUsePhotoValue:(BOOL)value_ {
	[self setPrimitiveUsePhoto:[NSNumber numberWithBool:value_]];
}

@dynamic visibility;

@dynamic threads;

- (NSMutableSet*)threadsSet {
	[self willAccessValueForKey:@"threads"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"threads"];

	[self didAccessValueForKey:@"threads"];
	return result;
}

@dynamic userProfile;

@end

