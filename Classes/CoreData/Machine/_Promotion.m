// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Promotion.m instead.

#import "_Promotion.h"

const struct PromotionAttributes PromotionAttributes = {
	.accepted = @"accepted",
	.cloudId = @"cloudId",
	.disclaimer = @"disclaimer",
	.discount = @"discount",
	.entityId = @"entityId",
	.name = @"name",
	.promoDescription = @"promoDescription",
	.status = @"status",
};

const struct PromotionRelationships PromotionRelationships = {
	.shoppingListItem = @"shoppingListItem",
};

@implementation PromotionID
@end

@implementation _Promotion

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Promotion" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Promotion";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Promotion" inManagedObjectContext:moc_];
}

- (PromotionID*)objectID {
	return (PromotionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"acceptedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accepted"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"entityIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"entityId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accepted;

- (BOOL)acceptedValue {
	NSNumber *result = [self accepted];
	return [result boolValue];
}

- (void)setAcceptedValue:(BOOL)value_ {
	[self setAccepted:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAcceptedValue {
	NSNumber *result = [self primitiveAccepted];
	return [result boolValue];
}

- (void)setPrimitiveAcceptedValue:(BOOL)value_ {
	[self setPrimitiveAccepted:[NSNumber numberWithBool:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic disclaimer;

@dynamic discount;

@dynamic entityId;

- (int32_t)entityIdValue {
	NSNumber *result = [self entityId];
	return [result intValue];
}

- (void)setEntityIdValue:(int32_t)value_ {
	[self setEntityId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveEntityIdValue {
	NSNumber *result = [self primitiveEntityId];
	return [result intValue];
}

- (void)setPrimitiveEntityIdValue:(int32_t)value_ {
	[self setPrimitiveEntityId:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic promoDescription;

@dynamic status;

@dynamic shoppingListItem;

@end

