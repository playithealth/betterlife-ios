// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanMO.m instead.

#import "_AdvisorMealPlanMO.h"

const struct AdvisorMealPlanMOAttributes AdvisorMealPlanMOAttributes = {
	.trackFruitsVeggies = @"trackFruitsVeggies",
	.trackSugaryDrinks = @"trackSugaryDrinks",
};

@implementation AdvisorMealPlanMOID
@end

@implementation _AdvisorMealPlanMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorMealPlan" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorMealPlan";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorMealPlan" inManagedObjectContext:moc_];
}

- (AdvisorMealPlanMOID*)objectID {
	return (AdvisorMealPlanMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"trackFruitsVeggiesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"trackFruitsVeggies"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"trackSugaryDrinksValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"trackSugaryDrinks"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic trackFruitsVeggies;

- (BOOL)trackFruitsVeggiesValue {
	NSNumber *result = [self trackFruitsVeggies];
	return [result boolValue];
}

- (void)setTrackFruitsVeggiesValue:(BOOL)value_ {
	[self setTrackFruitsVeggies:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveTrackFruitsVeggiesValue {
	NSNumber *result = [self primitiveTrackFruitsVeggies];
	return [result boolValue];
}

- (void)setPrimitiveTrackFruitsVeggiesValue:(BOOL)value_ {
	[self setPrimitiveTrackFruitsVeggies:[NSNumber numberWithBool:value_]];
}

@dynamic trackSugaryDrinks;

- (BOOL)trackSugaryDrinksValue {
	NSNumber *result = [self trackSugaryDrinks];
	return [result boolValue];
}

- (void)setTrackSugaryDrinksValue:(BOOL)value_ {
	[self setTrackSugaryDrinks:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveTrackSugaryDrinksValue {
	NSNumber *result = [self primitiveTrackSugaryDrinks];
	return [result boolValue];
}

- (void)setPrimitiveTrackSugaryDrinksValue:(BOOL)value_ {
	[self setPrimitiveTrackSugaryDrinks:[NSNumber numberWithBool:value_]];
}

@end

