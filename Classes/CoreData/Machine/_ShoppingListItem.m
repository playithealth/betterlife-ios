// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ShoppingListItem.m instead.

#import "_ShoppingListItem.h"

const struct ShoppingListItemAttributes ShoppingListItemAttributes = {
	.accountId = @"accountId",
	.allergyCount = @"allergyCount",
	.barcode = @"barcode",
	.calciumPercent = @"calciumPercent",
	.calories = @"calories",
	.caloriesFromFat = @"caloriesFromFat",
	.cholesterol = @"cholesterol",
	.cloudId = @"cloudId",
	.dietaryFiber = @"dietaryFiber",
	.imageId = @"imageId",
	.inCart = @"inCart",
	.inCartTime = @"inCartTime",
	.ingredientCount = @"ingredientCount",
	.insolubleFiber = @"insolubleFiber",
	.ironPercent = @"ironPercent",
	.isRecommendation = @"isRecommendation",
	.itemDescription = @"itemDescription",
	.lifestyleCount = @"lifestyleCount",
	.monounsaturatedFat = @"monounsaturatedFat",
	.name = @"name",
	.nameFirstCharacter = @"nameFirstCharacter",
	.nutritionHtml = @"nutritionHtml",
	.otherCarbohydrates = @"otherCarbohydrates",
	.polyunsaturatedFat = @"polyunsaturatedFat",
	.potassium = @"potassium",
	.private = @"private",
	.productId = @"productId",
	.promoAvailable = @"promoAvailable",
	.promoSelectedCount = @"promoSelectedCount",
	.protein = @"protein",
	.purchased = @"purchased",
	.quantity = @"quantity",
	.recommendationDays = @"recommendationDays",
	.saturatedFat = @"saturatedFat",
	.saturatedFatCalories = @"saturatedFatCalories",
	.sodium = @"sodium",
	.solubleFiber = @"solubleFiber",
	.status = @"status",
	.storeId = @"storeId",
	.sugars = @"sugars",
	.sugarsAlcohol = @"sugarsAlcohol",
	.totalCarbohydrates = @"totalCarbohydrates",
	.totalFat = @"totalFat",
	.transFat = @"transFat",
	.updatedOn = @"updatedOn",
	.vitaminAPercent = @"vitaminAPercent",
	.vitaminCPercent = @"vitaminCPercent",
};

const struct ShoppingListItemRelationships ShoppingListItemRelationships = {
	.category = @"category",
	.customFood = @"customFood",
	.mealPlannerDays = @"mealPlannerDays",
	.promotions = @"promotions",
	.storeCategory = @"storeCategory",
};

@implementation ShoppingListItemID
@end

@implementation _ShoppingListItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ShoppingListItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ShoppingListItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ShoppingListItem" inManagedObjectContext:moc_];
}

- (ShoppingListItemID*)objectID {
	return (ShoppingListItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"allergyCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"allergyCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"calciumPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"calciumPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"calories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesFromFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"caloriesFromFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cholesterolValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cholesterol"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"dietaryFiberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dietaryFiber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"imageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"imageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"inCartValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"inCart"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ingredientCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ingredientCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"insolubleFiberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"insolubleFiber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ironPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ironPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isRecommendationValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRecommendation"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"lifestyleCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"lifestyleCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"monounsaturatedFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"monounsaturatedFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"otherCarbohydratesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"otherCarbohydrates"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"polyunsaturatedFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"polyunsaturatedFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"potassiumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"potassium"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"privateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"private"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"productIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"productId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"promoAvailableValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"promoAvailable"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"promoSelectedCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"promoSelectedCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"proteinValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"protein"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"purchasedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"purchased"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"quantityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"quantity"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"recommendationDaysValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"recommendationDays"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"saturatedFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"saturatedFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"saturatedFatCaloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"saturatedFatCalories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sodiumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sodium"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"solubleFiberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"solubleFiber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"storeIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"storeId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sugarsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sugars"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sugarsAlcoholValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sugarsAlcohol"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalCarbohydratesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalCarbohydrates"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"transFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"transFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"vitaminAPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"vitaminAPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"vitaminCPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"vitaminCPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic allergyCount;

- (int16_t)allergyCountValue {
	NSNumber *result = [self allergyCount];
	return [result shortValue];
}

- (void)setAllergyCountValue:(int16_t)value_ {
	[self setAllergyCount:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveAllergyCountValue {
	NSNumber *result = [self primitiveAllergyCount];
	return [result shortValue];
}

- (void)setPrimitiveAllergyCountValue:(int16_t)value_ {
	[self setPrimitiveAllergyCount:[NSNumber numberWithShort:value_]];
}

@dynamic barcode;

@dynamic calciumPercent;

- (double)calciumPercentValue {
	NSNumber *result = [self calciumPercent];
	return [result doubleValue];
}

- (void)setCalciumPercentValue:(double)value_ {
	[self setCalciumPercent:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCalciumPercentValue {
	NSNumber *result = [self primitiveCalciumPercent];
	return [result doubleValue];
}

- (void)setPrimitiveCalciumPercentValue:(double)value_ {
	[self setPrimitiveCalciumPercent:[NSNumber numberWithDouble:value_]];
}

@dynamic calories;

- (int16_t)caloriesValue {
	NSNumber *result = [self calories];
	return [result shortValue];
}

- (void)setCaloriesValue:(int16_t)value_ {
	[self setCalories:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCaloriesValue {
	NSNumber *result = [self primitiveCalories];
	return [result shortValue];
}

- (void)setPrimitiveCaloriesValue:(int16_t)value_ {
	[self setPrimitiveCalories:[NSNumber numberWithShort:value_]];
}

@dynamic caloriesFromFat;

- (int16_t)caloriesFromFatValue {
	NSNumber *result = [self caloriesFromFat];
	return [result shortValue];
}

- (void)setCaloriesFromFatValue:(int16_t)value_ {
	[self setCaloriesFromFat:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCaloriesFromFatValue {
	NSNumber *result = [self primitiveCaloriesFromFat];
	return [result shortValue];
}

- (void)setPrimitiveCaloriesFromFatValue:(int16_t)value_ {
	[self setPrimitiveCaloriesFromFat:[NSNumber numberWithShort:value_]];
}

@dynamic cholesterol;

- (double)cholesterolValue {
	NSNumber *result = [self cholesterol];
	return [result doubleValue];
}

- (void)setCholesterolValue:(double)value_ {
	[self setCholesterol:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCholesterolValue {
	NSNumber *result = [self primitiveCholesterol];
	return [result doubleValue];
}

- (void)setPrimitiveCholesterolValue:(double)value_ {
	[self setPrimitiveCholesterol:[NSNumber numberWithDouble:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic dietaryFiber;

- (double)dietaryFiberValue {
	NSNumber *result = [self dietaryFiber];
	return [result doubleValue];
}

- (void)setDietaryFiberValue:(double)value_ {
	[self setDietaryFiber:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveDietaryFiberValue {
	NSNumber *result = [self primitiveDietaryFiber];
	return [result doubleValue];
}

- (void)setPrimitiveDietaryFiberValue:(double)value_ {
	[self setPrimitiveDietaryFiber:[NSNumber numberWithDouble:value_]];
}

@dynamic imageId;

- (int32_t)imageIdValue {
	NSNumber *result = [self imageId];
	return [result intValue];
}

- (void)setImageIdValue:(int32_t)value_ {
	[self setImageId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImageIdValue {
	NSNumber *result = [self primitiveImageId];
	return [result intValue];
}

- (void)setPrimitiveImageIdValue:(int32_t)value_ {
	[self setPrimitiveImageId:[NSNumber numberWithInt:value_]];
}

@dynamic inCart;

- (BOOL)inCartValue {
	NSNumber *result = [self inCart];
	return [result boolValue];
}

- (void)setInCartValue:(BOOL)value_ {
	[self setInCart:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveInCartValue {
	NSNumber *result = [self primitiveInCart];
	return [result boolValue];
}

- (void)setPrimitiveInCartValue:(BOOL)value_ {
	[self setPrimitiveInCart:[NSNumber numberWithBool:value_]];
}

@dynamic inCartTime;

@dynamic ingredientCount;

- (int16_t)ingredientCountValue {
	NSNumber *result = [self ingredientCount];
	return [result shortValue];
}

- (void)setIngredientCountValue:(int16_t)value_ {
	[self setIngredientCount:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveIngredientCountValue {
	NSNumber *result = [self primitiveIngredientCount];
	return [result shortValue];
}

- (void)setPrimitiveIngredientCountValue:(int16_t)value_ {
	[self setPrimitiveIngredientCount:[NSNumber numberWithShort:value_]];
}

@dynamic insolubleFiber;

- (double)insolubleFiberValue {
	NSNumber *result = [self insolubleFiber];
	return [result doubleValue];
}

- (void)setInsolubleFiberValue:(double)value_ {
	[self setInsolubleFiber:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveInsolubleFiberValue {
	NSNumber *result = [self primitiveInsolubleFiber];
	return [result doubleValue];
}

- (void)setPrimitiveInsolubleFiberValue:(double)value_ {
	[self setPrimitiveInsolubleFiber:[NSNumber numberWithDouble:value_]];
}

@dynamic ironPercent;

- (double)ironPercentValue {
	NSNumber *result = [self ironPercent];
	return [result doubleValue];
}

- (void)setIronPercentValue:(double)value_ {
	[self setIronPercent:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveIronPercentValue {
	NSNumber *result = [self primitiveIronPercent];
	return [result doubleValue];
}

- (void)setPrimitiveIronPercentValue:(double)value_ {
	[self setPrimitiveIronPercent:[NSNumber numberWithDouble:value_]];
}

@dynamic isRecommendation;

- (BOOL)isRecommendationValue {
	NSNumber *result = [self isRecommendation];
	return [result boolValue];
}

- (void)setIsRecommendationValue:(BOOL)value_ {
	[self setIsRecommendation:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsRecommendationValue {
	NSNumber *result = [self primitiveIsRecommendation];
	return [result boolValue];
}

- (void)setPrimitiveIsRecommendationValue:(BOOL)value_ {
	[self setPrimitiveIsRecommendation:[NSNumber numberWithBool:value_]];
}

@dynamic itemDescription;

@dynamic lifestyleCount;

- (int16_t)lifestyleCountValue {
	NSNumber *result = [self lifestyleCount];
	return [result shortValue];
}

- (void)setLifestyleCountValue:(int16_t)value_ {
	[self setLifestyleCount:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLifestyleCountValue {
	NSNumber *result = [self primitiveLifestyleCount];
	return [result shortValue];
}

- (void)setPrimitiveLifestyleCountValue:(int16_t)value_ {
	[self setPrimitiveLifestyleCount:[NSNumber numberWithShort:value_]];
}

@dynamic monounsaturatedFat;

- (double)monounsaturatedFatValue {
	NSNumber *result = [self monounsaturatedFat];
	return [result doubleValue];
}

- (void)setMonounsaturatedFatValue:(double)value_ {
	[self setMonounsaturatedFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveMonounsaturatedFatValue {
	NSNumber *result = [self primitiveMonounsaturatedFat];
	return [result doubleValue];
}

- (void)setPrimitiveMonounsaturatedFatValue:(double)value_ {
	[self setPrimitiveMonounsaturatedFat:[NSNumber numberWithDouble:value_]];
}

@dynamic name;

@dynamic nameFirstCharacter;

@dynamic nutritionHtml;

@dynamic otherCarbohydrates;

- (double)otherCarbohydratesValue {
	NSNumber *result = [self otherCarbohydrates];
	return [result doubleValue];
}

- (void)setOtherCarbohydratesValue:(double)value_ {
	[self setOtherCarbohydrates:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveOtherCarbohydratesValue {
	NSNumber *result = [self primitiveOtherCarbohydrates];
	return [result doubleValue];
}

- (void)setPrimitiveOtherCarbohydratesValue:(double)value_ {
	[self setPrimitiveOtherCarbohydrates:[NSNumber numberWithDouble:value_]];
}

@dynamic polyunsaturatedFat;

- (double)polyunsaturatedFatValue {
	NSNumber *result = [self polyunsaturatedFat];
	return [result doubleValue];
}

- (void)setPolyunsaturatedFatValue:(double)value_ {
	[self setPolyunsaturatedFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitivePolyunsaturatedFatValue {
	NSNumber *result = [self primitivePolyunsaturatedFat];
	return [result doubleValue];
}

- (void)setPrimitivePolyunsaturatedFatValue:(double)value_ {
	[self setPrimitivePolyunsaturatedFat:[NSNumber numberWithDouble:value_]];
}

@dynamic potassium;

- (double)potassiumValue {
	NSNumber *result = [self potassium];
	return [result doubleValue];
}

- (void)setPotassiumValue:(double)value_ {
	[self setPotassium:[NSNumber numberWithDouble:value_]];
}

- (double)primitivePotassiumValue {
	NSNumber *result = [self primitivePotassium];
	return [result doubleValue];
}

- (void)setPrimitivePotassiumValue:(double)value_ {
	[self setPrimitivePotassium:[NSNumber numberWithDouble:value_]];
}

@dynamic private;

- (BOOL)privateValue {
	NSNumber *result = [self private];
	return [result boolValue];
}

- (void)setPrivateValue:(BOOL)value_ {
	[self setPrivate:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePrivateValue {
	NSNumber *result = [self primitivePrivate];
	return [result boolValue];
}

- (void)setPrimitivePrivateValue:(BOOL)value_ {
	[self setPrimitivePrivate:[NSNumber numberWithBool:value_]];
}

@dynamic productId;

- (int32_t)productIdValue {
	NSNumber *result = [self productId];
	return [result intValue];
}

- (void)setProductIdValue:(int32_t)value_ {
	[self setProductId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveProductIdValue {
	NSNumber *result = [self primitiveProductId];
	return [result intValue];
}

- (void)setPrimitiveProductIdValue:(int32_t)value_ {
	[self setPrimitiveProductId:[NSNumber numberWithInt:value_]];
}

@dynamic promoAvailable;

- (BOOL)promoAvailableValue {
	NSNumber *result = [self promoAvailable];
	return [result boolValue];
}

- (void)setPromoAvailableValue:(BOOL)value_ {
	[self setPromoAvailable:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePromoAvailableValue {
	NSNumber *result = [self primitivePromoAvailable];
	return [result boolValue];
}

- (void)setPrimitivePromoAvailableValue:(BOOL)value_ {
	[self setPrimitivePromoAvailable:[NSNumber numberWithBool:value_]];
}

@dynamic promoSelectedCount;

- (int16_t)promoSelectedCountValue {
	NSNumber *result = [self promoSelectedCount];
	return [result shortValue];
}

- (void)setPromoSelectedCountValue:(int16_t)value_ {
	[self setPromoSelectedCount:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePromoSelectedCountValue {
	NSNumber *result = [self primitivePromoSelectedCount];
	return [result shortValue];
}

- (void)setPrimitivePromoSelectedCountValue:(int16_t)value_ {
	[self setPrimitivePromoSelectedCount:[NSNumber numberWithShort:value_]];
}

@dynamic protein;

- (double)proteinValue {
	NSNumber *result = [self protein];
	return [result doubleValue];
}

- (void)setProteinValue:(double)value_ {
	[self setProtein:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveProteinValue {
	NSNumber *result = [self primitiveProtein];
	return [result doubleValue];
}

- (void)setPrimitiveProteinValue:(double)value_ {
	[self setPrimitiveProtein:[NSNumber numberWithDouble:value_]];
}

@dynamic purchased;

- (BOOL)purchasedValue {
	NSNumber *result = [self purchased];
	return [result boolValue];
}

- (void)setPurchasedValue:(BOOL)value_ {
	[self setPurchased:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePurchasedValue {
	NSNumber *result = [self primitivePurchased];
	return [result boolValue];
}

- (void)setPrimitivePurchasedValue:(BOOL)value_ {
	[self setPrimitivePurchased:[NSNumber numberWithBool:value_]];
}

@dynamic quantity;

- (double)quantityValue {
	NSNumber *result = [self quantity];
	return [result doubleValue];
}

- (void)setQuantityValue:(double)value_ {
	[self setQuantity:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveQuantityValue {
	NSNumber *result = [self primitiveQuantity];
	return [result doubleValue];
}

- (void)setPrimitiveQuantityValue:(double)value_ {
	[self setPrimitiveQuantity:[NSNumber numberWithDouble:value_]];
}

@dynamic recommendationDays;

- (int16_t)recommendationDaysValue {
	NSNumber *result = [self recommendationDays];
	return [result shortValue];
}

- (void)setRecommendationDaysValue:(int16_t)value_ {
	[self setRecommendationDays:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveRecommendationDaysValue {
	NSNumber *result = [self primitiveRecommendationDays];
	return [result shortValue];
}

- (void)setPrimitiveRecommendationDaysValue:(int16_t)value_ {
	[self setPrimitiveRecommendationDays:[NSNumber numberWithShort:value_]];
}

@dynamic saturatedFat;

- (double)saturatedFatValue {
	NSNumber *result = [self saturatedFat];
	return [result doubleValue];
}

- (void)setSaturatedFatValue:(double)value_ {
	[self setSaturatedFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSaturatedFatValue {
	NSNumber *result = [self primitiveSaturatedFat];
	return [result doubleValue];
}

- (void)setPrimitiveSaturatedFatValue:(double)value_ {
	[self setPrimitiveSaturatedFat:[NSNumber numberWithDouble:value_]];
}

@dynamic saturatedFatCalories;

- (int16_t)saturatedFatCaloriesValue {
	NSNumber *result = [self saturatedFatCalories];
	return [result shortValue];
}

- (void)setSaturatedFatCaloriesValue:(int16_t)value_ {
	[self setSaturatedFatCalories:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSaturatedFatCaloriesValue {
	NSNumber *result = [self primitiveSaturatedFatCalories];
	return [result shortValue];
}

- (void)setPrimitiveSaturatedFatCaloriesValue:(int16_t)value_ {
	[self setPrimitiveSaturatedFatCalories:[NSNumber numberWithShort:value_]];
}

@dynamic sodium;

- (double)sodiumValue {
	NSNumber *result = [self sodium];
	return [result doubleValue];
}

- (void)setSodiumValue:(double)value_ {
	[self setSodium:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSodiumValue {
	NSNumber *result = [self primitiveSodium];
	return [result doubleValue];
}

- (void)setPrimitiveSodiumValue:(double)value_ {
	[self setPrimitiveSodium:[NSNumber numberWithDouble:value_]];
}

@dynamic solubleFiber;

- (double)solubleFiberValue {
	NSNumber *result = [self solubleFiber];
	return [result doubleValue];
}

- (void)setSolubleFiberValue:(double)value_ {
	[self setSolubleFiber:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSolubleFiberValue {
	NSNumber *result = [self primitiveSolubleFiber];
	return [result doubleValue];
}

- (void)setPrimitiveSolubleFiberValue:(double)value_ {
	[self setPrimitiveSolubleFiber:[NSNumber numberWithDouble:value_]];
}

@dynamic status;

@dynamic storeId;

- (int32_t)storeIdValue {
	NSNumber *result = [self storeId];
	return [result intValue];
}

- (void)setStoreIdValue:(int32_t)value_ {
	[self setStoreId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStoreIdValue {
	NSNumber *result = [self primitiveStoreId];
	return [result intValue];
}

- (void)setPrimitiveStoreIdValue:(int32_t)value_ {
	[self setPrimitiveStoreId:[NSNumber numberWithInt:value_]];
}

@dynamic sugars;

- (double)sugarsValue {
	NSNumber *result = [self sugars];
	return [result doubleValue];
}

- (void)setSugarsValue:(double)value_ {
	[self setSugars:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSugarsValue {
	NSNumber *result = [self primitiveSugars];
	return [result doubleValue];
}

- (void)setPrimitiveSugarsValue:(double)value_ {
	[self setPrimitiveSugars:[NSNumber numberWithDouble:value_]];
}

@dynamic sugarsAlcohol;

- (double)sugarsAlcoholValue {
	NSNumber *result = [self sugarsAlcohol];
	return [result doubleValue];
}

- (void)setSugarsAlcoholValue:(double)value_ {
	[self setSugarsAlcohol:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSugarsAlcoholValue {
	NSNumber *result = [self primitiveSugarsAlcohol];
	return [result doubleValue];
}

- (void)setPrimitiveSugarsAlcoholValue:(double)value_ {
	[self setPrimitiveSugarsAlcohol:[NSNumber numberWithDouble:value_]];
}

@dynamic totalCarbohydrates;

- (double)totalCarbohydratesValue {
	NSNumber *result = [self totalCarbohydrates];
	return [result doubleValue];
}

- (void)setTotalCarbohydratesValue:(double)value_ {
	[self setTotalCarbohydrates:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveTotalCarbohydratesValue {
	NSNumber *result = [self primitiveTotalCarbohydrates];
	return [result doubleValue];
}

- (void)setPrimitiveTotalCarbohydratesValue:(double)value_ {
	[self setPrimitiveTotalCarbohydrates:[NSNumber numberWithDouble:value_]];
}

@dynamic totalFat;

- (double)totalFatValue {
	NSNumber *result = [self totalFat];
	return [result doubleValue];
}

- (void)setTotalFatValue:(double)value_ {
	[self setTotalFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveTotalFatValue {
	NSNumber *result = [self primitiveTotalFat];
	return [result doubleValue];
}

- (void)setPrimitiveTotalFatValue:(double)value_ {
	[self setPrimitiveTotalFat:[NSNumber numberWithDouble:value_]];
}

@dynamic transFat;

- (double)transFatValue {
	NSNumber *result = [self transFat];
	return [result doubleValue];
}

- (void)setTransFatValue:(double)value_ {
	[self setTransFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveTransFatValue {
	NSNumber *result = [self primitiveTransFat];
	return [result doubleValue];
}

- (void)setPrimitiveTransFatValue:(double)value_ {
	[self setPrimitiveTransFat:[NSNumber numberWithDouble:value_]];
}

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic vitaminAPercent;

- (double)vitaminAPercentValue {
	NSNumber *result = [self vitaminAPercent];
	return [result doubleValue];
}

- (void)setVitaminAPercentValue:(double)value_ {
	[self setVitaminAPercent:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveVitaminAPercentValue {
	NSNumber *result = [self primitiveVitaminAPercent];
	return [result doubleValue];
}

- (void)setPrimitiveVitaminAPercentValue:(double)value_ {
	[self setPrimitiveVitaminAPercent:[NSNumber numberWithDouble:value_]];
}

@dynamic vitaminCPercent;

- (double)vitaminCPercentValue {
	NSNumber *result = [self vitaminCPercent];
	return [result doubleValue];
}

- (void)setVitaminCPercentValue:(double)value_ {
	[self setVitaminCPercent:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveVitaminCPercentValue {
	NSNumber *result = [self primitiveVitaminCPercent];
	return [result doubleValue];
}

- (void)setPrimitiveVitaminCPercentValue:(double)value_ {
	[self setPrimitiveVitaminCPercent:[NSNumber numberWithDouble:value_]];
}

@dynamic category;

@dynamic customFood;

@dynamic mealPlannerDays;

- (NSMutableSet*)mealPlannerDaysSet {
	[self willAccessValueForKey:@"mealPlannerDays"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mealPlannerDays"];

	[self didAccessValueForKey:@"mealPlannerDays"];
	return result;
}

@dynamic promotions;

- (NSMutableSet*)promotionsSet {
	[self willAccessValueForKey:@"promotions"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"promotions"];

	[self didAccessValueForKey:@"promotions"];
	return result;
}

@dynamic storeCategory;

@end

