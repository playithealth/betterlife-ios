// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrainingAssessmentMO.m instead.

#import "_TrainingAssessmentMO.h"

const struct TrainingAssessmentMOAttributes TrainingAssessmentMOAttributes = {
	.activityLevel = @"activityLevel",
	.age = @"age",
	.bmi = @"bmi",
	.bmr = @"bmr",
	.bodyFatPercent = @"bodyFatPercent",
	.chest = @"chest",
	.cloudId = @"cloudId",
	.crunches = @"crunches",
	.date = @"date",
	.height = @"height",
	.hips = @"hips",
	.leftBicep = @"leftBicep",
	.leftThigh = @"leftThigh",
	.loginId = @"loginId",
	.mileRun = @"mileRun",
	.neck = @"neck",
	.pullups = @"pullups",
	.pushups = @"pushups",
	.rightBicep = @"rightBicep",
	.rightThigh = @"rightThigh",
	.sex = @"sex",
	.squats = @"squats",
	.status = @"status",
	.stomach = @"stomach",
	.updatedOn = @"updatedOn",
	.waist = @"waist",
	.weight = @"weight",
};

const struct TrainingAssessmentMORelationships TrainingAssessmentMORelationships = {
	.tracking = @"tracking",
};

@implementation TrainingAssessmentMOID
@end

@implementation _TrainingAssessmentMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TrainingAssessment" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TrainingAssessment";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TrainingAssessment" inManagedObjectContext:moc_];
}

- (TrainingAssessmentMOID*)objectID {
	return (TrainingAssessmentMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"activityLevelValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"activityLevel"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"age"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"bmiValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"bmi"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"bmrValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"bmr"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"bodyFatPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"bodyFatPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"chestValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"chest"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"crunchesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"crunches"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"heightValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"height"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"hipsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"hips"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"leftBicepValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"leftBicep"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"leftThighValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"leftThigh"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"mileRunValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mileRun"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"neckValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"neck"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"pullupsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"pullups"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"pushupsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"pushups"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rightBicepValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rightBicep"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rightThighValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rightThigh"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sexValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sex"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"squatsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"squats"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stomachValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stomach"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"waistValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"waist"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"weightValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"weight"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic activityLevel;

- (double)activityLevelValue {
	NSNumber *result = [self activityLevel];
	return [result doubleValue];
}

- (void)setActivityLevelValue:(double)value_ {
	[self setActivityLevel:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveActivityLevelValue {
	NSNumber *result = [self primitiveActivityLevel];
	return [result doubleValue];
}

- (void)setPrimitiveActivityLevelValue:(double)value_ {
	[self setPrimitiveActivityLevel:[NSNumber numberWithDouble:value_]];
}

@dynamic age;

- (int16_t)ageValue {
	NSNumber *result = [self age];
	return [result shortValue];
}

- (void)setAgeValue:(int16_t)value_ {
	[self setAge:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveAgeValue {
	NSNumber *result = [self primitiveAge];
	return [result shortValue];
}

- (void)setPrimitiveAgeValue:(int16_t)value_ {
	[self setPrimitiveAge:[NSNumber numberWithShort:value_]];
}

@dynamic bmi;

- (double)bmiValue {
	NSNumber *result = [self bmi];
	return [result doubleValue];
}

- (void)setBmiValue:(double)value_ {
	[self setBmi:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveBmiValue {
	NSNumber *result = [self primitiveBmi];
	return [result doubleValue];
}

- (void)setPrimitiveBmiValue:(double)value_ {
	[self setPrimitiveBmi:[NSNumber numberWithDouble:value_]];
}

@dynamic bmr;

- (double)bmrValue {
	NSNumber *result = [self bmr];
	return [result doubleValue];
}

- (void)setBmrValue:(double)value_ {
	[self setBmr:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveBmrValue {
	NSNumber *result = [self primitiveBmr];
	return [result doubleValue];
}

- (void)setPrimitiveBmrValue:(double)value_ {
	[self setPrimitiveBmr:[NSNumber numberWithDouble:value_]];
}

@dynamic bodyFatPercent;

- (int16_t)bodyFatPercentValue {
	NSNumber *result = [self bodyFatPercent];
	return [result shortValue];
}

- (void)setBodyFatPercentValue:(int16_t)value_ {
	[self setBodyFatPercent:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveBodyFatPercentValue {
	NSNumber *result = [self primitiveBodyFatPercent];
	return [result shortValue];
}

- (void)setPrimitiveBodyFatPercentValue:(int16_t)value_ {
	[self setPrimitiveBodyFatPercent:[NSNumber numberWithShort:value_]];
}

@dynamic chest;

- (double)chestValue {
	NSNumber *result = [self chest];
	return [result doubleValue];
}

- (void)setChestValue:(double)value_ {
	[self setChest:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveChestValue {
	NSNumber *result = [self primitiveChest];
	return [result doubleValue];
}

- (void)setPrimitiveChestValue:(double)value_ {
	[self setPrimitiveChest:[NSNumber numberWithDouble:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic crunches;

- (int16_t)crunchesValue {
	NSNumber *result = [self crunches];
	return [result shortValue];
}

- (void)setCrunchesValue:(int16_t)value_ {
	[self setCrunches:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCrunchesValue {
	NSNumber *result = [self primitiveCrunches];
	return [result shortValue];
}

- (void)setPrimitiveCrunchesValue:(int16_t)value_ {
	[self setPrimitiveCrunches:[NSNumber numberWithShort:value_]];
}

@dynamic date;

@dynamic height;

- (double)heightValue {
	NSNumber *result = [self height];
	return [result doubleValue];
}

- (void)setHeightValue:(double)value_ {
	[self setHeight:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveHeightValue {
	NSNumber *result = [self primitiveHeight];
	return [result doubleValue];
}

- (void)setPrimitiveHeightValue:(double)value_ {
	[self setPrimitiveHeight:[NSNumber numberWithDouble:value_]];
}

@dynamic hips;

- (double)hipsValue {
	NSNumber *result = [self hips];
	return [result doubleValue];
}

- (void)setHipsValue:(double)value_ {
	[self setHips:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveHipsValue {
	NSNumber *result = [self primitiveHips];
	return [result doubleValue];
}

- (void)setPrimitiveHipsValue:(double)value_ {
	[self setPrimitiveHips:[NSNumber numberWithDouble:value_]];
}

@dynamic leftBicep;

- (double)leftBicepValue {
	NSNumber *result = [self leftBicep];
	return [result doubleValue];
}

- (void)setLeftBicepValue:(double)value_ {
	[self setLeftBicep:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLeftBicepValue {
	NSNumber *result = [self primitiveLeftBicep];
	return [result doubleValue];
}

- (void)setPrimitiveLeftBicepValue:(double)value_ {
	[self setPrimitiveLeftBicep:[NSNumber numberWithDouble:value_]];
}

@dynamic leftThigh;

- (double)leftThighValue {
	NSNumber *result = [self leftThigh];
	return [result doubleValue];
}

- (void)setLeftThighValue:(double)value_ {
	[self setLeftThigh:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLeftThighValue {
	NSNumber *result = [self primitiveLeftThigh];
	return [result doubleValue];
}

- (void)setPrimitiveLeftThighValue:(double)value_ {
	[self setPrimitiveLeftThigh:[NSNumber numberWithDouble:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic mileRun;

- (double)mileRunValue {
	NSNumber *result = [self mileRun];
	return [result doubleValue];
}

- (void)setMileRunValue:(double)value_ {
	[self setMileRun:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveMileRunValue {
	NSNumber *result = [self primitiveMileRun];
	return [result doubleValue];
}

- (void)setPrimitiveMileRunValue:(double)value_ {
	[self setPrimitiveMileRun:[NSNumber numberWithDouble:value_]];
}

@dynamic neck;

- (double)neckValue {
	NSNumber *result = [self neck];
	return [result doubleValue];
}

- (void)setNeckValue:(double)value_ {
	[self setNeck:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveNeckValue {
	NSNumber *result = [self primitiveNeck];
	return [result doubleValue];
}

- (void)setPrimitiveNeckValue:(double)value_ {
	[self setPrimitiveNeck:[NSNumber numberWithDouble:value_]];
}

@dynamic pullups;

- (int16_t)pullupsValue {
	NSNumber *result = [self pullups];
	return [result shortValue];
}

- (void)setPullupsValue:(int16_t)value_ {
	[self setPullups:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePullupsValue {
	NSNumber *result = [self primitivePullups];
	return [result shortValue];
}

- (void)setPrimitivePullupsValue:(int16_t)value_ {
	[self setPrimitivePullups:[NSNumber numberWithShort:value_]];
}

@dynamic pushups;

- (int16_t)pushupsValue {
	NSNumber *result = [self pushups];
	return [result shortValue];
}

- (void)setPushupsValue:(int16_t)value_ {
	[self setPushups:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePushupsValue {
	NSNumber *result = [self primitivePushups];
	return [result shortValue];
}

- (void)setPrimitivePushupsValue:(int16_t)value_ {
	[self setPrimitivePushups:[NSNumber numberWithShort:value_]];
}

@dynamic rightBicep;

- (double)rightBicepValue {
	NSNumber *result = [self rightBicep];
	return [result doubleValue];
}

- (void)setRightBicepValue:(double)value_ {
	[self setRightBicep:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveRightBicepValue {
	NSNumber *result = [self primitiveRightBicep];
	return [result doubleValue];
}

- (void)setPrimitiveRightBicepValue:(double)value_ {
	[self setPrimitiveRightBicep:[NSNumber numberWithDouble:value_]];
}

@dynamic rightThigh;

- (double)rightThighValue {
	NSNumber *result = [self rightThigh];
	return [result doubleValue];
}

- (void)setRightThighValue:(double)value_ {
	[self setRightThigh:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveRightThighValue {
	NSNumber *result = [self primitiveRightThigh];
	return [result doubleValue];
}

- (void)setPrimitiveRightThighValue:(double)value_ {
	[self setPrimitiveRightThigh:[NSNumber numberWithDouble:value_]];
}

@dynamic sex;

- (int16_t)sexValue {
	NSNumber *result = [self sex];
	return [result shortValue];
}

- (void)setSexValue:(int16_t)value_ {
	[self setSex:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSexValue {
	NSNumber *result = [self primitiveSex];
	return [result shortValue];
}

- (void)setPrimitiveSexValue:(int16_t)value_ {
	[self setPrimitiveSex:[NSNumber numberWithShort:value_]];
}

@dynamic squats;

- (int16_t)squatsValue {
	NSNumber *result = [self squats];
	return [result shortValue];
}

- (void)setSquatsValue:(int16_t)value_ {
	[self setSquats:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSquatsValue {
	NSNumber *result = [self primitiveSquats];
	return [result shortValue];
}

- (void)setPrimitiveSquatsValue:(int16_t)value_ {
	[self setPrimitiveSquats:[NSNumber numberWithShort:value_]];
}

@dynamic status;

@dynamic stomach;

- (double)stomachValue {
	NSNumber *result = [self stomach];
	return [result doubleValue];
}

- (void)setStomachValue:(double)value_ {
	[self setStomach:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveStomachValue {
	NSNumber *result = [self primitiveStomach];
	return [result doubleValue];
}

- (void)setPrimitiveStomachValue:(double)value_ {
	[self setPrimitiveStomach:[NSNumber numberWithDouble:value_]];
}

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic waist;

- (double)waistValue {
	NSNumber *result = [self waist];
	return [result doubleValue];
}

- (void)setWaistValue:(double)value_ {
	[self setWaist:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveWaistValue {
	NSNumber *result = [self primitiveWaist];
	return [result doubleValue];
}

- (void)setPrimitiveWaistValue:(double)value_ {
	[self setPrimitiveWaist:[NSNumber numberWithDouble:value_]];
}

@dynamic weight;

- (double)weightValue {
	NSNumber *result = [self weight];
	return [result doubleValue];
}

- (void)setWeightValue:(double)value_ {
	[self setWeight:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveWeightValue {
	NSNumber *result = [self primitiveWeight];
	return [result doubleValue];
}

- (void)setPrimitiveWeightValue:(double)value_ {
	[self setPrimitiveWeight:[NSNumber numberWithDouble:value_]];
}

@dynamic tracking;

@end

