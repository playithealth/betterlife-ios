// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanTimelineMO.h instead.

#import <CoreData/CoreData.h>
#import "AdvisorPlanTimelineMO.h"

@interface AdvisorActivityPlanTimelineMOID : AdvisorPlanTimelineMOID {}
@end

@interface _AdvisorActivityPlanTimelineMO : AdvisorPlanTimelineMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorActivityPlanTimelineMOID* objectID;

@end

@interface _AdvisorActivityPlanTimelineMO (CoreDataGeneratedPrimitiveAccessors)

@end
