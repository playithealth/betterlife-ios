// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanWorkoutActivitySetMO.h instead.

#import <CoreData/CoreData.h>
#import "ActivitySetMO.h"

@interface AdvisorActivityPlanWorkoutActivitySetMOID : ActivitySetMOID {}
@end

@interface _AdvisorActivityPlanWorkoutActivitySetMO : ActivitySetMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorActivityPlanWorkoutActivitySetMOID* objectID;

@end

@interface _AdvisorActivityPlanWorkoutActivitySetMO (CoreDataGeneratedPrimitiveAccessors)

@end
