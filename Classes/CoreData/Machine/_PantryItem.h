// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PantryItem.h instead.

#import <CoreData/CoreData.h>

extern const struct PantryItemAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *barcode;
	__unsafe_unretained NSString *calories;
	__unsafe_unretained NSString *caloriesFromFat;
	__unsafe_unretained NSString *carbohydrates;
	__unsafe_unretained NSString *cholesterol;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *createdOn;
	__unsafe_unretained NSString *fat;
	__unsafe_unretained NSString *imageId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *productId;
	__unsafe_unretained NSString *protein;
	__unsafe_unretained NSString *purchasedOn;
	__unsafe_unretained NSString *quantity;
	__unsafe_unretained NSString *reorderPoint;
	__unsafe_unretained NSString *saturatedFat;
	__unsafe_unretained NSString *sodium;
	__unsafe_unretained NSString *sourceId;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *updatedOn;
} PantryItemAttributes;

extern const struct PantryItemRelationships {
	__unsafe_unretained NSString *category;
} PantryItemRelationships;

@class CategoryMO;

@interface PantryItemID : NSManagedObjectID {}
@end

@interface _PantryItem : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PantryItemID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* barcode;

//- (BOOL)validateBarcode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* calories;

@property (atomic) int16_t caloriesValue;
- (int16_t)caloriesValue;
- (void)setCaloriesValue:(int16_t)value_;

//- (BOOL)validateCalories:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* caloriesFromFat;

@property (atomic) int16_t caloriesFromFatValue;
- (int16_t)caloriesFromFatValue;
- (void)setCaloriesFromFatValue:(int16_t)value_;

//- (BOOL)validateCaloriesFromFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* carbohydrates;

@property (atomic) double carbohydratesValue;
- (double)carbohydratesValue;
- (void)setCarbohydratesValue:(double)value_;

//- (BOOL)validateCarbohydrates:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cholesterol;

@property (atomic) double cholesterolValue;
- (double)cholesterolValue;
- (void)setCholesterolValue:(double)value_;

//- (BOOL)validateCholesterol:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* createdOn;

@property (atomic) int32_t createdOnValue;
- (int32_t)createdOnValue;
- (void)setCreatedOnValue:(int32_t)value_;

//- (BOOL)validateCreatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* fat;

@property (atomic) double fatValue;
- (double)fatValue;
- (void)setFatValue:(double)value_;

//- (BOOL)validateFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* imageId;

@property (atomic) int32_t imageIdValue;
- (int32_t)imageIdValue;
- (void)setImageIdValue:(int32_t)value_;

//- (BOOL)validateImageId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* productId;

@property (atomic) int32_t productIdValue;
- (int32_t)productIdValue;
- (void)setProductIdValue:(int32_t)value_;

//- (BOOL)validateProductId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* protein;

@property (atomic) double proteinValue;
- (double)proteinValue;
- (void)setProteinValue:(double)value_;

//- (BOOL)validateProtein:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* purchasedOn;

@property (atomic) int32_t purchasedOnValue;
- (int32_t)purchasedOnValue;
- (void)setPurchasedOnValue:(int32_t)value_;

//- (BOOL)validatePurchasedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* quantity;

@property (atomic) double quantityValue;
- (double)quantityValue;
- (void)setQuantityValue:(double)value_;

//- (BOOL)validateQuantity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* reorderPoint;

@property (atomic) double reorderPointValue;
- (double)reorderPointValue;
- (void)setReorderPointValue:(double)value_;

//- (BOOL)validateReorderPoint:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* saturatedFat;

@property (atomic) double saturatedFatValue;
- (double)saturatedFatValue;
- (void)setSaturatedFatValue:(double)value_;

//- (BOOL)validateSaturatedFat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sodium;

@property (atomic) double sodiumValue;
- (double)sodiumValue;
- (void)setSodiumValue:(double)value_;

//- (BOOL)validateSodium:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sourceId;

@property (atomic) int32_t sourceIdValue;
- (int32_t)sourceIdValue;
- (void)setSourceIdValue:(int32_t)value_;

//- (BOOL)validateSourceId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* status;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* updatedOn;

@property (atomic) int32_t updatedOnValue;
- (int32_t)updatedOnValue;
- (void)setUpdatedOnValue:(int32_t)value_;

//- (BOOL)validateUpdatedOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) CategoryMO *category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;

@end

@interface _PantryItem (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSString*)primitiveBarcode;
- (void)setPrimitiveBarcode:(NSString*)value;

- (NSNumber*)primitiveCalories;
- (void)setPrimitiveCalories:(NSNumber*)value;

- (int16_t)primitiveCaloriesValue;
- (void)setPrimitiveCaloriesValue:(int16_t)value_;

- (NSNumber*)primitiveCaloriesFromFat;
- (void)setPrimitiveCaloriesFromFat:(NSNumber*)value;

- (int16_t)primitiveCaloriesFromFatValue;
- (void)setPrimitiveCaloriesFromFatValue:(int16_t)value_;

- (NSNumber*)primitiveCarbohydrates;
- (void)setPrimitiveCarbohydrates:(NSNumber*)value;

- (double)primitiveCarbohydratesValue;
- (void)setPrimitiveCarbohydratesValue:(double)value_;

- (NSNumber*)primitiveCholesterol;
- (void)setPrimitiveCholesterol:(NSNumber*)value;

- (double)primitiveCholesterolValue;
- (void)setPrimitiveCholesterolValue:(double)value_;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveCreatedOn;
- (void)setPrimitiveCreatedOn:(NSNumber*)value;

- (int32_t)primitiveCreatedOnValue;
- (void)setPrimitiveCreatedOnValue:(int32_t)value_;

- (NSNumber*)primitiveFat;
- (void)setPrimitiveFat:(NSNumber*)value;

- (double)primitiveFatValue;
- (void)setPrimitiveFatValue:(double)value_;

- (NSNumber*)primitiveImageId;
- (void)setPrimitiveImageId:(NSNumber*)value;

- (int32_t)primitiveImageIdValue;
- (void)setPrimitiveImageIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveProductId;
- (void)setPrimitiveProductId:(NSNumber*)value;

- (int32_t)primitiveProductIdValue;
- (void)setPrimitiveProductIdValue:(int32_t)value_;

- (NSNumber*)primitiveProtein;
- (void)setPrimitiveProtein:(NSNumber*)value;

- (double)primitiveProteinValue;
- (void)setPrimitiveProteinValue:(double)value_;

- (NSNumber*)primitivePurchasedOn;
- (void)setPrimitivePurchasedOn:(NSNumber*)value;

- (int32_t)primitivePurchasedOnValue;
- (void)setPrimitivePurchasedOnValue:(int32_t)value_;

- (NSNumber*)primitiveQuantity;
- (void)setPrimitiveQuantity:(NSNumber*)value;

- (double)primitiveQuantityValue;
- (void)setPrimitiveQuantityValue:(double)value_;

- (NSNumber*)primitiveReorderPoint;
- (void)setPrimitiveReorderPoint:(NSNumber*)value;

- (double)primitiveReorderPointValue;
- (void)setPrimitiveReorderPointValue:(double)value_;

- (NSNumber*)primitiveSaturatedFat;
- (void)setPrimitiveSaturatedFat:(NSNumber*)value;

- (double)primitiveSaturatedFatValue;
- (void)setPrimitiveSaturatedFatValue:(double)value_;

- (NSNumber*)primitiveSodium;
- (void)setPrimitiveSodium:(NSNumber*)value;

- (double)primitiveSodiumValue;
- (void)setPrimitiveSodiumValue:(double)value_;

- (NSNumber*)primitiveSourceId;
- (void)setPrimitiveSourceId:(NSNumber*)value;

- (int32_t)primitiveSourceIdValue;
- (void)setPrimitiveSourceIdValue:(int32_t)value_;

- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;

- (NSNumber*)primitiveUpdatedOn;
- (void)setPrimitiveUpdatedOn:(NSNumber*)value;

- (int32_t)primitiveUpdatedOnValue;
- (void)setPrimitiveUpdatedOnValue:(int32_t)value_;

- (CategoryMO*)primitiveCategory;
- (void)setPrimitiveCategory:(CategoryMO*)value;

@end
