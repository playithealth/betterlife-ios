// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ExerciseRoutineMO.h instead.

#import <CoreData/CoreData.h>

extern const struct ExerciseRoutineMOAttributes {
	__unsafe_unretained NSString *activity;
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *factor;
} ExerciseRoutineMOAttributes;

extern const struct ExerciseRoutineMORelationships {
	__unsafe_unretained NSString *trainingActivities;
} ExerciseRoutineMORelationships;

@class TrainingActivityMO;

@interface ExerciseRoutineMOID : NSManagedObjectID {}
@end

@interface _ExerciseRoutineMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ExerciseRoutineMOID* objectID;

@property (nonatomic, strong) NSString* activity;

//- (BOOL)validateActivity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* factor;

@property (atomic) double factorValue;
- (double)factorValue;
- (void)setFactorValue:(double)value_;

//- (BOOL)validateFactor:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *trainingActivities;

- (NSMutableSet*)trainingActivitiesSet;

@end

@interface _ExerciseRoutineMO (TrainingActivitiesCoreDataGeneratedAccessors)
- (void)addTrainingActivities:(NSSet*)value_;
- (void)removeTrainingActivities:(NSSet*)value_;
- (void)addTrainingActivitiesObject:(TrainingActivityMO*)value_;
- (void)removeTrainingActivitiesObject:(TrainingActivityMO*)value_;

@end

@interface _ExerciseRoutineMO (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveActivity;
- (void)setPrimitiveActivity:(NSString*)value;

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveFactor;
- (void)setPrimitiveFactor:(NSNumber*)value;

- (double)primitiveFactorValue;
- (void)setPrimitiveFactorValue:(double)value_;

- (NSMutableSet*)primitiveTrainingActivities;
- (void)setPrimitiveTrainingActivities:(NSMutableSet*)value;

@end
