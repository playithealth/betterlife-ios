// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealPlannerShoppingListItem.h instead.

#import <CoreData/CoreData.h>

extern const struct MealPlannerShoppingListItemAttributes {
	__unsafe_unretained NSString *dateAdded;
	__unsafe_unretained NSString *quantity;
} MealPlannerShoppingListItemAttributes;

extern const struct MealPlannerShoppingListItemRelationships {
	__unsafe_unretained NSString *ingredient;
	__unsafe_unretained NSString *mealPlannerDay;
	__unsafe_unretained NSString *shoppingListItem;
} MealPlannerShoppingListItemRelationships;

@class MealIngredient;
@class MealPlannerDay;
@class ShoppingListItem;

@interface MealPlannerShoppingListItemID : NSManagedObjectID {}
@end

@interface _MealPlannerShoppingListItem : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MealPlannerShoppingListItemID* objectID;

@property (nonatomic, strong) NSNumber* dateAdded;

@property (atomic) int32_t dateAddedValue;
- (int32_t)dateAddedValue;
- (void)setDateAddedValue:(int32_t)value_;

//- (BOOL)validateDateAdded:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* quantity;

@property (atomic) int16_t quantityValue;
- (int16_t)quantityValue;
- (void)setQuantityValue:(int16_t)value_;

//- (BOOL)validateQuantity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MealIngredient *ingredient;

//- (BOOL)validateIngredient:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MealPlannerDay *mealPlannerDay;

//- (BOOL)validateMealPlannerDay:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) ShoppingListItem *shoppingListItem;

//- (BOOL)validateShoppingListItem:(id*)value_ error:(NSError**)error_;

@end

@interface _MealPlannerShoppingListItem (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveDateAdded;
- (void)setPrimitiveDateAdded:(NSNumber*)value;

- (int32_t)primitiveDateAddedValue;
- (void)setPrimitiveDateAddedValue:(int32_t)value_;

- (NSNumber*)primitiveQuantity;
- (void)setPrimitiveQuantity:(NSNumber*)value;

- (int16_t)primitiveQuantityValue;
- (void)setPrimitiveQuantityValue:(int16_t)value_;

- (MealIngredient*)primitiveIngredient;
- (void)setPrimitiveIngredient:(MealIngredient*)value;

- (MealPlannerDay*)primitiveMealPlannerDay;
- (void)setPrimitiveMealPlannerDay:(MealPlannerDay*)value;

- (ShoppingListItem*)primitiveShoppingListItem;
- (void)setPrimitiveShoppingListItem:(ShoppingListItem*)value;

@end
