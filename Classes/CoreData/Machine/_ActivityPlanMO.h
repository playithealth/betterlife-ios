// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityPlanMO.h instead.

#import <CoreData/CoreData.h>
#import "ActivityRecordMO.h"

@interface ActivityPlanMOID : ActivityRecordMOID {}
@end

@interface _ActivityPlanMO : ActivityRecordMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ActivityPlanMOID* objectID;

@end

@interface _ActivityPlanMO (CoreDataGeneratedPrimitiveAccessors)

@end
