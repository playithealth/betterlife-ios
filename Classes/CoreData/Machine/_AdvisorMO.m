// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMO.m instead.

#import "_AdvisorMO.h"

const struct AdvisorMOAttributes AdvisorMOAttributes = {
	.advisorId = @"advisorId",
	.canAddToShoppingList = @"canAddToShoppingList",
	.canDeleteFromShoppingList = @"canDeleteFromShoppingList",
	.canMessage = @"canMessage",
	.canRecommendExercise = @"canRecommendExercise",
	.canRecommendMealPlans = @"canRecommendMealPlans",
	.canRecommendMeals = @"canRecommendMeals",
	.canViewExercise = @"canViewExercise",
	.canViewMeals = @"canViewMeals",
	.canViewNutrition = @"canViewNutrition",
	.canViewPantry = @"canViewPantry",
	.canViewShoppingList = @"canViewShoppingList",
	.confirmation = @"confirmation",
	.loginId = @"loginId",
	.status = @"status",
	.updatedOn = @"updatedOn",
};

const struct AdvisorMORelationships AdvisorMORelationships = {
	.groups = @"groups",
};

@implementation AdvisorMOID
@end

@implementation _AdvisorMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Advisor" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Advisor";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Advisor" inManagedObjectContext:moc_];
}

- (AdvisorMOID*)objectID {
	return (AdvisorMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"advisorIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"advisorId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canAddToShoppingListValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canAddToShoppingList"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canDeleteFromShoppingListValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canDeleteFromShoppingList"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canMessageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canMessage"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canRecommendExerciseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canRecommendExercise"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canRecommendMealPlansValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canRecommendMealPlans"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canRecommendMealsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canRecommendMeals"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canViewExerciseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canViewExercise"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canViewMealsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canViewMeals"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canViewNutritionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canViewNutrition"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canViewPantryValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canViewPantry"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"canViewShoppingListValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canViewShoppingList"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic advisorId;

- (int32_t)advisorIdValue {
	NSNumber *result = [self advisorId];
	return [result intValue];
}

- (void)setAdvisorIdValue:(int32_t)value_ {
	[self setAdvisorId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAdvisorIdValue {
	NSNumber *result = [self primitiveAdvisorId];
	return [result intValue];
}

- (void)setPrimitiveAdvisorIdValue:(int32_t)value_ {
	[self setPrimitiveAdvisorId:[NSNumber numberWithInt:value_]];
}

@dynamic canAddToShoppingList;

- (BOOL)canAddToShoppingListValue {
	NSNumber *result = [self canAddToShoppingList];
	return [result boolValue];
}

- (void)setCanAddToShoppingListValue:(BOOL)value_ {
	[self setCanAddToShoppingList:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanAddToShoppingListValue {
	NSNumber *result = [self primitiveCanAddToShoppingList];
	return [result boolValue];
}

- (void)setPrimitiveCanAddToShoppingListValue:(BOOL)value_ {
	[self setPrimitiveCanAddToShoppingList:[NSNumber numberWithBool:value_]];
}

@dynamic canDeleteFromShoppingList;

- (BOOL)canDeleteFromShoppingListValue {
	NSNumber *result = [self canDeleteFromShoppingList];
	return [result boolValue];
}

- (void)setCanDeleteFromShoppingListValue:(BOOL)value_ {
	[self setCanDeleteFromShoppingList:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanDeleteFromShoppingListValue {
	NSNumber *result = [self primitiveCanDeleteFromShoppingList];
	return [result boolValue];
}

- (void)setPrimitiveCanDeleteFromShoppingListValue:(BOOL)value_ {
	[self setPrimitiveCanDeleteFromShoppingList:[NSNumber numberWithBool:value_]];
}

@dynamic canMessage;

- (BOOL)canMessageValue {
	NSNumber *result = [self canMessage];
	return [result boolValue];
}

- (void)setCanMessageValue:(BOOL)value_ {
	[self setCanMessage:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanMessageValue {
	NSNumber *result = [self primitiveCanMessage];
	return [result boolValue];
}

- (void)setPrimitiveCanMessageValue:(BOOL)value_ {
	[self setPrimitiveCanMessage:[NSNumber numberWithBool:value_]];
}

@dynamic canRecommendExercise;

- (BOOL)canRecommendExerciseValue {
	NSNumber *result = [self canRecommendExercise];
	return [result boolValue];
}

- (void)setCanRecommendExerciseValue:(BOOL)value_ {
	[self setCanRecommendExercise:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanRecommendExerciseValue {
	NSNumber *result = [self primitiveCanRecommendExercise];
	return [result boolValue];
}

- (void)setPrimitiveCanRecommendExerciseValue:(BOOL)value_ {
	[self setPrimitiveCanRecommendExercise:[NSNumber numberWithBool:value_]];
}

@dynamic canRecommendMealPlans;

- (BOOL)canRecommendMealPlansValue {
	NSNumber *result = [self canRecommendMealPlans];
	return [result boolValue];
}

- (void)setCanRecommendMealPlansValue:(BOOL)value_ {
	[self setCanRecommendMealPlans:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanRecommendMealPlansValue {
	NSNumber *result = [self primitiveCanRecommendMealPlans];
	return [result boolValue];
}

- (void)setPrimitiveCanRecommendMealPlansValue:(BOOL)value_ {
	[self setPrimitiveCanRecommendMealPlans:[NSNumber numberWithBool:value_]];
}

@dynamic canRecommendMeals;

- (BOOL)canRecommendMealsValue {
	NSNumber *result = [self canRecommendMeals];
	return [result boolValue];
}

- (void)setCanRecommendMealsValue:(BOOL)value_ {
	[self setCanRecommendMeals:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanRecommendMealsValue {
	NSNumber *result = [self primitiveCanRecommendMeals];
	return [result boolValue];
}

- (void)setPrimitiveCanRecommendMealsValue:(BOOL)value_ {
	[self setPrimitiveCanRecommendMeals:[NSNumber numberWithBool:value_]];
}

@dynamic canViewExercise;

- (BOOL)canViewExerciseValue {
	NSNumber *result = [self canViewExercise];
	return [result boolValue];
}

- (void)setCanViewExerciseValue:(BOOL)value_ {
	[self setCanViewExercise:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanViewExerciseValue {
	NSNumber *result = [self primitiveCanViewExercise];
	return [result boolValue];
}

- (void)setPrimitiveCanViewExerciseValue:(BOOL)value_ {
	[self setPrimitiveCanViewExercise:[NSNumber numberWithBool:value_]];
}

@dynamic canViewMeals;

- (BOOL)canViewMealsValue {
	NSNumber *result = [self canViewMeals];
	return [result boolValue];
}

- (void)setCanViewMealsValue:(BOOL)value_ {
	[self setCanViewMeals:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanViewMealsValue {
	NSNumber *result = [self primitiveCanViewMeals];
	return [result boolValue];
}

- (void)setPrimitiveCanViewMealsValue:(BOOL)value_ {
	[self setPrimitiveCanViewMeals:[NSNumber numberWithBool:value_]];
}

@dynamic canViewNutrition;

- (BOOL)canViewNutritionValue {
	NSNumber *result = [self canViewNutrition];
	return [result boolValue];
}

- (void)setCanViewNutritionValue:(BOOL)value_ {
	[self setCanViewNutrition:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanViewNutritionValue {
	NSNumber *result = [self primitiveCanViewNutrition];
	return [result boolValue];
}

- (void)setPrimitiveCanViewNutritionValue:(BOOL)value_ {
	[self setPrimitiveCanViewNutrition:[NSNumber numberWithBool:value_]];
}

@dynamic canViewPantry;

- (BOOL)canViewPantryValue {
	NSNumber *result = [self canViewPantry];
	return [result boolValue];
}

- (void)setCanViewPantryValue:(BOOL)value_ {
	[self setCanViewPantry:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanViewPantryValue {
	NSNumber *result = [self primitiveCanViewPantry];
	return [result boolValue];
}

- (void)setPrimitiveCanViewPantryValue:(BOOL)value_ {
	[self setPrimitiveCanViewPantry:[NSNumber numberWithBool:value_]];
}

@dynamic canViewShoppingList;

- (BOOL)canViewShoppingListValue {
	NSNumber *result = [self canViewShoppingList];
	return [result boolValue];
}

- (void)setCanViewShoppingListValue:(BOOL)value_ {
	[self setCanViewShoppingList:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanViewShoppingListValue {
	NSNumber *result = [self primitiveCanViewShoppingList];
	return [result boolValue];
}

- (void)setPrimitiveCanViewShoppingListValue:(BOOL)value_ {
	[self setPrimitiveCanViewShoppingList:[NSNumber numberWithBool:value_]];
}

@dynamic confirmation;

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic status;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic groups;

- (NSMutableSet*)groupsSet {
	[self willAccessValueForKey:@"groups"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"groups"];

	[self didAccessValueForKey:@"groups"];
	return result;
}

@end

