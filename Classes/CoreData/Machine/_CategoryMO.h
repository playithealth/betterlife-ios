// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CategoryMO.h instead.

#import <CoreData/CoreData.h>

extern const struct CategoryMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *nameFirstCharacter;
} CategoryMOAttributes;

extern const struct CategoryMORelationships {
	__unsafe_unretained NSString *customFoods;
	__unsafe_unretained NSString *mealIngredients;
	__unsafe_unretained NSString *pantryItems;
	__unsafe_unretained NSString *purchaseHxItems;
	__unsafe_unretained NSString *recommendations;
	__unsafe_unretained NSString *scannedItems;
	__unsafe_unretained NSString *shoppingListItems;
	__unsafe_unretained NSString *storeCategories;
} CategoryMORelationships;

@class CustomFoodMO;
@class MealIngredient;
@class PantryItem;
@class PurchaseHxItem;
@class Recommendation;
@class ScannedItem;
@class ShoppingListItem;
@class StoreCategory;

@interface CategoryMOID : NSManagedObjectID {}
@end

@interface _CategoryMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CategoryMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nameFirstCharacter;

//- (BOOL)validateNameFirstCharacter:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *customFoods;

- (NSMutableSet*)customFoodsSet;

@property (nonatomic, strong) NSSet *mealIngredients;

- (NSMutableSet*)mealIngredientsSet;

@property (nonatomic, strong) NSSet *pantryItems;

- (NSMutableSet*)pantryItemsSet;

@property (nonatomic, strong) NSSet *purchaseHxItems;

- (NSMutableSet*)purchaseHxItemsSet;

@property (nonatomic, strong) NSSet *recommendations;

- (NSMutableSet*)recommendationsSet;

@property (nonatomic, strong) NSSet *scannedItems;

- (NSMutableSet*)scannedItemsSet;

@property (nonatomic, strong) NSSet *shoppingListItems;

- (NSMutableSet*)shoppingListItemsSet;

@property (nonatomic, strong) NSSet *storeCategories;

- (NSMutableSet*)storeCategoriesSet;

@end

@interface _CategoryMO (CustomFoodsCoreDataGeneratedAccessors)
- (void)addCustomFoods:(NSSet*)value_;
- (void)removeCustomFoods:(NSSet*)value_;
- (void)addCustomFoodsObject:(CustomFoodMO*)value_;
- (void)removeCustomFoodsObject:(CustomFoodMO*)value_;

@end

@interface _CategoryMO (MealIngredientsCoreDataGeneratedAccessors)
- (void)addMealIngredients:(NSSet*)value_;
- (void)removeMealIngredients:(NSSet*)value_;
- (void)addMealIngredientsObject:(MealIngredient*)value_;
- (void)removeMealIngredientsObject:(MealIngredient*)value_;

@end

@interface _CategoryMO (PantryItemsCoreDataGeneratedAccessors)
- (void)addPantryItems:(NSSet*)value_;
- (void)removePantryItems:(NSSet*)value_;
- (void)addPantryItemsObject:(PantryItem*)value_;
- (void)removePantryItemsObject:(PantryItem*)value_;

@end

@interface _CategoryMO (PurchaseHxItemsCoreDataGeneratedAccessors)
- (void)addPurchaseHxItems:(NSSet*)value_;
- (void)removePurchaseHxItems:(NSSet*)value_;
- (void)addPurchaseHxItemsObject:(PurchaseHxItem*)value_;
- (void)removePurchaseHxItemsObject:(PurchaseHxItem*)value_;

@end

@interface _CategoryMO (RecommendationsCoreDataGeneratedAccessors)
- (void)addRecommendations:(NSSet*)value_;
- (void)removeRecommendations:(NSSet*)value_;
- (void)addRecommendationsObject:(Recommendation*)value_;
- (void)removeRecommendationsObject:(Recommendation*)value_;

@end

@interface _CategoryMO (ScannedItemsCoreDataGeneratedAccessors)
- (void)addScannedItems:(NSSet*)value_;
- (void)removeScannedItems:(NSSet*)value_;
- (void)addScannedItemsObject:(ScannedItem*)value_;
- (void)removeScannedItemsObject:(ScannedItem*)value_;

@end

@interface _CategoryMO (ShoppingListItemsCoreDataGeneratedAccessors)
- (void)addShoppingListItems:(NSSet*)value_;
- (void)removeShoppingListItems:(NSSet*)value_;
- (void)addShoppingListItemsObject:(ShoppingListItem*)value_;
- (void)removeShoppingListItemsObject:(ShoppingListItem*)value_;

@end

@interface _CategoryMO (StoreCategoriesCoreDataGeneratedAccessors)
- (void)addStoreCategories:(NSSet*)value_;
- (void)removeStoreCategories:(NSSet*)value_;
- (void)addStoreCategoriesObject:(StoreCategory*)value_;
- (void)removeStoreCategoriesObject:(StoreCategory*)value_;

@end

@interface _CategoryMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveNameFirstCharacter;
- (void)setPrimitiveNameFirstCharacter:(NSString*)value;

- (NSMutableSet*)primitiveCustomFoods;
- (void)setPrimitiveCustomFoods:(NSMutableSet*)value;

- (NSMutableSet*)primitiveMealIngredients;
- (void)setPrimitiveMealIngredients:(NSMutableSet*)value;

- (NSMutableSet*)primitivePantryItems;
- (void)setPrimitivePantryItems:(NSMutableSet*)value;

- (NSMutableSet*)primitivePurchaseHxItems;
- (void)setPrimitivePurchaseHxItems:(NSMutableSet*)value;

- (NSMutableSet*)primitiveRecommendations;
- (void)setPrimitiveRecommendations:(NSMutableSet*)value;

- (NSMutableSet*)primitiveScannedItems;
- (void)setPrimitiveScannedItems:(NSMutableSet*)value;

- (NSMutableSet*)primitiveShoppingListItems;
- (void)setPrimitiveShoppingListItems:(NSMutableSet*)value;

- (NSMutableSet*)primitiveStoreCategories;
- (void)setPrimitiveStoreCategories:(NSMutableSet*)value;

@end
