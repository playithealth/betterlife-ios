// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MessageDeliveryMO.m instead.

#import "_MessageDeliveryMO.h"

const struct MessageDeliveryMOAttributes MessageDeliveryMOAttributes = {
	.cloudId = @"cloudId",
	.isRead = @"isRead",
	.loginId = @"loginId",
	.receiverRm = @"receiverRm",
	.senderRm = @"senderRm",
	.status = @"status",
	.updatedOn = @"updatedOn",
};

const struct MessageDeliveryMORelationships MessageDeliveryMORelationships = {
	.conversation = @"conversation",
	.message = @"message",
};

@implementation MessageDeliveryMOID
@end

@implementation _MessageDeliveryMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MessageDelivery" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MessageDelivery";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MessageDelivery" inManagedObjectContext:moc_];
}

- (MessageDeliveryMOID*)objectID {
	return (MessageDeliveryMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isReadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRead"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"receiverRmValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"receiverRm"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"senderRmValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"senderRm"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic isRead;

- (BOOL)isReadValue {
	NSNumber *result = [self isRead];
	return [result boolValue];
}

- (void)setIsReadValue:(BOOL)value_ {
	[self setIsRead:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsReadValue {
	NSNumber *result = [self primitiveIsRead];
	return [result boolValue];
}

- (void)setPrimitiveIsReadValue:(BOOL)value_ {
	[self setPrimitiveIsRead:[NSNumber numberWithBool:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic receiverRm;

- (BOOL)receiverRmValue {
	NSNumber *result = [self receiverRm];
	return [result boolValue];
}

- (void)setReceiverRmValue:(BOOL)value_ {
	[self setReceiverRm:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveReceiverRmValue {
	NSNumber *result = [self primitiveReceiverRm];
	return [result boolValue];
}

- (void)setPrimitiveReceiverRmValue:(BOOL)value_ {
	[self setPrimitiveReceiverRm:[NSNumber numberWithBool:value_]];
}

@dynamic senderRm;

- (BOOL)senderRmValue {
	NSNumber *result = [self senderRm];
	return [result boolValue];
}

- (void)setSenderRmValue:(BOOL)value_ {
	[self setSenderRm:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSenderRmValue {
	NSNumber *result = [self primitiveSenderRm];
	return [result boolValue];
}

- (void)setPrimitiveSenderRmValue:(BOOL)value_ {
	[self setPrimitiveSenderRm:[NSNumber numberWithBool:value_]];
}

@dynamic status;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic conversation;

@dynamic message;

@end

