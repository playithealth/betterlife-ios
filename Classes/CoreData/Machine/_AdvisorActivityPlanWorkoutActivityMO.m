// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanWorkoutActivityMO.m instead.

#import "_AdvisorActivityPlanWorkoutActivityMO.h"

@implementation AdvisorActivityPlanWorkoutActivityMOID
@end

@implementation _AdvisorActivityPlanWorkoutActivityMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorActivityPlanWorkoutActivity" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorActivityPlanWorkoutActivity";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorActivityPlanWorkoutActivity" inManagedObjectContext:moc_];
}

- (AdvisorActivityPlanWorkoutActivityMOID*)objectID {
	return (AdvisorActivityPlanWorkoutActivityMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@end

