// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SyncUpdateMO.h instead.

#import <CoreData/CoreData.h>

extern const struct SyncUpdateMOAttributes {
	__unsafe_unretained NSString *lastUpdate;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *tableName;
} SyncUpdateMOAttributes;

@interface SyncUpdateMOID : NSManagedObjectID {}
@end

@interface _SyncUpdateMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SyncUpdateMOID* objectID;

@property (nonatomic, strong) NSNumber* lastUpdate;

@property (atomic) int32_t lastUpdateValue;
- (int32_t)lastUpdateValue;
- (void)setLastUpdateValue:(int32_t)value_;

//- (BOOL)validateLastUpdate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* tableName;

//- (BOOL)validateTableName:(id*)value_ error:(NSError**)error_;

@end

@interface _SyncUpdateMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveLastUpdate;
- (void)setPrimitiveLastUpdate:(NSNumber*)value;

- (int32_t)primitiveLastUpdateValue;
- (void)setPrimitiveLastUpdateValue:(int32_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveTableName;
- (void)setPrimitiveTableName:(NSString*)value;

@end
