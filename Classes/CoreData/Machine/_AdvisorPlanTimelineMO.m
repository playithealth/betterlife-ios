// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorPlanTimelineMO.m instead.

#import "_AdvisorPlanTimelineMO.h"

const struct AdvisorPlanTimelineMOAttributes AdvisorPlanTimelineMOAttributes = {
	.startDate = @"startDate",
	.stopDate = @"stopDate",
};

const struct AdvisorPlanTimelineMORelationships AdvisorPlanTimelineMORelationships = {
	.plan = @"plan",
};

@implementation AdvisorPlanTimelineMOID
@end

@implementation _AdvisorPlanTimelineMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AbstractAdvisorPlanTimeline" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AbstractAdvisorPlanTimeline";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AbstractAdvisorPlanTimeline" inManagedObjectContext:moc_];
}

- (AdvisorPlanTimelineMOID*)objectID {
	return (AdvisorPlanTimelineMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic startDate;

@dynamic stopDate;

@dynamic plan;

@end

