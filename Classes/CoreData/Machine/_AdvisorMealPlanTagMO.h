// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanTagMO.h instead.

#import <CoreData/CoreData.h>

extern const struct AdvisorMealPlanTagMOAttributes {
	__unsafe_unretained NSString *cloudId;
	__unsafe_unretained NSString *loginId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *servingTargetQuantity;
	__unsafe_unretained NSString *servingTargetUnit;
} AdvisorMealPlanTagMOAttributes;

extern const struct AdvisorMealPlanTagMORelationships {
	__unsafe_unretained NSString *entry;
	__unsafe_unretained NSString *foodLogs;
	__unsafe_unretained NSString *mealPlannerDays;
	__unsafe_unretained NSString *mealPredictions;
} AdvisorMealPlanTagMORelationships;

@class AdvisorMealPlanEntryMO;
@class FoodLogMO;
@class MealPlannerDay;
@class MealPredictionMO;

@interface AdvisorMealPlanTagMOID : NSManagedObjectID {}
@end

@interface _AdvisorMealPlanTagMO : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AdvisorMealPlanTagMOID* objectID;

@property (nonatomic, strong) NSNumber* cloudId;

@property (atomic) int32_t cloudIdValue;
- (int32_t)cloudIdValue;
- (void)setCloudIdValue:(int32_t)value_;

//- (BOOL)validateCloudId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* loginId;

@property (atomic) int32_t loginIdValue;
- (int32_t)loginIdValue;
- (void)setLoginIdValue:(int32_t)value_;

//- (BOOL)validateLoginId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* servingTargetQuantity;

@property (atomic) int16_t servingTargetQuantityValue;
- (int16_t)servingTargetQuantityValue;
- (void)setServingTargetQuantityValue:(int16_t)value_;

//- (BOOL)validateServingTargetQuantity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* servingTargetUnit;

//- (BOOL)validateServingTargetUnit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AdvisorMealPlanEntryMO *entry;

//- (BOOL)validateEntry:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *foodLogs;

- (NSMutableSet*)foodLogsSet;

@property (nonatomic, strong) NSSet *mealPlannerDays;

- (NSMutableSet*)mealPlannerDaysSet;

@property (nonatomic, strong) NSSet *mealPredictions;

- (NSMutableSet*)mealPredictionsSet;

@end

@interface _AdvisorMealPlanTagMO (FoodLogsCoreDataGeneratedAccessors)
- (void)addFoodLogs:(NSSet*)value_;
- (void)removeFoodLogs:(NSSet*)value_;
- (void)addFoodLogsObject:(FoodLogMO*)value_;
- (void)removeFoodLogsObject:(FoodLogMO*)value_;

@end

@interface _AdvisorMealPlanTagMO (MealPlannerDaysCoreDataGeneratedAccessors)
- (void)addMealPlannerDays:(NSSet*)value_;
- (void)removeMealPlannerDays:(NSSet*)value_;
- (void)addMealPlannerDaysObject:(MealPlannerDay*)value_;
- (void)removeMealPlannerDaysObject:(MealPlannerDay*)value_;

@end

@interface _AdvisorMealPlanTagMO (MealPredictionsCoreDataGeneratedAccessors)
- (void)addMealPredictions:(NSSet*)value_;
- (void)removeMealPredictions:(NSSet*)value_;
- (void)addMealPredictionsObject:(MealPredictionMO*)value_;
- (void)removeMealPredictionsObject:(MealPredictionMO*)value_;

@end

@interface _AdvisorMealPlanTagMO (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCloudId;
- (void)setPrimitiveCloudId:(NSNumber*)value;

- (int32_t)primitiveCloudIdValue;
- (void)setPrimitiveCloudIdValue:(int32_t)value_;

- (NSNumber*)primitiveLoginId;
- (void)setPrimitiveLoginId:(NSNumber*)value;

- (int32_t)primitiveLoginIdValue;
- (void)setPrimitiveLoginIdValue:(int32_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveServingTargetQuantity;
- (void)setPrimitiveServingTargetQuantity:(NSNumber*)value;

- (int16_t)primitiveServingTargetQuantityValue;
- (void)setPrimitiveServingTargetQuantityValue:(int16_t)value_;

- (NSString*)primitiveServingTargetUnit;
- (void)setPrimitiveServingTargetUnit:(NSString*)value;

- (AdvisorMealPlanEntryMO*)primitiveEntry;
- (void)setPrimitiveEntry:(AdvisorMealPlanEntryMO*)value;

- (NSMutableSet*)primitiveFoodLogs;
- (void)setPrimitiveFoodLogs:(NSMutableSet*)value;

- (NSMutableSet*)primitiveMealPlannerDays;
- (void)setPrimitiveMealPlannerDays:(NSMutableSet*)value;

- (NSMutableSet*)primitiveMealPredictions;
- (void)setPrimitiveMealPredictions:(NSMutableSet*)value;

@end
