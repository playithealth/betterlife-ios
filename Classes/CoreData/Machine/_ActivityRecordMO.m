// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityRecordMO.m instead.

#import "_ActivityRecordMO.h"

const struct ActivityRecordMOAttributes ActivityRecordMOAttributes = {
	.activityId = @"activityId",
	.isRemoved = @"isRemoved",
	.recordId = @"recordId",
	.status = @"status",
	.targetDate = @"targetDate",
	.targetDateString = @"targetDateString",
	.updatedOn = @"updatedOn",
};

const struct ActivityRecordMORelationships ActivityRecordMORelationships = {
	.linkedWorkout = @"linkedWorkout",
};

@implementation ActivityRecordMOID
@end

@implementation _ActivityRecordMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ActivityRecord" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ActivityRecord";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ActivityRecord" inManagedObjectContext:moc_];
}

- (ActivityRecordMOID*)objectID {
	return (ActivityRecordMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"activityIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"activityId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isRemovedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRemoved"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"recordIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"recordId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic activityId;

- (int32_t)activityIdValue {
	NSNumber *result = [self activityId];
	return [result intValue];
}

- (void)setActivityIdValue:(int32_t)value_ {
	[self setActivityId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveActivityIdValue {
	NSNumber *result = [self primitiveActivityId];
	return [result intValue];
}

- (void)setPrimitiveActivityIdValue:(int32_t)value_ {
	[self setPrimitiveActivityId:[NSNumber numberWithInt:value_]];
}

@dynamic isRemoved;

- (BOOL)isRemovedValue {
	NSNumber *result = [self isRemoved];
	return [result boolValue];
}

- (void)setIsRemovedValue:(BOOL)value_ {
	[self setIsRemoved:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsRemovedValue {
	NSNumber *result = [self primitiveIsRemoved];
	return [result boolValue];
}

- (void)setPrimitiveIsRemovedValue:(BOOL)value_ {
	[self setPrimitiveIsRemoved:[NSNumber numberWithBool:value_]];
}

@dynamic recordId;

- (int32_t)recordIdValue {
	NSNumber *result = [self recordId];
	return [result intValue];
}

- (void)setRecordIdValue:(int32_t)value_ {
	[self setRecordId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRecordIdValue {
	NSNumber *result = [self primitiveRecordId];
	return [result intValue];
}

- (void)setPrimitiveRecordIdValue:(int32_t)value_ {
	[self setPrimitiveRecordId:[NSNumber numberWithInt:value_]];
}

@dynamic status;

@dynamic targetDate;

@dynamic targetDateString;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic linkedWorkout;

@end

