// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorMealPlanEntryMO.m instead.

#import "_AdvisorMealPlanEntryMO.h"

const struct AdvisorMealPlanEntryMOAttributes AdvisorMealPlanEntryMOAttributes = {
	.cloudId = @"cloudId",
	.logTime = @"logTime",
	.loginId = @"loginId",
	.nutritionPercent = @"nutritionPercent",
	.unplannedMealOption = @"unplannedMealOption",
};

const struct AdvisorMealPlanEntryMORelationships AdvisorMealPlanEntryMORelationships = {
	.phase = @"phase",
	.tags = @"tags",
};

@implementation AdvisorMealPlanEntryMOID
@end

@implementation _AdvisorMealPlanEntryMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorMealPlanEntry" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorMealPlanEntry";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorMealPlanEntry" inManagedObjectContext:moc_];
}

- (AdvisorMealPlanEntryMOID*)objectID {
	return (AdvisorMealPlanEntryMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"logTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"logTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loginIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loginId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"nutritionPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"nutritionPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic logTime;

- (int16_t)logTimeValue {
	NSNumber *result = [self logTime];
	return [result shortValue];
}

- (void)setLogTimeValue:(int16_t)value_ {
	[self setLogTime:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLogTimeValue {
	NSNumber *result = [self primitiveLogTime];
	return [result shortValue];
}

- (void)setPrimitiveLogTimeValue:(int16_t)value_ {
	[self setPrimitiveLogTime:[NSNumber numberWithShort:value_]];
}

@dynamic loginId;

- (int32_t)loginIdValue {
	NSNumber *result = [self loginId];
	return [result intValue];
}

- (void)setLoginIdValue:(int32_t)value_ {
	[self setLoginId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveLoginIdValue {
	NSNumber *result = [self primitiveLoginId];
	return [result intValue];
}

- (void)setPrimitiveLoginIdValue:(int32_t)value_ {
	[self setPrimitiveLoginId:[NSNumber numberWithInt:value_]];
}

@dynamic nutritionPercent;

- (int16_t)nutritionPercentValue {
	NSNumber *result = [self nutritionPercent];
	return [result shortValue];
}

- (void)setNutritionPercentValue:(int16_t)value_ {
	[self setNutritionPercent:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveNutritionPercentValue {
	NSNumber *result = [self primitiveNutritionPercent];
	return [result shortValue];
}

- (void)setPrimitiveNutritionPercentValue:(int16_t)value_ {
	[self setPrimitiveNutritionPercent:[NSNumber numberWithShort:value_]];
}

@dynamic unplannedMealOption;

@dynamic phase;

@dynamic tags;

- (NSMutableSet*)tagsSet {
	[self willAccessValueForKey:@"tags"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"tags"];

	[self didAccessValueForKey:@"tags"];
	return result;
}

@end

