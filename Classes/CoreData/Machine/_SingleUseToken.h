// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SingleUseToken.h instead.

#import <CoreData/CoreData.h>

extern const struct SingleUseTokenAttributes {
	__unsafe_unretained NSString *accountId;
	__unsafe_unretained NSString *timeViewed;
	__unsafe_unretained NSString *token;
} SingleUseTokenAttributes;

@interface SingleUseTokenID : NSManagedObjectID {}
@end

@interface _SingleUseToken : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SingleUseTokenID* objectID;

@property (nonatomic, strong) NSNumber* accountId;

@property (atomic) int32_t accountIdValue;
- (int32_t)accountIdValue;
- (void)setAccountIdValue:(int32_t)value_;

//- (BOOL)validateAccountId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* timeViewed;

@property (atomic) int32_t timeViewedValue;
- (int32_t)timeViewedValue;
- (void)setTimeViewedValue:(int32_t)value_;

//- (BOOL)validateTimeViewed:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* token;

//- (BOOL)validateToken:(id*)value_ error:(NSError**)error_;

@end

@interface _SingleUseToken (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccountId;
- (void)setPrimitiveAccountId:(NSNumber*)value;

- (int32_t)primitiveAccountIdValue;
- (void)setPrimitiveAccountIdValue:(int32_t)value_;

- (NSNumber*)primitiveTimeViewed;
- (void)setPrimitiveTimeViewed:(NSNumber*)value;

- (int32_t)primitiveTimeViewedValue;
- (void)setPrimitiveTimeViewedValue:(int32_t)value_;

- (NSString*)primitiveToken;
- (void)setPrimitiveToken:(NSString*)value;

@end
