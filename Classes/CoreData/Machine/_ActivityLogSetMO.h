// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityLogSetMO.h instead.

#import <CoreData/CoreData.h>
#import "ActivitySetMO.h"

@interface ActivityLogSetMOID : ActivitySetMOID {}
@end

@interface _ActivityLogSetMO : ActivitySetMO {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ActivityLogSetMOID* objectID;

@end

@interface _ActivityLogSetMO (CoreDataGeneratedPrimitiveAccessors)

@end
