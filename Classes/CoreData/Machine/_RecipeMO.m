// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RecipeMO.m instead.

#import "_RecipeMO.h"

const struct RecipeMOAttributes RecipeMOAttributes = {
	.accountId = @"accountId",
	.advisorId = @"advisorId",
	.advisorName = @"advisorName",
	.calories = @"calories",
	.caloriesFromFat = @"caloriesFromFat",
	.carbs = @"carbs",
	.cholesterol = @"cholesterol",
	.cloudId = @"cloudId",
	.cookTime = @"cookTime",
	.cookTimeUnit = @"cookTimeUnit",
	.externalId = @"externalId",
	.fat = @"fat",
	.frequency = @"frequency",
	.imageId = @"imageId",
	.imageUrl = @"imageUrl",
	.instructions = @"instructions",
	.isOwner = @"isOwner",
	.name = @"name",
	.nutritionSummary = @"nutritionSummary",
	.prepTime = @"prepTime",
	.protein = @"protein",
	.rating = @"rating",
	.recommendedDate = @"recommendedDate",
	.removed = @"removed",
	.satfat = @"satfat",
	.serves = @"serves",
	.sodium = @"sodium",
	.status = @"status",
	.updatedOn = @"updatedOn",
	.url = @"url",
};

const struct RecipeMORelationships RecipeMORelationships = {
	.advisorGroups = @"advisorGroups",
	.foodLog = @"foodLog",
	.ingredients = @"ingredients",
	.mealPredictions = @"mealPredictions",
	.nutrition = @"nutrition",
	.plannerDays = @"plannerDays",
	.tags = @"tags",
};

@implementation RecipeMOID
@end

@implementation _RecipeMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Recipe" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Recipe";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Recipe" inManagedObjectContext:moc_];
}

- (RecipeMOID*)objectID {
	return (RecipeMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accountIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accountId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"advisorIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"advisorId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"calories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesFromFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"caloriesFromFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"carbsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"carbs"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cholesterolValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cholesterol"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cookTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cookTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"fatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"fat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"frequencyValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"frequency"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"imageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"imageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isOwnerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isOwner"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"prepTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"prepTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"proteinValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"protein"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ratingValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rating"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"removedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"removed"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"satfatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"satfat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"servesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"serves"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sodiumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sodium"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updatedOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accountId;

- (int32_t)accountIdValue {
	NSNumber *result = [self accountId];
	return [result intValue];
}

- (void)setAccountIdValue:(int32_t)value_ {
	[self setAccountId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAccountIdValue {
	NSNumber *result = [self primitiveAccountId];
	return [result intValue];
}

- (void)setPrimitiveAccountIdValue:(int32_t)value_ {
	[self setPrimitiveAccountId:[NSNumber numberWithInt:value_]];
}

@dynamic advisorId;

- (int32_t)advisorIdValue {
	NSNumber *result = [self advisorId];
	return [result intValue];
}

- (void)setAdvisorIdValue:(int32_t)value_ {
	[self setAdvisorId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAdvisorIdValue {
	NSNumber *result = [self primitiveAdvisorId];
	return [result intValue];
}

- (void)setPrimitiveAdvisorIdValue:(int32_t)value_ {
	[self setPrimitiveAdvisorId:[NSNumber numberWithInt:value_]];
}

@dynamic advisorName;

@dynamic calories;

- (int16_t)caloriesValue {
	NSNumber *result = [self calories];
	return [result shortValue];
}

- (void)setCaloriesValue:(int16_t)value_ {
	[self setCalories:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCaloriesValue {
	NSNumber *result = [self primitiveCalories];
	return [result shortValue];
}

- (void)setPrimitiveCaloriesValue:(int16_t)value_ {
	[self setPrimitiveCalories:[NSNumber numberWithShort:value_]];
}

@dynamic caloriesFromFat;

- (int16_t)caloriesFromFatValue {
	NSNumber *result = [self caloriesFromFat];
	return [result shortValue];
}

- (void)setCaloriesFromFatValue:(int16_t)value_ {
	[self setCaloriesFromFat:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCaloriesFromFatValue {
	NSNumber *result = [self primitiveCaloriesFromFat];
	return [result shortValue];
}

- (void)setPrimitiveCaloriesFromFatValue:(int16_t)value_ {
	[self setPrimitiveCaloriesFromFat:[NSNumber numberWithShort:value_]];
}

@dynamic carbs;

- (double)carbsValue {
	NSNumber *result = [self carbs];
	return [result doubleValue];
}

- (void)setCarbsValue:(double)value_ {
	[self setCarbs:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCarbsValue {
	NSNumber *result = [self primitiveCarbs];
	return [result doubleValue];
}

- (void)setPrimitiveCarbsValue:(double)value_ {
	[self setPrimitiveCarbs:[NSNumber numberWithDouble:value_]];
}

@dynamic cholesterol;

- (double)cholesterolValue {
	NSNumber *result = [self cholesterol];
	return [result doubleValue];
}

- (void)setCholesterolValue:(double)value_ {
	[self setCholesterol:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCholesterolValue {
	NSNumber *result = [self primitiveCholesterol];
	return [result doubleValue];
}

- (void)setPrimitiveCholesterolValue:(double)value_ {
	[self setPrimitiveCholesterol:[NSNumber numberWithDouble:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic cookTime;

- (int32_t)cookTimeValue {
	NSNumber *result = [self cookTime];
	return [result intValue];
}

- (void)setCookTimeValue:(int32_t)value_ {
	[self setCookTime:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCookTimeValue {
	NSNumber *result = [self primitiveCookTime];
	return [result intValue];
}

- (void)setPrimitiveCookTimeValue:(int32_t)value_ {
	[self setPrimitiveCookTime:[NSNumber numberWithInt:value_]];
}

@dynamic cookTimeUnit;

@dynamic externalId;

@dynamic fat;

- (double)fatValue {
	NSNumber *result = [self fat];
	return [result doubleValue];
}

- (void)setFatValue:(double)value_ {
	[self setFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveFatValue {
	NSNumber *result = [self primitiveFat];
	return [result doubleValue];
}

- (void)setPrimitiveFatValue:(double)value_ {
	[self setPrimitiveFat:[NSNumber numberWithDouble:value_]];
}

@dynamic frequency;

- (int16_t)frequencyValue {
	NSNumber *result = [self frequency];
	return [result shortValue];
}

- (void)setFrequencyValue:(int16_t)value_ {
	[self setFrequency:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveFrequencyValue {
	NSNumber *result = [self primitiveFrequency];
	return [result shortValue];
}

- (void)setPrimitiveFrequencyValue:(int16_t)value_ {
	[self setPrimitiveFrequency:[NSNumber numberWithShort:value_]];
}

@dynamic imageId;

- (int32_t)imageIdValue {
	NSNumber *result = [self imageId];
	return [result intValue];
}

- (void)setImageIdValue:(int32_t)value_ {
	[self setImageId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveImageIdValue {
	NSNumber *result = [self primitiveImageId];
	return [result intValue];
}

- (void)setPrimitiveImageIdValue:(int32_t)value_ {
	[self setPrimitiveImageId:[NSNumber numberWithInt:value_]];
}

@dynamic imageUrl;

@dynamic instructions;

@dynamic isOwner;

- (BOOL)isOwnerValue {
	NSNumber *result = [self isOwner];
	return [result boolValue];
}

- (void)setIsOwnerValue:(BOOL)value_ {
	[self setIsOwner:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsOwnerValue {
	NSNumber *result = [self primitiveIsOwner];
	return [result boolValue];
}

- (void)setPrimitiveIsOwnerValue:(BOOL)value_ {
	[self setPrimitiveIsOwner:[NSNumber numberWithBool:value_]];
}

@dynamic name;

@dynamic nutritionSummary;

@dynamic prepTime;

- (int32_t)prepTimeValue {
	NSNumber *result = [self prepTime];
	return [result intValue];
}

- (void)setPrepTimeValue:(int32_t)value_ {
	[self setPrepTime:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePrepTimeValue {
	NSNumber *result = [self primitivePrepTime];
	return [result intValue];
}

- (void)setPrimitivePrepTimeValue:(int32_t)value_ {
	[self setPrimitivePrepTime:[NSNumber numberWithInt:value_]];
}

@dynamic protein;

- (double)proteinValue {
	NSNumber *result = [self protein];
	return [result doubleValue];
}

- (void)setProteinValue:(double)value_ {
	[self setProtein:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveProteinValue {
	NSNumber *result = [self primitiveProtein];
	return [result doubleValue];
}

- (void)setPrimitiveProteinValue:(double)value_ {
	[self setPrimitiveProtein:[NSNumber numberWithDouble:value_]];
}

@dynamic rating;

- (int16_t)ratingValue {
	NSNumber *result = [self rating];
	return [result shortValue];
}

- (void)setRatingValue:(int16_t)value_ {
	[self setRating:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveRatingValue {
	NSNumber *result = [self primitiveRating];
	return [result shortValue];
}

- (void)setPrimitiveRatingValue:(int16_t)value_ {
	[self setPrimitiveRating:[NSNumber numberWithShort:value_]];
}

@dynamic recommendedDate;

@dynamic removed;

- (BOOL)removedValue {
	NSNumber *result = [self removed];
	return [result boolValue];
}

- (void)setRemovedValue:(BOOL)value_ {
	[self setRemoved:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveRemovedValue {
	NSNumber *result = [self primitiveRemoved];
	return [result boolValue];
}

- (void)setPrimitiveRemovedValue:(BOOL)value_ {
	[self setPrimitiveRemoved:[NSNumber numberWithBool:value_]];
}

@dynamic satfat;

- (double)satfatValue {
	NSNumber *result = [self satfat];
	return [result doubleValue];
}

- (void)setSatfatValue:(double)value_ {
	[self setSatfat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSatfatValue {
	NSNumber *result = [self primitiveSatfat];
	return [result doubleValue];
}

- (void)setPrimitiveSatfatValue:(double)value_ {
	[self setPrimitiveSatfat:[NSNumber numberWithDouble:value_]];
}

@dynamic serves;

- (int16_t)servesValue {
	NSNumber *result = [self serves];
	return [result shortValue];
}

- (void)setServesValue:(int16_t)value_ {
	[self setServes:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveServesValue {
	NSNumber *result = [self primitiveServes];
	return [result shortValue];
}

- (void)setPrimitiveServesValue:(int16_t)value_ {
	[self setPrimitiveServes:[NSNumber numberWithShort:value_]];
}

@dynamic sodium;

- (double)sodiumValue {
	NSNumber *result = [self sodium];
	return [result doubleValue];
}

- (void)setSodiumValue:(double)value_ {
	[self setSodium:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSodiumValue {
	NSNumber *result = [self primitiveSodium];
	return [result doubleValue];
}

- (void)setPrimitiveSodiumValue:(double)value_ {
	[self setPrimitiveSodium:[NSNumber numberWithDouble:value_]];
}

@dynamic status;

@dynamic updatedOn;

- (int32_t)updatedOnValue {
	NSNumber *result = [self updatedOn];
	return [result intValue];
}

- (void)setUpdatedOnValue:(int32_t)value_ {
	[self setUpdatedOn:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveUpdatedOnValue {
	NSNumber *result = [self primitiveUpdatedOn];
	return [result intValue];
}

- (void)setPrimitiveUpdatedOnValue:(int32_t)value_ {
	[self setPrimitiveUpdatedOn:[NSNumber numberWithInt:value_]];
}

@dynamic url;

@dynamic advisorGroups;

- (NSMutableSet*)advisorGroupsSet {
	[self willAccessValueForKey:@"advisorGroups"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"advisorGroups"];

	[self didAccessValueForKey:@"advisorGroups"];
	return result;
}

@dynamic foodLog;

- (NSMutableSet*)foodLogSet {
	[self willAccessValueForKey:@"foodLog"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"foodLog"];

	[self didAccessValueForKey:@"foodLog"];
	return result;
}

@dynamic ingredients;

- (NSMutableSet*)ingredientsSet {
	[self willAccessValueForKey:@"ingredients"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"ingredients"];

	[self didAccessValueForKey:@"ingredients"];
	return result;
}

@dynamic mealPredictions;

- (NSMutableSet*)mealPredictionsSet {
	[self willAccessValueForKey:@"mealPredictions"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mealPredictions"];

	[self didAccessValueForKey:@"mealPredictions"];
	return result;
}

@dynamic nutrition;

@dynamic plannerDays;

- (NSMutableSet*)plannerDaysSet {
	[self willAccessValueForKey:@"plannerDays"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"plannerDays"];

	[self didAccessValueForKey:@"plannerDays"];
	return result;
}

@dynamic tags;

- (NSMutableSet*)tagsSet {
	[self willAccessValueForKey:@"tags"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"tags"];

	[self didAccessValueForKey:@"tags"];
	return result;
}

@end

