// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AdvisorActivityPlanTimelineMO.m instead.

#import "_AdvisorActivityPlanTimelineMO.h"

@implementation AdvisorActivityPlanTimelineMOID
@end

@implementation _AdvisorActivityPlanTimelineMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AdvisorActivityPlanTimeline" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AdvisorActivityPlanTimeline";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AdvisorActivityPlanTimeline" inManagedObjectContext:moc_];
}

- (AdvisorActivityPlanTimelineMOID*)objectID {
	return (AdvisorActivityPlanTimelineMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@end

