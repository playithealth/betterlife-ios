// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealIngredient.m instead.

#import "_MealIngredient.h"

const struct MealIngredientAttributes MealIngredientAttributes = {
	.addToShoppingList = @"addToShoppingList",
	.answer = @"answer",
	.cloudId = @"cloudId",
	.fullText = @"fullText",
	.measure = @"measure",
	.measureUnits = @"measureUnits",
	.measureUnitsId = @"measureUnitsId",
	.name = @"name",
	.notes = @"notes",
	.productId = @"productId",
	.shoppingListQuantity = @"shoppingListQuantity",
	.shoppingListText = @"shoppingListText",
	.sortOrder = @"sortOrder",
	.status = @"status",
};

const struct MealIngredientRelationships MealIngredientRelationships = {
	.mealPlannerShoppingListItems = @"mealPlannerShoppingListItems",
	.recipe = @"recipe",
	.shoppingListCategory = @"shoppingListCategory",
};

@implementation MealIngredientID
@end

@implementation _MealIngredient

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MealIngredient" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MealIngredient";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MealIngredient" inManagedObjectContext:moc_];
}

- (MealIngredientID*)objectID {
	return (MealIngredientID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"addToShoppingListValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"addToShoppingList"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"answerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"answer"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cloudIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cloudId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"measureUnitsIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"measureUnitsId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"productIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"productId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"shoppingListQuantityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"shoppingListQuantity"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic addToShoppingList;

- (int16_t)addToShoppingListValue {
	NSNumber *result = [self addToShoppingList];
	return [result shortValue];
}

- (void)setAddToShoppingListValue:(int16_t)value_ {
	[self setAddToShoppingList:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveAddToShoppingListValue {
	NSNumber *result = [self primitiveAddToShoppingList];
	return [result shortValue];
}

- (void)setPrimitiveAddToShoppingListValue:(int16_t)value_ {
	[self setPrimitiveAddToShoppingList:[NSNumber numberWithShort:value_]];
}

@dynamic answer;

- (BOOL)answerValue {
	NSNumber *result = [self answer];
	return [result boolValue];
}

- (void)setAnswerValue:(BOOL)value_ {
	[self setAnswer:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAnswerValue {
	NSNumber *result = [self primitiveAnswer];
	return [result boolValue];
}

- (void)setPrimitiveAnswerValue:(BOOL)value_ {
	[self setPrimitiveAnswer:[NSNumber numberWithBool:value_]];
}

@dynamic cloudId;

- (int32_t)cloudIdValue {
	NSNumber *result = [self cloudId];
	return [result intValue];
}

- (void)setCloudIdValue:(int32_t)value_ {
	[self setCloudId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCloudIdValue {
	NSNumber *result = [self primitiveCloudId];
	return [result intValue];
}

- (void)setPrimitiveCloudIdValue:(int32_t)value_ {
	[self setPrimitiveCloudId:[NSNumber numberWithInt:value_]];
}

@dynamic fullText;

@dynamic measure;

@dynamic measureUnits;

@dynamic measureUnitsId;

- (int32_t)measureUnitsIdValue {
	NSNumber *result = [self measureUnitsId];
	return [result intValue];
}

- (void)setMeasureUnitsIdValue:(int32_t)value_ {
	[self setMeasureUnitsId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMeasureUnitsIdValue {
	NSNumber *result = [self primitiveMeasureUnitsId];
	return [result intValue];
}

- (void)setPrimitiveMeasureUnitsIdValue:(int32_t)value_ {
	[self setPrimitiveMeasureUnitsId:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic notes;

@dynamic productId;

- (int32_t)productIdValue {
	NSNumber *result = [self productId];
	return [result intValue];
}

- (void)setProductIdValue:(int32_t)value_ {
	[self setProductId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveProductIdValue {
	NSNumber *result = [self primitiveProductId];
	return [result intValue];
}

- (void)setPrimitiveProductIdValue:(int32_t)value_ {
	[self setPrimitiveProductId:[NSNumber numberWithInt:value_]];
}

@dynamic shoppingListQuantity;

- (int16_t)shoppingListQuantityValue {
	NSNumber *result = [self shoppingListQuantity];
	return [result shortValue];
}

- (void)setShoppingListQuantityValue:(int16_t)value_ {
	[self setShoppingListQuantity:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveShoppingListQuantityValue {
	NSNumber *result = [self primitiveShoppingListQuantity];
	return [result shortValue];
}

- (void)setPrimitiveShoppingListQuantityValue:(int16_t)value_ {
	[self setPrimitiveShoppingListQuantity:[NSNumber numberWithShort:value_]];
}

@dynamic shoppingListText;

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic status;

@dynamic mealPlannerShoppingListItems;

- (NSMutableSet*)mealPlannerShoppingListItemsSet {
	[self willAccessValueForKey:@"mealPlannerShoppingListItems"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mealPlannerShoppingListItems"];

	[self didAccessValueForKey:@"mealPlannerShoppingListItems"];
	return result;
}

@dynamic recipe;

@dynamic shoppingListCategory;

@end

