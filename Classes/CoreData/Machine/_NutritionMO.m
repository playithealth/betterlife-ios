// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to NutritionMO.m instead.

#import "_NutritionMO.h"

const struct NutritionMOAttributes NutritionMOAttributes = {
	.calciumPercent = @"calciumPercent",
	.calories = @"calories",
	.caloriesFromFat = @"caloriesFromFat",
	.cholesterol = @"cholesterol",
	.dietaryFiber = @"dietaryFiber",
	.insolubleFiber = @"insolubleFiber",
	.ironPercent = @"ironPercent",
	.monounsaturatedFat = @"monounsaturatedFat",
	.nutritionId = @"nutritionId",
	.otherCarbohydrates = @"otherCarbohydrates",
	.polyunsaturatedFat = @"polyunsaturatedFat",
	.potassium = @"potassium",
	.protein = @"protein",
	.saturatedFat = @"saturatedFat",
	.saturatedFatCalories = @"saturatedFatCalories",
	.sodium = @"sodium",
	.solubleFiber = @"solubleFiber",
	.sugars = @"sugars",
	.sugarsAlcohol = @"sugarsAlcohol",
	.totalCarbohydrates = @"totalCarbohydrates",
	.totalFat = @"totalFat",
	.transFat = @"transFat",
	.vitaminAPercent = @"vitaminAPercent",
	.vitaminCPercent = @"vitaminCPercent",
};

const struct NutritionMORelationships NutritionMORelationships = {
	.customFood = @"customFood",
	.foodLog = @"foodLog",
	.maxPhase = @"maxPhase",
	.maxUserProfile = @"maxUserProfile",
	.mealPlannerDay = @"mealPlannerDay",
	.mealPredictions = @"mealPredictions",
	.minPhase = @"minPhase",
	.minUserProfile = @"minUserProfile",
	.recipe = @"recipe",
};

@implementation NutritionMOID
@end

@implementation _NutritionMO

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Nutrition" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Nutrition";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Nutrition" inManagedObjectContext:moc_];
}

- (NutritionMOID*)objectID {
	return (NutritionMOID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"calciumPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"calciumPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"calories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesFromFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"caloriesFromFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cholesterolValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cholesterol"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"dietaryFiberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dietaryFiber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"insolubleFiberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"insolubleFiber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ironPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ironPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"monounsaturatedFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"monounsaturatedFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"nutritionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"nutritionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"otherCarbohydratesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"otherCarbohydrates"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"polyunsaturatedFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"polyunsaturatedFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"potassiumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"potassium"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"proteinValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"protein"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"saturatedFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"saturatedFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"saturatedFatCaloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"saturatedFatCalories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sodiumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sodium"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"solubleFiberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"solubleFiber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sugarsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sugars"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sugarsAlcoholValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sugarsAlcohol"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalCarbohydratesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalCarbohydrates"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"transFatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"transFat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"vitaminAPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"vitaminAPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"vitaminCPercentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"vitaminCPercent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic calciumPercent;

- (double)calciumPercentValue {
	NSNumber *result = [self calciumPercent];
	return [result doubleValue];
}

- (void)setCalciumPercentValue:(double)value_ {
	[self setCalciumPercent:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCalciumPercentValue {
	NSNumber *result = [self primitiveCalciumPercent];
	return [result doubleValue];
}

- (void)setPrimitiveCalciumPercentValue:(double)value_ {
	[self setPrimitiveCalciumPercent:[NSNumber numberWithDouble:value_]];
}

@dynamic calories;

- (double)caloriesValue {
	NSNumber *result = [self calories];
	return [result doubleValue];
}

- (void)setCaloriesValue:(double)value_ {
	[self setCalories:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCaloriesValue {
	NSNumber *result = [self primitiveCalories];
	return [result doubleValue];
}

- (void)setPrimitiveCaloriesValue:(double)value_ {
	[self setPrimitiveCalories:[NSNumber numberWithDouble:value_]];
}

@dynamic caloriesFromFat;

- (double)caloriesFromFatValue {
	NSNumber *result = [self caloriesFromFat];
	return [result doubleValue];
}

- (void)setCaloriesFromFatValue:(double)value_ {
	[self setCaloriesFromFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCaloriesFromFatValue {
	NSNumber *result = [self primitiveCaloriesFromFat];
	return [result doubleValue];
}

- (void)setPrimitiveCaloriesFromFatValue:(double)value_ {
	[self setPrimitiveCaloriesFromFat:[NSNumber numberWithDouble:value_]];
}

@dynamic cholesterol;

- (double)cholesterolValue {
	NSNumber *result = [self cholesterol];
	return [result doubleValue];
}

- (void)setCholesterolValue:(double)value_ {
	[self setCholesterol:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveCholesterolValue {
	NSNumber *result = [self primitiveCholesterol];
	return [result doubleValue];
}

- (void)setPrimitiveCholesterolValue:(double)value_ {
	[self setPrimitiveCholesterol:[NSNumber numberWithDouble:value_]];
}

@dynamic dietaryFiber;

- (double)dietaryFiberValue {
	NSNumber *result = [self dietaryFiber];
	return [result doubleValue];
}

- (void)setDietaryFiberValue:(double)value_ {
	[self setDietaryFiber:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveDietaryFiberValue {
	NSNumber *result = [self primitiveDietaryFiber];
	return [result doubleValue];
}

- (void)setPrimitiveDietaryFiberValue:(double)value_ {
	[self setPrimitiveDietaryFiber:[NSNumber numberWithDouble:value_]];
}

@dynamic insolubleFiber;

- (double)insolubleFiberValue {
	NSNumber *result = [self insolubleFiber];
	return [result doubleValue];
}

- (void)setInsolubleFiberValue:(double)value_ {
	[self setInsolubleFiber:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveInsolubleFiberValue {
	NSNumber *result = [self primitiveInsolubleFiber];
	return [result doubleValue];
}

- (void)setPrimitiveInsolubleFiberValue:(double)value_ {
	[self setPrimitiveInsolubleFiber:[NSNumber numberWithDouble:value_]];
}

@dynamic ironPercent;

- (double)ironPercentValue {
	NSNumber *result = [self ironPercent];
	return [result doubleValue];
}

- (void)setIronPercentValue:(double)value_ {
	[self setIronPercent:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveIronPercentValue {
	NSNumber *result = [self primitiveIronPercent];
	return [result doubleValue];
}

- (void)setPrimitiveIronPercentValue:(double)value_ {
	[self setPrimitiveIronPercent:[NSNumber numberWithDouble:value_]];
}

@dynamic monounsaturatedFat;

- (double)monounsaturatedFatValue {
	NSNumber *result = [self monounsaturatedFat];
	return [result doubleValue];
}

- (void)setMonounsaturatedFatValue:(double)value_ {
	[self setMonounsaturatedFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveMonounsaturatedFatValue {
	NSNumber *result = [self primitiveMonounsaturatedFat];
	return [result doubleValue];
}

- (void)setPrimitiveMonounsaturatedFatValue:(double)value_ {
	[self setPrimitiveMonounsaturatedFat:[NSNumber numberWithDouble:value_]];
}

@dynamic nutritionId;

- (int32_t)nutritionIdValue {
	NSNumber *result = [self nutritionId];
	return [result intValue];
}

- (void)setNutritionIdValue:(int32_t)value_ {
	[self setNutritionId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveNutritionIdValue {
	NSNumber *result = [self primitiveNutritionId];
	return [result intValue];
}

- (void)setPrimitiveNutritionIdValue:(int32_t)value_ {
	[self setPrimitiveNutritionId:[NSNumber numberWithInt:value_]];
}

@dynamic otherCarbohydrates;

- (double)otherCarbohydratesValue {
	NSNumber *result = [self otherCarbohydrates];
	return [result doubleValue];
}

- (void)setOtherCarbohydratesValue:(double)value_ {
	[self setOtherCarbohydrates:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveOtherCarbohydratesValue {
	NSNumber *result = [self primitiveOtherCarbohydrates];
	return [result doubleValue];
}

- (void)setPrimitiveOtherCarbohydratesValue:(double)value_ {
	[self setPrimitiveOtherCarbohydrates:[NSNumber numberWithDouble:value_]];
}

@dynamic polyunsaturatedFat;

- (double)polyunsaturatedFatValue {
	NSNumber *result = [self polyunsaturatedFat];
	return [result doubleValue];
}

- (void)setPolyunsaturatedFatValue:(double)value_ {
	[self setPolyunsaturatedFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitivePolyunsaturatedFatValue {
	NSNumber *result = [self primitivePolyunsaturatedFat];
	return [result doubleValue];
}

- (void)setPrimitivePolyunsaturatedFatValue:(double)value_ {
	[self setPrimitivePolyunsaturatedFat:[NSNumber numberWithDouble:value_]];
}

@dynamic potassium;

- (double)potassiumValue {
	NSNumber *result = [self potassium];
	return [result doubleValue];
}

- (void)setPotassiumValue:(double)value_ {
	[self setPotassium:[NSNumber numberWithDouble:value_]];
}

- (double)primitivePotassiumValue {
	NSNumber *result = [self primitivePotassium];
	return [result doubleValue];
}

- (void)setPrimitivePotassiumValue:(double)value_ {
	[self setPrimitivePotassium:[NSNumber numberWithDouble:value_]];
}

@dynamic protein;

- (double)proteinValue {
	NSNumber *result = [self protein];
	return [result doubleValue];
}

- (void)setProteinValue:(double)value_ {
	[self setProtein:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveProteinValue {
	NSNumber *result = [self primitiveProtein];
	return [result doubleValue];
}

- (void)setPrimitiveProteinValue:(double)value_ {
	[self setPrimitiveProtein:[NSNumber numberWithDouble:value_]];
}

@dynamic saturatedFat;

- (double)saturatedFatValue {
	NSNumber *result = [self saturatedFat];
	return [result doubleValue];
}

- (void)setSaturatedFatValue:(double)value_ {
	[self setSaturatedFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSaturatedFatValue {
	NSNumber *result = [self primitiveSaturatedFat];
	return [result doubleValue];
}

- (void)setPrimitiveSaturatedFatValue:(double)value_ {
	[self setPrimitiveSaturatedFat:[NSNumber numberWithDouble:value_]];
}

@dynamic saturatedFatCalories;

- (double)saturatedFatCaloriesValue {
	NSNumber *result = [self saturatedFatCalories];
	return [result doubleValue];
}

- (void)setSaturatedFatCaloriesValue:(double)value_ {
	[self setSaturatedFatCalories:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSaturatedFatCaloriesValue {
	NSNumber *result = [self primitiveSaturatedFatCalories];
	return [result doubleValue];
}

- (void)setPrimitiveSaturatedFatCaloriesValue:(double)value_ {
	[self setPrimitiveSaturatedFatCalories:[NSNumber numberWithDouble:value_]];
}

@dynamic sodium;

- (double)sodiumValue {
	NSNumber *result = [self sodium];
	return [result doubleValue];
}

- (void)setSodiumValue:(double)value_ {
	[self setSodium:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSodiumValue {
	NSNumber *result = [self primitiveSodium];
	return [result doubleValue];
}

- (void)setPrimitiveSodiumValue:(double)value_ {
	[self setPrimitiveSodium:[NSNumber numberWithDouble:value_]];
}

@dynamic solubleFiber;

- (double)solubleFiberValue {
	NSNumber *result = [self solubleFiber];
	return [result doubleValue];
}

- (void)setSolubleFiberValue:(double)value_ {
	[self setSolubleFiber:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSolubleFiberValue {
	NSNumber *result = [self primitiveSolubleFiber];
	return [result doubleValue];
}

- (void)setPrimitiveSolubleFiberValue:(double)value_ {
	[self setPrimitiveSolubleFiber:[NSNumber numberWithDouble:value_]];
}

@dynamic sugars;

- (double)sugarsValue {
	NSNumber *result = [self sugars];
	return [result doubleValue];
}

- (void)setSugarsValue:(double)value_ {
	[self setSugars:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSugarsValue {
	NSNumber *result = [self primitiveSugars];
	return [result doubleValue];
}

- (void)setPrimitiveSugarsValue:(double)value_ {
	[self setPrimitiveSugars:[NSNumber numberWithDouble:value_]];
}

@dynamic sugarsAlcohol;

- (double)sugarsAlcoholValue {
	NSNumber *result = [self sugarsAlcohol];
	return [result doubleValue];
}

- (void)setSugarsAlcoholValue:(double)value_ {
	[self setSugarsAlcohol:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveSugarsAlcoholValue {
	NSNumber *result = [self primitiveSugarsAlcohol];
	return [result doubleValue];
}

- (void)setPrimitiveSugarsAlcoholValue:(double)value_ {
	[self setPrimitiveSugarsAlcohol:[NSNumber numberWithDouble:value_]];
}

@dynamic totalCarbohydrates;

- (double)totalCarbohydratesValue {
	NSNumber *result = [self totalCarbohydrates];
	return [result doubleValue];
}

- (void)setTotalCarbohydratesValue:(double)value_ {
	[self setTotalCarbohydrates:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveTotalCarbohydratesValue {
	NSNumber *result = [self primitiveTotalCarbohydrates];
	return [result doubleValue];
}

- (void)setPrimitiveTotalCarbohydratesValue:(double)value_ {
	[self setPrimitiveTotalCarbohydrates:[NSNumber numberWithDouble:value_]];
}

@dynamic totalFat;

- (double)totalFatValue {
	NSNumber *result = [self totalFat];
	return [result doubleValue];
}

- (void)setTotalFatValue:(double)value_ {
	[self setTotalFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveTotalFatValue {
	NSNumber *result = [self primitiveTotalFat];
	return [result doubleValue];
}

- (void)setPrimitiveTotalFatValue:(double)value_ {
	[self setPrimitiveTotalFat:[NSNumber numberWithDouble:value_]];
}

@dynamic transFat;

- (double)transFatValue {
	NSNumber *result = [self transFat];
	return [result doubleValue];
}

- (void)setTransFatValue:(double)value_ {
	[self setTransFat:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveTransFatValue {
	NSNumber *result = [self primitiveTransFat];
	return [result doubleValue];
}

- (void)setPrimitiveTransFatValue:(double)value_ {
	[self setPrimitiveTransFat:[NSNumber numberWithDouble:value_]];
}

@dynamic vitaminAPercent;

- (double)vitaminAPercentValue {
	NSNumber *result = [self vitaminAPercent];
	return [result doubleValue];
}

- (void)setVitaminAPercentValue:(double)value_ {
	[self setVitaminAPercent:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveVitaminAPercentValue {
	NSNumber *result = [self primitiveVitaminAPercent];
	return [result doubleValue];
}

- (void)setPrimitiveVitaminAPercentValue:(double)value_ {
	[self setPrimitiveVitaminAPercent:[NSNumber numberWithDouble:value_]];
}

@dynamic vitaminCPercent;

- (double)vitaminCPercentValue {
	NSNumber *result = [self vitaminCPercent];
	return [result doubleValue];
}

- (void)setVitaminCPercentValue:(double)value_ {
	[self setVitaminCPercent:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveVitaminCPercentValue {
	NSNumber *result = [self primitiveVitaminCPercent];
	return [result doubleValue];
}

- (void)setPrimitiveVitaminCPercentValue:(double)value_ {
	[self setPrimitiveVitaminCPercent:[NSNumber numberWithDouble:value_]];
}

@dynamic customFood;

@dynamic foodLog;

@dynamic maxPhase;

@dynamic maxUserProfile;

@dynamic mealPlannerDay;

@dynamic mealPredictions;

@dynamic minPhase;

@dynamic minUserProfile;

@dynamic recipe;

@end

