//
//  UserStoreMO.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/13/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "_UserStoreMO.h"

extern NSString * const UserStoresDidChangeStoreCategories;
extern NSString * const UserStoresDidChangeStoreId;

@interface UserStoreMO : _UserStoreMO {}

@property (weak, readonly, nonatomic) NSNumber *cloud_id;
@property (weak, readonly, nonatomic) NSNumber *store_name;
@property (strong, nonatomic) NSNumber *distcalc;

+ (id)storeByEntityId:(NSNumber *)entityId inMOC:(NSManagedObjectContext *)moc;
- (void)setWithDictionary:(NSDictionary *)inStore;
- (NSData *)toJSON;
- (NSData *)toJSONWithEntityData;
- (NSData *)toJSONWithCategories;
- (void)populateStoreCategories;

@end
