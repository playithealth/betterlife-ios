#import "ActivitySetMO.h"

#import "NSDictionary+NumberValue.h"

@interface ActivitySetMO ()
@end

@implementation ActivitySetMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.cloudId = [update BC_numberForKey:kId];
	self.sortOrder = [update BC_numberForKey:kActivitySetSequenceNumber];
	self.activityType = [update BC_numberOrNilForKey:kActivitySetTypeId];
	self.calories = [update BC_numberForKey:kNutritionCalories];
	self.factor = [update BC_numberOrNilForKey:kActivityRecordFactor];
	self.heartZone = [update BC_numberOrNilForKey:kActivitySetZone];
	self.miles = [update BC_numberOrNilForKey:kActivitySetMiles];
	self.mph = [update BC_numberOrNilForKey:kActivitySetMPH];
	self.name = [update BC_stringOrNilForKey:kName];
	self.pounds = [update BC_numberOrNilForKey:kActivitySetPounds];
	self.reps = [update BC_numberOrNilForKey:kActivitySetReps];
	self.seconds = [update BC_numberOrNilForKey:kActivitySetSeconds];
	self.watts = [update BC_numberOrNilForKey:kActivitySetWatts];
}

#pragma mark - convenience methods
- (NSString *)secondsString {
	NSInteger seconds = self.secondsValue % 60;
	NSInteger minutes = (self.secondsValue / 60) % 60;
	NSInteger hours = (self.secondsValue / 3600);
	return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (void)copyFromActivitySet:(ActivitySetMO *)activitySet {
	self.cloudId = nil; // Copies need their own cloudId
	self.sortOrder = activitySet.sortOrder;
	self.activityType = activitySet.activityType;
	self.calories = activitySet.calories;
	self.factor = activitySet.factor;
	self.heartZone = activitySet.heartZone;
	self.miles = activitySet.miles;
	self.mph = activitySet.mph;
	self.name = activitySet.name;
	self.pounds = activitySet.pounds;
	self.reps = activitySet.reps;
	self.seconds = activitySet.seconds;
	self.watts = activitySet.watts;
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
#warning "Must provide toJSON implementation"
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
