#import "_ScannedItem.h"

@interface ScannedItem : _ScannedItem {}
+ (id)cacheScannedBarcode:(NSString *)barcode withName:(NSString *)name inMOC:(NSManagedObjectContext *)inMOC;
+ (id)cachedItemByBarcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC;
@end
