#import "ConversationMO.h"

#import "NSDictionary+NumberValue.h"
#import "User.h"

@interface ConversationMO ()

@end


@implementation ConversationMO
- (void)awakeFromFetch
{
	[super awakeFromFetch];

	[self setupFPSortDescriptors];
}

- (void)awakeFromInsert
{
	[super awakeFromInsert];

	[self setupFPSortDescriptors];
}

- (void)setupFPSortDescriptors
{
    if (![self isKindOfClass:[ConversationMO class]]) {
		return;
	}
    NSEntityDescription *entityDescription = [ConversationMO entityInManagedObjectContext:self.managedObjectContext];
    NSFetchedPropertyDescription *fetchedPropertyDescription = [entityDescription propertiesByName][ConversationMOFetchedProperties.newestMessage];
    NSFetchRequest *fetchRequest = [fetchedPropertyDescription fetchRequest];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"message.createdOn" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
}

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.loginId = [[User currentUser] loginId];

	self.conversationUserId = [update BC_numberForKey:kUserId];
	self.conversationUserGender = [update BC_numberForKey:kUserProfileGender];
	if ([update objectForKey:kMessageUserName] != (id)[NSNull null]) {
		self.conversationUserName = [update valueForKey:kMessageUserName];
	}
	else {
		if ([self.conversationUserId integerValue] == 0) {
			self.conversationUserName = @"BettrLife";
		}
		else {
			// Unsure if this can happen
			self.conversationUserName = @"No user name";
		}
	}
	self.conversationUserImageId = [update BC_numberOrNilForKey:kImageId];
}


@end
