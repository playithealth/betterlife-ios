#import "ActivityLogSetMO.h"

#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"

@interface ActivityLogSetMO ()
@end

@implementation ActivityLogSetMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
    [super setWithUpdateDictionary:update];
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
