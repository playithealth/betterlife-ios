// 
//  InboxMessage.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/13/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "InboxMessage.h"

#import "NSDictionary+NumberValue.h"


@implementation InboxMessage

- (NSData *)toJSON 
{
	// QuietLog(@"%s", __FUNCTION__);
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] 
		initWithObjectsAndKeys:
			self.deliveryId, kMessageDeliveryId,
			nil];

	if ([self.status isEqualToString:kStatusPost]) {
		[jsonDict setValue:self.subject forKey:kMessageSubject];
		[jsonDict setValue:self.text forKey:kMessageText];
		[jsonDict setValue:self.isPrivate forKey:kMessageIsPrivate];
		[jsonDict setValue:[NSArray arrayWithObject:self.replyTo.senderId] 
			forKey:kMessageDeliveryList];
		[jsonDict setValue:self.replyTo.cloudId forKey:kMessageReplyId];
	}
	else {
		// deleted
		if ([self.isRemoved boolValue]) {
			[jsonDict setValue:kMarkDeleted forKey:kAction];
		}
		// read / unread
		else if ([self.isRead boolValue]) {
			[jsonDict setValue:kMarkRead forKey:kAction];
		}
		else {
			[jsonDict setValue:kMarkUnread forKey:kAction];
		}
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.accountId = [update BC_numberForKey:kMessageToAccountId];
	self.loginId = [update BC_numberForKey:kMessageToLoginId];
	self.cloudId = [update BC_numberForKey:kId];
	self.deliveryId = [update BC_numberForKey:kMessageDeliveryId];
	self.subject = [update valueForKey:kMessageSubject];
	self.summary = [update valueForKey:kMessageSummary];
	self.createdOn = [update BC_numberForKey:kCreatedOn];
	self.updatedOn = [update BC_numberForKey:kUpdatedOn];

	self.senderId = [update BC_numberForKey:kMessageSenderId];
	id updateValue = [update objectForKey:kMessageUserName];
	if (updateValue && (updateValue != [NSNull null])) {
		self.sender = updateValue;
	}
	else {
		self.sender = @"BettrLife";
	}

	self.isPrivate = [update BC_numberForKey:kMessageIsPrivate];
	self.isRead = [update BC_numberForKey:kMessageIsRead];
	self.isRemoved = [update BC_numberForKey:kMessageReceiverDeleted];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

@end
