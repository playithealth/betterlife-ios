#import "_RecipeTagMO.h"

@interface RecipeTagMO : _RecipeTagMO {}
+ (id)tagByName:(NSString *)name withManagedObjectContext:(NSManagedObjectContext *)inMOC;
@end
