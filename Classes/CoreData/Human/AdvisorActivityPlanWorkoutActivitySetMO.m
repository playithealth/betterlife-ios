#import "AdvisorActivityPlanWorkoutActivitySetMO.h"

#import "NSDictionary+NumberValue.h"

@interface AdvisorActivityPlanWorkoutActivitySetMO ()

// Private interface goes here.

@end

@implementation AdvisorActivityPlanWorkoutActivitySetMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.name = [update BC_stringOrNilForKey:kName];
	self.activityType = [update BC_numberOrNilForKey:kAdvisorActivityPlanWorkoutActivitySetTypeId];
	self.calories = [update BC_numberOrNilForKey:kNutritionCalories];
	self.factor = [update BC_numberOrNilForKey:kActivityRecordFactor];
	self.heartZone = [update BC_numberOrNilForKey:kAdvisorActivityPlanWorkoutActivitySetZone];
	self.miles = [update BC_numberOrNilForKey:kAdvisorActivityPlanWorkoutActivitySetMiles];
	self.mph = [update BC_numberOrNilForKey:kAdvisorActivityPlanWorkoutActivitySetMPH];
	self.pounds = [update BC_numberOrNilForKey:kAdvisorActivityPlanWorkoutActivitySetPounds];
	self.reps = [update BC_numberOrNilForKey:kStrengthLogSetReps];
	self.seconds = [update BC_numberOrNilForKey:kAdvisorActivityPlanWorkoutActivitySetSeconds];
	self.sortOrder = [update BC_numberOrNilForKey:kAdvisorActivityPlanWorkoutActivitySetSequenceNumber];
	self.watts = [update BC_numberOrNilForKey:kAdvisorActivityPlanWorkoutActivitySetWatts];
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
