#import "_ActivityLogSetMO.h"

#import "BLSyncEntity.h"

@interface ActivityLogSetMO : _ActivityLogSetMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
@end
