#import "ActivityMO.h"

#import "ActivitySetMO.h"
#import "AdvisorActivityPlanWorkoutMO.h"
#import "NSDictionary+NumberValue.h"

@interface ActivityMO ()
@end

@implementation ActivityMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.activityName = [update BC_stringOrNilForKey:kName];
	self.activityDescription = [update BC_stringOrNilForKey:kDescription];
	self.basedOnId = [update BC_numberOrNilForKey:kActivityBasedOnId];
	self.factor = [update BC_numberForKey:kActivityForceFactor];
	self.isTimedActivity = [update BC_numberForKey:kActivityIsTimedActivity];
	self.link = [update BC_stringOrNilForKey:kActivityLink];
	self.notes = [update BC_stringOrNilForKey:kActivityNotes];
	self.routineId = [update BC_numberOrNilForKey:kActivityRoutineId];
	self.source = [update BC_numberOrNilForKey:kSource];

	NSArray *inSets = [update objectForKey:kActivitySets];

	// delete any sets that didn't come with the update
	[self.sets enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
		NSUInteger idx = [inSets indexOfObjectPassingTest:^(id searchObj, NSUInteger idx, BOOL *stop) {
			return [[searchObj BC_numberOrNilForKey:kId] isEqualToNumber:[obj cloudId]];
		}];
		if (idx == NSNotFound) {
			[self.managedObjectContext deleteObject:obj];
		}
	}];

	for (NSDictionary *setDict in inSets) {
		NSNumber *cloudId = [setDict BC_numberForKey:kId];
		ActivitySetMO *activitySetMO = [ActivitySetMO
			MR_findFirstByAttribute:ActivitySetMOAttributes.cloudId withValue:cloudId
			inContext:self.managedObjectContext];

		if (!activitySetMO) {
			activitySetMO = [ActivitySetMO insertInManagedObjectContext:self.managedObjectContext];
			activitySetMO.cloudId = cloudId;
			//activitySetMO.loginId = thisUser.loginId;
			
			activitySetMO.activity = self;
		}

		[activitySetMO setWithUpdateDictionary:setDict];
	}

	// Workout/Entry
	NSNumber *workoutId = [update BC_numberOrNilForKey:kActivityRecordWorkoutId];
	if (workoutId) {
		self.workout = [AdvisorActivityPlanWorkoutMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@",
			AdvisorActivityPlanWorkoutMOAttributes.loginId, self.loginId, AdvisorActivityPlanWorkoutMOAttributes.cloudId, workoutId]
			inContext:self.managedObjectContext];
	}

	// TODO childActivities

	// TODO sets

}

- (void)copyFromActivity:(ActivityMO *)activity includeSets:(BOOL)includeSets {
	self.cloudId = nil; // Copies need their own cloudId
	self.loginId = activity.loginId;
	self.activityName = activity.activityName;
	self.activityDescription = activity.activityDescription;
	self.basedOnId = activity.basedOnId; // TODO Should this be a direct copy, or should it now point to the thing we're copying from?
	self.factor = activity.factor;
	self.isTimedActivity = activity.isTimedActivity;
	self.link = activity.link;
	self.notes = activity.notes;
	self.routineId = activity.routineId;
	self.source = activity.source;
	self.sortOrder = activity.sortOrder;

	self.basedOnActivity = activity;

	// Do we want to copy the sets?
	if (includeSets) {
		for (ActivitySetMO *set in activity.sets) {
			// Copy the set, then relate it to this activity
			ActivitySetMO *newSetMO = [ActivitySetMO insertInManagedObjectContext:self.managedObjectContext];
			[newSetMO copyFromActivitySet:set];
			newSetMO.activity = self;
		}
	}

	// Workout/Entry
	//self.workout = activity.workout; // TODO Is this correct? Need to verify
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
		<#kConstName#> : self.activityDescription,
		<#kConstName#> : self.activityName,
		<#kConstName#> : self.basedOnId,
		<#kConstName#> : self.cloudId,
		<#kConstName#> : self.factor,
		<#kConstName#> : self.isTimedActivity,
		<#kConstName#> : self.link,
		<#kConstName#> : self.notes,
		<#kConstName#> : self.routineId,
		<#kConstName#> : self.sortOrder,
		<#kConstName#> : self.source,
	};

	// TODO childActivities

	// TODO sets

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
