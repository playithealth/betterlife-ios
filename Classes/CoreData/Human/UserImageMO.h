#import "_UserImageMO.h"

typedef enum {
    kUserImageTypeUserProfile = 0,
    kUserImageTypeRecipe = 1,
    kUserImageTypeProduct = 2,
} ImageType;

@interface UserImageMO : _UserImageMO {}
- (NSData *)toJSON;
- (void)markForSync;
- (void)setUserImageWithUIImage:(UIImage *)userImage;
@end
