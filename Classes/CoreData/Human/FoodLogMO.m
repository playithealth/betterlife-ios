//
//  FoodLogMO.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/16/2011.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "FoodLogMO.h"

#import "AdvisorMealPlanEntryMO.h"
#import "AdvisorMealPlanTagMO.h"
#import "BCUrlFactory.h"
#import "CustomFoodMO.h"
#import "MealPlannerDay.h"
#import "MealPredictionMO.h"
#import "RecipeMO.h"
#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "User.h"

@implementation FoodLogMO

#pragma mark - initialization
- (void)awakeFromInsert
{
    [super awakeFromInsert];
 
	[self setCreationDate:[NSDate date]];
}

+ (NSString *)syncName {
	return kSyncFoodLog;
}

+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict {
	return [updateDict BC_numberForKey:kId];
}

+ (NSString *)statusString {
	return kSyncUpdateHumanReadable_FoodLog;
}

+ (NSString *)updatesSubkey {
	return kFoodLogs;
}

+ (NSURL *)urlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit {
	return [BCUrlFactory foodLogURLNewerThan:timestamp withOffset:offset andLimit:limit];
}

+ (NSPredicate *)userPredicate {
	return [self BL_loginIdPredicate];
}

#pragma mark - set with...
+ (void)setWithUpdatesArray:(NSArray *)updates forLogin:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)inMOC {
	[self BL_syncUpdateForLogin:loginId withUpdates:updates inMOC:inMOC];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.cloudId = [update BC_numberForKey:kId];
	self.loginId = [update BC_numberForKey:kUserId];

	if ((NSNull *)[update objectForKey:kFoodLogItemType] != [NSNull null]) {
		self.itemType = [update valueForKey:kFoodLogItemType];
	}

	if ((NSNull *)[update objectForKey:kRecipeId] != [NSNull null]) {
		self.recipe = [RecipeMO recipeById:[update BC_numberOrNilForKey:kRecipeId] withManagedObjectContext:self.managedObjectContext];
	}
	if ((NSNull *)[update objectForKey:kFoodLogMenuId] != [NSNull null]) {
		self.menuId = [update BC_numberForKey:kFoodLogMenuId];
	}
	if ((NSNull *)[update objectForKey:kProductId] != [NSNull null]) {
		self.productId = [update BC_numberForKey:kProductId];
		self.customFood = [CustomFoodMO MR_findFirstByAttribute:CustomFoodMOAttributes.cloudId
			withValue:self.productId inContext:self.managedObjectContext];
	}
	if ([update objectForKey:kMPTagID] != (id)[NSNull null]) {
		self.tagId = [update BC_numberOrNilForKey:kMPTagID];
		self.tag = [AdvisorMealPlanTagMO MR_findFirstByAttribute:AdvisorMealPlanTagMOAttributes.cloudId withValue:self.tagId
			inContext:self.managedObjectContext];
	}

	self.imageId = [update BC_numberOrNilForKey:kFoodLogImageId];
	self.date = [NSDate dateWithTimeIntervalSince1970:[[update valueForKey:kFoodLogDay] integerValue]];
	self.logTime = [update BC_numberForKey:kFoodLogLogTime];
	self.servings = [update BC_numberForKey:kFoodLogMultiplier];
	self.name = [update valueForKey:kName];
	self.updatedOn = [update BC_numberForKey:kUpdatedOn];

	id servingSize = [update objectForKey:kFoodLogServingSize];
	if ([servingSize isKindOfClass:[NSString class]]) {
		self.servingSize = servingSize;
	}

	id servingsUnit = [update objectForKey:kFoodLogServingsUnits];
	if ([servingsUnit isKindOfClass:[NSString class]]) {
		self.servingsUnit = servingsUnit;
	}
	self.nutritionFactor = [update BC_numberForKey:kFoodLogNutritionFactor];

	if ([update objectForKey:kSource]) {
        self.source = [update BC_numberOrNilForKey:kSource];
	}

	// nutritional data
	self.nutritionId = [update BC_numberOrNilForKey:kNutritionId];
	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}
	[self.nutrition setWithUpdateDictionary:update];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)setWithProductDictionary:(NSDictionary *)product
{
	self.servings = @1;
	self.nutritionFactor = @1;
	self.name = [product objectForKey:@"name"];

	self.itemType = @"product";
	self.productId = [product BC_numberOrNilForKey:kId];
	self.imageId = [product BC_numberOrNilForKey:kFoodLogImageId];

	id servingSize = [product objectForKey:@"serving_size_text"];
	if ([servingSize isKindOfClass:[NSString class]]) {
		self.servingSize = servingSize;
	}

	id servingsUnit = [product objectForKey:@"serving_size_uom"];
	if ([servingsUnit isKindOfClass:[NSString class]]) {
		self.servingsUnit = servingsUnit;
	}

	// nutritional data
	self.nutritionId = [product BC_numberOrNilForKey:kNutritionId];
	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}
	[self.nutrition setWithUpdateDictionary:product];
}

- (void)setWithPrediction:(MealPredictionMO *)prediction
{
	self.loginId = [[User currentUser] loginId];
	self.servings = @1;
	self.nutritionFactor = @1;
	self.name = prediction.name;
	self.servings = prediction.servings;
	self.imageId = prediction.image_id;

	self.itemType = prediction.item_type;
	if (prediction.customFood) {
		self.customFood = (id)[self.managedObjectContext existingObjectWithID:[prediction.customFood objectID] error:nil];
	}
	if (prediction.recipe) {
		self.recipe = (id)[self.managedObjectContext existingObjectWithID:[prediction.recipe objectID] error:nil];
	}
	if (!self.recipe && [prediction.item_type isEqualToString:@"recipe"]) {
		self.recipe = [RecipeMO recipeById:prediction.item_id withManagedObjectContext:self.managedObjectContext];
	}
	else if ([prediction.item_type isEqualToString:@"product"]) {
		self.productId = prediction.item_id;
	}
	else if ([prediction.item_type isEqualToString:@"menu"]) {
		self.menuId = prediction.item_id;
	}

	// nutritional data
	if (prediction.nutrition) {
		if (!self.nutrition) {
			self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
		}
		[self.nutrition copyFromNutritionMO:prediction.nutrition];
	}
}

// This method is used after a barcode scan was done, and thus essentially only adds products to the food log, however...
// FIXME: it isn't guaranteed to fill out the imageId attribute!!!
- (void)setWithManagedObject:(id)managedObject
{
	self.loginId = [[User currentUser] loginId];
	self.servings = @1;
	self.nutritionFactor = @1;
	self.name = [managedObject name];
	self.itemType = kItemTypeProduct;
	if ([managedObject respondsToSelector:NSSelectorFromString(@"imageId")]) {
		self.imageId = [managedObject imageId];
	}
	if ([managedObject productId]) {
		self.productId = [managedObject productId];
	}

    if ([managedObject respondsToSelector:@selector(nutritionId)]) {
        self.nutritionId = [managedObject nutritionId];
    }
    if ([managedObject respondsToSelector:@selector(nutrition)] && [managedObject nutrition]) {
		[self setNutritionFromMO:[managedObject nutrition]];
	}
	else {
		// Backwards compatibility for entities that have not yet been refactored to carry a relationship to a nutrition MO, but
		// instead have the nutrients within their MO
		[self setNutritionFromMO:managedObject];
	}
}

#pragma mark - set nutrition...
- (void)setNutritionWithDictionary:(NSDictionary *)nutrition
{
	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}

	self.nutrition.calories = [nutrition BC_numberOrNilForKey:kNutritionCalories];
	self.nutrition.caloriesFromFat = [nutrition BC_numberOrNilForKey:kNutritionCaloriesFromFat];
	self.nutrition.totalFat = [nutrition BC_numberOrNilForKey:kNutritionTotalFat];
	self.nutrition.transFat = [nutrition BC_numberOrNilForKey:kNutritionTransFat];
	self.nutrition.saturatedFat = [nutrition BC_numberOrNilForKey:kNutritionSaturatedFat];
	self.nutrition.saturatedFatCalories = [nutrition BC_numberOrNilForKey:kNutritionSaturatedFatCalories];
	self.nutrition.polyunsaturatedFat = [nutrition BC_numberOrNilForKey:kNutritionPolyunsaturatedFat];
	self.nutrition.monounsaturatedFat = [nutrition BC_numberOrNilForKey:kNutritionMonounsaturatedFat];
	self.nutrition.cholesterol = [nutrition BC_numberOrNilForKey:kNutritionCholesterol];
	self.nutrition.sodium = [nutrition BC_numberOrNilForKey:kNutritionSodium];
	self.nutrition.potassium = [nutrition BC_numberOrNilForKey:kNutritionPotassium];
	self.nutrition.totalCarbohydrates = [nutrition BC_numberOrNilForKey:kNutritionTotalCarbohydrates];
	self.nutrition.otherCarbohydrates = [nutrition BC_numberOrNilForKey:kNutritionOtherCarbohydrates];
	self.nutrition.dietaryFiber = [nutrition BC_numberOrNilForKey:kNutritionDietaryFiber];
	self.nutrition.solubleFiber = [nutrition BC_numberOrNilForKey:kNutritionSolubleFiber];
	self.nutrition.insolubleFiber = [nutrition BC_numberOrNilForKey:kNutritionInsolubleFiber];
	self.nutrition.sugars = [nutrition BC_numberOrNilForKey:kNutritionSugars];
	self.nutrition.sugarsAlcohol = [nutrition BC_numberOrNilForKey:kNutritionSugarsAlcohol];
	self.nutrition.protein = [nutrition BC_numberOrNilForKey:kNutritionProtein];
	self.nutrition.vitaminAPercent = [nutrition BC_numberOrNilForKey:kNutritionVitaminAPercent];
	self.nutrition.vitaminCPercent = [nutrition BC_numberOrNilForKey:kNutritionVitaminCPercent];
	self.nutrition.calciumPercent = [nutrition BC_numberOrNilForKey:kNutritionCalciumPercent];
	self.nutrition.ironPercent = [nutrition BC_numberOrNilForKey:kNutritionIronPercent];
}

- (void)setNutritionFromMO:(id)nutritionSource
{
	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}

	if ([nutritionSource respondsToSelector:@selector(nutrition)]) {
		 [self.nutrition copyFromNutritionMO:[nutritionSource nutrition]];
	}
	else {
		if ([nutritionSource respondsToSelector:@selector(calories)]) {
			self.nutrition.calories = [nutritionSource calories];
		}
		if ([nutritionSource respondsToSelector:@selector(caloriesFromFat)]) {
			self.nutrition.caloriesFromFat = [nutritionSource caloriesFromFat];
		}
		if ([nutritionSource respondsToSelector:@selector(totalFat)]) {
			self.nutrition.totalFat = [nutritionSource totalFat];
		}
		if ([nutritionSource respondsToSelector:@selector(transFat)]) {
			self.nutrition.transFat = [nutritionSource transFat];
		}
		if ([nutritionSource respondsToSelector:@selector(saturatedFat)]) {
			self.nutrition.saturatedFat = [nutritionSource saturatedFat];
		}
		if ([nutritionSource respondsToSelector:@selector(saturatedFatCalories)]) {
			self.nutrition.saturatedFatCalories = [nutritionSource saturatedFatCalories];
		}
		if ([nutritionSource respondsToSelector:@selector(polyunsaturatedFat)]) {
			self.nutrition.polyunsaturatedFat = [nutritionSource polyunsaturatedFat];
		}
		if ([nutritionSource respondsToSelector:@selector(monounsaturatedFat)]) {
			self.nutrition.monounsaturatedFat = [nutritionSource monounsaturatedFat];
		}
		if ([nutritionSource respondsToSelector:@selector(cholesterol)]) {
			self.nutrition.cholesterol = [nutritionSource cholesterol];
		}
		if ([nutritionSource respondsToSelector:@selector(sodium)]) {
			self.nutrition.sodium = [nutritionSource sodium];
		}
		if ([nutritionSource respondsToSelector:@selector(potassium)]) {
			self.nutrition.potassium = [nutritionSource potassium];
		}
		if ([nutritionSource respondsToSelector:@selector(totalCarbohydrates)]) {
			self.nutrition.totalCarbohydrates = [nutritionSource totalCarbohydrates];
		}
		if ([nutritionSource respondsToSelector:@selector(otherCarbohydrates)]) {
			self.nutrition.otherCarbohydrates = [nutritionSource otherCarbohydrates];
		}
		if ([nutritionSource respondsToSelector:@selector(dietaryFiber)]) {
			self.nutrition.dietaryFiber = [nutritionSource dietaryFiber];
		}
		if ([nutritionSource respondsToSelector:@selector(solubleFiber)]) {
			self.nutrition.solubleFiber = [nutritionSource solubleFiber];
		}
		if ([nutritionSource respondsToSelector:@selector(insolubleFiber)]) {
			self.nutrition.insolubleFiber = [nutritionSource insolubleFiber];
		}
		if ([nutritionSource respondsToSelector:@selector(sugars)]) {
			self.nutrition.sugars = [nutritionSource sugars];
		}
		if ([nutritionSource respondsToSelector:@selector(sugarsAlcohol)]) {
			self.nutrition.sugarsAlcohol = [nutritionSource sugarsAlcohol];
		}
		if ([nutritionSource respondsToSelector:@selector(protein)]) {
			self.nutrition.protein = [nutritionSource protein];
		}
		if ([nutritionSource respondsToSelector:@selector(vitaminAPercent)]) {
			self.nutrition.vitaminAPercent = [nutritionSource vitaminAPercent];
		}
		if ([nutritionSource respondsToSelector:@selector(vitaminCPercent)]) {
			self.nutrition.vitaminCPercent = [nutritionSource vitaminCPercent];
		}
		if ([nutritionSource respondsToSelector:@selector(calciumPercent)]) {
			self.nutrition.calciumPercent = [nutritionSource calciumPercent];
		}
		if ([nutritionSource respondsToSelector:@selector(ironPercent)]) {
			self.nutrition.ironPercent = [nutritionSource ironPercent];
		}
	}
}

+ (id)insertFoodLogWithFoodLog:(FoodLogMO *)foodLog inContext:(NSManagedObjectContext *)moc
{
	FoodLogMO *newFoodLog = [FoodLogMO insertInManagedObjectContext:moc];
	newFoodLog.loginId = [[User currentUser] loginId];
	newFoodLog.servings = foodLog.servings;
	newFoodLog.nutritionFactor = foodLog.nutritionFactor;
	newFoodLog.name = foodLog.name;
	newFoodLog.itemType = foodLog.itemType;
	newFoodLog.imageId = foodLog.imageId;
	newFoodLog.productId = foodLog.productId;
	newFoodLog.menuId = foodLog.menuId;
	newFoodLog.recipe = foodLog.recipe;
	newFoodLog.servingSize = foodLog.servingSize;
	newFoodLog.servingsUnit = foodLog.servingsUnit;

	newFoodLog.nutritionId = @0;
	newFoodLog.nutrition = [NutritionMO insertInManagedObjectContext:moc];
	[newFoodLog.nutrition copyFromNutritionMO:foodLog.nutrition];

	[newFoodLog markForSync];;

	// TODO save to storage internal to this data model?

	return newFoodLog;
}

+ (id)insertFoodLogWithPrediction:(MealPredictionMO *)prediction inContext:(NSManagedObjectContext *)moc
{
	FoodLogMO *newFoodLog = [FoodLogMO insertInManagedObjectContext:moc];
	[newFoodLog setWithPrediction:prediction];

	[newFoodLog markForSync];

	return newFoodLog;
}

+ (id)insertFoodLogWithMealPlanner:(MealPlannerDay *)mealPlanner inContext:(NSManagedObjectContext *)moc
{
	// the selection is a choice from the PlanningTagSearchViewController
	// and represents a "tag" for an "event" in the health coach meal plan
	FoodLogMO *newFoodLog = [FoodLogMO insertInManagedObjectContext:moc];
	newFoodLog.loginId = [[User currentUser] loginId];
	newFoodLog.date = mealPlanner.date;
	if (mealPlanner.tag) {
		newFoodLog.tag = (id)[moc existingObjectWithID:[mealPlanner.tag objectID] error:nil];
	}
	newFoodLog.tagId = newFoodLog.tag.cloudId;
	newFoodLog.logTime = mealPlanner.logTime;
	newFoodLog.name = mealPlanner.name;
	newFoodLog.servings = mealPlanner.servings;
	newFoodLog.servingsUnit = @"servings"; ///< MealPlanner doesn't carry servings unit, because it is implied as servings
	if (mealPlanner.servingSize && mealPlanner.servingSizeUnit) {
		newFoodLog.servingSize = [NSString stringWithFormat:@"%@ %@", mealPlanner.servingSize, mealPlanner.servingSizeUnit];
	}
	else {
		// Not sure this is correct, but default to 1 serving for this
		newFoodLog.servingSize = @"1 serving";
	}
	newFoodLog.nutritionFactor = @1; // Always 1, because meal planners only use servings for the measurement
	newFoodLog.itemType = mealPlanner.itemType;
	newFoodLog.imageId = mealPlanner.imageId;
	if (mealPlanner.recipe) {
		newFoodLog.recipe = (id)[moc existingObjectWithID:[mealPlanner.recipe objectID] error:nil];

		if (!newFoodLog.recipe) {
			// This could happen if this mealPlanner is in a different MOC, and created the recipe there, thus the new recipe won't
			// be in the foodLog MOC, and needs to be 'copied' to the foodLog MOC
			newFoodLog.recipe = [RecipeMO insertInManagedObjectContext:moc];
			[newFoodLog.recipe setWithRecipeMO:mealPlanner.recipe];
		}
	}
	newFoodLog.productId = mealPlanner.productId;
	newFoodLog.menuId = mealPlanner.menuId;

	// Copy the nutrition
	newFoodLog.nutrition = [NutritionMO insertInManagedObjectContext:moc];
	[newFoodLog.nutrition copyFromNutritionMO:mealPlanner.nutrition];

	[newFoodLog markForSync];
	
	return newFoodLog;
}

// NOTE: Currently unused
+ (id)insertFoodLogWithTag:(AdvisorMealPlanTagMO *)tag forDate:(NSDate *)date withAdvisorMealPlanTagDictionary:(NSDictionary *)tagDict
	inContext:(NSManagedObjectContext *)moc
{
	// the selection is a choice from the PlanningTagSearchViewController
	// and represents a "tag" for an "event" in the health coach meal plan
	FoodLogMO *newFoodLog = [FoodLogMO insertInManagedObjectContext:moc];
	newFoodLog.loginId = [[User currentUser] loginId];
	newFoodLog.date = date;
	newFoodLog.tag = tag;
	newFoodLog.tagId = newFoodLog.tag.cloudId;
	newFoodLog.logTime = tag.entry.logTime;
	newFoodLog.name = tagDict[kName];
	newFoodLog.servings = tagDict[kMPTagServings];
	newFoodLog.servingsUnit = (tagDict[kMPTagServingsUnit] ?: @"servings");
	if ([tagDict BC_numberOrNilForKey:kMPTagServingSize]) {
		newFoodLog.servingSize = [NSString stringWithFormat:@"%@ %@", tagDict[kMPTagServingSize], tagDict[kMPTagServingSizeUOM]];
	}
	else {
		// Not sure this is correct, but default to 1 serving for this
		newFoodLog.servingSize = @"1 serving";
	}
	newFoodLog.nutritionFactor = @1; // Always 1, because tags only give back servings for the measurement
	newFoodLog.itemType = tagDict[kMPTagItemType];
	newFoodLog.imageId = [tagDict BC_numberOrNilForKey:kImageId];
	newFoodLog.nutrition = [NutritionMO insertInManagedObjectContext:moc];
	[newFoodLog.nutrition setWithUpdateDictionary:tagDict[kMPNutrients]];

	if ([newFoodLog.itemType isEqual:kItemTypeRecipe]) {
		newFoodLog.recipe = [RecipeMO recipeById:[tagDict[kMPExternalId] numberValueDecimal] withManagedObjectContext:moc];
		if (!newFoodLog.recipe) {
			newFoodLog.recipe = [RecipeMO insertInManagedObjectContext:moc];
			newFoodLog.recipe.accountId = [[User currentUser] accountId];
			[newFoodLog.recipe setWithRecipeDictionary:tagDict];
		}
	}
	else if ([newFoodLog.itemType isEqual:kItemTypeProduct]) {
		newFoodLog.productId = [tagDict[kMPExternalId] numberValueDecimal];
	}
	else if ([newFoodLog.itemType isEqual:kItemTypeMenu]) {
		newFoodLog.menuId = [tagDict[kMPExternalId] numberValueDecimal];
	}

	[newFoodLog markForSync];
	
	return newFoodLog;
}

#pragma mark - convenience methods
// shallow copy 
- (id)copyWithZone:(NSZone *)zone
{
	NSManagedObjectContext *context = [self managedObjectContext];
	id copied = [[[self class] allocWithZone: zone] initWithEntity:[self entity] insertIntoManagedObjectContext:context];

	for (NSString *key in [[[self entity] attributesByName] allKeys]) {
		[copied setValue:[self valueForKey:key] forKey:key];
	}

	for (NSString *key in [[[self entity] relationshipsByName] allKeys]) {
		[copied setValue:[self valueForKey:key] forKey:key];
	}
	return copied;
}

- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

+ (NSNumber *)guessMealTime
{
	// try to speculate a time for a food log
	NSNumber *mealTime = nil;

	// if today before 11 AM use breakfast
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [calendar components:(NSHourCalendarUnit) fromDate:[NSDate date]];
	if (components.hour < 11) {
		mealTime = @0;
	}
	// before 5 PM use lunch
	else if (components.hour < 17) {
		mealTime = @2;
	}
	// after that, use dinner
	else {
		mealTime = @4;
	}

	return mealTime;
}

+ (NSArray *)findLogsForLogTime:(NSNumber *)logTime after:(NSDate *)after before:(NSDate *)before inContext:(NSManagedObjectContext *)moc
{
	NSFetchRequest *request = [FoodLogMO MR_createFetchRequestInContext:moc];
	[request setFetchBatchSize:20];

	User *thisUser = [User currentUser];

	request.predicate = [NSPredicate predicateWithFormat:@"%K BETWEEN {%@, %@} AND %K = %@ AND %K = %@ AND %K <> %@",
		FoodLogMOAttributes.date, after, before,
		FoodLogMOAttributes.loginId, thisUser.loginId,
		FoodLogMOAttributes.logTime, logTime,
		FoodLogMOAttributes.status, kStatusDelete];
	
    // sort by calories descending
	// NOTE put something more meaningful here? logged order?
    NSSortDescriptor *sortByCreationDate = [NSSortDescriptor sortDescriptorWithKey:FoodLogMOAttributes.creationDate ascending:YES];
    NSSortDescriptor *sortByCalories = [NSSortDescriptor sortDescriptorWithKey:@"nutrition.calories" ascending:NO];
    
    [request setSortDescriptors:@[sortByCreationDate, sortByCalories]];

	return [moc executeFetchRequest:request error:nil];
}

+ (NSArray *)findLogsAfter:(NSDate *)after before:(NSDate *)before inContext:(NSManagedObjectContext *)moc
{
	NSFetchRequest *request = [FoodLogMO MR_createFetchRequestInContext:moc];
	[request setFetchBatchSize:20];

	User *thisUser = [User currentUser];

	request.predicate = [NSPredicate predicateWithFormat:@"%K BETWEEN {%@, %@} AND %K = %@ AND %K <> %@",
		FoodLogMOAttributes.date, after, before,
		FoodLogMOAttributes.loginId, thisUser.loginId,
		FoodLogMOAttributes.status, kStatusDelete];
	
    // sort by creation date and calories descending
    NSSortDescriptor *sortByCreationDate = [NSSortDescriptor sortDescriptorWithKey:FoodLogMOAttributes.creationDate ascending:YES];
    NSSortDescriptor *sortByCalories = [NSSortDescriptor sortDescriptorWithKey:@"nutrition.calories" ascending:NO];
    
    [request setSortDescriptors:@[sortByCreationDate, sortByCalories]];

	return [moc executeFetchRequest:request error:nil];
}

#pragma mark - virtual properties
- (NSNumber *)itemId
{
	NSNumber *itemId = nil;
	if ([self.itemType isEqualToString:@"recipe"]) {
		itemId = self.recipe.cloudId;
	}
	else if ([self.itemType isEqualToString:@"menu"]) {
		itemId = self.menuId;
	}
	else {
		itemId = self.productId;
	}
	return itemId;
}

#pragma mark - JSON
- (NSData *)toJSON
{
	// QuietLog(@"%s", __FUNCTION__);

	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];

	if ([self.status isEqualToString:kStatusDelete]) {
		[jsonDict setValue:self.cloudId forKey:kFoodLogId];
	}
	else {
		[jsonDict setValue:@([self.date timeIntervalSince1970]) forKey:kFoodLogDay];
		[jsonDict setValue:self.logTime forKey:kFoodLogLogTime];
		[jsonDict setValue:self.name forKey:kName];
		[jsonDict setValue:self.servings forKey:kFoodLogMultiplier];
		[jsonDict setValue:self.servingsUnit forKey:kFoodLogServingsUnits];
		[jsonDict setValue:self.nutritionFactor forKey:kFoodLogNutritionFactor];
		[jsonDict setValue:self.itemType forKey:kFoodLogItemType];

		if (self.cloudId) {
			[jsonDict setValue:self.cloudId forKey:kFoodLogId];
		}

		[jsonDict setValue:(self.recipe ? self.recipe.cloudId : [NSNull null]) forKey:kRecipeId];

		[jsonDict setValue:(self.menuId ? self.menuId : [NSNull null]) forKey:kFoodLogMenuId];

		[jsonDict setValue:(self.productId ? self.productId : [NSNull null]) forKey:kProductId];

		[jsonDict setValue:(self.tagId ?: [NSNull null]) forKey:kMPTagID];

		// Nutrients
		[self.nutrition toJSONDict:jsonDict];
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

#pragma mark - image handling
- (void)setProductImage:(UIImage *)productImage
{
	[self willChangeValueForKey:FoodLogMOAttributes.image];
	
	NSData *imageAsData = UIImagePNGRepresentation(productImage);
	[self setImage:imageAsData];
	[self setPrimitiveValue:imageAsData forKey:FoodLogMOAttributes.image];
	[self didChangeValueForKey:FoodLogMOAttributes.image];
}

- (UIImage*)productImage
{
	[self willAccessValueForKey:FoodLogMOAttributes.image];
	UIImage *productImage = [UIImage imageWithData:
		[self primitiveValueForKey:FoodLogMOAttributes.image]];
	[self didAccessValueForKey:FoodLogMOAttributes.image];
	return productImage;
}

@end
