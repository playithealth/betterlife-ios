#import "_AdvisorGroup.h"

@interface AdvisorGroup : _AdvisorGroup {}
- (void)prepareForDeletion;
+ (id)advisorGroupById:(NSNumber *)advisorGroupId
	withManagedObjectContext:(NSManagedObjectContext *)context;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
@end
