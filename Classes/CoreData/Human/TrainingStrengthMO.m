#import "TrainingStrengthMO.h"

#import "NSDictionary+NumberValue.h"
#import "TrainingStrengthSetMO.h"

@implementation TrainingStrengthMO
- (NSArray *)sortedSets
{
	NSSortDescriptor *sortBySortOrder = [NSSortDescriptor sortDescriptorWithKey:TrainingStrengthSetMOAttributes.sortOrder ascending:YES];
	return [[self.sets allObjects] sortedArrayUsingDescriptors:@[sortBySortOrder]];
}

- (NSString *)setsDescription
{
	NSSortDescriptor *sortBySortOrder = [NSSortDescriptor sortDescriptorWithKey:TrainingStrengthSetMOAttributes.sortOrder ascending:YES];
	NSArray *setsBySortOrder = [self.sets sortedArrayUsingDescriptors:@[sortBySortOrder]];

	NSString *descString = @"";
	if ([setsBySortOrder count]) {
		if ([setsBySortOrder count] == 1) {
			TrainingStrengthSetMO *set = [setsBySortOrder firstObject];
			if (set.weight && set.reps) {
				descString = [NSString stringWithFormat:@"%@ lbs %@ reps", set.weight, set.reps];
			}
			else if (set.weight) {
				descString = [NSString stringWithFormat:@"%@ lbs", set.weight];
			}
			else if (set.reps) {
				descString = [NSString stringWithFormat:@"%@ reps", set.reps];
			}
			else {
				// Empty string (default) if reps and weight are both nil
			}
		}
		else {
			descString = [NSString stringWithFormat:@"%lu sets", (unsigned long)[setsBySortOrder count]];
		}
	}

	/*
	NSInteger setNumber = 1;
	NSMutableArray *setsArray = [NSMutableArray array];
	for (TrainingStrengthSetMO *set in setsBySortOrder) {
		[setsArray addObject:[NSString stringWithFormat:@"%ld: %@ lbs %@ reps", (long)setNumber++, set.weight, set.reps]];
	}

	return [setsArray componentsJoinedByString:@", "];
	*/

	return descString;
}

#pragma mark - convenience methods
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// required keys
	NSNumber *day =	[update valueForKey:kTrainingLogDay];
	self.date = [NSDate dateWithTimeIntervalSince1970:[day integerValue]];
	self.updatedOn = [update BC_numberOrNilForKey:kUpdatedOn];
	self.name = [update BC_stringOrNilForKey:kName];

	// set data
	NSArray *sets = [update objectForKey:@"sets"];
	if ([sets count] > 0) {
		for (NSDictionary *setDict in sets) {
			// See if there is an existing set to update
			NSNumber *setNumber = [setDict BC_numberForKey:@"set_number"];
			NSSet *existingSets = [self.sets filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"%K = %@",
				TrainingStrengthSetMOAttributes.sortOrder, setNumber]];
			TrainingStrengthSetMO *setMO = nil;
			if ([existingSets count]) {
				setMO = [existingSets anyObject];
			}
			else {
				TrainingStrengthSetMO *setMO = [TrainingStrengthSetMO insertInManagedObjectContext:self.managedObjectContext];
				setMO.trainingStrength = self;
				setMO.sortOrder = setNumber;
			}

			setMO.weight = [setDict BC_numberForKey:@"weight"];
			setMO.reps = [setDict BC_numberForKey:@"reps"];
		}
	}

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

#pragma mark - JSON
- (NSData *)toJSON
{
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
		self.name, kStrengthLogName,
		@([self.date timeIntervalSince1970]), kTrainingLogDay,
		nil];

	if (self.cloudId) {
		[jsonDict setValue:self.cloudId forKey:kStrengthLogId];
	}

	NSMutableDictionary *setDicts = [NSMutableDictionary dictionary];
	for (TrainingStrengthSetMO *set in self.sets) {
		if (set.weight && set.reps) {
			NSMutableDictionary *setDict = [[NSMutableDictionary alloc] 
				initWithObjectsAndKeys:
				set.weight, kStrengthLogSetWeight,
				set.reps, kStrengthLogSetReps,
				nil];
			NSString *setKey = nil;
			if (set.sortOrder) {
				setKey = [set.sortOrder stringValue];
			}
			else {
				setKey = @"0";
			}
			[setDicts setValue:setDict forKey:setKey];
		}
	}
	[jsonDict setValue:setDicts forKey:kStrengthLogSets];

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
