#import "AdvisorMealPlanTagMO.h"

#import "NumberValue.h"

@implementation AdvisorMealPlanTagMO

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.name = [update objectForKey:kAdvisorMealPlanPhaseEntryTagName];
	self.servingTargetQuantity = [[update objectForKey:kAdvisorMealPlanPhaseEntryTagServingTargetQuantity] numberValueDecimal];
	self.servingTargetUnit = [update objectForKey:kAdvisorMealPlanPhaseEntryTagServingTargetUnit];
}

@end
