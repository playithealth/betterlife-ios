//
//  MealPlannerDay.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/15/2011.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "_MealPlannerDay.h"

#import "BLFoodItem.h"

@interface MealPlannerDay : _MealPlannerDay <BLFoodItem> {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
+ (id)insertMealPlannerDayWithTag:(AdvisorMealPlanTagMO *)tag forDate:(NSDate *)date withAdvisorMealPlanTagDictionary:(NSDictionary *)tagDict
	inContext:(NSManagedObjectContext *)moc;
+ (id)insertMealPlannerDayWithRecipeDictionary:(NSDictionary *)recipeDict forDate:(NSDate *)date logTime:(NSNumber *)logTime
	inContext:(NSManagedObjectContext *)moc;
+ (id)insertMealPlannerDayWithRecipeMO:(RecipeMO *)recipe forDate:(NSDate *)date logTime:(NSNumber *)logTime
	inContext:(NSManagedObjectContext *)moc;
- (void)markForSync;
- (NSNumber *)itemId;
- (NSString *)servingsUnit; ///< This is to be compliant with the protocol
- (NSData *)toJSON;
@end
