#import "AdvisorActivityPlanPhaseMO.h"

#import "AdvisorActivityPlanWorkoutMO.h"
#import "NSDictionary+NumberValue.h"
#import "User.h"

@interface AdvisorActivityPlanPhaseMO ()

// Private interface goes here.

@end

@implementation AdvisorActivityPlanPhaseMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	[super setWithUpdateDictionary:update];

	NSArray *inWorkouts = [update objectForKey:kAdvisorPlanPhaseEntries];

	// delete any workouts that didn't come with the update
	[self.workouts enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
		NSUInteger idx = [inWorkouts indexOfObjectPassingTest:^(id searchObj, NSUInteger idx, BOOL *stop) {
			return [[searchObj BC_numberOrNilForKey:kId] isEqualToNumber:[obj cloudId]];
		}];
		if (idx == NSNotFound) {
			[self.managedObjectContext deleteObject:obj];
		}
	}];

	User *thisUser = [User currentUser];

	NSUInteger sortOrder = 0;
	for (NSDictionary *entryDict in inWorkouts) {
		NSNumber *cloudId = [entryDict BC_numberForKey:kId];
		AdvisorActivityPlanWorkoutMO *workoutMO = [AdvisorActivityPlanWorkoutMO
			MR_findFirstByAttribute:AdvisorActivityPlanWorkoutMOAttributes.cloudId withValue:cloudId inContext:self.managedObjectContext];

		if (!workoutMO) {
			workoutMO = [AdvisorActivityPlanWorkoutMO insertInManagedObjectContext:self.managedObjectContext];
			workoutMO.cloudId = cloudId;
			workoutMO.loginId = thisUser.loginId;
			
			workoutMO.phase = self;
		}

		// I'm flattening out this structure, the cloud seems to have an 'Entry' layer, then 'Workout' layers beneath that, and I'm just
		// using the 'Entry' layer for its ids, then the 'Workout' layer for the details. This may need to be altered later if it causes
		// problems, time will tell.
		[workoutMO setWithUpdateDictionary:[entryDict objectForKey:kAdvisorActivityPlanWorkout]];
		workoutMO.sortOrder = @(sortOrder);
		sortOrder++;
	}
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
