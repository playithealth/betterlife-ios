#import "_RecipeMO.h"

@interface RecipeMO : _RecipeMO {}
+ (id)recipeById:(NSNumber *)mealId withManagedObjectContext:(NSManagedObjectContext *)context;
+ (id)mealByExternalId:(NSString *)externalId withManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSArray*)findAllByNameOrTag:(NSString *)searchTerm inContext:(NSManagedObjectContext *)inMOC;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)setWithRecipeDictionary:(NSDictionary *)recipe;
- (void)setWithUnifiedSearchDictionary:(NSDictionary *)recipe;
- (void)setStatus:(NSString *)newStatus;
- (void)setWithRecipeMO:(RecipeMO *)meal;
- (NSData *)toJSON;
- (NSString *)tagsString;
@end
