#import "CommunityMO.h"

#import "NSDictionary+NumberValue.h"
#import "UserProfileMO.h"

@interface CommunityMO ()
@end


@implementation CommunityMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.cloudId = [update BC_numberForKey:kId];
	self.name = [update valueForKey:kName];
	self.type = [update valueForKey:kCommunityType];
	self.visibility = [update valueForKey:kCommunityVisibility];
	self.ownerName = [update valueForKey:kCommunityOwnerName];
	self.ownerId = [update BC_numberForKey:kCommunityOwnerId];

	NSDictionary *memberDict = [update objectForKey:kCommunityMember];
	self.createdOn = [memberDict BC_numberForKey:kCreatedOn]; ///< Joined this community on...
	self.updatedOn = [memberDict BC_numberForKey:kUpdatedOn]; ///< Altered the 'options' for this user within the community on...
	self.communityStatus = [memberDict valueForKey:kStatus];
	self.usePhoto = @([[memberDict BC_numberForKey:kCommunityUsePhoto] boolValue]);

	NSNumber *loginId = [memberDict BC_numberForKey:kUserId];
	self.userProfile = [UserProfileMO MR_findFirstByAttribute:UserProfileMOAttributes.loginId withValue:loginId
			inContext:self.managedObjectContext];

	self.status = kStatusOk;
}

#pragma mark - sync to cloud
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
		kCommunityId : self.cloudId,
		kStatus : self.communityStatus,
		kCommunityUsePhoto : self.usePhoto
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
