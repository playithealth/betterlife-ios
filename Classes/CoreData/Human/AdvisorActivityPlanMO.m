#import "AdvisorActivityPlanMO.h"

#import "AdvisorActivityPlanPhaseMO.h"
#import "AdvisorActivityPlanTimelineMO.h"
#import "BCUrlFactory.h"
#import "NSCalendar+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"
#import "User.h"

@interface AdvisorActivityPlanMO ()
@end

@implementation AdvisorActivityPlanMO

+ (NSString *)syncName {
	return kSyncAdvisorActivityPlans;
}

+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict {
	return [[updateDict objectForKey:kAdvisorActivityPlan] BC_numberForKey:kId];
}

+ (NSString *)statusString {
	return kSyncUpdateHumanReadable_ActivityPlans;
}

+ (NSURL *)urlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit {
	return [BCUrlFactory advisorActivityPlansURL];
}

+ (void)setWithUpdatesArray:(NSArray *)updates forLogin:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)inMOC {
	[self BL_syncUpdateUsingModifiedDeleteStrategyForLogin:loginId withUpdates:updates inMOC:inMOC];
}

+ (NSPredicate *)userPredicate {
	return [self BL_loginIdPredicate];
}

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// I believe the topmost layer of the dictionary is the subscription info

	// Get the plan info
	NSDictionary *planUpdates = [update objectForKey:kAdvisorActivityPlan];
	[super setWithUpdateDictionary:planUpdates withPhaseClass:[AdvisorActivityPlanPhaseMO class]];
}

+ (id)getActivityPlanForDate:(NSDate *)date inContext:(NSManagedObjectContext *)context
{
	return [AdvisorPlanMO getPlanForClass:[AdvisorActivityPlanMO class] forDate:date inContext:context];
}

+ (id)getCurrentActivityPlanInContext:(NSManagedObjectContext *)context
{
	return [AdvisorActivityPlanMO getActivityPlanForDate:[NSDate date] inContext:context];
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
