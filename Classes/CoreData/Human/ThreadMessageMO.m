#import "ThreadMessageMO.h"

#import "NSDictionary+NumberValue.h"
#import "ThreadMO.h"
#import "UserProfileMO.h"

@interface ThreadMessageMO ()

@end


@implementation ThreadMessageMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.cloudId = [update BC_numberForKey:kThreadId];
	self.createdOn = [update BC_numberForKey:kCreatedOn];
	self.updatedOn = [update BC_numberForKey:kUpdatedOn];
	self.senderId = [update BC_numberForKey:kUserId];
	self.senderName = [update BC_stringOrNilForKey:kSenderName];
	self.body = [update valueForKey:kMessageBody];

	NSNumber *threadId = [update BC_numberForKey:kThreadId];
	self.thread = [ThreadMO MR_findFirstByAttribute:ThreadMOAttributes.cloudId withValue:threadId
		inContext:self.managedObjectContext];

	self.userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	self.status = kStatusOk;
}

#pragma mark - sync to cloud
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
		kThreadId : self.thread.cloudId,
		kMessageId : (self.cloudId ?: @0),
		kMessageBody : (self.body ?: @""),
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
