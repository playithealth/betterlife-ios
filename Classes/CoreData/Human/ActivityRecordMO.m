#import "ActivityRecordMO.h"

#import "ActivitySetMO.h"
#import "AdvisorActivityPlanWorkoutMO.h"
#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"

@interface ActivityRecordMO ()
@end

@implementation ActivityRecordMO

+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict {
	return [updateDict BC_numberForKey:kId];
}

+ (NSPredicate *)userPredicate {
	return [self BL_loginIdPredicate];
}

// Need a custom setter for targetDate so that we may generate the appropriate string for targetDateString
- (void)setTargetDate:(NSDate *)targetDate
{
    [self willChangeValueForKey:ActivityRecordMOAttributes.targetDate];
    [self setPrimitiveTargetDate:targetDate];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [self setPrimitiveTargetDateString:[dateFormatter stringFromDate:targetDate]];
    [self didChangeValueForKey:ActivityRecordMOAttributes.targetDate];
}

- (void)updateFromSearchDict:(NSDictionary *)searchDict inManagedObjectContext:(NSManagedObjectContext *)context
{
	// NOTE: Search dicts do no not have a target date or a loginId, they need to be set outside of this method
	self.cloudId = @0;
	self.activityId = [searchDict BC_numberForKey:kId];
	self.activityName = [searchDict BC_stringOrNilForKey:kName];
	self.activityDescription = [searchDict BC_stringOrNilForKey:kDescription];
	self.isTimedActivity = [searchDict BC_numberForKey:kActivityRecordTimed];
	self.factor = [searchDict BC_numberOrNilForKey:kActivityRecordFactor];
	self.link = [searchDict BC_stringOrNilForKey:kActivityRecordLink];
	self.source = [searchDict BC_numberOrNilForKey:kSource];
	self.notes = [searchDict BC_stringOrNilForKey:kActivityRecordNotes];
	self.isRemoved = [searchDict BC_numberForKey:kDeleted];

	// Sets
	NSArray *sets = [searchDict objectForKey:kActivityRecordSets];
	for (NSDictionary *setDict in sets) {
		ActivitySetMO *activitySet = nil;

		for (ActivitySetMO *existingSet in self.sets) {
			if ([existingSet.cloudId isEqualToNumber:[setDict BC_numberForKey:kId]]) {
				activitySet = existingSet;
				break;
			}
		}

		if (!activitySet) {
			activitySet = [ActivitySetMO insertInManagedObjectContext:self.managedObjectContext];
			[self addSetsObject:activitySet];
		}

		[activitySet setWithUpdateDictionary:setDict];
	}
}

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	NSDictionary *activity = [update objectForKey:kActivityRecordActivity]; // Some things are under the activity sub-key

	// Let the activity super class fill out its stuff
	[super setWithUpdateDictionary:update];

	// Since ActivityRecord inherits from Activity, the cloudId needs to be set to the activity id, not the activity record id!!!
	self.cloudId = [update BC_numberForKey:kId];
	// Activity Id here represents an activity on the cloud (separate table on cloud, same table in Core Data), and is the activityId
	self.activityId = [update BC_numberForKey:kActivityId];

	self.targetDateString = [update valueForKey:kActivityRecordTargetDate];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
	self.targetDate = [dateFormatter dateFromString:self.targetDateString];
	self.updatedOn = [update BC_numberOrNilForKey:kActivityRecordUpdatedOn];

	self.isRemoved = [activity BC_numberForKey:kDeleted];

	// Sets
	NSArray *sets = [activity objectForKey:kActivityRecordSets];
	for (NSDictionary *setDict in sets) {
		ActivitySetMO *activitySet = nil;

		for (ActivitySetMO *existingSet in self.sets) {
			if ([existingSet.cloudId isEqualToNumber:[setDict BC_numberForKey:kId]]) {
				activitySet = existingSet;
				break;
			}
		}

		if (!activitySet) {
			activitySet = [ActivitySetMO insertInManagedObjectContext:self.managedObjectContext];
			[self addSetsObject:activitySet];
		}

		[activitySet setWithUpdateDictionary:setDict];
	}

	self.status = kStatusOk;
}

#pragma mark - convenience methods
- (NSNumber *)totalCalories {
	double calories = 0.0;
	for (ActivitySetMO *set in self.sets) {
		calories += [set.calories doubleValue];
	}

	return @(calories);
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
#warning "Must provide toJSON implementation"
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{

		<#kConstName#> : self.activityDescription,

		<#kConstName#> : self.activityId,

		<#kConstName#> : self.cloudId,

		<#kConstName#> : self.factor,

		<#kConstName#> : self.isRemoved,

		<#kConstName#> : self.isTimedActivity,

		<#kConstName#> : self.link,

		<#kConstName#> : self.name,

		<#kConstName#> : self.notes,

		<#kConstName#> : self.source,

		<#kConstName#> : self.status,

		<#kConstName#> : self.targetDate,

		<#kConstName#> : self.targetDateString,

		<#kConstName#> : self.updatedOn,

		<#kConstName#> : self.usedBy,

	};

	// TODO sets

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
