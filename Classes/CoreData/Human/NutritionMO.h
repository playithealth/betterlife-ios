#import "_NutritionMO.h"

@class RecipeMO;

@interface NutritionMO : _NutritionMO {}

@property (readonly) NSNumber *calories_from_fat;
@property (readonly) NSNumber *saturated_fat_calories;
@property (readonly) NSNumber *total_fat;
@property (readonly) NSNumber *saturated_fat;
@property (readonly) NSNumber *polyunsaturated_fat;
@property (readonly) NSNumber *monounsaturated_fat;
@property (readonly) NSNumber *trans_fat;
@property (readonly) NSNumber *total_carbohydrate;
@property (readonly) NSNumber *dietary_fiber;
@property (readonly) NSNumber *soluble_fiber;
@property (readonly) NSNumber *insoluble_fiber;
@property (readonly) NSNumber *sugar_alcohol;
@property (readonly) NSNumber *other_carbohydrate;
@property (readonly) NSNumber *vitamin_a_pct;
@property (readonly) NSNumber *vitamin_c_pct;
@property (readonly) NSNumber *calcium_pct;
@property (readonly) NSNumber *iron_pct;

//+ (id)nutritionById:(NSNumber *)nutritionId inMOC:(NSManagedObjectContext *)inMOC;
+ (NutritionMO *)insertNutritionWithDictionary:(NSDictionary *)nutritionDictionary inContext:(NSManagedObjectContext *)moc;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)copyFromNutritionMO:(NutritionMO *)nutrition;

- (NSSet *)nutrientsWithValues;
- (NSMutableDictionary *)nutrientDictWithValues;
+ (NSArray *)allKeys;
+ (NSString *)localKeyFromCloudKey:(NSString *)cloudKey;
- (void)toJSONDict:(NSMutableDictionary *)jsonDict;

@end
