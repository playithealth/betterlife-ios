//
//  CategoryMO.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 5/2/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//


#import "CategoryMO.h"

@implementation CategoryMO
- (NSString *)nameFirstCharacter 
{
    [self willAccessValueForKey:@"nameFirstCharacter"];
	NSRange firstCharacterRange = [[self name] rangeOfCharacterFromSet:
		[NSCharacterSet alphanumericCharacterSet]];
    NSString *firstCharacter = nil;
	if (firstCharacterRange.location != NSNotFound) {
		firstCharacter = [[[self name] uppercaseString] 
			substringWithRange:firstCharacterRange];
	}
    [self didAccessValueForKey:@"nameFirstCharacter"];
    return firstCharacter;
}

+ (id)categoryById:(NSNumber *)categoryId withManagedObjectContext:(NSManagedObjectContext *)context
{
	return [CategoryMO MR_findFirstByAttribute:CategoryMOAttributes.cloudId withValue:categoryId inContext:context];
}

@end

@implementation CategoryMO (GenericValueDisplay)
- (NSString *)genericValueDisplay {
	return self.name;
}

@end
