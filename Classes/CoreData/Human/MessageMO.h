#import "_MessageMO.h"

@interface MessageMO : _MessageMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (NSData *)toJSON;
@end
