//
// TrackingMO.m
// BettrLife Corporation
//
// Created by Sef Tarbell on 5/11/2012.
// Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "TrackingMO.h"

#import "NSCalendar+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NumberValue.h"
#import "TrackingTypeMO.h"
#import "TrackingValueMO.h"
#import "User.h"

const struct TrackingTypes TrackingTypes = {
	.weight = 1,
	.bodyFat = 2,
	.bloodPressure = 3,
	.bloodGlucose = 4,
	.hA1c = 5,
	.myzone = 18,
};

@implementation TrackingMO

#pragma mark - sync methods
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	NSNumber *date = [update valueForKey:kTrackingDate];
	self.date = [NSDate dateWithTimeIntervalSince1970:[date integerValue]];
	self.trackingType = [[update valueForKey:kTrackingType] numberValueDecimal];
	self.updatedOn = [[update valueForKey:kUpdatedOn] numberValueDecimal];

	NSMutableDictionary *inputDict = [NSMutableDictionary dictionary];
	for (NSInteger idx = 1; idx <= 6; idx++) {
		NSString *trackValueName = [NSString stringWithFormat:@"track_value%ld", (long)idx];
		NSNumber *trackValue = [update objectForKey:trackValueName];
		if (trackValue && (trackValue != (id)[NSNull null])) {
			[inputDict setValue:trackValue forKey:trackValueName];
		}
	}

	if ([update objectForKey:kSource]) {
        self.source = [update BC_numberOrNilForKey:kSource];
	}

	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputDict options:kNilOptions error:nil];
	self.valueJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];;

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

#pragma mark - convenience methods
+ (NSNumber *)getCurrentWeightInMOC:(NSManagedObjectContext *)context
{
	NSNumber *currentWeight = nil;
	TrackingMO *lastWeightTrack = [TrackingMO getLastTrackOfType:TrackingTypes.weight inMOC:context];
	currentWeight = [lastWeightTrack getValueFromJSONForKey:kTrackingValue1];

	return currentWeight;
}

+ (TrackingMO *)getLastTrackOfType:(NSInteger)type inMOC:(NSManagedObjectContext *)context
{
	return [TrackingMO MR_findFirstWithPredicate:
		[NSPredicate predicateWithFormat:@"loginId = %@ and status <> %@ and trackingType = %d",
			[[User currentUser] loginId], kStatusDelete, type]
		sortedBy:TrackingMOAttributes.date ascending:NO inContext:context];
}

- (NSDictionary *)valueDictionary
{
	NSDictionary *valueDict = nil;
	if (self.valueJSON) {
		valueDict = [NSJSONSerialization JSONObjectWithData:[self.valueJSON dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];

		// If there were no values, set the dictionary back to nil
		if (![valueDict count]) {
			valueDict = nil;
		}
	}

	return valueDict;
}

- (void)setValueDictionary:(NSDictionary *)valueDictionary
{
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:valueDictionary options:kNilOptions error:nil];
	self.valueJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]; 
}

- (id)getValueFromJSONForKey:(NSString *)key
{
	NSNumber *value = nil;
	NSData *jsonData = [self.valueJSON dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
	if (jsonData) {
		NSDictionary *trackingDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
		value = [trackingDict BC_numberOrNilForKey:key];
	}

	return value;
}

+ (TrackingMO *)addTrackOfType:(NSInteger)type onDate:(NSDate *)date withValueDictionary:(NSDictionary *)valueDictionary
inMOC:(NSManagedObjectContext *)context
{
	TrackingMO *newTrack = [TrackingMO insertInManagedObjectContext:context];
	newTrack.loginId = [[User currentUser] loginId];
	newTrack.trackingType = [NSNumber numberWithInteger:type];
	newTrack.status = kStatusPost;

	newTrack.valueDictionary = valueDictionary;

	newTrack.date = date;

	return newTrack;
}

+ (TrackingMO *)addWeight:(NSNumber *)weight onDate:(NSDate *)date inMOC:(NSManagedObjectContext *)context
{
	return [TrackingMO addTrackOfType:TrackingTypes.weight onDate:date withValueDictionary:@{ kTrackingValue1 : weight } inMOC:context];
}

+ (NSArray *)keysForTrackingType:(BCTrackingType)trackingType
{
	NSArray *keys = nil;
	switch (trackingType) {
		case BCTrackingTypeWeight:
			keys = @[kTrackingWeight];
			break;
		case BCTrackingTypeBodyFat:
			keys = @[kTrackingBodyFat];
			break;
		case BCTrackingTypeBloodPressure:
			keys = @[kTrackingDiastolic, kTrackingSystolic];
			break;
		case BCTrackingTypeBloodGlucose:
			keys = @[kTrackingGlucose];
			break;
		case BCTrackingTypeHA1c:
			keys = @[kTrackingHA1c];
			break;
		default:
			break;
	}
	return keys;
}

#pragma mark - statistic methods
/* This method returns something that looks like this:
	NSArray [
		NSDictionary {
			dayOffset = 1;
			"track_value1" = 130;
			"track_value2" = 72;
		},
		NSDictionary {
			dayOffset = 2;
			"track_value1" = 122;
			"track_value2" = 82;
		},
		NSDictionary {
			dayOffset = 4;
			"track_value1" = 118;
			"track_value2" = 74;
		},
		NSDictionary {
			dayOffset = 6;
			"track_value1" = 140;
			"track_value2" = 60;
		}
	]
*/
+ (NSArray *)onePerDayForTrackingType:(BCTrackingType)trackingType numberOfDays:(NSInteger)numberOfDays
usingCalendar:(NSCalendar *)calendar inMOC:(NSManagedObjectContext *)context
{
	NSMutableArray *dailyValues = [NSMutableArray array];

	NSDictionary *valueDictionary;
	for (NSInteger dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
		NSDate *date = [calendar BC_dateByAddingDays:-1 * dayIndex toDate:[NSDate date]];

		TrackingMO *trackingMO = [TrackingMO MR_findFirstWithPredicate:
			[NSPredicate predicateWithFormat:@"trackingType = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
				trackingType, [calendar BC_startOfDate:date], [calendar BC_endOfDate:date], kStatusDelete, [User loginId]]
			sortedBy:TrackingMOAttributes.date ascending:NO inContext:context];
		if (trackingMO && (valueDictionary = trackingMO.valueDictionary)) {
			NSMutableDictionary *dailyDict = [valueDictionary mutableCopy];
			[dailyDict setObject:@(dayIndex) forKey:@"dayOffset"];
			[dailyValues addObject:dailyDict];
		}
	}

	// If we have some points for this time period, grab the prior value as well, so that we can trend the line in
	if ([dailyValues count]) {
		NSDate *date = [calendar BC_dateByAddingDays:-1 * numberOfDays toDate:[NSDate date]];
		TrackingMO *trackingMO = [TrackingMO MR_findFirstWithPredicate:
			[NSPredicate predicateWithFormat:@"trackingType = %d AND date < %@ AND status <> %@ and loginId = %@",
				trackingType, [calendar BC_endOfDate:date], kStatusDelete, [User loginId]]
			sortedBy:TrackingMOAttributes.date ascending:NO inContext:context];

		if (trackingMO && (valueDictionary = trackingMO.valueDictionary)) {
			NSMutableDictionary *dailyDict = [valueDictionary mutableCopy];
			[dailyDict setObject:@(numberOfDays) forKey:@"dayOffset"];
			[dailyValues addObject:dailyDict];
		}
	}

	return dailyValues;
}

//
// NOTE: Unused right now
//
+ (NSDictionary *)dailyAveragesForTrackingType:(BCTrackingType)trackingType numberOfDays:(NSInteger)numberOfDays usingCalendar:(NSCalendar *)calendar inMOC:(NSManagedObjectContext *)context
{
	User *thisUser = [User currentUser];

	NSMutableDictionary *dailyAveragesByKey = [NSMutableDictionary dictionary];

	NSArray *keys = [TrackingMO keysForTrackingType:trackingType];
	for (id key in keys) {

		NSMutableArray *dailyAverages = [NSMutableArray array];
		NSMutableArray *indices = [NSMutableArray array];

		for (NSInteger dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
			NSDate *date = [calendar BC_dateByAddingDays:-1 * dayIndex toDate:[NSDate date]];

			NSArray *logs = [TrackingMO MR_findAllSortedBy:TrackingMOAttributes.date ascending:NO
				withPredicate:[NSPredicate predicateWithFormat:@"trackingType = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
					trackingType, [calendar BC_startOfDate:date], [calendar BC_endOfDate:date], kStatusDelete, thisUser.loginId]
				inContext:context];

			if ([logs count] > 0) {
				// translate the data to just a collection of NSNumbers
				NSMutableArray *values = [NSMutableArray array];
				for (TrackingMO *log in logs) {
					[values addObject:[log getValueFromJSONForKey:key]];
				}

				[dailyAverages addObject:@([[values valueForKeyPath:@"@avg.floatValue"] floatValue])];
				[indices addObject:@(dayIndex)];
			}
		}

		dailyAveragesByKey[key] = dailyAverages;
		dailyAveragesByKey[[NSString stringWithFormat:@"%@indices", key]] = indices;
	}

	return dailyAveragesByKey;
}

+ (NSArray *)monthlyAveragesForTrackingType:(BCTrackingType)trackingType numberOfMonths:(NSInteger)numberOfMonths
usingCalendar:(NSCalendar *)calendar inMOC:(NSManagedObjectContext *)context
{
	NSMutableArray *monthlyAverages = [NSMutableArray array];

	for (NSInteger monthIndex = 0; monthIndex < numberOfMonths; monthIndex++) {
		NSDate *baseDate = [calendar BC_dateByAddingMonths:-1 * monthIndex toDate:[NSDate date]];
		NSDate *firstOfMonth = [calendar BC_firstOfMonthForDate:baseDate];
		NSDate *lastOfMonth = [calendar BC_lastOfMonthForDate:baseDate];

		NSArray *logs = [TrackingMO MR_findAllSortedBy:TrackingMOAttributes.date ascending:NO
			withPredicate:[NSPredicate predicateWithFormat:@"trackingType = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
				trackingType, [calendar BC_startOfDate:firstOfMonth], [calendar BC_endOfDate:lastOfMonth], kStatusDelete, [User loginId]]
			inContext:context];

		if ([logs count]) {
			NSArray *logValues = [logs valueForKeyPath:@"valueDictionary"];
			TrackingTypeMO *trackingTypeMO = [TrackingTypeMO MR_findFirstByAttribute:TrackingTypeMOAttributes.cloudId
				withValue:@(trackingType)];
			NSMutableDictionary *valueAveragesDict = [NSMutableDictionary dictionary];
			for (TrackingValueMO *trackingValueMO in trackingTypeMO.trackingValues) {
				NSNumber *averageValue = [logValues valueForKeyPath:[NSString stringWithFormat:@"@avg.%@", trackingValueMO.field]];
				if (averageValue) {
					[valueAveragesDict setObject:averageValue forKey:trackingValueMO.field];
				}
			}
			if ([valueAveragesDict count]) {
				[valueAveragesDict setObject:@(monthIndex) forKey:@"dayOffset"];
				[monthlyAverages addObject:valueAveragesDict];
			}
		}
	}

	return monthlyAverages;
}

#pragma mark - JSON
- (NSData *)toJSON
{
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
		self.trackingType, kTrackingType,
		[NSNumber numberWithInt:[self.date timeIntervalSince1970]], kTrackingDate,
		nil];

	if (self.cloudId) {
		[jsonDict setValue:self.cloudId forKey:kTrackingID];
	}

	[jsonDict setValue:self.valueDictionary forKey:kTrackingValues];

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
