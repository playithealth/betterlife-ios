#import "MealIngredient.h"

#import "CategoryMO.h"
#import "NumberValue.h"

@implementation MealIngredient

- (NSString *)description
{
	if (self.fullText && [self.fullText length]) {
		return self.fullText;
	}
	else {
		return [self generateFullText];
	}
}

- (NSString *)generateFullText
{
	NSMutableString *descriptionString = [[NSMutableString alloc] init];

	if (self.measure && [self.measure length]) {
		if ([descriptionString length]) {
			[descriptionString appendFormat:@" %@", self.measure];
		}
		else {
			[descriptionString appendFormat:@"%@", self.measure];
		}
	}

	if (self.measureUnits && [self.measureUnits length]) {
		if ([descriptionString length]) {
			[descriptionString appendFormat:@" %@", self.measureUnits];
		}
		else {
			[descriptionString appendFormat:@"%@", self.measureUnits];
		}
	}

	if (self.name && [self.name length]) {
		if ([descriptionString length]) {
			[descriptionString appendFormat:@" %@", self.name];
		}
		else {
			[descriptionString appendFormat:@"%@", self.name];
		}
	}

	if (self.notes && [self.notes length]) {
		if ([descriptionString length]) {
			[descriptionString appendFormat:@", %@", self.notes];
		}
		else {
			[descriptionString appendFormat:@"%@", self.notes];
		}
	}

	return descriptionString;
}

+ (MealIngredient *)itemByCloudId:(NSNumber *)cloudId inMOC:(NSManagedObjectContext *)inMOC
{
	return [MealIngredient MR_findFirstByAttribute:MealIngredientAttributes.cloudId withValue:cloudId inContext:inMOC];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update 
{
	// required keys
	self.cloudId = [[update valueForKey:kId]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];

	// optional keys
	if ((NSNull *)[update objectForKey:kItemName] != [NSNull null]) {
		self.name = [update valueForKey:kItemName];
	}

	if ((NSNull *)[update objectForKey:kProductId] != [NSNull null]) {
		self.productId = [[update valueForKey:kProductId]
			numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	}
	if ((NSNull *)[update objectForKey:kAddToShoppingList] != [NSNull null]) {
		self.addToShoppingList = [[update valueForKey:kAddToShoppingList]
			numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	}
	if ((NSNull *)[update objectForKey:kMeasure] != [NSNull null]) {
		self.measure = [update valueForKey:kMeasure];
	}
	if ((NSNull *)[update objectForKey:kMeasureUnits] != [NSNull null]) {
		self.measureUnits = [update valueForKey:kMeasureUnits];
	}
	if ((NSNull *)[update objectForKey:kMeasureUnitsId] != [NSNull null]) {
		self.measureUnitsId = [[update valueForKey:kMeasureUnitsId]
			numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	}
	if ((NSNull *)[update objectForKey:kShoppingListText] != [NSNull null]) {
		self.shoppingListText = [update valueForKey:kShoppingListText];
	}
	if ((NSNull *)[update objectForKey:kShoppingListQuantity] != [NSNull null]) {
		self.shoppingListQuantity = [[update valueForKey:kShoppingListQuantity]
			numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	}
	if ((NSNull *)[update objectForKey:kShoppingListCategoryId] != [NSNull null]) {
		self.shoppingListCategory = [CategoryMO categoryById:[[update valueForKey:kShoppingListCategoryId]
			numberValueWithNumberStyle:NSNumberFormatterDecimalStyle]
			withManagedObjectContext:self.managedObjectContext];
	}
	if ((NSNull *)[update objectForKey:kIngredientNotes] != [NSNull null]) {
		self.notes = [update valueForKey:kIngredientNotes];
	}
	if ((NSNull *)[update objectForKey:kOriginalIngredientText] != [NSNull null]) {
		self.fullText = [update valueForKey:kOriginalIngredientText];
	}
}

- (void)setWithExternalRecipeDetailDictionary:(NSDictionary *)recipeDetailDictionary
{
	self.name = [recipeDetailDictionary valueForKey:@"desc"];
	self.measure = [recipeDetailDictionary valueForKey:@"quant"];
	self.measureUnits = [recipeDetailDictionary valueForKey:@"measure"];
	self.shoppingListText = [recipeDetailDictionary valueForKey:@"desc"];
	self.notes = [recipeDetailDictionary valueForKey:@"notes"];
	self.fullText = [recipeDetailDictionary valueForKey:@"text"];
}

- (void)setWithRecipeDetailDictionary:(NSDictionary *)recipeDetailDictionary
{
	self.name = [recipeDetailDictionary objectForKey:kItemName];
	self.cloudId = [[recipeDetailDictionary objectForKey:kId] numberValueDecimal];
	if ((NSNull *)[recipeDetailDictionary objectForKey:kMeasure] != [NSNull null]) {
		self.measure = [recipeDetailDictionary objectForKey:kMeasure];
	}
	if ((NSNull *)[recipeDetailDictionary objectForKey:kMeasureUnits] != [NSNull null]) {
		self.measureUnits = [recipeDetailDictionary objectForKey:kMeasureUnits];
	}
	self.shoppingListText = [recipeDetailDictionary objectForKey:kShoppingListText];

	if ((NSNull *)[recipeDetailDictionary objectForKey:kIngredientNotes] != [NSNull null]) {
		self.notes = [recipeDetailDictionary objectForKey:kIngredientNotes];
	}
	
	if ((NSNull *)[recipeDetailDictionary objectForKey:kOriginalIngredientText] != [NSNull null]) {
		self.fullText = [recipeDetailDictionary objectForKey:kOriginalIngredientText];
	}
	else {
		self.fullText = @"";
	}
}

- (void)setWithMealIngredient:(MealIngredient *)mealIngredient
{
	self.cloudId = mealIngredient.cloudId;
	self.name = mealIngredient.name;
	self.productId = mealIngredient.productId;
	self.sortOrder = mealIngredient.sortOrder;
	self.addToShoppingList = mealIngredient.addToShoppingList;
	self.measure = mealIngredient.measure;
	self.measureUnits = mealIngredient.measureUnits;
	self.measureUnitsId = mealIngredient.measureUnitsId;
	self.shoppingListText = mealIngredient.shoppingListText;
	self.shoppingListQuantity = mealIngredient.shoppingListQuantity;
	self.shoppingListCategory = mealIngredient.shoppingListCategory;
	self.notes = mealIngredient.notes;
	self.fullText = mealIngredient.fullText;
}

#pragma mark - JSON
- (NSDictionary *)toDictionary
{
	// QuietLog(@"%s", __FUNCTION__);
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] 
		initWithObjectsAndKeys:
			self.name, kItemName,
			nil];

	if (self.cloudId) {
		[jsonDict setValue:self.cloudId forKey:kId];
	}
	if (self.productId) {
		[jsonDict setValue:self.productId forKey:kProductId];
	}
	if (self.addToShoppingList) {
		// This looks wierd, but we need this to NOT be a boolean NSNumber or it may go out as true/false
		[jsonDict setValue:[NSNumber numberWithInteger:[self.addToShoppingList integerValue]]
			forKey:kAddToShoppingList];
	}
	else {
		// Default to NO
		[jsonDict setValue:[NSNumber numberWithInteger:0] forKey:kAddToShoppingList];
	}
	if (self.measure) {
		[jsonDict setValue:self.measure forKey:kMeasure];
	}
	if (self.measureUnits) {
		[jsonDict setValue:self.measureUnits forKey:kMeasureUnits];
	}
	if (self.measureUnitsId) {
		[jsonDict setValue:self.measureUnitsId forKey:kMeasureUnitsId];
	}
	if (self.shoppingListText) {
		[jsonDict setValue:self.shoppingListText forKey:kShoppingListText];
	}
	if (self.shoppingListQuantity) {
		[jsonDict setValue:self.shoppingListQuantity forKey:kShoppingListQuantity];
	}
	if (self.shoppingListCategory) {
		[jsonDict setValue:self.shoppingListCategory.cloudId forKey:kShoppingListCategoryId];
	}
	if (self.notes) {
		[jsonDict setValue:self.notes forKey:kIngredientNotes];
	}
	if (self.fullText) {
		[jsonDict setValue:self.fullText forKey:kOriginalIngredientText];
	}

	return jsonDict;
}

- (NSData *)toJSON
{
	return [NSJSONSerialization dataWithJSONObject:[self toDictionary] options:kNilOptions error:nil];
}

@end
