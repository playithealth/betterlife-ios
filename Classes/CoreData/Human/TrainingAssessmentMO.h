//
// TrainingAssessmentMO.h
// BettrLife Corporation
//
// Created by Sef Tarbell on 5/31/2012.
// Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "_TrainingAssessmentMO.h"

@interface TrainingAssessmentMO : _TrainingAssessmentMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)markForSync;
+ (TrainingAssessmentMO *)getLastAssessmentInMOC:(NSManagedObjectContext *)context;
- (NSString *)listTitle;
- (NSString *)listDescription;
- (NSData *)toJSON;
@end
