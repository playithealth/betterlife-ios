#import "_MealIngredient.h"

@interface MealIngredient : _MealIngredient {}
- (NSString *)description;
- (NSString *)generateFullText;
+ (MealIngredient *)itemByCloudId:(NSNumber *)cloudId
	inMOC:(NSManagedObjectContext *)inMOC;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)setWithExternalRecipeDetailDictionary:(NSDictionary *)recipeDetailDictionary;
- (void)setWithRecipeDetailDictionary:(NSDictionary *)recipeDetailDictionary;
- (void)setWithMealIngredient:(MealIngredient *)mealIngredient;
- (NSDictionary *)toDictionary;
- (NSData *)toJSON;
@end
