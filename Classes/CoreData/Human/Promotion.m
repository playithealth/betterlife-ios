//
//  Promotion.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 4/8/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "Promotion.h"

#import "ShoppingListItem.h"

@implementation Promotion

- (NSData *)toJSON 
{
	// QuietLog(@"%s", __FUNCTION__);
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] 
		initWithObjectsAndKeys:
			self.cloudId, kPromotionId,
			self.shoppingListItem.cloudId, kPromotionShoppingListId,
			nil];

	if (![self.accepted boolValue]) {
		[jsonDict setValue:[NSNumber numberWithInt:1] forKey:kPromotionDelete];
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
