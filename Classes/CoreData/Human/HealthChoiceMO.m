#import "HealthChoiceMO.h"

const struct HealthChoiceTypes HealthChoiceTypes = {
	.allergy = 0,
	.lifestyle = 1,
	.ingredient = 2,
	.maximum = 3,
};

@implementation HealthChoiceMO

@end
