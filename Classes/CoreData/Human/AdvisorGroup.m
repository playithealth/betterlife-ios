#import "AdvisorGroup.h"

#import "RecipeMO.h"
#import "NumberValue.h"

@implementation AdvisorGroup

#pragma mark - Managed Object overrides

- (void)prepareForDeletion
{
	// Soft-delete all the meals associated with this advisorGroup,
	// if the recipe is exclusive to this group
	for (RecipeMO *recipe in self.recipes) {
		if ([recipe.advisorGroups count] <= 1) {
			// If the recipe hasn't been used by this account, just delete it instead of soft-deleting it
			if (![recipe.foodLog count] && ![recipe.plannerDays count]) {
				[self.managedObjectContext deleteObject:recipe];
			}
			else {
				// Soft delete
				recipe.removed = @YES;
			}
		}
	}
}

#pragma mark - convenience methods

+ (id)advisorGroupById:(NSNumber *)advisorGroupId withManagedObjectContext:(NSManagedObjectContext *)context
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cloudId = %@", advisorGroupId];

	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:[AdvisorGroup entityInManagedObjectContext:context]];
	[request setPredicate:predicate];

	NSError *error;
	NSArray *results = [context executeFetchRequest:request error:&error];

	if ([results count] > 0) {
		return [results objectAtIndex:0];
	}
	
	return nil;
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.cloudId = [[update valueForKey:kAdvisorGroupId] numberValueDecimal];
}

@end
