#import "_AdvisorMealPlanMO.h"

#import "BLSyncEntity.h"

@interface AdvisorMealPlanMO : _AdvisorMealPlanMO <BLSyncEntity> {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (NSString *)getMealPlanDetailText;
+ (id)getCurrentMealPlanInContext:(NSManagedObjectContext *)context;
+ (id)getMealPlanForDate:(NSDate *)date inContext:(NSManagedObjectContext *)context;
- (id)getMealPlanEntryForDate:(NSDate *)date atLogTime:(NSNumber *)logTime;
+ (id)mealPlanById:(NSNumber *)mealPlanId inContext:(NSManagedObjectContext *)context;
@end
