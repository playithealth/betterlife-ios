//
//  ShoppingListItem.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 4/8/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "ShoppingListItem.h"

#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BCAppDelegate.h"
#import "CategoryMO.h"
#import "CustomFoodMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "NumberValue.h"
#import "PantryItem.h"
#import "PurchaseHxItem.h"
#import "Store.h"
#import "StoreCategory.h"
#import "User.h"

#define kShopListValidationDomain @"com.BettrLife.BettrLifeApp.ShopListValidationDomain"
#define kShopListValidationNameQuantityCode 1000

@interface ShoppingListItem ()
- (void)setCategoryUsingSuggestedCategory:(CategoryMO *)suggestedCategory;
@end

@implementation ShoppingListItem

#pragma mark - virtual properties
- (NSString *)nameFirstCharacter
{
	[self willAccessValueForKey:@"nameFirstCharacter"];
	NSRange firstCharacterRange = [[self name] rangeOfCharacterFromSet:
		[NSCharacterSet alphanumericCharacterSet]];
	NSString *firstCharacter = [[[self name] uppercaseString]
		substringWithRange:firstCharacterRange];
	[self didAccessValueForKey:@"nameFirstCharacter"];
	return firstCharacter;
}

#pragma mark - validation methods
- (BOOL)validateNameQuantity:(NSError **)outError
{
	if ((self.name == nil || [self.name length] == 0)
		|| (self.quantity == nil || [self.quantity integerValue] < 0)) {
		if (outError != NULL) {
			NSString *errorStr = @"Must provide item name, quantity";
			NSDictionary *userInfoDict = [NSDictionary dictionaryWithObject:errorStr
				forKey:NSLocalizedDescriptionKey];
			NSError *error = [[NSError alloc] initWithDomain:kShopListValidationDomain
				code:kShopListValidationNameQuantityCode
				userInfo:userInfoDict];
			*outError = error;
		}
		DDLogWarn(@"shoppinglist item failed validation\r%@", [self description]);
		return NO;
	}
	// QuietLog(@"shoppinglist item passed validation\r%@", [self description]);
	return YES;
}

- (BOOL)validateForInsert:(NSError **)outError
{
	BOOL validated = [self validateNameQuantity:outError];

	if (!validated) {
		return validated;
	}
	return [super validateForInsert:outError];
}

- (BOOL)validateForUpdate:(NSError **)outError
{
	BOOL validated = [self validateNameQuantity:outError];

	if (!validated) {
		return validated;
	}
	return [super validateForUpdate:outError];
}

#pragma mark - JSON
- (NSData *)toJSON
{
	NSMutableDictionary *jsonDict = nil;

	if ([self.isRecommendation boolValue]) {
		/* code */
		jsonDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
			self.name, kItemName,
			self.productId, kProductId,
			nil];
	}
	else {
		jsonDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
			self.name, kItemName,
			self.quantity, kQuantity,
			nil];

		if (self.cloudId) {
			[jsonDict setValue:self.cloudId forKey:kCloudId];
		}
		if (self.productId) {
			[jsonDict setValue:self.productId forKey:kProductId];
		}
		if (self.category) {
			[jsonDict setValue:self.category.cloudId forKey:kCategoryId];
		}
		if (self.barcode) {
			[jsonDict setValue:self.barcode forKey:kBarcode];
		}
		if (self.purchased) {
			NSNumber *purchasedNumber = [NSNumber numberWithInteger:[self.purchased integerValue]];
			[jsonDict setValue:purchasedNumber forKey:kPurchased];

			[jsonDict setValue:self.storeId forKey:kEntityId];
		}
		if (self.private) {
			NSNumber *privateNumber = [NSNumber numberWithInteger:[self.private integerValue]];
			[jsonDict setValue:privateNumber forKey:kPrivate];
		}
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

#pragma mark - convenience methods
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// required keys
	self.cloudId = [[update valueForKey:kCloudId] numberValueDecimal];
	self.productId = [[update valueForKey:kProductId] numberValueDecimal];
	self.name = [update valueForKey:kItemName];
	self.category = [CategoryMO categoryById:
		[[update valueForKey:kCategoryId] numberValueDecimal]
		withManagedObjectContext:self.managedObjectContext];
	self.quantity = [[update valueForKey:kQuantity] numberValueDecimal];
	self.updatedOn = [[update valueForKey:kUpdatedOn] numberValueDecimal];
	self.private = [[update valueForKey:kPrivate] numberValueDecimal];
	// the getList model call filters out purchased items and does not send the purchased
	// column. We will need to fix that before uncommenting the code below
	// self.purchased =
	//	[[update valueForKey:@"purchased"] numberValueDecimal];
	self.promoSelectedCount = [[update valueForKey:kPromoSelectedCount] numberValueDecimal];
	self.promoAvailable = [[update valueForKey:kPromoAvailable] numberValueDecimal];
	self.imageId = [[update valueForKey:kImageId] numberValueDecimal];

	self.lifestyleCount =  [[update valueForKey:kLifestyleCount] numberValueDecimal];
	self.allergyCount =  [[update valueForKey:kAllergyCount] numberValueDecimal];
	self.ingredientCount =  [[update valueForKey:kIngredientCount] numberValueDecimal];

	self.calories = [[update objectForKey:kNutritionCalories] numberValueDecimal];
	self.protein = [[update objectForKey:kNutritionProtein] numberValueDecimal];
	self.totalCarbohydrates = [[update objectForKey:kNutritionTotalCarbohydrates] numberValueDecimal];
	self.totalFat = [[update objectForKey:kNutritionTotalFat] numberValueDecimal];
	self.saturatedFat = [[update objectForKey:kNutritionSaturatedFat] numberValueDecimal];
	self.sodium = [[update objectForKey:kNutritionSodium] numberValueDecimal];
	self.cholesterol = [[update objectForKey:kNutritionCholesterol] numberValueDecimal];
	self.caloriesFromFat = [[update objectForKey:kNutritionCaloriesFromFat] numberValueDecimal];

	// optional keys
	self.isRecommendation = @NO;
	self.recommendationDays = @0;

	// these pieces of data could be null
	if ((NSNull *)[update objectForKey:kBarcode] != [NSNull null]) {
		self.barcode = [update valueForKey:kBarcode];
	}

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)setWithRecommendationDictionary:(NSDictionary *)recommendation
{
	// required keys
	self.name = [recommendation valueForKey:kItemName];
	self.quantity = [[recommendation valueForKey:kQuantity] numberValueDecimal];
	self.productId = [[recommendation valueForKey:kProductId] numberValueDecimal];
	self.category = [CategoryMO categoryById: [[recommendation valueForKey:kCategoryId] numberValueDecimal]
		withManagedObjectContext:self.managedObjectContext];
	self.promoSelectedCount = [[recommendation valueForKey:kPromoSelectedCount] numberValueDecimal];
	self.promoAvailable = [[recommendation valueForKey:kPromoAvailable] numberValueDecimal];
	self.isRecommendation = [[recommendation valueForKey:kIsRecommendation] numberValueDecimal];
	self.recommendationDays = [[recommendation valueForKey:kRecommendationDays] numberValueDecimal];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)markForSync
{
	if ([self.isRecommendation boolValue]) {
		self.status = kStatusPut;
	}
	else if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId integerValue] == 0) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

+ (ShoppingListItem *)itemByCloudId:(NSNumber *)cloudId inMOC:(NSManagedObjectContext *)inMOC
{
	return [ShoppingListItem MR_findFirstByAttribute:ShoppingListItemAttributes.cloudId withValue:cloudId inContext:inMOC];
}

+ (ShoppingListItem *)itemByBarcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC;
{
	NSString *regex = [NSString stringWithFormat:@"0*%@", barcode];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:
		@"barcode MATCHES %@ AND status <> %@ AND accountId = %@", regex, kStatusDelete, [[User currentUser] accountId]];

	return [ShoppingListItem MR_findFirstWithPredicate:predicate inContext:inMOC];
}

+ (ShoppingListItem *)itemByName:(NSString *)name andProductId:(NSNumber *)productId
	inMOC:(NSManagedObjectContext *)inMOC
{
	return [ShoppingListItem MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:
		@"productId = %@ AND name = %@ AND status <> %@ AND purchased = NO AND accountId = %@", 
			productId, name, kStatusDelete, [[User currentUser] accountId]] inContext:inMOC];
}

+ (ShoppingListItem *)addOrIncrementItemNamed:(NSString *)name productId:(NSNumber *)productId
	category:(CategoryMO *)category barcode:(NSString *)barcode quantity:(NSUInteger)quantity
	inMOC:(NSManagedObjectContext *)inMOC
{
	// See if this exists already as a custom food
	CustomFoodMO *aCustomFood = [CustomFoodMO foodByName:name productId:productId barcode:barcode inMOC:inMOC];

	if (!productId) {
		if (aCustomFood) {
			productId = aCustomFood.cloudId;
		}
		else {
			productId = @0;
		}
	}

	// check for an existing shopping list item by productId and name
	ShoppingListItem *item = [ShoppingListItem itemByName:name andProductId:productId inMOC:inMOC];

	if (item) {
		item.quantity = [NSNumber numberWithInteger:[item.quantity integerValue] + quantity];
	}
	else {
		// Adding a new shopping list item
		item = [ShoppingListItem insertInManagedObjectContext:inMOC];

		item.name = name;
		item.quantity = [NSNumber numberWithInteger:quantity];
		item.accountId = [[User currentUser] accountId];
		item.productId = productId;
		item.barcode = barcode;
	}

	[item setCategoryUsingSuggestedCategory:category];

	// Add this as a custom food, if it isn't already a product or custom food
	if (!aCustomFood && ([productId integerValue] == 0)) {
		aCustomFood = [CustomFoodMO insertInManagedObjectContext:inMOC];
		aCustomFood.name = item.name;
		aCustomFood.barcode = item.barcode;
		aCustomFood.accountId = item.accountId;
		// NOTE:  This category may get changed asynchronously by the setCategoryUsingSuggestedCategory
		// method call above.
		aCustomFood.category = item.category;
		aCustomFood.status = kStatusPost;
	}
	item.customFood = aCustomFood;

	// Turn off the recommendation flag if it is set
	// TODO - tentatively adjusting the quantity down by 1, since recommendations
	// are implied at one already, and we want to 'replace' that with our new value
	if ([item.isRecommendation boolValue]) {
		item.isRecommendation = [NSNumber numberWithBool:NO];
		item.quantity = [NSNumber numberWithInteger:[item.quantity integerValue] - 1];
	}

	[item markForSync];

	return item;
}

+ (ShoppingListItem *)removeOrDecrementItemNamed:(NSString *)name productId:(NSNumber *)productId
	quantity:(NSUInteger)quantity inMOC:(NSManagedObjectContext *)inMOC
{
	if (!productId) {
		productId = @0;
	}

	// check for an existing item by productId and name
	ShoppingListItem *item = [ShoppingListItem itemByName:name andProductId:productId inMOC:inMOC];

	if (item) {
		if ([item.quantity integerValue] > quantity) {
			item.quantity = [NSNumber numberWithInteger:[item.quantity integerValue] - quantity];
			[item markForSync];
		}
		else {
			// Quantity going to or below zero, delete
			if ([item.status isEqualToString:kStatusPost]) {
				// We're posting, which implies the cloud doesn't know about this object,
				// just delete it instead
				[inMOC deleteObject:item];
			}
			else {
				item.status = kStatusDelete;
			}
		}

		[inMOC save:nil];
	}

	return item;
}

+ (BOOL)isDesiredCategory:(CategoryMO *)category
{
	return (category && ([category.cloudId integerValue] > 1));
}

- (void)setCategoryUsingSuggestedCategory:(CategoryMO *)suggestedCategory
{
	if (!self.category || ([self.category.cloudId integerValue] < 1)) {
		// Default the category to 'Other'
		self.category = [CategoryMO categoryById:@1 withManagedObjectContext:self.managedObjectContext];
	}

	// If we already have a category on this item, and it is not an undesirable value, use it
	if ([ShoppingListItem isDesiredCategory:self.category]) {
		// This also assumes that the storeCategory has already been established.  We're
		// not setting it here, as we didn't alter the item's category
		return;
	}

	// Check the purchase history to see if we can get a category from it
	PurchaseHxItem *phxItem = [PurchaseHxItem itemByName:self.name andProductId:self.productId
		inMOC:self.managedObjectContext];
	if (phxItem && [ShoppingListItem isDesiredCategory:phxItem.category]) {
		self.category = phxItem.category;
	}
	else {
		// Check the suggested category
		if ([ShoppingListItem isDesiredCategory:suggestedCategory]) {
			self.category = (id)[self.managedObjectContext existingObjectWithID:[suggestedCategory objectID] error:nil];
		}
		else {
			// Call the cloud to see if it has a category for this item
			//
			// NOTE:  We're going to go ahead and search the pantry now, just in case the
			// cloud check fails.  Since the cloud call is async, we'll do the pantry first, then
			// overwrite it with the cloud info if it comes back and is a desired value
			//
			// Check the pantry for this item to establish a category
			PantryItem *pantryItem = [PantryItem itemByName:self.name andProductId:self.productId
				inMOC:self.managedObjectContext];
			if (pantryItem && [ShoppingListItem isDesiredCategory:pantryItem.category]) {
				self.category = pantryItem.category;
			}

			// TODO Should we do this if this is a real product (productId <> 0)?
			BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
			if (appDelegate.connected) {
				NSURL *speculateCategoryURL = [BCUrlFactory productSpeculateCategoryForFreetextItem:self.name];

				GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:speculateCategoryURL];
				[fetcher beginFetchWithCompletionHandler:^(NSData * retrievedData, NSError * error) {
					if (error) {
						[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
					}
					else {
						// fetch succeeded
						NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

						NSNumber *speculatedCategoryId = [[resultsDict objectForKey:kCategoryId] numberValueDecimal];
						if (speculatedCategoryId) {
							CategoryMO *speculatedCategory = [CategoryMO categoryById:speculatedCategoryId
								withManagedObjectContext:self.managedObjectContext];

							if ([ShoppingListItem isDesiredCategory:speculatedCategory]) {
								self.category = speculatedCategory;
								StoreCategory *storeCategory = [[Store currentStore] storeCategoryForCategory:self.category];
								if (storeCategory) {
									self.storeCategory = (id)[self.managedObjectContext existingObjectWithID:[storeCategory objectID] error:nil];
								}
								else {
									self.storeCategory = nil;
								}
								if (self.customFood) {
									self.customFood.category = speculatedCategory;
								}
							}
						}

						[self.managedObjectContext BL_save];
					}
				}];
			}
		}
	}
	
	StoreCategory *storeCategory = [[Store currentStore] storeCategoryForCategory:self.category];
	if (storeCategory) {
		self.storeCategory = (StoreCategory *)[self.managedObjectContext objectWithID:[storeCategory objectID]];
	}
	else {
		self.storeCategory = nil;
	}

	if (self.customFood) {
		self.customFood.category = self.category;
	}
}

@end
