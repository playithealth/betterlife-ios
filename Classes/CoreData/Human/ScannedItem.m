#import "ScannedItem.h"

#import "User.h"

@implementation ScannedItem

+ (id)cacheScannedBarcode:(NSString *)barcode withName:(NSString *)name inMOC:(NSManagedObjectContext *)inMOC
{
	ScannedItem *scannedItem = [NSEntityDescription 
		insertNewObjectForEntityForName:[ScannedItem entityName] 
		inManagedObjectContext:inMOC];

	scannedItem.name = name;
	scannedItem.barcode = barcode;
	scannedItem.date = [NSDate date];

	// Save the context.
	NSError *error = nil;
	if (![inMOC save:&error])
	{
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	return scannedItem;
}

+ (id)cachedItemByBarcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC
{
	NSString *regex = [NSString stringWithFormat:@"0*%@", barcode];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:
		@"barcode MATCHES %@", regex];

	return [ScannedItem MR_findFirstWithPredicate:predicate inContext:inMOC];
}

@end
