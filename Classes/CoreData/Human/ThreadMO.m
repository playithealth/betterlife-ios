#import "ThreadMO.h"

#import "CommunityMO.h"
#import "NSDictionary+NumberValue.h"
#import "UserProfileMO.h"

@interface ThreadMO ()

@end


@implementation ThreadMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.cloudId = [update BC_numberForKey:kThreadId];
	self.name = [update BC_stringOrNilForKey:kName];
	self.type = [update valueForKey:kThreadType];
	self.unsubscribed = [update BC_numberForKey:kDeleted];
	self.readOn = [update BC_numberForKey:kReadOn];
	self.createdOn = [update BC_numberForKey:kCreatedOn]; ///< Subscribed to this thread on...
	self.updatedOn = [update BC_numberForKey:kUpdatedOn]; ///< Subscription updated on...
	self.threadUpdatedOn = [update BC_numberForKey:kThreadUpdatedOn]; ///< Thread updated on...

	NSNumber *communityId = [update BC_numberForKey:kCommunityId];
	self.community = [CommunityMO MR_findFirstByAttribute:CommunityMOAttributes.cloudId withValue:communityId
		inContext:self.managedObjectContext];

	self.userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	self.status = kStatusOk;
}

#pragma mark - sync to cloud
- (NSData *)toJSON
{
	// The only data to be sent for a thread is when it was read, and thus we only need to send these fields
	// NOTE that the readOn field should contain the syncUpdate timestamp that was current when the user read this thread, as that
	// is the best way to ensure that we communicate to the cloud which messages have been read within the thread
	// Name is included for future proofing so that if we implement POSTing a new thread on the mobiles
	NSDictionary *jsonDict = @{
		kThreadId : self.cloudId,
		kReadOn : (self.readOn ?: @0),
		kName : (self.name ?: @""),
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
