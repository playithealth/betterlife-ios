//
//  UserStoreMO.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/13/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "UserStoreMO.h"

#import "CategoryMO.h"
#import "NumberValue.h"
#import "StoreCategory.h"
#import "User.h"

NSString *const UserStoresDidChangeStoreCategories = @"UserStoresDidChangeStoreCategories";
NSString *const UserStoresDidChangeStoreId = @"storeId";

@implementation UserStoreMO
@synthesize distcalc;

- (void)didTurnIntoFault
{
	distcalc = nil;
}

- (void)setWithDictionary:(NSDictionary *)inStore
{
	self.accountId = [[User currentUser] accountId];
	self.cloudId = [[inStore valueForKey:kId] numberValueDecimal];
	self.storeEntityId = [[inStore valueForKey:kEntityId] numberValueDecimal];
	self.name = [inStore valueForKey:kStore];
	if ((NSNull *)[inStore objectForKey:kAddress] != [NSNull null]) {
		self.address = [inStore valueForKey:kAddress];
	}
	if ((NSNull *)[inStore objectForKey:kAddress2] != [NSNull null]) {
		self.address2 = [inStore valueForKey:kAddress2];
	}
	if ((NSNull *)[inStore objectForKey:kCity] != [NSNull null]) {
		self.city = [inStore valueForKey:kCity];
	}
	if ((NSNull *)[inStore objectForKey:kState] != [NSNull null]) {
		self.state = [inStore valueForKey:kState];
	}
	if ((NSNull *)[inStore objectForKey:kZip] != [NSNull null]) {
		self.zip = [inStore valueForKey:kZip];
	}
	if ((NSNull *)[inStore objectForKey:kPhone] != [NSNull null]) {
		self.phone = [inStore valueForKey:kPhone];
	}
	self.latitude = [[inStore valueForKey:kLatitude] numberValueDecimal];
	self.longitude = [[inStore valueForKey:kLongitude] numberValueDecimal];
	self.buyerCompassClient = [[inStore valueForKey:kBuyerCompassClient] numberValueDecimal];
	self.updatedOn = [[inStore valueForKey:kUpdatedOn] numberValueDecimal];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (NSData *)toJSON
{
	NSDictionary *storeDict = @ { kEntityId : self.storeEntityId, kDefaultStore :[NSNumber numberWithInt:0] };

	return [NSJSONSerialization dataWithJSONObject:storeDict options:kNilOptions error:nil];
}

- (NSData *)toJSONWithEntityData
{
	NSDictionary *storeDict =
		@ { kName : self.name,
			kAddress: (self.address ? self.address : @""),
			kCity: (self.city ? self.city : @""),
			kState: (self.state ? self.state : @""),
			@"zipcode" : (self.zip ? self.zip : @""),
			kPhone: (self.phone ? self.phone : @""),
			@"web" : @"",
			kEmail: @"",
			@"description" : @"",
			kLatitude: (self.latitude ? self.latitude : @""),
			kLongitude: (self.longitude ? self.longitude : @"") };
	NSDictionary *jsonDict =
		@ { kEntityId : self.storeEntityId,
			kDefaultStore :[NSNumber numberWithInt:0],
			@"store_data" : storeDict };

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

- (NSData *)toJSONWithCategories
{
	// Grab the categories first, sorted by the sortOrder
	NSArray *sortedCategories = [self.categories sortedArrayUsingDescriptors:
		@[[NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES]]];

	NSArray *categoryIds = [sortedCategories valueForKeyPath:@"category.cloudId"];

	NSDictionary *jsonDict = @ {
		kEntityId: self.storeEntityId,
		@"sorted_categories" : categoryIds
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

- (void)populateStoreCategories
{
	// get the count of the store categories for this store
	NSPredicate *storeCategoryPredicate = [NSPredicate predicateWithFormat:@"store.accountId = %@ and store.storeEntityId = %@",
		[[User currentUser] accountId], self.storeEntityId];
	NSUInteger storeCategoryCount = [StoreCategory MR_countOfEntitiesWithPredicate:storeCategoryPredicate inContext:self.managedObjectContext];

	// get the count of the non-store categories
	NSUInteger categoryCount = [CategoryMO MR_countOfEntitiesWithContext:self.managedObjectContext];

	// if there aren't any store categories for this store for this user add them
	if (storeCategoryCount == 0) {
		NSArray *categories = [CategoryMO MR_findAllSortedBy:CategoryMOAttributes.name ascending:YES inContext:self.managedObjectContext];

		int sortIndex = 0;
		for (CategoryMO *category in categories) {
			StoreCategory *storeCategory = [StoreCategory insertInManagedObjectContext:self.managedObjectContext];
			storeCategory.sortOrder = [NSNumber numberWithInt:sortIndex++];
			storeCategory.category = category;
			storeCategory.store = self;
		}
	}
	// there could be new categories to append to the end of the list
	else if (storeCategoryCount < categoryCount) {
		NSArray *storeCategories = [StoreCategory MR_findAllWithPredicate:storeCategoryPredicate inContext:self.managedObjectContext];

		NSArray *missingCats = [CategoryMO MR_findAllSortedBy:CategoryMOAttributes.name ascending:YES
			withPredicate:[NSPredicate predicateWithFormat:@"NOT (cloudId IN %@)", [storeCategories valueForKeyPath:@"category.cloudId"]]
			inContext:self.managedObjectContext];

		int sortIndex = [[[storeCategories objectAtIndex:0] sortOrder] intValue] + 1;
		for (CategoryMO *category in missingCats) {
			StoreCategory *storeCategory = [StoreCategory insertInManagedObjectContext:self.managedObjectContext];
			storeCategory.sortOrder = [NSNumber numberWithInt:sortIndex++];
			storeCategory.category = category;
			storeCategory.store = self;
		}
	}

	[self.managedObjectContext BL_save];
}

+ (id)storeByEntityId:(NSNumber *)storeEntityId inMOC:(NSManagedObjectContext *)inMOC
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@", UserStoreMOAttributes.storeEntityId, storeEntityId];

	return [UserStoreMO MR_findFirstWithPredicate:predicate inContext:inMOC];
}

- (NSNumber *)cloud_id
{
	return self.storeEntityId;
}

- (NSString *)store_name
{
	return self.name;
}

@end
