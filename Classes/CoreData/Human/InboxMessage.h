//
//  InboxMessage.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/13/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "_InboxMessage.h"

@interface InboxMessage : _InboxMessage {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (NSData *)toJSON;
@end
