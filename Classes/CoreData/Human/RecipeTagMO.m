#import "RecipeTagMO.h"


@interface RecipeTagMO ()

// Private interface goes here.

@end


@implementation RecipeTagMO

+ (id)tagByName:(NSString *)name withManagedObjectContext:(NSManagedObjectContext *)inMOC
{
	RecipeTagMO *returnTag = nil;
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = %@",
							  name];
	
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:[RecipeTagMO entityInManagedObjectContext:inMOC]];
	[request setPredicate:predicate];
	
	NSArray *results = [inMOC executeFetchRequest:request error:nil];
	if ([results count] > 0) {
		returnTag = [results objectAtIndex:0];
	}
	else {
		returnTag = [RecipeTagMO insertInManagedObjectContext:inMOC];
		returnTag.name = name;
	}
	
	return returnTag;
}

@end
