#import "_AdvisorActivityPlanTimelineMO.h"

#import "BLSyncEntity.h"

@interface AdvisorActivityPlanTimelineMO : _AdvisorActivityPlanTimelineMO <BLSyncEntity> {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
@end
