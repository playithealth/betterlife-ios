//
//  Promotion.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 4/8/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "_Promotion.h"

@interface Promotion : _Promotion {}
- (NSData *)toJSON;
@end
