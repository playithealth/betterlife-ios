//
// TrackingMO.h
// BettrLife Corporation
//
// Created by Sef Tarbell on 5/11/2012.
// Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "_TrackingMO.h"

typedef NS_ENUM(NSInteger, BCTrackingType) {
    BCTrackingTypeWeight = 1,
    BCTrackingTypeBodyFat = 2,
    BCTrackingTypeBloodPressure = 3,
    BCTrackingTypeBloodGlucose = 4,
    BCTrackingTypeHA1c = 5,
    BCTrackingTypeMyZone = 18,
};

extern const struct TrackingTypes {
	int weight;
	int bodyFat;
	int bloodPressure;
	int bloodGlucose;
	int hA1c;
	int myzone;
} TrackingTypes;

@interface TrackingMO : _TrackingMO {}
- (NSDictionary *)valueDictionary;
- (void)setValueDictionary:(NSDictionary *)valueDictionary;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)markForSync;
+ (NSNumber *)getCurrentWeightInMOC:(NSManagedObjectContext *)context;
+ (TrackingMO *)getLastTrackOfType:(NSInteger)type inMOC:(NSManagedObjectContext *)context;
- (id)getValueFromJSONForKey:(NSString *)key;
+ (TrackingMO *)addTrackOfType:(NSInteger)type onDate:(NSDate *)date withValueDictionary:(NSDictionary *)valueDictionary
inMOC:(NSManagedObjectContext *)context;
+ (TrackingMO *)addWeight:(NSNumber *)weight onDate:(NSDate *)date inMOC:(NSManagedObjectContext *)context;
- (NSData *)toJSON;

+ (NSArray *)keysForTrackingType:(BCTrackingType)trackingType;
+ (NSArray *)onePerDayForTrackingType:(BCTrackingType)trackingType numberOfDays:(NSInteger)numberOfDays usingCalendar:(NSCalendar *)calendar inMOC:(NSManagedObjectContext *)context;
+ (NSDictionary *)dailyAveragesForTrackingType:(BCTrackingType)trackingType numberOfDays:(NSInteger)numberOfDays usingCalendar:(NSCalendar *)calendar inMOC:(NSManagedObjectContext *)context;
+ (NSArray *)monthlyAveragesForTrackingType:(BCTrackingType)trackingType numberOfMonths:(NSInteger)numberOfMonths usingCalendar:(NSCalendar *)calendar inMOC:(NSManagedObjectContext *)context;
@end
