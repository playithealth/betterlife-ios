#import "ExerciseRoutineMO.h"

@implementation ExerciseRoutineMO

+ (id)exerciseRoutineById:(NSNumber *)cloudId inManagedObjectContext:(NSManagedObjectContext *)inMOC
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cloudId = %@", cloudId];

	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:[ExerciseRoutineMO entityInManagedObjectContext:inMOC]];
	[request setPredicate:predicate];

	NSError *error;
	NSArray *results = [inMOC executeFetchRequest:request error:&error];

	if ([results count] > 0) {
		return [results objectAtIndex:0];
	}
	
	return nil;
}

@end
