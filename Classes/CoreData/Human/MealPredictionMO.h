#import "_MealPredictionMO.h"

@interface MealPredictionMO : _MealPredictionMO {}
- (void)setWithRecommendationDictionary:(NSDictionary *)recommendation;
- (NSNumber *)itemId;
- (NSString *)itemType;
- (NSNumber *)imageId;
@end
