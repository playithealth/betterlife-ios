#import "_TrainingStrengthMO.h"

@interface TrainingStrengthMO : _TrainingStrengthMO {}
- (NSArray *)sortedSets;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)markForSync;
- (NSString *)setsDescription;
- (NSData *)toJSON;
@end
