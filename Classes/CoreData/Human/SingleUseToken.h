//
//  SingleUseToken.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/1/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "_SingleUseToken.h"

@interface SingleUseToken : _SingleUseToken {}
- (NSData *)toJSON;
@end
