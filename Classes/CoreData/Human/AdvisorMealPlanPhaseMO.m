#import "AdvisorMealPlanPhaseMO.h"

#import "AdvisorMealPlanEntryMO.h"
#import "NSDictionary+NumberValue.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "User.h"

@implementation AdvisorMealPlanPhaseMO

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	[super setWithUpdateDictionary:update];

	self.bmr = [update BC_numberOrNilForKey:kAdvisorMealPlanPhaseBMR];
	self.useAutoRatios = [update BC_numberForKey:kAdvisorMealPlanPhaseAutoRatio];
	self.proteinRatio = [update BC_numberOrNilForKey:kAdvisorMealPlanPhaseProteinRatio];
	self.totalCarbohydrateRatio = [update BC_numberOrNilForKey:kAdvisorMealPlanPhaseTotalCarbohydrateRatio];
	self.totalFatRatio = [update BC_numberOrNilForKey:kAdvisorMealPlanPhaseTotalFatRatio];

	NSArray *inEntries = [update objectForKey:kAdvisorPlanPhaseEntries];
	NSArray *entryIDs = [inEntries valueForKeyPath:kId];

	// nutrition min/max for this phase

	id minNutritionValue = [update objectForKey:kAdvisorMealPlanPhaseNutritionMinData];
	if ([minNutritionValue isKindOfClass:[NSDictionary class]]) {
		if (!self.minNutrition) {
			self.minNutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
		}
		[self.minNutrition setWithUpdateDictionary:minNutritionValue];
	}

	id maxNutritionValue = [update objectForKey:kAdvisorMealPlanPhaseNutritionMaxData];
	if ([maxNutritionValue isKindOfClass:[NSDictionary class]]) {
		if (!self.maxNutrition) {
			self.maxNutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
		}
		[self.maxNutrition setWithUpdateDictionary:maxNutritionValue];
	}

	// delete any entries that didn't come with the update
	/* TODO!!!
	[self.entries enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
		NSUInteger idx = [inSets indexOfObjectPassingTest:^(id searchObj, NSUInteger idx, BOOL *stop) {
			return [[searchObj BC_numberOrNilForKey:kId] isEqualToNumber:[obj cloudId]];
		}];
		if (idx == NSNotFound) {
			[self.managedObjectContext deleteObject:obj];
		}
	}];
	*/
	[self.entries enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
		if (![entryIDs containsObject:(AdvisorMealPlanEntryMO *)[obj cloudId]]) {
			[self.managedObjectContext deleteObject:obj];
		}
	}];

	User *thisUser = [User currentUser];

	for (NSDictionary *entryDict in inEntries) {
		NSNumber *cloudId = [entryDict BC_numberOrNilForKey:kAdvisorMealPlanPhaseEntryID];
		AdvisorMealPlanEntryMO *entryMO = [AdvisorMealPlanEntryMO MR_findFirstByAttribute:AdvisorMealPlanEntryMOAttributes.cloudId withValue:cloudId inContext:self.managedObjectContext];

		if (!entryMO) {
			entryMO = [AdvisorMealPlanEntryMO insertInManagedObjectContext:self.managedObjectContext];
			entryMO.cloudId = cloudId;
			entryMO.loginId = thisUser.loginId;
			
			entryMO.phase = self;
		}

		[entryMO setWithUpdateDictionary:entryDict];
	}
}

@end
