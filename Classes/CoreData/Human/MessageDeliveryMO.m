#import "MessageDeliveryMO.h"

#import "MessageMO.h"
#import "NSDictionary+NumberValue.h"
#import "User.h"

@interface MessageDeliveryMO ()

// Private interface goes here.

@end


@implementation MessageDeliveryMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.cloudId = [update BC_numberForKey:kMessageDeliveryId];
	self.receiverRm = [update BC_numberForKey:kMessageReceiverDeleted];
	self.senderRm = [update BC_numberForKey:kMessageSenderDeleted];
	self.loginId = [update BC_numberForKey:kUserId];
	self.isRead = [update BC_numberForKey:kMessageIsRead];
	self.updatedOn = [update BC_numberForKey:kUpdatedOn];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (NSData *)toJSON
{
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];

	[jsonDict setValue:self.cloudId forKey:kMessageDeliveryId];

	// Mark deleted if this user is either the sender or the receiver, and the appropriate flag is set
	if (([self.receiverRm boolValue] && [self.loginId isEqualToNumber:[[User currentUser] loginId]])
			|| ([self.senderRm boolValue] && [self.message.loginId isEqualToNumber:[[User currentUser] loginId]])) {

		[jsonDict setValue:kMarkDeleted forKey:kAction];
	}
	// read / unread
	else if ([self.isRead boolValue]) {
		[jsonDict setValue:kMarkRead forKey:kAction];
	}
	else {
		[jsonDict setValue:kMarkUnread forKey:kAction];
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
