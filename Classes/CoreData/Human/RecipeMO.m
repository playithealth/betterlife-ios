#import "RecipeMO.h"

#import "AdvisorGroup.h"
#import "MealIngredient.h"
#import "RecipeTagMO.h"
#import "NSDictionary+NumberValue.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "User.h"

@implementation RecipeMO

#pragma mark - virtual properties
- (NSString *)nutritionSummary
{
	[self willAccessValueForKey:@"nutritionSummary"];
	NSMutableString *summary = [[NSMutableString alloc] init];
	if (self.calories) {
		[summary appendFormat:@"calories:%@ ", self.calories];
	}
	if (self.caloriesFromFat) {
		[summary appendFormat:@"calories from fat:%@ ", self.caloriesFromFat];
	}
	if (self.fat) {
		[summary appendFormat:@"fat:%@g ", self.fat];
	}
	if (self.satfat) {
		[summary appendFormat:@"saturated fat:%@g ", self.satfat];
	}
	if (self.cholesterol) {
		[summary appendFormat:@"cholesterol:%@mg ", self.cholesterol];
	}
	if (self.sodium) {
		[summary appendFormat:@"sodium:%@mg ", self.sodium];
	}
	if (self.carbs) {
		[summary appendFormat:@"carbohydrates:%@g ", self.carbs];
	}
	if (self.protein) {
		[summary appendFormat:@"protein:%@g ", self.protein];
	}
	[self didAccessValueForKey:@"nutritionSummary"];
	return summary;
}

#pragma mark - convenience methods
+ (id)recipeById:(NSNumber *)mealId withManagedObjectContext:(NSManagedObjectContext *)inMOC
{
	return [RecipeMO MR_findFirstByAttribute:RecipeMOAttributes.cloudId withValue:mealId inContext:inMOC];
}

+ (id)mealByExternalId:(NSString *)externalId withManagedObjectContext:(NSManagedObjectContext *)inMOC
{
	return [RecipeMO MR_findFirstByAttribute:RecipeMOAttributes.externalId withValue:externalId inContext:inMOC];
}

+ (NSArray *)findAllByNameOrTag:(NSString *)searchTerm inContext:(NSManagedObjectContext *)inMOC
{
	User *thisUser = [User currentUser];
	NSMutableArray *allMatches = [NSMutableArray array];

	// search for any recipes that contain the search term
	NSPredicate *predicate = nil;
	if ([searchTerm length] <= 3) {
		// If the search term is short, do a begins with predicate
		predicate = [NSPredicate predicateWithFormat:@"%K BEGINSWITH[c] %@ AND status <> %@ AND accountId = %@", 
					  RecipeMOAttributes.name, searchTerm, kStatusDelete, thisUser.accountId];
	}
	else {
		// Longer search term, search the entire meal name with our term(s)
		predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@ AND status <> %@ AND accountId = %@", 
					  RecipeMOAttributes.name, searchTerm, kStatusDelete, thisUser.accountId];
	}
	[allMatches addObjectsFromArray:[RecipeMO MR_findAllWithPredicate:predicate inContext:inMOC]];

	// search for any recipes by tag names in the search term
	NSArray *wordsInSearch = [searchTerm componentsSeparatedByString:@" "];
	for (NSString *word in wordsInSearch) {
		RecipeTagMO *tag = [RecipeTagMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K ==[c] %@", RecipeTagMOAttributes.name, word] inContext:inMOC];
		if (tag) {
			for (RecipeMO *meal in tag.recipes) {
				if (![allMatches containsObject:meal]) {
					[allMatches addObject:meal];
				}
			}
		}
	}

	return allMatches;
}

- (void)setWithUpdateDictionary:(NSDictionary *)update 
{
	// required keys
	self.cloudId = [update BC_numberForKey:kId];
	self.name = [update valueForKey:kName];
	self.updatedOn = [update BC_numberForKey:kUpdatedOn];
	self.imageId = [update BC_numberOrNilForKey:kImageId];
	self.advisorId = [update BC_numberForKey:kRecipeAdvisorId];
	self.rating = [update BC_numberForKey:kRating];
	self.isOwner = [update BC_numberForKey:kRecipeIsOwner];

	id updateValue = nil;

	// optional keys
	self.removed = [update BC_numberOrNilForKey:kDeleted];

	if ((NSNull *)[update objectForKey:kRecipeExternalId] != [NSNull null]) {
		self.externalId = [update valueForKey:kRecipeExternalId];
	}

	if ((NSNull *)[update objectForKey:kRecipeURL] != [NSNull null]) {
		self.url = [update valueForKey:kRecipeURL];
	}

	if ((NSNull *)[update objectForKey:kWebRecipeImageURL] != [NSNull null]) {
		self.imageUrl = [update valueForKey:kWebRecipeImageURL];
	}

	id instructionsValue = update[kRecipeInstructions];
	if ((NSNull *)instructionsValue != [NSNull null]) {
		if ([instructionsValue isKindOfClass:[NSArray class]]) {
			self.instructions = [instructionsValue componentsJoinedByString:@"\n"];
		}
		else if ([instructionsValue isKindOfClass:[NSString class]]) {
			self.instructions = instructionsValue;
		}
	}

	self.serves = [update BC_numberOrNilForKey:kRecipeServes];
	self.cookTime = [update BC_numberOrNilForKey:kRecipeCookTime];
	self.prepTime = [update BC_numberOrNilForKey:kRecipePrepTime];

	if ((NSNull *)[update objectForKey:kRecipeCookTimeUnits] != [NSNull null]) {
		self.cookTimeUnit = [update valueForKey:kRecipeCookTimeUnits];
	}

	// Nutrients
	// TODO Remove these!
	self.calories = [update BC_numberOrNilForKey:NutritionMOAttributes.calories];
	self.caloriesFromFat = [update BC_numberOrNilForKey:NutritionMOAttributes.caloriesFromFat];
	self.carbs = [update BC_numberOrNilForKey:NutritionMOAttributes.totalCarbohydrates];
	self.cholesterol = [update BC_numberOrNilForKey:NutritionMOAttributes.cholesterol];
	self.fat = [update BC_numberOrNilForKey:NutritionMOAttributes.totalFat];
	self.satfat = [update BC_numberOrNilForKey:NutritionMOAttributes.saturatedFat];
	self.protein = [update BC_numberOrNilForKey:NutritionMOAttributes.protein];
	self.sodium = [update BC_numberOrNilForKey:NutritionMOAttributes.sodium];

	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}
	[self.nutrition setWithUpdateDictionary:update];

	/*
	self.transFat = [update BC_numberForKey:NutritionMOAttributes.transFat];
	self.saturatedFatCalories = [update BC_numberForKey:NutritionMOAttributes.saturatedFatCalories];
	self.polyunsaturatedFat = [update BC_numberForKey:NutritionMOAttributes.polyunsaturatedFat];
	self.monounsaturatedFat = [update BC_numberForKey:NutritionMOAttributes.monounsaturatedFat];
	self.potassium = [update BC_numberForKey:NutritionMOAttributes.potassium];
	self.otherCarbohydrates = [update BC_numberForKey:NutritionMOAttributes.otherCarbohydrates];
	self.dietaryFiber = [update BC_numberForKey:NutritionMOAttributes.dietaryFiber];
	self.solubleFiber = [update BC_numberForKey:NutritionMOAttributes.solubleFiber];
	self.insolubleFiber = [update BC_numberForKey:NutritionMOAttributes.insolubleFiber];
	self.sugars = [update BC_numberForKey:NutritionMOAttributes.sugars];
	self.sugarsAlcohol = [update BC_numberForKey:NutritionMOAttributes.sugarsAlcohol];
	self.vitaminAPercent = [update BC_numberForKey:NutritionMOAttributes.vitaminAPercent];
	self.vitaminCPercent = [update BC_numberForKey:NutritionMOAttributes.vitaminCPercent];
	self.calciumPercent = [update BC_numberForKey:NutritionMOAttributes.calciumPercent];
	self.ironPercent = [update BC_numberForKey:NutritionMOAttributes.ironPercent];
	*/


	if ((NSNull *)[update objectForKey:kRecipeAdvisorName] != [NSNull null]) {
		self.advisorName = [update valueForKey:kRecipeAdvisorName];
	}

	// Deal with advisor groups, need to determine if this group is already related to this meal,
	// and if not, associate the group
	updateValue = [update objectForKey:kAdvisorGroupId];
	if (updateValue && (updateValue != [NSNull null])) {
		AdvisorGroup *group = [AdvisorGroup advisorGroupById:[updateValue numberValueDecimal]
			withManagedObjectContext:self.managedObjectContext];

		// This should ALWAYS be true, or our advisor sync is not working or being called
		// in the wrong order in relation to this
		if (group) {
			// See if this group is already associated with this meal
			BOOL (^SameGroupId)(id, BOOL *) = ^BOOL(id obj, BOOL *stop) {
				AdvisorGroup *item = obj;
				return ([item.cloudId isEqualToNumber:group.cloudId]);
			};

			NSSet *matches = [self.advisorGroups objectsPassingTest:SameGroupId];

			if (!matches || ![matches count]) {
				[self addAdvisorGroupsObject:group];
			}
		}
	}

	// Delete ingredients (delete strategy)
	for (MealIngredient *ingredient in self.ingredients) {
		[self.managedObjectContext deleteObject:ingredient];
	}

	if ((NSNull *)[update objectForKey:kRecipeIngredients] != [NSNull null]) {
		NSInteger sortOrder = 0;
		for (NSDictionary *mealIngredientDict in [update objectForKey:kRecipeIngredients]) {
			MealIngredient *ingredient = [MealIngredient
				insertInManagedObjectContext:self.managedObjectContext];
			ingredient.recipe = self;

			// Update ingredient
			[ingredient setWithUpdateDictionary:mealIngredientDict]; 

			// set the status of this item to OK since it came from the API
			ingredient.status = kStatusOk;

			ingredient.sortOrder = [NSNumber numberWithInteger:sortOrder];
			sortOrder++;
		}
	}

	// "tags":["vegan","baking"]},
	NSMutableSet *tagsSet = [NSMutableSet set];
	for (NSString *tagName in [update objectForKey:@"tags"]) {
		RecipeTagMO *tag = [RecipeTagMO tagByName:tagName withManagedObjectContext:self.managedObjectContext];
		[tagsSet addObject:tag];
	}
	self.tags = tagsSet;
	
	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)setWithRecipeDictionary:(NSDictionary *)recipe
{
	self.externalId = [recipe objectForKey:kId];
	self.name = [recipe objectForKey:kRecipeName];
	self.url = [recipe objectForKey:kRecipeSource];
	self.imageId = [recipe BC_numberOrNilForKey:kRecipeImageId];
	
	// recipe image url comes in as a 0 sometimes
	id recipeValue = [recipe objectForKey:kRecipeImageURL];
	if ([recipeValue isKindOfClass:[NSString class]]) {
		self.imageUrl = [recipe objectForKey:kRecipeImageURL];
	}

	self.serves = [recipe BC_numberOrNilForKey:kRecipeServes];
	self.cookTime = [recipe BC_numberOrNilForKey:kRecipeCookTime];
	self.cookTimeUnit = [recipe BC_stringOrNilForKey:kRecipeCookTimeUnits];
	self.prepTime = [recipe BC_numberOrNilForKey:kRecipePrepTime];

	// careful handling of cases for instructions
	recipeValue = [recipe objectForKey:kRecipeInstructions];
	if ([recipeValue isKindOfClass:[NSString class]]) {
		self.instructions = recipeValue;
	}
	else if ([recipeValue isKindOfClass:[NSArray class]]) {
		self.instructions = [recipeValue componentsJoinedByString:@"\n"];
	}

	// TODO Remove these!
	self.calories = [recipe BC_numberOrNilForKey:NutritionMOAttributes.calories];
	self.caloriesFromFat = [recipe BC_numberOrNilForKey:NutritionMOAttributes.caloriesFromFat];
	self.carbs = [recipe BC_numberOrNilForKey:NutritionMOAttributes.totalCarbohydrates];
	self.cholesterol = [recipe BC_numberOrNilForKey:NutritionMOAttributes.cholesterol];
	self.fat = [recipe BC_numberOrNilForKey:NutritionMOAttributes.totalFat];
	self.satfat = [recipe BC_numberOrNilForKey:NutritionMOAttributes.saturatedFat];
	self.protein = [recipe BC_numberOrNilForKey:NutritionMOAttributes.protein];
	self.sodium = [recipe BC_numberOrNilForKey:NutritionMOAttributes.sodium];

	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}
	// If the nutrients are in a sub-array, use that, otherwise, look in the top level of the dictionary
	if ([recipe objectForKey:kMPNutrients]) {
		[self.nutrition setWithUpdateDictionary:[recipe objectForKey:kMPNutrients]];
	}
	else {
		[self.nutrition setWithUpdateDictionary:recipe];
	}

	/*
	self.transFat = [recipe BC_numberForKey:NutritionMOAttributes.transFat];
	self.saturatedFatCalories = [recipe BC_numberForKey:NutritionMOAttributes.saturatedFatCalories];
	self.polyunsaturatedFat = [recipe BC_numberForKey:NutritionMOAttributes.polyunsaturatedFat];
	self.monounsaturatedFat = [recipe BC_numberForKey:NutritionMOAttributes.monounsaturatedFat];
	self.potassium = [recipe BC_numberForKey:NutritionMOAttributes.potassium];
	self.otherCarbohydrates = [recipe BC_numberForKey:NutritionMOAttributes.otherCarbohydrates];
	self.dietaryFiber = [recipe BC_numberForKey:NutritionMOAttributes.dietaryFiber];
	self.solubleFiber = [recipe BC_numberForKey:NutritionMOAttributes.solubleFiber];
	self.insolubleFiber = [recipe BC_numberForKey:NutritionMOAttributes.insolubleFiber];
	self.sugars = [recipe BC_numberForKey:NutritionMOAttributes.sugars];
	self.sugarsAlcohol = [recipe BC_numberForKey:NutritionMOAttributes.sugarsAlcohol];
	self.vitaminAPercent = [recipe BC_numberForKey:NutritionMOAttributes.vitaminAPercent];
	self.vitaminCPercent = [recipe BC_numberForKey:NutritionMOAttributes.vitaminCPercent];
	self.calciumPercent = [recipe BC_numberForKey:NutritionMOAttributes.calciumPercent];
	self.ironPercent = [recipe BC_numberForKey:NutritionMOAttributes.ironPercent];
	*/
}

- (void)setWithUnifiedSearchDictionary:(NSDictionary *)recipe
{
	self.cloudId = [recipe BC_numberOrNilForKey:kUnifiedSearchItemID];
	self.accountId = [recipe BC_numberForKey:kUnifiedSearchAccountID];
	self.advisorId = [recipe BC_numberOrNilForKey:kUnifiedSearchAdvisorID];
	self.cookTime = [recipe BC_numberOrNilForKey:kUnifiedSearchCookTime];
	self.cookTimeUnit = [recipe BC_stringOrNilForKey:kUnifiedSearchCookTimeUnits];
	self.isOwner = [recipe BC_numberForKey:kUnifiedSearchIsOwner];
	self.prepTime = [recipe BC_numberOrNilForKey:kUnifiedSearchPrepTime];
	self.serves = [recipe BC_numberOrNilForKey:kUnifiedSearchServes];

	self.name = [recipe objectForKey:kUnifiedSearchName];
    if ([recipe objectForKey:kUnifiedSearchURL] != (id)[NSNull null]) {
        self.url = [recipe objectForKey:kUnifiedSearchURL];
    }
	self.imageId = [recipe BC_numberOrNilForKey:kUnifiedSearchImageID];

	// recipe image url comes in as a 0 sometimes
	id recipeValue = [recipe objectForKey:kUnifiedSearchImageURL];
	if ([recipeValue isKindOfClass:[NSString class]]) {
		self.imageUrl = [recipe objectForKey:kUnifiedSearchImageURL];
	}

	// the old 8 fields for nutrition, should phase these out
	self.calories = [recipe BC_numberOrNilForKey:NutritionMOAttributes.calories];
	self.caloriesFromFat = [recipe BC_numberOrNilForKey:NutritionMOAttributes.caloriesFromFat];
	self.carbs = [recipe BC_numberOrNilForKey:NutritionMOAttributes.totalCarbohydrates];
	self.cholesterol = [recipe BC_numberOrNilForKey:NutritionMOAttributes.cholesterol];
	self.fat = [recipe BC_numberOrNilForKey:NutritionMOAttributes.totalFat];
	self.satfat = [recipe BC_numberOrNilForKey:NutritionMOAttributes.saturatedFat];
	self.protein = [recipe BC_numberOrNilForKey:NutritionMOAttributes.protein];
	self.sodium = [recipe BC_numberOrNilForKey:NutritionMOAttributes.sodium];
}

- (void)setStatus:(NSString *)newStatus
{
	[self willChangeValueForKey:RecipeMOAttributes.status];
	if ([newStatus isEqualToString:kStatusDelete]) {
		self.removed = [NSNumber numberWithBool:YES];
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
			// If this meal has not yet gone to the cloud, it needs to, and that is a post
			// The POST verb on the api will still set the meal to deleted while adding it
			[self setPrimitiveValue:kStatusPost forKey:RecipeMOAttributes.status];
		}
		else {
			// If you had set this meal to PUT, due to making changes, and then deleted the meal,
			// you will lose those changes, as we're going to send this as a delete
			[self setPrimitiveValue:kStatusDelete forKey:RecipeMOAttributes.status];
		}
	}
	// If this is marked as a PUT, but we have no cloudId, it needs to be a POST instead
	else if ([self.status isEqualToString:kStatusPut]
			&& [self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
		[self setPrimitiveValue:kStatusPost forKey:RecipeMOAttributes.status];
	}
	else {
		// Just do the 'normal' action of setting the status to what was passed in
		[self setPrimitiveValue:newStatus forKey:RecipeMOAttributes.status];
	}
	[self didChangeValueForKey:@"status"];
}

- (void)setWithRecipeMO:(RecipeMO *)meal
{
	self.name = meal.name;
	self.accountId = meal.accountId;
	self.externalId = meal.externalId;
	self.url = meal.url;
	self.imageId = meal.imageId;
	self.imageUrl = meal.imageUrl;
	self.serves = meal.serves;
	self.cookTime = meal.cookTime;
	self.cookTimeUnit = meal.cookTimeUnit;
	self.instructions = meal.instructions;
	self.rating = meal.rating;
	self.isOwner = meal.isOwner;

	// TODO Remove these, as they should no longer be used, since we have .nutrition
	self.calories = meal.calories;
	self.protein = meal.protein;
	self.carbs = meal.carbs;
	self.fat = meal.fat;
	self.satfat = meal.satfat;
	self.sodium = meal.sodium;
	self.cholesterol = meal.cholesterol;
	self.caloriesFromFat = meal.caloriesFromFat;

	if (meal.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
		[self.nutrition copyFromNutritionMO:meal.nutrition];
	}

	MealIngredient *newIngredient = nil;
	for (MealIngredient *ingredient in meal.ingredients) {
		newIngredient = [MealIngredient insertInManagedObjectContext:self.managedObjectContext];
		[self addIngredientsObject:newIngredient];
		[newIngredient setWithMealIngredient:ingredient];
	}
}

- (NSString *)tagsString
{
	NSSortDescriptor *sortByName = [NSSortDescriptor
		sortDescriptorWithKey:@"name" ascending:YES
		selector:@selector(caseInsensitiveCompare:)];
	NSArray *sortedTags = [self.tags sortedArrayUsingDescriptors:
		[NSArray arrayWithObject:sortByName]];

	return [[sortedTags valueForKeyPath:@"name"] componentsJoinedByString:@","];	
}

#pragma mark - JSON
- (NSData *)toJSON
{
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] 
		initWithObjectsAndKeys:
			self.name, kName,
			nil];

	if (self.cloudId) {
		[jsonDict setValue:self.cloudId forKey:kRecipeId];
	}
	if (self.removed) {
		// This looks wierd, but we need this to NOT be a boolean NSNumber or it may go out as true/false
		[jsonDict setValue:[NSNumber numberWithInteger:[self.removed integerValue]] forKey:kDeleted];
	}
	if (self.externalId) {
		[jsonDict setValue:self.externalId forKey:kRecipeExternalId];
	}
	if (self.calories) {
		[jsonDict setValue:self.calories forKey:kNutritionCalories];
	}
	if (self.cookTime) {
		[jsonDict setValue:self.cookTime forKey:kRecipeCookTime];
	}
	if (self.cookTimeUnit) {
		[jsonDict setValue:self.cookTimeUnit forKey:kRecipeCookTimeUnits];
	}
	if (self.imageId) {
		[jsonDict setValue:self.imageId forKey:kImageId];
	}
	if (self.imageUrl) {
		[jsonDict setValue:self.imageUrl forKey:kWebRecipeImageURL];
	}
	if (self.instructions) {
		[jsonDict setValue:self.instructions forKey:kRecipeInstructions];
	}
	else {
		[jsonDict setValue:@"" forKey:kRecipeInstructions];
	}
	if (self.rating) {
		[jsonDict setValue:self.rating forKey:kRating];
	}
	if (self.serves) {
		[jsonDict setValue:self.serves forKey:kRecipeServes];
	}
	if (self.url) {
		[jsonDict setValue:self.url forKey:kRecipeURL];
	}
	if (self.isOwner) {
		[jsonDict setValue:self.isOwner forKey:kRecipeIsOwner];
	}

	[jsonDict setValue:(self.protein ? self.protein : [NSNull null])
		forKey:kNutritionProtein];

	[jsonDict setValue:(self.carbs ? self.carbs : [NSNull null])
		forKey:kNutritionTotalCarbohydrates];

	[jsonDict setValue:(self.fat ? self.fat : [NSNull null])
		forKey:kNutritionTotalFat];

	[jsonDict setValue:(self.satfat ? self.satfat : [NSNull null])
		forKey:kNutritionSaturatedFat];

	[jsonDict setValue:(self.sodium ? self.sodium : [NSNull null])
		forKey:kNutritionSodium];

	[jsonDict setValue:(self.cholesterol ? self.cholesterol : [NSNull null])
		forKey:kNutritionCholesterol];

	[jsonDict setValue:(self.caloriesFromFat ? self.caloriesFromFat : [NSNull null])
		forKey:kNutritionCaloriesFromFat];

	if ([self.ingredients count]) {
		NSMutableArray *ingredientsArray = [[NSMutableArray alloc]
			initWithCapacity:[self.ingredients count]];

		NSArray *sortedIngredients = [self.ingredients
			sortedArrayUsingDescriptors:[NSArray arrayWithObject:
			[NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES]]];
		for (MealIngredient *ingredient in sortedIngredients) {
			[ingredientsArray addObject:[ingredient toDictionary]];
		}
		[jsonDict setValue:ingredientsArray forKey:kRecipeIngredients];
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
