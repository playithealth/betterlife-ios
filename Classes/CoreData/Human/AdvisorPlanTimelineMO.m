#import "AdvisorPlanTimelineMO.h"

#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"

@interface AdvisorPlanTimelineMO ()
@end

@implementation AdvisorPlanTimelineMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.startDate = [NSDate dateWithTimeIntervalSince1970:[[update valueForKey:kAdvisorPlanStartDate] integerValue]];
	self.stopDate = [NSDate dateWithTimeIntervalSince1970:[[update valueForKey:kAdvisorPlanStopDate] integerValue]];
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
		<#kConstName#> : self.startDate,
		<#kConstName#> : self.stopDate,
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
