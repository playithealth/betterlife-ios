#import "TrackingValueMO.h"

#import "NSDictionary+NumberValue.h"

@interface TrackingValueMO ()
@end


@implementation TrackingValueMO
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.field = [update valueForKey:kTrackingValueField];
	self.dataType = [update valueForKey:kTrackingValueDataType];
	self.title = [update valueForKey:kTitle];
	self.units = [update valueForKey:kUnits];
	self.unitAbbrev = [update valueForKey:kTrackingValueUnitAbbrev];

	self.precision = [update BC_numberOrNilForKey:kTrackingValuePrecision];
	self.defaultValue = [update BC_numberOrNilForKey:kTrackingValueDefault];
	self.rangeMin = [update BC_numberOrNilForKey:kTrackingValueRangeMin];
	self.rangeMax = [update BC_numberOrNilForKey:kTrackingValueRangeMax];
	self.optional = [update BC_numberForKey:kTrackingValueOptional];
}
@end
