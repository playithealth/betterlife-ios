#import "_AdvisorPlanMO.h"

#import "BLSyncEntity.h"

@interface AdvisorPlanMO : _AdvisorPlanMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update withPhaseClass:(Class)phaseClass;
+ (id)getPlanForClass:(Class)planClass forDate:(NSDate *)date inContext:(NSManagedObjectContext *)context;
- (id)getTimelineForDate:(NSDate *)date;
- (id)getPhaseForDate:(NSDate *)date;
@end
