#import "_LoggingNoteMO.h"

@interface LoggingNoteMO : _LoggingNoteMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)setStatus:(NSString *)newStatus;
- (NSData *)toJSON;
@end
