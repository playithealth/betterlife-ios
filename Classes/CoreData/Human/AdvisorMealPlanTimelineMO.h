#import "_AdvisorMealPlanTimelineMO.h"

#import "BLSyncEntity.h"

@interface AdvisorMealPlanTimelineMO : _AdvisorMealPlanTimelineMO <BLSyncEntity> {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
@end
