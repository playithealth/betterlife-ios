#import "CoachInviteMO.h"

#import "NSDictionary+NumberValue.h"

@interface CoachInviteMO ()

// Private interface goes here.

@end


@implementation CoachInviteMO

#pragma mark - set with...
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.code = [update valueForKey:kInviteCode];
	self.loginId = [update BC_numberForKey:kUserId];

	self.createdOn = [update BC_numberOrNilForKey:kCreatedOn];
	self.advisorId = [update BC_numberOrNilForKey:kAdvisorId];
	if ((NSNull *)[update objectForKey:kAdvisorName] != [NSNull null]) {
		self.advisorName = [update valueForKey:kAdvisorName];
	}
	if ((NSNull *)[update objectForKey:kEntityName] != [NSNull null]) {
		self.rootEntityName = [update valueForKey:kEntityName];
	}

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

#pragma mark - JSON
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{ kInviteCode : self.code };

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
