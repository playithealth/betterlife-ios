#import "_HelpView.h"

@interface HelpView : _HelpView {}

+ (id)helpViewByName:(NSString *)name inMOC:(NSManagedObjectContext *)inMOC;

@end
