#import "MealPredictionMO.h"

#import "AdvisorMealPlanTagMO.h"
#import "CustomFoodMO.h"
#import "NSDictionary+NumberValue.h"
#import "NutritionMO.h"
#import "RecipeMO.h"

@interface MealPredictionMO ()

// Private interface goes here.

@end


@implementation MealPredictionMO

#pragma mark - convenience methods
- (void)setWithRecommendationDictionary:(NSDictionary *)recommendation
{
	self.usageCount = [recommendation BC_numberForKey:@"count"];
	self.item_id = [recommendation BC_numberForKey:@"item_id"];
	self.item_type = [recommendation objectForKey:@"item_type"];
	self.logTime = [recommendation BC_numberForKey:@"logtime"];
	self.name = [recommendation objectForKey:@"name"];
	self.image_id = [recommendation BC_numberForKey:@"image_id"];
	self.servings = [recommendation BC_numberForKey:@"multiplier"];

	if ((NSNull *)[recommendation objectForKey:@"serving_size_text"] != [NSNull null]) {
		self.serving_size_text = [recommendation objectForKey:@"serving_size_text"];
	}

	// careful handling of this key since it appears to be somewhat broken
	id servingUnitsValue = [recommendation objectForKey:@"serving_size_uom"];
	if ([servingUnitsValue isKindOfClass:[NSString class]]) {
  		if ([servingUnitsValue isEqualToString:@"null"]) {
			self.serving_size_uom = @"serving";
		}
		else {
			self.serving_size_uom = servingUnitsValue;
		}
	}

	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}
	[self.nutrition setWithUpdateDictionary:recommendation];

	// If this is a recipe, then it should be a subscribed recipe (predictions are based upon past items logged), so attach it
	if ([self.item_type isEqualToString:kItemTypeRecipe] && self.item_idValue) {
		self.recipe = [RecipeMO recipeById:self.item_id withManagedObjectContext:self.managedObjectContext];
	}

	// If this is a product, and has an account_id, then it should be a custom food, so attach it
	if ([self.item_type isEqualToString:kItemTypeProduct] && self.item_idValue && [recommendation BC_numberOrNilForKey:kAccountId]) {
		self.customFood = [CustomFoodMO MR_findFirstByAttribute:CustomFoodMOAttributes.cloudId
			withValue:self.item_id inContext:self.managedObjectContext];
	}

	// If this has a tag_id, then we might have the tag, so attach it
	NSNumber *tagId = [recommendation BC_numberOrNilForKey:kMPTagID];
	if (tagId) {
		self.tag = [AdvisorMealPlanTagMO MR_findFirstByAttribute:AdvisorMealPlanTagMOAttributes.cloudId
			withValue:tagId inContext:self.managedObjectContext];
	}

	NSSet *oldSides = self.sides;
	[oldSides enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
		[self.managedObjectContext deleteObject:obj];
	}];

	for (NSDictionary *sideDict in [recommendation objectForKey:@"Sides"]) {
		NSString *item_type = [recommendation objectForKey:@"item_type"];
		NSNumber *item_id = [sideDict BC_numberOrNilForKey:@"item_id"];
		NSSet *matches = [self.sides objectsPassingTest:^BOOL(id obj, BOOL *stop) {
			MealPredictionMO *prediction = obj;
			return ([prediction.item_id isEqualToNumber:item_id] && [prediction.item_type isEqualToString:item_type]);
		}];
		MealPredictionMO *sideMO = nil;
		if ([matches count]) {
			sideMO = [matches anyObject];
		}
		else {
			sideMO = [MealPredictionMO insertInManagedObjectContext:self.managedObjectContext];
			sideMO.login_id = self.login_id;
		}
		sideMO.date = self.date;

		[sideMO setWithRecommendationDictionary:sideDict];

		sideMO.entree = self;
	}

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

#pragma mark - virtual properties
- (NSNumber *)itemId
{
	return self.item_id;
}

- (NSString *)itemType
{
	return self.item_type;
}

- (NSNumber *)imageId
{
	return self.image_id;
}

@end
