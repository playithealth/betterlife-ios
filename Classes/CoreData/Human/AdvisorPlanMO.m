#import "AdvisorPlanMO.h"

#import "AdvisorPlanPhaseMO.h"
#import "AdvisorPlanTimelineMO.h"
#import "NSCalendar+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"
#import "User.h"

@interface AdvisorPlanMO ()
@end

@implementation AdvisorPlanMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update withPhaseClass:(Class)phaseClass
{
	self.cloudId = [update BC_numberForKey:kId];
	self.planName = [update BC_stringOrNilForKey:kAdvisorPlanName];
	// Variation between Meal Plan and Activity Plan
	if ([update objectForKey:kAdvisorMealPlanAdvisorId]) {
		self.advisorId = [update BC_numberOrNilForKey:kAdvisorMealPlanAdvisorId];
	}
	else {
		self.advisorId = [update BC_numberOrNilForKey:kAccountId];
	}
	self.advisorName = [update BC_stringOrNilForKey:kAdvisorName];
	if ([update objectForKey:kAdvisorPlanDescription] != [NSNull null]) {
		self.planDescription = [update BC_stringOrNilForKey:kAdvisorPlanDescription];
	}
	self.isRepeating = [update BC_numberForKey:kAdvisorPlanIsRepeating];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;

	NSArray *inPhases = [update objectForKey:kAdvisorPlanPhases];

	// delete any phases that didn't come with the update
	[self.phases enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
		// The phaseIDs may come in as strings, and thus won't directly match the MO phases, so need
		// to do some extra work here so that they aren't deleted unnecessarily
		BOOL found = NO;
		for (NSDictionary *phaseDict in inPhases) {
			NSNumber *cloudId = [phaseDict BC_numberForKey:kId];
			if ([cloudId integerValue] == [[(AdvisorPlanPhaseMO *)obj cloudId] integerValue]) {
				found = YES;
			}
		}
		if (!found) {
			[self.managedObjectContext deleteObject:obj];
		}
	}];

	User *thisUser = [User currentUser];
	for (NSDictionary *phaseDict in inPhases) {
		NSNumber *cloudId = [phaseDict BC_numberForKey:kId];
		AdvisorPlanPhaseMO *phaseMO = [phaseClass MR_findFirstByAttribute:AdvisorPlanMOAttributes.cloudId withValue:cloudId
			inContext:self.managedObjectContext];

		if (!phaseMO) {
			phaseMO = [phaseClass insertInManagedObjectContext:self.managedObjectContext];
			phaseMO.cloudId = cloudId;
			phaseMO.loginId = thisUser.loginId;
			
			phaseMO.plan = self;
		}

		[phaseMO setWithUpdateDictionary:phaseDict];
	}
}

+ (id)getPlanForClass:(Class)planClass forDate:(NSDate *)date inContext:(NSManagedObjectContext *)context
{
	id currentPlan = nil;

	User *thisUser = [User currentUser];
	NSPredicate *loginPredicate = [NSPredicate predicateWithFormat:@"%K = %@", AdvisorPlanMOAttributes.loginId, thisUser.loginId];
	NSArray *usersPlans = [planClass MR_findAllWithPredicate:loginPredicate inContext:context];

	// Timeline data changed Summer of 2014 so that the start and stop dates are no longer 'normalized', and are actually the time the
	// user started/stopped the plan instead. Thus we need to adjust the comparison date to be at the end of the target date in order to
	// work properly
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDate *endOfTargetDate = [calendar BC_endOfDate:date];

	for (AdvisorPlanMO *plan in usersPlans) {
		NSArray *sortedTimelines = [plan.timelines sortedArrayUsingDescriptors:
			@[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:NO],
			[NSSortDescriptor sortDescriptorWithKey:@"stopDate" ascending:NO]]];
		for (AdvisorPlanTimelineMO *timeline in sortedTimelines) {
			// if the start date is in the past and the stop date is in the future, then this plan is current
			if ([timeline.startDate timeIntervalSinceDate:endOfTargetDate] <= 0
					&& [timeline.stopDate timeIntervalSinceDate:endOfTargetDate] > 0) {
				currentPlan = plan;
				break;
			}
		}
	}

	return currentPlan;
}

- (id)getTimelineForDate:(NSDate *)date
{
	id currentTimeline = nil;

	// Timeline data changed Summer of 2014 so that the start and stop dates are no longer 'normalized', and are actually the time the
	// user started/stopped the plan instead. Thus we need to adjust the comparison date to be at the end of the target date in order to
	// work properly
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDate *endOfTargetDate = [calendar BC_endOfDate:date];

	NSArray *sortedTimelines = [self.timelines sortedArrayUsingDescriptors:
		@[[NSSortDescriptor sortDescriptorWithKey:AdvisorPlanTimelineMOAttributes.startDate ascending:NO],
		[NSSortDescriptor sortDescriptorWithKey:AdvisorPlanTimelineMOAttributes.stopDate ascending:NO]]];
	for (AdvisorPlanTimelineMO *timeline in sortedTimelines) {
		// if today is after the start date and before the stop date
		if ([timeline.startDate timeIntervalSinceDate:endOfTargetDate] <= 0
				&& [timeline.stopDate timeIntervalSinceDate:endOfTargetDate] > 0) {
			currentTimeline = timeline;
			break;
		}
	}

	return currentTimeline;
}

- (id)getPhaseForDate:(NSDate *)date
{
	id currentPhase = nil;

	AdvisorPlanTimelineMO *currentTimeline = [self getTimelineForDate:date];
	NSDate *startDate = [currentTimeline startDate];
	NSArray *sortedPhases = [self.phases sortedArrayUsingDescriptors:@[[NSSortDescriptor
		sortDescriptorWithKey:AdvisorPlanPhaseMOAttributes.phaseNumber ascending:YES]]];
	
	NSDate *phaseStartDate = startDate;

	// Timeline data changed Summer of 2014 so that the start and stop dates are no longer 'normalized', and are actually the time the
	// user started/stopped the plan instead. Thus we need to adjust the comparison date to be at the end of the target date in order to
	// work properly
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDate *endOfTargetDate = [calendar BC_endOfDate:date];

	while (!currentPhase && self.isRepeating) {
		for (AdvisorPlanPhaseMO *phase in sortedPhases) {
			NSDate *phaseEndDate = [calendar BC_dateByAddingDays:[phase.durationInDays integerValue] toDate:phaseStartDate];
			// if starts before today and ends after today
			if ([phaseStartDate timeIntervalSinceDate:endOfTargetDate] <= 0
					&& [phaseEndDate timeIntervalSinceDate:endOfTargetDate] > 0) {
				currentPhase = phase;
				break;
			}
			else {
				// prepare for the next phase
				phaseStartDate = phaseEndDate;
				phaseEndDate = nil;
			}
		}
	}

	return currentPhase;
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
		<#kConstName#> : self.advisorId,
		<#kConstName#> : self.advisorName,
		<#kConstName#> : self.cloudId,
		<#kConstName#> : self.isNew,
		<#kConstName#> : self.isRepeating,
		<#kConstName#> : self.loginId,
		<#kConstName#> : self.planDescription,
		<#kConstName#> : self.planName,
		<#kConstName#> : self.status,
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
