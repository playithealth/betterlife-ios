#import "_TrainingActivityMO.h"

#define kTimeInfoHours		@"hours"
#define kTimeInfoMinutes	@"minutes"
#define kTimeInfoSeconds	@"seconds"

// NOTE: This should probably be something like BLExternalSource or somesuch, as these sources are used in other MOs
typedef NS_ENUM(NSInteger, BLActivitySource) {
	BLActivitySourceFitBit = 1,
	BLActivitySourceMapMyFitness = 8,
	BLActivitySourceWithingsSteps = 10,
	BLActivitySourceMyZone = 16,
};

@interface TrainingActivityMO : _TrainingActivityMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)markForSync;
+ (double)calculateFactorForTime:(double)timeInMinutes andDistance:(double)distanceInMiles;
- (NSNumber *)calculateCalories;
- (NSDictionary *)timeInfo;
- (NSData *)toJSON;
- (NSNumber *)timeInMinutes;
- (NSString *)timeString;
@end
