#import "AdvisorActivityPlanWorkoutMO.h"

#import "AdvisorActivityPlanWorkoutActivityMO.h"
#import "NSDictionary+NumberValue.h"
#import "User.h"

@interface AdvisorActivityPlanWorkoutMO ()

// Private interface goes here.

@end

@implementation AdvisorActivityPlanWorkoutMO

#pragma mark - enum conversions
- (PerformOperator)performOpFromString:(NSString *)cloudValue
{
	PerformOperator performOperator = PerformAll;

	if ([cloudValue isEqualToString:@"all"]) {
		performOperator = PerformAll;
	}
	else if ([cloudValue isEqualToString:@"at_least"]) {
		performOperator = PerformAtLeast;
	}
	else if ([cloudValue isEqualToString:@"at_most"]) {
		performOperator = PerformAtMost;
	}
	else if ([cloudValue isEqualToString:@"exactly"]) {
		performOperator = PerformExactly;
	}

	return performOperator;
}

- (WorkoutTime)workoutTimeFromString:(NSString *)cloudValue
{
	WorkoutTime workoutTime = WorkoutTimeAny;

	if ([cloudValue isEqualToString:@"anytime"]) {
		workoutTime = WorkoutTimeAny;
	}
	return workoutTime;
}

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.numberToPerform = [update BC_numberForKey:kAdvisorActivityPlanWorkoutNumberToPerform];
	self.performOp = @([self performOpFromString:[update objectForKey:kAdvisorActivityPlanWorkoutPerformOperator]]);
	self.workoutTime = @([self workoutTimeFromString:[update objectForKey:kAdvisorActivityPlanWorkoutTime]]);
	self.workoutName = [update BC_stringOrNilForKey:kName];

	NSArray *inActivities = [update objectForKey:kAdvisorActivityPlanWorkoutActivities];

	// delete any activities that didn't come with the update
	[self.activities enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
		NSUInteger idx = [inActivities indexOfObjectPassingTest:^(id searchObj, NSUInteger idx, BOOL *stop) {
			return [[searchObj BC_numberOrNilForKey:kId] isEqualToNumber:[obj cloudId]];
		}];
		if (idx == NSNotFound) {
			[self.managedObjectContext deleteObject:obj];
		}
	}];

	NSUInteger sortOrder = 0;
	for (NSDictionary *activityDict in inActivities) {
		NSNumber *cloudId = [activityDict BC_numberForKey:kId];
		AdvisorActivityPlanWorkoutActivityMO *activityMO = [AdvisorActivityPlanWorkoutActivityMO
			MR_findFirstByAttribute:AdvisorActivityPlanWorkoutMOAttributes.cloudId withValue:cloudId inContext:self.managedObjectContext];

		if (!activityMO) {
			activityMO = [AdvisorActivityPlanWorkoutActivityMO insertInManagedObjectContext:self.managedObjectContext];
			activityMO.cloudId = cloudId;
			activityMO.loginId = [User loginId];
			
			activityMO.workout = self;
		}

		[activityMO setWithUpdateDictionary:activityDict];
		activityMO.sortOrder = @(sortOrder);
		sortOrder++;
	}

}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
