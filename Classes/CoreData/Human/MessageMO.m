#import "MessageMO.h"

#import "ConversationMO.h"
#import "MessageDeliveryMO.h"
#import "NSDictionary+NumberValue.h"
#import "User.h"

@interface MessageMO ()

@end


@implementation MessageMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.loginId = [update BC_numberForKey:kUserId];
	self.cloudId = [update BC_numberForKey:kMessageId];
	self.subject = [update valueForKey:kMessageSubject];
	self.body = [update valueForKey:kMessageBody];
	self.createdOn = [update BC_numberForKey:kCreatedOn];
	self.updatedOn = [update BC_numberForKey:kUpdatedOn];

	// Determine if the logged in user is the sender of this message, we'll use it later for conversations
	BOOL isSender = [[[User currentUser] loginId] isEqualToNumber:[update BC_numberForKey:kUserId]];

	// The update dictionary coming into this method contains the message, with a sub-array under the key 'to_users' with
	// all recipients of this message. If the logged in user is the recipient, there will only be one element in this array, otherwise,
	// there could be many, each needing its own message delivery MO
	for (NSDictionary *recipientDict in [update objectForKey:kMessageToUsers]) {
		// See if this delivery exists
		MessageDeliveryMO *deliveryMO = [MessageDeliveryMO MR_findFirstByAttribute:MessageDeliveryMOAttributes.cloudId
			withValue:[recipientDict valueForKey:kMessageDeliveryId] inContext:self.managedObjectContext];

		if (!deliveryMO) {
			deliveryMO = [MessageDeliveryMO insertInManagedObjectContext:self.managedObjectContext];
			[self addDeliveriesObject:deliveryMO];
		}

		[deliveryMO setWithUpdateDictionary:recipientDict];

		// See if a conversation with this person exists
		NSNumber *conversationUserId = (isSender ? [recipientDict BC_numberForKey:kUserId] : [update BC_numberForKey:kUserId]);

		ConversationMO *conversationMO = [ConversationMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@",
			ConversationMOAttributes.loginId, [[User currentUser] loginId], ConversationMOAttributes.conversationUserId, conversationUserId]
			inContext:self.managedObjectContext];

		BOOL createdConversation = NO;
		if (!conversationMO) {
			createdConversation = YES;
			conversationMO = [ConversationMO insertInManagedObjectContext:self.managedObjectContext];
		}

		[conversationMO setWithUpdateDictionary:(isSender ? recipientDict : update)];
		if (!createdConversation) {
			// Unsure of the placement of this, however, it is designed to make sure the fetched property 'newestMessage' gets refreshed properly
			[self.managedObjectContext BL_save];
			[self.managedObjectContext refreshObject:conversationMO mergeChanges:NO];
		}
		deliveryMO.conversation = conversationMO;

		if (!isSender && ([[recipientDict BC_numberOrNilForKey:kMessageIsRead] boolValue] == NO)) {
			// Update the conversation isRead flag
			conversationMO.isRead = @(NO);
		}
	}

}

- (NSData *)toJSON
{
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];


	[jsonDict setValue:[NSNull null] forKey:kMessageSubject];
	[jsonDict setValue:self.body forKey:kMessageText];
	//[jsonDict setValue:self.isPrivate forKey:kMessageIsPrivate];
	[jsonDict setValue:[[self.deliveries valueForKeyPath:@"loginId"] allObjects] forKey:kMessageDeliveryList];
	//[jsonDict setValue:self.replyTo.cloudId forKey:kMessageReplyId];

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}


@end
