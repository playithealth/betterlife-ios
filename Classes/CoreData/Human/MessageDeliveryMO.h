#import "_MessageDeliveryMO.h"

@interface MessageDeliveryMO : _MessageDeliveryMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (NSData *)toJSON;
@end
