#import "AdvisorActivityPlanTimelineMO.h"

#import "AdvisorActivityPlanMO.h"
#import "BCUrlFactory.h"
#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"
#import "User.h"

@interface AdvisorActivityPlanTimelineMO ()

// Private interface goes here.

@end

@implementation AdvisorActivityPlanTimelineMO

+ (NSString *)syncName {
	return kSyncAdvisorActivityPlans;
}

+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict {
	return [updateDict BC_numberForKey:kAdvisorActivityPlanId];
}

+ (NSString *)statusString {
	return kSyncUpdateHumanReadable_ActivityPlans;
}

+ (NSURL *)urlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit {
	return [BCUrlFactory activityPlanTimelineURL];
}

+ (NSPredicate *)userPredicate {
	return [NSPredicate predicateWithFormat:@"%K.%K = %@", AdvisorPlanTimelineMORelationships.plan,
		AdvisorPlanMOAttributes.loginId, [User loginId]];
}

#pragma mark - sync from cloud
+ (void)setWithUpdatesArray:(NSArray *)updates forLogin:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)inMOC
{
	// Straight up delete strategy for timelines
	[self BL_syncUpdateUsingDeleteStrategyForLogin:loginId withUpdates:updates inMOC:inMOC];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	[super setWithUpdateDictionary:update];
	self.plan = [AdvisorActivityPlanMO MR_findFirstByAttribute:AdvisorPlanMOAttributes.cloudId
		withValue:[update BC_numberForKey:kAdvisorActivityPlanId] inContext:self.managedObjectContext];
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
