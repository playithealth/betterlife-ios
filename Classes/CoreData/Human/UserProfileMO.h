//
//  UserProfileMO.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 5/10/12.
//  Copyright 2012 BettrLife Corporation. All rights reserved.
//

#import "_UserProfileMO.h"

#import "BLGoalInfo.h"

extern const double kActivityLevelValueTrackingDevice;
extern const double kActivityLevelValueSedentary;
extern const double kActivityLevelValueLight;
extern const double kActivityLevelValueModerate;
extern const double kActivityLevelValueActive;
extern const double kActivityLevelValueVeryActive;

@interface UserProfileMO : _UserProfileMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)profileDict;
- (void)updatePermissions:(NSDictionary *)permissions;
+ (UserProfileMO *)createUserProfileInMOC:(NSManagedObjectContext *)inMOC;
+ (UserProfileMO *)getCurrentUserProfileInMOC:(NSManagedObjectContext *)inMOC;
- (BLGoalInfo *)goalInfoForDate:(NSDate *)date;
- (NSData *)toJSON;

typedef NS_ENUM(NSInteger, ActivityLevel) {
	ActivityLevelTrackingDevice,
	ActivityLevelSedentary,
	ActivityLevelLight,
	ActivityLevelModerate,
	ActivityLevelActive,
	ActivityLevelVeryActive,
};

@end
