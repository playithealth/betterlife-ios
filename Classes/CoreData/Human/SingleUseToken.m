//
//  SingleUseToken.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/1/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "SingleUseToken.h"

@implementation SingleUseToken

- (NSData *)toJSON {
	// QuietLog(@"%s", __FUNCTION__);
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] 
		initWithObjectsAndKeys:
			self.token, kSingleUseTokenKey,
			nil];

	if (self.timeViewed > 0) {
		NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
		NSUInteger age = (now - [self.timeViewed integerValue]);
		[jsonDict setValue:[NSNumber numberWithInteger:age] forKey:@"su_age"];
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
