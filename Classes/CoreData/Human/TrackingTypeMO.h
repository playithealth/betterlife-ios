#import "_TrackingTypeMO.h"

@interface TrackingTypeMO : _TrackingTypeMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (NSString *)valueStringForTrackingValueDictionary:(NSDictionary *)valueDict;
- (NSString *)unitString;
@end
