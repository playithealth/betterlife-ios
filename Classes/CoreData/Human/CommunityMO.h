#import "_CommunityMO.h"

@interface CommunityMO : _CommunityMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (NSData *)toJSON;
@end
