//
//  MealPlannerDay.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/15/2011.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "MealPlannerDay.h"

#import "AdvisorMealPlanEntryMO.h"
#import "AdvisorMealPlanTagMO.h"
#import "RecipeMO.h"
#import "MealPlannerShoppingListItem.h"
#import "NSDictionary+NumberValue.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "User.h"

@implementation MealPlannerDay

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// required keys
	self.cloudId = [[update objectForKey:kId] numberValueDecimal];
	self.updatedOn = [[update objectForKey:kUpdatedOn] numberValueDecimal];
	self.name = [update objectForKey:kName];
	self.imageId = [update BC_numberOrNilForKey:kImageId];
	self.date = [NSDate dateWithTimeIntervalSince1970:[[update objectForKey:kMealPlannerDay] integerValue]];
	self.logTime = [[update objectForKey:kMealPlannerTime] numberValueDecimal];
	self.itemType = [update objectForKey:kMPItemType];

	self.recipeId = [update BC_numberOrNilForKey:kMPRecipeId];
	if (self.recipeId) {
		self.recipe = [RecipeMO recipeById:self.recipeId withManagedObjectContext:self.managedObjectContext];

	}
	self.productId = [update BC_numberOrNilForKey:kMPProductId];
	self.menuId = [update BC_numberOrNilForKey:kMPMenuId];

	self.tagId = [update BC_numberOrNilForKey:kMPTagID];
	if (self.tagId) {
		self.tag = [AdvisorMealPlanTagMO MR_findFirstByAttribute:AdvisorMealPlanTagMOAttributes.cloudId withValue:self.tagId
			inContext:self.managedObjectContext];
	}
	if ((NSNull *)[update objectForKey:kMPServingSize] != [NSNull null]) {
		self.servingSize = [update objectForKey:kMPServingSize];
	}
	if ((NSNull *)[update objectForKey:kMPServingSizeUOM] != [NSNull null]) {
		self.servingSizeUnit = [update objectForKey:kMPServingSizeUOM];
	}
	if ((NSNull *)[update objectForKey:kMPServingSizeUOMAbbreviation] != [NSNull null]) {
		self.servingSizeUnitAbbreviation = [update objectForKey:kMPServingSizeUOMAbbreviation];
	}

	self.nutritionId = [[update objectForKey:kNutritionId] numberValueDecimal];
	if ([self.nutritionId integerValue] > 0) {
		if (!self.nutrition) {
			self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
		}
		[self.nutrition setWithUpdateDictionary:update];
	}
	
	// Delete MealPlannerShoppingListItems (delete strategy)
	for (MealPlannerShoppingListItem *mpslItem in self.shoppingListItems) {
		[self.managedObjectContext deleteObject:mpslItem];
	}
	// Now add the ones from the dictionary
	if ((NSNull *)[update objectForKey:kMPShoppingListItems] != [NSNull null]) {
		for (NSDictionary *mpsliDict in [update objectForKey:kMPShoppingListItems]) {
			MealPlannerShoppingListItem *mpslItem = [MealPlannerShoppingListItem
				insertInManagedObjectContext:self.managedObjectContext];
			mpslItem.mealPlannerDay = self;

			// Update MealPlannerShoppingListItem
			[mpslItem setWithUpdateDictionary:mpsliDict];
		}
	}

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

+ (id)insertMealPlannerDayWithTag:(AdvisorMealPlanTagMO *)tag forDate:(NSDate *)date withAdvisorMealPlanTagDictionary:(NSDictionary *)tagDict
	inContext:(NSManagedObjectContext *)moc
{
	// the selection is a choice from the PlanningTagSearchViewController
	// and represents a "tag" for an "event" in the health coach meal plan
	MealPlannerDay *newMP = [MealPlannerDay insertInManagedObjectContext:moc];
	newMP.accountId = [[User currentUser] accountId];
	newMP.date = date;
	newMP.tag = (id)[moc existingObjectWithID:[tag objectID] error:nil];
	newMP.tagId = newMP.tag.cloudId;
	newMP.logTime = newMP.tag.entry.logTime;
	newMP.name = tagDict[kName];
	newMP.itemType = tagDict[kMPTagItemType];
	newMP.imageId = [tagDict BC_numberOrNilForKey:kImageId];
	newMP.servings = [tagDict BC_numberOrNilForKey:kMPTagServings];
	newMP.servingSize = [tagDict BC_stringOrNilForKey:kMPTagServingSize];
	newMP.servingSizeUnit = [tagDict BC_stringOrNilForKey:kMPTagServingSizeUOM];
	newMP.nutrition = [NutritionMO insertInManagedObjectContext:moc];
	[newMP.nutrition setWithUpdateDictionary:tagDict[kMPNutrients]];

	if ([newMP.itemType isEqual:kItemTypeRecipe]) {
		newMP.recipeId = [tagDict BC_numberOrNilForKey:kMPExternalId];
		newMP.recipe = [RecipeMO recipeById:[tagDict[kMPExternalId] numberValueDecimal] withManagedObjectContext:moc];
		if (!newMP.recipe) {
			newMP.recipe = [RecipeMO insertInManagedObjectContext:moc];
			newMP.recipe.accountId = newMP.accountId;
			[newMP.recipe setWithRecipeDictionary:tagDict];
		}
	}
	else if ([newMP.itemType isEqual:kItemTypeProduct]) {
		newMP.productId = [tagDict BC_numberOrNilForKey:kMPExternalId];
	}
	else if ([newMP.itemType isEqual:kItemTypeMenu]) {
		newMP.menuId = [tagDict BC_numberOrNilForKey:kMPExternalId];
	}

	[newMP markForSync];
	
	return newMP;
}

+ (id)insertMealPlannerDayWithRecipeDictionary:(NSDictionary *)recipeDict forDate:(NSDate *)date logTime:(NSNumber *)logTime
	inContext:(NSManagedObjectContext *)moc
{
	MealPlannerDay *newMP = [MealPlannerDay insertInManagedObjectContext:moc];
	newMP.accountId = [[User currentUser] accountId];
	newMP.recipeId = ([recipeDict BC_numberOrNilForKey:kRecipeId] ?: [recipeDict BC_numberOrNilForKey:kId]);
	newMP.recipe = [RecipeMO recipeById:newMP.recipeId withManagedObjectContext:moc];
	if (!newMP.recipe) {
		newMP.recipe = [RecipeMO insertInManagedObjectContext:moc];
		//newMP.recipe.status = kStatusPost; ///< TODO I don't believe we need to post the recipe, as I think using it subscribes us
		newMP.recipe.accountId = newMP.accountId;
		[newMP.recipe setWithRecipeDictionary:recipeDict];
	}

	newMP.date = date;
	newMP.logTime = logTime;
	newMP.sortOrder = 0;
	newMP.name = newMP.recipe.name;
	newMP.itemType = kItemTypeRecipe;
	newMP.imageId = newMP.recipe.imageId;
	newMP.nutrition = [NutritionMO insertNutritionWithDictionary:recipeDict inContext:moc];

	[newMP markForSync];
    
    return newMP;
}

+ (id)insertMealPlannerDayWithRecipeMO:(RecipeMO *)recipe forDate:(NSDate *)date logTime:(NSNumber *)logTime
	inContext:(NSManagedObjectContext *)moc
{
	MealPlannerDay *newMP = [MealPlannerDay insertInManagedObjectContext:moc];
	newMP.accountId = [[User currentUser] accountId];
	newMP.date = date;
	newMP.logTime = logTime;
	newMP.recipe = (id)[moc existingObjectWithID:[recipe objectID] error:nil];
	newMP.name = recipe.name;
	newMP.imageId = recipe.imageId;
	newMP.itemType = kItemTypeRecipe;
	// Copy the nutrition
	if (recipe.nutrition) {
		newMP.nutrition = [NutritionMO insertInManagedObjectContext:moc];
		[newMP.nutrition copyFromNutritionMO:recipe.nutrition];
	}

	[newMP markForSync];

	return newMP;
}

// NOTE Moved this comment to here, leaving this in place until I can get rid of all calls to the method below
// NOTE this is redundant, but necessary as I transition to using the nutritionMO below
- (void)updateNutritionFields:(NSDictionary *)nutrition
{
	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}
	[self.nutrition setWithUpdateDictionary:nutrition];
}

- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInteger:0]]) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

#pragma mark - virtual properties
// This probably isn't the most efficient way, possibly a transient property would be more efficient, where it gets set only
// when the MO changes, etc
- (NSNumber *)itemId
{
	NSNumber *itemId = nil;
	if ([self.itemType isEqualToString:kItemTypeProduct]) {
		itemId = self.productId;
	}
	else if ([self.itemType isEqualToString:kItemTypeRecipe]) {
		itemId = self.recipeId;
		if (!itemId || ![itemId integerValue]) {
			itemId = self.recipe.cloudId;
		}
	}
	else if ([self.itemType isEqualToString:kItemTypeMenu]) {
		itemId = self.menuId;
	}
	return itemId;
}

- (NSString *)servingsUnit
{
	// The only time you can set servings in a meal plan are when using a health coach plan, and these are all in 'servings'
	return @"servings";
}

#pragma mark - JSON
- (NSData *)toJSON
{
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];

	[jsonDict setValue:[NSNumber numberWithInteger:[self.date timeIntervalSince1970]] forKey:kMealPlannerDay];
	[jsonDict setValue:self.logTime forKey:kMealPlannerTime];

	if (self.recipe) {
		[jsonDict setValue:self.recipe.name forKey:kName];
		[jsonDict setValue:self.recipe.cloudId forKey:kMPRecipeId];
	}
	else if ([self.productId integerValue] > 0) {
		[jsonDict setValue:self.name forKey:kName];
		[jsonDict setValue:self.productId forKey:kMPProductId];
	}
	else if ([self.menuId integerValue] > 0) {
		[jsonDict setValue:self.name forKey:kName];
		[jsonDict setValue:self.menuId forKey:kMPMenuId];
	}

	if ([self.nutritionId integerValue] > 0) {
		[jsonDict setValue:self.nutritionId forKey:kNutritionId];
	}

	if (self.cloudId) {
		[jsonDict setValue:self.cloudId forKey:kMealPlannerId];
	}

	if (self.sortOrder) {
		[jsonDict setValue:self.sortOrder forKey:@"sort_order"];
	}

	if (self.tagId) {
		[jsonDict setValue:self.tagId forKey:kMPTagID];
	}

	if (self.servingSize) {
		[jsonDict setValue:self.servingSize forKey:kMPServingSize];
	}

	if (self.servingSizeUnit) {
		[jsonDict setValue:self.servingSizeUnit forKey:kMPServingSizeUOM];
	}

	if ([self.shoppingListItems count]) {
		NSMutableArray *slItemsArray = [[NSMutableArray alloc]
			initWithCapacity:[self.shoppingListItems count]];

		for (MealPlannerShoppingListItem *mpsli in self.shoppingListItems) {
			[slItemsArray addObject:[mpsli toDictionary]];
		}
		[jsonDict setValue:slItemsArray forKey:kMPShoppingListItems];
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
