#import "_SyncUpdateMO.h"

@interface SyncUpdateMO : _SyncUpdateMO {}
+ (BOOL)hasPerformedInitialSync:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)moc;
+ (void)syncSucceededForUser:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)moc;
+ (NSNumber *)lastUpdateForTable:(NSString *)tableName forUser:(NSNumber *)loginId inContext:(NSManagedObjectContext *)moc;
+ (void)setLastUpdate:(NSNumber *)lastUpdate forTable:(NSString *)tableName forUser:(NSNumber *)loginId
	inContext:(NSManagedObjectContext *)moc;
+ (NSArray *)syncUpdatesForUser:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)moc;
@end
