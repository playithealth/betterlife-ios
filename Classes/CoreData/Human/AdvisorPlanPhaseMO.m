#import "AdvisorPlanPhaseMO.h"

#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"

@interface AdvisorPlanPhaseMO ()
@end

@implementation AdvisorPlanPhaseMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	/*
	self.cloudId = [update objectForKey:<#kConstName#>];
	self.loginId = [update objectForKey:<#kConstName#>];
	*/

	self.durationInDays = [update BC_numberForKey:kAdvisorPlanPhaseDuration];
	self.phaseNumber = [update BC_numberForKey:kAdvisorPlanPhaseNumber];
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
		<#kConstName#> : self.cloudId,
		<#kConstName#> : self.durationInDays,
		<#kConstName#> : self.loginId,
		<#kConstName#> : self.phaseNumber,
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
