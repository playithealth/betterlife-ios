//
//  UserProfileMO.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 5/10/12.
//  Copyright 2012 BettrLife Corporation. All rights reserved.
//

#import "UserProfileMO.h"

#import "AdvisorMealPlanMO.h"
#import "AdvisorMealPlanPhaseMO.h"
#import "AdvisorMealPlanEntryMO.h"
#import "BCUtilities.h"
#import "NSCalendar+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NumberValue.h"
#import "NutrientMO.h"
#import "NutritionMO.h"
#import "TrackingMO.h"
#import "TrainingAssessmentMO.h"
#import "User.h"
#import "UserPermissionMO.h"

const double kActivityLevelValueTrackingDevice = 1.0;
const double kActivityLevelValueSedentary = 1.2;
const double kActivityLevelValueLight = 1.375;
const double kActivityLevelValueModerate = 1.55;
const double kActivityLevelValueActive = 1.725;
const double kActivityLevelValueVeryActive = 1.9;

/*
const NSDictionary *activityLevelMap = @{
	ActivityLevelSedentary : @(1.2),
	ActivityLevelLight : @(1.375),
	ActivityLevelModerate : @(1.55),
	ActivityLevelActive : @(1.725),
	ActivityLevelVeryActive : @(1.9)
};
*/

@implementation UserProfileMO

#pragma mark - convenience methods
- (void)setWithUpdateDictionary:(NSDictionary *)profileDict
{
	// {"height":"69","birthday":"4\/4\/1944","gender":"2","goal_weight":"250","current_weight":"265","activity_level":"1.375"}
	// {"name":"Sef Tarbell","email":"sef.tarbell@bettrlife.com","zipcode":"50325","height":"69","birthday":"8\/17\/1970","gender":"2","goal_weight":"250",
	// "goals":[{"user_id":"2","updated_on":"1351283445","updating_device":"web",
	// s"calories_min":null,"calories_max":null,"protein_min":null,"protein_max":null,"fat_min":null,"fat_max":null,"satfat_min":null,"satfat_max":null,"carbs_min":null,"carbs_max":null,
	// "sodium_min":null,"sodium_max":null,"cholesterol_min":null,"cholesterol_max":null,"goal_weight":"250"}],
	// "current_weight":"279.3","activity_level":"1.55","paidcustomer":1}

	if ([profileDict objectForKey:kUserProfileName]) {
        self.name = [profileDict valueForKey:kUserProfileName];
	}

	if ([profileDict objectForKey:kUserProfileEmail]) { 
        self.email = [profileDict valueForKey:kUserProfileEmail];
	}

	if ([profileDict objectForKey:kLoginPaidCustomer]) { 
        self.isPaidCustomer = [profileDict valueForKey:kLoginPaidCustomer];
	}

	if ([profileDict objectForKey:kUserProfileZipCode]) {
        self.zipCode = [profileDict BC_stringOrNilForKey:kUserProfileZipCode];
	}

	self.imageId = [profileDict BC_numberOrNilForKey:kImageId];

	if ((NSNull *)[profileDict objectForKey:kUserProfileHeight] != [NSNull null]) {
        self.height = [[profileDict valueForKey:kUserProfileHeight] numberValueDecimal];
    }
	if ((NSNull *)[profileDict objectForKey:kUserProfileBirthday] != [NSNull null]) {
		NSString *birthdayString = [profileDict valueForKey:kUserProfileBirthday];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateStyle:NSDateFormatterShortStyle];
		[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		NSDate *birthdate = [dateFormatter dateFromString:birthdayString];

        self.birthday = birthdate;
    }
	if ((NSNull *)[profileDict objectForKey:kUserProfileGender] != [NSNull null]) {
        self.gender = [[profileDict valueForKey:kUserProfileGender] numberValueDecimal];
    }
	if ((NSNull *)[profileDict objectForKey:kUserProfileGoalWeight] != [NSNull null]) {
        self.goalWeight = [[profileDict valueForKey:kUserProfileGoalWeight] numberValueDecimal];
    }

	id updateItem = [profileDict objectForKey:kUserProfileActivityLevel];
	if (updateItem && (updateItem != (id)[NSNull null])) { 
        self.activityLevel = [updateItem numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	}

	if ([profileDict objectForKey:kUserProfileOnboardingComplete]) { 
        self.onboardingComplete = [profileDict BC_numberOrNilForKey:kUserProfileOnboardingComplete];
	}

	self.hasMyZone = [profileDict BC_numberForKey:kUserProfileMyZone];

	NSString *skin = [profileDict BC_stringOrNilForKey:kUserProfileSkin];
	self.skin = (skin && [skin length] ? skin : nil);

	NSNumber *value = [profileDict BC_numberOrNilForKey:kUserProfileCustomNutritionTimeframe];
	if (value) {
		self.customNutritionTimeframe = value;
	}
	value = [profileDict BC_numberOrNilForKey:kUserProfileCustomTrackingTimeframe];
	if (value) {
		self.customTrackingTimeframe = value;
	}

	// Nutrition Goals
	id goals = [profileDict objectForKey:@"goals"];
	NSDictionary *goalsDict = nil;
	if ([goals isKindOfClass:[NSArray class]] && [(NSArray*)goals count] > 0) {
		goalsDict = [goals objectAtIndex:0];
	}
	if (goalsDict) {
		if ((NSNull *)[goalsDict objectForKey:kUserProfileGoalNutritionEffort] != [NSNull null]) {
			self.nutritionEffort = [[goalsDict valueForKey:kUserProfileGoalNutritionEffort] numberValueDecimal];
		}

		// We don't want a 'null' to be misrepresented as a 'NO', as we want to default to 'YES', so check for this
		value = [goalsDict BC_numberOrNilForKey:kUserProfileUseBMR];
		if (value) {
			self.useBMR = @([value boolValue]);
		}

		// Ratios
		self.useAutoRatios = [goalsDict BC_numberForKey:kAdvisorMealPlanPhaseAutoRatio];
		if ([self.useAutoRatios boolValue]) {
			self.proteinRatio = [goalsDict BC_numberOrNilForKey:kAdvisorMealPlanPhaseProteinRatio];
			self.totalFatRatio = [goalsDict BC_numberOrNilForKey:kAdvisorMealPlanPhaseTotalFatRatio];
			self.totalCarbohydrateRatio = [goalsDict BC_numberOrNilForKey:kAdvisorMealPlanPhaseTotalCarbohydrateRatio];
		}

		// nutrition min/max
		id minNutritionValue = [goalsDict objectForKey:kNutritionMinData];
		if ([minNutritionValue isKindOfClass:[NSDictionary class]]) {
			if (!self.minNutrition) {
				self.minNutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
			}
			[self.minNutrition setWithUpdateDictionary:minNutritionValue];
		}

		id maxNutritionValue = [goalsDict objectForKey:kNutritionMaxData];
		if ([maxNutritionValue isKindOfClass:[NSDictionary class]]) {
			if (!self.maxNutrition) {
				self.maxNutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
			}
			[self.maxNutrition setWithUpdateDictionary:maxNutritionValue];
		}
	}

	// Nutrients - always show
	NSArray *alwaysShow = [profileDict objectForKey:kUserProfileAlwaysShowNutrients];
	NSMutableSet *existingShowNutrients = [self.alwaysShowNutrients mutableCopy];
	for (NSString *cloudKey in alwaysShow) {
		NutrientMO *nutrient = [NutrientMO MR_findFirstByAttribute:NutrientMOAttributes.cloudKey
			withValue:cloudKey inContext:self.managedObjectContext];

		if (nutrient) {
			if ([existingShowNutrients containsObject:nutrient]) {
				// We already have this nutrient relationship, so keep track of that
				[existingShowNutrients removeObject:nutrient];
			}
			else {
				// New nutrient to always show, add it
				[self addAlwaysShowNutrientsObject:nutrient];
			}
		}
	}
	// Now, any nutrient that we didn't encounter in the update should be removed from the always show set
	for (NutrientMO *removedNutrient in existingShowNutrients) {
		[self removeAlwaysShowNutrientsObject:removedNutrient];
	}

	// TODO if there is a current weight, we may need to create an assessment or track this in another table like davin does
//	if ((NSNull *)[profileDict objectForKey:kUserProfileCurrentWeight] != [NSNull null]) {
//        self.currentWeight = [[profileDict valueForKey:kUserProfileCurrentWeight] numberValueDecimal];
//    }

	// TODO if there is an activity level, we may need to create an assessment
//	if ((NSNull *)[profileDict objectForKey:kUserProfileActivityLevel] != [NSNull null]) {
//        self.activityLevel = [[profileDict valueForKey:kUserProfileActivityLevel] numberValueDecimal];
//    }

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)updatePermissions:(NSDictionary *)permissions
{
	NSSet *unsortedPerms = self.permissions;
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:UserPermissionMOAttributes.name ascending:YES selector:@selector(caseInsensitiveCompare:)];
	NSArray *existingPerms = [unsortedPerms sortedArrayUsingDescriptors:@[sortByName]];

	[permissions enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop){
		NSString *permissionName = key;
		NSUInteger matchingPermissionIndex = [existingPerms indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
			if ([[obj name] isEqualToString:permissionName]) {
				*stop = YES;
				return YES;
			}
			else {
				return NO;
			}
		}];

		if (matchingPermissionIndex != NSNotFound) {
			UserPermissionMO *matchingPermission = existingPerms[matchingPermissionIndex];
			matchingPermission.enabled = obj;
		}
		else {
			UserPermissionMO *newPermission = [UserPermissionMO insertInManagedObjectContext:self.managedObjectContext];
			newPermission.name = permissionName;
			newPermission.enabled = obj;
			newPermission.user = self;
		}
	}];
}

+ (UserProfileMO *)createUserProfileInMOC:(NSManagedObjectContext *)inMOC
{
	User *thisUser = [User currentUser];
	if ([thisUser loginIsValid]) {
		// verify that there is no profile for this user
		UserProfileMO *userProfile = [UserProfileMO MR_findFirstByAttribute:UserProfileMOAttributes.loginId withValue:thisUser.loginId
			inContext:inMOC];

		if (!userProfile) {
			userProfile = [UserProfileMO insertInManagedObjectContext:inMOC];
			userProfile.loginId = thisUser.loginId;

			// this is being created for the first time, mostly from
			// the login info - there will only be a need to sync
			// if they make changes in the profile view 
			userProfile.status = kStatusOk;
		}

		return userProfile;
	}
	else {
		DDLogWarn(@"Unable to create UserProfileMO, no valid login.");			
		return nil;
	}
}

+ (UserProfileMO *)getCurrentUserProfileInMOC:(NSManagedObjectContext *)inMOC
{
	User *thisUser = [User currentUser];
	if ([thisUser loginIsValid]) {
		UserProfileMO *userProfile = [UserProfileMO MR_findFirstByAttribute:UserProfileMOAttributes.loginId withValue:thisUser.loginId
			inContext:inMOC];

		return userProfile;
	}
	else {
		DDLogWarn(@"Unable to retrieve UserProfileMO, no valid login.");			
		return nil;
	}
}

- (BLGoalInfo *)goalInfoForDate:(NSDate *)date
{
	BLGoalInfo *goalInfo = [[BLGoalInfo alloc] init];
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSMutableDictionary *minNutrition = nil;
	NSMutableDictionary *maxNutrition = nil;
	NSArray *mealPlanEntries = @[];
	NSNumber *bmr;
	NSNumber *bmrGoal;

	// Explicit defaulting of the goal source to defaults, just for documentation purposes and clarity
	goalInfo.goalSource = GoalsDefaults;

	// Calculate the BMR, if we can. It will potentially be used as a calorie goal, as well as an additive to the calories expended
	NSNumber *lastWeight = nil;
	lastWeight = [TrackingMO getCurrentWeightInMOC:self.managedObjectContext];

	if (self.goalWeight && lastWeight && ([self.goalWeight doubleValue] > [lastWeight doubleValue])) {
		// Flip the weight loss flag to false, as they appear to be trying to gain weight
		// TODO Should this be using the last weight, or the first weight, or something else??? It seems that if we use their last
		// weight, and they've surpassed their goal weight, then we will mistakenly think they're trying to gain weight
		goalInfo.isWeightLoss = NO;
	}
	if (lastWeight && self.birthday && self.height && self.gender) {
		bmr = [BCUtilities calculateBmrForGender:[self.gender integerValue]
			ageInYears:[calendar BC_yearsFromDate:self.birthday toDate:date]
			weightInPounds:[lastWeight doubleValue]
			heightInInches:[self.height doubleValue] usingEquation:kBMREquationMifflinStJeor];

		// Figure out the activity level calories, to lessen the math, remove 1.0, which represents the BMR itself
		if (self.activityLevel) {
			goalInfo.activityLevelCalories = @([bmr doubleValue] * ([self.activityLevel doubleValue] - 1.0));
		}
	}

	NSArray *nutrientsCollection = nil;

	// First, see if they have a meal plan, and if so, use the nutrients from that
	AdvisorMealPlanMO *currentPlan = [AdvisorMealPlanMO getMealPlanForDate:date inContext:self.managedObjectContext];
	AdvisorMealPlanPhaseMO *phase = [currentPlan getPhaseForDate:date];
	if (phase) {
		NSSortDescriptor *logTimes = [NSSortDescriptor sortDescriptorWithKey:AdvisorMealPlanEntryMOAttributes.logTime ascending:YES];
		mealPlanEntries = [phase.entries sortedArrayUsingDescriptors:@[logTimes]];
	}
	minNutrition = [phase.minNutrition nutrientDictWithValues];
	maxNutrition = [phase.maxNutrition nutrientDictWithValues];
	if ([minNutrition count] || [maxNutrition count] || phase.bmr) {

		// Since we have some goals set from the meal plan, we're using meal plan goals
		goalInfo.goalSource = GoalsMealPlan;

		if (phase.bmr && bmr) {
			goalInfo.usingBMR = YES;
			// Adjust the BMR Goal based upon the value set in the meal plan
			// The meal plan bmr is specified as +-2.5 in 0.5 increments, where 1 represents 500 kCal, so do the math
			bmrGoal = @(round([bmr doubleValue] + ([phase.bmr doubleValue] * 500.0)));

			// BMR should be either max or min calorie value, depending on if they're trying to gain or lose
			if ([phase.bmr doubleValue] <= 0) {
				[maxNutrition setValue:bmrGoal forKey:NutritionMOAttributes.calories];
				[minNutrition removeObjectForKey:NutritionMOAttributes.calories];
			}
			else {
				[minNutrition setValue:bmrGoal forKey:NutritionMOAttributes.calories];
				[maxNutrition removeObjectForKey:NutritionMOAttributes.calories];
			}

		}
	}
	else {
		// No meal plan, see if they have goals set
		minNutrition = [self.minNutrition nutrientDictWithValues];
		maxNutrition = [self.maxNutrition nutrientDictWithValues];

		// Use BMR if the user has selected useBMR
		if ([self.useBMR boolValue]) {
			goalInfo.usingBMR = YES;
			if (!bmr) {
				// Since the user wants to use BMR, but doesn't currently have a calculable BMR, we cannot set a calorie goal
				[minNutrition removeObjectForKey:NutritionMOAttributes.calories];
				[maxNutrition removeObjectForKey:NutritionMOAttributes.calories];
			}
			// Determine if this person is trying to gain or lose weight, and add/subtract the nutritionEffort based upon that
			else if (goalInfo.isWeightLoss) {
				bmrGoal = @(round([bmr doubleValue] - [self.nutritionEffort doubleValue]));
				[maxNutrition setValue:bmrGoal forKey:NutritionMOAttributes.calories];
				[minNutrition removeObjectForKey:NutritionMOAttributes.calories];
			}
			else {
				bmrGoal = @(round([bmr doubleValue] + [self.nutritionEffort doubleValue]));
				[minNutrition setValue:bmrGoal forKey:NutritionMOAttributes.calories];
				[maxNutrition removeObjectForKey:NutritionMOAttributes.calories];
			}
		}

		// If we came away from here with any nutrients with goals, then they are using personal goals
		if ([minNutrition count] || [maxNutrition count]) {
			goalInfo.goalSource = GoalsPersonal;
		}
	}

	// If auto_ratio is set, then calculate fat/carbs/protein values based upon the ratios given to them
	// This needs to be down here because we need to establish what their calorie goals are before we attempt to calculate this.
	if ((phase && [phase.useAutoRatios boolValue]) || ((goalInfo.goalSource == GoalsPersonal) && [self.useAutoRatios boolValue])) {
		NSNumber *minCalorieGoal = [minNutrition objectForKey:NutritionMOAttributes.calories];
		NSNumber *maxCalorieGoal = [maxNutrition objectForKey:NutritionMOAttributes.calories];
		double goalValue;
		NSNumber *proteinRatio = ([phase.useAutoRatios boolValue] ? phase.proteinRatio : self.proteinRatio);
		NSNumber *carbRatio = ([phase.useAutoRatios boolValue] ? phase.totalCarbohydrateRatio : self.totalCarbohydrateRatio);
		NSNumber *fatRatio = ([phase.useAutoRatios boolValue] ? phase.totalFatRatio : self.totalFatRatio);

		if (proteinRatio) {
			// Remove any existing values, and then add values based upon calorie goals
			[minNutrition removeObjectForKey:NutritionMOAttributes.protein];
			[maxNutrition removeObjectForKey:NutritionMOAttributes.protein];
			if (minCalorieGoal) {
				goalValue = round([minCalorieGoal doubleValue] * ([proteinRatio doubleValue] / 100.0) / 4.0);
				[minNutrition setObject:@((int)goalValue) forKey:NutritionMOAttributes.protein];
			}
			if (maxCalorieGoal) {
				double goalValue = round([maxCalorieGoal doubleValue] * ([proteinRatio doubleValue] / 100.0) / 4.0);
				[maxNutrition setObject:@((int)goalValue) forKey:NutritionMOAttributes.protein];
			}
		}
		if (carbRatio) {
			// Remove any existing values, and then add values based upon calorie goals
			[minNutrition removeObjectForKey:NutritionMOAttributes.totalCarbohydrates];
			[maxNutrition removeObjectForKey:NutritionMOAttributes.totalCarbohydrates];
			if (minCalorieGoal) {
				goalValue = round([minCalorieGoal doubleValue] * ([carbRatio doubleValue] / 100.0) / 4.0);
				[minNutrition setObject:@((int)goalValue) forKey:NutritionMOAttributes.totalCarbohydrates];
			}
			if (maxCalorieGoal) {
				double goalValue = round([maxCalorieGoal doubleValue] * ([carbRatio doubleValue] / 100.0) / 4.0);
				[maxNutrition setObject:@((int)goalValue) forKey:NutritionMOAttributes.totalCarbohydrates];
			}
		}
		if (fatRatio) {
			// Remove any existing values, and then add values based upon calorie goals
			[minNutrition removeObjectForKey:NutritionMOAttributes.totalFat];
			[maxNutrition removeObjectForKey:NutritionMOAttributes.totalFat];
			if (minCalorieGoal) {
				goalValue = round([minCalorieGoal doubleValue] * ([fatRatio doubleValue] / 100.0) / 9.0);
				[minNutrition setObject:@((int)goalValue) forKey:NutritionMOAttributes.totalFat];
			}
			if (maxCalorieGoal) {
				double goalValue = round([maxCalorieGoal doubleValue] * ([fatRatio doubleValue] / 100.0) / 9.0);
				[maxNutrition setObject:@((int)goalValue) forKey:NutritionMOAttributes.totalFat];
			}
		}
	}

	if ([minNutrition count]) {
		if ([maxNutrition count]) {
			// Use a set because it will eliminate duplicates automatically
			NSMutableSet *nutrientsSet = [[NSMutableSet alloc] init];
			[nutrientsSet addObjectsFromArray:[minNutrition allKeys]];
			[nutrientsSet addObjectsFromArray:[maxNutrition allKeys]];

			// Now transfer the set into an array
			nutrientsCollection = [nutrientsSet allObjects];
		}
		else {
			nutrientsCollection = [minNutrition allKeys];
		}

	}
	else if ([maxNutrition count]) {
		nutrientsCollection = [maxNutrition allKeys];
	}
	else {
		// No goal nutrients
	}

	goalInfo.minNutrition = minNutrition;
	goalInfo.maxNutrition = maxNutrition;
	goalInfo.bmr = bmr;
	goalInfo.bmrGoal = bmrGoal;
	goalInfo.goalNutrients = nutrientsCollection;
	goalInfo.mealPlanEntries = mealPlanEntries;

	return goalInfo;
}

#pragma mark - JSON
- (NSData *)toJSON
{
	// {"height":"69","birthday":"4\/4\/1944","gender":"2","goal_weight":"250",
	// "current_weight":"265","activity_level":"1.375"}
	// QuietLog(@"%s", __FUNCTION__);

	// NOTE this looks stupid as I am sending the birthday as a string
	// and as pieces, but I want to change the api to use the string
	// the pieces are for compatibility with Davin's code for now
	NSCalendar *gregorian = nil;
	NSDateComponents *components = nil;
	NSString *birthdayString = nil;
	if (self.birthday) {
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateStyle:NSDateFormatterShortStyle];
		[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		birthdayString = [dateFormatter stringFromDate:self.birthday];
		gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

		components = [gregorian components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:self.birthday];
	}
	

	// Nutrition
	NSMutableDictionary *minNutritionDict = [[NSMutableDictionary alloc] init];
	NSMutableDictionary *maxNutritionDict = [[NSMutableDictionary alloc] init];
	[self.minNutrition toJSONDict:minNutritionDict];
	[self.maxNutrition toJSONDict:maxNutritionDict];

	// Always show nutrients
	// Cloud is silly, as it expects these as a comma-delimited string instead of an array, so doing that in the dict initializer
	NSArray *alwaysShowNutrients = [[self.alwaysShowNutrients valueForKeyPath:NutrientMOAttributes.cloudKey] allObjects];

	NSDictionary *jsonDict = @{
		kUserProfileName : self.name,
		kUserProfileEmail : self.email,
		kUserProfileZipCode : (self.zipCode ?: [NSNull null]),
		kUserProfileHeight : (self.height ?: [NSNull null]),
		kUserProfileBirthday : (birthdayString ?: [NSNull null]),
		kUserProfileBirthdayDay : (components ? [NSNumber numberWithInteger:components.day] : [NSNull null]),
		kUserProfileBirthdayMonth : (components ? [NSNumber numberWithInteger:components.month] : [NSNull null]),
		kUserProfileBirthdayYear : (components ? [NSNumber numberWithInteger:components.year] : [NSNull null]),
		kUserProfileGender : (self.gender ?: [NSNull null]),
		kUserProfileGoalWeight : (self.goalWeight ?: [NSNull null]),
		kUserProfileActivityLevel : (self.activityLevel ?: [NSNull null]),
		kUserProfileGoalNutritionEffort : (self.nutritionEffort ?: [NSNull null]),
		kUserProfileOnboardingComplete : (self.onboardingComplete ? @1 : @0),
		kUserProfileCustomNutritionTimeframe : self.customNutritionTimeframe,
		kUserProfileCustomTrackingTimeframe : self.customTrackingTimeframe,
		kImageId : (self.imageId ?: [NSNull null]),
		kNutritionMinData : minNutritionDict,
		kNutritionMaxData : maxNutritionDict,
		kUserProfileAlwaysShowNutrients : (alwaysShowNutrients ? [alwaysShowNutrients componentsJoinedByString:@","] : [NSNull null]),
		kUserProfileUseBMR : self.useBMR,
		kAdvisorMealPlanPhaseAutoRatio : self.useAutoRatios,
		kAdvisorMealPlanPhaseProteinRatio : (self.proteinRatio ?: [NSNull null]),
		kAdvisorMealPlanPhaseTotalFatRatio : (self.totalFatRatio ?: [NSNull null]),
		kAdvisorMealPlanPhaseTotalCarbohydrateRatio : (self.totalCarbohydrateRatio ?: [NSNull null]),
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
