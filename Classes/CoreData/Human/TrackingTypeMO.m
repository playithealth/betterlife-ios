#import "TrackingTypeMO.h"

#import "NSDictionary+NumberValue.h"
#import "TrackingMO.h"
#import "TrackingValueMO.h"

@interface TrackingTypeMO ()
@end


@implementation TrackingTypeMO

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.cloudId = [update BC_numberForKey:kTrackingTypeId]; ///< This doesn't use the traditional id key
	self.title = [update valueForKey:kTitle];
	self.view = [update valueForKey:kTrackingTypeView];
	self.sortOrder = [update BC_numberForKey:kTrackingTypeSortOrder]; ///< This isn't currently coming from the cloud, but is set by sync

	self.readOnly = [update BC_numberForKey:kTrackingTypeReadOnly];

	NSArray *trackValues = [update objectForKey:kTrackingTypeValues];
	NSMutableArray *existingTrackingValues = [[self.trackingValues allObjects] mutableCopy];
	for (NSDictionary *trackValueDict in trackValues) {
		NSString *field = [trackValueDict valueForKey:kTrackingValueField];
		// this code block is used below to grab the index of the matching managedObject by id
		BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
			TrackingValueMO *managedObject = (TrackingValueMO *)obj;
			return ([managedObject.field isEqualToString:field]);
		};

		NSUInteger cacheIndex = [existingTrackingValues indexOfObjectPassingTest:SameCloudId];

		TrackingValueMO *trackingValueMO = nil;
		if (cacheIndex == NSNotFound) {
			trackingValueMO = [TrackingValueMO insertInManagedObjectContext:self.managedObjectContext];
			// Connect this value to this type
			trackingValueMO.trackingType = self;
		}
		else {
			trackingValueMO = [existingTrackingValues objectAtIndex:cacheIndex];
			[existingTrackingValues removeObjectAtIndex:cacheIndex];
		}
		[trackingValueMO setWithUpdateDictionary:trackValueDict];
	}

	// Remove any tracking values that didn't receive an update
	for (TrackingValueMO *trackingValueMO in existingTrackingValues) {
		[self.managedObjectContext deleteObject:trackingValueMO];
	}
}

#pragma mark - Convenience methods
- (NSString *)valueStringForTrackingValueDictionary:(NSDictionary *)valueDict
{
	// Get an array of tracking values that are not optional, sorted by their field names, which are track_value1, track_value2, etc...
	NSArray *trackingValues = [[self.trackingValues filteredSetUsingPredicate:
			[NSPredicate predicateWithFormat:@"%K = NO", TrackingValueMOAttributes.optional]]
		sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:TrackingValueMOAttributes.field ascending:YES]]];

	NSMutableArray *valuesArray = [NSMutableArray array];
	for (TrackingValueMO *trackingValue in trackingValues) {
		NSNumber *currentValue = [valueDict BC_numberOrNilForKey:trackingValue.field];
		if (currentValue) {
			[valuesArray addObject:[NSString stringWithFormat:@"%1$.*2$f", [currentValue doubleValue], [trackingValue.precision intValue]]];
		}
		else {
			[valuesArray addObject:@"-"];
		}
	}

	return [valuesArray componentsJoinedByString:@"/"];
}

- (NSString *)unitString
{
	// Get an array of tracking values that are not optional, sorted by their field names, which are track_value1, track_value2, etc...
	NSArray *trackingValues = [[self.trackingValues filteredSetUsingPredicate:
			[NSPredicate predicateWithFormat:@"%K = NO", TrackingValueMOAttributes.optional]]
		sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:TrackingValueMOAttributes.field ascending:YES]]];

	// Using sameUnits and priorUnits, try to determine if all the units for this tracking type are the same, if so, just show
	// the units once in the label, otherwise, show each value's units, separated by a separator
	BOOL sameUnits = YES;
	NSString *priorUnits = [[trackingValues firstObject] unitAbbrev];
	for (TrackingValueMO *trackingValue in trackingValues) {
		if (sameUnits && (!priorUnits || ![trackingValue.unitAbbrev isEqualToString:priorUnits])) {
			sameUnits = NO;
		}
	}

	if (!sameUnits) {
		priorUnits = [[trackingValues valueForKeyPath:TrackingValueMOAttributes.unitAbbrev] componentsJoinedByString:@"/"];
	}

	return priorUnits;
}

@end
