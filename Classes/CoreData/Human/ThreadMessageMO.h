#import "_ThreadMessageMO.h"

@interface ThreadMessageMO : _ThreadMessageMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (NSData *)toJSON;
@end
