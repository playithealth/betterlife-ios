#import "_NutrientMO.h"

@interface NutrientMO : _NutrientMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
+ (NSArray *)orderedNutrientsWithNames:(id)nutrientNamesCollection inMOC:(NSManagedObjectContext *)moc;
@end
