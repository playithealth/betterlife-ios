//
//  PurchaseHxItem.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/19/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "_PurchaseHxItem.h"

#import "BLProductItem.h"

@interface PurchaseHxItem : _PurchaseHxItem <BLProductItem> {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
+ (PurchaseHxItem *)itemByBarcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC;
+ (PurchaseHxItem *)itemByName:(NSString *)name andProductId:(NSNumber *)productId inMOC:(NSManagedObjectContext *)inMOC;
@end
