#import "AdvisorMO.h"

#import "AdvisorGroup.h"
#import "BCUrlFactory.h"
#import "RecipeMO.h"
#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"
#import "NumberValue.h"

@implementation AdvisorMO

+ (NSString *)syncName {
	return kSyncAdvisorUsers;
}

+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict {
	return [updateDict BC_numberForKey:kAdvisorId];
}

- (id)cloudId {
	return self.advisorId;
}

+ (NSString *)statusString {
	return kSyncUpdateHumanReadable_Advisors;
}

+ (NSURL *)urlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit {
	return [BCUrlFactory advisorsURL];
}

+ (NSPredicate *)userPredicate {
	return [self BL_loginIdPredicate];
}

#pragma mark - set with...
+ (void)setWithUpdatesArray:(NSArray *)updates forLogin:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)inMOC {
	[self BL_syncUpdateUsingModifiedDeleteStrategyForLogin:loginId withUpdates:updates inMOC:inMOC];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// required keys
	self.advisorId = [[update valueForKey:kAdvisorId] numberValueDecimal];
	self.loginId = [[update valueForKey:kUserId] numberValueDecimal];
	self.updatedOn = [[update valueForKey:kUpdatedOn] numberValueDecimal];

	id updateValue = nil;

	// optional keys
	updateValue = [update objectForKey:kAdvisorCanMessage];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canMessage = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanRecommendMeals];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canRecommendMeals = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanRecommendMealPlans];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canRecommendMealPlans = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanRecommendExercise];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canRecommendExercise = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanViewNutrition];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canViewNutrition = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanViewExercise];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canViewExercise = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanViewMeals];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canViewMeals = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanViewShoppingList];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canViewShoppingList = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanViewPantry];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canViewPantry = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanAddToShoppingList];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canAddToShoppingList = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorCanDeleteFromShoppingList];
	if (updateValue && (updateValue != [NSNull null])) {
		self.canDeleteFromShoppingList = [updateValue numberValueDecimal];
	}
	updateValue = [update objectForKey:kAdvisorConfirmation];
	if (updateValue && (updateValue != [NSNull null])) {
		self.confirmation = updateValue;
	}

	// Advisor groups always all come down from the cloud, so if any are missing, then this user
	// no longer belongs to that group, so remove any groups that don't come down
	NSMutableSet *currentGroups = [[self.groups valueForKeyPath:@"cloudId"] mutableCopy];
	updateValue = [update objectForKey:kAdvisorGroups];
	if (updateValue && (updateValue != [NSNull null])) {
		for (NSDictionary *advisorGroupDict in updateValue) {
			NSNumber *advisorGroupId = [[advisorGroupDict objectForKey:kAdvisorGroupId] numberValueDecimal];

			// Remove this group from the current groups array, since we processed it
			// this code block is used below to grab the matching item by id
			BOOL (^SameGroupId)(id, BOOL *) = ^BOOL(id obj, BOOL *stop) {
				AdvisorGroup *item = obj;
				return ([item.cloudId isEqualToNumber:advisorGroupId]);
			};

			NSSet *matches = [currentGroups objectsPassingTest:SameGroupId];

			AdvisorGroup *advisorGroup = nil;
			if (![matches count]) {
				advisorGroup = [AdvisorGroup insertInManagedObjectContext:self.managedObjectContext];
				advisorGroup.advisor = self;
			}
			else {
				advisorGroup = [matches anyObject];
				[currentGroups removeObject:advisorGroup];
			}

			// Update advisorGroup
			[advisorGroup setWithUpdateDictionary:advisorGroupDict]; 
		}
	}

	// Now remove any groups remaining in currentGroups, as this array now contains groups that were not referenced
	// in the update object
	for (AdvisorGroup *advisorGroup in currentGroups) {
		[self.managedObjectContext deleteObject:advisorGroup];
	}

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

@end
