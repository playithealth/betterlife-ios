#import "_ActivityMO.h"

@interface ActivityMO : _ActivityMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)copyFromActivity:(ActivityMO *)activity includeSets:(BOOL)includeSets;
@end
