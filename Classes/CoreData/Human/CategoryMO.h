//
//  CategoryMO.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 5/2/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "_CategoryMO.h"

@interface CategoryMO : _CategoryMO {}
// Custom logic goes here.
+ (id)categoryById:(NSNumber *)categoryId withManagedObjectContext:(NSManagedObjectContext *)context;
@end
