// 
//  PurchaseHxItem.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/19/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "PurchaseHxItem.h"

#import "CategoryMO.h"
#import "NumberValue.h"
#import "User.h"

@implementation PurchaseHxItem

// Custom logic goes here.
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.name = [update valueForKey:kItemName];
	self.productId = [[update valueForKey:kProductId] numberValueDecimal];
	self.updatedOn = [[update valueForKey:kUpdatedOn] numberValueDecimal];
	self.purchasedOn = [[update valueForKey:kPurchaseHxPurchasedOn] numberValueDecimal];

	// these pieces of data could be null
	if ((NSNull *)[update objectForKey:kBarcode] != [NSNull null]) {
		self.barcode = [update valueForKey:kBarcode];
	}
	if ((NSNull *)[update objectForKey:kImageId] != [NSNull null]) {
		self.imageId = [[update valueForKey:kImageId] numberValueDecimal];
	}
	if ((NSNull *)[update objectForKey:kStore] != [NSNull null]) {
		self.storeName = [update valueForKey:kStore];
	}
	if ((NSNull *)[update objectForKey:kProductCategoryId] == [NSNull null]) {
		self.category = [CategoryMO categoryById:[NSNumber numberWithInt:1] withManagedObjectContext:self.managedObjectContext];
	}
	else {
		self.category = [CategoryMO categoryById:[[update valueForKey:kProductCategoryId] numberValueDecimal]
			withManagedObjectContext:self.managedObjectContext];
	}

	self.calories = [[update objectForKey:kNutritionCalories] numberValueDecimal];
	self.protein = [[update objectForKey:kNutritionProtein] numberValueDecimal];
	self.carbohydrates = [[update objectForKey:kNutritionTotalCarbohydrates] numberValueDecimal];
	self.fat = [[update objectForKey:kNutritionTotalFat] numberValueDecimal];
	self.saturatedFat = [[update objectForKey:kNutritionSaturatedFat] numberValueDecimal];
	self.sodium = [[update objectForKey:kNutritionSodium] numberValueDecimal];
	self.cholesterol = [[update objectForKey:kNutritionCholesterol] numberValueDecimal];
	self.caloriesFromFat = [[update objectForKey:kNutritionCaloriesFromFat] numberValueDecimal];
}

#pragma mark - Convenience methods
+ (PurchaseHxItem *)itemByBarcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC
{
	NSString *regex = [NSString stringWithFormat:@"0*%@", barcode];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:
		@"barcode MATCHES %@ AND status <> %@ AND accountId = %@", regex, kStatusDelete, [[User currentUser] accountId]];

	return [PurchaseHxItem MR_findFirstWithPredicate:predicate sortedBy:@"updatedOn" ascending:NO inContext:inMOC];
}

+ (PurchaseHxItem *)itemByName:(NSString *)name andProductId:(NSNumber *)productId inMOC:(NSManagedObjectContext *)inMOC
{
	// Default productId to zero if it isn't passed in
	if (!productId) {
		productId = [NSNumber numberWithInteger:0];
	}

	NSPredicate *predicate = [NSPredicate predicateWithFormat:
		@"productId = %@ and name = %@ and accountId = %@ and status <> %@", productId, name, [[User currentUser] accountId], kStatusDelete];

	return [PurchaseHxItem MR_findFirstWithPredicate:predicate sortedBy:@"updatedOn" ascending:NO inContext:inMOC];
}

@end
