typedef NS_ENUM(uint16_t, PerformOperator) {
  PerformAll,
  PerformAtLeast,
  PerformAtMost,
  PerformExactly,
};

typedef NS_ENUM(uint16_t, WorkoutTime) {
  WorkoutTimeAny,
};

