//
// TrainingAssessmentMO.m
// BettrLife Corporation
//
// Created by Sef Tarbell on 5/31/2012.
// Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "TrainingAssessmentMO.h"

#import "BCUtilities.h"
#import "NSCalendar+Additions.h"
#import "NumberValue.h"
#import "User.h"
#import "UserProfileMO.h"

@implementation TrainingAssessmentMO

#pragma mark - virtual properties
- (NSNumber *)bmi
{
	[self willAccessValueForKey:@"bmi"];
	// english: bmi = weigh in lbs / height in inches squared x 703
	// metric: bmi = weigh in kilograms / height in meters squared
	NSNumber *theBMI = [BCUtilities calculateBmiForWeightInPounds:self.weight heightInInches:self.height];
	[self didAccessValueForKey:@"bmi"];
	return theBMI;
}

- (NSNumber *)bmr
{
	[self willAccessValueForKey:@"bmr"];
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSNumber *theBMR = [BCUtilities calculateBmrForGender:[self.sex integerValue] ageInYears:[calendar BC_yearsFromDate:userProfile.birthday toDate:[NSDate date]] 
		weightInPounds:[self.weight floatValue] heightInInches:[self.height floatValue] usingEquation:kBMREquationMifflinStJeor];
	[self didAccessValueForKey:@"bmr"];
	return theBMR;
}

- (NSString *)listTitle
{
	return @"Assessment";
}

- (NSString *)listDescription
{
	//42yo male weight 265lbs 
	//Chest 42in Waist 44in Stomach 48in Hips 46in Neck 18in Right Bicep 22in Left Bicep 21in Right Thigh 33in 
	//Body Fat 33% Crunches 1 Push-ups 2 Pull-ups 3 Squats 4 Mile Run 5 BMI: 39 BMR: 2307.7 Suggested daily calorie intake 3173
	NSString *genderName = nil;
	if ([self.sex integerValue] == 1) {
		genderName = @"female";
	}
	else if ([self.sex integerValue] == 2) {
		genderName = @"male";
	}
	return [NSString stringWithFormat:@"%@yo %@ weight:%@ chest:%@ waist:%@ stomach:%@ "
		"hips:%@ neck:%@ left bicep:%@ right bicep:%@ left thigh:%@ right thigh:%@ "
		"body fat:%@%% crunches:%@ push-ups:%@ pull-ups:%@ squats:%@ mile run:%@ "
		"BMI:%0.1f BMR:%0.1f", 
		self.age, genderName, self.weight, self.chest, self.waist, self.stomach,
		self.hips, self.neck, self.leftBicep, self.rightBicep, self.leftThigh,
		self.rightThigh, self.bodyFatPercent, self.crunches, self.pushups, self.pullups,
		self.squats, self.mileRun, [self.bmi floatValue], [self.bmr floatValue]];
  
}

#pragma mark - convenience methods
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	//{"day":"1329890400","age":"42","sex":"2","height":"69","weight":"267",
	//"neck":"19","chest":"52","waist":"44","stomach":"49","hips":"51",
	//"right_bicep":"22","left_bicep":"23","right_thigh":"29","left_thigh":"30",
	//"activity_level":"1.55","crunches":"20","pushups":"21","pullups":"22",
	//"squats":"23","mile_run":"24","updated_on":"1329954063"}

	// required keys
	NSNumber *day =	[update valueForKey:kTrainingLogDay];
	self.date = [NSDate dateWithTimeIntervalSince1970:[day integerValue]];
	self.updatedOn = [[update valueForKey:kUpdatedOn]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];

	if ((NSNull *)[update objectForKey:kAssessmentLogAge] != [NSNull null]) {
		self.age = [[update objectForKey:kAssessmentLogAge] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogSex] != [NSNull null]) {
		self.sex = [[update objectForKey:kAssessmentLogSex] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogHeight] != [NSNull null]) {
		self.height = [[update objectForKey:kAssessmentLogHeight] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogWeight] != [NSNull null]) {
		self.weight = [[update objectForKey:kAssessmentLogWeight] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogBodyFatPercent] != [NSNull null]) {
		self.bodyFatPercent = [[update objectForKey:kAssessmentLogBodyFatPercent] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogNeck] != [NSNull null]) {
		self.neck = [[update objectForKey:kAssessmentLogNeck] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogChest] != [NSNull null]) {
		self.chest = [[update objectForKey:kAssessmentLogChest] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogWaist] != [NSNull null]) {
		self.waist = [[update objectForKey:kAssessmentLogWaist] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogStomach] != [NSNull null]) {
		self.stomach = [[update objectForKey:kAssessmentLogStomach] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogHips] != [NSNull null]) {
		self.hips = [[update objectForKey:kAssessmentLogHips] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogLeftBicep] != [NSNull null]) {
		self.leftBicep = [[update objectForKey:kAssessmentLogLeftBicep] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogRightBicep] != [NSNull null]) {
		self.rightBicep = [[update objectForKey:kAssessmentLogRightBicep] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogLeftThigh] != [NSNull null]) {
		self.leftThigh = [[update objectForKey:kAssessmentLogLeftThigh] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogRightThigh] != [NSNull null]) {
		self.rightThigh = [[update objectForKey:kAssessmentLogRightThigh] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogActivityLevel] != [NSNull null]) {
		self.activityLevel = [[update objectForKey:kAssessmentLogActivityLevel] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogCrunches] != [NSNull null]) {
		self.crunches = [[update objectForKey:kAssessmentLogCrunches] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogPushups] != [NSNull null]) {
		self.pushups = [[update objectForKey:kAssessmentLogPushups] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogPullups] != [NSNull null]) {
		self.pullups = [[update objectForKey:kAssessmentLogPullups] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogSquats] != [NSNull null]) {
		self.squats = [[update objectForKey:kAssessmentLogSquats] numberValueDecimal];
	}

	if ((NSNull *)[update objectForKey:kAssessmentLogMileRun] != [NSNull null]) {
		self.mileRun = [[update objectForKey:kAssessmentLogMileRun] numberValueDecimal];
	}

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

+ (TrainingAssessmentMO *)getLastAssessmentInMOC:(NSManagedObjectContext *)context
{
	return [TrainingAssessmentMO MR_findFirstWithPredicate:
		[NSPredicate predicateWithFormat:@"loginId = %@ AND status <> %@", [[User currentUser] loginId], kStatusDelete]
		sortedBy:TrainingAssessmentMOAttributes.date ascending:NO inContext:context];
}

#pragma mark - JSON
- (NSData *)toJSON
{
	//{"day":"1329890400","age":"42","sex":"2","height":"69","weight":"267",
	//"neck":"19","chest":"52","waist":"44","stomach":"49","hips":"51",
	//"right_bicep":"22","left_bicep":"23","right_thigh":"29","left_thigh":"30",
	//"activity_level":"1.55","crunches":"20","pushups":"21","pullups":"22",
	//"squats":"23","mile_run":"24","updated_on":"1329954063"}

	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
		[NSNumber numberWithInt:[self.date timeIntervalSince1970]], kTrainingLogDay,
		nil];

	if (self.cloudId) {
		[jsonDict setValue:self.cloudId forKey:kAssessmentLogId];
	}

	[jsonDict setValue:(self.age ? self.age : [NSNull null])
		forKey:kAssessmentLogAge];

	[jsonDict setValue:(self.sex ? self.sex : [NSNull null])
		forKey:kAssessmentLogSex];

	[jsonDict setValue:(self.height ? self.height : [NSNull null])
		forKey:kAssessmentLogHeight];

	[jsonDict setValue:(self.weight ? self.weight : [NSNull null])
		forKey:kAssessmentLogWeight];

	[jsonDict setValue:(self.bodyFatPercent ? self.bodyFatPercent : [NSNull null])
		forKey:kAssessmentLogBodyFatPercent];

	[jsonDict setValue:(self.neck ? self.neck : [NSNull null])
		forKey:kAssessmentLogNeck];

	[jsonDict setValue:(self.chest ? self.chest : [NSNull null])
		forKey:kAssessmentLogChest];

	[jsonDict setValue:(self.waist ? self.waist : [NSNull null])
		forKey:kAssessmentLogWaist];

	[jsonDict setValue:(self.stomach ? self.stomach : [NSNull null])
		forKey:kAssessmentLogStomach];

	[jsonDict setValue:(self.hips ? self.hips : [NSNull null])
		forKey:kAssessmentLogHips];

	[jsonDict setValue:(self.leftBicep ? self.leftBicep : [NSNull null])
		forKey:kAssessmentLogLeftBicep];

	[jsonDict setValue:(self.rightBicep ? self.rightBicep : [NSNull null])
		forKey:kAssessmentLogRightBicep];

	[jsonDict setValue:(self.leftThigh ? self.leftThigh : [NSNull null])
		forKey:kAssessmentLogLeftThigh];

	[jsonDict setValue:(self.rightThigh ? self.rightThigh : [NSNull null])
		forKey:kAssessmentLogRightThigh];

	[jsonDict setValue:(self.activityLevel ? self.activityLevel : [NSNull null])
		forKey:kAssessmentLogActivityLevel];

	[jsonDict setValue:(self.crunches ? self.crunches : [NSNull null])
		forKey:kAssessmentLogCrunches];

	[jsonDict setValue:(self.pushups ? self.pushups : [NSNull null])
		forKey:kAssessmentLogPushups];

	[jsonDict setValue:(self.pullups ? self.pullups : [NSNull null])
		forKey:kAssessmentLogPullups];

	[jsonDict setValue:(self.squats ? self.squats : [NSNull null])
		forKey:kAssessmentLogSquats];

	[jsonDict setValue:(self.mileRun ? self.mileRun : [NSNull null])
		forKey:kAssessmentLogMileRun];

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
