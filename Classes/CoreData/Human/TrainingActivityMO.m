#import "TrainingActivityMO.h"

#import "ExerciseRoutineMO.h"
#import "NSDictionary+NumberValue.h"

@implementation TrainingActivityMO

#pragma mark - convenience methods
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	NSNumber *day =	[update BC_numberForKey:kTrainingLogDay];
	self.date = [NSDate dateWithTimeIntervalSince1970:[day integerValue]];
	self.updatedOn = [update BC_numberForKey:kUpdatedOn];
	self.activity = [update BC_stringOrNilForKey:kActivityRecordActivity];
	self.calories = [update BC_numberOrNilForKey:kNutritionCalories];
	self.weight = [update BC_numberOrNilForKey:kActivityLogWeight];
	self.time = [update BC_stringOrNilForKey:kActivityLogTime];
	self.factor = [update BC_numberOrNilForKey:kActivityRecordFactor];
	self.distance = [update BC_numberOrNilForKey:kActivityLogDistance];

	if ([update objectForKey:kSource]) {
        self.source = [update BC_numberOrNilForKey:kSource];
	}
	
	// set up the relationship with the exercise routine
	NSNumber *exerciseRoutineID = [update BC_numberOrNilForKey:kActivityLogExerciseRoutinesId];
	if (exerciseRoutineID) {
		self.exerciseRoutine = [ExerciseRoutineMO MR_findFirstByAttribute:ExerciseRoutineMOAttributes.cloudId withValue:exerciseRoutineID inContext:self.managedObjectContext];
	}

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

+ (double)calculateFactorForTime:(double)timeInMinutes andDistance:(double)distanceInMiles
{
	double mph = abs(distanceInMiles / (timeInMinutes / 60));

	if (mph < 2.0) {
		return 0.01512;
	}
	else if (mph < 2.8) {
		return 0.021168;
	}
	else if (mph < 3.5) {
		return 0.02646;
	}
	else if (mph < 3.9) {
		return 0.032507;
	}
	else if (mph < 4.0) {
		return 0.037799;
	}
	else if (mph < 5.0) {
		return 0.045359;
	}
	else if (mph < 5.2) {
		return 0.062747;
	}
	else if (mph < 6.0) {
		return 0.068039;
	}
	else if (mph < 6.7) {
		return 0.074087;
	}
	else if (mph < 7.0) {
		return 0.079379;
	}
	else if (mph < 7.5) {
		return 0.083159;
	}
	else if (mph < 8.0) {
		return 0.086938;
	}
	else if (mph < 8.6) {
		return 0.089206;
	}
	else if (mph < 9.0) {
		return 0.092986;
	}
	else if (mph < 10.0) {
		return 0.096766;
	}
	else if (mph < 11.0) {
		return 0.109618;
	}
	else if (mph < 12.0) {
		return 0.120958;
	}
	else if (mph < 13.0) {
		return 0.143637;
	}
	else if (mph < 14.0) {
		return 0.149685;
	}
	else {
		return 0.173877;
	}
}

- (NSNumber *)calculateCalories
{
	NSArray *timePieces = [self.time componentsSeparatedByString:@":"];
	double calculatedMinutes = 0.0f;
	if ([timePieces count] == 3) {
		NSInteger hours = [[timePieces objectAtIndex:0] intValue];
		NSInteger minutes = [[timePieces objectAtIndex:1] intValue];
		NSInteger seconds = [[timePieces objectAtIndex:2] intValue];
		calculatedMinutes = (hours * 60) + minutes + ((double)seconds / 60.0f);
	}
	// TODO remove these other calculations when a picker is implemented?
	else if ([timePieces count] == 2) {
		NSInteger hours = [[timePieces objectAtIndex:0] intValue];
		NSInteger minutes = [[timePieces objectAtIndex:1] intValue];
		calculatedMinutes = (hours * 60) + minutes;
	}
	else if ([timePieces count] == 1) {
		calculatedMinutes = [[timePieces objectAtIndex:0] intValue];
	}

	double factor = [TrainingActivityMO calculateFactorForTime:calculatedMinutes andDistance:[self.distance doubleValue]];
	self.factor = [NSNumber numberWithDouble:factor];

	NSInteger estimatedCaloriesBurned = (NSInteger)(calculatedMinutes * factor * [self.weight doubleValue]);
	self.calories = [NSNumber numberWithInteger:estimatedCaloriesBurned];

	return self.calories;
}

- (NSDictionary *)timeInfo
{
	if ([self.time rangeOfString:@":"].location == NSNotFound) {
		return nil;
	}

	NSMutableDictionary *timeInfo = [NSMutableDictionary dictionary];

	NSArray *timePieces = [self.time componentsSeparatedByString:@":"];

	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];

	if ([timePieces count] == 3) {
		timeInfo[kTimeInfoHours] = [numberFormatter numberFromString:[timePieces objectAtIndex:0]];
		timeInfo[kTimeInfoMinutes] = [numberFormatter numberFromString:[timePieces objectAtIndex:1]];
		timeInfo[kTimeInfoSeconds] = [numberFormatter numberFromString:[timePieces objectAtIndex:2]];
	}
	else if ([timePieces count] >= 2) {
		timeInfo[kTimeInfoHours] = [numberFormatter numberFromString:[timePieces objectAtIndex:0]];
		timeInfo[kTimeInfoMinutes] = [numberFormatter numberFromString:[timePieces objectAtIndex:1]];
	}
	else if ([timePieces count] >= 1) {
		timeInfo[kTimeInfoMinutes] = [numberFormatter numberFromString:[timePieces objectAtIndex:0]];
	}

	return timeInfo;
}

- (NSNumber *)timeInMinutes
{
	NSDictionary *timeInfo = [self timeInfo];
	NSNumber *hours = timeInfo[kTimeInfoHours];
	NSNumber *minutes = timeInfo[kTimeInfoMinutes];
	NSNumber *seconds = timeInfo[kTimeInfoSeconds];
	NSNumber *timeInMinutes = @(([hours integerValue] * 60) + [minutes integerValue] + ([seconds doubleValue] / 60.0f));

	return timeInMinutes;
}

- (NSString *)timeString
{
	NSDictionary *timeInfo = [self timeInfo];
	NSNumber *hours = timeInfo[kTimeInfoHours];
	NSNumber *minutes = timeInfo[kTimeInfoMinutes];
	NSNumber *seconds = timeInfo[kTimeInfoSeconds];

	NSMutableArray *timeComponents = [NSMutableArray array];
	if ([hours integerValue] > 0) {
		[timeComponents addObject:[NSString stringWithFormat:@"%@h", hours]];
	}
	if ([minutes integerValue] > 0) {
		[timeComponents addObject:[NSString stringWithFormat:@"%@m", minutes]];
	}
	if ([seconds integerValue] > 0) {
		[timeComponents addObject:[NSString stringWithFormat:@"%@s", seconds]];
	}
	
	return [timeComponents componentsJoinedByString:@" "];
}

#pragma mark - JSON
- (NSData *)toJSON
{
	NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];

	if (self.cloudId) {
		[jsonDict setValue:self.cloudId forKey:kActivityId];
	}

	if (![self.status isEqualToString:kStatusDelete]) {
		[jsonDict setValue:[NSNumber numberWithInt:[self.date timeIntervalSince1970]] 
					forKey:kTrainingLogDay];

		[jsonDict setValue:(self.activity ? self.activity : [NSNull null])
					forKey:kActivityRecordActivity];

		[jsonDict setValue:(self.calories ? self.calories : [NSNull null])
					forKey:kActivityLogCalories];

		[jsonDict setValue:(self.weight ? self.weight : [NSNull null])
					forKey:kActivityLogWeight];

		[jsonDict setValue:(self.time ? self.time : [NSNull null])
					forKey:kActivityLogTime];

		[jsonDict setValue:(self.factor ? self.factor : [NSNull null])
					forKey:kActivityRecordFactor];

		[jsonDict setValue:(self.distance ? self.distance : [NSNull null])
					forKey:kActivityLogDistance];

		[jsonDict setValue:self.exerciseRoutine.cloudId forKey:kActivityLogExerciseRoutinesId];
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}


@end
