#import "_AdvisorActivityPlanMO.h"

#import "BLSyncEntity.h"

@interface AdvisorActivityPlanMO : _AdvisorActivityPlanMO <BLSyncEntity> {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
+ (id)getActivityPlanForDate:(NSDate *)date inContext:(NSManagedObjectContext *)context;
+ (id)getCurrentActivityPlanInContext:(NSManagedObjectContext *)context;
@end
