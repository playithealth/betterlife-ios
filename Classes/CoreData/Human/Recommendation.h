#import "_Recommendation.h"

@interface Recommendation : _Recommendation {}
- (NSData *)toJSON;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
@end
