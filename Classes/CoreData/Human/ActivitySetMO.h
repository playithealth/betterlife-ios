#import "_ActivitySetMO.h"

@interface ActivitySetMO : _ActivitySetMO {}
- (NSString *)secondsString;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)copyFromActivitySet:(ActivitySetMO *)activitySet;
@end
