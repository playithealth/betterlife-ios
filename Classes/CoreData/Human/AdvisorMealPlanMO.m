#import "AdvisorMealPlanMO.h"

#import "AdvisorMO.h"
#import "AdvisorMealPlanEntryMO.h"
#import "AdvisorMealPlanPhaseMO.h"
#import "AdvisorMealPlanTimelineMO.h"
#import "BCUrlFactory.h"
#import "NSCalendar+Additions.h"
#import "NSDate+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"
#import "NumberValue.h"
#import "User.h"

@implementation AdvisorMealPlanMO

+ (NSString *)syncName {
	return kSyncAdvisorMealPlans;
}

+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict {
	return [updateDict BC_numberForKey:kAdvisorMealPlanId];
}

+ (NSString *)statusString {
	return kSyncUpdateHumanReadable_MealPlans;
}

+ (NSURL *)urlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit {
	return [BCUrlFactory advisorMealPlansURL];
}

+ (NSPredicate *)userPredicate {
    return [self BL_loginIdPredicate];
}

+ (void)setWithUpdatesArray:(NSArray *)updates forLogin:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)inMOC {
	[self BL_syncUpdateUsingModifiedDeleteStrategyForLogin:loginId withUpdates:updates inMOC:inMOC];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	[super setWithUpdateDictionary:update withPhaseClass:[AdvisorMealPlanPhaseMO class]];
	// The generic setWithUpdateDictionary looks for 'id' for the cloud id, and this returns it in a different key
	self.cloudId = [update BC_numberForKey:kAdvisorMealPlanId];

	self.trackSugaryDrinks = [update BC_numberForKey:kAdvisorMealPlanSugaryDrinks];
	self.trackFruitsVeggies = [update BC_numberForKey:kAdvisorMealPlanFruitsVeggies];
}

+ (id)getMealPlanForDate:(NSDate *)date inContext:(NSManagedObjectContext *)context
{
	return [AdvisorPlanMO getPlanForClass:[AdvisorMealPlanMO class] forDate:date inContext:context];
}

+ (id)getCurrentMealPlanInContext:(NSManagedObjectContext *)context
{
	return [AdvisorMealPlanMO getMealPlanForDate:[NSDate date] inContext:context];
}

- (NSString *)getMealPlanDetailText
{
	NSString *detailText = nil;

	NSArray *sortedTimelines = [self.timelines sortedArrayUsingDescriptors:
		@[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:NO],
		[NSSortDescriptor sortDescriptorWithKey:@"stopDate" ascending:NO]]];
	for (AdvisorMealPlanTimelineMO *timeline in sortedTimelines) {
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[dateFormatter setDateStyle:NSDateFormatterShortStyle];

		// unless the stop date is in the future, ignore
		if ([timeline.stopDate timeIntervalSinceNow] > 0) {
			// if it's scheduled in the future, set text but keep looking
			if ([timeline.startDate timeIntervalSinceNow] > 0) {
				detailText = [NSString stringWithFormat:@"Scheduled to start %@", [dateFormatter stringFromDate:timeline.startDate]];
			}
			// otherwise it is current then set text and escape the loop
			else {
				detailText = [NSString stringWithFormat:@"Current plan since %@", [dateFormatter stringFromDate:timeline.startDate]];
				break;
			}
		}
	}

	// if marked new && not scheduled or current
	if (!detailText && [self.isNew boolValue]) {
		detailText = @"This plan is new!";
	}

	return detailText;
}

- (id)getMealPlanEntryForDate:(NSDate *)date atLogTime:(NSNumber *)logTime
{
	// First, get the phase
	AdvisorMealPlanPhaseMO *phase = [self getPhaseForDate:date];

	// Now get the entries for this phase and logtime
	NSSet *entriesForLogTime = [phase.entries filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"%K = %@",
		AdvisorMealPlanEntryMOAttributes.logTime, logTime]];

	// This should only be returning one object in the set, as we don't currently support multiple entries per logTime,
	// so just return anyObject
	return [entriesForLogTime anyObject];
}

+ (id)mealPlanById:(NSNumber *)mealPlanId inContext:(NSManagedObjectContext *)context
{
	return [AdvisorMealPlanMO MR_findFirstByAttribute:AdvisorPlanMOAttributes.cloudId withValue:mealPlanId inContext:context];
}

@end
