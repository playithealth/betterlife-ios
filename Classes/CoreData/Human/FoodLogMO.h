#import "_FoodLogMO.h"

#import "BLFoodItem.h"
#import "BLSyncEntity.h"
#import "NutritionMO.h"

@class MealPredictionMO;
@class AdvisorMealPlanTagMO;
@class MealPlannerDay;

typedef NS_ENUM(NSInteger, BLLogTime) {
	LogTimeNone = -1,
	LogTimeBreakfast = 0,
	LogTimeMorningSnack,
	LogTimeLunch,
	LogTimeAfternoonSnack,
	LogTimeDinner,
	LogTimeEveningSnack,
	LogTimeWater,
	LogTimeFoodNote,
	LogTimeCardio,
	LogTimeStrength,
	LogTimeActivityNote,
	LogTimeSugaryDrinks,
	LogTimeFruitsVeggies,
	};

@interface FoodLogMO : _FoodLogMO <BLFoodItem, BLPagingSyncEntity> {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)setWithProductDictionary:(NSDictionary *)product;
- (void)setWithPrediction:(MealPredictionMO *)prediction;
- (void)setWithManagedObject:(id)managedObject;
- (void)setNutritionWithDictionary:(id)nutrition;
- (void)setNutritionFromMO:(id)nutritionSource;
+ (id)insertFoodLogWithFoodLog:(FoodLogMO *)foodLog inContext:(NSManagedObjectContext *)moc;
+ (id)insertFoodLogWithPrediction:(MealPredictionMO *)prediction inContext:(NSManagedObjectContext *)moc;
+ (id)insertFoodLogWithMealPlanner:(MealPlannerDay *)mealPlanner inContext:(NSManagedObjectContext *)moc;
+ (id)insertFoodLogWithTag:(AdvisorMealPlanTagMO *)tag forDate:(NSDate *)date withAdvisorMealPlanTagDictionary:(NSDictionary *)tagDict
	inContext:(NSManagedObjectContext *)moc;
- (void)markForSync;
- (void)setProductImage:(UIImage *)image;
- (NSNumber *)itemId;
- (UIImage *)productImage;
- (NSData *)toJSON;
+ (NSNumber *)guessMealTime;
+ (NSArray *)findLogsAfter:(NSDate *)after before:(NSDate *)before inContext:(NSManagedObjectContext *)moc;
+ (NSArray *)findLogsForLogTime:(NSNumber *)logTime after:(NSDate *)after before:(NSDate *)before inContext:(NSManagedObjectContext *)moc;
@end
