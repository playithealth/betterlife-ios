#import "_HealthChoiceMO.h"

extern const struct HealthChoiceTypes {
	int allergy;
	int lifestyle;
	int ingredient;
	int maximum;
} HealthChoiceTypes;

@interface HealthChoiceMO : _HealthChoiceMO {}

@end
