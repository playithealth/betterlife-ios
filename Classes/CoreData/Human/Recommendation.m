#import "Recommendation.h"

#import "CategoryMO.h"
#import "NumberValue.h"

@implementation Recommendation

#pragma mark - JSON
- (NSData *)toJSON
{
	return [NSJSONSerialization dataWithJSONObject: @{kItemName : self.name, kProductId : self.productId} 
		options:kNilOptions error:nil];
}

#pragma mark - convenience methods
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.name = [update valueForKey:kItemName];
	self.productId = [[update valueForKey:kProductId] numberValueDecimal];
	self.category = [CategoryMO categoryById: [[update valueForKey:kCategoryId] numberValueDecimal]
		withManagedObjectContext:self.managedObjectContext];
	self.quantity = [[update valueForKey:kQuantity] numberValueDecimal];
	self.days = [[update valueForKey:kRecommendationDays] numberValueDecimal];
	self.hidden = [[update valueForKey:kRecommendationHidden] numberValueDecimal];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

@end
