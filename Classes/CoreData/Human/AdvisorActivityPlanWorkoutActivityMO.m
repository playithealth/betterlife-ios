#import "AdvisorActivityPlanWorkoutActivityMO.h"

#import "AdvisorActivityPlanWorkoutActivitySetMO.h"
#import "NSDictionary+NumberValue.h"

@interface AdvisorActivityPlanWorkoutActivityMO ()

// Private interface goes here.

@end

@implementation AdvisorActivityPlanWorkoutActivityMO

#pragma mark - sync from cloud
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// Handled by ActivityMO super class
	[super setWithUpdateDictionary:update];
}

#pragma mark - sync to cloud
// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
