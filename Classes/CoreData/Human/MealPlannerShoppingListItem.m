#import "MealPlannerShoppingListItem.h"

#import "MealIngredient.h"
#import "NumberValue.h"
#import "ShoppingListItem.h"

@implementation MealPlannerShoppingListItem

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.quantity = [[update valueForKey:kMPShoppingListItemQuantity]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	self.dateAdded = [[update valueForKey:kMPShoppingListItemDateAdded] 
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	self.shoppingListItem = [ShoppingListItem
		itemByCloudId:[[update valueForKey:kMPShoppingListItemSLId]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle]
		inMOC:self.managedObjectContext];
	self.ingredient = [MealIngredient
		itemByCloudId:[[update valueForKey:kMPShoppingListItemIngredientId]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle]
		inMOC:self.managedObjectContext];
}

#pragma mark - JSON
- (NSDictionary *)toDictionary
{
	// QuietLog(@"%s", __FUNCTION__);
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] 
		initWithObjectsAndKeys:
			self.quantity, kMPShoppingListItemQuantity,
			self.ingredient.cloudId, kMPShoppingListItemIngredientId,
			nil];

	if (self.dateAdded) {
		[jsonDict setValue:self.dateAdded forKey:kMPShoppingListItemDateAdded];
	}
	if (self.shoppingListItem) {
		[jsonDict setValue:self.shoppingListItem.cloudId forKey:kMPShoppingListItemSLId];
	}
	else {
		[jsonDict setValue:[NSNumber numberWithInteger:0] forKey:kMPShoppingListItemSLId];
	}

	return jsonDict;
}

@end
