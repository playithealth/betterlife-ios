#import "_CustomFoodMO.h"

@class FoodLogMO;
@protocol BLFoodItem;

@interface CustomFoodMO : _CustomFoodMO {}
@property (weak, readonly, nonatomic) NSNumber *productId;

+ (CustomFoodMO*)foodByName:(NSString *)inName productId:(NSNumber *)inProductId barcode:(NSString *)inBarcode inMOC:(NSManagedObjectContext *)context;
- (void)setImageWithUIImage:(UIImage *)anImage;
- (UIImage*)imageAsUIImage;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)setWithFoodItem:(id<BLFoodItem>)foodItem;
- (NSData *)toJSON;
- (void)markForSync;

@end
