#import "UnitMO.h"

#import "BCUrlFactory.h"
#import "NSManagedObject+BLSync.h"

@interface UnitMO ()
@end

@implementation UnitMO

+ (NSString *)syncName {
	return kSyncUnits;
}

+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict {
	// Not needed, since this uses a pure delete strategy
	return nil;
}

+ (NSString *)statusString {
	return kSyncUpdateHumanReadable_MeasurementUnits;
}

+ (NSURL *)urlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit {
	return [BCUrlFactory unitsURL];
}

+ (NSPredicate *)userPredicate {
	// return nil, as units are not attached to users
	return nil;
}

#pragma mark - set with...
+ (void)setWithUpdatesArray:(NSArray *)updates forLogin:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)inMOC {
	[self BL_syncUpdateUsingDeleteStrategyForLogin:loginId withUpdates:updates inMOC:inMOC];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// required keys
	self.abbrev = [update valueForKey:kUnitAbbreviation];
	self.singular = [update valueForKey:kUnitSingular];
	self.plural = [update valueForKey:kUnitPlural];
	self.sortOrder = [update valueForKey:kUnitSortOrder];
}

@end
