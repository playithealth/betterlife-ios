#import "_ActivityPlanMO.h"

#import "BLSyncEntity.h"

@interface ActivityPlanMO : _ActivityPlanMO <BLSyncEntity> {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
+ (NSArray *)activityPlansForLoginId:(NSNumber *)loginId forDate:(NSDate *)date inContext:(NSManagedObjectContext *)context;
- (void)markForSync;
@end
