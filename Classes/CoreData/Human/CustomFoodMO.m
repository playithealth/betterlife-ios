#import "CustomFoodMO.h"

#import "BLFoodItem.h"
#import "CategoryMO.h"
#import "FoodLogMO.h"
#import "NSDictionary+NumberValue.h"
#import "NutritionMO.h"
#import "User.h"

@implementation CustomFoodMO

//
// Only one param is required.  Maybe this should be several methods, rather than just one multi-purpose
//
+ (CustomFoodMO*)foodByName:(NSString *)inName productId:(NSNumber *)inProductId barcode:(NSString *)inBarcode inMOC:(NSManagedObjectContext*)context
{
	NSPredicate *predicate;

	if (inName && [inName length]) {
		if (inProductId && [inProductId integerValue]) {
			predicate = [NSPredicate predicateWithFormat:@"name = %@ AND cloudId = %@",
				inName, inProductId];
		}
		else {
			predicate = [NSPredicate predicateWithFormat:@"name = %@", inName];
		}
	}
	else if (inBarcode && [inBarcode length]) {
		predicate = [NSPredicate predicateWithFormat:@"barcode = %@", inBarcode];
	}
	else if (inProductId && [inProductId integerValue]) {
		predicate = [NSPredicate predicateWithFormat:@"cloudId = %@", inProductId];
	}
	else {
		return nil;
	}
	
	return [CustomFoodMO MR_findFirstWithPredicate:predicate inContext:context];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// required keys
	self.cloudId = [update BC_numberForKey:kId];
	self.name = [update BC_stringOrNilForKey:kName];
	self.imageId = [update BC_numberOrNilForKey:kImageId];
	self.updatedOn = [update BC_numberForKey:kCFUpdatedOn]; 
	self.category = [CategoryMO categoryById:[update BC_numberOrNilForKey:kProductCategoryId] withManagedObjectContext:self.managedObjectContext];
	self.servingSize = [update BC_numberOrNilForKey:kCFServingSize];
	self.servingSizeUnit = [update BC_stringOrNilForKey:kCFServingSizeUnit];
	self.servingsPerContainer = [update BC_numberOrNilForKey:kCFServingsPerContainer];

	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}
	[self.nutrition setWithUpdateDictionary:update];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)setNutritionFromNutrition:(NutritionMO *)nutrition
{
	if (!self.nutrition) {
		self.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}
	[self.nutrition copyFromNutritionMO:nutrition];
}

- (void)setWithFoodItem:(id<BLFoodItem>)foodItem
{
	self.name = foodItem.name;
	self.accountId = [[User currentUser] accountId];
	self.category = [CategoryMO categoryById:@1 withManagedObjectContext:self.managedObjectContext];
	self.status = kStatusPost;

	// NOTE: Foodlog uses a combined serving size and units, so that has to be dealt with outside of this method,
	// along with other attributes that foodlog lacks, such as servingsPerContainer, etc

	// set the nutrition from the food log item
	[self setNutritionFromNutrition:foodItem.nutrition];
}

#pragma mark - image handling
- (void)setImageWithUIImage:(UIImage *)anImage
{
	[self willChangeValueForKey:CustomFoodMOAttributes.image];
	
	NSData *imageAsData = UIImagePNGRepresentation(anImage);
	[self setImage:imageAsData];
	[self setPrimitiveValue:imageAsData forKey:CustomFoodMOAttributes.image];
	[self didChangeValueForKey:CustomFoodMOAttributes.image];
}

- (UIImage*)imageAsUIImage
{
	[self willAccessValueForKey:FoodLogMOAttributes.image];
	UIImage *anImage = [UIImage imageWithData:[self primitiveValueForKey:FoodLogMOAttributes.image]];
	[self didAccessValueForKey:FoodLogMOAttributes.image];
	return anImage;
}

#pragma mark - JSON
- (NSData *)toJSON
{
	// QuietLog(@"%s", __FUNCTION__);

	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];

	if ([self.status isEqualToString:kStatusDelete]) {
		[jsonDict setValue:self.cloudId forKey:kFoodLogId];
	}
	else {
		[jsonDict setValue:self.name forKey:kName];

		if (self.cloudId) {
			[jsonDict setValue:self.cloudId forKey:kId];
		}
		if (self.category) {
			[jsonDict setValue:self.category.cloudId forKey:kProductCategoryId];
		}
		if (self.servingSize) {
			[jsonDict setValue:self.servingSize forKey:kCFServingSize];
		}
		if (self.servingSizeUnit) {
			[jsonDict setValue:self.servingSizeUnit forKey:kCFServingSizeUnit];
		}
		if (self.servingsPerContainer) {
			[jsonDict setValue:self.servingsPerContainer forKey:kCFServingsPerContainer];
		}

		[jsonDict setValue:(self.nutrition.calories ? self.nutrition.calories : [NSNull null]) forKey:kNutritionCalories];
		[jsonDict setValue:(self.nutrition.caloriesFromFat ? self.nutrition.caloriesFromFat : [NSNull null]) forKey:kNutritionCaloriesFromFat];
		[jsonDict setValue:(self.nutrition.totalFat ? self.nutrition.totalFat : [NSNull null]) forKey:kNutritionTotalFat];
		[jsonDict setValue:(self.nutrition.transFat ? self.nutrition.transFat : [NSNull null]) forKey:kNutritionTransFat];
		[jsonDict setValue:(self.nutrition.saturatedFat ? self.nutrition.saturatedFat : [NSNull null]) forKey:kNutritionSaturatedFat];
		[jsonDict setValue:(self.nutrition.saturatedFatCalories ? self.nutrition.saturatedFatCalories : [NSNull null]) forKey:kNutritionSaturatedFatCalories];
		[jsonDict setValue:(self.nutrition.polyunsaturatedFat ? self.nutrition.polyunsaturatedFat : [NSNull null]) forKey:kNutritionPolyunsaturatedFat];
		[jsonDict setValue:(self.nutrition.monounsaturatedFat ? self.nutrition.monounsaturatedFat : [NSNull null]) forKey:kNutritionMonounsaturatedFat];
		[jsonDict setValue:(self.nutrition.cholesterol ? self.nutrition.cholesterol : [NSNull null]) forKey:kNutritionCholesterol];
		[jsonDict setValue:(self.nutrition.sodium ? self.nutrition.sodium : [NSNull null]) forKey:kNutritionSodium];
		[jsonDict setValue:(self.nutrition.potassium ? self.nutrition.potassium : [NSNull null]) forKey:kNutritionPotassium];
		[jsonDict setValue:(self.nutrition.totalCarbohydrates ? self.nutrition.totalCarbohydrates : [NSNull null]) forKey:kNutritionTotalCarbohydrates];
		[jsonDict setValue:(self.nutrition.otherCarbohydrates ? self.nutrition.otherCarbohydrates : [NSNull null]) forKey:kNutritionOtherCarbohydrates];
		[jsonDict setValue:(self.nutrition.dietaryFiber ? self.nutrition.dietaryFiber : [NSNull null]) forKey:kNutritionDietaryFiber];
		[jsonDict setValue:(self.nutrition.solubleFiber ? self.nutrition.solubleFiber : [NSNull null]) forKey:kNutritionSolubleFiber];
		[jsonDict setValue:(self.nutrition.insolubleFiber ? self.nutrition.insolubleFiber : [NSNull null]) forKey:kNutritionInsolubleFiber];
		[jsonDict setValue:(self.nutrition.sugars ? self.nutrition.sugars : [NSNull null]) forKey:kNutritionSugars];
		[jsonDict setValue:(self.nutrition.sugarsAlcohol ? self.nutrition.sugarsAlcohol : [NSNull null]) forKey:kNutritionSugarsAlcohol];
		[jsonDict setValue:(self.nutrition.protein ? self.nutrition.protein : [NSNull null]) forKey:kNutritionProtein];
		[jsonDict setValue:(self.nutrition.vitaminAPercent ? self.nutrition.vitaminAPercent : [NSNull null]) forKey:kNutritionVitaminAPercent];
		[jsonDict setValue:(self.nutrition.vitaminCPercent ? self.nutrition.vitaminCPercent : [NSNull null]) forKey:kNutritionVitaminCPercent];
		[jsonDict setValue:(self.nutrition.calciumPercent ? self.nutrition.calciumPercent : [NSNull null]) forKey:kNutritionCalciumPercent];
		[jsonDict setValue:(self.nutrition.ironPercent ? self.nutrition.ironPercent : [NSNull null]) forKey:kNutritionIronPercent];
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

#pragma mark - convenience methods
- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

- (NSNumber *)productId
{
	return self.cloudId;
}

@end
