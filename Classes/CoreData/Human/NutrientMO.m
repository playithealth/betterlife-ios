#import "NutrientMO.h"
#import "NutritionMO.h"

@interface NutrientMO ()

@end

@implementation NutrientMO

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// required keys
	self.name = [NutritionMO localKeyFromCloudKey:[update valueForKey:kNutrientName]];
	self.cloudKey = [update valueForKey:kNutrientName];
	self.displayName = [update valueForKey:kNutrientTitle];
	self.precision = [update valueForKey:kNutrientPrecision];
	self.sortOrder = [update valueForKey:kNutrientSortOrder];

	// optional keys
	if ([update objectForKey:kNutrientIsSubNutrient] && ([update objectForKey:kNutrientIsSubNutrient] != (id)[NSNull null])) {
		self.isSubNutrient = [update valueForKey:kNutrientIsSubNutrient];
	}

	if ([update objectForKey:kNutrientUnits] != (id)[NSNull null]) {
		self.units = [update valueForKey:kNutrientUnits];
	}
}

// nutrientNamesCollection can be an NSArray, NSSet, or NSDictionary, or mutable variants of each (dictionaries use the values, not keys)
+ (NSArray *)orderedNutrientsWithNames:(id)nutrientNamesCollection inMOC:(NSManagedObjectContext *)moc
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K IN %@", NutrientMOAttributes.name, nutrientNamesCollection];

	return [NutrientMO MR_findAllSortedBy:NutrientMOAttributes.sortOrder ascending:YES withPredicate:predicate inContext:moc];
}

@end
