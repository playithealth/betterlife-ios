//
//  ShoppingListItem.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 4/8/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "_ShoppingListItem.h"

#import "BLProductItem.h"

@interface ShoppingListItem : _ShoppingListItem <BLProductItem> {}
- (void)markForSync;
- (void)setWithUpdateDictionary:(NSDictionary *)updateDict;
- (void)setWithRecommendationDictionary:(NSDictionary *)updateDict;
+ (ShoppingListItem *)itemByCloudId:(NSNumber *)cloudId inMOC:(NSManagedObjectContext *)inMOC;
+ (ShoppingListItem *)itemByBarcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC;
+ (ShoppingListItem *)itemByName:(NSString *)name andProductId:(NSNumber *)productId inMOC:(NSManagedObjectContext *)inMOC;
+ (ShoppingListItem *)addOrIncrementItemNamed:(NSString *)name productId:(NSNumber *)productId
	category:(CategoryMO *)category barcode:(NSString *)barcode quantity:(NSUInteger)quantity
	inMOC:(NSManagedObjectContext *)inMOC;
+ (ShoppingListItem *)removeOrDecrementItemNamed:(NSString *)name productId:(NSNumber *)productId
	quantity:(NSUInteger)quantity inMOC:(NSManagedObjectContext *)inMOC;
- (NSData *)toJSON;
@end
