#import "AdvisorMealPlanEntryMO.h"

#import "AdvisorMealPlanTagMO.h"
#import "NumberValue.h"
#import "User.h"

@implementation AdvisorMealPlanEntryMO

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.logTime = [[update valueForKey:kAdvisorMealPlanPhaseEntryLogtimeNumber] numberValueDecimal];
	self.nutritionPercent = [[update valueForKey:kAdvisorMealPlanPhaseEntryNutritionPercent] numberValueDecimal];
	self.unplannedMealOption = [update valueForKey:kAdvisorMealPlanPhaseEntryUnplannedMealOption];

	NSArray *inTags = [update objectForKey:kAdvisorMealPlanPhaseEntryTags];
	NSArray *tagIDs = [inTags valueForKeyPath:kAdvisorMealPlanPhaseEntryTagID];

	// delete any tags that didn't come with the update
	/* TODO!!!!!
	[self.tags enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
		NSUInteger idx = [inSets indexOfObjectPassingTest:^(id searchObj, NSUInteger idx, BOOL *stop) {
			return [[searchObj BC_numberOrNilForKey:kAdvisorMealPlanPhaseEntryTagID] isEqualToNumber:[obj cloudId]];
		}];
		if (idx == NSNotFound) {
			[self.managedObjectContext deleteObject:obj];
		}
	}];
	*/
	[self.tags enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
		if (![tagIDs containsObject:[(AdvisorMealPlanTagMO *)obj cloudId]]) {
			[self.managedObjectContext deleteObject:obj];
		}
	}];

	User *thisUser = [User currentUser];

	for (NSDictionary *tagDict in inTags) {
		NSNumber *cloudId = [[tagDict valueForKey:kAdvisorMealPlanPhaseEntryTagID] numberValueDecimal];
		AdvisorMealPlanTagMO *tagMO = [AdvisorMealPlanTagMO MR_findFirstByAttribute:AdvisorMealPlanTagMOAttributes.cloudId withValue:cloudId inContext:self.managedObjectContext];

		if (!tagMO) {
			tagMO = [AdvisorMealPlanTagMO insertInManagedObjectContext:self.managedObjectContext];
			tagMO.cloudId = cloudId;
			tagMO.loginId = thisUser.loginId;
		}

		tagMO.entry = self;

		[tagMO setWithUpdateDictionary:tagDict];
	}
}

@end
