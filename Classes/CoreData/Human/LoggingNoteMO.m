#import "LoggingNoteMO.h"

#import "NSDictionary+NumberValue.h"


@implementation LoggingNoteMO

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	self.date = [NSDate dateWithTimeIntervalSince1970:[[update valueForKey:kLoggingNoteDay] integerValue]];
	self.noteType = [update BC_stringOrNilForKey:kLoggingNoteType];
	self.note = [update BC_stringOrNilForKey:kLoggingNoteNote];
	self.source = [update BC_numberOrNilForKey:kLoggingNoteSource];
	self.updatedOn = [update BC_numberForKey:kUpdatedOn];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)setStatus:(NSString *)newStatus
{
	[self willChangeValueForKey:LoggingNoteMOAttributes.status];
	if ([newStatus isEqualToString:kStatusDelete]) {
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
			// If this meal has not yet gone to the cloud, it needs to, and that is a post
			// The POST verb on the api will still set the meal to deleted while adding it
			[self setPrimitiveValue:kStatusPost forKey:LoggingNoteMOAttributes.status];
		}
		else {
			// If you had set this meal to PUT, due to making changes, and then deleted the meal,
			// you will lose those changes, as we're going to send this as a delete
			[self setPrimitiveValue:kStatusDelete forKey:LoggingNoteMOAttributes.status];
		}
	}
	// If this is marked as a PUT, but we have no cloudId, it needs to be a POST instead
	else if ([newStatus isEqualToString:kStatusPut] && ![self.cloudId integerValue]) {
		[self setPrimitiveValue:kStatusPost forKey:LoggingNoteMOAttributes.status];
	}
	// If this is marked as a POST, but we have a cloudId, it needs to be a PUT instead
	else if ([newStatus isEqualToString:kStatusPost] && [self.cloudId integerValue]) {
		[self setPrimitiveValue:kStatusPut forKey:LoggingNoteMOAttributes.status];
	}
	else {
		// Just do the 'normal' action of setting the status to what was passed in
		[self setPrimitiveValue:newStatus forKey:LoggingNoteMOAttributes.status];
	}
	[self didChangeValueForKey:LoggingNoteMOAttributes.status];
}

#pragma mark - JSON
- (NSData *)toJSON
{
	// QuietLog(@"%s", __FUNCTION__);
	NSDictionary *jsonDict = @{
			kId : self.cloudId,
			kLoggingNoteDay : [NSNumber numberWithInt:[self.date timeIntervalSince1970]],
			kLoggingNoteType : self.noteType,
			kLoggingNoteNote : self.note
	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
