#import "UserImageMO.h"

#import "NSData+Base64.h"


@interface UserImageMO ()

// Private interface goes here.

@end


@implementation UserImageMO

- (void)setUserImageWithUIImage:(UIImage *)userImage
{
	[self willChangeValueForKey:UserImageMOAttributes.imageData];
	
	NSData *imageAsData = UIImagePNGRepresentation(userImage);
	[self setImageData:imageAsData];
	[self setPrimitiveValue:imageAsData forKey:UserImageMOAttributes.imageData];
	[self didChangeValueForKey:UserImageMOAttributes.imageData];
}

- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		// currently this API only supports POST
		self.status = kStatusPost;
	}
}

#pragma mark - JSON
- (NSData *)toJSON
{
	// {"recipe_id":"69","mime_encoded_image":"<image data>"

	NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
	if (self.imageTypeValue == kUserImageTypeUserProfile) {
		jsonDict[@"profile"] = @1;
	}
	else if (self.imageTypeValue == kUserImageTypeRecipe) {
		jsonDict[@"recipe_id"] = self.imageId;
	}
	else if (self.imageTypeValue == kUserImageTypeProduct) {
		jsonDict[@"product_id"] = self.imageId;
	}
	
	NSString *encodedImage = [NSData base64EncodeData:self.imageData];
	
	jsonDict[@"mime_encoded_image"] = encodedImage;

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

@end
