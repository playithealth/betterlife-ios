#import "_ActivityRecordMO.h"

@interface ActivityRecordMO : _ActivityRecordMO {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)updateFromSearchDict:(NSDictionary *)searchDict inManagedObjectContext:(NSManagedObjectContext *)context;
- (NSNumber *)totalCalories;
@end
