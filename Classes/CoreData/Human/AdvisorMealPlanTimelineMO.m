#import "AdvisorMealPlanTimelineMO.h"

#import "AdvisorMealPlanMO.h"
#import "BCUrlFactory.h"
#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"
#import "User.h"

@implementation AdvisorMealPlanTimelineMO

+ (NSString *)syncName {
	return kSyncAdvisorMealPlans;
}

+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict {
	return [updateDict BC_numberForKey:kMealPlannerId];
}

+ (NSString *)statusString {
	return kSyncUpdateHumanReadable_MealPlans;
}

+ (NSURL *)urlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit {
	return [BCUrlFactory advisorMealPlanTimelineURL];
}

+ (NSPredicate *)userPredicate {
	return [NSPredicate predicateWithFormat:@"%K.%K = %@", AdvisorPlanTimelineMORelationships.plan,
		AdvisorPlanMOAttributes.loginId, [User loginId]];
}

#pragma mark - sync from cloud
+ (void)setWithUpdatesArray:(NSArray *)updates forLogin:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)inMOC
{
	// Straight up delete strategy for timelines
	[self BL_syncUpdateUsingDeleteStrategyForLogin:loginId withUpdates:updates inMOC:inMOC];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	[super setWithUpdateDictionary:update];
	self.plan = [AdvisorMealPlanMO MR_findFirstByAttribute:AdvisorPlanMOAttributes.cloudId
		withValue:[update BC_numberForKey:kMealPlannerId] inContext:self.managedObjectContext];
}

@end
