#import "ActivityPlanMO.h"

#import "BCUrlFactory.h"
#import "NSCalendar+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NSManagedObject+BLSync.h"

@interface ActivityPlanMO ()
@end

@implementation ActivityPlanMO

+ (NSString *)syncName {
	return kSyncActivityPlan;
}

+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict {
	return [updateDict BC_numberForKey:kId];
}

+ (NSString *)statusString {
	return kSyncUpdateHumanReadable_ActivityPlans;
}

+ (NSURL *)urlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit {
	return [BCUrlFactory activityPlanUrlNewerThan:timestamp];
}

+ (NSPredicate *)userPredicate {
	return [self BL_loginIdPredicate];
}

#pragma mark - sync from cloud
+ (void)setWithUpdatesArray:(NSArray *)updates forLogin:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)inMOC {
	[self BL_syncUpdateForLogin:loginId withUpdates:updates inMOC:inMOC];
}

- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	[super setWithUpdateDictionary:update];
}

#pragma mark - Convenience methods
+ (NSArray *)activityPlansForLoginId:(NSNumber *)loginId forDate:(NSDate *)date inContext:(NSManagedObjectContext *)context {
	NSDate *startDate = [[NSCalendar currentCalendar] BC_startOfDate:date];
	NSDate *endDate = [[NSCalendar currentCalendar] BC_endOfDate:date];

	return [ActivityPlanMO MR_findAllWithPredicate:[NSPredicate
		predicateWithFormat:@"%K = %@ AND %K <> %@ AND %K BETWEEN {%@, %@}",
			ActivityMOAttributes.loginId, loginId,
			ActivityRecordMOAttributes.status, kStatusDelete,
			ActivityRecordMOAttributes.targetDate, startDate, endDate]
		inContext:context];
}

#pragma mark - sync to cloud
- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId isEqualToNumber:@0]) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

// If this will sync data to the cloud, implement this method
/*
- (NSData *)toJSON
{
	NSDictionary *jsonDict = @{

	};

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}
*/

@end
