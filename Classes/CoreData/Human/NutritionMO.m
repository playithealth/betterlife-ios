#import "NutritionMO.h"

#import "RecipeMO.h"
#import "NSDictionary+NumberValue.h"

@implementation NutritionMO

+ (id)nutritionById:(NSNumber *)nutritionId inMOC:(NSManagedObjectContext *)inMOC
{
	return [NutritionMO MR_findFirstByAttribute:NutritionMOAttributes.nutritionId withValue:nutritionId inContext:inMOC];
}

// Convenience method for dropping in a .nutrition relationship filled out with data
+ (NutritionMO *)insertNutritionWithDictionary:(NSDictionary *)nutritionDictionary inContext:(NSManagedObjectContext *)moc
{
	NutritionMO *nutritionMO = [NutritionMO insertInManagedObjectContext:moc];
	[nutritionMO setWithUpdateDictionary:nutritionDictionary];

	return nutritionMO;
}

- (void)setWithUpdateDictionary:(NSDictionary *)update 
{
	NSAssert([update isKindOfClass:[NSDictionary class]], @"NutritionMO setWithUpdateDictionary received a bad argument");

	self.nutritionId = [update BC_numberOrNilForKey:kNutritionId];
	self.calories = [update BC_numberOrNilForKey:kNutritionCalories];
	self.caloriesFromFat = [update BC_numberOrNilForKey:kNutritionCaloriesFromFat];
	self.totalFat = [update BC_numberOrNilForKey:kNutritionTotalFat];
	self.transFat = [update BC_numberOrNilForKey:kNutritionTransFat];
	self.saturatedFat = [update BC_numberOrNilForKey:kNutritionSaturatedFat];
	self.saturatedFatCalories = [update BC_numberOrNilForKey:kNutritionSaturatedFatCalories];
	self.polyunsaturatedFat = [update BC_numberOrNilForKey:kNutritionPolyunsaturatedFat];
	self.monounsaturatedFat = [update BC_numberOrNilForKey:kNutritionMonounsaturatedFat];
	self.cholesterol = [update BC_numberOrNilForKey:kNutritionCholesterol];
	self.sodium = [update BC_numberOrNilForKey:kNutritionSodium];
	self.potassium = [update BC_numberOrNilForKey:kNutritionPotassium];
	self.totalCarbohydrates = [update BC_numberOrNilForKey:kNutritionTotalCarbohydrates];
	self.otherCarbohydrates = [update BC_numberOrNilForKey:kNutritionOtherCarbohydrates];
	self.dietaryFiber = [update BC_numberOrNilForKey:kNutritionDietaryFiber];
	self.solubleFiber = [update BC_numberOrNilForKey:kNutritionSolubleFiber];
	self.insolubleFiber = [update BC_numberOrNilForKey:kNutritionInsolubleFiber];
	self.sugars = [update BC_numberOrNilForKey:kNutritionSugars];
	self.sugarsAlcohol = [update BC_numberOrNilForKey:kNutritionSugarsAlcohol];
	self.protein = [update BC_numberOrNilForKey:kNutritionProtein];
	self.vitaminAPercent = [update BC_numberOrNilForKey:kNutritionVitaminAPercent];
	self.vitaminCPercent = [update BC_numberOrNilForKey:kNutritionVitaminCPercent];
	self.calciumPercent = [update BC_numberOrNilForKey:kNutritionCalciumPercent];
	self.ironPercent = [update BC_numberOrNilForKey:kNutritionIronPercent];
}

- (void)copyFromNutritionMO:(NutritionMO *)nutrition
{
	self.calories = nutrition.calories;
	self.caloriesFromFat = nutrition.caloriesFromFat;
	self.totalFat = nutrition.totalFat;
	self.transFat = nutrition.transFat;
	self.saturatedFat = nutrition.saturatedFat;
	self.saturatedFatCalories = nutrition.saturatedFatCalories;
	self.polyunsaturatedFat = nutrition.polyunsaturatedFat;
	self.monounsaturatedFat = nutrition.monounsaturatedFat;
	self.cholesterol = nutrition.cholesterol;
	self.sodium = nutrition.sodium;
	self.potassium = nutrition.potassium;
	self.totalCarbohydrates = nutrition.totalCarbohydrates;
	self.otherCarbohydrates = nutrition.otherCarbohydrates;
	self.dietaryFiber = nutrition.dietaryFiber;
	self.solubleFiber = nutrition.solubleFiber;
	self.insolubleFiber = nutrition.insolubleFiber;
	self.sugars = nutrition.sugars;
	self.sugarsAlcohol = nutrition.sugarsAlcohol;
	self.protein = nutrition.protein;
	self.vitaminAPercent = nutrition.vitaminAPercent;
	self.vitaminCPercent = nutrition.vitaminCPercent;
	self.calciumPercent = nutrition.calciumPercent;
	self.ironPercent = nutrition.ironPercent;
}

#pragma mark - convenience methods
- (NSSet *)nutrientsWithValues
{
	NSMutableSet *nutrients = [[NSMutableSet alloc] init];
	for (NSString *keyName in [NutritionMO allKeys]) {
		NSNumber *value = [self valueForKey:keyName];
		if (value) {
			[nutrients addObject:keyName];
		}
	}

	return nutrients;
}

- (NSMutableDictionary *)nutrientDictWithValues
{
	NSMutableDictionary *nutrients = [[NSMutableDictionary alloc] init];
	for (NSString *keyName in [NutritionMO allKeys]) {
		NSNumber *value = [self valueForKey:keyName];
		if (value) {
			[nutrients setValue:value forKey:keyName];
		}
	}

	return nutrients;
}

+ (NSArray *)allKeys
{
	return @[
		NutritionMOAttributes.calciumPercent,
		NutritionMOAttributes.calories,
		NutritionMOAttributes.caloriesFromFat,
		NutritionMOAttributes.cholesterol,
		NutritionMOAttributes.dietaryFiber,
		NutritionMOAttributes.insolubleFiber,
		NutritionMOAttributes.ironPercent,
		NutritionMOAttributes.monounsaturatedFat,
		NutritionMOAttributes.nutritionId,
		NutritionMOAttributes.otherCarbohydrates,
		NutritionMOAttributes.polyunsaturatedFat,
		NutritionMOAttributes.potassium,
		NutritionMOAttributes.protein,
		NutritionMOAttributes.saturatedFat,
		NutritionMOAttributes.saturatedFatCalories,
		NutritionMOAttributes.sodium,
		NutritionMOAttributes.solubleFiber,
		NutritionMOAttributes.sugars,
		NutritionMOAttributes.sugarsAlcohol,
		NutritionMOAttributes.totalCarbohydrates,
		NutritionMOAttributes.totalFat,
		NutritionMOAttributes.transFat,
		NutritionMOAttributes.vitaminAPercent,
		NutritionMOAttributes.vitaminCPercent ];
}

+ (NSString *)localKeyFromCloudKey:(NSString *)cloudKey
{
	static NSDictionary *map = nil;
	if (!map) {
		map = @{
			@"calcium_pct" : NutritionMOAttributes.calciumPercent,
			@"calories" : NutritionMOAttributes.calories,
			@"calories_from_fat" : NutritionMOAttributes.caloriesFromFat,
			@"cholesterol" : NutritionMOAttributes.cholesterol,
			@"dietary_fiber" : NutritionMOAttributes.dietaryFiber,
			@"insoluble_fiber" : NutritionMOAttributes.insolubleFiber,
			@"iron_pct" : NutritionMOAttributes.ironPercent,
			@"monounsaturated_fat" : NutritionMOAttributes.monounsaturatedFat,
			@"other_carbohydrate" : NutritionMOAttributes.otherCarbohydrates,
			@"polyunsaturated_fat" : NutritionMOAttributes.polyunsaturatedFat,
			@"potassium" : NutritionMOAttributes.potassium,
			@"protein" : NutritionMOAttributes.protein,
			@"saturated_fat" : NutritionMOAttributes.saturatedFat,
			@"saturated_fat_calories" : NutritionMOAttributes.saturatedFatCalories,
			@"sodium" : NutritionMOAttributes.sodium,
			@"soluble_fiber" : NutritionMOAttributes.solubleFiber,
			@"sugars" : NutritionMOAttributes.sugars,
			@"sugar_alcohol" : NutritionMOAttributes.sugarsAlcohol,
			@"total_carbohydrate" : NutritionMOAttributes.totalCarbohydrates,
			@"total_fat" : NutritionMOAttributes.totalFat,
			@"trans_fat" : NutritionMOAttributes.transFat,
			@"vitamin_a_pct" : NutritionMOAttributes.vitaminAPercent,
			@"vitamin_c_pct" : NutritionMOAttributes.vitaminCPercent
		};
	}

	return [map valueForKey:cloudKey];
}

- (void)toJSONDict:(NSMutableDictionary *)jsonDict
{
	[jsonDict setValue:(self.calories ? self.calories : [NSNull null]) forKey:kNutritionCalories];
	[jsonDict setValue:(self.caloriesFromFat ? self.caloriesFromFat : [NSNull null]) forKey:kNutritionCaloriesFromFat];
	[jsonDict setValue:(self.totalFat ? self.totalFat : [NSNull null]) forKey:kNutritionTotalFat];
	[jsonDict setValue:(self.transFat ? self.transFat : [NSNull null]) forKey:kNutritionTransFat];
	[jsonDict setValue:(self.saturatedFat ? self.saturatedFat : [NSNull null]) forKey:kNutritionSaturatedFat];
	[jsonDict setValue:(self.saturatedFatCalories ? self.saturatedFatCalories : [NSNull null]) forKey:kNutritionSaturatedFatCalories];
	[jsonDict setValue:(self.polyunsaturatedFat ? self.polyunsaturatedFat : [NSNull null]) forKey:kNutritionPolyunsaturatedFat];
	[jsonDict setValue:(self.monounsaturatedFat ? self.monounsaturatedFat : [NSNull null]) forKey:kNutritionMonounsaturatedFat];
	[jsonDict setValue:(self.cholesterol ? self.cholesterol : [NSNull null]) forKey:kNutritionCholesterol];
	[jsonDict setValue:(self.sodium ? self.sodium : [NSNull null]) forKey:kNutritionSodium];
	[jsonDict setValue:(self.potassium ? self.potassium : [NSNull null]) forKey:kNutritionPotassium];
	[jsonDict setValue:(self.totalCarbohydrates ? self.totalCarbohydrates : [NSNull null]) forKey:kNutritionTotalCarbohydrates];
	[jsonDict setValue:(self.otherCarbohydrates ? self.otherCarbohydrates : [NSNull null]) forKey:kNutritionOtherCarbohydrates];
	[jsonDict setValue:(self.dietaryFiber ? self.dietaryFiber : [NSNull null]) forKey:kNutritionDietaryFiber];
	[jsonDict setValue:(self.solubleFiber ? self.solubleFiber : [NSNull null]) forKey:kNutritionSolubleFiber];
	[jsonDict setValue:(self.insolubleFiber ? self.insolubleFiber : [NSNull null]) forKey:kNutritionInsolubleFiber];
	[jsonDict setValue:(self.sugars ? self.sugars : [NSNull null]) forKey:kNutritionSugars];
	[jsonDict setValue:(self.sugarsAlcohol ? self.sugarsAlcohol : [NSNull null]) forKey:kNutritionSugarsAlcohol];
	[jsonDict setValue:(self.protein ? self.protein : [NSNull null]) forKey:kNutritionProtein];
	[jsonDict setValue:(self.vitaminAPercent ? self.vitaminAPercent : [NSNull null]) forKey:kNutritionVitaminAPercent];
	[jsonDict setValue:(self.vitaminCPercent ? self.vitaminCPercent : [NSNull null]) forKey:kNutritionVitaminCPercent];
	[jsonDict setValue:(self.calciumPercent ? self.calciumPercent : [NSNull null]) forKey:kNutritionCalciumPercent];
	[jsonDict setValue:(self.ironPercent ? self.ironPercent : [NSNull null]) forKey:kNutritionIronPercent];
}

#pragma mark - virtual properties to cover the service names
- (NSNumber *)calories_from_fat
{
	return self.caloriesFromFat;
}
- (NSNumber *)saturated_fat_calories
{
	return self.saturatedFatCalories;
}
- (NSNumber *)total_fat
{
	return self.totalFat;
}
- (NSNumber *)saturated_fat
{
	return self.saturatedFat;
}
- (NSNumber *)polyunsaturated_fat
{
	return self.polyunsaturatedFat;
}
- (NSNumber *)monounsaturated_fat
{
	return self.monounsaturatedFat;
}
- (NSNumber *)trans_fat
{
	return self.transFat;
}
- (NSNumber *)total_carbohydrate
{
	return self.totalCarbohydrates;
}
- (NSNumber *)dietary_fiber
{
	return self.dietaryFiber;
}
- (NSNumber *)soluble_fiber
{
	return self.solubleFiber;
}
- (NSNumber *)insoluble_fiber
{
	return self.insolubleFiber;
}
- (NSNumber *)sugar_alcohol
{
	return self.sugarsAlcohol;
}
- (NSNumber *)other_carbohydrate
{
	return self.otherCarbohydrates;
}
- (NSNumber *)vitamin_a_pct
{
	return self.vitaminAPercent;
}
- (NSNumber *)vitamin_c_pct
{
	return self.vitaminCPercent;
}
- (NSNumber *)calcium_pct
{
	return self.calciumPercent;
}
- (NSNumber *)iron_pct
{
	return self.ironPercent;
}

@end
