#import "_MealPlannerShoppingListItem.h"

@interface MealPlannerShoppingListItem : _MealPlannerShoppingListItem {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (NSDictionary *)toDictionary;
@end
