//
//  PantryItem.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/25/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "_PantryItem.h"

#import "BLProductItem.h"

@interface PantryItem : _PantryItem <BLProductItem> {}
- (void)setWithUpdateDictionary:(NSDictionary *)update;
- (void)markForSync;
+ (PantryItem *)itemByBarcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC;
+ (PantryItem *)itemByName:(NSString *)name andProductId:(NSNumber *)productId inMOC:(NSManagedObjectContext *)inMOC;
+ (PantryItem *)addOrIncrementItemNamed:(NSString *)name productId:(NSNumber *)productId categoryId:(NSNumber *)categoryId barcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC;
- (NSData *)toJSON;
@end
