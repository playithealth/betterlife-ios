#import "HelpView.h"

#import "User.h"

@implementation HelpView

+ (id)helpViewByName:(NSString *)name inMOC:(NSManagedObjectContext *)inMOC
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:
		@"loginId = %@ AND name = %@", [[User currentUser] loginId], name];
	HelpView *helpView = [HelpView MR_findFirstWithPredicate:predicate inContext:inMOC];

	return helpView;
}

@end
