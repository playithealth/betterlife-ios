#import "SyncUpdateMO.h"

@interface SyncUpdateMO ()

@end


@implementation SyncUpdateMO

+ (BOOL)hasPerformedInitialSync:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)moc
{
	return [[SyncUpdateMO lastUpdateForTable:@"SyncUpdate" forUser:loginId inContext:moc] boolValue];
}

+ (void)syncSucceededForUser:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)moc
{
	[self setLastUpdate:@([[NSDate date] timeIntervalSince1970]) forTable:@"SyncUpdate" forUser:loginId inContext:moc];
}

+ (SyncUpdateMO *)getSyncUpdateForTable:(NSString *)tableName forUser:(NSNumber *)loginId inContext:(NSManagedObjectContext *)moc
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((loginId = %@) OR (loginId = nil)) AND tableName = %@",
		loginId, tableName];
	return [SyncUpdateMO MR_findFirstWithPredicate:predicate inContext:moc];
}

+ (NSNumber *)lastUpdateForTable:(NSString *)tableName forUser:(NSNumber *)loginId inContext:(NSManagedObjectContext *)moc
{
	NSNumber *lastUpdate = @0;

	SyncUpdateMO *syncUpdate = [SyncUpdateMO getSyncUpdateForTable:tableName forUser:loginId inContext:moc];
	if (syncUpdate && syncUpdate.lastUpdate) {
		lastUpdate = syncUpdate.lastUpdate;
	}

	return lastUpdate;
}

+ (void)setLastUpdate:(NSNumber *)lastUpdate forTable:(NSString *)tableName forUser:(NSNumber *)loginId
	inContext:(NSManagedObjectContext *)moc
{
	SyncUpdateMO *syncUpdate = [SyncUpdateMO getSyncUpdateForTable:tableName forUser:loginId inContext:moc];

	if (!syncUpdate) {
		syncUpdate = [SyncUpdateMO insertInManagedObjectContext:moc];
		syncUpdate.loginId = loginId;
		syncUpdate.tableName = tableName;
	}

	syncUpdate.lastUpdate = lastUpdate;
}

+ (NSArray *)syncUpdatesForUser:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)moc
{
	return [SyncUpdateMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"(loginId = %@) OR (loginId = nil)", loginId]];
}
@end
