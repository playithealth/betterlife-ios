//
//  PantryItem.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/25/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "PantryItem.h"

#import "CategoryMO.h"
#import "NumberValue.h"
#import "User.h"

@implementation PantryItem

#pragma mark - JSON
- (NSData *)toJSON
{
	NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
		self.accountId, @"account_id",
		self.quantity, @"quantity",
		self.name, @"item",
		self.sourceId, @"source_id",
		self.updatedOn, @"updated_on",
		self.createdOn, @"created_on",
		nil];

	if (self.cloudId) {
		[jsonDict setValue:self.cloudId forKey:@"cloud_id"];
	}
	if (self.productId) {
		[jsonDict setValue:self.productId forKey:@"product_id"];
	}
	if (self.category) {
		[jsonDict setValue:self.category.cloudId forKey:@"category_id"];
	}

	return [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];
}

#pragma mark - convenience methods
- (void)setWithUpdateDictionary:(NSDictionary *)update
{
	// required keys
	self.cloudId = [[update valueForKey:kId] numberValueDecimal];
	self.productId = [[update valueForKey:kProductId] numberValueDecimal];
	self.name = [update valueForKey:kItemName];
	self.category = [CategoryMO categoryById:[[update valueForKey:kCategoryId] numberValueDecimal] withManagedObjectContext:self.managedObjectContext];
	self.quantity = [[update valueForKey:kQuantity] numberValueDecimal];
	self.sourceId = [[update valueForKey:kSourceId] numberValueDecimal];
	self.createdOn = [[update valueForKey:kCreatedOn] numberValueDecimal];
	self.updatedOn = [[update valueForKey:kUpdatedOn] numberValueDecimal];
	self.purchasedOn = [[update valueForKey:kPantryItemPurchasedOn] numberValueDecimal];

	// these pieces of data could be null
	if ((NSNull *)[update objectForKey:kBarcode] != [NSNull null]) {
		self.barcode = [update valueForKey:kBarcode];
	}

	if ((NSNull *)[update objectForKey:kImageId] != [NSNull null]) {
		self.imageId = [[update valueForKey:kImageId] numberValueDecimal];
	}

	self.calories = [[update objectForKey:kNutritionCalories] numberValueDecimal];
	self.protein = [[update objectForKey:kNutritionProtein] numberValueDecimal];
	self.carbohydrates = [[update objectForKey:kNutritionTotalCarbohydrates] numberValueDecimal];
	self.fat = [[update objectForKey:kNutritionTotalFat] numberValueDecimal];
	self.saturatedFat = [[update objectForKey:kNutritionSaturatedFat] numberValueDecimal];
	self.sodium = [[update objectForKey:kNutritionSodium] numberValueDecimal];
	self.cholesterol = [[update objectForKey:kNutritionCholesterol] numberValueDecimal];
	self.caloriesFromFat = [[update objectForKey:kNutritionCaloriesFromFat] numberValueDecimal];

	// set the status of this item to OK since it came from the API
	self.status = kStatusOk;
}

- (void)markForSync
{
	if (![self.status isEqualToString:kStatusPost]) {
		if ([self.cloudId isEqualToNumber:[NSNumber numberWithInt:0]]) {
			self.status = kStatusPost;
		}
		else {
			self.status = kStatusPut;
		}
	}
}

+ (PantryItem *)itemByBarcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC
{
	NSString *regex = [NSString stringWithFormat:@"0*%@", barcode];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:
		@"barcode MATCHES %@ AND status <> %@ AND accountId = %@", regex, kStatusDelete, [[User currentUser] accountId]];

	return [PantryItem MR_findFirstWithPredicate:predicate inContext:inMOC];
}

+ (PantryItem *)itemByName:(NSString *)name andProductId:(NSNumber *)productId inMOC:(NSManagedObjectContext *)inMOC
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:
		@"accountId = %@ AND productId = %@ AND name = %@", [[User currentUser] accountId], productId, name];

	return [PantryItem MR_findFirstWithPredicate:predicate inContext:inMOC];
}

+ (PantryItem *)addOrIncrementItemNamed:(NSString *)name productId:(NSNumber *)productId categoryId:(NSNumber *)categoryId barcode:(NSString *)barcode inMOC:(NSManagedObjectContext *)inMOC
{
	User *thisUser = [User currentUser];

	// check for an existing item by productId and name
	PantryItem *item = [PantryItem itemByName:name andProductId:productId inMOC:inMOC];

	if (item) {
		item.quantity = [NSNumber numberWithInteger:[item.quantity integerValue] + 1];
		[item markForSync];
	}
	else {
		item = [NSEntityDescription insertNewObjectForEntityForName:[PantryItem entityName] inManagedObjectContext:inMOC];

		item.name = name;
		item.quantity = @1;
		item.accountId = thisUser.accountId;
		item.productId = productId;
		item.barcode = barcode;
		item.purchasedOn = [NSNumber numberWithInteger:[[NSDate date] timeIntervalSince1970]];
		if ([categoryId integerValue] <= 0) {
			// Default this item's category to 'other'
			item.category = [CategoryMO categoryById:@1 withManagedObjectContext:inMOC];
		}
		else {
			item.category = [CategoryMO categoryById:categoryId withManagedObjectContext:inMOC];
		}
		[item markForSync];
	}

	return item;
}

@end
