#import "_ActivityLogMO.h"

#import "BLSyncEntity.h"

@interface ActivityLogMO : _ActivityLogMO <BLSyncEntity> {}
+ (id)insertFromSearchDict:(NSDictionary *)searchDict inManagedObjectContext:(NSManagedObjectContext *)context;
+ (id)activityLogFromWorkoutActivity:(ActivityMO *)workoutActivityMO
	inManagedObjectContext:(NSManagedObjectContext *)context;
- (void)setWithUpdateDictionary:(NSDictionary *)update;
@end
