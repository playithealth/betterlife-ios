//
//  ShoppingListViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/9/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCColorButton.h"
#import "BCViewController.h"
#import "ShoppingListSearchViewController.h"
#import "ShoppingListItemCell.h"
#import "ShoppingListItemDetailViewController.h"
#import "StoreSearchViewController.h"

@interface ShoppingListViewController : BCViewController 
<NSFetchedResultsControllerDelegate, BCSearchViewDelegate,
	ShoppingListItemDetailViewDelegate,	StoreSelectionDelegate, UITableViewDataSource,
	UITableViewDelegate, ZBarReaderDelegate>

@end
