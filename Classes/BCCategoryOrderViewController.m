//
//  CategoryOrderViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 5/2/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "BCCategoryOrderViewController.h"

#import "BCAppDelegate.h"
#import "CategoryMO.h"
#import "Store.h"
#import "StoreCategory.h"
#import "User.h"

@interface BCCategoryOrderViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation BCCategoryOrderViewController

#pragma mark - view lifecycle
- (void)viewDidLoad
{
	[self.tableView setEditing:YES animated:NO];

	[super viewDidLoad];
}

#pragma mark - local methods
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	StoreCategory *storeCategory = [self.fetchedResultsController objectAtIndexPath:indexPath];
	cell.textLabel.text = storeCategory.category.name;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSArray *sections = [self.fetchedResultsController sections];
	NSUInteger count = 0;

	if ([sections count]) {
		id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
		count = [sectionInfo numberOfObjects];
	}
	return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellId = @"CategoryOrderCell";

	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
	NSMutableArray *categories = [[self.fetchedResultsController fetchedObjects] mutableCopy];

	// Grab the item we're moving.
	NSManagedObject *category = [self.fetchedResultsController objectAtIndexPath:fromIndexPath];

	// Remove the object we're moving from the array.
	[categories removeObject:category];

	// Now re-insert it at the destination.
	[categories insertObject:category atIndex:[toIndexPath row]];

	// All of the objects are now in their correct order. Update each
	// object's displayOrder field by iterating through the array.
	NSInteger sortOrder = 0;
	for (NSManagedObject *mo in categories) {
		[mo setValue:@(sortOrder++) forKey:@"sortOrder"];
	}

	categories = nil;

	// Ensure the status on the user store is set
	self.userStore.storeCategoryStatus = kStatusPut;

	NSError *error = nil;
	if (![[self.fetchedResultsController managedObjectContext] save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}
	else {
		// No error on saving, fire a notification that we updated this store's categories
		[[NSNotificationCenter defaultCenter] postNotificationName:UserStoresDidChangeStoreCategories
			object:self userInfo:[NSDictionary dictionaryWithObject:self.userStore.storeEntityId
			forKey:UserStoresDidChangeStoreId]];
	}

	if (![self.fetchedResultsController performFetch:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

#pragma mark - UITableViewDelegate
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return UITableViewCellEditingStyleNone;
}

#pragma mark - Fetched results controller
- (NSFetchedResultsController *)fetchedResultsController
{
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}

	User *thisUser = [User currentUser];

	[self.userStore populateStoreCategories];

	// Create the fetch request for the entity.
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	fetchRequest.entity = [StoreCategory entityInManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"store.accountId = %@ and store.storeEntityId = %@",
		thisUser.accountId, self.userStore.storeEntityId];
	[fetchRequest setFetchBatchSize:20];
	[fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES]]];

	// create and init the fetched results controller
	NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]
		initWithFetchRequest:fetchRequest
		managedObjectContext:self.managedObjectContext
		sectionNameKeyPath:nil
		cacheName:nil];
	aFetchedResultsController.delegate = self;

	self.fetchedResultsController = aFetchedResultsController;

	NSError *error = nil;
	if (![_fetchedResultsController performFetch:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	return _fetchedResultsController;
}

@end
