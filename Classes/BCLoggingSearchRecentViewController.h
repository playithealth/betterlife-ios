//
//  BCLoggingSearchRecentViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/9/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCLoggingSearchRecentViewController : UITableViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSDate *logDate;
@property (assign, nonatomic) NSNumber *logTime;
@end
