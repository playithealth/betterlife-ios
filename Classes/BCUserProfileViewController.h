//
//  BCUserProfileViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCPopupView.h"
#import "UserProfileMO.h"

@interface BCUserProfileViewController : UITableViewController <BCPopupViewDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) UserProfileMO *userProfile;

@end

