//
//  BLMigrationViewController.m
//  BuyerCompass
//
//  Created by Greg Goodrich on 3/25/14.
//  Copyright (c) 2014 BuyerCompass, LLC. All rights reserved.
//

#import "BLMigrationViewController.h"

@interface BLMigrationViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *updatingLabel;

@end

@implementation BLMigrationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.activityIndicator.hidden = NO;
    self.updatingLabel.hidden = NO;
    
	/*
    __typeof__(self) __weak weakSelf = self;
    // Only show the activity indicator and text after 1 second, however, this doesn't work with migration, as the
    // code block for migration is already running, and on the same queue, so we will never get here until it is too
    // late, as this view should be going away by then.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        weakSelf.activityIndicator.hidden = NO;
        weakSelf.updatingLabel.hidden = NO;
    });
    */

}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showUpdateNotificationsFrom:(id)notifyingObject
{
	[[NSNotificationCenter defaultCenter] addObserverForName:kSyncUpdateUserStatusNotification object:nil queue:nil
		usingBlock:^(NSNotification *notification) {
			NSString *humanReadable = [notification.userInfo objectForKey:kSyncUpdateHumanReadableStatusKey];
			if (humanReadable != nil)
				self.updatingLabel.text = humanReadable;
		}];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
