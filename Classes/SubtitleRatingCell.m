//
//  SubtitleRatingCell.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/2/2011
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "SubtitleRatingCell.h"
#import "StarRatingControl.h"

static const NSInteger kAccessoryWidthAllowance = 30; ///< Width of a disclosure indicator or button
static const NSInteger kMargin = 8;
static const NSInteger kImageWidth = 60;
static const NSInteger kImageMargin = 6;
static const NSInteger kLabelWidth = 240;
static const NSInteger kLabelLineHeight = 22;

@implementation SubtitleRatingCell
@synthesize imageView;
@synthesize activityIndicator;
@synthesize nameLabel;
@synthesize detailLabel;
@synthesize ratingControl;
@synthesize accessoryButton;

- (id)initWithStyle:(UITableViewCellStyle)style
reuseIdentifier:(NSString *)reuseIdentifier
{
	if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
		self.imageView = [[UIImageView alloc] initWithFrame:
			CGRectMake(kMargin, 0, kImageWidth, kImageWidth)];
		[self.contentView addSubview:self.imageView];

		self.activityIndicator = [[UIActivityIndicatorView alloc]
			initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		self.activityIndicator.hidden = YES;
		self.activityIndicator.hidesWhenStopped = YES;
		self.activityIndicator.center = self.imageView.center;
		[self.contentView addSubview:self.activityIndicator];

		self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kMargin, 0, 
			kLabelWidth, kLabelLineHeight)];
		self.nameLabel.textAlignment = NSTextAlignmentLeft;
		self.nameLabel.adjustsFontSizeToFitWidth = YES;
		self.nameLabel.textColor = [UIColor blackColor];
		self.nameLabel.font = [UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
		[self.contentView addSubview:self.nameLabel];

		self.ratingControl = [[StarRatingControl alloc] 
			initWithFrame:CGRectMake(kMargin,
			self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height,
			73, 16)];
			//initWithFrame:CGRectMake(245, 5, 73, 16)] autorelease];
		self.ratingControl.userInteractionEnabled = NO;
		self.ratingControl.emptyStar = [UIImage imageNamed:@"RatingStar-Empty.png"];
		self.ratingControl.fullStar = [UIImage imageNamed:@"RatingStar-Gold.png"];

		self.detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(kMargin, 
			self.ratingControl.frame.origin.y + self.ratingControl.frame.size.height,
			kLabelWidth, kLabelLineHeight)];
			//kLabelLineHeight, kLabelWidth, kLabelLineHeight)] autorelease];
		self.detailLabel.textAlignment = NSTextAlignmentLeft;
		self.detailLabel.adjustsFontSizeToFitWidth = YES;
		self.detailLabel.textColor = [UIColor grayColor];
		self.detailLabel.font = [UIFont systemFontOfSize:[UIFont labelFontSize] - 3];
		[self.contentView addSubview:self.detailLabel];

		[self.contentView addSubview:self.ratingControl];
	}
	return self;
}
- (void)willTransitionToState:(UITableViewCellStateMask)state
{
    [super willTransitionToState:state];

    if ((state & UITableViewCellStateShowingDeleteConfirmationMask) == UITableViewCellStateShowingDeleteConfirmationMask) {
		self.accessoryButton.hidden = YES;
    }
	else {
		self.accessoryButton.hidden = NO;
	}
}
- (void)layoutSubviews
{
	NSInteger leftMargin = kMargin;
	NSInteger labelWidth = kLabelWidth;
	NSInteger imageWidth = self.frame.size.height - (2 * kImageMargin);
	// Determine the left margin for the text fields by checking to see if we have an image
	if (self.imageView.image) {
		NSInteger y = (self.frame.size.height - imageWidth) / 2;
		self.imageView.frame = CGRectMake(kImageMargin, y, imageWidth, imageWidth);
		leftMargin = imageWidth + (2 * kImageMargin);

		self.activityIndicator.center = self.imageView.center;
	}

	// Determine the width for the text fields by checking for an accessory
	if ((self.accessoryType == UITableViewCellAccessoryNone) && !self.accessoryView) {
		// No accessory being used, so allow the remaining width to be used for the labels
		labelWidth = self.frame.size.width - kMargin - leftMargin;
	}
	else {
		// Adjust the right margin of the labels for the accessory
		if (self.accessoryView) {
			// Adjust by the width of the accessory view
			labelWidth = self.accessoryView.frame.origin.x - leftMargin - kMargin;
		}
		else {
			// Allow enough space for a disclosure indicator, button, or checkmark
			labelWidth = self.frame.size.width - leftMargin - kAccessoryWidthAllowance;
		}
	}

	if ([self.nameLabel.text sizeWithFont:self.nameLabel.font].width <= self.nameLabel.frame.size.width) {
		self.nameLabel.numberOfLines = 1;
	}
	else {
		self.nameLabel.numberOfLines = 2;
	}

	NSInteger nameLabelHeight = kLabelLineHeight * self.nameLabel.numberOfLines;
	NSInteger totalheight = nameLabelHeight + 16 + ([self.detailLabel.text length] ? kLabelLineHeight : 0);
	NSInteger startingy = (self.frame.size.height - totalheight) / 2;
	self.nameLabel.frame = CGRectMake(leftMargin, startingy, labelWidth, nameLabelHeight);

	self.ratingControl.frame = CGRectMake(leftMargin,
		self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height, 73, 16);

	if ([self.detailLabel.text length]) {
		self.detailLabel.frame = CGRectMake(leftMargin,
			self.ratingControl.frame.origin.y + self.ratingControl.frame.size.height,
			labelWidth, kLabelLineHeight);
		self.detailLabel.hidden = NO;
	}
	else {
		self.detailLabel.hidden = YES;
	}

	[super layoutSubviews];
}

- (void)setAccessoryType:(UITableViewCellAccessoryType)accessoryType
{
	self.accessoryButton = nil;
	
	// Just pass the call through
	[super setAccessoryType:accessoryType];
}
@end
