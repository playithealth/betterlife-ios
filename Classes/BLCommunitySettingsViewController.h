//
//  BLCommunitySettingsViewController.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 9/19/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CommunityMO;

@interface BLCommunitySettingsViewController : UIViewController
@property (strong, nonatomic) CommunityMO *community;
@end
