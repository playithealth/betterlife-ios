//
//  ProductSearchResultsCell.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/14/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "ProductSearchResultsCell.h"

#import "StarRatingControl.h"

@implementation ProductSearchResultsCell
@synthesize nameLabel;
@synthesize sizeLabel;
@synthesize productImageView;
@synthesize alertImageView;
@synthesize lifestyleImageView;
@synthesize ratingControl;


@end
