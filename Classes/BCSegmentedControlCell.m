//
//  BCSegmentedControlCell.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/10/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCSegmentedControlCell.h"

#import "UIColor+Additions.h"

@implementation BCSegmentedControlCell

- (id)initWithItems:(NSArray*)items reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
		self.bcTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 25)];
		self.bcTextLabel.textAlignment = NSTextAlignmentRight;
		self.bcTextLabel.font = [UIFont boldSystemFontOfSize:14];
		self.bcTextLabel.textColor = [UIColor BC_nonEditableTextColor];
		self.bcTextLabel.backgroundColor = [UIColor BC_groupedTableViewCellBackgroundColor];
		[self.contentView addSubview:self.bcTextLabel];

		_segmentedControl = [[UISegmentedControl alloc] initWithItems:items];
		_segmentedControl.frame = CGRectMake(115, 7, 180, 30);
		[self.contentView addSubview:_segmentedControl];
	}
	return self;
}

@end
