//
//  User.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/22/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "User.h"

#import "BCAppDelegate.h"
#import "KeychainItemWrapper.h"

#define kFilename			@"login"
#define kDataKey			@"Data"

@interface User ()
@property (nonatomic, strong) KeychainItemWrapper *userKeychainItem;
@property (weak, nonatomic, readwrite) UserProfileMO *userProfile;
@end

@implementation User

- (id)init {
	return [self initWithCoder:nil];
}

- (id)initWithCoder:(NSCoder *)decoder
{
	self = [super init];
	if (self) {
		self.userKeychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"User" accessGroup:nil];
		if (decoder) {
			self.loginId = [decoder decodeObjectForKey:kLoginUserId];
			self.email = [decoder decodeObjectForKey:kEmail];
			self.oauthAccessToken = [decoder decodeObjectForKey:kLoginOAuthAccessToken];
			self.oauthSecret = [decoder decodeObjectForKey:kLoginOAuthSecret];
		}
	}
	return self;
}

- (BOOL)loginIsValid
{
	return (([self.oauthAccessToken length] > 0) && ([self.oauthSecret length] > 0));
}

+ (User *)currentUser
{
	static dispatch_once_t once;
	static User *sharedInstance = nil;

	dispatch_once(&once, ^{
		sharedInstance = [[User alloc] init];

		// Check to see if we got values from keychain, as if not, we may need to load them from the  old files
		if (!sharedInstance.email || [sharedInstance.email isEqualToString:@""]) {
			User *oldUser = [User loadUser];
			if (oldUser) {
				sharedInstance.loginId = oldUser.loginId;
				sharedInstance.email = oldUser.email;
				sharedInstance.oauthAccessToken = oldUser.oauthAccessToken;
				sharedInstance.oauthSecret = oldUser.oauthSecret;

				// Remove the old mechanism of the consumer file
				[User deleteLoginArchive];
			}
		}
	});

	return sharedInstance;
}

- (UserProfileMO *)userProfile
{
	if (!_userProfile) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		_userProfile = [UserProfileMO getCurrentUserProfileInMOC:appDelegate.adManagedObjectContext];
	}

	return _userProfile;
}

+ (NSNumber *)loginId
{
	User *currentUser = [User currentUser];
	return currentUser.loginId;
}

- (NSNumber *)loginId
{
	return [self.userKeychainItem objectForKey:(__bridge id)kSecAttrDescription];
}

- (void)setLoginId:(NSNumber *)loginId
{
	[self.userKeychainItem setObject:loginId forKey:(__bridge id)kSecAttrDescription];
}

- (NSString *)email
{
	return [self.userKeychainItem objectForKey:(__bridge id)kSecAttrLabel];
}

- (void)setEmail:(NSString *)email
{
	[self.userKeychainItem setObject:email forKey:(__bridge id)kSecAttrLabel];
}

- (NSString*)oauthAccessToken
{
	return [self.userKeychainItem objectForKey:(__bridge id)kSecAttrGeneric];
}

- (void)setOauthAccessToken:(NSString *)accessToken
{
	[self.userKeychainItem setObject:accessToken forKey:(__bridge id)kSecAttrGeneric];
}

- (NSString*)oauthSecret
{
	return [self.userKeychainItem objectForKey:(__bridge id)kSecValueData];
}

- (void)setOauthSecret:(NSString *)secret
{
	[self.userKeychainItem setObject:secret forKey:(__bridge id)kSecValueData];
}

+ (NSNumber *)accountId
{
	User *currentUser = [User currentUser];
	return currentUser.accountId;
}

- (NSNumber *)accountId
{
	return self.userProfile.accountId;
}

- (NSString *)name
{
	return self.userProfile.name;
}

- (NSString *)homeZip
{
	return self.userProfile.zipCode;
}

+ (NSString *)dataFilePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return [documentsDirectory stringByAppendingPathComponent:kFilename];
}

+ (BOOL)deleteLoginArchive
{
	// delete the user archive
	NSError *error;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *archivePath = [User dataFilePath];

	if ([fileManager fileExistsAtPath:archivePath]) {
		if (![fileManager removeItemAtPath:archivePath error:&error]) {
			NSLog(@"Unresolved error trying to login store %@, %@", error, [error userInfo]);
			return NO;
		}
	}

	return YES;
}

+ (User *)loadUser
{
	NSData *data = [[NSMutableData alloc] initWithContentsOfFile:[self dataFilePath]];
	NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
	User *outUser = [unarchiver decodeObjectForKey:kDataKey];
	[unarchiver finishDecoding];
	return outUser;
}

+ (void)clearCurrentUser
{
	User *currentUser = [User currentUser];
	// Keep the email for the login screen
	NSString *email = currentUser.email;
	[currentUser.userKeychainItem resetKeychainItem];
	currentUser.email = email;
	currentUser.userProfile = nil;
}

#pragma mark NSCoding
- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.loginId forKey:kLoginUserId];
	[encoder encodeObject:self.email forKey:kEmail];
	[encoder encodeObject:self.oauthAccessToken forKey:kLoginOAuthAccessToken];
	[encoder encodeObject:self.oauthSecret forKey:kLoginOAuthSecret];
}

#pragma mark NSCopying
- (id)copyWithZone:(NSZone *)zone
{
	User *userCopy = [[User allocWithZone:zone] init];
	userCopy.loginId = self.loginId;
	userCopy.email = self.email;
	userCopy.oauthAccessToken = self.oauthAccessToken;
	userCopy.oauthSecret = self.oauthSecret;
	return userCopy;
}

@end
