//
//  BLGoalInfo.h
//  BettrLife
//
//  Created by Greg Goodrich on 5/2/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLGoalInfo : NSObject
typedef NS_ENUM(NSUInteger, GoalSource) { GoalsDefaults = 0, GoalsPersonal, GoalsMealPlan };
@property (strong, nonatomic) NSDictionary *minNutrition; ///< Dictionary of key/value pairs of nutrients that have goals on them
@property (strong, nonatomic) NSDictionary *maxNutrition; ///< Dictionary of key/value pairs of nutrients that have goals on them
@property (strong, nonatomic) NSArray *goalNutrients; ///< Array of strings with key values for nutrients that have goals set
@property (strong, nonatomic) NSNumber *bmr; ///< The raw bmr calculated value for this user
@property (strong, nonatomic) NSNumber *bmrGoal; ///< The actual calorie goal for bmr
@property (assign, nonatomic) BOOL usingBMR; ///< This tells you whether the calorie goals came from BMR, or from hard-coded calorie goals
@property (strong, nonatomic) NSArray *mealPlanEntries; ///< Convenience property that will have meal plan entry MOs if they're on a plan
@property (strong, nonatomic) NSDate *date; ///< The date that this goal info is valid for
@property (assign, nonatomic) GoalSource goalSource; ///< This tells you where the goals came from, see the GoalSource enum
@property (assign, nonatomic) BOOL isWeightLoss; ///< This tells you if the user is trying to lose weight or gain weight
@property (strong, nonatomic) NSNumber *activityLevelCalories; ///< This tells you the calories to add for their activity level
@property (assign, nonatomic) NSUInteger activityCalories; ///< This tells you the calories expended for logged activities
@end
