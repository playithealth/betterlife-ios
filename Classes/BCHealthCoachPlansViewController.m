//
//	UNUSED?!
//  BCHealthCoachPlansViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 4/26/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCHealthCoachPlansViewController.h"

#import "AdvisorMealPlanMO.h"
#import "AdvisorMealPlanTimelineMO.h"
#import "BCAppDelegate.h"
#import "BCObjectManager.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BLHealthCoachPlanViewController.h"
#import "GTMHTTPFetcherAdditions.h"
#import "NumberValue.h"
#import "User.h"

@interface BCHealthCoachPlansViewController ()

@property (strong, nonatomic) NSArray *assignedMealPlans;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@end

@implementation BCHealthCoachPlansViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		self.dateFormatter = [[NSDateFormatter alloc] init];
		[self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.navigationItem.rightBarButtonItem = nil;

	[self reloadBCTableViewData:nil];

	// Want to receive notifications regarding plans changing
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadBCTableViewData:)
		name:[AdvisorMealPlanTimelineMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadBCTableViewData:)
		name:[AdvisorMealPlanMO entityName] object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

// Added for iOS 5 and below, as it isn't called in iOS 6 and above
- (void)viewDidUnload
{
	// Remove all observers
	[[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];

	[super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowPlanDetail"]) {
		BLHealthCoachPlanViewController *viewController = segue.destinationViewController;
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		viewController.plan = [self.assignedMealPlans objectAtIndex:indexPath.row];
	}
}

#pragma mark - BCTableViewController overrides
- (void)reloadBCTableViewData:(NSNotification *)notification
{
	self.assignedMealPlans = [AdvisorMealPlanMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"%K = %@ AND %K <> %@", 
		AdvisorPlanMOAttributes.loginId, [[User currentUser] loginId],
		AdvisorPlanMOAttributes.status, kStatusDelete] inContext:self.managedObjectContext];

	[self.tableView reloadData];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.assignedMealPlans count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
	AdvisorMealPlanMO *mealPlan = [self.assignedMealPlans objectAtIndex:indexPath.row];
    cell.textLabel.text = mealPlan.planName;
    cell.detailTextLabel.text = [mealPlan getMealPlanDetailText];
	[cell layoutIfNeeded];

    return cell;
}

@end
