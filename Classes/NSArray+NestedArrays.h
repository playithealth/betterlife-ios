//
//  NSArray+NestedArrays.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/3/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray(NestedArrays)
/**
 This method will return an object contained with an array 
 contained within this array. It is intended to allow 
 single-step retrieval of objects in the nested array 
 using an index path
 */
- (id)BC_nestedObjectAtIndexPath:(NSIndexPath *)indexPath;

/**
 this method will return the count from a subarray.
 */
- (NSInteger)BC_countOfNestedArrayAtIndex:(NSUInteger)section;

/**
 this method will return the sum of counts from all subarrays.
 */
- (NSInteger)BC_countOfNestedArrays;

/**
 this method will remove an object from a subarray.
 */
- (void)BC_removeObjectAtIndexPath:(NSIndexPath *)indexPath;

/**
 This method will replace an object in a subarray.
 */
- (void)BC_replaceObjectAtIndexPath:(NSIndexPath *)indexPath withObject:(id)object;
@end
