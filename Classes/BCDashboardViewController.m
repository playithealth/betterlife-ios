//
//  BCDashboardViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/13/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import "BCDashboardViewController.h"

#import "AdvisorMealPlanMO.h"
#import "AdvisorMealPlanPhaseMO.h"
#import "BCDashboardCell.h"
#import "BCDetailViewDelegate.h"
#import "BCDrawBlockView.h"
#import "BCNutritionDetailViewController.h"
#import "BCObjectManager.h"
#import "BCPermissionsDataModel.h"
#import "BCTrackingDetailViewController.h"
#import "BCUtilities.h"
#import "BLAlertController.h"
#import "BLAlwaysShowViewController.h"
#import	"FoodLogMO.h"
#import "NSArray+NestedArrays.h"
#import "NSCalendar+Additions.h"
#import "NSDate+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NumberValue.h"
#import "NutrientMO.h"
#import "NutritionMO.h"
#import "TrackingMO.h"
#import "TrackingTypeMO.h"
#import "TrackingValueMO.h"
#import "TrainingActivityMO.h"
#import "UIColor+Additions.h"
#import "User.h"
#import "UserProfileMO.h"

@interface BCDashboardViewController () <BCDetailViewDelegate>
@property (strong, nonatomic) UserProfileMO *userProfileMO;
@property (strong, nonatomic) NSArray *nutrients; ///< Array of NutrientMOs
@property (strong, nonatomic) BLGoalInfo *goalInfo;
@property (strong, nonatomic) NSArray *trackingLogs;
@property (strong, nonatomic) NSArray *nutrientLogs;
@property (strong, nonatomic) NSArray *foodLogEntries;
@property (strong, nonatomic) NSArray *caloriesExpendedByPeriod;
@property (assign, nonatomic) NSInteger dataTypeIndex;
@property (assign, nonatomic) NSInteger scopeIndex;
@property (strong, nonatomic) NSArray *trackingTypes;
@property (strong, nonatomic) NSNumberFormatter *numberFormatter;
@property (strong, nonatomic) NSCalendar *calendar;
@property (assign, nonatomic) NSUInteger customTrackingScopeValue;
@property (assign, nonatomic) NSUInteger customNutritionScopeValue;
@property (strong, nonatomic) UISegmentedControl *scopeControl;
@property (strong, nonatomic) UIBarButtonItem *editBarButtonItem;

@property (assign, nonatomic) BOOL needsToReload;
@end

@implementation BCDashboardViewController

static const NSInteger kSectionTracking = 0;
static const NSInteger kSectionNutrients = 1;

enum { DataTypeTracking = 0, DataTypeNutrition };
enum { ScopeWeek = 0, ScopeMonth, ScopeYear, ScopeCustom, ScopeConfigure };

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:(NSCoder *)aDecoder];
	if (self) {
		self.scopeIndex = ScopeWeek;
		self.needsToReload = YES;

		self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

		self.numberFormatter = [[NSNumberFormatter alloc] init];
		[self.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[self.numberFormatter setMinimumFractionDigits:0];
		[self.numberFormatter setMaximumFractionDigits:1];
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];

	// Create the right bar button item for edit, but we'll only show it if we're on the Nutrition dataTypeIndex 'tab'
	self.editBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
		target:self action:@selector(editButtonTapped:)];

	[self buildDataTypeSegmentedControl];

	UserProfileMO *userProfile = [User currentUser].userProfile;
	self.customTrackingScopeValue = [userProfile.customTrackingTimeframe integerValue];
	self.customNutritionScopeValue = [userProfile.customNutritionTimeframe integerValue];

	self.scopeControl = [[UISegmentedControl alloc]
		initWithItems:@[@"7 days", @"30 days", @"1 year", @"90 days", @"..."]];
	self.scopeControl.frame = CGRectMake(0, 0, 260, 30);
	self.scopeControl.segmentedControlStyle = UISegmentedControlStyleBar;
	[self.scopeControl addTarget:self action:@selector(dataScopeChanged:) forControlEvents:UIControlEventValueChanged];
	[self.scopeControl setTitle:[NSString stringWithFormat:@"%lu days", (long)self.customTrackingScopeValue]
		forSegmentAtIndex:ScopeCustom];
	[self.scopeControl setSelectedSegmentIndex:ScopeWeek];
	self.scopeControl.apportionsSegmentWidthsByContent = YES;
	UIBarButtonItem *segmentedControlItem = [[UIBarButtonItem alloc] initWithCustomView:self.scopeControl];
	UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	
	self.toolbarItems = @[flexItem,segmentedControlItem,flexItem];

	[self reloadTrackingMetadata];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:[FoodLogMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:[TrackingMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:[UserProfileMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:[TrackingTypeMO entityName] object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userPermissionsUpdated:)
		name:[UserProfileMO entityName] object:nil];

	self.userProfileMO = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	[self reloadNutrients];
}

- (void)viewWillAppear:(BOOL)animated
{
	self.navigationController.toolbarHidden = NO;

	if (self.needsToReload) {
		[self reloadBCTableViewData];
		self.needsToReload = NO;
	}

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	self.navigationController.toolbarHidden = YES;

	[super viewWillDisappear:animated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowTrackingDetail"]) {
		BCTrackingDetailViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;
		detailView.managedObjectContext = self.managedObjectContext;
		detailView.scopeIndex = self.scopeIndex;

		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		TrackingTypeMO *trackingTypeMO = [self.trackingTypes objectAtIndex:indexPath.row];
		detailView.navigationItem.title = trackingTypeMO.title;
		detailView.trackingTypeMO = trackingTypeMO;
	}
	else if ([segue.identifier isEqualToString:@"ShowNutritionDetail"]) {
		BCNutritionDetailViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;
		detailView.managedObjectContext = self.managedObjectContext;
		detailView.scopeIndex = self.scopeIndex;
		detailView.foodLogEntries = self.foodLogEntries;
		detailView.goalInfo = self.goalInfo;

		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		NutrientMO *nutrientMO = [self.nutrients objectAtIndex:[indexPath row]];
		detailView.nutrientName = nutrientMO.name;
		detailView.nutrientDisplayName = nutrientMO.displayName;
		detailView.nutrientUnits = nutrientMO.units;
	}
	else if ([segue.identifier isEqualToString:@"EditAlwaysShow"]) {
		BLAlwaysShowViewController *detailView = segue.destinationViewController;
		detailView.managedObjectContext = self.managedObjectContext;
	}
}

- (IBAction)alwaysShowViewDone:(UIStoryboardSegue *)segue
{
}

#pragma mark - local
+ (NSArray *)graphColors
{
	static NSArray *colors = nil;

	if (!colors) {
		colors = @[
			[UIColor BL_graphBlue],
			[UIColor BL_graphGreen],
			[UIColor BL_graphRed],
			[UIColor BL_graphOrange],
			[UIColor BL_graphPurple],
			[UIColor BL_graphGray],
		];
	}

	return colors;
}

- (void)reloadTrackingMetadata
{
	self.trackingTypes = [TrackingTypeMO MR_findByAttribute:TrackingTypeMOAttributes.loginId withValue:[User loginId]
		andOrderBy:TrackingTypeMOAttributes.sortOrder ascending:YES inContext:self.managedObjectContext];
}

- (void)reloadNutrients
{
	NSSet *alwaysShowNutrientNames = [self.userProfileMO.alwaysShowNutrients valueForKeyPath:NutrientMOAttributes.name];
	self.goalInfo = [self.userProfileMO goalInfoForDate:[NSDate date]];

	// If we have any always show nutrients, then merge that with goal nutrients
	// else if there are goal nutrients, use those,
	// else use the default set of nutrients
	id nutrientNames = ((alwaysShowNutrientNames
		? [alwaysShowNutrientNames setByAddingObjectsFromArray:self.goalInfo.goalNutrients] : self.goalInfo.goalNutrients)
		?: @[
			NutritionMOAttributes.calories,
			NutritionMOAttributes.protein,
			NutritionMOAttributes.totalCarbohydrates,
			NutritionMOAttributes.totalFat
		]);

	self.nutrients = [NutrientMO orderedNutrientsWithNames:nutrientNames inMOC:self.managedObjectContext];
}

- (void)refreshView:(NSNotification *)notification
{
	UserProfileMO *userProfile = [User currentUser].userProfile;
	self.customTrackingScopeValue = [userProfile.customTrackingTimeframe integerValue];
	self.customNutritionScopeValue = [userProfile.customNutritionTimeframe integerValue];
	NSUInteger customScope = (self.dataTypeIndex == DataTypeTracking ? self.customTrackingScopeValue : self.customNutritionScopeValue);
	[self.scopeControl setTitle:[NSString stringWithFormat:@"%lu days", (long)customScope]
		forSegmentAtIndex:ScopeCustom];
	[self reloadTrackingMetadata];
	[self reloadNutrients];
	[self reloadBCTableViewData];
}

- (NSArray *)trackingLogs
{
    if (_trackingLogs != nil) {
        return _trackingLogs;
    }

	NSMutableArray *logs = [NSMutableArray array];

	if (self.scopeIndex != ScopeYear) {
		NSInteger numberOfDays = 0;
		switch (self.scopeIndex) {
			case ScopeWeek:
				numberOfDays = 7;
				break;
			case ScopeMonth:
				numberOfDays = 30;
				break;
			case ScopeCustom:
				numberOfDays = self.customTrackingScopeValue;
				break;
		}
		for (TrackingTypeMO *trackingTypeMO in self.trackingTypes) {
			[logs addObject:[TrackingMO onePerDayForTrackingType:trackingTypeMO.cloudIdValue numberOfDays:numberOfDays
				usingCalendar:self.calendar inMOC:self.managedObjectContext]];
		}
	}
	else {
		for (TrackingTypeMO *trackingTypeMO in self.trackingTypes) {
			[logs addObject:[TrackingMO monthlyAveragesForTrackingType:trackingTypeMO.cloudIdValue numberOfMonths:12
				usingCalendar:self.calendar inMOC:self.managedObjectContext]];
		}
	}
	
	self.trackingLogs = logs;

	return _trackingLogs;
}

//- (NSArray *)nutrientLogs
- (void)loadNutrientLogs
{
	/*
	if (_nutrientLogs != nil) {
		return _nutrientLogs;
	}
	*/

	User *thisUser = [User currentUser];

	NSMutableArray *logs = [NSMutableArray array];

	if (self.scopeIndex != ScopeYear) {
		NSInteger numberOfDays = 0;
		switch (self.scopeIndex) {
			case ScopeWeek:
				numberOfDays = 7;
				break;
			case ScopeMonth:
				numberOfDays = 30;
				break;
			case ScopeCustom:
				numberOfDays = self.customNutritionScopeValue;
				break;
		}

		for (NSInteger dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
			if (dayIndex == 0) {
				// First, set up an array for each nutrient, this way we can just add things to them down below
				for (int nutrientIndex = 0; nutrientIndex < [self.nutrients count]; nutrientIndex++) {
					[logs addObject:[NSMutableArray array]];
				}
			}
			NSDate *date = [self.calendar BC_dateByAddingDays:-1 * dayIndex toDate:[NSDate date]];
			NSArray *logsForPeriod = [FoodLogMO findLogsAfter:[self.calendar BC_startOfDate:date] 
				before:[self.calendar BC_endOfDate:date] inContext:self.managedObjectContext];
			int idx = 0;
			for (NutrientMO *nutrient in self.nutrients) {
				double sum = 0.0f;
				for (FoodLogMO* foodLog in logsForPeriod) {
					sum += [[foodLog.nutrition valueForKey:nutrient.name] doubleValue] * [foodLog.nutritionFactor doubleValue]
						* [foodLog.servings doubleValue];
				}
				[[logs objectAtIndex:idx] addObject:@(sum)];
				idx++;
			}
		}
	}
	else {
		// query all the entries for the last year
		NSDate *today = [NSDate date];
		NSDate *startDate = [self.calendar BC_dateByAddingYears:-1 toDate:today];
		NSArray *foodLogEntries = [FoodLogMO MR_findAllSortedBy:FoodLogMOAttributes.date ascending:NO 
			withPredicate:[NSPredicate predicateWithFormat:@"date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
			startDate, today, kStatusDelete, thisUser.loginId] inContext:self.managedObjectContext];

		for (NSInteger nutrientIndex = 0; nutrientIndex < [self.nutrients count]; nutrientIndex++) {
			NSMutableArray *innerArray = [NSMutableArray array];
			for (NSInteger index = 0; index < 12; index++) {
				[innerArray addObject:@0];
			}

			NSInteger month = 0;
			double monthSum = 0.0f;
			NSInteger daysWithEntries = 0;

			// loop thru each food log entry
			for (FoodLogMO *foodLog in foodLogEntries) {
				// is this in the same month?
				NSInteger monthDiff = [[self.calendar components:NSMonthCalendarUnit fromDate:foodLog.date toDate:today options:0] month];
				if (month != monthDiff) {
					// avoid dividing by zero
					if (daysWithEntries > 0) {
						[innerArray replaceObjectAtIndex:month withObject:[NSNumber numberWithDouble:monthSum / daysWithEntries]];
					}
					// reset the counters
					month = monthDiff;
					monthSum = 0.0f;
					daysWithEntries = 0;
				}

				daysWithEntries++;

				// add this to the month
				NutritionMO *nutrition = foodLog.nutrition;
				if (nutrition) {
					monthSum += [[nutrition valueForKey:[[self.nutrients objectAtIndex:nutrientIndex] name]] doubleValue]
						* [foodLog.nutritionFactor doubleValue] * [foodLog.servings doubleValue];
				}
			}

			[logs addObject:innerArray];
		}
	}

	self.nutrientLogs = logs;
}

- (NSArray *)foodLogEntries
{
	if (_foodLogEntries != nil) {
		return _foodLogEntries;
	}

	User *thisUser = [User currentUser];

	// query all the entries for the last year
	NSDate *today = [NSDate date];
	NSDate *startDate = [self.calendar BC_dateByAddingYears:-1 toDate:today];
	self.foodLogEntries = [FoodLogMO MR_findAllSortedBy:FoodLogMOAttributes.date ascending:NO 
		withPredicate:[NSPredicate predicateWithFormat:@"date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
		startDate, today, kStatusDelete, thisUser.loginId] inContext:self.managedObjectContext];

	return _foodLogEntries;
}

- (NSArray *)caloriesExpendedByPeriod
{
	if (_caloriesExpendedByPeriod != nil) {
		return _caloriesExpendedByPeriod;
	}

	User *thisUser = [User currentUser];

	NSMutableArray *tempArray = [NSMutableArray array];

	if (self.scopeIndex != ScopeYear) {
		NSInteger numberOfDays = 0;
		switch (self.scopeIndex) {
			case ScopeWeek:
				numberOfDays = 7;
				break;
			case ScopeMonth:
				numberOfDays = 30;
				break;
			case ScopeCustom:
				numberOfDays = self.customNutritionScopeValue;
				break;
		}
		for (NSInteger dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
			NSDate *date = [self.calendar BC_dateByAddingDays:-1 * dayIndex toDate:[NSDate date]];
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
				[self.calendar BC_startOfDate:date], [self.calendar BC_endOfDate:date], kStatusDelete, thisUser.loginId];

			NSNumber *activityCalories = [TrainingActivityMO MR_aggregateOperation:@"sum:" onAttribute:TrainingActivityMOAttributes.calories
				withPredicate:predicate inContext:self.managedObjectContext];

			// Adjust for BMR, if we're using it
			if (self.goalInfo.usingBMR) {
				activityCalories = @([activityCalories doubleValue] + [self.goalInfo.bmr doubleValue]);
			}
			[tempArray addObject:activityCalories];
		}
	}
	else {
		for (NSInteger monthOffset = 0; monthOffset > -12; monthOffset--) {
			NSDate *date = [self.calendar BC_dateByAddingMonths:monthOffset toDate:[NSDate date]];
			NSDate *firstOfMonth = [self.calendar BC_firstOfMonthForDate:date];
			NSDate *lastOfMonth = [self.calendar BC_lastOfMonthForDate:date];
			NSInteger daysInMonth = [self.calendar BC_daysFromDate:firstOfMonth toDate:lastOfMonth];
			NSInteger daysWithEntries = 0;
			double monthSum = 0.0f;
			for (NSInteger dayIndex = 0; dayIndex < daysInMonth; dayIndex++) {
				NSDate *date = [self.calendar BC_dateByAddingDays:dayIndex toDate:firstOfMonth];
				NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
					[self.calendar BC_startOfDate:date], [self.calendar BC_endOfDate:date], kStatusDelete, thisUser.loginId];

				NSNumber *activityCalories = [TrainingActivityMO MR_aggregateOperation:@"sum:" onAttribute:TrainingActivityMOAttributes.calories
					withPredicate:predicate inContext:self.managedObjectContext];
				NSNumber *cardioCalories = [TrainingActivityMO MR_aggregateOperation:@"sum:" onAttribute:TrainingActivityMOAttributes.calories
					withPredicate:predicate inContext:self.managedObjectContext];

				NSNumber *daySum = @([activityCalories integerValue] + [cardioCalories integerValue]);

				// Adjust for BMR, if we're using it
				if (self.goalInfo.usingBMR) {
					daySum = @([daySum doubleValue] + [self.goalInfo.bmr doubleValue]);
				}
				if ([daySum doubleValue] > 0) {
					daysWithEntries++;
					monthSum += [daySum doubleValue];
				}
			}
			if (monthSum > 0) {
				[tempArray addObject:[NSNumber numberWithDouble:monthSum / daysWithEntries]];
			}
			else {
				[tempArray addObject:@0];
			}
		}
	}

	self.caloriesExpendedByPeriod = tempArray;

	return _caloriesExpendedByPeriod;
}

- (void)buildDataTypeSegmentedControl
{
	// only show the segmented control if the user has both foodlog and tracking
	if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]
			&& [BCPermissionsDataModel userHasPermission:kPermissionTracking]) {
		// NOTE This seems over the top that it is recreating this every time, maybe we should be creating it and storing it,
		// I mean, if it changes due to permissions, then that might change things, unsure
		UISegmentedControl *dataTypeControl = [[UISegmentedControl alloc] initWithItems:@[@"Tracking", @"Nutrition"]];
		dataTypeControl.segmentedControlStyle = UISegmentedControlStyleBar;
		CGRect segmentFrame = dataTypeControl.frame;
		segmentFrame.size.width = 180;
		dataTypeControl.frame = segmentFrame;
		[dataTypeControl addTarget:self action:@selector(dataTypeChanged:) forControlEvents:UIControlEventValueChanged];
		[dataTypeControl setSelectedSegmentIndex:self.dataTypeIndex];
		self.navigationItem.titleView = dataTypeControl;
	}
	else {
		// Only touch the dataTypeIndex if they don't have both permissions, otherwise, a refresh can cause it to change
		if ([BCPermissionsDataModel userHasPermission:kPermissionTracking]) {
			self.dataTypeIndex = DataTypeTracking;
		}
		else if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
			self.dataTypeIndex = DataTypeNutrition;
		}
		self.navigationItem.title = @"Progress";
	}

	self.navigationItem.rightBarButtonItem = (self.dataTypeIndex == DataTypeNutrition ? self.editBarButtonItem : nil);
}

- (void)reloadBCTableViewData
{
	// purge the local cache of tracking and force the table to reload
	self.trackingLogs = nil;
	[self loadNutrientLogs];
	self.caloriesExpendedByPeriod = nil;

	[self.tableView reloadData];
}

- (NSArray *)oneLogPerDay:(NSArray *)logs
{
	NSMutableArray *filteredLogs = [NSMutableArray array];
	NSInteger dayIndex = 1;
	for (TrackingMO *log in logs) {
		NSInteger days = [self.calendar BC_daysFromDate:[NSDate date] toDate:log.date];
		if (days < dayIndex) {
			dayIndex = days;
			[filteredLogs addObject:log];
		}
	}
	return filteredLogs;
}

- (NSArray *)oneLogPerMonth:(NSArray *)logs
{
	NSMutableArray *filteredLogs = [NSMutableArray array];
	NSInteger monthIndex = -1;
	NSInteger yearIndex = -1;
	for (TrackingMO *log in logs) {
		NSDateComponents *logDateComponents = [self.calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:log.date];
		if (logDateComponents.month != monthIndex || logDateComponents.year != yearIndex) {
			monthIndex = logDateComponents.month;
			yearIndex = logDateComponents.year;
			[filteredLogs addObject:log];
		}
	}
	return filteredLogs;
}

- (void)configureNutritionCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
	BCDashboardCell *bcCell = (BCDashboardCell *)cell;
	NutrientMO *nutrient = [self.nutrients objectAtIndex:indexPath.row];
	bcCell.nameLabel.text = nutrient.displayName;
	bcCell.unitLabel.text = nutrient.units;
	bcCell.arrowImageView.image = nil;

	NSArray *logs = [self.nutrientLogs objectAtIndex:indexPath.row];
	NSNumber *lastValue = [logs objectAtIndex:0];
	bcCell.valueLabel.text = [self.numberFormatter stringFromNumber:lastValue];

	// Figure out the min, max, and range
	// Start out by setting the min and max values to the goal min and max
	NSNumber *goalMin = [self.goalInfo.minNutrition valueForKey:nutrient.name];
	NSNumber *goalMax = [self.goalInfo.maxNutrition valueForKey:nutrient.name];

	// Set min and max based upon values logged
	double minValue = [[logs valueForKeyPath:@"@min.doubleValue"] doubleValue];
	double maxValue = [[logs valueForKeyPath:@"@max.doubleValue"] doubleValue];

	// Special handling for calories, as there are other factors in play, such as BMR, and calorie burn, etc
	if ([nutrient.name isEqualToString:NutritionMOAttributes.calories]) {
		// Adjust the min and max for calorie burn too
		double minBurn = [[self.caloriesExpendedByPeriod valueForKeyPath:@"@min.doubleValue"] doubleValue];
		double maxBurn = [[self.caloriesExpendedByPeriod valueForKeyPath:@"@max.doubleValue"] doubleValue];

		// Finally, adjust our graph min and max values to account for the goal min/max
		minValue = MIN(minValue, minBurn);
		maxValue = MAX(maxValue, maxBurn);

		// Alter goals if the bmrGoal is set, as it can override a normal calorie goal range
		if (self.goalInfo.usingBMR) {
			// Adjust the goal min/max based upon weight gain/loss
			if (self.goalInfo.isWeightLoss) {
				goalMin = nil;
				goalMax = self.goalInfo.bmrGoal;
			}
			else {
				goalMin = self.goalInfo.bmrGoal;
				goalMax = nil;
			}
		}
	}

	// Adjust minValue and maxValue to accommodate for goals
	minValue = (goalMin ? MIN(minValue, [goalMin doubleValue]) : minValue);
	minValue = (goalMax ? MIN(minValue, [goalMax doubleValue]) : minValue);
	maxValue = (goalMin ? MAX(maxValue, [goalMin doubleValue]) : maxValue);
	maxValue = (goalMax ? MAX(maxValue, [goalMax doubleValue]) : maxValue);

	if (maxValue == 0) {
		maxValue = 100;
	}

	double range = maxValue - minValue;

	NSInteger circleRadius = ([logs count] > 12 ? 2 : 4);
	double circleLineWidth = 1.5;
	NSInteger graphWidth = 290;
	NSInteger graphHeight = 44;
	double xStep = graphWidth / [logs count];
	NSInteger topOffset = graphHeight / 6;
	NSInteger maxGraphHeight = graphHeight * 5 / 6;
	NSInteger xIndex = 0;
	double xLineStart = 16;

	NSMutableArray *dataSets = [NSMutableArray array];
	NSMutableArray *dataPoints = [NSMutableArray array];
	NSMutableArray *goalPoints = [NSMutableArray array];
	for (NSNumber *value in logs) {
		[dataPoints addObject:[NSValue valueWithCGPoint:CGPointMake(
			graphWidth - (xIndex++ * xStep + (circleRadius * 2) + circleLineWidth),
			maxGraphHeight * (maxValue - [value doubleValue]) / range + topOffset)]];
	}
	[dataSets addObject:dataPoints];

	double maxGoalY = 0.0f;
	double minGoalY = 0.0f;

	if ([nutrient.name isEqualToString:NutritionMOAttributes.calories]) {
		xIndex = 0;
		NSMutableArray *dataPoints2 = [NSMutableArray array];
		UserProfileMO *userProfile = [User currentUser].userProfile;
		for (NSNumber *value in self.caloriesExpendedByPeriod) {
			double xValue = graphWidth - (xIndex++ * xStep + (circleRadius * 2) + circleLineWidth);
			//double yValue = maxGraphHeight * (maxValue - ([value doubleValue] + [self.goalInfo.bmr doubleValue])) / range + topOffset;
			double yValue = maxGraphHeight * (maxValue - [value doubleValue]) / range + topOffset;
			[dataPoints2 addObject:[NSValue valueWithCGPoint:CGPointMake(xValue, yValue)]];

			// Add in the point for the goal line, if we're using BMR, as this line will not necessarily be straight, due to activities,
			// and will track parallel to the burn line, with the gap between determined by the nutrition effort
			// Also factor in weight gain/loss by adding/subtracting nutrition effort!
			if (self.goalInfo.usingBMR) {
				[goalPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xValue,
					maxGraphHeight * (maxValue - ([value doubleValue]
					+ ((self.goalInfo.isWeightLoss ? -1 : 1) * [userProfile.nutritionEffort doubleValue]))) / range + topOffset)]];
			}
		}
		[dataSets addObject:dataPoints2];
	}
	else {
		if (goalMin) {
			double goalMinValue = [goalMin doubleValue];
			minGoalY = maxGraphHeight * (maxValue - goalMinValue) / (maxValue - minValue) + topOffset;
		}

		if (goalMax) {
			double goalMaxValue = [goalMax doubleValue];
			maxGoalY = maxGraphHeight * (maxValue - goalMaxValue) / (maxValue - minValue) + topOffset;
		}
	}

	bcCell.graphView.drawBlock = ^(UIView* v, CGContextRef context) {
		/*
        NSInteger bottomOffset = 0.0;
		double shadeTop = (goalMax ? maxGoalY : 0.0);
		double shadeBottom = (goalMin ? minGoalY : graphHeight - bottomOffset);
		if (goalMin || goalMax) {
			// Shade between the goal min and max
			CGContextSetFillColorWithColor(context, [[UIColor BL_goalRangeColor] CGColor]);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, xLineStart, shadeTop);
			CGContextAddLineToPoint(context, graphWidth, shadeTop);
			CGContextAddLineToPoint(context, graphWidth, shadeBottom);
			CGContextAddLineToPoint(context, xLineStart, shadeBottom);
			CGContextClosePath(context);
			CGContextDrawPath(context, kCGPathFill);
		}
		*/

		// Draw the goal line
		CGFloat dashLengths[] = {10, 10};
		CGContextSetLineDash(context, 0, dashLengths, 2);
		CGContextSetLineWidth(context, 2.0);
		CGContextSetStrokeColorWithColor(context, [[UIColor BL_graphGoal] CGColor]);
		CGContextBeginPath(context);
		if ([goalPoints count]) {
			CGPoint firstPoint = [[goalPoints objectAtIndex:0] CGPointValue];
			CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
			for (int pointIndex = 1; pointIndex < [goalPoints count]; pointIndex++) {
				CGPoint destinationPoint = [[goalPoints objectAtIndex:pointIndex] CGPointValue];
				CGPoint previousPoint = [[goalPoints objectAtIndex:pointIndex - 1] CGPointValue];
				if (previousPoint.y == destinationPoint.y) {
					CGContextAddLineToPoint(context, destinationPoint.x, destinationPoint.y);
				}
				else {
					CGPoint controlPoint1 = CGPointMake(previousPoint.x - xStep * 0.50, previousPoint.y);
					CGPoint controlPoint2 = CGPointMake(destinationPoint.x + xStep * 0.50, destinationPoint.y);
					CGContextAddCurveToPoint(context, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y,
						destinationPoint.x, destinationPoint.y);
				}
			}
			CGContextDrawPath(context, kCGPathStroke);
		}
		else if (goalMin || goalMax) {
			if (goalMin) {
				CGContextBeginPath(context);
				CGContextMoveToPoint(context, xLineStart, minGoalY);
				CGContextAddLineToPoint(context, graphWidth, minGoalY);
				CGContextDrawPath(context, kCGPathStroke);
			}
			if (goalMax) {
				CGContextBeginPath(context);
				CGContextMoveToPoint(context, xLineStart, maxGoalY);
				CGContextAddLineToPoint(context, graphWidth, maxGoalY);
				CGContextDrawPath(context, kCGPathStroke);
			}
		}
		CGContextSetLineDash(context, 0, nil, 0);

		NSArray *colors = @[
			[UIColor BL_graphGreen],
			[UIColor BL_graphBlue],
		];

		for (NSInteger setIndex = 0; setIndex < [dataSets count]; setIndex++) {
			NSArray *dataPoints = [dataSets objectAtIndex:setIndex];

			// If, somehow, we get here with no data points, then do not continue, as we can't do anything productive.
			// Should not happen, defensive coding
			if (![dataPoints count]) {
				continue;
			}

			CGPoint firstPoint = [[dataPoints objectAtIndex:0] CGPointValue];

			/*
			 * Draw the data line
			 */
			CGContextSetLineWidth(context, 2.0);
			CGContextSetStrokeColorWithColor(context, [[colors objectAtIndex:setIndex] CGColor]);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
			for (int pointIndex = 1; pointIndex < [dataPoints count]; pointIndex++) {
				CGPoint destinationPoint = [[dataPoints objectAtIndex:pointIndex] CGPointValue];
				CGPoint previousPoint = [[dataPoints objectAtIndex:pointIndex - 1] CGPointValue];
				if (previousPoint.y == destinationPoint.y) {
					CGContextAddLineToPoint(context, destinationPoint.x, destinationPoint.y);
				}
				else {
					CGPoint controlPoint1 = CGPointMake(previousPoint.x - xStep * 0.50, previousPoint.y);
					CGPoint controlPoint2 = CGPointMake(destinationPoint.x + xStep * 0.50, destinationPoint.y);
					CGContextAddCurveToPoint(context, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y,
						destinationPoint.x, destinationPoint.y);
				}
			}
			CGContextDrawPath(context, kCGPathStroke);

			// Don't draw the circles if they would be too close together
			if ([dataPoints count] <= 30) {
				/*
				 * add the circles at the data points
				 */
				CGRect rect;
				CGContextBeginPath(context);
				CGContextSetLineWidth(context, circleLineWidth);
				CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
				for (int pointIndex = 0; pointIndex < [dataPoints count]; pointIndex++) {
					CGPoint point = [[dataPoints objectAtIndex:pointIndex] CGPointValue];
					rect = CGRectMake(point.x - circleRadius, point.y - circleRadius, 2 * circleRadius, 2 * circleRadius);
					CGContextAddEllipseInRect(context, rect);
				}
				CGContextDrawPath(context, kCGPathFillStroke);

				/*
				 * the last circle should be filled
				 */
				rect = CGRectMake(firstPoint.x - circleRadius, firstPoint.y - circleRadius, 2 * circleRadius, 2 * circleRadius);
				CGContextSetFillColorWithColor(context, [[colors objectAtIndex:setIndex] CGColor]);
				CGContextBeginPath(context);
				CGContextAddEllipseInRect(context, rect);
				CGContextDrawPath(context, kCGPathFillStroke);
			}
		}
	};
	[bcCell.graphView setNeedsDisplay];
}

- (void)configureTrackingCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
	BCDashboardCell *bcCell = (BCDashboardCell *)cell;
	TrackingTypeMO *trackingTypeMO = [self.trackingTypes objectAtIndex:indexPath.row];
	// Get an array of tracking values that are not optional, sorted by their field names, which are track_value1, track_value2, etc...
	NSArray *trackingValues = [[trackingTypeMO.trackingValues filteredSetUsingPredicate:
		[NSPredicate predicateWithFormat:@"%K = NO", TrackingValueMOAttributes.optional]]
		sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:TrackingValueMOAttributes.field ascending:YES]]];
	bcCell.nameLabel.text = trackingTypeMO.title;
	bcCell.unitLabel.text = [trackingTypeMO unitString];
	bcCell.arrowImageView.image = nil;

	NSArray *trackingLogsByDay = [self.trackingLogs objectAtIndex:indexPath.row];

	// If we don't have any track points, turn off the UI elements for displaying them
	if ([trackingLogsByDay count] == 0) {
		bcCell.unitLabel.hidden = YES;
		bcCell.valueLabel.hidden = YES;
		bcCell.graphView.hidden = YES;
		return;
	}
	else {
		bcCell.unitLabel.hidden = NO;
		bcCell.valueLabel.hidden = NO;
		bcCell.graphView.hidden = NO;
	}

	NSDictionary *currentValueDict = [trackingLogsByDay firstObject];
	
	// Determine how many points to show based on the period
	NSInteger numberOfPoints = 1;
	switch (self.scopeIndex) {
		case ScopeWeek:
			numberOfPoints = 7;
			break;
		case ScopeMonth:
			numberOfPoints = 30;
			break;
		case ScopeYear:
			numberOfPoints = 12;
			break;
		case ScopeCustom:
			numberOfPoints = self.customTrackingScopeValue;
			break;
	}

	double graphWidth = bcCell.graphView.frame.size.width;
	NSInteger graphHeight = 44;
	double circleRadius = 4.0;
	double circleLineWidth = 1.5;
	double xStep = (graphWidth - ((2.0 * circleRadius) + (2.0 * circleLineWidth))) / (numberOfPoints - 1); // Total width / gaps between points

	NSInteger maxGraphHeight = graphHeight * 5 / 7;

	NSInteger topOffset = graphHeight / 7 + circleRadius;

	NSMutableDictionary *graphData = [NSMutableDictionary dictionary];
	NSMutableArray *dataSets = [NSMutableArray array];

	// First things first, we need to know what the min and max are for this particular tracking value field, so that
	// we have the proper screen ratio for calculating the data points for each data element, thus we end up looping twice
	double maxValue = -DBL_MAX;
	double minValue = DBL_MAX;

	for (TrackingValueMO *trackingValue in trackingValues) {
		for (NSDictionary *valuesDict in trackingLogsByDay) {
			NSNumber *value = [valuesDict BC_numberOrNilForKey:trackingValue.field];
			if (!value) {
				// Don't factor any entry that lacks this data point
				continue;
			}
			double dblValue = [value doubleValue];
			minValue = (dblValue < minValue ? dblValue : minValue);
			maxValue = (dblValue > maxValue ? dblValue : maxValue);
		}
		minValue = (minValue == DBL_MAX ? 0 : minValue);
		maxValue = (maxValue == -DBL_MAX ? 0 : maxValue);

	}

	double range = maxValue - minValue;

	// Special code for weight
	if ((trackingTypeMO.cloudIdValue == BCTrackingTypeWeight) && self.userProfileMO.goalWeight) {
		// add a line for the goal weight
		double goalWeight = [self.userProfileMO.goalWeight doubleValue];
		// Goal weight needs to be factored into the min and/or max values so that it shows up
		maxValue = (goalWeight > maxValue ? goalWeight : maxValue);
		minValue = (goalWeight < minValue ? goalWeight : minValue);
		// Alter the range as well
		range = maxValue - minValue;
		double maxGoalY = maxGraphHeight * (maxValue - goalWeight) / range + topOffset;
		[graphData setValue:[NSNumber numberWithDouble:maxGoalY] forKey:@"maxGoalY"];
	}

	for (TrackingValueMO *trackingValue in trackingValues) {
		NSMutableArray *graphDataPoints = [NSMutableArray array];
		for (NSDictionary *valuesDict in trackingLogsByDay) {
			NSNumber *day = [valuesDict objectForKey:@"dayOffset"];
			NSNumber *value = [valuesDict BC_numberOrNilForKey:trackingValue.field];
			if (!value) {
				// Don't add a point for any day that didn't have a data value for it
				continue;
			}

			NSNumber *numberY;
			if (range == 0) {
				// No range, all points are on the same y coordinate, so just center the points in the graph
				numberY = @(maxGraphHeight / 2 + topOffset);
			}
			else {
				numberY = @(maxGraphHeight * (maxValue - [value doubleValue]) / range + topOffset);
			}
			[graphDataPoints addObject:@{
				@"x" : @(graphWidth - ([day integerValue] * xStep + (circleRadius + circleLineWidth))),
				@"y" : numberY }];

		}

		[dataSets addObject:graphDataPoints];
	}
	[graphData setObject:dataSets forKey:@"points"];

	bcCell.valueLabel.text = [trackingTypeMO valueStringForTrackingValueDictionary:currentValueDict];

	// Special code for weight
	if (trackingTypeMO.cloudIdValue == BCTrackingTypeWeight) {
		NSString *arrowColor = @"green";
		NSString *arrowDirection = nil;
		NSNumber *currentWeight = [currentValueDict BC_numberOrNilForKey:kTrackingValue1];
		NSNumber *previousWeight = nil;

		// See if we have a previous value
		if ([trackingLogsByDay count] > 1) {
			NSDictionary *previousValueDict = trackingLogsByDay[1];
			previousWeight = [previousValueDict BC_numberOrNilForKey:kTrackingValue1];
		}

		if (currentWeight && previousWeight) {
			NSComparisonResult compareWeight = [previousWeight compare:currentWeight];

			// if the user's weight is higher than last week then make the arrow point up
			if (compareWeight == NSOrderedAscending) {
				arrowDirection = @"up";
			}
			else {
				arrowDirection = @"down";
			}

			// if there is a goal
			if ([self.userProfileMO.goalWeight integerValue] > 0) {

				// the user is moving in the direction of their goal, the arrow should be green
				if (compareWeight != NSOrderedSame) {
					NSComparisonResult compareGoal = [currentWeight compare:self.userProfileMO.goalWeight];
					if (compareWeight == compareGoal) {
						arrowColor = @"green";
					}
					// otherwise, red arrow and text
					else {
						arrowColor = @"red";
					}
				}
			}
			if (arrowColor && arrowDirection) {
				bcCell.arrowImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"arrow-%@-%@",
					arrowColor, arrowDirection]];
			}
		}

	}

	bcCell.graphView.drawBlock = ^(UIView* v, CGContextRef context) {
		NSNumber *maxGoalY = [graphData objectForKey:@"maxGoalY"];
		NSNumber *minGoalY = [graphData objectForKey:@"minGoalY"];
		if (minGoalY || maxGoalY) {
			CGFloat dashLengths[] = {10, 10};
			CGContextSetStrokeColorWithColor(context, [[UIColor BL_graphGoal] CGColor]);
			CGContextSetLineDash(context, 0, dashLengths, 2);
			CGContextSetLineWidth(context, 1.0);
			if (maxGoalY) {
				CGContextBeginPath(context);
				CGContextMoveToPoint(context, 0, [maxGoalY doubleValue]);
				CGContextAddLineToPoint(context, graphWidth, [maxGoalY doubleValue]);
				CGContextDrawPath(context, kCGPathStroke);
			}
			if (minGoalY) {
				CGContextBeginPath(context);
				CGContextMoveToPoint(context, 0, [minGoalY doubleValue]);
				CGContextAddLineToPoint(context, graphWidth, [minGoalY doubleValue]);
				CGContextDrawPath(context, kCGPathStroke);
			}
			CGContextSetLineDash(context, 0, nil, 0);
		}

		NSArray *dataSets = [graphData objectForKey:@"points"];
		for (NSInteger setIndex = 0; setIndex < [dataSets count]; setIndex++) {
			NSArray *dataPoints = [dataSets objectAtIndex:setIndex];

			// If, somehow, we get here with no data points, then do not continue, as we can't do anything productive.
			// Should not happen, defensive coding
			if (![dataPoints count]) {
				continue;
			}

			CGPoint firstPoint = CGPointMake([[[dataPoints objectAtIndex:0] valueForKey:@"x"] doubleValue],
				[[[dataPoints objectAtIndex:0] valueForKey:@"y"] doubleValue]);

			/* 
			 * fill below the data line
			 */
			/* NOTE: We're no longer filling below the line due to the potential for multiple values, and them obscuring them
			CGPoint lastPoint = CGPointMake([[[dataPoints lastObject] valueForKey:@"x"] doubleValue],
				[[[dataPoints lastObject] valueForKey:@"y"] doubleValue]);
			CGContextSetFillColorWithColor(context, [[[[BCDashboardViewController graphColors] objectAtIndex:setIndex]
				colorWithAlphaComponent:0.5] CGColor]);
			CGContextBeginPath(context);
			// bottom right corner of fill
			CGContextMoveToPoint(context, firstPoint.x, graphHeight);
			CGContextAddLineToPoint(context, firstPoint.x, firstPoint.y);
			// loop thru all the other points
			for (int pointIndex = 1; pointIndex < [dataPoints count]; pointIndex++) {
				CGPoint destinationPoint = CGPointMake([[[dataPoints objectAtIndex:pointIndex] valueForKey:@"x"] doubleValue],
					[[[dataPoints objectAtIndex:pointIndex] valueForKey:@"y"] doubleValue]);
				CGPoint previousPoint = CGPointMake([[[dataPoints objectAtIndex:pointIndex - 1] valueForKey:@"x"] doubleValue],
					[[[dataPoints objectAtIndex:pointIndex - 1] valueForKey:@"y"] doubleValue]);
				if (previousPoint.y == destinationPoint.y) {
					CGContextAddLineToPoint(context, destinationPoint.x, destinationPoint.y);
				}
				else {
					CGPoint controlPoint1 = CGPointMake(previousPoint.x - xStep * 0.50, previousPoint.y);
					CGPoint controlPoint2 = CGPointMake(destinationPoint.x + xStep * 0.50, destinationPoint.y);
					CGContextAddCurveToPoint(context, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y,
						destinationPoint.x, destinationPoint.y);
				}
			}
			// bottom left corner of fill
			CGContextAddLineToPoint(context, lastPoint.x, graphHeight);
			CGContextClosePath(context);
			CGContextDrawPath(context, kCGPathFill);
			*/

			/*
			 * Draw the data line
			 */
			CGContextSetLineWidth(context, 2.0);
			CGContextSetStrokeColorWithColor(context, [[[BCDashboardViewController graphColors] objectAtIndex:setIndex] CGColor]);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
			for (int pointIndex = 1; pointIndex < [dataPoints count]; pointIndex++) {
				CGPoint destinationPoint = CGPointMake([[[dataPoints objectAtIndex:pointIndex] valueForKey:@"x"] doubleValue],
					[[[dataPoints objectAtIndex:pointIndex] valueForKey:@"y"] doubleValue]);
				CGPoint previousPoint = CGPointMake([[[dataPoints objectAtIndex:pointIndex - 1] valueForKey:@"x"] doubleValue],
					[[[dataPoints objectAtIndex:pointIndex - 1] valueForKey:@"y"] doubleValue]);
				if (previousPoint.y == destinationPoint.y) {
					CGContextAddLineToPoint(context, destinationPoint.x, destinationPoint.y);
				}
				else {
					CGPoint controlPoint1 = CGPointMake(previousPoint.x - xStep * 0.50, previousPoint.y);
					CGPoint controlPoint2 = CGPointMake(destinationPoint.x + xStep * 0.50, destinationPoint.y);
					CGContextAddCurveToPoint(context, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y,
						destinationPoint.x, destinationPoint.y);
				}
			}
			CGContextDrawPath(context, kCGPathStroke);

			// Don't draw the circles if they would be too close together
			if (numberOfPoints <= 30) {
				/*
				 * add the circles at the data points
				 */
				CGRect rect;
				CGContextBeginPath(context);
				CGContextSetLineWidth(context, circleLineWidth);
				CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
				for (int pointIndex = 0; pointIndex < [dataPoints count]; pointIndex++) {
					double x = [[[dataPoints objectAtIndex:pointIndex] valueForKey:@"x"] doubleValue];
					double y = [[[dataPoints objectAtIndex:pointIndex] valueForKey:@"y"] doubleValue];
					rect = CGRectMake(x - circleRadius, y - circleRadius, 2 * circleRadius, 2 * circleRadius);
					CGContextAddEllipseInRect(context, rect);
				}
				CGContextDrawPath(context, kCGPathFillStroke);

				/*
				 * the last circle should be filled
				 */
				rect = CGRectMake(firstPoint.x - circleRadius, firstPoint.y - circleRadius, 2 * circleRadius, 2 * circleRadius);
				CGContextSetFillColorWithColor(context, [[[BCDashboardViewController graphColors] objectAtIndex:setIndex] CGColor]);
				CGContextBeginPath(context);
				CGContextAddEllipseInRect(context, rect);
				CGContextDrawPath(context, kCGPathFillStroke);
			}
		}
	};
	[bcCell.graphView setNeedsDisplay];
}

- (IBAction)dataTypeChanged:(id)sender
{
	self.dataTypeIndex = [sender selectedSegmentIndex];

	NSUInteger customScope = (self.dataTypeIndex == DataTypeTracking ? self.customTrackingScopeValue : self.customNutritionScopeValue);
	[self.scopeControl setTitle:[NSString stringWithFormat:@"%lu days", (long)customScope]
		forSegmentAtIndex:ScopeCustom];

	self.navigationItem.rightBarButtonItem = (self.dataTypeIndex == DataTypeNutrition ? self.editBarButtonItem : nil);

	[self reloadBCTableViewData];
}

- (IBAction)dataScopeChanged:(id)sender
{
	NSUInteger scopeIndex = [sender selectedSegmentIndex];
	if (scopeIndex == ScopeConfigure) {
		BLAlertController* alert = [BLAlertController alertControllerWithTitle:@"Custom range" message:@"Please enter a range, in days"
			preferredStyle:UIAlertControllerStyleAlert];

		[alert addAction:[BLAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
			handler:^(UIAlertAction * action) {
				// Re-select the prior scope index, since they canceled
				[self.scopeControl setSelectedSegmentIndex:self.scopeIndex];
			}]];

		[alert addAction:[BLAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
			handler:^(UIAlertAction * action) {
				NSNumber *value = [[[alert.textFields objectAtIndex:0] text] numberValueDecimal];
				NSUInteger newScopeValue = MIN([value integerValue], 365);
				UserProfileMO *userProfile = [User currentUser].userProfile;
				self.scopeIndex = ScopeCustom;
				if (self.dataTypeIndex == DataTypeTracking) {
					self.customTrackingScopeValue = newScopeValue;
					userProfile.customTrackingTimeframe = @(newScopeValue);
				}
				else {
					self.customNutritionScopeValue = newScopeValue;
					userProfile.customNutritionTimeframe = @(newScopeValue);
				}
				userProfile.status = kStatusPut;
				[userProfile.managedObjectContext BL_save];

				[self.scopeControl setTitle:[NSString stringWithFormat:@"%lu days", (long)newScopeValue]
					forSegmentAtIndex:ScopeCustom];
				[self reloadBCTableViewData];

				[self.scopeControl setSelectedSegmentIndex:self.scopeIndex];
			}]];

		[alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
			textField.text = [NSString stringWithFormat:@"%lu",
				(self.dataTypeIndex == DataTypeTracking ? (long)self.customTrackingScopeValue : (long)self.customNutritionScopeValue)];
			textField.placeholder = @"days";
			textField.keyboardType = UIKeyboardTypeNumberPad;
		}];

		[alert presentInViewController:self animated:YES completion:nil];
	}
	else {
		self.scopeIndex = scopeIndex;
		[self reloadBCTableViewData];
	}
}

- (void)userPermissionsUpdated:(NSNotification* )note
{
	[self buildDataTypeSegmentedControl];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	if (self.dataTypeIndex == DataTypeTracking && section == kSectionTracking) {
		numberOfRows = [self.trackingTypes count];
	}
	else if (self.dataTypeIndex == DataTypeNutrition && section == kSectionNutrients) {
		numberOfRows = [self.nutrients count];
	}
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *vitalsCellId = @"TrackingCellNoAdd"; // Decided to not allow adding from this view, leaving old code JIC
    //static NSString *vitalsCellId = @"TrackingCell";
    static NSString *nutritionCellId = @"NutritionCell";

    BCDashboardCell *cell = nil;
	if (indexPath.section == kSectionTracking) {
		cell = [tableView dequeueReusableCellWithIdentifier:vitalsCellId];
		[self configureTrackingCell:cell inTableView:tableView atIndexPath:indexPath];
	}
	else if (indexPath.section == kSectionNutrients) {
		cell = [tableView dequeueReusableCellWithIdentifier:nutritionCellId];
		[self configureNutritionCell:cell inTableView:tableView atIndexPath:indexPath];
	}
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 88.0f;
	if (self.dataTypeIndex == DataTypeTracking && indexPath.section == kSectionTracking) {
		NSArray *trackingLogsByDay = [self.trackingLogs objectAtIndex:indexPath.row];
		if ([trackingLogsByDay count] == 0) {
			rowHeight = 44.0f;
		}
	}	
	return rowHeight;
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	DDLogInfo(@"view:%@ didUpdateObject:%@", viewController, object);
	[self reloadBCTableViewData];
}

#pragma mark - responders
- (IBAction)editButtonTapped:(id)sender
{
	[self performSegueWithIdentifier:@"EditAlwaysShow" sender: self];
}

@end
