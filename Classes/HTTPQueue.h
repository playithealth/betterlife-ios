//
//  HTTPQueue.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 7/7/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TaskQueue.h"
#import "GTMHTTPFetcher.h"

typedef void (^BC_HTTPQueueCompletionBlock)(NSData *data, NSError *error);


@interface HTTPQueueItem : TaskQueueItem

- (void)cancel;

@end // HTTPQueueItem

@interface HTTPQueue : TaskQueue

+ (id)queueNamed:(NSString *)queueIdentifier completionBlock:(BC_TaskQueueCompletionBlock)completionBlock;
- (void)addRequest:(GTMHTTPFetcher *)fetcher completionBlock:(BC_HTTPQueueCompletionBlock)completionBlock;
- (void)runQueue:(NSError *)error;

@end
