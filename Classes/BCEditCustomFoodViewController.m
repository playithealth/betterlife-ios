//
//  BCEditCustomFoodViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 12/16/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCEditCustomFoodViewController.h"

#import "BCCategorySelectViewController.h"
#import "BCDetailViewDelegate.h"
#import "BCImageCache.h"
#import "CategoryMO.h"
#import "CustomFoodMO.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "UITableView+DownloadImage.h"
#import "UIImage+Additions.h"
#import "User.h"
#import "UserImageMO.h"

@interface BCEditCustomFoodViewController () <BCCategorySelectDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationBarDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *editingIndexPath;
@property (strong, nonatomic) UIImage *productImage;
@end

@implementation BCEditCustomFoodViewController

static const NSInteger kTagImageView = 1000;
static const NSInteger kTagNameLabel = 1001;
static const NSInteger kTagValueLabel = 1002;
static const NSInteger kTagUnitsLabel = 1003;
static const NSInteger kTagTextField = 1004;

static const NSInteger kSectionDetails = 0;
static const NSInteger kSectionServings = 1;
static const NSInteger kSectionNutrition = 2;
static const NSInteger kSectionIngredients = 3;
static const NSInteger kNumberOfSections = 4;

static const NSInteger kRowPicture = 0;
static const NSInteger kRowName = 1;
static const NSInteger kRowCategory = 2;
static const NSInteger kNumberOfRowsDetails = 3;

static const NSInteger kRowServingSize = 0;
static const NSInteger kRowServingQuantity = 1;
static const NSInteger kNumberOfRowsServings = 2;

static const NSInteger kRowCalories = 0;
static const NSInteger kRowCaloriesFromFat = 1;
static const NSInteger kRowSaturatedFatCalories = 2;
static const NSInteger kRowTotalFat = 3;
static const NSInteger kRowSaturatedFat = 4;
static const NSInteger kRowTransFat = 5;
static const NSInteger kRowPolyunsaturatedFat = 6;
static const NSInteger kRowMonounsaturatedFat = 7;
static const NSInteger kRowCholesterol = 8;
static const NSInteger kRowSodium = 9;
static const NSInteger kRowPotassium = 10;
static const NSInteger kRowTotalCarbohydrates = 11;
static const NSInteger kRowDietaryFiber = 12;
static const NSInteger kRowSolubleFiber = 13;
static const NSInteger kRowInsolubleFiber = 14;
static const NSInteger kRowSugars = 15;
static const NSInteger kRowSugarsAlcohol = 16;
static const NSInteger kRowOtherCarbohydrates = 17;
static const NSInteger kRowProtein = 18;
static const NSInteger kRowVitaminAPercent = 19;
static const NSInteger kRowVitaminCPercent = 20;
static const NSInteger kRowCalciumPercent = 21;
static const NSInteger kRowIronPercent = 22;
static const NSInteger kNumberOfRowsNutrition = 23;

//static const NSInteger kRowIngredients = 0;
static const NSInteger kNumberOfRowsIngredients = 1;

- (void)viewDidLoad
{
    [super viewDidLoad];

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kRowPicture inSection:kSectionDetails];
	[self.tableView downloadImageWithImageId:self.customFood.imageId forIndexPath:indexPath withSize:kImageSizeLarge];

	[self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];

	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
	return UIBarPositionTopAttached;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"SetCategory"]) {
		BCCategorySelectViewController *viewController = segue.destinationViewController;
		viewController.managedObjectContext = self.managedObjectContext;
		viewController.delegate = self;
	}
}

#pragma mark - Local
- (NSString *)nameForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *name = nil;
	if (indexPath.section == kSectionDetails) {
		if (indexPath.row == kRowName) {
			name = @"Name";
		}
		else if (indexPath.row == kRowCategory) {
			name = @"Category";
		}
	}
	else if (indexPath.section == kSectionServings) {
		if (indexPath.row == kRowServingSize) {
			name = @"Size";
		}
		else if (indexPath.row == kRowServingQuantity) {
			name = @"Quantity";
		}
	}
	else if (indexPath.section == kSectionNutrition) {
		NSArray *names = @[@"Calories", @"Fat Calories", @"Saturated Fat Calories", @"Total Fat", @"Saturated Fat", @"Trans Fat",
			@"Polyunsaturated Fat", @"Monounsaturated Fat", @"Cholesterol", @"Sodium", @"Potassium", @"Total Carbohydrates",
			@"Dietary Fiber", @"Soluble Fiber", @"Insoluble Fiber", @"Sugars", @"Sugars Alcohol", @"Other Carbohydrates",
			@"Protein", @"Vitamin A", @"Vitamin C", @"Calcium", @"Iron"];
		name = names[indexPath.row];
	}
	else if (indexPath.section == kSectionIngredients) {
		/* code */
	}
	return name;
}

- (NSString *)keyForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *key = nil;
	if (indexPath.section == kSectionDetails) {
		if (indexPath.row == kRowName) {
			key = CustomFoodMOAttributes.name;
		}
	}
	else if (indexPath.section == kSectionServings) {
		if (indexPath.row == kRowServingSize) {
			key = CustomFoodMOAttributes.servingSize;
		}
		else if (indexPath.row == kRowServingQuantity) {
			//key = CustomFoodMOAttributes.servingSize;
		}
	}
	else if (indexPath.section == kSectionNutrition) {
		NSArray *keys = @[NutritionMOAttributes.calories, NutritionMOAttributes.caloriesFromFat, NutritionMOAttributes.saturatedFatCalories,
				NutritionMOAttributes.totalFat, NutritionMOAttributes.saturatedFat, NutritionMOAttributes.transFat,
				NutritionMOAttributes.polyunsaturatedFat, NutritionMOAttributes.monounsaturatedFat, NutritionMOAttributes.cholesterol,
				NutritionMOAttributes.sodium, NutritionMOAttributes.potassium, NutritionMOAttributes.totalCarbohydrates,
				NutritionMOAttributes.dietaryFiber, NutritionMOAttributes.solubleFiber, NutritionMOAttributes.insolubleFiber,
				NutritionMOAttributes.sugars, NutritionMOAttributes.sugarsAlcohol, NutritionMOAttributes.otherCarbohydrates,
				NutritionMOAttributes.protein, NutritionMOAttributes.vitaminAPercent, NutritionMOAttributes.vitaminCPercent,
				NutritionMOAttributes.calciumPercent, NutritionMOAttributes.ironPercent];
		key = keys[indexPath.row];
	}
	else if (indexPath.section == kSectionIngredients) {
		/* code */
	}
	return key;
}

- (NSString *)unitsForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *units = nil;
	if (indexPath.section == kSectionNutrition) {
		NSArray *unitStrings = @[@"", @"", @"", @"g", @"g", @"g",
			@"g", @"g", @"mg", @"mg", @"g", @"g",
			@"g", @"g", @"g", @"g", @"g", @"g",
			@"g", @"%", @"%", @"%", @"%"];
		units = unitStrings[indexPath.row];
	}
	return units;
}

- (IBAction)promptUserForPictureSource
{
	UIActionSheet *actionSheet = [[UIActionSheet alloc]
		initWithTitle:@"Choose a source" 
		delegate:self
		cancelButtonTitle:@"Cancel" 
		destructiveButtonTitle:nil
		otherButtonTitles:@"Take a new picture", @"Choose from library", nil];
	[actionSheet showInView:self.view];
}

- (IBAction)save:(id)sender
{
	[self.delegate view:self didUpdateObject:self.customFood];
}

- (IBAction)cancel:(id)sender
{
	[self.delegate view:self didUpdateObject:nil];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return kNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	switch (section) {
		case kSectionDetails:
			numberOfRows = kNumberOfRowsDetails;
			break;
		case kSectionServings:
			numberOfRows = kNumberOfRowsServings;
			break;
		case kSectionNutrition:
			numberOfRows = kNumberOfRowsNutrition;
			break;
		case kSectionIngredients:
			numberOfRows = kNumberOfRowsIngredients;
			break;
		default:
			break;
	}

	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *pictureCellId = @"PictureCell";
	static NSString *detailCellId = @"DetailCell";
	static NSString *categoryCellId = @"CategoryCell";
	static NSString *nutritionCellId = @"NutritionCell";
	static NSString *ingredientsCellId = @"IngredientsCell";

	UITableViewCell *cell = nil;

	if (indexPath.section == kSectionDetails) {
		if (indexPath.row == kRowPicture) {
			cell = [tableView dequeueReusableCellWithIdentifier:pictureCellId];
		}
		else if (indexPath.row == kRowName) {
			cell = [tableView dequeueReusableCellWithIdentifier:detailCellId];
		}
		else if (indexPath.row == kRowCategory) {
			cell = [tableView dequeueReusableCellWithIdentifier:categoryCellId];
		}
	}
	else if (indexPath.section == kSectionServings) {
		cell = [tableView dequeueReusableCellWithIdentifier:detailCellId];
	}
	else if (indexPath.section == kSectionNutrition) {
		cell = [tableView dequeueReusableCellWithIdentifier:nutritionCellId];
	}
	else if (indexPath.section == kSectionIngredients) {
		cell = [tableView dequeueReusableCellWithIdentifier:ingredientsCellId];
	}

	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionDetails) {
		if (indexPath.row == kRowPicture) {
			UIImageView *imageView = (UIImageView *)[cell viewWithTag:kTagImageView];
			if (self.customFood.image) {
				imageView.image = [self.customFood imageAsUIImage];
			}
			else {
				imageView.image = [[BCImageCache sharedCache] imageWithImageId:self.customFood.imageId
					withItemType:kItemTypeProduct withSize:kImageSizeLarge];
			}
		}
		else if (indexPath.row == kRowName) {
			UILabel *nameLabel = (UILabel *)[cell viewWithTag:kTagNameLabel];
			nameLabel.text = [self nameForRowAtIndexPath:indexPath];
			UILabel *valueLabel = (UILabel *)[cell viewWithTag:kTagValueLabel];
			NSString *valueKey = [self keyForRowAtIndexPath:indexPath];
			if (valueKey) {
				valueLabel.text = [self.customFood valueForKey:valueKey];
			}
		}
		else if (indexPath.row == kRowCategory) {
			UILabel *nameLabel = (UILabel *)[cell viewWithTag:kTagNameLabel];
			nameLabel.text = [self nameForRowAtIndexPath:indexPath];
			UILabel *valueLabel = (UILabel *)[cell viewWithTag:kTagValueLabel];
			valueLabel.text = self.customFood.category.name;
		}
	}
	else if (indexPath.section == kSectionServings) {
		if (indexPath.row == kRowServingSize) {
			UILabel *nameLabel = (UILabel *)[cell viewWithTag:kTagNameLabel];
			nameLabel.text = [self nameForRowAtIndexPath:indexPath];
			UILabel *valueLabel = (UILabel *)[cell viewWithTag:kTagValueLabel];
			valueLabel.text = @"1 cup";
		}
		else if (indexPath.row == kRowServingQuantity) {
			UILabel *nameLabel = (UILabel *)[cell viewWithTag:kTagNameLabel];
			nameLabel.text = [self nameForRowAtIndexPath:indexPath];
			UILabel *valueLabel = (UILabel *)[cell viewWithTag:kTagValueLabel];
			valueLabel.text = @"1";
		}
	}
	else if (indexPath.section == kSectionNutrition) {
		UILabel *nameLabel = (UILabel *)[cell viewWithTag:kTagNameLabel];
		nameLabel.text = [self nameForRowAtIndexPath:indexPath];
		UILabel *valueLabel = (UILabel *)[cell viewWithTag:kTagValueLabel];
		NSString *valueKeyPath = [NSString stringWithFormat:@"nutrition.%@", [self keyForRowAtIndexPath:indexPath]];
		if (valueKeyPath) {
			NSNumber *value = [self.customFood valueForKeyPath:valueKeyPath];
			if ([value doubleValue] > 0.0f) {
				valueLabel.text = [NSString stringWithFormat:@"%@", value];
			}
			else {
				valueLabel.text = nil;
			}
		}
		UILabel *unitsLabel = (UILabel *)[cell viewWithTag:kTagUnitsLabel];
		NSString *units = [self unitsForRowAtIndexPath:indexPath];
		unitsLabel.text = units;
	}
	else if (indexPath.section == kSectionIngredients) {
		/* code */
	}
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat headerHeight = 0.0f;
	if (section != kSectionDetails) {
		headerHeight = 40.0f;
	}
	return headerHeight;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString *headerText = nil;
	if (section == kSectionDetails) {
		headerText = nil;
	}
	else if (section == kSectionServings) {
		headerText = @"SERVINGS PER CONTAINER";
	}
	else if (section == kSectionNutrition) {
		headerText = @"NUTRITION";
	}
	else if (section == kSectionIngredients) {
		headerText = @"INGREDIENTS";
	}
	return headerText;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44.0f;
	if (indexPath.section == kSectionDetails && indexPath.row == kRowPicture) {
		rowHeight = 150.0f;
	}
	return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.editingIndexPath = nil;

	if (indexPath.section == kSectionDetails) {
		if (indexPath.row == kRowPicture) {
			[self promptUserForPictureSource];
		}
		else if (indexPath.row == kRowName) {
			UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
			UILabel *label = (UILabel *)[cell viewWithTag:kTagValueLabel];
			UITextField *textField = (UITextField *)[cell viewWithTag:kTagTextField];
			textField.text = label.text;
			textField.hidden = NO;
			label.hidden = !textField.hidden;
			[textField becomeFirstResponder];

			self.editingIndexPath = indexPath;
		}
	}
	else if (indexPath.section == kSectionNutrition) {
		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
		UILabel *label = (UILabel *)[cell viewWithTag:kTagValueLabel];
		UITextField *textField = (UITextField *)[cell viewWithTag:kTagTextField];
		textField.text = label.text;
		textField.hidden = NO;
		label.hidden = !textField.hidden;
		[textField becomeFirstResponder];

		self.editingIndexPath = indexPath;
	}
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//	CGPoint textFieldPosition = [textField convertPoint:CGPointZero toView:self.tableView];
//	NSIndexPath* indexPath = [self.tableView indexPathForRowAtPoint:textFieldPosition];
//
//	[self saveValueFromTextField:textField atIndexPath:indexPath];

	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	CGPoint textFieldPosition = [textField convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath* indexPath = [self.tableView indexPathForRowAtPoint:textFieldPosition];

	[self saveValueFromTextField:textField atIndexPath:indexPath];
}

- (void)saveValueFromTextField:(UITextField *)textField atIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	UILabel *label = (UILabel *)[cell viewWithTag:kTagValueLabel];
	label.hidden = NO;
	textField.hidden = !label.hidden;
	[textField resignFirstResponder];

	if (indexPath.section == kSectionDetails) {
		if (indexPath.row == kRowName) {
			self.customFood.name = textField.text;
		}
	}
	else if (indexPath.section == kSectionServings) {
	}
	else if (indexPath.section == kSectionNutrition) {
		if (!self.customFood.nutrition) {
			self.customFood.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
		}
		if (indexPath.row == kRowCalories) {
			self.customFood.nutrition.calories = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowCaloriesFromFat) {
			self.customFood.nutrition.caloriesFromFat = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowSaturatedFatCalories) {
			self.customFood.nutrition.saturatedFatCalories = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowTotalFat) {
			self.customFood.nutrition.totalFat = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowSaturatedFat) {
			self.customFood.nutrition.saturatedFat = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowTransFat) {
			self.customFood.nutrition.transFat = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowPolyunsaturatedFat) {
			self.customFood.nutrition.polyunsaturatedFat = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowMonounsaturatedFat) {
			self.customFood.nutrition.monounsaturatedFat = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowCholesterol) {
			self.customFood.nutrition.cholesterol = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowSodium) {
			self.customFood.nutrition.sodium = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowPotassium) {
			self.customFood.nutrition.potassium = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowTotalCarbohydrates) {
			self.customFood.nutrition.totalCarbohydrates = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowDietaryFiber) {
			self.customFood.nutrition.dietaryFiber = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowSolubleFiber) {
			self.customFood.nutrition.solubleFiber = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowInsolubleFiber) {
			self.customFood.nutrition.insolubleFiber = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowSugars) {
			self.customFood.nutrition.sugars = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowSugarsAlcohol) {
			self.customFood.nutrition.sugarsAlcohol = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowOtherCarbohydrates) {
			self.customFood.nutrition.otherCarbohydrates = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowProtein) {
			self.customFood.nutrition.protein = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowVitaminAPercent) {
			self.customFood.nutrition.vitaminAPercent = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowVitaminCPercent) {
			self.customFood.nutrition.vitaminCPercent = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowCalciumPercent) {
			self.customFood.nutrition.calciumPercent = [textField.text numberValueDecimal];
		}
		else if (indexPath.row == kRowIronPercent) {
			self.customFood.nutrition.ironPercent = [textField.text numberValueDecimal];
		}
	}
	else if (indexPath.section == kSectionIngredients) {
	}

	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
}

#pragma mark UIImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info 
{
	NSManagedObjectContext *context = self.managedObjectContext;
	User *thisUser = [User currentUser];

	UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
	[self.customFood setImageWithUIImage:chosenImage];
	
	// create a 200x200 size image and save it to be uploaded
	UIImage *imageToUpload = [chosenImage BC_imageWithTargetSize:CGSizeMake(200, 200)];
	UserImageMO *customImage = [UserImageMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K = %d AND %K = %@ AND %K = %@",
			UserImageMOAttributes.imageType, kUserImageTypeProduct, 
			UserImageMOAttributes.accountId, thisUser.accountId, 
			UserImageMOAttributes.imageId, self.customFood.cloudId]
		inContext:context];
	if (!customImage) {
		customImage = [UserImageMO insertInManagedObjectContext:context];
		customImage.imageType = @(kUserImageTypeProduct);
		customImage.accountId = thisUser.accountId;
		customImage.imageId = self.customFood.cloudId;
	}
	[customImage setUserImageWithUIImage:imageToUpload];
	[customImage markForSync];
	[context BL_save];

	[[BCImageCache sharedCache] setImage:imageToUpload forProductId:self.customFood.cloudId size:@"large"];

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kRowPicture inSection:kSectionDetails];
	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];

	[picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker 
{ 
	[picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex != [actionSheet cancelButtonIndex]) {
		switch (buttonIndex) {
			case 0:
				[self getMediaFromSource:UIImagePickerControllerSourceTypeCamera];
				break;
			case 1:	
			default:
				[self getMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
				break;
		}		
	}
}

#pragma mark - Image Handling
- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType 
{ 
	NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType]; 
	if ([UIImagePickerController isSourceTypeAvailable:sourceType] 
			&& [mediaTypes count] > 0) { 
		NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType]; 
		UIImagePickerController *picker = [[UIImagePickerController alloc] init]; 
		picker.mediaTypes = mediaTypes;
		picker.delegate = self;
		picker.allowsEditing = YES;
		picker.sourceType = sourceType;
		[self presentViewController:picker animated:YES completion:nil];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] 
			initWithTitle:@"Error accessing media"
			message:@"Device doesn't support that media source."
			delegate:self 
			cancelButtonTitle:@"OK"
			otherButtonTitles:nil];
		[alert show];
	}
}

#pragma mark - keyboard handling
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
            selector:@selector(keyboardWasShown:)
            name:UIKeyboardDidShowNotification object:nil];
 
   [[NSNotificationCenter defaultCenter] addObserver:self
             selector:@selector(keyboardWillBeHidden:)
             name:UIKeyboardWillHideNotification object:nil];
 
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
	NSDictionary *info = [aNotification userInfo];
	CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);

	self.tableView.contentInset = contentInsets;
	self.tableView.scrollIndicatorInsets = contentInsets;

	[self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
	self.tableView.contentInset = UIEdgeInsetsZero;
	self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

#pragma mark - BCCategorySelectDelegate
- (void)categorySelectView:(BCCategorySelectViewController *)categorySelectView didSelectCategory:(CategoryMO *)category
{
	self.customFood.category = category;

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kRowCategory inSection:kSectionDetails];
	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];

	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
