//
//  BLCoachInvitationViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 7/25/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCViewController.h"
#import "BLOnboardViewDelegate.h"

@class CoachInviteMO;
@protocol BLOnboardDelegate;

@interface BLCoachInvitationViewController : BCViewController <BLOnboardViewDelegate>
@property (weak, nonatomic) CoachInviteMO *invitation; ///< This is weak as we don't wish to own it. If it becomes nil, then sync may have deleted
@property (strong, nonatomic) NSArray *viewStack;
@property (weak, nonatomic) id<BLOnboardDelegate> delegate;
@end
