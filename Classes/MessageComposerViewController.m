//
//  MessageComposerViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/20/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "MessageComposerViewController.h"

#import "UIColor+Additions.h"
#import "User.h"

@interface MessageComposerViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewBottomConstraint;

@property (assign, nonatomic) CGRect oldRect;
@property (strong, nonatomic) NSTimer *caretVisibilityTimer;

@end

@implementation MessageComposerViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.senderLabel.text = self.replyMessage.sender;
	self.subjectLabel.text = [NSString stringWithFormat:@"RE: %@", self.replyMessage.subject];

	self.textView.delegate = self;

	[self registerForKeyboardNotifications];

	[self.textView becomeFirstResponder];
}

#pragma mark - Local
- (IBAction)sendTapped:(id)sender 
{
	User *thisUser = [User currentUser];

	InboxMessage *newReply = [InboxMessage insertInManagedObjectContext:self.replyMessage.managedObjectContext];
	newReply.accountId = [thisUser accountId];
	newReply.loginId = [thisUser loginId];
	newReply.senderId = [thisUser loginId];
	newReply.sender = [thisUser name];
	newReply.createdOn = [NSNumber numberWithInt:[[NSDate date] timeIntervalSince1970]];
	newReply.updatedOn = [NSNumber numberWithInt:[[NSDate date] timeIntervalSince1970]];
	newReply.subject = [NSString stringWithFormat:@"RE: %@", self.replyMessage.subject];
	newReply.text = self.textView.text;
	newReply.status = kStatusPost;
	newReply.replyTo = self.replyMessage;

	[newReply.managedObjectContext save:nil];

	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelTapped:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - keyboard
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
            selector:@selector(keyboardWasShown:)
            name:UIKeyboardDidShowNotification object:nil];
 
   [[NSNotificationCenter defaultCenter] addObserver:self
             selector:@selector(keyboardWillBeHidden:)
             name:UIKeyboardWillHideNotification object:nil];
 
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
 
	self.textViewBottomConstraint.constant = kbSize.height;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
	self.textViewBottomConstraint.constant = 0;
}

- (void)scrollCaretToVisible
{
    //This is where the cursor is at.
    CGRect caretRect = [self.textView caretRectForPosition:self.textView.selectedTextRange.end];

    if (CGRectEqualToRect(caretRect, self.oldRect)) {
        return;
	}

    self.oldRect = caretRect;

    //This is the visible rect of the textview.
    CGRect visibleRect = self.textView.bounds;
    visibleRect.size.height -= (self.textView.contentInset.top + self.textView.contentInset.bottom);
    visibleRect.origin.y = self.textView.contentOffset.y;

    //We will scroll only if the caret falls outside of the visible rect.
    if (!CGRectContainsRect(visibleRect, caretRect)) {
        CGPoint newOffset = self.textView.contentOffset;

        newOffset.y = MAX((caretRect.origin.y + caretRect.size.height) - visibleRect.size.height + 10, 0);

        [self.textView setContentOffset:newOffset animated:YES];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{    
    [self.caretVisibilityTimer invalidate];
    self.caretVisibilityTimer = nil;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{ 
    self.oldRect = [self.textView caretRectForPosition:self.textView.selectedTextRange.end];
    self.caretVisibilityTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(scrollCaretToVisible) userInfo:nil repeats:YES];
}

@end
