//
//  MealDetailViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/18/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCViewController.h"
#import "RecipeMO.h"
#import "MealDetailCell.h"
#import "MealEditViewController.h"

@interface RecipeDetailViewController : UITableViewController
<MealEditViewDelegate, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate>

enum sections { Details, Tags, Ingredients, AddToShoppingList, SetupShoppingListItems, Instructions };

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) id<MealEditViewDelegate>delegate;
@property (strong, nonatomic) RecipeMO *recipe;
@property (strong, nonatomic) id recipeId;

@end
