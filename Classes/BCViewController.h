//
//  BCViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/21/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCSidebarViewController.h"

@interface BCViewController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

+ (UIView *)customNavigationHeaderViewForTitle:(NSString *)title;

- (void) willNavigateAway;

@end
