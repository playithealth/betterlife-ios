//
//  BCStrengthSearchRecentViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/20/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BCStrengthSearchDataModel;

@interface BCStrengthSearchRecentViewController : UITableViewController
@property (strong, nonatomic) BCStrengthSearchDataModel *dataModel;
@end
