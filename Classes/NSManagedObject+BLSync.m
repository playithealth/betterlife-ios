//
//  NSManagedObject+BLSync.m
//  BettrLife
//
//  Created by Greg Goodrich on 12/2/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "NSManagedObject+BLSync.h"
#import "SyncUpdateMO.h"
#import "BLSyncEntity.h"
#import "User.h"

@implementation NSManagedObject (BLSync)

+ (NSPredicate *)BL_loginIdPredicate {
	return [NSPredicate predicateWithFormat:@"loginId = %@", [User loginId]];
}

+ (NSPredicate *)BL_accountIdPredicate {
	return [NSPredicate predicateWithFormat:@"accountId = %@", [User accountId]];
}

/*
 * Modified Delete method - This method will find all existing managed objects for the user with loginId, and track whether there were
 * updates from the cloud for each. All updates are applied to existing objects, and new objects are created as needed.
 * Any objects that did not get updates (not referenced in the cloud data) will be deleted from the persistent store
 * at the end of the process.
 *
 * BLSyncEntity protocol uses:
 *  - for finding the existing objects via the uniqueKeyFromDict: method
 *  - for inserting new managed objects if they do not exist via the insertInManagedObjectContext: method
 *  - for setting the loginId in the managed object via the loginId property (optional)
 *  - for updating/deleting records for this user, the userPredicate is leveraged
 *  - for calling setWithUpdateDictionary: to apply the updates to the object
 *  - for tracking the lastUpdate information by leveraging the syncName property
 */
+ (void)BL_syncUpdateUsingModifiedDeleteStrategyForLogin:(NSNumber *)loginId withUpdates:(NSArray *)updates
inMOC:(NSManagedObjectContext *)moc
{
	assert([self conformsToProtocol:@protocol(BLSyncEntity)] && "Unexpected class passed in, does not conform to BLSyncEntity");

	NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:moc];
	NSMutableArray *allMOs = [[self MR_findAllWithPredicate:[(id <BLSyncEntity>)self userPredicate] inContext:scratchContext] mutableCopy];

	// process the updates
	for (NSDictionary *updateDict in updates) {
		id uniqueKey = [(id <BLSyncEntity>)self uniqueKeyFromDict:updateDict];

		// this code block is used below to grab the index of the matching managedObject by id
		// TODO This should probably be a category method itself
		NSUInteger index = [allMOs indexOfObjectPassingTest:^BOOL(id<BLSyncEntity> obj, NSUInteger idx, BOOL *stop) {
			BOOL isEqual = NO;
			if (obj.cloudId && uniqueKey) {
				if ([uniqueKey isKindOfClass:[NSNumber class]] && [obj.cloudId isKindOfClass:[NSNumber class]]) {
					isEqual = ([obj.cloudId isEqualToNumber:uniqueKey]);
				}
				else if ([uniqueKey isKindOfClass:[NSString class]] && [obj.cloudId isKindOfClass:[NSString class]]) {
					isEqual = ([obj.cloudId isEqualToString:uniqueKey]);
				}
				else {
					isEqual = ([obj.cloudId isEqual:uniqueKey]);
				}
			}

			return isEqual;
		}];

		NSManagedObject<BLSyncEntity> *managedObject = nil;
		if (index == NSNotFound) {
			managedObject = [(id <BLSyncEntity>)self insertInManagedObjectContext:scratchContext];
			if ([managedObject respondsToSelector:@selector(setLoginId:)]) {
				[managedObject setLoginId:loginId];
			}
		}
        else {
            managedObject = [allMOs objectAtIndex:index];
			[allMOs removeObjectAtIndex:index];
        }

		[managedObject setWithUpdateDictionary:updateDict];
	}

	// Delete any MOs that still exist in allMOs, as these weren't sent down from the cloud, thus they must
	// have been deleted
	for (NSManagedObject *mo in allMOs) {
		[scratchContext deleteObject:mo];
	}

	// Push the changes to the main context all at once
	[scratchContext BL_save];

	// Save the context.
	[moc BL_save];
}

/*
 * Normal sync method - This method will insert/update/delete managed objects for the user with loginId. Deletes happen based upon the
 * standard kDeleted constant being set in the update dictionary
 *  - for finding the existing objects via the uniqueKeyFromDict: method
 *  - for inserting new managed objects if they do not exist via the insertInManagedObjectContext: method
 *  - for setting the loginId in the managed object via the loginId property (optional)
 *  - for updating/deleting records for this user, the userPredicate is leveraged
 *  - for calling setWithUpdateDictionary: to apply the updates to the object
 *  - for tracking the lastUpdate information by leveraging the syncName property
 */
+ (void)BL_syncUpdateForLogin:(NSNumber *)loginId withUpdates:(NSArray *)updates inMOC:(NSManagedObjectContext *)moc
{
	assert([self conformsToProtocol:@protocol(BLSyncEntity)] && "Unexpected class passed in, does not conform to BLSyncEntity");
	
	// process any updates, this check optimizes things by allowing us to not fetch existing objects if no updates come through
	if ([updates count] > 0) {
		// fetch all the cached managedObjects for this user
		NSArray *allMOs = [self MR_findAllWithPredicate:[(id <BLSyncEntity>)self userPredicate] inContext:moc];

		for (NSMutableDictionary *updateDict in updates) {
			id uniqueKey = [(id <BLSyncEntity>)self uniqueKeyFromDict:updateDict];

			// this code block is used below to grab the index of the matching managedObject by id
			// TODO This should probably be a category method itself
			BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id<BLSyncEntity> obj, NSUInteger idx, BOOL *stop) {
				BOOL isEqual = NO;
				if (obj.cloudId && uniqueKey) {
					if ([uniqueKey isKindOfClass:[NSNumber class]] && [obj.cloudId isKindOfClass:[NSNumber class]]) {
						isEqual = ([obj.cloudId isEqualToNumber:uniqueKey]);
					}
					else if ([uniqueKey isKindOfClass:[NSString class]] && [obj.cloudId isKindOfClass:[NSString class]]) {
						isEqual = ([obj.cloudId isEqualToString:uniqueKey]);
					}
					else {
						isEqual = ([obj.cloudId isEqual:uniqueKey]);
					}
				}

				return isEqual;
			};

			NSUInteger cacheIndex = [allMOs indexOfObjectPassingTest:SameCloudId];

			BOOL deleted = [[updateDict valueForKey:kDeleted] boolValue];

			NSManagedObject<BLSyncEntity> *managedObject = nil;
			if (cacheIndex == NSNotFound) {
				// this has to be checked inside NSNotFound - because the item may not exist on 
				// the phone
				if (!deleted) {
					managedObject = [(id <BLSyncEntity>)self insertInManagedObjectContext:moc];
					if ([managedObject respondsToSelector:@selector(setLoginId:)]) {
						[managedObject setLoginId:loginId];
					}
				}
			}
			else {
				managedObject = [allMOs objectAtIndex:cacheIndex];

				if (deleted) {
					[moc deleteObject:managedObject];
					continue;
				}
			}

			[managedObject setWithUpdateDictionary:updateDict];
		}

		// Save the context.
		[moc BL_save];
	}
}

/*
 * Delete strategy method - This method deletes all existing managed objects first, then adds all managed objects coming from the cloud.
 * This is mostly used for data sets where it is too difficult to identify existing managed objects (complicated unique criteria) or just
 * not worth the effort, such as with plan timelines
 *
 * BLSyncEntity protocol is used and required for this method
 * - No cloudId is needed to leverage this method
 */
+ (void)BL_syncUpdateUsingDeleteStrategyForLogin:(NSNumber *)loginId withUpdates:(NSArray *)updates inMOC:(NSManagedObjectContext *)moc
{
	assert([self conformsToProtocol:@protocol(BLSyncEntity)] && "Unexpected class passed in, does not conform to BLSyncEntity");

	NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:moc];
	NSArray *allMOs = [self MR_findAllWithPredicate:[(id <BLSyncEntity>)self userPredicate] inContext:scratchContext];
	for (NSManagedObject *managedObject in allMOs) {
		[scratchContext deleteObject:managedObject];
	}

	// Add all rows from cloud
	NSManagedObject<BLSyncEntity> *managedObject = nil;
	for (NSDictionary *updateDict in updates) {
		managedObject = [(id <BLSyncEntity>)self insertInManagedObjectContext:moc];
		if ([managedObject respondsToSelector:@selector(setLoginId:)]) {
			[managedObject setLoginId:loginId];
		}
		[managedObject setWithUpdateDictionary:updateDict];
	}

	// Push the changes to the main context all at once
	[scratchContext BL_save];

	// Save the context.
	[moc BL_save];
}

@end
