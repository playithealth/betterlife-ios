//
//  BCRecipeBoxViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 8/19/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "BCRecipeBoxViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCObjectManager.h"
#import "BCTableViewCell.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcher.h"
#import "GTMHTTPFetcherAdditions.h"
#import "RecipeDetailViewController.h"
#import "MealIngredient.h"
#import "BCRecipeBoxViewController.h"
#import "NSCalendar+Additions.h"
#import "NSDate+Additions.h"
#import "NSString+BCAdditions.h"
#import "NumberValue.h"
#import "RecipeMO.h"
#import "RecipeTagMO.h"
#import "StarRatingControl.h"
#import "UIColor+Additions.h"
#import "UIImage+Additions.h"
#import "UITableView+DownloadImage.h"
#import "User.h"

@interface BCRecipeBoxViewController() <MealEditViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *emptyImageView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (strong, nonatomic) NSDateFormatter *formatter;
@property (strong, nonatomic) NSArray *localRecipes;
@property (strong, nonatomic) NSArray *searchRecipes;
@property (assign, nonatomic) BOOL allowRecipeSearching;
@property (assign, nonatomic) NSUInteger recipePages;
@property (assign, nonatomic) BOOL showAddMealFromURLRow;

- (void)loadImagesForOnscreenRows;
@end

@implementation BCRecipeBoxViewController

static const CGFloat kSectionHeaderHeight = 32.0;
static const NSInteger kSectionAddFromURL = 0;
static const NSInteger kSectionLocalRecipes = 1;
static const NSInteger kSectionSearchRecipes = 2;
static const NSInteger kSectionSearchMore = 3;

static const NSInteger kTagAdvisorTag = 12345;

#pragma mark - memory management
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:(NSCoder *)aDecoder];
	if (self) {
		self.formatter = [[NSDateFormatter alloc] init];
		[self.formatter setDateFormat:@"MMM d"];

	}
	return self;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];

	self.navigationController.toolbarHidden = YES;

	self.needsSync = NO;
	self.allowRecipeSearching = NO;
	self.recipePages = 0;
	
	[self refresh];

}

- (void)viewWillAppear:(BOOL)animated
{
	// Want to receive notifications regarding the meal entity changing from sync
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(syncCheckDidFinish:)
		name:@"syncCheck" object:nil];

	__typeof__(self) __weak weakSelf = self;
	[[NSNotificationCenter defaultCenter]
		addObserverForName:[RecipeMO entityName]
		object:nil
		queue:nil
		usingBlock:^(NSNotification *notification) {
			[weakSelf reloadData];
		}];

    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];
	[self.tableView cancelAllImageFetchers];

	// Remove the notification observer for meal entity changing from sync
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"syncCheck" object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:[RecipeMO entityName] object:nil];

	if (self.needsSync) {
		[BCObjectManager syncCheck:SendOnly];
		self.needsSync = NO;
	}
    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowLocalRecipeDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		RecipeMO *localRecipe = [self.localRecipes objectAtIndex:[indexPath row]];

		RecipeDetailViewController *detailView = segue.destinationViewController;
		detailView.managedObjectContext = self.managedObjectContext;
		detailView.recipe = localRecipe;
		detailView.recipeId = localRecipe.cloudId;
		detailView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"ShowWebRecipeDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		NSDictionary *webRecipe = [self.searchRecipes objectAtIndex:indexPath.row];

		RecipeMO *temp = [[RecipeMO alloc] initWithEntity:[RecipeMO entityInManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:nil];

		temp.externalId = [webRecipe valueForKey:kId];
		temp.name = [webRecipe valueForKey:kName];
		temp.calories = [[webRecipe objectForKey:kNutritionCalories] numberValueDecimal];
		if ((NSNull *)[webRecipe objectForKey:kRecipeImageURL] == [NSNull null]) {
			temp.imageUrl = [webRecipe objectForKey:kRecipeImageURL];
		}

		RecipeDetailViewController *detailView = segue.destinationViewController;
		detailView.managedObjectContext = self.managedObjectContext;
		detailView.recipe = temp;
		detailView.recipeId = [webRecipe valueForKey:kId];
		detailView.delegate = self;
	}
}

#pragma mark - BCTableViewController overrides
- (void)refresh
{
	// if the user is trying to refresh the data, throw away the search text
	// since a refresh currently ignores the search text anyway, if we leave this
	// here, it looks a little weird...
	self.searchTextField.text = @"";

	[BCObjectManager syncCheck:SendAndReceive];
}

#pragma mark - local methods
- (void)reloadData
{
	// Ensure that the image trackers don't try to update indexPaths until after we refresh
	[self.tableView clearTrackerIndexPaths];
	[self.tableView reloadData];
}

- (BOOL)isRecommendedMeal:(RecipeMO *)meal
{
	return (self.date && ([meal.recommendedDate timeIntervalSinceDate:self.date] <= 0.0));
}

- (void)addMealFromURL:(NSString *)url
{
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:kSectionAddFromURL]];
	// Add an activity indicator
	UIActivityIndicatorView *indicator = nil;
	if (cell) {
		indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		indicator.center = cell.imageView.center;
		cell.textLabel.text = [NSString stringWithFormat:@"Adding meal for URL: '%@'", url];
		[cell.contentView addSubview:indicator];
		[indicator startAnimating];
	}

	NSDictionary *urlDict = [NSDictionary dictionaryWithObject:[url lowercaseString] forKey:@"url"];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory mealsURL] httpMethod:kPost 
		postData:[NSJSONSerialization dataWithJSONObject:urlDict options:kNilOptions error:nil]];
	[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		self.showAddMealFromURLRow = NO;

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];

			// pop up an alert view
			UIAlertView *alert = [[UIAlertView alloc] 
				initWithTitle:@"Error adding meal from URL"
				message:[error description]
				delegate:self 
				cancelButtonTitle:@"OK"
				otherButtonTitles:nil];
			[alert show];

			// Reload the table to get rid of the "Add recipe from URL" row
			[self reloadData];
		}
		else {
			// fetch succeeded
			NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			RecipeMO *newMeal = [RecipeMO insertInManagedObjectContext:self.managedObjectContext];
			newMeal.accountId = [[User currentUser] accountId];

			[newMeal setWithUpdateDictionary:resultsDict];

			if (![self.managedObjectContext save:&error]) {
				DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
			}

			[self reloadDataCache];

			NSUInteger row = [self.localRecipes indexOfObject:newMeal];
			NSIndexPath *newItemIndexPath = [NSIndexPath indexPathForRow:row inSection:kSectionLocalRecipes];
			[self.tableView selectRowAtIndexPath:newItemIndexPath animated:NO
				scrollPosition:UITableViewScrollPositionMiddle];
			// TODO
			//[self showMealDetailsForIndexPath:newItemIndexPath];
		}
		if (indicator) {
			[indicator stopAnimating];
			[indicator removeFromSuperview];
		}
	}];
}

- (void)sendSearchRequest
{
	// Hide the keyboard (it may not be in view, but is easiest to just try, in case)
	[self.searchTextField resignFirstResponder];

	// grab the search cell, maybe we should scroll this into view as well
	NSIndexPath *searchCellIndexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionSearchMore];
	BCTableViewCell *searchCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:searchCellIndexPath];

	NSString *searchCellText = nil;
	if ([self.searchRecipes count]) {
		searchCellText = [NSString stringWithFormat:@"Fetching more recipes containing “%@”", self.searchTextField.text];
	}
	else {
		searchCellText = [NSString stringWithFormat:@"Searching for recipes containing “%@”", self.searchTextField.text];
	}

	if ([searchCell.bcTextLabel respondsToSelector:@selector(setAttributedText:)]) {
		NSMutableAttributedString *searchAttrString = [[NSMutableAttributedString alloc] initWithString:searchCellText 
			attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_mediumDarkGrayColor] }];
		[searchAttrString setAttributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_darkGrayColor] }
			range:NSMakeRange(33, [self.searchTextField.text length] + 2)];
		[searchCell.bcTextLabel setAttributedText:searchAttrString];
	}
	else {
		searchCell.bcTextLabel.text = searchCellText;
	}

	[self.tableView scrollToRowAtIndexPath:searchCellIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

	[searchCell.spinner startAnimating];

	// Perform recipe search
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory googleRecipeSearchForSearchTerm:self.searchTextField.text
		page:(self.recipePages + 1) includeDetails:NO]];
	[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *returnedRecipes = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			if (self.searchRecipes) {
				self.searchRecipes = [self.searchRecipes arrayByAddingObjectsFromArray:returnedRecipes];
			}
			else {
				self.searchRecipes = returnedRecipes;
			}
			self.recipePages++;
			[self reloadData];
		}
		[searchCell.spinner stopAnimating];

		[self loadImagesForOnscreenRows];

		[self.tableView scrollToRowAtIndexPath:searchCellIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}];

}

- (IBAction)searchButtonTapped:(id)sender
{
	[self sendSearchRequest];
}

- (void)updateFrequencyForMeal:(RecipeMO *)meal
{
	// Initialize to the distant future
	meal.recommendedDate = [NSDate distantFuture];

	NSSet *plannerDays = [meal.plannerDays filteredSetUsingPredicate:
		[NSPredicate predicateWithFormat:@"(accountId = %@) AND (status <> %@)", 
		[[User currentUser] accountId], kStatusDelete]];

	// This array allows us to ignore the same meal on the same day
	NSArray *uniqueMealDays = [plannerDays valueForKeyPath:@"@distinctUnionOfObjects.date"];

	// If this meal hasn't been eaten at least twice, we can't really calculate the period to recommend it
	NSUInteger countDate = [uniqueMealDays count];
	if (countDate < 2) {
		return;
	}

	NSDate *maxDate = [plannerDays valueForKeyPath:@"@max.date"];
	NSDate *minDate = [plannerDays valueForKeyPath:@"@min.date"];

	// this calculates the average number of days between when this meal is planned
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSInteger freqDays = [calendar BC_daysFromDate:minDate toDate:maxDate] / (countDate - 1);
	meal.frequency = [NSNumber numberWithInteger:freqDays];

	// and then calculates the average next date for this meal to be planned
	meal.recommendedDate = [calendar BC_dateByAddingDays:freqDays toDate:maxDate];
}

- (void)reloadDataCache
{
	NSFetchRequest *mealsRequest = [RecipeMO MR_createFetchRequestInContext:self.managedObjectContext];
	mealsRequest.predicate = [NSPredicate predicateWithFormat:@"(%K = %@) AND (%K <> %@) AND (deleted = NO)", 
		RecipeMOAttributes.accountId, [[User currentUser] accountId], RecipeMOAttributes.status, kStatusDelete];

	NSSortDescriptor *sortByAdvisorId = [NSSortDescriptor sortDescriptorWithKey:@"advisorId" ascending:NO];
	NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
	mealsRequest.sortDescriptors = @[sortByAdvisorId, sortByName];

	self.localRecipes = [self.managedObjectContext executeFetchRequest:mealsRequest error:nil];

	self.emptyImageView.hidden = [self.localRecipes count] > 0;

	[self reloadData];

	[self loadImagesForOnscreenRows];
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visibleRows = [self.tableView indexPathsForVisibleRows];
	for (NSIndexPath *indexPath in visibleRows) {
		if (indexPath.section == kSectionLocalRecipes) {
			RecipeMO *recipe = [self.localRecipes objectAtIndex:indexPath.row];

			UIImage *recipeImage = nil;
			if (recipe.imageId && recipe.imageIdValue > 0) {
				[self.tableView downloadImageWithImageId:recipe.imageId forIndexPath:indexPath withSize:kImageSizeMedium];
			}
			else if (recipe.imageUrl) {
				recipeImage = [[BCImageCache sharedCache] imageWithImageURL:recipe.imageUrl size:@"medium"];
				if (!recipeImage) {
					[self.tableView downloadImageWithImageURL:recipe.imageUrl forIndexPath:indexPath completionBlock:
						^(NSError *error, UIImage *image) {
							if (image) {
								image = [image BC_imageWithTargetSize:CGSizeMake(43, 43)];
								BCTableViewCell *bcCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
								bcCell.bcImageView.image = image;
							}
						}];
				}
			}
		}
		else if (indexPath.section == kSectionSearchRecipes) {
			NSDictionary *recipeDict = [self.searchRecipes objectAtIndex:[indexPath row]];

			NSString *imageURL = [recipeDict valueForKey:kWebRecipeImageURL];

			if ((NSNull *)imageURL != [NSNull null]) {
				UIImage *recipeImage = [[BCImageCache sharedCache] imageWithImageURL:imageURL size:@"medium"];
				if (!recipeImage) {
					[self.tableView downloadImageWithImageURL:imageURL forIndexPath:indexPath completionBlock:
						^(NSError *error, UIImage *image) {
							if (image) {
								image = [image BC_imageWithTargetSize:CGSizeMake(43, 43)];
								BCTableViewCell *bcCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
								bcCell.bcImageView.image = image;
							}
						}];
				}
			}
		}
		else {
			continue;
		}
	}
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rowcount = 0;
	switch (section) {
		case kSectionAddFromURL:
			rowcount = self.showAddMealFromURLRow ? 1 : 0;
			break;
		case kSectionLocalRecipes:
			rowcount = [self.localRecipes count];
			break;
		case kSectionSearchRecipes:
			rowcount = [self.searchRecipes count];
			break;
		case kSectionSearchMore:
			rowcount = self.allowRecipeSearching ? 1 : 0;
			break;
		default:
			break;
	}

	return rowcount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *localRecipeCellId = @"LocalRecipeCell";
	static NSString *searchRecipeCellId = @"SearchRecipeCell";
	static NSString *searchCellId = @"SearchCell";
	static NSString *addRecipeCellId = @"AddRecipeCell";

	UITableViewCell *cell = nil;

	// Check to see if we're in the special case of having the first section (and row)
	// being the add meal from URL capability
	switch ([indexPath section]) {
		case kSectionAddFromURL:
		{
			cell = [self.tableView dequeueReusableCellWithIdentifier:addRecipeCellId];

			NSString *addRecipeString = [NSString stringWithFormat:@"Add recipe from URL: “%@”", self.searchTextField.text];

			if ([cell.textLabel respondsToSelector:@selector(setAttributedText:)]) {
				NSMutableAttributedString *addAttrString = [[NSMutableAttributedString alloc] initWithString:addRecipeString 
					attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_mediumDarkGrayColor] }];
				[addAttrString setAttributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_darkGrayColor] }
					range:NSMakeRange(21, [self.searchTextField.text length] + 2)];
				[cell.textLabel setAttributedText:addAttrString];
			}
			else {
				cell.textLabel.text = addRecipeString;
			}
			break;
		}
		case kSectionLocalRecipes:
		{
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:localRecipeCellId];
			//bcCell.ratingControl.rating = 0;

			RecipeMO *meal = [self.localRecipes objectAtIndex:[indexPath row]];
			bcCell.bcTextLabel.text = meal.name;
			bcCell.bcImageView.image = [UIImage imageNamed:kStockImageRecipe];
			if ([meal.frequency integerValue]) {
				if (![meal.recommendedDate isEqual:[NSDate distantFuture]]) {
					// This is a recommended meal
					bcCell.bcDetailTextLabel.text = [NSString stringWithFormat:@"every %ld days, after %@",
						(long)[meal.frequency integerValue], [self.formatter stringFromDate:meal.recommendedDate]];
				}
				else {
					// This is a meal that has a frequency, but is not yet due
					bcCell.bcDetailTextLabel.text = [NSString stringWithFormat:@"every %ld days", (long)[meal.frequency integerValue]];
				}
			}
			else {
				// This meal has no frequency yet
				bcCell.bcDetailTextLabel.text = nil;
			}

			// If this is a recommended meal, use the magic wand image
			if ([self isRecommendedMeal:meal]) {
				bcCell.bcImageView.image = [UIImage imageNamed:@"lightbulb-gray"];
			}
			else if (meal.imageUrl && [meal.imageUrl length]) {
				UIImage *image = [[BCImageCache sharedCache] imageWithImageURL:meal.imageUrl];
				if (image) {
					image = [image BC_imageWithTargetSize:CGSizeMake(43, 43)];
					bcCell.bcImageView.image = image;
				}
			}
			//bcCell.ratingControl.rating = [meal.rating intValue];

			// remove the green view
			UIView *greenView = [bcCell.contentView viewWithTag:kTagAdvisorTag];
			if (greenView) {
				[greenView removeFromSuperview];
			}

			// if this is a recipe shared from an advisor, show a green bar across it
			if ([meal.advisorId integerValue] > 0) {
				UIView *contentView = bcCell.contentView;
				UIView *recipeImageView = bcCell.bcImageView;

				greenView = [[UIView alloc] init];
				greenView.translatesAutoresizingMaskIntoConstraints = NO;
				greenView.backgroundColor = [UIColor BC_mediumLightGreenColor];
				greenView.tag = kTagAdvisorTag;
				[contentView insertSubview:greenView aboveSubview:recipeImageView];

				NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(recipeImageView, greenView);

				[contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[greenView(==recipeImageView)]" options:0 metrics:nil views:viewsDictionary]];
				[contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[greenView(10)]" options:0 metrics:nil views:viewsDictionary]];

				[contentView addConstraint:[NSLayoutConstraint constraintWithItem:greenView
											 attribute:NSLayoutAttributeLeft
											 relatedBy:NSLayoutRelationEqual
												toItem:recipeImageView
											 attribute:NSLayoutAttributeLeft
											multiplier:1.0
											  constant:0]];
				[contentView addConstraint:[NSLayoutConstraint constraintWithItem:greenView
											 attribute:NSLayoutAttributeBottom
											 relatedBy:NSLayoutRelationEqual
												toItem:recipeImageView
											 attribute:NSLayoutAttributeBottom
											multiplier:1.0
											  constant:0]];
			}

			cell = bcCell;
			break;
		}
		case kSectionSearchRecipes:
		{
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:searchRecipeCellId];
			bcCell.bcImageView.image = [UIImage imageNamed:kStockImageRecipe];

			NSDictionary *recipe = [self.searchRecipes objectAtIndex:indexPath.row];

			bcCell.bcTextLabel.text = [recipe valueForKey:@"name"];

			if ([[recipe objectForKey:@"calories"] isKindOfClass:[NSNull class]]) {
				bcCell.bcDetailTextLabel.text = nil;
			}
			else {
				bcCell.bcDetailTextLabel.text = [NSString stringWithFormat:@"%@ calories", [recipe valueForKey:@"calories"]];
			}

			if (recipe[kWebRecipeImageURL]) {
				UIImage *image = [[BCImageCache sharedCache] imageWithImageURL:recipe[kWebRecipeImageURL]];
				if (image) {
					image = [image BC_imageWithTargetSize:CGSizeMake(43, 43)];
					bcCell.bcImageView.image = image;
				}
			}

			cell = bcCell;
			break;
		}
		case kSectionSearchMore:
		{
			// Recipe search row
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:searchCellId];

			NSInteger offset = 0;
			NSString *searchCellText = nil;
			if ([self.searchRecipes count]) {
				searchCellText = [NSString stringWithFormat:@"Show more recipes containing “%@”", self.searchTextField.text];
				offset = 29;
			}
			else if ([self.searchTextField.text length]) {
				searchCellText = [NSString stringWithFormat:@"Search for recipes containing “%@”", self.searchTextField.text];
				offset = 30;
			}

			if ([bcCell.bcTextLabel respondsToSelector:@selector(setAttributedText:)]) {
				NSMutableAttributedString *searchAttrString = [[NSMutableAttributedString alloc] initWithString:searchCellText 
					attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_mediumDarkGrayColor] }];
				[searchAttrString setAttributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_darkGrayColor] }
					range:NSMakeRange(offset, [self.searchTextField.text length] + 2)];
				[bcCell.bcTextLabel setAttributedText:searchAttrString];
			}
			else {
				bcCell.bcTextLabel.text = searchCellText;
			}

			cell = bcCell;
			break;
		}
		default:
			break;
	}
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Mark the managed object as deleted for this row
        RecipeMO *item;
	
		item = [self.localRecipes objectAtIndex:[indexPath row]];
		item.removed = [NSNumber numberWithBool:YES];
		// Clear the externalId so that we don't accidentally find this meal later if
		// the user tries to re-add this recipe as a meal
		item.externalId = nil;
		item.status = kStatusDelete;
        
        // Save the context.
        NSError *error = nil;
        if (![item.managedObjectContext save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
        }

		[self reloadDataCache];

		self.needsSync = YES;
    }   
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat headerHeight = kSectionHeaderHeight;
	if (section != kSectionSearchRecipes || ![self.searchRecipes count]) {
		headerHeight = 0.0f;
	}
	return headerHeight;
}

- (UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section
{
	return [BCTableViewController customViewForHeaderWithTitle:@"Search Results"];
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	//QuietLog(@"Entering %s", __FUNCTION__);

	// Hide the keyboard (it may not be in view, but is easiest to just try, in case)
	[self.searchTextField resignFirstResponder];

	switch ([indexPath section]) {
		case kSectionAddFromURL:
			// User clicked the add meal from URL row
			[self addMealFromURL:self.searchTextField.text];
			self.searchTextField.text = @"";
			break;
		case kSectionSearchMore:
			[self sendSearchRequest];
			break;
		case kSectionLocalRecipes:
		case kSectionSearchRecipes:
		default:
			break;
	}
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];

	return YES;
}

- (IBAction)searchTextDidChange:(id)sender
{
	NSString *searchTerm = [self.searchTextField text];
	self.allowRecipeSearching = NO;

	// Clear out the prior search
	if ([self.searchRecipes count]) {
		self.searchRecipes = nil;
		self.recipePages = 0;
		[self reloadData];
	}

	self.showAddMealFromURLRow = [[searchTerm lowercaseString] hasPrefix:@"http"];

	// Don't allow filtering if we don't have any characters in the search text,
	// or if the text is a url
	if (([searchTerm length] > 0) && !self.showAddMealFromURLRow) {
		NSSortDescriptor *sortByAdvisorId = [NSSortDescriptor sortDescriptorWithKey:@"advisorId" ascending:NO];
		NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
		self.localRecipes = [[RecipeMO findAllByNameOrTag:searchTerm inContext:self.managedObjectContext] sortedArrayUsingDescriptors:@[sortByAdvisorId, sortByName]];

		[self reloadData];

		// If there are at least 3 characters in the search term, allow searching for recipes
		// NOTE the recipe search (thru google or whatever) is name only at this time
		if ([searchTerm length] >= 3) {
			self.allowRecipeSearching = YES;
		}
	}
	else {
		[self reloadDataCache];
	}

	[self reloadData];
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}

	//[super scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - Notifications
- (void)syncCheckDidFinish:(NSNotification *)notification
{
	[self reloadDataCache];
}

#pragma mark - MealEditViewDelegate
- (void)mealEditView:(MealEditViewController *)mealEditView didUpdateMeal:(RecipeMO *)meal
{
	[self.navigationController popViewControllerAnimated:YES];

	[self reloadData];
}

@end
