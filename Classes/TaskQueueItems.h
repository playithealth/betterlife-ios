//
//  TaskQueueItems.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 8/3/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaskQueue.h"
#import "GTMHTTPFetcher.h"

@interface TaskQueueItems : NSObject 

/**
 * Adds a queue, if it isn't already there, and returns the success or failure
 * @details This method takes a queue identifier string and adds it to the singleton's list
 * of tracked queue.  If the queue is already there, it returns NO, meaning that some other entity is
 * already running that queue, and it should not be run again
 */
+ (BOOL)registerQueue:(TaskQueue *)queue withIdentifier:(NSString *)queueIdentifier;

/// Checks to see if the queue is currently being tracked
+ (BOOL)isQueueRegistered:(NSString *)queueIdentifier;

/// Removes the queue from tracking
+ (void)unregisterQueue:(NSString *)queueIdentifier;

/// Cancels all queues
+ (void)cancelAllQueues;

/**
 * Adds a task, if it isn't already there, and returns the success or failure
 * @details This method takes a task identifier string and adds it to the singleton's list
 * of tracked tasks.  If the task is already there, it returns NO, meaning that some other entity is
 * already running that task, and it should not be run again
 */
+ (BOOL)registerTaskWithIdentifier:(NSString *)taskIdentifier;

+ (BOOL)registerTaskWithFetcher:(GTMHTTPFetcher *)taskFetcher identifier:(NSString *)taskIdentifier;

/// Checks to see if the task is currently being tracked
+ (BOOL)isTaskRegistered:(NSString *)taskIdentifier;

/// Removes the task from tracking
+ (void)unregisterTask:(NSString *)taskIdentifier;

@end
