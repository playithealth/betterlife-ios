//
//  NSString+BCAdditions.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/5/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (BCAdditions)

- (NSString *)BC_escapeSpecialCharacters;
- (NSString *)BC_escapeURIReservedCharacters;
- (NSData *)BC_decodeBase64EncodedJPEG;
- (NSString *)BC_passwordHash;
+ (NSString *)BC_stringWithNutrientNumber:(NSNumber *)number withPrecision:(NSUInteger)precision;
+ (NSString *)BC_stringWithNutrientNumber:(NSNumber *)number;
- (NSNumber *)BC_nutrientNumber;
- (NSNumber *)BL_numberWithPrecision:(NSUInteger)precision;

@end

