//
//  OAuthUtilities.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/25/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OAuthConsumer;
@class User;

@interface OAuthUtilities : NSObject

+ (NSString *)makeMD5Hash:(NSString *)string;
+ (NSDictionary *)createOAuthParametersDictionaryWithConsumerKey:(NSString *)consumerKey 
accessToken:(NSString *)accessToken;
+ (void)signRequest:(NSMutableURLRequest*)request 
withOAuthParameters:(NSDictionary *)oauthParams 
consumerSecret:(NSString *)consumerSecret tokenSecret:(NSString *)tokenSecret;
+ (void)signRequest:(NSMutableURLRequest *)request withConsumer:(OAuthConsumer*)consumer andUser:(User *)user;
+ (NSString *)signatureBaseStringUsingParameterString:(NSString *)inParameterString forRequest:(NSURLRequest *)inRequest;
+ (NSString *)HMAC_SHA1SignatureForText:(NSString *)inText usingSecret:(NSString *)inSecret;
+ (NSString *)generateNOnce:(NSString *)prefix;

@end
