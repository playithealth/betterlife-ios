//
//  RegisterAuthCodeViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/5/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCObjectManager.h"

@interface RegisterAuthCodeViewController : UIViewController {
	UILabel *authCodeLabel;
	UILabel *showThisCodeLabel;
	UIButton *getCodeButton;
	UILabel *thisWillMarkLabel;
	UIButton *cancelButton;
	UIButton *purchaseButton;
}

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) IBOutlet UILabel *authCodeLabel;
@property (nonatomic, strong) IBOutlet UILabel *showThisCodeLabel;
@property (nonatomic, strong) IBOutlet UIButton *getCodeButton;
@property (nonatomic, strong) IBOutlet UILabel *thisWillMarkLabel;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet UIButton *purchaseButton;

- (IBAction)getSingleUseToken:(id)sender;
- (IBAction)purchaseClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;

@end
