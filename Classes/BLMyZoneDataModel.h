//
//  BLMyZoneDataModel.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 10/29/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLMyZoneDataModel : NSObject
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *weekdaysWithData;
@property (strong, nonatomic) NSArray *daysWithData;
@property (strong, nonatomic) NSNumber *weekMEPs;
@property (strong, nonatomic) NSNumber *monthMEPs;
@property (strong, nonatomic) NSNumber *yearMEPs;
@property (strong, nonatomic) NSNumber *weekCals;
@property (strong, nonatomic) NSNumber *monthCals;
@property (strong, nonatomic) NSNumber *yearCals;

- (id)initWithMOC:(NSManagedObjectContext *)moc;
- (void)reloadData;
- (void)loadDailyLogsForCalendarWithStartDate:(NSDate *)startDate;
@end
