//
//  PlanningSearchViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 2/19/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "PlanningSearchViewController.h"

#import "ActivityPlanMO.h"
#import "BCAppDelegate.h"
#import "BCDetailViewDelegate.h"
#import "BCImageCache.h"
#import "BCPermissionsDataModel.h"
#import "BCTableViewController.h"
#import "BCTableViewCell.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "ExerciseRoutineMO.h"
#import "FoodLogDetailViewController.h"
#import "GTMHTTPFetcherAdditions.h"
#import "HTTPQueue.h"
#import "RecipeMO.h"
#import "MealPlannerDay.h"
#import "MealPlannerDetailViewController.h"
#import "NSDictionary+NumberValue.h"
#import "NSString+BCAdditions.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "TrainingActivityMO.h"
#import "UIColor+Additions.h"
#import "UITableView+DownloadImage.h"
#import "User.h"

@interface PlanningSearchViewController ()  <BCDetailViewDelegate>

@property (strong, nonatomic) NSArray *localRecipes;
@property (strong, nonatomic) NSMutableArray *publicRecipes;
@property (strong, nonatomic) NSMutableArray *webRecipes;
@property (strong, nonatomic) NSArray *extraRecipes;
@property (strong, nonatomic) NSMutableArray *products;
@property (strong, nonatomic) NSMutableArray *menuItems;
@property (strong, nonatomic) NSArray *commonActivities;
@property (strong, nonatomic) NSArray *activities;
@property (strong, nonatomic) NSArray *sectionTitles;
@property (assign, nonatomic) BOOL moreLocalRecipes;
@property (assign, nonatomic) BOOL morePublicRecipes;
@property (assign, nonatomic) BOOL moreWebRecipes;
@property (assign, nonatomic) BOOL evenMoreWebRecipes;
@property (assign, nonatomic) BOOL moreProducts;
@property (assign, nonatomic) BOOL moreMenuItems;
@property (assign, nonatomic) BOOL moreCommonActivities;
@property (assign, nonatomic) BOOL moreActivities;
@property (strong, nonatomic) NSMutableDictionary *imageFetchersInProgress;
@property (strong, nonatomic) NSString *searchTerm;
@property (strong, nonatomic) NSManagedObjectContext *scratchContext;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)loadImagesForOnscreenRows;
- (void)cancelAllImageFetchers;

@end

@implementation PlanningSearchViewController

// meal planning
static const NSInteger kSectionAddCustomRecipe = 0;
static const NSInteger kSectionLocalRecipes = 1;
static const NSInteger kSectionPublicRecipes = 2;
static const NSInteger kSectionProducts = 3;
static const NSInteger kSectionMenuItems = 4;
static const NSInteger kSectionWebRecipes = 5;
static const NSInteger kNumberOfMealPlanningSections = 6;
// activity planning
static const NSInteger kSectionCommonActivities = 6;
static const NSInteger kSectionActivities = 7;
static const NSInteger kNumberOfExercisePlanningSections = 2;

static const NSInteger kInitialRowsPerSection = 3;
static const NSInteger kSearchPageSize = 10;

static const CGFloat kSectionHeaderHeight = 22;

static const NSInteger kTagAdvisorTag = 12345;

#pragma mark - view lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.imageFetchersInProgress = [NSMutableDictionary dictionary];

		self.sectionTitles = @[@"Your recipes", @"BettrLife recipes", @"Products", @"Restaurant Menu Items", @"Web search", @"Common Activities", @"Activities"];
		self.date = [NSDate date];
		self.logTime = 0; ///< Breakfast by default

		[self resetResultArraysExcluding:nil];

	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self showBackView:NO withLabelText:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
	[self loadImagesForOnscreenRows];
	[super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[self cancelAllImageFetchers];
	[self.tableView cancelAllImageTrackers];

	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    User *thisUser = [User currentUser];

	// All views segued to here utilize a scratch context, so just create it now
	NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];

	if ([segue.identifier isEqualToString:@"ShowUserRecipeDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:scratchContext];
		tempMPD.accountId = thisUser.accountId;
		tempMPD.date = [NSDate date];
		tempMPD.recipe = (RecipeMO *)[scratchContext objectWithID:[[self.localRecipes objectAtIndex:[indexPath row]] objectID]];
		tempMPD.imageId = tempMPD.recipe.imageId;
		tempMPD.itemType = kItemTypeRecipe;

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"ShowPublicRecipeDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		NSDictionary *publicRecipe = [self.publicRecipes objectAtIndex:[indexPath row]];

		RecipeMO *tempMeal = [RecipeMO insertInManagedObjectContext:scratchContext];
		tempMeal.accountId = thisUser.accountId;
		tempMeal.cloudId = [[publicRecipe objectForKey:kRecipeId] numberValueDecimal];
		[tempMeal setWithRecipeDictionary:publicRecipe];

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:scratchContext];
		tempMPD.accountId = thisUser.accountId;
		tempMPD.date = [NSDate date];
		tempMPD.recipe = tempMeal;
		tempMPD.imageId = tempMPD.recipe.imageId;
		tempMPD.itemType = kItemTypeRecipe;

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"ShowWebRecipeDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		NSDictionary *webRecipe = [self.webRecipes objectAtIndex:[indexPath row]];

		RecipeMO *tempMeal = [RecipeMO insertInManagedObjectContext:scratchContext];
		tempMeal.accountId = thisUser.accountId;
		tempMeal.externalId = [webRecipe objectForKey:kId];
		tempMeal.name = [webRecipe objectForKey:kName];
		tempMeal.calories = [[webRecipe objectForKey:kNutritionCalories] numberValueDecimal];
		if ((NSNull *)[webRecipe objectForKey:kRecipeImageURL] != [NSNull null]) {
			tempMeal.imageUrl = [webRecipe objectForKey:kRecipeImageURL];
		}

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:scratchContext];
		tempMPD.accountId = thisUser.accountId;
		tempMPD.date = [NSDate date];
		tempMPD.recipe = tempMeal;
		tempMPD.itemType = kItemTypeRecipe;

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"AddCustomRecipe"]) {
		RecipeMO *tempMeal = [RecipeMO insertInManagedObjectContext:scratchContext];
		tempMeal.accountId = thisUser.accountId;
		tempMeal.name = self.searchTextField.text;

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:scratchContext];
		tempMPD.accountId = thisUser.accountId;
		tempMPD.date = [NSDate date];
		tempMPD.recipe = tempMeal;

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"ShowProductDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		id product = [self.products objectAtIndex:indexPath.row];

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:scratchContext];
		tempMPD.accountId = [[User currentUser] accountId];
		tempMPD.date = [NSDate date];
		tempMPD.productId = [[product objectForKey:kId] numberValueDecimal];
		tempMPD.name = [product objectForKey:kName];
		tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:product inContext:scratchContext];
		tempMPD.imageId = [product BC_numberOrNilForKey:kImageId];
		tempMPD.itemType = kItemTypeProduct;

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"ShowMenuItemDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		id menuItem = [self.menuItems objectAtIndex:indexPath.row];

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:scratchContext];
		tempMPD.accountId = [[User currentUser] accountId];
		tempMPD.date = [NSDate date];
		tempMPD.menuId = [[menuItem objectForKey:kId] numberValueDecimal];
		tempMPD.name = [menuItem objectForKey:kName];
		tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:menuItem inContext:scratchContext];
		tempMPD.itemType = kItemTypeMenu;

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"ShowActivityDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		ActivityPlanMO *tempAP = [ActivityPlanMO insertInManagedObjectContext:scratchContext];
		if (indexPath.section == kSectionActivities) {
			ExerciseRoutineMO *routine = [self.activities objectAtIndex:indexPath.row];

			tempAP.loginId = [User loginId];
			tempAP.targetDate = [NSDate date];
			tempAP.activityName = routine.activity;
			#pragma message "TODO Fix this via an activity set!!!"
			//tempAP.factor = routine.factor;
		}
		else if (indexPath.section == kSectionCommonActivities) {
			TrainingActivityMO *commonActivity = [self.commonActivities objectAtIndex:indexPath.row];

			tempAP.loginId = [User loginId];
			tempAP.targetDate = [NSDate date];
			tempAP.activityName = commonActivity.exerciseRoutine.activity;
			#pragma message "TODO Fix this via an activity set!!!"
			/*
			tempAP.distance = commonActivity.distance;
			tempAP.factor = commonActivity.factor;
			tempAP.time = commonActivity.time;
			*/
		}

		#pragma message "TODO Hook in a new activity detail view here"
		/*
		BCActivityPlanViewController *detailView = segue.destinationViewController;
		detailView.plan = tempAP;
		detailView.delegate = self;
		*/
	}
	if ([segue.destinationViewController respondsToSelector:@selector(setManagedObjectContext:)]) {
		[segue.destinationViewController setManagedObjectContext:scratchContext];
	}
}

#pragma mark - local methods
- (void)reloadData
{
	// Ensure that the image trackers don't try to update indexPaths until after we refresh
	[self.tableView clearTrackerIndexPaths];
	[self.tableView reloadData];
}

- (void)sendSearchRequests
{
	// the searches below are for meal planning, if we ever add an API for exercises
	// this will need to be restructured 
	if ([BCPermissionsDataModel userHasPermission:kPermissionMealplan]) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];
	

		HTTPQueue *searchQueue = [HTTPQueue queueNamed:@"MealSearch" completionBlock:
			^(NSString *identifier, NSError *error) {
				[self reloadData];
				[self loadImagesForOnscreenRows];

				[appDelegate setNetworkActivityIndicatorVisible:NO];
			}];

		NSURL *googleSearchURL = [BCUrlFactory googleRecipeSearchForSearchTerm:self.searchTerm page:1 includeDetails:NO];
		GTMHTTPFetcher *googleSearchFetcher = [GTMHTTPFetcher signedFetcherWithURL:googleSearchURL];
		[searchQueue addRequest:googleSearchFetcher completionBlock:
			^(NSData *retrievedData, NSError *error) {
				if (error) {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:googleSearchFetcher retrievedData:retrievedData];
				}
				else {
					NSArray *googleRecipes = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

					// NOTE this API actually returns up to 10 results, so this is kind of hacky to just truncate the results here
					// but it's the best option I could come up with -- see the sendMore for a continued hack
					NSInteger resultsCount = [googleRecipes count];
					if (resultsCount > kInitialRowsPerSection) {
						self.webRecipes = [[googleRecipes subarrayWithRange:NSMakeRange(0, kInitialRowsPerSection)] mutableCopy];
						self.extraRecipes = [googleRecipes subarrayWithRange:NSMakeRange(kInitialRowsPerSection, resultsCount - kInitialRowsPerSection)];
						self.moreWebRecipes = YES;
					}
					else {
						self.moreWebRecipes = NO;
					}
				}

				[self.searchTextField resignFirstResponder];
			}];

		NSURL *bcSearchURL =  [BCUrlFactory searchUrlForSearchTerm:[self.searchTextField text] searchSections:@[@"products",@"menuitems",@"publicrecipes"] showRows:kInitialRowsPerSection showOnlyFood:YES];
		GTMHTTPFetcher *bcSearchFetcher = [GTMHTTPFetcher signedFetcherWithURL:bcSearchURL];
		[searchQueue addRequest:bcSearchFetcher completionBlock:
			^(NSData *retrievedData, NSError *error) {
				if (error) {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:bcSearchFetcher retrievedData:retrievedData];
				}
				else {
					NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

					NSDictionary *recipeDict = [resultsDict objectForKey:@"publicrecipes"];
					self.publicRecipes = [[recipeDict objectForKey:@"results"] mutableCopy];
					self.morePublicRecipes = [[recipeDict objectForKey:@"more"] boolValue];

					NSDictionary *productsDict = [resultsDict objectForKey:@"products"];
					self.products = [[productsDict objectForKey:@"results"] mutableCopy];
					self.moreProducts = [[productsDict objectForKey:@"more"] boolValue];

					NSDictionary *menuItemsDict = [resultsDict objectForKey:@"menuitems"];
					self.menuItems = [[menuItemsDict objectForKey:@"results"] mutableCopy];
					self.moreMenuItems = [[menuItemsDict objectForKey:@"more"] boolValue];
				}

				[self.searchTextField resignFirstResponder];
			}];

		[searchQueue runQueue:nil];
	}
}

- (NSArray *)fetchLocalRecipes:(BOOL)loadMore filter:(BOOL)filter
{
	User *thisUser = [User currentUser];

	if (!self.morePage && loadMore) {
		[self showBackView:YES withLabelText:@"All results for Recipes"];
		[self resetResultArraysExcluding:self.localRecipes];
		// default this to YES so that the load more row will show while it's loading
		self.moreLocalRecipes = YES;
		[self reloadData];
	}

	if (loadMore) {
		self.morePage += 1;
	}

	NSArray *recipes = nil;
	NSSortDescriptor *sortByAdvisorId = [NSSortDescriptor sortDescriptorWithKey:@"advisorId" ascending:NO];
	NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];

	// if we have a search term
	if (filter && [self.searchTerm length]) {
		recipes = [[RecipeMO findAllByNameOrTag:self.searchTerm inContext:self.managedObjectContext]
			sortedArrayUsingDescriptors:@[sortByAdvisorId, sortByName]];

		NSInteger totalCount = [recipes count];
		NSInteger displayCount = MIN(totalCount, self.morePage * kSearchPageSize + kInitialRowsPerSection);
		
		self.moreLocalRecipes = (displayCount < totalCount);
		
		recipes = [recipes subarrayWithRange:NSMakeRange(0, displayCount)];
	}
	else {
		NSFetchRequest *fetchRequest = [RecipeMO MR_requestAllWithPredicate:[NSPredicate predicateWithFormat:@"status <> %@ AND accountId = %@", 
				kStatusDelete, thisUser.accountId] inContext:self.managedObjectContext];
		fetchRequest.sortDescriptors = @[sortByAdvisorId, sortByName];

		NSInteger totalCount = [self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
		NSInteger displayCount = MIN(totalCount, self.morePage * kSearchPageSize + kInitialRowsPerSection);
		fetchRequest.fetchLimit = displayCount;
		
		self.moreLocalRecipes = (displayCount < totalCount);
		
		recipes = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
	}

	return recipes;
}

- (void)fetchLocalResults 
{
	if ([BCPermissionsDataModel userHasPermission:kPermissionMealplan]) {
		self.localRecipes = [self fetchLocalRecipes:NO filter:YES];
	}
	if ([BCPermissionsDataModel userHasPermission:kPermissionActivityplan]) {
		self.commonActivities = [self fetchCommonActivities:NO filter:YES];
		self.activities = [self fetchExerciseRoutines:NO];
	}

	[self reloadData];
	[self loadImagesForOnscreenRows];
}

- (void)fetchInitialResults
{
	if ([BCPermissionsDataModel userHasPermission:kPermissionMealplan]) {
		self.localRecipes = [self fetchLocalRecipes:NO filter:NO];
	}
	if ([BCPermissionsDataModel userHasPermission:kPermissionActivityplan]) {
		self.commonActivities = [self fetchCommonActivities:NO filter:NO];
	}

	[self reloadData];
	[self loadImagesForOnscreenRows];
}

- (NSArray *)fetchCommonActivities:(BOOL)loadMore filter:(BOOL)filter
{
	User *thisUser = [User currentUser];

	if (!self.morePage && loadMore) {
		[self showBackView:YES withLabelText:@"All results for Common Activities"];
		[self resetResultArraysExcluding:self.commonActivities];
		// default this to YES so that the load more row will show while it's loading
		self.moreCommonActivities = YES;
		[self reloadData];
	}

	NSArray *allActivities = nil;
	if (filter && [self.searchTerm length]) {
		allActivities = [TrainingActivityMO MR_findAllSortedBy:TrainingActivityMOAttributes.activity ascending:YES
			withPredicate:[NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@ AND status <> %@ AND loginId = %@", 
			TrainingActivityMOAttributes.activity, self.searchTerm, kStatusDelete, thisUser.loginId] inContext:self.managedObjectContext];
	}
	else {
		allActivities = [TrainingActivityMO MR_findAllSortedBy:TrainingActivityMOAttributes.activity ascending:YES
			withPredicate:[NSPredicate predicateWithFormat:@"status <> %@ AND loginId = %@", kStatusDelete, thisUser.loginId]
			inContext:self.managedObjectContext];
	}

	NSMutableArray* activitiesWithCount = [[NSMutableArray alloc] init];
	for (TrainingActivityMO *loggedActivity in allActivities) {
		// this code block is used below to grab the index of the matching item by activity name
		BOOL (^SameActivity)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
			NSString *activityName = [obj objectForKey:kName];
			return [activityName isEqualToString:loggedActivity.activity];
		};

		NSUInteger matchingLogIndex = [activitiesWithCount indexOfObjectPassingTest:SameActivity];
		if (matchingLogIndex == NSNotFound) {
			NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

			[dict setValue:@1 forKey:@"count"];
			[dict setValue:loggedActivity forKey:@"log"];

			[activitiesWithCount addObject:dict];
		}
		else {
			NSMutableDictionary *dict = [activitiesWithCount objectAtIndex:matchingLogIndex];
			NSNumber *count = [dict objectForKey:@"count"];
			[dict setValue:[NSNumber numberWithInteger:[count doubleValue] + 1]  forKey:@"count"];
		}
	}

	NSSortDescriptor *sortByCount = [NSSortDescriptor sortDescriptorWithKey:@"count" ascending:NO];
	NSSortDescriptor *sortByActivity = [NSSortDescriptor sortDescriptorWithKey:TrainingActivityMOAttributes.activity ascending:YES];
	NSArray *activitiesSortedByCount = [activitiesWithCount sortedArrayUsingDescriptors:@[sortByCount, sortByActivity]];

	if (loadMore) {
		self.morePage += 1;
	}

	NSInteger displayCount = MIN([activitiesSortedByCount count], self.morePage * kSearchPageSize + kInitialRowsPerSection);

	self.moreCommonActivities = (displayCount < [activitiesSortedByCount count]);

	// put the first n logs into an array and return them
	NSMutableArray *results = [[NSMutableArray alloc] init];
	for (NSInteger index = 0; index < displayCount; index++) {
		[results addObject:[[activitiesWithCount objectAtIndex:index] objectForKey:@"log"]];
	}

	return results;
}

- (NSArray *)fetchExerciseRoutines:(BOOL)loadMore
{
	if (!self.morePage && loadMore) {
		[self showBackView:YES withLabelText:@"All results for Activities"];
		[self resetResultArraysExcluding:self.activities];
		// default this to YES so that the load more row will show while it's loading
		self.moreActivities = YES;
		[self reloadData];
	}

    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [ExerciseRoutineMO MR_requestAllSortedBy:ExerciseRoutineMOAttributes.activity ascending:YES
		withPredicate:[NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@", ExerciseRoutineMOAttributes.activity, self.searchTerm]
		inContext:self.managedObjectContext];
	
	NSInteger totalCount = [self.managedObjectContext countForFetchRequest:fetchRequest error:nil];

	if (loadMore) {
		self.morePage += 1;
	}

	NSInteger displayCount = MIN(totalCount, self.morePage * kSearchPageSize + kInitialRowsPerSection);
	fetchRequest.fetchLimit = displayCount;

	self.moreActivities = (displayCount < totalCount);

	return [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
}

- (void)sendMorePublicRecipesRequest 
{ 
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	if (!self.morePage) {
		[self showBackView:YES withLabelText:@"All results for BettrLife Recipes"];
		[self resetResultArraysExcluding:self.publicRecipes];
		self.morePublicRecipes = YES;
		[self reloadData];
	}
	self.morePage += 1;
	
	NSIndexPath *moreIndexPath = [NSIndexPath indexPathForRow:[self.publicRecipes count] inSection:kSectionPublicRecipes];
	BCTableViewCell *moreCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:moreIndexPath];
	[moreCell.spinner startAnimating];
	moreCell.bcTextLabel.text = @"Loading...";

	NSURL *bcSearchURL = [BCUrlFactory publicRecipesForSearchTerm:self.searchTerm showRows:kSearchPageSize + 1 
		rowOffset:self.morePage * kSearchPageSize + kInitialRowsPerSection];
	GTMHTTPFetcher *bcSearchFetcher = [GTMHTTPFetcher signedFetcherWithURL:bcSearchURL];
    [bcSearchFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:bcSearchFetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *recipesArray = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			self.morePublicRecipes = [recipesArray count] > kSearchPageSize;
			[self insertRowsFromArray:recipesArray toArray:self.publicRecipes inSection:kSectionPublicRecipes more:self.morePublicRecipes];
		}

		[appDelegate setNetworkActivityIndicatorVisible:NO];
		[moreCell.spinner stopAnimating];
		moreCell.bcTextLabel.text = @"Load more results...";
	}];
}

- (void)sendMoreWebRecipesRequest 
{
	if (!self.morePage) {
		[self showBackView:YES withLabelText:@"All results for Web Recipes"];
		[self resetResultArraysExcluding:self.webRecipes];
		[self reloadData];
	}
	self.morePage += 1;
	
	if (self.morePage == 1 && self.extraRecipes) {
		// always assume there are more
		self.moreWebRecipes = YES;
		// start by inserting the rows left over from the first request
		[self.webRecipes addObjectsFromArray:self.extraRecipes];
		self.extraRecipes = nil;
		[self reloadData];
	}
	else {
		NSIndexPath *moreIndexPath = [NSIndexPath indexPathForRow:[self.webRecipes count] inSection:kSectionWebRecipes];
		BCTableViewCell *moreCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:moreIndexPath];
		[moreCell.spinner startAnimating];
		moreCell.bcTextLabel.text = @"Loading...";

		NSURL *googleSearchURL = [BCUrlFactory googleRecipeSearchForSearchTerm:self.searchTerm page:self.morePage + 1 includeDetails:NO];

		GTMHTTPFetcher *googleSearchFetcher = [GTMHTTPFetcher signedFetcherWithURL:googleSearchURL];
		[googleSearchFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
			if (error) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:googleSearchFetcher retrievedData:retrievedData];
			}
			else {
				NSArray *googleRecipes = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([googleRecipes count] > 0) {
					// for the google search, assume that there are more unless we received no results
					self.moreWebRecipes = YES;
					[self insertRowsFromArray:googleRecipes toArray:self.webRecipes inSection:kSectionWebRecipes more:self.moreWebRecipes];
				}
				else {
					self.moreWebRecipes = NO;
					[self.tableView beginUpdates];
					[self.tableView deleteRowsAtIndexPaths:@[moreIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
					[self.tableView endUpdates];
				}
			}

			[moreCell.spinner stopAnimating];
			moreCell.bcTextLabel.text = @"Load more results...";
		}];
	}
}

- (void)sendMoreProductsRequest
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	if (!self.morePage) {
		[self showBackView:YES withLabelText:@"All results for Products"];
		[self resetResultArraysExcluding:self.products];
		// default this to YES so that the load more row will show while it's loading
		self.moreProducts = YES;
		[self reloadData];
	}
	self.morePage += 1;
	
	NSIndexPath *moreIndexPath = [NSIndexPath indexPathForRow:[self.products count] inSection:kSectionProducts];
	BCTableViewCell *moreCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:moreIndexPath];
	[moreCell.spinner startAnimating];
	moreCell.bcTextLabel.text = @"Loading...";

	NSURL *searchURL = [BCUrlFactory productSearchURLForSearchTerm:self.searchTerm
		showRows:kSearchPageSize + 1 rowOffset:self.morePage * kSearchPageSize + kInitialRowsPerSection filters:nil showOnlyFood:YES];
    
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSDictionary *searchResultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			NSArray *productsArray = [searchResultsDict objectForKey:kProductResults];
			self.moreProducts = [productsArray count] > kSearchPageSize;
			[self insertRowsFromArray:productsArray toArray:self.products inSection:kSectionProducts more:self.moreProducts];
		}

		[self loadImagesForOnscreenRows];

		[appDelegate setNetworkActivityIndicatorVisible:NO];
		[moreCell.spinner stopAnimating];
		moreCell.bcTextLabel.text = @"Load more results...";
	}];
}

- (void)sendMoreMenuItemsRequest
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	if (!self.morePage) {
		[self showBackView:YES withLabelText:@"All results for Menu Items"];
		[self resetResultArraysExcluding:self.menuItems];
		// default this to YES so that the load more row will show while it's loading
		self.moreMenuItems = YES;
		[self reloadData];
	}
	self.morePage += 1;
	
	NSIndexPath *moreIndexPath = [NSIndexPath indexPathForRow:[self.menuItems count] inSection:kSectionMenuItems];
	BCTableViewCell *moreCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:moreIndexPath];
	[moreCell.spinner startAnimating];
	moreCell.bcTextLabel.text = @"Loading...";

	NSURL *searchURL = [BCUrlFactory menuItemsSearchURLForSearchTerm:self.searchTerm
		limit:kSearchPageSize + 1 offset:self.morePage * kSearchPageSize + kInitialRowsPerSection];
    
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *returnedMenuItems = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			self.moreMenuItems = [returnedMenuItems count] > kSearchPageSize;
			[self insertRowsFromArray:returnedMenuItems toArray:self.menuItems inSection:kSectionMenuItems more:self.moreMenuItems];
		}

		[self loadImagesForOnscreenRows];

		[appDelegate setNetworkActivityIndicatorVisible:NO];
		[moreCell.spinner stopAnimating];
		moreCell.bcTextLabel.text = @"Load more results...";
	}];
}

- (void)resetResultArraysExcluding:(NSArray *)exclude
{
	if (exclude != self.localRecipes) {
		self.localRecipes = nil;
	}
	if (exclude != self.publicRecipes) {
		self.publicRecipes = nil;
	}
	if (exclude != self.webRecipes) {
		self.webRecipes = nil;
	}
	if (exclude != self.products) {
		self.products = nil;
	}
	if (exclude != self.menuItems) {
		self.menuItems = nil;
	}
	if (exclude != self.commonActivities) {
		self.commonActivities = nil;
	}
	if (exclude != self.activities) {
		self.activities = nil;
	}

	self.morePage = 0;

	self.moreLocalRecipes = NO;
	self.morePublicRecipes = NO;
	self.moreWebRecipes = NO;
	self.moreProducts = NO;
	self.moreMenuItems = NO;
	self.moreCommonActivities = NO;
	self.moreActivities = NO;
}

- (void)cancelAllImageFetchers
{
	NSArray *allDownloads = [self.imageFetchersInProgress allValues];
	[allDownloads makeObjectsPerformSelector:@selector(stopFetching)];
	self.imageFetchersInProgress = nil;
}

// TODO Switch to the UITableView category method!
- (void)downloadImageAtURL:(NSString *)url forIndexPath:(NSIndexPath *)indexPath
{
	GTMHTTPFetcher *imageFetcher = [self.imageFetchersInProgress objectForKey:indexPath];
	if (imageFetcher == nil) {
		NSString *encodingPrefix = @"data:image/jpeg;base64";
		if ([url hasPrefix:encodingPrefix]) {
			NSData *imageData = [url BC_decodeBase64EncodedJPEG];

			UIImage *recipeImage = [UIImage imageWithData:imageData];
			[[BCImageCache sharedCache] setImage:recipeImage forImageURL:url];

			// Display the newly loaded image
			[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[indexPath row] inSection:[indexPath section]]]
				withRowAnimation:UITableViewRowAnimationNone];
		}
		else {
			GTMHTTPFetcher *imageFetcher = [self.imageFetchersInProgress objectForKey:url];
			if (!imageFetcher) {
				imageFetcher = [GTMHTTPFetcher fetcherWithURL:[NSURL URLWithString:url]];

				[self.imageFetchersInProgress setObject:imageFetcher forKey:url];

				[imageFetcher beginFetchWithCompletionHandler:
					^(NSData *retrievedData, NSError *error) {
						if (error) {
							[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:imageFetcher retrievedData:retrievedData];
						} 
						else {
							UIImage *recipeImage = [[UIImage alloc] initWithData:retrievedData];
							if (recipeImage.size.width != 40 && recipeImage.size.height != 40) {
								CGSize itemSize = CGSizeMake(40, 40);
								UIGraphicsBeginImageContext(itemSize);
								CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
								[recipeImage drawInRect:imageRect];
								recipeImage = UIGraphicsGetImageFromCurrentImageContext();
								UIGraphicsEndImageContext();
							}

							[[BCImageCache sharedCache] setImage:recipeImage forImageURL:url];

							// Display the newly loaded image
							BCTableViewCell *bcCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
							bcCell.bcImageView.image = recipeImage;
						}
						[self.imageFetchersInProgress removeObjectForKey:url];
					}];
			}
		}
	}
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
	for (NSIndexPath *indexPath in visiblePaths) {
		if (indexPath.section == kSectionLocalRecipes) {
			if (indexPath.row < [self.localRecipes count]) {
				RecipeMO *recipe = [self.localRecipes objectAtIndex:indexPath.row];
				if (recipe.imageIdValue) {
					[self.tableView downloadImageWithImageId:recipe.imageId forIndexPath:indexPath withSize:kImageSizeMedium];
				}
			}
		}
		else if (indexPath.section == kSectionPublicRecipes) {
			if (indexPath.row < [self.publicRecipes count]) {
				NSDictionary *recipeDict = [self.publicRecipes objectAtIndex:[indexPath row]];

				NSNumber *imageId = [[recipeDict objectForKey:kImageId] numberValueDecimal];

				if ([imageId integerValue]) {
					[self.tableView downloadImageWithImageId:imageId forIndexPath:indexPath withSize:kImageSizeMedium];
				}
			}
		}
		else if (indexPath.section == kSectionWebRecipes) {
			if (indexPath.row < [self.webRecipes count]) {
				NSDictionary *recipeDict = [self.webRecipes objectAtIndex:[indexPath row]];

				NSString *imageUrl = [recipeDict objectForKey:kWebRecipeImageURL];

				if ((NSNull *)imageUrl != [NSNull null] && [imageUrl length]) {
					UIImage *recipeImage = [[BCImageCache sharedCache] imageWithImageURL:imageUrl];
					if (!recipeImage) {
						[self downloadImageAtURL:imageUrl forIndexPath:indexPath];
					}
				}
				else {
					BCTableViewCell *bcCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
					bcCell.bcImageView.image = [UIImage imageNamed:kStockImageRecipe];
				}
			}
		}
		else if (indexPath.section == kSectionProducts) {
			if (indexPath.row < [self.products count]) {
				NSDictionary *product = [self.products objectAtIndex:indexPath.row];

				NSNumber *imageId = [[product objectForKey:kImageId] numberValueDecimal];

				if ([imageId integerValue]) {
					[self.tableView downloadImageWithImageId:imageId forIndexPath:indexPath withSize:kImageSizeMedium];
				}
			}
		}
	}
}

- (void)resignKeyboard:(id)sender
{
	[self.searchTextField resignFirstResponder];
}

- (IBAction)backButtonTapped:(id)sender
{
	[self showBackView:NO withLabelText:nil];

	self.searchTextField.text = self.searchTerm;
	
	[self resetResultArraysExcluding:nil];

	if ([self.searchTerm length] > 0) {
        [self sendSearchRequests];
    }
	[self fetchLocalResults];
}

- (void)showBackView:(BOOL)show withLabelText:(NSString *)labelText
{
	self.searchTextField.text = nil;

	self.backButton.hidden = !show;
	self.backLabel.hidden = !show;

	self.backLabel.text = labelText;

	self.headerImageView.hidden = show;
	self.cancelButton.hidden = show;
	self.searchTextField.hidden = show;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self resetResultArraysExcluding:nil];
	[self reloadData];
    
	if ([self.searchTextField.text length] > 0) {
		self.searchTerm = self.searchTextField.text;
		[self.searchTextField resignFirstResponder];

		[self sendSearchRequests];
		[self fetchLocalResults];
    }

    return YES;
}

- (IBAction)searchTextFieldEditingChanged:(id)sender 
{
	self.searchTerm = [sender text];
	
	[self resetResultArraysExcluding:nil];
	[self fetchLocalResults];
	[self reloadData];
}

#pragma mark - button responders
- (void)quickAddLocalRecipeButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
	id recipe = [self.localRecipes objectAtIndex:indexPath.row];

	if (self.delegate != nil
			&& [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:recipe];
	}
}

- (void)quickAddPublicRecipeButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
	NSDictionary *publicRecipe = [self.publicRecipes objectAtIndex:indexPath.row];

	if (self.delegate != nil
			&& [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:publicRecipe];
	}
}

- (void)quickAddWebRecipeButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
	id recipe = [self.webRecipes objectAtIndex:indexPath.row];
    
	if (self.delegate != nil
			&& [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:recipe];
	}
}

- (void)quickAddProductButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
	
	id product = [self.products objectAtIndex:indexPath.row];

	MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.managedObjectContext];
	tempMPD.accountId = [[User currentUser] accountId];
	tempMPD.date = [NSDate date];
	tempMPD.productId = [[product objectForKey:kId] numberValueDecimal];
	tempMPD.name = [product objectForKey:kName];
	tempMPD.imageId = [product BC_numberOrNilForKey:kImageId];
	tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:product inContext:self.managedObjectContext];

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:tempMPD];
	}
}

- (void)quickAddMenuItemButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
	
	id menuItem = [self.menuItems objectAtIndex:indexPath.row];

	MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.managedObjectContext];
	tempMPD.accountId = [[User currentUser] accountId];
	tempMPD.date = [NSDate date];
	tempMPD.menuId = [[menuItem objectForKey:kId] numberValueDecimal];
	tempMPD.name = [menuItem objectForKey:kName];
	tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:menuItem inContext:self.managedObjectContext];

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:tempMPD];
	}
}

- (void)quickAddCommonActivityButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
	
	TrainingActivityMO *activityLog = [self.commonActivities objectAtIndex:indexPath.row];

	ActivityPlanMO *tempAP = [ActivityPlanMO insertInManagedObjectContext:self.managedObjectContext];
	tempAP.loginId = [User loginId];
	tempAP.targetDate = [NSDate date];
	if (activityLog.exerciseRoutine) {
		tempAP.activityName = activityLog.exerciseRoutine.activity;
	}
	else {
		tempAP.activityName = activityLog.activity;
	}
	#pragma message "TODO Fix this via an activity set!!!"
	/*
	tempAP.distance = activityLog.distance;
	tempAP.factor = activityLog.factor;
	tempAP.time = activityLog.time;
	*/

	[self.scratchContext BL_save];

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:tempAP];
    }
}

- (void)quickAddActivityButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
	
	ExerciseRoutineMO *routine = [self.activities objectAtIndex:indexPath.row];

	ActivityPlanMO *tempAP = [ActivityPlanMO insertInManagedObjectContext:self.managedObjectContext];
	tempAP.loginId = [User loginId];
	tempAP.targetDate = [NSDate date];
	tempAP.activityName = routine.activity;
	#pragma message "TODO Fix this via an activity set!!!"
	//tempAP.factor = routine.factor;

	[self.scratchContext BL_save];

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:tempAP];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numberOfSections = kNumberOfMealPlanningSections + kNumberOfExercisePlanningSections;

    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    
	if ([BCPermissionsDataModel userHasPermission:kPermissionMealplan]) {
		if (section == kSectionAddCustomRecipe) {
			// TODO For now, turning this off, as we're not supposed to have recipe creation at this time on the mobiles,
			// someday it may allow adding a custom food
			//if ([self.searchTextField.text length] > 0) {
			//	numberOfRows = 1;
			//}
			numberOfRows = 0;
		}
		else if (section == kSectionLocalRecipes) {
			numberOfRows = [self.localRecipes count];
			if (self.moreLocalRecipes) {
				numberOfRows++;
			}
		}
		else if (section == kSectionPublicRecipes) {
			numberOfRows = [self.publicRecipes count];
			if (self.morePublicRecipes) {
				numberOfRows++;
			}
		}
		else if (section == kSectionWebRecipes) {
			numberOfRows = [self.webRecipes count];
			if (self.moreWebRecipes) {
				numberOfRows++;
			}
		}
		else if (section == kSectionProducts) {
			numberOfRows = [self.products count];
			if (self.moreProducts) {
				numberOfRows++;
			}
		}
		else if (section == kSectionMenuItems) {
			numberOfRows = [self.menuItems count];
			if (self.moreMenuItems) {
				numberOfRows++;
			}
		}
	}
	if ([BCPermissionsDataModel userHasPermission:kPermissionActivityplan]) {
		if (section == kSectionCommonActivities) {
			numberOfRows = [self.commonActivities count];
			if (self.moreCommonActivities) {
				numberOfRows++;
			}
		}
		else if (section == kSectionActivities) {
			numberOfRows = [self.activities count];
			if (self.moreActivities) {
				numberOfRows++;
			}
		}
	}
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *addCustomCellId = @"AddCustomCell";
    static NSString *userRecipeCellId = @"UserRecipeCell";
    static NSString *publicRecipeCellId = @"PublicRecipeCell";
    static NSString *webRecipeCellId = @"WebRecipeCell";
    static NSString *menuItemCellId = @"MenuItemCell";
    static NSString *productCellId = @"ProductCell";
    static NSString *activityCellId = @"ActivityCell";
    static NSString *showMoreCellId = @"ShowMoreCell";

    UITableViewCell *cell = nil;
    if (indexPath.section == kSectionAddCustomRecipe) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:addCustomCellId];
    }
	else if (indexPath.section == kSectionLocalRecipes) {
		if (indexPath.row < [self.localRecipes count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:userRecipeCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddLocalRecipeButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }
	else if (indexPath.section == kSectionPublicRecipes) {
		if (indexPath.row < [self.publicRecipes count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:publicRecipeCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddPublicRecipeButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
	}
	else if (indexPath.section == kSectionWebRecipes) {
		if (indexPath.row < [self.webRecipes count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:webRecipeCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddWebRecipeButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
	}
    else if (indexPath.section == kSectionProducts) {
		if (indexPath.row < [self.products count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:productCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddProductButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }
    else if (indexPath.section == kSectionMenuItems) {
		if (indexPath.row < [self.menuItems count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:menuItemCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddMenuItemButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }
    else if (indexPath.section == kSectionCommonActivities) {
		if (indexPath.row < [self.commonActivities count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:activityCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddCommonActivityButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }
    else if (indexPath.section == kSectionActivities) {
		if (indexPath.row < [self.activities count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:activityCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddActivityButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }

	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionAddCustomRecipe) {
        cell.textLabel.text = [NSString stringWithFormat:@"Add “%@” as a custom recipe", self.searchTextField.text];
    }
    else if (indexPath.section == kSectionLocalRecipes) {
		if (indexPath.row < [self.localRecipes count]) {
			RecipeMO *recipe = [self.localRecipes objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = recipe.name;

			bcCell.bcImageView.image = [[BCImageCache sharedCache] imageWithImageId:recipe.imageId withItemType:kItemTypeRecipe
				withSize:kImageSizeMedium];

			// remove the green view
			UIView *greenView = [bcCell.contentView viewWithTag:kTagAdvisorTag];
			if (greenView) {
				[greenView removeFromSuperview];
			}

			if ([recipe.advisorId integerValue] > 0) {
				UIView *contentView = bcCell.contentView;
				UIView *recipeImageView = bcCell.bcImageView;

				greenView = [[UIView alloc] init];
				greenView.translatesAutoresizingMaskIntoConstraints = NO;
				greenView.backgroundColor = [UIColor BC_mediumLightGreenColor];
				greenView.tag = kTagAdvisorTag;
				[contentView insertSubview:greenView aboveSubview:recipeImageView];

				NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(recipeImageView, greenView);

				[contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[greenView(==recipeImageView)]" options:0 metrics:nil views:viewsDictionary]];
				[contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[greenView(10)]" options:0 metrics:nil views:viewsDictionary]];

				[contentView addConstraint:[NSLayoutConstraint constraintWithItem:greenView
											 attribute:NSLayoutAttributeLeft
											 relatedBy:NSLayoutRelationEqual
												toItem:recipeImageView
											 attribute:NSLayoutAttributeLeft
											multiplier:1.0
											  constant:0]];
				[contentView addConstraint:[NSLayoutConstraint constraintWithItem:greenView
											 attribute:NSLayoutAttributeBottom
											 relatedBy:NSLayoutRelationEqual
												toItem:recipeImageView
											 attribute:NSLayoutAttributeBottom
											multiplier:1.0
											  constant:0]];
			}
		}
	}
	else if (indexPath.section == kSectionPublicRecipes) {
		if (indexPath.row < [self.publicRecipes count]) {
			NSDictionary *recipeDict = [self.publicRecipes objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = [recipeDict objectForKey:kName];

			NSNumber *imageId = [recipeDict BC_numberForKey:kImageId];
			bcCell.bcImageView.image = [[BCImageCache sharedCache] imageWithImageId:imageId withItemType:kItemTypeRecipe
				withSize:kImageSizeMedium];
		}
	}
	else if (indexPath.section == kSectionWebRecipes) {
		if (indexPath.row < [self.webRecipes count]) {
			NSDictionary *recipeDict = [self.webRecipes objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = [recipeDict objectForKey:kName];

			NSString *imageUrl = [recipeDict objectForKey:kWebRecipeImageURL];

			UIImage *recipeImage = nil;
			if ((NSNull *)imageUrl != [NSNull null] && [imageUrl length]) {
				recipeImage = [[BCImageCache sharedCache] imageWithImageURL:imageUrl];

				if (recipeImage.size.width != 40 && recipeImage.size.height != 40) {
					CGSize itemSize = CGSizeMake(40, 40);
					UIGraphicsBeginImageContext(itemSize);
					CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
					[recipeImage drawInRect:imageRect];
					recipeImage = UIGraphicsGetImageFromCurrentImageContext();
					UIGraphicsEndImageContext();
				}
			}
			if (!recipeImage) {
				recipeImage = [UIImage imageNamed:kStockImageRecipe];
			}
			bcCell.bcImageView.image = recipeImage;
		}
	}
    else if (indexPath.section == kSectionProducts) {
		if (indexPath.row < [self.products count]) {
			NSDictionary *product = [self.products objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = [product objectForKey:kName];
			bcCell.bcAlertImageView.hidden = [[product valueForKey:kAllergyCount] integerValue] == 0;
			bcCell.bcLeafImageView.hidden = [[product valueForKey:kLifestyleCount] integerValue] == 0;

			NSNumber *imageId = [product BC_numberForKey:kImageId];
			bcCell.bcImageView.image = [[BCImageCache sharedCache] imageWithImageId:imageId withItemType:kItemTypeProduct
				withSize:kImageSizeMedium];
		}
    }
    else if (indexPath.section == kSectionMenuItems) {
		if (indexPath.row < [self.menuItems count]) {
			NSDictionary *menuItem = [self.menuItems objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = [menuItem objectForKey:kMenuItemName];
			bcCell.bcDetailTextLabel.text = [menuItem objectForKey:kMenuRestaurantName];
			bcCell.bcImageView.image = [UIImage imageNamed:kStockImageMenuItem];
		}
    }
    else if (indexPath.section == kSectionCommonActivities) {
		if (indexPath.row < [self.commonActivities count]) {
			TrainingActivityMO* activity = [self.commonActivities objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = activity.activity;
		}
    }
    else if (indexPath.section == kSectionActivities) {
		if (indexPath.row < [self.activities count]) {
			ExerciseRoutineMO* routine = [self.activities objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = routine.activity;
		}
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 0.0f;
    switch (section) {
        case kSectionLocalRecipes:
            if ([self.localRecipes count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
        case kSectionPublicRecipes:
            if ([self.publicRecipes count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
		case kSectionProducts:
            if ([self.products count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
		case kSectionMenuItems:
            if ([self.menuItems count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
        case kSectionWebRecipes:
            if ([self.webRecipes count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
        case kSectionCommonActivities:
            if ([self.commonActivities count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
        case kSectionActivities:
            if ([self.activities count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
        case kSectionAddCustomRecipe:
        default:
            break;
    }
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section
{
	return [BCTableViewController customViewForHeaderWithTitle:self.sectionTitles[section - 1]];
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
		case kSectionLocalRecipes:
			if (indexPath.row == [self.localRecipes count]) {
				self.localRecipes = [self fetchLocalRecipes:YES filter:YES];
				[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionLocalRecipes] withRowAnimation:UITableViewRowAnimationAutomatic];
			}
			else {
				// View details
				NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
				MealPlannerDay *tempMPD = [MealPlannerDay insertMealPlannerDayWithRecipeMO:[self.localRecipes objectAtIndex:[indexPath row]]
					forDate:self.date logTime:self.logTime inContext:scratchContext];

				UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Logging" bundle:nil];
				FoodLogDetailViewController *detailView = [storyboard instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
				detailView.item = tempMPD;
				detailView.managedObjectContext = scratchContext;
				detailView.delegate = self;
				detailView.detailViewMode = ItemDetailViewModePlanning;

				[self.navigationController presentViewController:detailView animated:YES completion:nil];
			}
			break;
		case kSectionPublicRecipes:
			if (indexPath.row == [self.publicRecipes count]) {
				[self sendMorePublicRecipesRequest];
			}
			else {
				// View details
				NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
				NSDictionary *publicRecipe = [self.publicRecipes objectAtIndex:[indexPath row]];
				MealPlannerDay *tempMPD = [MealPlannerDay insertMealPlannerDayWithRecipeDictionary:publicRecipe forDate:self.date
					logTime:self.logTime inContext:scratchContext];

				UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Logging" bundle:nil];
				FoodLogDetailViewController *detailView = [storyboard instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
				detailView.item = tempMPD;
				detailView.managedObjectContext = scratchContext;
				detailView.delegate = self;
				detailView.detailViewMode = ItemDetailViewModePlanning;

				[self.navigationController presentViewController:detailView animated:YES completion:nil];
			}
			break;
		case kSectionWebRecipes:
			if (indexPath.row == [self.webRecipes count]) {
				[self sendMoreWebRecipesRequest];
			}
			else {
				// View details
				NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
				NSDictionary *webRecipe = [self.webRecipes objectAtIndex:[indexPath row]];
				MealPlannerDay *tempMPD = [MealPlannerDay insertMealPlannerDayWithRecipeDictionary:webRecipe forDate:self.date
					logTime:self.logTime inContext:scratchContext];
				tempMPD.recipe.externalId = [webRecipe objectForKey:kId];
				if ((NSNull *)[webRecipe objectForKey:kRecipeImageURL] != [NSNull null]) {
					tempMPD.recipe.imageUrl = [webRecipe objectForKey:kRecipeImageURL];
				}

				UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Logging" bundle:nil];
				FoodLogDetailViewController *detailView = [storyboard instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
				detailView.item = tempMPD;
				detailView.managedObjectContext = scratchContext;
				detailView.delegate = self;
				detailView.detailViewMode = ItemDetailViewModePlanning;

				[self.navigationController presentViewController:detailView animated:YES completion:nil];
			}
			break;
		case kSectionProducts:
			if (indexPath.row == [self.products count]) {
				[self sendMoreProductsRequest];
			}
			else {
				// View details
				NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
				id product = [self.products objectAtIndex:indexPath.row];

				MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:scratchContext];
				tempMPD.accountId = [[User currentUser] accountId];
				tempMPD.date = self.date;
				tempMPD.logTime = self.logTime;
				tempMPD.productId = [[product objectForKey:kId] numberValueDecimal];
				tempMPD.name = [product objectForKey:kName];
				tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:product inContext:scratchContext];
				tempMPD.imageId = [product BC_numberOrNilForKey:kImageId];
				tempMPD.itemType = kItemTypeProduct;

				UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Logging" bundle:nil];
				FoodLogDetailViewController *detailView = [storyboard instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
				detailView.item = tempMPD;
				detailView.managedObjectContext = scratchContext;
				detailView.delegate = self;
				detailView.detailViewMode = ItemDetailViewModePlanning;

				[self.navigationController presentViewController:detailView animated:YES completion:nil];
			}
			break;
		case kSectionMenuItems:
			if (indexPath.row == [self.menuItems count]) {
				[self sendMoreMenuItemsRequest];
			}
			else {
				// View details
				NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
				id menuItem = [self.menuItems objectAtIndex:indexPath.row];

				MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:scratchContext];
				tempMPD.accountId = [[User currentUser] accountId];
				tempMPD.date = self.date;
				tempMPD.logTime = self.logTime;
				tempMPD.menuId = [[menuItem objectForKey:kId] numberValueDecimal];
				tempMPD.name = [menuItem objectForKey:kName];
				tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:menuItem inContext:scratchContext];
				tempMPD.itemType = kItemTypeMenu;

				UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Logging" bundle:nil];
				FoodLogDetailViewController *detailView = [storyboard instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
				detailView.item = tempMPD;
				detailView.managedObjectContext = scratchContext;
				detailView.delegate = self;
				detailView.detailViewMode = ItemDetailViewModePlanning;

				[self.navigationController presentViewController:detailView animated:YES completion:nil];
			}
			break;
		case kSectionActivities:
			if (indexPath.row == [self.activities count]) {
				self.activities = [self fetchExerciseRoutines:YES];
				[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionActivities] withRowAnimation:UITableViewRowAnimationAutomatic];
			}
			break;
		case kSectionCommonActivities:
			if (indexPath.row == [self.commonActivities count]) {
				self.commonActivities = [self fetchCommonActivities:YES filter:YES];
				[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionCommonActivities] withRowAnimation:UITableViewRowAnimationAutomatic];
			}
			break;
        case kSectionAddCustomRecipe:
        default:
			// do nothing
			break;
	}
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	DDLogInfo(@"%@:%@", THIS_FILE, THIS_METHOD);

	if ([viewController isKindOfClass:[FoodLogDetailViewController class]]) {
		[self.navigationController dismissViewControllerAnimated:YES completion:nil];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}

	if (object) {
		[self.managedObjectContext BL_save];
	}

	// shouldn't do much here, just let the delegate know about the item
	if (self.delegate != nil
			&& [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:object];
	}
}

@end
