//
//  BLLoggingWorkoutActivityViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 2/10/15.
//  Copyright (c) 2015 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ActivityMO;
@class ActivityLogMO;

@interface BLLoggingWorkoutActivityViewController : UIViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) ActivityMO *activityMO; ///< The planned activity, meta
@property (strong, nonatomic) ActivityLogMO *activityLog; ///< The logged activity, if it has been logged
@property (strong, nonatomic) NSDate *logDate; ///< The date that the activity is performed on
@end
