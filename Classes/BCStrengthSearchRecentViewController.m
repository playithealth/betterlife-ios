//
//  BCStrengthSearchRecentViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/20/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCStrengthSearchRecentViewController.h"

#import "BCProgressHUD.h"
#import "BCStrengthSearchDataModel.h"
#import "TrainingStrengthMO.h"

@implementation BCStrengthSearchRecentViewController
- (void)viewDidLoad
{
    [super viewDidLoad];

}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataModel.trainingStrengthsGroupedByDate.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"NormalCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];

	UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTapped:)];
	[cell.contentView addGestureRecognizer:recognizer];

	NSDictionary *strengthsDict = self.dataModel.trainingStrengthsGroupedByDate[indexPath.row];
	NSArray *strengths = strengthsDict[@"strengths"];
    
    cell.textLabel.text = strengthsDict[TrainingStrengthMOAttributes.name];
	cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%lu item%@)", strengthsDict[TrainingStrengthMOAttributes.date], (unsigned long)[strengths count], ([strengths count] > 1) ? @"s": @""];

    return cell;
}

#pragma mark - responders
- (void)contentViewTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	// get the strength list at this indexPath
	NSDictionary *strengthsDict = self.dataModel.trainingStrengthsGroupedByDate[indexPath.row];

	// get the embedded array
	NSArray *strengths = strengthsDict[@"strengths"];

	for (TrainingStrengthMO *original in strengths) {
		[self.dataModel insertStrengthBasedOnTrainingStrength:original];
	}

	[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%lu items added to your log", (unsigned long)[strengths count]]
		onView:self.view.superview];
}

#pragma mark - local
- (NSString *)stringFromDate:(NSDate *)date
{
	static NSDateFormatter *_dateFormatter = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
			_dateFormatter = [[NSDateFormatter alloc] init];
			[_dateFormatter setDateFormat:@"EEEE M/d/yyyy"];
		});

	NSString *string = [_dateFormatter stringFromDate:date];
	return string;
}

@end
