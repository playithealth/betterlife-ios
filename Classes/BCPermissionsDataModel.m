//
//  BCPermissionsDataModel.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 1/2/14.
//  Copyright (c) 2014 BuyerCompass, LLC. All rights reserved.
//

#import "BCPermissionsDataModel.h"

#import "AdvisorMealPlanMO.h"
#import "BCAppDelegate.h"
#import "InboxMessage.h"
#import "ShoppingListItem.h"
#import "User.h"
#import "UserPermissionMO.h"
#import "UserProfileMO.h"

#define USE_PERMISSIONS_TABLE 1

@interface BCPermissionsDataModel ()
@end


@implementation BCPermissionsDataModel


#pragma mark - object lifecycle
- (id)init
{
	self = [super init];
	if (self) {
	}
	return self;
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - class methods
+ (BOOL)userHasPermission:(NSString *)name
{
#if USE_PERMISSIONS_TABLE
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:appDelegate.adManagedObjectContext];
	NSSet *permissions = userProfile.permissions;

	NSSet *filteredPermissions = [permissions objectsPassingTest:^(id obj, BOOL *stop) {
		UserPermissionMO *permission = (UserPermissionMO*)obj;
		if ([permission.name isEqualToString:name]) {
			*stop = YES;
			return YES;
		}
		else {
			return NO;
		}
	}];

	NSAssert(filteredPermissions.count <= 1, @"Why is there more than one of this permission?");

	UserPermissionMO *permissionToCheck = [filteredPermissions anyObject];

	return permissionToCheck.enabledValue;
#else
	if ([name isEqualToString:kPermissionShop]) {
		return NO;
	}
	else if ([name isEqualToString:kPermissionPantry]) {
		return NO;
	}
	else if ([name isEqualToString:kPermissionMealplan]) {
		return NO;
	}
	else if ([name isEqualToString:kPermissionActivityplan]) {
		return YES;
	}
	else if ([name isEqualToString:kPermissionFoodlog]) {
		return NO;
	}
	else if ([name isEqualToString:kPermissionActivitylog]) {
		return YES;
	}
	else if ([name isEqualToString:kPermissionRecipes]) {
		return NO;
	}
	else if ([name isEqualToString:kPermissionGoals]) {
		return YES;
	}
	else if ([name isEqualToString:kPermissionHealthprefs]) {
		return YES;
	}
	else if ([name isEqualToString:kPermissionTracking]) {
		return YES;
	}
	else {
		return NO;
	}
#endif

}

#pragma mark - instance methods

@end
