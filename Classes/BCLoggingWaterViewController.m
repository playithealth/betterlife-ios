//
//  BCLoggingWaterViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/24/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCLoggingWaterViewController.h"

#import "UIColor+Additions.h"
#import "User.h"

@interface BCLoggingWaterViewController ()

@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalUnitsLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *totalLabelCenterXConstraint;
@property (weak, nonatomic) IBOutlet UILabel *deltaLabel;
@property (weak, nonatomic) IBOutlet UILabel *deltaUnitsLabel;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UIImageView *tidalImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tidalTopConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *detailImageView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *addButtons;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleNavigationItem;
@property (assign, nonatomic) double originalServings;
@property (strong, nonatomic) NSArray *buttonConfigs;
@property (assign, nonatomic) NSInteger maxSliderValue;
@end

@implementation BCLoggingWaterViewController

- (void)viewDidLoad
{
	[super viewDidLoad];

	// set up the parallax for the background
	if ([self.tidalImageView respondsToSelector:@selector(addMotionEffect:)]) {
		UIInterpolatingMotionEffect *interpolationHorizontal = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
		interpolationHorizontal.minimumRelativeValue = @-10.0;
		interpolationHorizontal.maximumRelativeValue = @10.0;

		UIInterpolatingMotionEffect *interpolationVertical = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
		interpolationVertical.minimumRelativeValue = @-10.0;
		interpolationVertical.maximumRelativeValue = @10.0;

		[self.tidalImageView addMotionEffect:interpolationHorizontal];
		[self.tidalImageView addMotionEffect:interpolationVertical];
	}

	if (!self.foodLog) {
		self.foodLog = [FoodLogMO insertInManagedObjectContext:self.managedObjectContext];
		self.foodLog.loginId = [[User currentUser] loginId];
		self.foodLog.logTime = @(self.logTime);
		self.foodLog.date = self.logDate;
		self.foodLog.servings = @0;
		[self.foodLog markForSync];
	}

	self.originalServings = self.foodLog.servingsValue;

	switch (self.logTime) {
		case LogTimeWater:
			self.foodLog.name = @"Water";
			self.foodLog.servingSize = @"1 oz";
			self.foodLog.servingsUnit = @"oz";
			self.foodLog.itemType = @"water";
			self.buttonConfigs = @[
				@{ @"title" : @"+8 oz", @"value" : @8 },
				@{ @"title" : @"+16.9 oz", @"value" : @16.9 },
				@{ @"title" : @"+20 oz", @"value" : @20 },
				@{ @"title" : @"+1 L", @"value" : @33.814023 },
			];
			self.detailImageView.image = [UIImage imageNamed:@"WaterDetails"];
			self.tidalImageView.image = [UIImage imageNamed:@"WaterBackground"];
			self.titleNavigationItem.title = @"Water";
			self.maxSliderValue = 200;
			break;
		case LogTimeSugaryDrinks:
			self.foodLog.name = @"Sugary Drinks";
			self.foodLog.servingSize = @"1 oz";
			self.foodLog.servingsUnit = @"oz";
			self.foodLog.itemType = @"";
			self.buttonConfigs = @[
				@{ @"title" : @"+12 oz", @"value" : @12 },
				@{ @"title" : @"+16 oz", @"value" : @16 },
				@{ @"title" : @"+20 oz", @"value" : @20 },
				@{ @"title" : @"+1 L", @"value" : @33.814023 },
			];
			self.detailImageView.image = [UIImage imageNamed:@"SugaryDrinksEditor"];
			// Decided to not use the background image for sugary drinks, as we don't know what the threshhold should be
			self.tidalImageView.image = nil;
			//self.tidalImageView.image = [UIImage imageNamed:@"SugaryDrinksBackground"];
			self.titleNavigationItem.title = @"Sugary Drinks";
			self.maxSliderValue = 200;
			break;
		case LogTimeFruitsVeggies:
			self.foodLog.name = @"Fruits & Vegetables";
			self.foodLog.itemType = @"";
			self.buttonConfigs = @[
				@{ @"title" : @"+1", @"value" : @1 },
				@{ @"title" : @"+2", @"value" : @2 },
				@{ @"title" : @"+3", @"value" : @3 },
				@{ @"title" : @"+4", @"value" : @4 },
			];
			self.detailImageView.image = [UIImage imageNamed:@"FruitsVegetablesEditor"];
			self.tidalImageView.image = nil;
			self.titleNavigationItem.title = @"Fruits & Vegetables";
			self.maxSliderValue = 30;
			break;
		default:
			break;
	}

	for (UIButton *button in self.addButtons) {
		[button setTitle:[[self.buttonConfigs objectAtIndex:button.tag] valueForKey:@"title"] forState:UIControlStateNormal];
	}

	[self.managedObjectContext BL_save];

	[self configureView:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - responders
- (IBAction)sliderChanged:(id)sender
{
	UISlider *slider = sender;
	self.foodLog.servings = @(slider.value * self.maxSliderValue);
	[self.foodLog markForSync];
	[self.managedObjectContext BL_save];
	[self configureView:sender];
}

- (IBAction)buttonTapped:(UIButton *)sender
{
	NSNumber *buttonValue = [[self.buttonConfigs objectAtIndex:sender.tag] objectForKey:@"value"];
	self.foodLog.servings = @(self.foodLog.servingsValue + [buttonValue doubleValue]);
	[self.foodLog markForSync];
	[self.managedObjectContext BL_save];
	[self configureView:sender];
}

#pragma mark - local
- (void)configureView:(id)sender
{
	if (![sender isKindOfClass:[UISlider class]]) {
		if (self.foodLog.servingsValue == 0) {
			self.slider.value = 0;
		}
		else if (self.foodLog.servingsValue > self.maxSliderValue) {
			self.slider.value = self.maxSliderValue;
		}
		else {
			self.slider.value = self.foodLog.servingsValue / self.maxSliderValue;
		}
	}

	if (self.logTime == LogTimeFruitsVeggies) {
		self.deltaUnitsLabel.text = @"servings";
		self.totalUnitsLabel.text = @"servings";
		self.informationLabel.text = @"1 Serving = 1 cup of leafy vegetables, 1/2 cup of other vegetables, "
			"1 medium fruit or 1/2 cup chopped or canned fruit";
		self.informationLabel.hidden = NO;
	}
	else {
		self.deltaUnitsLabel.text = @"oz";
		self.totalUnitsLabel.text = @"oz";
		self.informationLabel.hidden = YES;
	}

	self.totalLabel.text = [NSString stringWithFormat:@"%0.1f", self.foodLog.servingsValue];
	self.deltaLabel.text = [NSString stringWithFormat:@"%+0.1f", self.foodLog.servingsValue - self.originalServings];

	self.deltaLabel.hidden = (self.originalServings == self.foodLog.servingsValue);
	self.deltaUnitsLabel.hidden = (self.originalServings == self.foodLog.servingsValue);
	if (self.originalServings == self.foodLog.servingsValue) {
		self.totalLabelCenterXConstraint.constant = 0;
	}
	else {
		self.totalLabelCenterXConstraint.constant = 85;
	}

	//[self.view layoutIfNeeded];

	CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
	NSInteger contentHeight = appFrame.size.height;

	[UIView animateWithDuration:0.3 animations:^{
		self.tidalTopConstraint.constant = contentHeight - (self.slider.value * contentHeight);
	}];
}

@end
