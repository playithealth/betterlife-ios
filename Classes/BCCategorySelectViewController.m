//
//  BCCategorySelectViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/7/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "BCCategorySelectViewController.h"

#import "CategoryMO.h"
#import "Store.h"

@interface BCCategorySelectViewController () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation BCCategorySelectViewController

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)table
{
    // return list of section titles to display in section index view (e.g. "ABCD...Z#")
    return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    // Return the number of rows in the section.
    NSArray *sections = [self.fetchedResultsController sections];
    NSUInteger count = 0;
    if ([sections count]) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        count = [sectionInfo numberOfObjects];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellId = @"CategoryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    // Configure the cell...
	CategoryMO *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
	cell.textLabel.text = category.name;
    
    return cell;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	CategoryMO *category = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
	if (self.delegate
			&& [self.delegate conformsToProtocol:@protocol(BCCategorySelectDelegate)]) {
		[self.delegate categorySelectView:self didSelectCategory:category];
	}
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

    // specify that we are fetching the Categories
    [fetchRequest setEntity:[CategoryMO entityInManagedObjectContext:self.managedObjectContext]];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // sort by name
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] 
		initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortByName, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // create and init the fetched results controller
    NSFetchedResultsController *aFetchedResultsController = 
		[[NSFetchedResultsController alloc] 
		initWithFetchRequest:fetchRequest 
		managedObjectContext:self.managedObjectContext 
		sectionNameKeyPath:@"nameFirstCharacter" 
		cacheName:@"CategorySelectView"];
    aFetchedResultsController.delegate = self;

    _fetchedResultsController = aFetchedResultsController;
    

	NSError *error = nil;
	if (![_fetchedResultsController performFetch:&error]) {
	    DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}    

@end

