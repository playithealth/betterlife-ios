//
//  ShoppingListItemCaloriesCell.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/6/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import "ShoppingListItemCaloriesCell.h"

@implementation ShoppingListItemCaloriesCell
@synthesize caloriesLabel;
@synthesize caloriesFromFatLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
