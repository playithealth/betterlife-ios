//
//  BCUtilities.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/6/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreLocation/CoreLocation.h"
#import "GTMHTTPFetcher.h"

#define kMetersPerMile 1609.344

#define kBMREquationRevisedHarrisBenedict 0
#define kBMREquationMifflinStJeor 1

@class OAuthConsumer;

typedef NS_ENUM(NSUInteger, BLSmiley) { SmileyHappy = 0, SmileyMeh, SmileySad };

@interface BCUtilities : NSObject

+ (void)logHttpError:(NSError *)error inFunctionNamed:(const char *)functionName
	withFetcher:(GTMHTTPFetcher *)fetcher response:(NSString *)response;
+ (void)logHttpError:(NSError *)error inFunctionNamed:(const char *)functionName 
	withFetcher:(GTMHTTPFetcher *)fetcher retrievedData:(NSData *)data;

+ (CGRect)fixRect:(CGRect)rect;
+ (CGRect)fixOriginRotationOfRect:(CGRect) rect orientation:(UIInterfaceOrientation) orientation
	parentWidth:(int)parentWidth parentHeight:(int)parentHeight;

+ (NSNumber *)calculateBmiForWeightInPounds:(NSNumber *)weight heightInInches:(NSNumber *)height;
+ (NSNumber *)calculateBmiForMassInKg:(NSNumber *)mass heightInCm:(NSNumber *)height;
+ (NSNumber *)calculateBmrForGender:(NSInteger)gender ageInYears:(NSInteger)years weightInPounds:(float)pounds heightInInches:(float)inches usingEquation:(NSInteger)equation;
+ (NSNumber *)calculateBmrForGender:(NSInteger)gender ageInYears:(NSInteger)age massInKilograms:(float)kg heightInCentimeters:(float)cm usingEquation:(NSInteger)equation;

@end
