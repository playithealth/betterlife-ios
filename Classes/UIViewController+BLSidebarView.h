//
//  UIViewController+BLSidebarView.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 9/9/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BLSidebarView)
- (void)BL_implementSidebar;
@end
