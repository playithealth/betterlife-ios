//
//  NSDate+Additions.m
//  BettrLife
//
//  Created by Sef Tarbell on 8/23/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "NSDate+Additions.h"

const struct BCDateWeekDays BCDateWeekDays =
{
	.sunday = 1,
	.monday = 2,
	.tuesday = 3,
	.wednesday = 4,
	.thursday = 5,
	.friday = 6,
	.saturday = 7,
};

@implementation NSDate (Additions)

- (NSString *)BC_monthYearString
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"yMMMM"
								options:0
								locale:[NSLocale currentLocale]];
	return [dateFormatter stringFromDate:self];
}

- (NSString *)BC_monthString
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MMMM"];
	return [dateFormatter stringFromDate:self];
}

- (NSString *)BC_dayString
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"d"];
	return [dateFormatter stringFromDate:self];
}

- (NSString *)BC_yearString
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy"];
	return [dateFormatter stringFromDate:self];
}

- (BOOL)BC_isInSameMonthAs:(NSDate *)monthDate
{
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components1 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:self];
	NSDateComponents *components2 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:monthDate];
	return([components1 year] == [components2 year] && [components1 month] == [components2 month]);
}

- (BOOL)BC_isInSameWeekAs:(NSDate *)weekDate
{
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components1 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit) fromDate:self];
	NSDateComponents *components2 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit) fromDate:weekDate];
	return([components1 year] == [components2 year] && [components1 month] == [components2 month] && [components1 week] == [components2 week]);
}

- (BOOL)BC_isSameDayAs:(NSDate *)anotherDate
{
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components1 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
	NSDateComponents *components2 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:anotherDate];
	return([components1 year] == [components2 year] && [components1 month] == [components2 month] && [components1 day] == [components2 day]);
}

@end
