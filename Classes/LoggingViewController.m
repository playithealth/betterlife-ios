//
// LoggingViewController.m
// BettrLife Corporation
//
// Created by Sef Tarbell on 11/4/11.
// Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "LoggingViewController.h"
#import <objc/runtime.h>

#import <QuartzCore/QuartzCore.h>

#import "ActivityLogMO.h"
#import "ActivitySetMO.h"
#import "AdvisorActivityPlanMO.h"
#import "AdvisorActivityPlanWorkoutMO.h"
#import "AdvisorActivityPlanWorkoutActivityMO.h"
#import "AdvisorMealPlanEntryMO.h"
#import "AdvisorMealPlanMO.h"
#import "AdvisorMealPlanTagMO.h"
#import "BCActivityLogDetailViewController.h"
#import "BCAppDelegate.h"
#import "BCCardioLogDetailViewController.h"
#import "BCFitbitStepsDetailViewController.h"
#import "BCImageCache.h"
#import "BCLoggingJustCaloriesViewController.h"
#import "BCLoggingNoteViewController.h"
#import "BCLoggingSearchMainViewController.h"
#import "BCLoggingWaterViewController.h"
#import "BCObjectManager.h"
#import "BCPermissionsDataModel.h"
#import "BCPopupView.h"
#import "BCProgressHUD.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BLFoodItem.h"
#import "BLHeaderFooterMultiline.h"
#import "BLLoggingDataModel.h"
#import "BLLoggingWorkoutActivityViewController.h"
#import "BLNutritionStatusViewController.h"
#import "CustomBadge.h"
#import "CustomFoodMO.h"
#import "ExerciseRoutineMO.h"
#import "FoodLogDetailViewController.h"
#import "FoodLogMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "LoggingNoteMO.h"
#import "MealPlannerDay.h"
#import "MealPredictionMO.h"
#import "NSArray+NestedArrays.h"
#import "NSCalendar+Additions.h"
#import "NSDate+Additions.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "PantryItem.h"
#import "PlanningTagSearchViewController.h"
#import "PurchaseHxItem.h"
#import "RecipeMO.h"
#import "ShoppingListItem.h"
#import "TrackingMO.h"
#import "TrainingActivityMO.h"
#import "UIBarButtonItemAdditions.h"
#import "UIColor+Additions.h"
#import "UIImage+Additions.h"
#import "UITableView+DownloadImage.h"
#import "UserProfileMO.h"
#import "User.h"

@interface LoggingViewController ()

@property (strong, nonatomic) UISegmentedControl *headerSegmentControl;
@property (strong, nonatomic) UIButton *smileyView;

@property (weak, nonatomic) IBOutlet UIView *summaryView;
@property (weak, nonatomic) IBOutlet UILabel *summaryCaloriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryCaloriesValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryPlusLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryActivitiesLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryActivitiesValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryMinusLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryFoodLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryFoodValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryEqualLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryRemainingLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryRemainingValueLabel;
@property (weak, nonatomic) IBOutlet UIView *greenBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *greenBarWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *summaryMinusLabelTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *summaryFoodValueLabelTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *summaryEqualLabelTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *summaryActivitiesValueLabelLeadingConstraint;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) BLLoggingDataModel *dataModel;
@property (strong, nonatomic) NSMutableArray *itemsBySection;
@property (strong, nonatomic) NSArray *sectionTitles;
@property (strong, nonatomic) UIView *statsView;
@property (assign, nonatomic) NSInteger addLogMode;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSCalendar *calendar;
@property (strong, nonatomic) NSMutableDictionary *imageFetchersInProgress;
@property (strong, nonatomic) NSMutableDictionary *totalNutrition;

@property (strong, nonatomic) NSNumberFormatter *numberFormatter;
@property (strong, nonatomic) NSDate *initialDate; ///< To be used to launch this view and jump to a date other than today

//@property (strong, nonatomic) IBOutlet UIView *vTestView;

- (void)dayChanged;

@end

@implementation LoggingViewController

static char barcodeKey;

// these are the actual sections of the table

// these are the indexes of the sub-arrays in the cache
enum {
	SectionFoodLogHeader,
	SectionBreakfast,
	SectionMorningSnack,
	SectionLunch,
	SectionAfternoonSnack,
	SectionDinner,
	SectionEveningSnack,
	SectionWater,
	SectionSugaryDrinks,
	SectionFruitsVeggies,
	SectionFoodNote,
	SectionActivityHeader,
	SectionActivities,
	SectionExerciseNote,
	NumberOfSections,
};

static const NSInteger kTagCalendarPopup = 12342;

static const NSInteger kTagAlertItemNotFound = 2;

static const NSInteger kHeaderSegmentPrevious = 0;
static const NSInteger kHeaderSegmentDate = 1;
static const NSInteger kHeaderSegmentNext = 2;


#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {

		self.sectionTitles = @[@"Food Log", @"Breakfast", @"Morning Snack", @"Lunch", @"Afternoon Snack", @"Dinner", @"Evening Snack", @"Water", @"Sugary Drinks", @"Fruits & Vegetables", @"Food Notes", @"Activities", @"Activity", @"Exercise Notes"];

		self.dateFormatter = [[NSDateFormatter alloc] init];
		[self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];

		self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

		self.imageFetchersInProgress = [NSMutableDictionary dictionary];

		self.numberFormatter = [[NSNumberFormatter alloc] init];
		[self.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[self.numberFormatter setMinimumFractionDigits:0];
		[self.numberFormatter setMaximumFractionDigits:1];

	}
	return self;
}

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self.tableView registerClass:[BLHeaderFooterMultiline class] forHeaderFooterViewReuseIdentifier:@"SectionHeader"];
	self.dataModel = [[BLLoggingDataModel alloc] initWithMOC:self.managedObjectContext];

	self.headerSegmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Previous", @"Date", @"Next"]];
	[self.headerSegmentControl setImage:[UIImage imageNamed:@"previous"] forSegmentAtIndex:kHeaderSegmentPrevious];
	[self.headerSegmentControl setWidth:30 forSegmentAtIndex:kHeaderSegmentPrevious];
	[self.headerSegmentControl setImage:[UIImage imageNamed:@"next"] forSegmentAtIndex:kHeaderSegmentNext];
	[self.headerSegmentControl setWidth:30 forSegmentAtIndex:kHeaderSegmentNext];
	[self.headerSegmentControl setWidth:110 forSegmentAtIndex:kHeaderSegmentDate];
	self.headerSegmentControl.momentary = YES;
	self.headerSegmentControl.tintColor = [UIColor BC_mediumLightGreenColor];
	self.headerSegmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
	[self.headerSegmentControl addTarget:self action:@selector(headerSegmentControlValueChanged:) forControlEvents:UIControlEventValueChanged];
	self.navigationItem.titleView = self.headerSegmentControl;

	// the summary view needs to be built from scratch so the perms can control it
	[self buildSummaryView];

	// if the user can log food, no reason to allow them to scan
	if (![BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
		self.navigationItem.rightBarButtonItem = nil;
	}

	if (self.initialDate) {
		self.dataModel.dayOffset = [self.calendar BC_daysFromDate:[NSDate date] toDate:self.initialDate];
		self.initialDate = nil;
	}

	[self refresh];
	[self dayChanged];

	
	// Want to receive notifications regarding updates related to Logging
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataCache:) name:[FoodLogMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataCache:) name:[LoggingNoteMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataCache:) name:[ActivityLogMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataCache:) name:[MealPredictionMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataCache:) name:[MealPlannerDay entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataCache:) name:[AdvisorMealPlanMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataCache:) name:[UserProfileMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataCache:) name:[AdvisorActivityPlanMO entityName] object:nil];
}

- (void) updateSmiley
{
	if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
		UIImage *smileyImage;
		switch ([self.dataModel currentCompliance])
		{
			case SmileyMeh:
				smileyImage = [UIImage imageNamed:@"Smiley-Meh"];
				break;
			case SmileySad:
				smileyImage = [UIImage imageNamed:@"Smiley-Sad"];
				break;
			case SmileyHappy:
			default:
				smileyImage = [UIImage imageNamed:@"Smiley-Happy"];
		}
		
		[self.smileyView setImage:smileyImage forState:UIControlStateNormal];
		[self.smileyView setImage:smileyImage forState:UIControlStateHighlighted];
	}
}

- (void) showSmileyView
{
	if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
		UIView *smileyParentView = self.navigationController.navigationBar;
		CGSize smileySize = CGSizeMake(38, 29);
		self.smileyView = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(smileyParentView.bounds) - smileySize.width, 7, smileySize.width, smileySize.height)];
		[self updateSmiley];
		[self.smileyView addTarget:self action:@selector(smileyTapped:) forControlEvents:UIControlEventTouchUpInside];
		[smileyParentView addSubview:self.smileyView];
	}
}

- (void) hideSmileyView
{
	[self.smileyView removeFromSuperview];
	self.smileyView = nil;
}

- (void)willNavigateAway
{
	// Need this hack because viewWillDisappear doesn't quite get called early enough to hide the smiley
	[self hideSmileyView];
	
	[super willNavigateAway];
}

-(void)viewDidDisappear:(BOOL)animated
{
	[self hideSmileyView];
	
	[super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
	self.navigationController.toolbarHidden = YES;
	[self showSmileyView];

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];

	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
	// Remove the notification observers
	[[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];

	[super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - navigation
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
	// couldn't figure out an easy way to manage this segue in IB... 
	if ([identifier isEqualToString:@"ShowSearch"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		NSInteger rows = [self.itemsBySection BC_countOfNestedArrayAtIndex:indexPath.section];
		if (indexPath.row == 0 && rows > 0) {
			return NO;
		}
	}

	//return YES;
	return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"AddFood"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		BCLoggingSearchMainViewController *searchView = segue.destinationViewController;
		searchView.delegate = self;
		searchView.managedObjectContext = self.managedObjectContext;
		searchView.logDate = self.dataModel.startOfLogDate;
		searchView.logTime = @(indexPath.section - SectionBreakfast);
		searchView.headerText = self.sectionTitles[indexPath.section];
		searchView.loggingType = BCLoggingTypeFood;
	}
	else if ([segue.identifier isEqualToString:@"EditJustCalories"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		FoodLogMO *justCalories = [self logItemAtIndexPath:indexPath];

		BCLoggingJustCaloriesViewController *justCaloriesView = segue.destinationViewController;
		justCaloriesView.delegate = self;
		justCaloriesView.foodLog = justCalories;
	}
	else if ([segue.identifier isEqualToString:@"AddCardio"]) {
		BCLoggingSearchMainViewController *searchView = segue.destinationViewController;
		searchView.managedObjectContext = self.managedObjectContext;
		searchView.logDate = self.dataModel.startOfLogDate;
		searchView.loggingType = BCLoggingTypeActivity;
	}
	else if ([segue.identifier isEqualToString:@"ShowFoodLogDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		FoodLogDetailViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;

		NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
		detailView.managedObjectContext = scratchContext;
		detailView.item = (FoodLogMO *)[scratchContext objectWithID:[[self logItemAtIndexPath:indexPath] objectID]];
		detailView.detailViewMode = ItemDetailViewModeLogging;
	}
	else if ([segue.identifier isEqualToString:@"ShowActivityLogDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		TrainingActivityMO *itemToEdit = [self logItemAtIndexPath:indexPath];

		BCActivityLogDetailViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;
		detailView.activityLog = itemToEdit;
	}
	else if ([segue.identifier isEqualToString:@"CardioDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];

		BCCardioLogDetailViewController *detailView = segue.destinationViewController;
		detailView.managedObjectContext = scratchContext;
		detailView.activityLog = (TrainingActivityMO *)[scratchContext objectWithID:[[self logItemAtIndexPath:indexPath] objectID]];
		detailView.logDate = self.dataModel.startOfLogDate;
	}
	else if ([segue.identifier isEqualToString:@"ShowFitbitDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		BCFitbitStepsDetailViewController *detailView = segue.destinationViewController;
		detailView.activityLog = [self logItemAtIndexPath:indexPath];
	}
	else if ([segue.identifier isEqualToString:@"ShowAddWater"] || [segue.identifier isEqualToString:@"ShowMoreWater"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		BCLoggingWaterViewController *waterView = segue.destinationViewController;

		if ([self.itemsBySection BC_countOfNestedArrayAtIndex:indexPath.section]) {
			waterView.foodLog = self.itemsBySection[indexPath.section][0];
		}

		waterView.logDate = self.dataModel.startOfLogDate;
		waterView.logTime = (indexPath.section == SectionWater ? LogTimeWater
			: (indexPath.section == SectionSugaryDrinks ? LogTimeSugaryDrinks : LogTimeFruitsVeggies));
		waterView.managedObjectContext = self.managedObjectContext;
	}
	else if ([segue.identifier isEqualToString:@"EditLoggingNote"]) {
		BCLoggingNoteViewController *noteView = segue.destinationViewController;
		NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
		noteView.managedObjectContext = scratchContext;

		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		LoggingNoteMO *loggingNote = nil;
		if ([self.itemsBySection BC_countOfNestedArrayAtIndex:indexPath.section]) {
			loggingNote = self.itemsBySection[indexPath.section][0];
		}
		else {
			loggingNote = [LoggingNoteMO insertInManagedObjectContext:scratchContext];
			loggingNote.loginId = [[User currentUser] loginId];
			loggingNote.date = self.dataModel.startOfLogDate;
			if (indexPath.section == SectionFoodNote) {
				loggingNote.noteType = kLoggingNoteTypeFood;
			}
			else if (indexPath.section == SectionExerciseNote) {
				loggingNote.noteType = kLoggingNoteTypeExercise;
			}

			[self.itemsBySection[indexPath.section] addObject:loggingNote];
		}

		noteView.loggingNote = loggingNote;
	}
	else if ([segue.identifier isEqualToString:@"WorkoutDetails"]) {
		// TODO
	}
}

// return segue from the search view and water view
- (IBAction)searchViewDidFinish:(UIStoryboardSegue *)segue
{
	[BCObjectManager syncCheck:SendOnly];

	[self reloadDataCache:nil];
}

// return segues from the cardio view
- (IBAction)cardioViewDidSave:(UIStoryboardSegue *)segue
{
	DDLogInfo(@"detail save");
	BCCardioLogDetailViewController *detailView = segue.sourceViewController;

	// Ensure that if the user was in the middle of editing a text field, that that field gets saved out
	[detailView finishEditing];

	[detailView.activityLog markForSync];

	// This view uses a scratch context, so save both contexts
	[detailView.managedObjectContext BL_save];
	[self.managedObjectContext BL_save];

	[BCObjectManager syncCheck:SendOnly];

	[self reloadDataCacheForSection:SectionActivities];
	[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:SectionActivities] withRowAnimation:UITableViewRowAnimationAutomatic];

	[self refreshSummaryData];
}

- (IBAction)cardioViewDidCancel:(UIStoryboardSegue *)segue
{
	DDLogInfo(@"detail cancel");
}

// return segue from the logging note view
- (IBAction)loggingNoteViewDone:(UIStoryboardSegue *)segue
{
	DDLogInfo(@"loggingNote done");
	BCLoggingNoteViewController *noteView = segue.sourceViewController;

	LoggingNoteMO *note = noteView.loggingNote;
	if ([note.note isEqualToString:noteView.textView.text]) {
		// Nothing changed in the note, so nothing to do
		return;
	}
	note.note = noteView.textView.text;
	NSString *noteType = note.noteType;
	if (![noteView.textView.text length]) {
		// If this note was never pushed to the cloud, and has no text, then just delete the MO
		if (!note.cloudIdValue && ![note.note length]) {
			[noteView.managedObjectContext deleteObject:note];
			note = nil;
		}
		else {
			note.status = kStatusDelete;
		}
	}
	else {
		note.status = kStatusPut;
	}

	// This view uses a scratch context, so save both contexts
	[noteView.managedObjectContext BL_save];
	[self.managedObjectContext BL_save];

	[BCObjectManager syncCheck:SendOnly];

	NSIndexSet *indexSet = nil;
	if ([noteType isEqualToString:kLoggingNoteTypeFood]) {
		[self reloadDataCacheForSection:SectionFoodNote];
		indexSet = [NSIndexSet indexSetWithIndex:SectionFoodNote];
	}
	else if ([noteType isEqualToString:kLoggingNoteTypeExercise]) {
		[self reloadDataCacheForSection:SectionExerciseNote];
		indexSet = [NSIndexSet indexSetWithIndex:SectionExerciseNote];
	}
	[self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}

// unwind segue from the planning tag search view
- (IBAction)planningTagSearchViewDone:(UIStoryboardSegue *)segue
{
	PlanningTagSearchViewController *viewController = segue.sourceViewController;

	if (viewController.selection && [viewController.selection isKindOfClass:[MealPlannerDay class]]) {
		FoodLogMO *newFoodLog = [FoodLogMO insertFoodLogWithMealPlanner:viewController.selection inContext:self.managedObjectContext];
		newFoodLog.date = self.dataModel.startOfLogDate;

		[self.managedObjectContext BL_save];

		[BCObjectManager syncCheck:SendOnly];

		[self reloadDataCache:nil];
	}
}

#pragma mark - BCTableView overrides
- (void)refresh
{
	[BCObjectManager syncCheck:SendAndReceive];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return NumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	switch (section) {
		// these rows should always display, but if they have items they should show the items plus a header and footer
		case SectionFoodLogHeader:
		case SectionBreakfast:
		case SectionMorningSnack:
		case SectionLunch:
		case SectionAfternoonSnack:
		case SectionDinner:
		case SectionEveningSnack:
		{
			if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
				NSInteger rows = [self.itemsBySection BC_countOfNestedArrayAtIndex:section];
				if (rows > 0) {
					// if there are any items in this meal, then we need to show the header and the footer
					numberOfRows = rows + 2;
				}
				else {
					numberOfRows = 1;
				}
			}
			break;
		}
		case SectionFoodNote:
		{
			if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
				NSInteger rows = [self.itemsBySection BC_countOfNestedArrayAtIndex:section];
				if (rows > 0) {
					numberOfRows = rows + 1;
				}
				else {
					numberOfRows = 1;
				}
			}
			break;
		}
		case SectionActivityHeader:
		{
			if ([BCPermissionsDataModel userHasPermission:kPermissionActivitylog]) {
				numberOfRows = 1;

				// If we have a calculated BMR value for this person, then we can use the activity level, so show it
				if (self.dataModel.goalInfo.usingBMR) {
					numberOfRows = 2;
				}
			}
			break;
		}
		case SectionActivities:
		{
			if ([BCPermissionsDataModel userHasPermission:kPermissionActivitylog]) {
				NSInteger rows = [self.itemsBySection BC_countOfNestedArrayAtIndex:section];
				if (rows > 0) {
					// if there are any items in this section, then we need to show the header and the footer
					numberOfRows = rows + 2;
				}
				else {
					numberOfRows = 1;
				}
			}
			break;
		}
		case SectionExerciseNote:
		{
			if ([BCPermissionsDataModel userHasPermission:kPermissionActivitylog]) {
				NSInteger rows = [self.itemsBySection BC_countOfNestedArrayAtIndex:section];
				if (rows > 0) {
					numberOfRows = rows + 1;
				}
				else {
					numberOfRows = 1;
				}
			}
			break;
		}
		case SectionWater:
			numberOfRows = 1;
			break;
		case SectionSugaryDrinks:
			numberOfRows = (self.dataModel.advisorMealPlanMO.trackSugaryDrinks ? 1 : 0);
			break;
		case SectionFruitsVeggies:
			numberOfRows = (self.dataModel.advisorMealPlanMO.trackFruitsVeggies ? 1 : 0);
			break;
		default:
			break;
	}
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *headerCellId = @"HeaderCell";
	static NSString *foodEatenCellId = @"FoodEatenCell";
	static NSString *plannedCellId = @"PlannedCell";
	static NSString *predictionCellId = @"PredictionCell";
	static NSString *waterHeaderCellId = @"WaterHeaderCell";
	static NSString *waterCellId = @"WaterCell";
	static NSString *cardioHeaderCellId = @"CardioHeaderCell";
	static NSString *sectionHeaderCell = @"SectionHeaderCell";
	static NSString *activityLevelCreditCell = @"ActivityLevelCreditCell";
	static NSString *activityLogCellId = @"ActivityLogCell";
	static NSString *workoutCellId = @"WorkoutCell";
	static NSString *workoutActivityCellId = @"WorkoutActivityCell";
	static NSString *justCaloriesCellId = @"JustCaloriesCell";
	static NSString *normalCellId = @"NormalCell";

	UITableViewCell *cell = nil;
	
	NSInteger countOfItems = [self.itemsBySection BC_countOfNestedArrayAtIndex:indexPath.section];

	// the first cell should always be a header
	if (indexPath.row == 0) {
		switch (indexPath.section) {
			case SectionWater:
			case SectionSugaryDrinks:
			case SectionFruitsVeggies: {
				if (countOfItems) {
					cell = [self.tableView dequeueReusableCellWithIdentifier:waterCellId];
				}
				else {
					cell = [self.tableView dequeueReusableCellWithIdentifier:waterHeaderCellId];
				}
				break;
			}
			case SectionFoodLogHeader:
			case SectionActivityHeader: {
				cell = [self.tableView dequeueReusableCellWithIdentifier:sectionHeaderCell];
				break;
			}
			case SectionActivities: {
				cell = [self.tableView dequeueReusableCellWithIdentifier:cardioHeaderCellId];
				cell.selectionStyle = (countOfItems ? UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleDefault);
				break;
			}
			case SectionFoodNote:
			case SectionExerciseNote: {
				cell = [self.tableView dequeueReusableCellWithIdentifier:headerCellId];
				cell.selectionStyle = (countOfItems ? UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleDefault);
				break;
			}
			default: {
				cell = [self.tableView dequeueReusableCellWithIdentifier:headerCellId];
				cell.selectionStyle = (countOfItems ? UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleDefault);
				break;
			}
		}
	}
	// the last should always be a more cell
	else if (indexPath.row > countOfItems) {
		if (indexPath.section == SectionActivities) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:cardioHeaderCellId];
			cell.selectionStyle = UITableViewCellSelectionStyleDefault;
		}
		else if (indexPath.section == SectionActivityHeader) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:activityLevelCreditCell];
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:headerCellId];
			cell.selectionStyle = UITableViewCellSelectionStyleDefault;
		}
	}
	else {
		// need custom cells for the actual content
		switch (indexPath.section) {
			case SectionBreakfast:
			case SectionMorningSnack:
			case SectionLunch:
			case SectionAfternoonSnack:
			case SectionDinner:
			case SectionEveningSnack:
			{
				id logItem = [self logItemAtIndexPath:indexPath];
				if ([logItem isKindOfClass:[FoodLogMO class]]) {
					FoodLogMO *foodLog = logItem;
					if ([foodLog.itemType isEqualToString:kItemTypeCalories]) {
						cell = [self.tableView dequeueReusableCellWithIdentifier:justCaloriesCellId];
					}
					else {
						cell = [self.tableView dequeueReusableCellWithIdentifier:foodEatenCellId];
					}
				}
				else if ([logItem isKindOfClass:[MealPlannerDay class]]) {
					cell = [self.tableView dequeueReusableCellWithIdentifier:plannedCellId];
				}
				else if ([logItem isKindOfClass:[AdvisorMealPlanTagMO class]]) {
					cell = [self.tableView dequeueReusableCellWithIdentifier:plannedCellId];
				}
				else {
					cell = [self.tableView dequeueReusableCellWithIdentifier:predictionCellId];
				}

				break;
			}
			case SectionActivities:
			{
				id logItem = [self logItemAtIndexPath:indexPath];
				if ([logItem isKindOfClass:[ActivityLogMO class]]) {
					ActivityLogMO *activityMO = logItem;
					if (activityMO.source) {
						// TODO May need other icons, or to genericise these
						cell = [self.tableView dequeueReusableCellWithIdentifier:activityLogCellId];
						cell.selectionStyle = UITableViewCellSelectionStyleNone;
					}
					else {
						cell = [self.tableView dequeueReusableCellWithIdentifier:activityLogCellId];
						cell.selectionStyle = UITableViewCellSelectionStyleDefault;
					}
				}
				else if ([logItem isKindOfClass:[AdvisorActivityPlanWorkoutMO class]]) {
					//AdvisorActivityPlanWorkoutMO *workoutMO = logItem;
					cell = [self.tableView dequeueReusableCellWithIdentifier:workoutCellId];
				}
				else if ([logItem isKindOfClass:[AdvisorActivityPlanWorkoutActivityMO class]]) {
					cell = [self.tableView dequeueReusableCellWithIdentifier:workoutActivityCellId];
					cell.selectionStyle = UITableViewCellSelectionStyleNone;
				}
				break;
			}
			case SectionExerciseNote:
			case SectionFoodNote:
			{
				cell = [self.tableView dequeueReusableCellWithIdentifier:normalCellId];
				break;
			}
			default:
				break;
		}
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	FoodLogViewCustomCell *foodLogCell = (FoodLogViewCustomCell *)cell;
	foodLogCell.topStripeView.hidden = NO;

	NSInteger countOfItems = [self.itemsBySection BC_countOfNestedArrayAtIndex:indexPath.section];
	id firstItem = nil;
	if (countOfItems) {
		firstItem = self.itemsBySection[indexPath.section][0];
	}

	if (indexPath.section == SectionWater) {
		foodLogCell.primaryLabel.text = @"Water";
		foodLogCell.tertiaryLabel.hidden = NO;
		if (countOfItems) {
			FoodLogMO *waterLog = self.itemsBySection[SectionWater][0];
			NSString *water = [NSString stringWithFormat:@"%@ %@",
				  [self.numberFormatter stringFromNumber:waterLog.servings],
					 waterLog.servingsUnit];
			foodLogCell.tertiaryLabel.text = water;
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"Water"];
			foodLogCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
		else {
			foodLogCell.tertiaryLabel.text = @"Staying hydrated?";
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"plus-blue"];
			foodLogCell.accessoryType = UITableViewCellAccessoryNone;
		}
	}
	else if (indexPath.section == SectionSugaryDrinks) {
		foodLogCell.primaryLabel.text = @"Sugary Drinks";
		foodLogCell.tertiaryLabel.hidden = NO;
		if (countOfItems) {
			FoodLogMO *log = self.itemsBySection[SectionSugaryDrinks][0];
			NSString *sugaryDrinks = [NSString stringWithFormat:@"%@ %@",
				  [self.numberFormatter stringFromNumber:log.servings],
					 log.servingsUnit];
			foodLogCell.tertiaryLabel.text = sugaryDrinks;
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"SugaryDrinks"];
			foodLogCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
		else {
			foodLogCell.tertiaryLabel.text = @"Not too much?";
			foodLogCell.secondaryLabel.text = @"The fewer the better.";
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"plus-blue"];
			foodLogCell.accessoryType = UITableViewCellAccessoryNone;
		}
	}
	else if (indexPath.section == SectionFruitsVeggies) {
		foodLogCell.primaryLabel.text = @"Fruits & Vegetables";
		foodLogCell.tertiaryLabel.hidden = NO;
		if (countOfItems) {
			FoodLogMO *log = self.itemsBySection[SectionFruitsVeggies][0];
			NSString *fruitsVeggies = [NSString stringWithFormat:@"%@ %@",
				  [self.numberFormatter stringFromNumber:log.servings],
					 log.servingsUnit];
			foodLogCell.tertiaryLabel.text = fruitsVeggies;
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"FruitsVegetables"];
			foodLogCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
		else {
			foodLogCell.tertiaryLabel.text = @"Had any?";
			foodLogCell.secondaryLabel.text = @"The more the better.";
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"plus-blue"];
			foodLogCell.accessoryType = UITableViewCellAccessoryNone;
		}
	}
	else if (indexPath.section == SectionFoodLogHeader) {
		foodLogCell.primaryLabel.text = @"Food Log";
		foodLogCell.primaryImageView.image = [UIImage imageNamed:@"LogFoodHeader"];
		foodLogCell.backgroundColor = [UIColor BL_foodlogPurple];
	}
	else if (indexPath.section == SectionActivityHeader) {
		foodLogCell.backgroundColor = [UIColor BL_activityOrange];
		if (indexPath.row == 0) {
			foodLogCell.primaryLabel.text = @"Activities Log";
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"LogActivitiesHeader"];
		}
		else {
			UserProfileMO *userProfile = [User currentUser].userProfile;
			// Activity Level Credit cell config
			foodLogCell.primaryLabel.text = @"Activity Level Credit";
			if ([userProfile.activityLevel doubleValue] == kActivityLevelValueTrackingDevice) {
				foodLogCell.secondaryLabel.text = @"Your activity level is set to Use Fitness Tracker. "
					"Wear your fitness tracker as often as you can. Log any activities you do when not wearing it, "
					"including everyday things, to get caloric credit for all you do.";
			}
			else if ([userProfile.activityLevel doubleValue] == kActivityLevelValueSedentary) {
				foodLogCell.secondaryLabel.text = @"Your activity level is set to Manual/Basic Living. "
					"You should log all activities you do, including everyday things, to get full caloric credit.";
			}
			else if ([userProfile.activityLevel doubleValue] == kActivityLevelValueLight) {
				foodLogCell.secondaryLabel.text = @"Your activity level is set to Light Exercise. "
					"You should only log activities that go beyond light exercise.";
			}
			else if ([userProfile.activityLevel doubleValue] == kActivityLevelValueModerate) {
				foodLogCell.secondaryLabel.text = @"Your activity level is set to Moderate Exercise. "
					"You should only log activities that go beyond moderate exercise.";
			}
			else if ([userProfile.activityLevel doubleValue] == kActivityLevelValueActive) {
				foodLogCell.secondaryLabel.text = @"Your activity level is set to Active. "
					"You should only log activities that go beyond hard exercise.";
			}
			else if ([userProfile.activityLevel doubleValue] == kActivityLevelValueVeryActive) {
				foodLogCell.secondaryLabel.text = @"Your activity level is set to Very Active. "
					"You should not log any activities because you are already getting caloric credit for them automatically.";
			}

			foodLogCell.tertiaryLabel.text = [NSString stringWithFormat:@"%ld cals",
				(long)([self.dataModel.goalInfo.activityLevelCalories doubleValue] + 0.5)];
		}
	}
	else if (indexPath.row == 0) {
		foodLogCell.primaryLabel.text = self.sectionTitles[indexPath.section];
		if ((indexPath.section == SectionBreakfast) || (indexPath.section == SectionActivities)) {
			foodLogCell.topStripeView.hidden = YES;
		}

		if (countOfItems) {
			foodLogCell.tertiaryLabel.hidden = YES;
			foodLogCell.primaryImageView.image = nil;
		}
		else {
			foodLogCell.tertiaryLabel.hidden = NO;
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"plus-blue"];

			// text for the header cell
			if (indexPath.section == SectionBreakfast || indexPath.section == SectionLunch || indexPath.section == SectionDinner) {
				foodLogCell.tertiaryLabel.text = @"Have anything to eat?";
			}
			else if (indexPath.section == SectionMorningSnack || indexPath.section == SectionAfternoonSnack
					|| indexPath.section == SectionEveningSnack) {
				foodLogCell.tertiaryLabel.text = @"Had a snack?";
			}
			else if (indexPath.section == SectionActivities) {
				foodLogCell.tertiaryLabel.text = @"Keeping fit?";
			}
			else if (indexPath.section == SectionFoodNote) {
				foodLogCell.tertiaryLabel.text = @"How was your day?";
			}
			else if (indexPath.section == SectionExerciseNote) {
				foodLogCell.tertiaryLabel.text = @"How was your workout?";
			}
		}
	}
	else if (indexPath.row > countOfItems) {
		foodLogCell.primaryLabel.text = nil;
		foodLogCell.primaryImageView.image = [UIImage imageNamed:@"plus-blue"];
		foodLogCell.tertiaryLabel.hidden = NO;
		if ([firstItem isKindOfClass:[FoodLogMO class]]) {
			foodLogCell.tertiaryLabel.text = @"Anything else?";
		}
		else {
			foodLogCell.tertiaryLabel.text = @"Or something else?";
		}
		foodLogCell.topStripeView.hidden = YES;
	}
	else {
		id logItem = [self logItemAtIndexPath:indexPath];
		if ([logItem isKindOfClass:[FoodLogMO class]]) {
			FoodLogMO *foodLog = logItem;
			foodLogCell.primaryLabel.text = foodLog.name;
			double calories = [foodLog.nutrition.calories doubleValue] * [foodLog.nutritionFactor doubleValue] * [foodLog.servings doubleValue];
			foodLogCell.tertiaryLabel.text = [NSString stringWithFormat:@"%d cals", (int)(calories + 0.5)];
			foodLogCell.secondaryLabel.text = [NSString stringWithFormat:@"%@ %@", foodLog.servings, foodLog.servingsUnit];

			foodLogCell.primaryImageView.image = [[BCImageCache sharedCache] imageWithImageId:foodLog.imageId
				withItemType:foodLog.itemType withSize:kImageSizeMedium];
		}
		else if ([logItem isKindOfClass:[MealPlannerDay class]]) {
			foodLogCell.primaryLabel.text = [logItem name];
			foodLogCell.secondaryLabel.text = @"You planned this";
			foodLogCell.tertiaryLabel.text = @"Did you eat this?";
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"Logging-Planned-Item"];
		}
		else if ([logItem isKindOfClass:[AdvisorMealPlanTagMO class]]) {
			foodLogCell.primaryLabel.text = [logItem name];
			foodLogCell.secondaryLabel.text = @"A meal plan list";
			foodLogCell.tertiaryLabel.text = @"Which did you eat?";
			foodLogCell.primaryImageView.image = [UIImage imageNamed:@"Logging-Coach-Item"];
		}
		else if ([logItem isKindOfClass:[MealPredictionMO class]]) {
			foodLogCell.primaryLabel.text = [logItem name];
			foodLogCell.secondaryLabel.text = nil;

			if ([firstItem isKindOfClass:[FoodLogMO class]]) {
				if (indexPath.row == 1) {
					foodLogCell.tertiaryLabel.text = @"And this?";
				}
				else if (indexPath.row == 2) {
					foodLogCell.tertiaryLabel.text = @"This, too?";
				}
				else if (indexPath.row >= 3) {
					foodLogCell.tertiaryLabel.text = @"Also this?";
				}
			}
			else {
				if (indexPath.row == 1) {
					foodLogCell.tertiaryLabel.text = @"Did you eat this?";
				}
				else if (indexPath.row == 2) {
					foodLogCell.tertiaryLabel.text = @"Or this?";
				}
				else if (indexPath.row >= 3) {
					foodLogCell.tertiaryLabel.text = @"How about this?";
				}
			}

			// Handle the image
			if ([logItem respondsToSelector:NSSelectorFromString(@"imageId")]) {
				foodLogCell.primaryImageView.image = [[BCImageCache sharedCache] grayscaleImageWithImageId:[logItem imageId]
					withItemType:[logItem itemType]];
			}
			else {
				foodLogCell.primaryImageView.image = nil;
			}
		}
		else if ([logItem isKindOfClass:[ActivityLogMO class]]) {
			ActivityLogMO *activityItem = logItem;
			foodLogCell.primaryLabel.text = activityItem.activityName;
			double calories = [activityItem.totalCalories doubleValue];
			foodLogCell.tertiaryLabel.text = [NSString stringWithFormat:@"%d cals", (int)(calories + 0.5)];
		}
		else if ([logItem isKindOfClass:[LoggingNoteMO class]]) {
			LoggingNoteMO *note = logItem;

			cell.imageView.image = [UIImage imageNamed:@"Logging-Notes"];
			cell.textLabel.text = note.note;
		}
		else if ([logItem isKindOfClass:[AdvisorActivityPlanWorkoutMO class]]) {
			AdvisorActivityPlanWorkoutMO *workoutMO = logItem;
			foodLogCell.primaryLabel.text = workoutMO.workoutName;
		}
		else if ([logItem isKindOfClass:[AdvisorActivityPlanWorkoutActivityMO class]]) {
			AdvisorActivityPlanWorkoutActivityMO *activityMO = logItem;
			foodLogCell.primaryLabel.text = activityMO.activityName;
			if ([activityMO.isTimedActivity boolValue]) {
				// Timed activity, total up the times and display that
				NSUInteger totalDuration = 0;
				for (ActivitySetMO *set in activityMO.sets) {
					totalDuration += [set.seconds doubleValue];
				}
				// TODO Format this differently, depending on whether the duration is over an hour, minute, etc
				foodLogCell.secondaryLabel.text = [NSString stringWithFormat:@"%@ min", 
				  [self.numberFormatter stringFromNumber:@(totalDuration / 60.0)]];
			}
			else if ([activityMO.sets count] == 1) {
				// TODO Only one set, try to find some useful info to display rather than just the count of sets
				foodLogCell.secondaryLabel.text = [NSString stringWithFormat:@"%lu sets", (unsigned long)[activityMO.sets count]];
			}
			else {
				// Multiple sets, not a timed activity, just show a count of sets
				foodLogCell.secondaryLabel.text = [NSString stringWithFormat:@"%lu sets", (unsigned long)[activityMO.sets count]];
			}
			// TODO
			//foodLogCell.tertiaryLabel.text = activityMO.activityName;
		}
	}
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	id logItem = [self logItemAtIndexPath:indexPath];
	BOOL canEdit = NO;

	// Make sure they're not trying to edit the header or footer, as well as, if this is from an external source, don't allow editing
	if (([logItem isKindOfClass:[FoodLogMO class]] || [logItem isKindOfClass:[ActivityLogMO class]])
			&& ![[logItem source] integerValue]) {

		canEdit = YES;
	}
	else if ([logItem isKindOfClass:[LoggingNoteMO class]]) {
		canEdit = YES;
	}

	return canEdit;
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		// Delete the managed object for the given index path
		id logItem = [self logItemAtIndexPath:indexPath];

		NSManagedObjectContext *moc = [logItem managedObjectContext];
		if ([[logItem status] isEqualToString:kStatusPost]) {
			// actually delete the log if it is set to POST and/or has no cloud id yet
			[moc deleteObject:logItem];
		}
		else {
			[logItem setStatus:kStatusDelete];
		}

		[moc BL_save];

		[BCObjectManager syncCheck:SendOnly];

		[self reloadDataCacheForSection:indexPath.section];

		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];

		[self refreshSummaryData];

		[self loadImagesForOnscreenRows];
	}
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = UITableViewAutomaticDimension;
	if ((indexPath.section == SectionActivityHeader) && (indexPath.row == 1)) {
		height = 120.0f;
	}
	else if ((indexPath.section == SectionActivityHeader) || (indexPath.section == SectionFoodLogHeader)) {
		height = 36.0f;
	}

	return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat height = 0;

	/*
	if ((section == SectionBreakfast) && [BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
		height = 44;
	}
	else if ((section == SectionActivityHeader) && [BCPermissionsDataModel userHasPermission:kPermissionActivitylog]) {
		height = 44;
	}
	*/

	return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *headerView = nil;

	/*
	switch (section) {
		case SectionBreakfast:
			headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"SectionHeader"];
			headerView.normalLabel.text = @"Food Log";
			// TODO Get the food header image
			headerView.headerImageView.image = [UIImage imageNamed:@"ActivitiesHeader"];
			break;
		case SectionActivityHeader:
			headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"SectionHeader"];
			headerView.normalLabel.text = @"Activities Log";
			headerView.headerImageView.image = [UIImage imageNamed:@"ActivitiesHeader"];
			break;
	}
	*/

	return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	id logItem = [self logItemAtIndexPath:indexPath];
	NSInteger countOfItems = [self.itemsBySection BC_countOfNestedArrayAtIndex:indexPath.section];

	// If there are no items in this section, or if they clicked on a 'Something else', then they're trying to add
	BOOL isAdding = (!countOfItems || (indexPath.row > countOfItems));

	// If they're not adding, and there is no logItem, then nothing to do
	// This probably means that they've tapped a header cell, but there are items in the section, which means to add you must click
	// the bottom row of the section
	if (!isAdding && !logItem) {
		return;
	}

	switch (indexPath.section) {
		case SectionFoodLogHeader:
			break;
		case SectionBreakfast:
		case SectionMorningSnack:
		case SectionLunch:
		case SectionAfternoonSnack:
		case SectionDinner:
		case SectionEveningSnack:
			if (logItem) {
				if ([logItem isKindOfClass:[MealPlannerDay class]]) {
					FoodLogMO *newFoodLog = [FoodLogMO insertFoodLogWithMealPlanner:logItem inContext:self.managedObjectContext];
					if (![newFoodLog.servings integerValue]) {
						// Default to 1 serving if there are no servings set
						newFoodLog.servings = @1;
					}

					[self.managedObjectContext BL_save];

					[BCObjectManager syncCheck:SendOnly];
					[self reloadDataCacheForSection:indexPath.section];
					[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]
						withRowAnimation:UITableViewRowAnimationAutomatic];
					[self refreshSummaryData];
					[self loadImagesForOnscreenRows];
				}
				else if ([logItem isKindOfClass:[MealPredictionMO class]]) {
					MealPredictionMO *prediction = [self logItemAtIndexPath:indexPath];

					FoodLogMO *foodLog = [FoodLogMO insertInManagedObjectContext:self.managedObjectContext];
					[foodLog setWithPrediction:prediction];
					foodLog.date = self.dataModel.startOfLogDate;
					foodLog.logTime = @(indexPath.section - SectionBreakfast); // Need to offset by the first food log time section number

					[foodLog markForSync];
					[foodLog.managedObjectContext BL_save];

					[self reloadDataCacheForSection:indexPath.section];
					[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]
						withRowAnimation:UITableViewRowAnimationAutomatic];
					[self refreshSummaryData];
				}
				else if ([logItem isKindOfClass:[AdvisorMealPlanTagMO class]]) {
					UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Planning" bundle:nil];

					PlanningTagSearchViewController *viewController = [storyboard
						instantiateViewControllerWithIdentifier:@"PlanningTagSearchView"];
					viewController.loggingMode = YES;
					viewController.tag = logItem;
					viewController.logDate = self.dataModel.startOfLogDate;
					NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
					viewController.managedObjectContext = scratchContext;
					[self presentViewController:viewController animated:YES completion:nil];
				}
				else if ([logItem isKindOfClass:[FoodLogMO class]]) {
					FoodLogMO *foodLog = logItem;
					if (foodLog.itemType && [foodLog.itemType isEqualToString:@"calories"]) {
						// Just calories
						BCLoggingJustCaloriesViewController *justCaloriesView = [self.storyboard
							instantiateViewControllerWithIdentifier:@"JustCalories"];
						justCaloriesView.delegate = self;
						justCaloriesView.foodLog = foodLog;
						[self presentViewController:justCaloriesView animated:YES completion:nil];
					}
					else {
						FoodLogDetailViewController *detailView = [self.storyboard
							instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
						detailView.delegate = self;

						NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
						detailView.managedObjectContext = scratchContext;
						detailView.item = (FoodLogMO *)[scratchContext objectWithID:[logItem objectID]];
						detailView.detailViewMode = ItemDetailViewModeLogging;
						[self presentViewController:detailView animated:YES completion:nil];
					}
				}
			}
			else {
				// Search for an item to add
				BCLoggingSearchMainViewController *searchView = [self.storyboard
					instantiateViewControllerWithIdentifier:@"FoodLogSearch"];
				searchView.delegate = self;
				searchView.managedObjectContext = self.managedObjectContext;
				searchView.logDate = self.dataModel.startOfLogDate;
				searchView.logTime = @(indexPath.section - SectionBreakfast); // Need to offset by the first food log time section number
				searchView.headerText = self.sectionTitles[indexPath.section];
				searchView.loggingType = BCLoggingTypeFood;
				[self presentViewController:searchView animated:YES completion:nil];
			}
			break;
		case SectionWater:
		case SectionSugaryDrinks:
		case SectionFruitsVeggies: {
			BCLoggingWaterViewController *waterView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoggingWater"];

			if ([self.itemsBySection BC_countOfNestedArrayAtIndex:indexPath.section]) {
				waterView.foodLog = self.itemsBySection[indexPath.section][0];
			}

			waterView.logDate = self.dataModel.startOfLogDate;
			waterView.logTime = (indexPath.section == SectionWater ? LogTimeWater
				: (indexPath.section == SectionSugaryDrinks ? LogTimeSugaryDrinks : LogTimeFruitsVeggies));
			waterView.managedObjectContext = self.managedObjectContext;
			[self presentViewController:waterView animated:YES completion:nil];
			break;
		}
		case SectionActivities:
			if (logItem) {
				/* For now, I don't think we want to use a separate view for activities, so commenting it out in leiu of the new one
				if ([logItem isKindOfClass:[ActivityLogMO class]]) {
					// 'Client' activity
					// Don't allow viewing details of items with an external source
					// TODO may want to allow them to view but not edit in the future
					if (![[logItem source] integerValue]) {
						NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
						BCCardioLogDetailViewController *detailView = [self.storyboard
							instantiateViewControllerWithIdentifier:@"CardioDetail"];
						detailView.managedObjectContext = scratchContext;
						// TODO
						detailView.activityLog = (TrainingActivityMO *)[scratchContext objectWithID:[logItem objectID]];
						detailView.logDate = self.dataModel.startOfLogDate;
						[self presentViewController:detailView animated:YES completion:nil];
					}
				}
				*/
				if ([logItem isKindOfClass:[AdvisorActivityPlanWorkoutActivityMO class]]) {
					BLLoggingWorkoutActivityViewController *detailView = [self.storyboard
						instantiateViewControllerWithIdentifier:@"ActivityDetails"];
					detailView.managedObjectContext = self.managedObjectContext;
					detailView.activityMO = logItem;
					detailView.activityLog = nil;
					detailView.logDate = self.dataModel.startOfLogDate;
					[self.navigationController pushViewController:detailView animated:YES];
				}
				else if ([logItem isKindOfClass:[ActivityLogMO class]]) {
					BLLoggingWorkoutActivityViewController *detailView = [self.storyboard
						instantiateViewControllerWithIdentifier:@"ActivityDetails"];
					detailView.managedObjectContext = self.managedObjectContext;
					// Only set the activityMO if this activity came from a workout
					detailView.activityMO = ([logItem linkedWorkout] ? [logItem basedOnActivity] : nil);
					detailView.activityLog = logItem;
					detailView.logDate = self.dataModel.startOfLogDate;
					[self.navigationController pushViewController:detailView animated:YES];
				}
				else if ([logItem isKindOfClass:[AdvisorActivityPlanWorkoutMO class]]) {
					// 'Advisor' workout
				}
			}
			else {
				BCLoggingSearchMainViewController *searchView = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCardio"];
				searchView.managedObjectContext = self.managedObjectContext;
				searchView.logDate = self.dataModel.startOfLogDate;
				searchView.loggingType = BCLoggingTypeActivity;
				[self presentViewController:searchView animated:YES completion:nil];
			}
			break;
		case SectionFoodNote:
		case SectionExerciseNote: {
			BCLoggingNoteViewController *noteView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoggingNote"];
			NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
			noteView.managedObjectContext = scratchContext;

			LoggingNoteMO *loggingNote = nil;
			if ([self.itemsBySection BC_countOfNestedArrayAtIndex:indexPath.section]) {
				loggingNote = self.itemsBySection[indexPath.section][0];
			}
			else {
				loggingNote = [LoggingNoteMO insertInManagedObjectContext:scratchContext];
				loggingNote.loginId = [[User currentUser] loginId];
				loggingNote.date = self.dataModel.startOfLogDate;
				if (indexPath.section == SectionFoodNote) {
					loggingNote.noteType = kLoggingNoteTypeFood;
				}
				else if (indexPath.section == SectionExerciseNote) {
					loggingNote.noteType = kLoggingNoteTypeExercise;
				}

				[self.itemsBySection[indexPath.section] addObject:loggingNote];
			}

			noteView.loggingNote = loggingNote;
			[self presentViewController:noteView animated:YES completion:nil];
			break;
		}
		case SectionActivityHeader:
			break;
	}

	// Always turn off the selection
	[[self.tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
}

#pragma mark - Fetched results array
- (void)reloadDataCacheForSection:(NSInteger)section
{
	switch (section) {
		case SectionFoodLogHeader: {
			self.itemsBySection[section] = @[];
			break;
		}
		case SectionBreakfast:
		case SectionMorningSnack:
		case SectionLunch:
		case SectionAfternoonSnack:
		case SectionDinner:
		case SectionEveningSnack:
			self.itemsBySection[section] = [self.dataModel loadFoodLogsForLogTime:@(section - SectionBreakfast)];
			break;
		case SectionWater: {
			self.itemsBySection[section] = [self.dataModel loadWaterLogs];
			break;
		}
		case SectionSugaryDrinks: {
			self.itemsBySection[section] = [self.dataModel loadSugaryDrinkLogs];
			break;
		}
		case SectionFruitsVeggies: {
			self.itemsBySection[section] = [self.dataModel loadFruitVeggieLogs];
			break;
		}
		case SectionFoodNote: {
			self.itemsBySection[section] = [self.dataModel loadFoodNotes];
			break;
		}
		case SectionActivityHeader: {
			self.itemsBySection[section] = @[];
			break;
		}
		case SectionActivities: {
			// fetch the activities
			self.itemsBySection[section] = [self.dataModel loadActivityLogs];
			break;
		}
		case SectionExerciseNote: {
			self.itemsBySection[section] = [self.dataModel loadActivityNotes];
			break;
		}
	}
}

- (NSMutableArray *)itemsBySection
{
	if (_itemsBySection != nil) {
		return _itemsBySection;
	}

	// Initialize array
	_itemsBySection = [[NSMutableArray alloc] initWithCapacity:NumberOfSections];

	// reload ALL of the sections
	for (NSInteger sectionIndex = SectionFoodLogHeader; sectionIndex < NumberOfSections; sectionIndex++) {
		[self reloadDataCacheForSection:sectionIndex];
	}

	return _itemsBySection;
}

#pragma mark - Local Methods
- (void)setInitialDate:(NSDate *)toDate
{
	_initialDate = toDate;
}

- (void)reloadDataCache:(NSNotification *)notification
{
	self.dataModel.advisorMealPlanMO = [AdvisorMealPlanMO getMealPlanForDate:self.dataModel.startOfLogDate
		inContext:self.managedObjectContext];

	self.dataModel.advisorActivityPlanMO = [AdvisorActivityPlanMO getActivityPlanForDate:self.dataModel.startOfLogDate
		inContext:self.managedObjectContext];

	// delete the cache
	self.itemsBySection = nil;

    [self refreshView];
}

- (void)refreshView
{
	// Ensure that the image trackers don't try to update indexPaths until after we refresh
	[self.tableView clearTrackerIndexPaths];

	// this will recreate the cache
    [self.tableView reloadData];

	// rebuild the data in the summary
	[self refreshSummaryData];
    
	// fetch any images
	[self loadImagesForOnscreenRows];
}

- (IBAction)dateButtonTapped:(id)sender
{
	BCCalendarView *aCalendarView = [[BCCalendarView alloc] initWithBaseDate:[NSDate date] delegate:self];

	aCalendarView.tag = kTagCalendarPopup;
	[self.navigationController.view addSubview:aCalendarView];
}

- (IBAction)headerSegmentControlValueChanged:(id)sender
{
	if ([sender selectedSegmentIndex] == kHeaderSegmentPrevious) {
		[self previousDayButtonTapped:sender];
	}
	else if ([sender selectedSegmentIndex] == kHeaderSegmentDate) {
		[self dateButtonTapped:sender];
	}
	else if ([sender selectedSegmentIndex] == kHeaderSegmentNext) {
		[self nextDayButtonTapped:sender];
	}
}

- (IBAction)nextDayButtonTapped:(id)sender
{
	self.dataModel.dayOffset++;
	[self dayChanged];
}

- (IBAction)previousDayButtonTapped:(id)sender
{
	self.dataModel.dayOffset--;
	[self dayChanged];
}

- (void)dayChanged
{
	[self.headerSegmentControl setTitle:[self.dateFormatter stringFromDate:self.dataModel.startOfLogDate]
		forSegmentAtIndex:kHeaderSegmentDate];

	// since the cached data is filtered from the date, reload
	[self reloadDataCache:nil];

	if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:SectionFoodLogHeader]
			atScrollPosition:UITableViewScrollPositionTop animated:YES];
	}
	else if ([BCPermissionsDataModel userHasPermission:kPermissionActivitylog]) {
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:SectionActivityHeader]
			atScrollPosition:UITableViewScrollPositionTop animated:YES];
	}
}

- (void)smileyTapped:(id)sender
{
	BLNutritionStatusViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Nutrition Status"];
	viewController.managedObjectContext = self.managedObjectContext;
	viewController.dataModel = self.dataModel;

	[self presentViewController:viewController animated:YES completion:nil];
}

- (void)addLogWithDictionary:(NSDictionary *)product atLogTime:(NSNumber *)logTime
{
	FoodLogMO *foodLog = [FoodLogMO insertInManagedObjectContext:self.managedObjectContext];
	foodLog.loginId = [[User currentUser] loginId];
	foodLog.date = self.dataModel.startOfLogDate;
	foodLog.logTime = logTime;
	[foodLog setWithProductDictionary:product];

	[foodLog markForSync];

	[foodLog.managedObjectContext BL_save];

	[[self.itemsBySection objectAtIndex:[logTime integerValue] + SectionBreakfast] addObject:foodLog];

	[BCObjectManager syncCheck:SendOnly];

	// since we added the item to the cache manually, just need to refresh the table and summary
	[self refreshView];
}

- (void)addLogWithGeneric:(id)generic atLogTime:(NSNumber *)time
{
	FoodLogMO *foodLog = [FoodLogMO insertInManagedObjectContext:self.managedObjectContext];

	foodLog.status = kStatusPost;
	foodLog.loginId = [[User currentUser] loginId];
	foodLog.date = self.dataModel.startOfLogDate;
	foodLog.logTime = time;
	foodLog.servings = @1;
	foodLog.nutritionFactor = @1;
	foodLog.name = [generic valueForKey:kName];
	foodLog.productId = [generic valueForKey:@"productId"];
	foodLog.itemType = kItemTypeProduct;
	foodLog.imageId = [generic objectForKey:@"imageId"];

	NSArray *keys = [[[generic entity] attributesByName] allKeys];
	NSDictionary *dict = [generic dictionaryWithValuesForKeys:keys];
	[foodLog setNutritionWithDictionary:dict];

	if (![foodLog.managedObjectContext BL_save]) {
		return;
	}

	[[self.itemsBySection objectAtIndex:[time integerValue]] addObject:foodLog];

	[BCObjectManager syncCheck:SendOnly];

	// since we added the item to the cache manually, just need to refresh the table and summary
	[self refreshView];
}

- (IBAction)scanButtonTapped:(id)sender
{
	ZBarReaderViewController *reader = [ZBarReaderViewController new];

	reader.readerDelegate = self;
	[reader.scanner setSymbology:ZBAR_NONE config:ZBAR_CFG_ENABLE to:0];
	[reader.scanner setSymbology:ZBAR_EAN8 config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_EAN13 config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_UPCA config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_UPCE config:ZBAR_CFG_ENABLE to:1];
	[self presentViewController:reader animated:YES completion:nil];
}

- (NSNumber *)getCaloriesEaten
{
	double calories = 0;

	for (NSInteger section = SectionBreakfast; section < SectionWater; section++) {
		NSArray *logItems = [self.itemsBySection objectAtIndex:section];
		for (id logItem in logItems) {
			// jic
			if ([logItem isKindOfClass:[FoodLogMO class]]) {
				FoodLogMO *foodLog = logItem;
				calories += foodLog.nutrition.caloriesValue * foodLog.nutritionFactorValue * foodLog.servingsValue;
			}
		}
	}

	return @((int)(calories + 0.5));
}

- (NSNumber *)getCaloriesBurned
{
	double calories = 0;

	NSArray *logItems = [self.itemsBySection objectAtIndex:SectionActivities];
	for (id logItem in logItems) {
		// jic
		if ([logItem isKindOfClass:[ActivityLogMO class]]) {
			ActivityLogMO *activity = (ActivityLogMO *)logItem;
			calories += [activity.totalCalories doubleValue];
		}
	}

	// If we're using activity level, then add those in
	if (self.dataModel.goalInfo.activityLevelCalories) {
		calories += [self.dataModel.goalInfo.activityLevelCalories doubleValue];
	}

	return @((int)(calories + 0.5));
}

- (void)buildSummaryView
{
	// if the user has foodlog but not activitylog, we need to hide the activity calories from the header
	if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]
			&& ![BCPermissionsDataModel userHasPermission:kPermissionActivitylog]) {
		// hide the activity and plus labels
		self.summaryPlusLabel.hidden = YES;
		self.summaryActivitiesLabel.hidden = YES;
		self.summaryActivitiesValueLabel.hidden = YES;

		self.summaryMinusLabelTrailingConstraint.constant = 220;
		self.summaryFoodValueLabelTrailingConstraint.constant = self.summaryView.bounds.size.width / 2 - self.summaryFoodValueLabel.frame.size.width / 2 + 20;
		self.summaryEqualLabelTrailingConstraint.constant = 100;
	}
	// if the user has activitylog but not foodlog, we need to hide the food calories from the header
	else if ([BCPermissionsDataModel userHasPermission:kPermissionActivitylog]
			&& ![BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
		// hide everything BUT the activity label
		self.summaryCaloriesLabel.hidden = YES;
		self.summaryCaloriesValueLabel.hidden = YES;
		self.summaryMinusLabel.hidden = YES;
		self.summaryPlusLabel.hidden = YES;
		self.summaryFoodLabel.hidden = YES;
		self.summaryFoodValueLabel.hidden = YES;
		self.summaryEqualLabel.hidden = YES;
		self.summaryRemainingLabel.hidden = YES;
		self.summaryRemainingValueLabel.hidden = YES;
		self.greenBarView.hidden = YES;

		self.summaryActivitiesValueLabelLeadingConstraint.constant = self.summaryView.bounds.size.width / 2 - self.summaryFoodValueLabel.frame.size.width / 2;
	}

}

- (void)refreshSummaryData
{
	NSNumber *caloriesGoal;
	NSInteger caloriesBurned = 0;
	BOOL goalIsMax = YES;
	NSNumber *minCaloriesGoalNumber = [self.dataModel.goalInfo.minNutrition valueForKey:NutritionMOAttributes.calories];
	NSNumber *maxCaloriesGoalNumber = [self.dataModel.goalInfo.maxNutrition valueForKey:NutritionMOAttributes.calories];

	//
	// Establish a local caloriesGoal variable, as well as whether this is a minimum or a maximum
	//
	// If min and max calories set, check to see if they're trying to gain or lose weight to determine which to use
	if (minCaloriesGoalNumber && maxCaloriesGoalNumber) {
		caloriesGoal = (self.dataModel.goalInfo.isWeightLoss ? maxCaloriesGoalNumber : minCaloriesGoalNumber);
		goalIsMax = self.dataModel.goalInfo.isWeightLoss;
	}
	// If only max is set, use it
	else if (maxCaloriesGoalNumber) {
		caloriesGoal = maxCaloriesGoalNumber;
	}
	// If only min is set, use it
	else if (minCaloriesGoalNumber) {
		caloriesGoal = minCaloriesGoalNumber;
		goalIsMax = NO;
	}
	// Special case for calories, if we don't have a calorie goal, then we'll use the USDA 2000 calorie standard for the calorie bar
	else {
		caloriesGoal = @(2000);
	}

	self.summaryCaloriesLabel.text = (goalIsMax ? @"Calories" : @"Min Calories");

	// If there is a hard-coded calorie goal, do not show activities
	// Make sure we are using BMR (there is a hard-coded calorie goal) before showing activities
	if (self.dataModel.goalInfo.usingBMR) {
		//
		// No hard-coded calorie goal, display activities, and factor them into 'remaining' calories
		//
		self.summaryActivitiesLabel.hidden = NO;
		self.summaryActivitiesValueLabel.hidden = NO;
		self.summaryPlusLabel.hidden = NO;

		// We care about burned calories, so fetch them
		caloriesBurned = [[self getCaloriesBurned] integerValue];
		// Write the calories burned back out to the goals so that other views may access this (subviews?)
		self.dataModel.goalInfo.activityCalories = caloriesBurned;
	}
	else {
		// Hide activities in the bar
		self.summaryActivitiesLabel.hidden = YES;
		self.summaryActivitiesValueLabel.hidden = YES;
		self.summaryPlusLabel.hidden = YES;

		// We don't care about calories burned, so do nothing for that in this case, and really, populating it is bad, as below we
		// will blindly use the calories burned value to adjust remaining!
	}

	NSInteger caloriesEaten = [[self getCaloriesEaten] integerValue];
	// NOTE: Calories burned should be zero here if we are intentionally not factoring in calorie burn (activities) even if there
	// were activities logged for the day
	NSInteger caloriesRemaining = [caloriesGoal integerValue] + caloriesBurned - caloriesEaten;

	self.summaryActivitiesValueLabel.text = [NSString stringWithFormat:@"%ld", (long)caloriesBurned];
	self.summaryFoodValueLabel.text = [NSString stringWithFormat:@"%ld", (long)caloriesEaten];

	if (caloriesGoal) {
		self.summaryCaloriesValueLabel.text = [NSString stringWithFormat:@"%@", caloriesGoal];
		self.summaryRemainingValueLabel.text = [NSString stringWithFormat:@"%ld", (long)caloriesRemaining];
	}
	else {
		self.summaryCaloriesValueLabel.text = @"??";
		self.summaryRemainingValueLabel.text = @"??";
	}

	//
	// Deal with the green bar
	//
	// Default it to the 'good' color of green
	self.greenBarView.backgroundColor = [UIColor BC_lightGreenColor];
	if (caloriesEaten == 0) {
		// essentially this hides the green bar
		self.greenBarWidthConstraint.constant = 0;
	}
	else if (caloriesRemaining == 0) {
		// This is always green, as no matter whether we're losing or gaining weight (using min or max goal), we're equal
		// fill the entire area with the bar
		self.greenBarWidthConstraint.constant = self.summaryView.bounds.size.width;
	}
	else if (caloriesRemaining > 0) {
		// make the green bar reflect the ratio of what has been consumed to the "target" calories
		self.greenBarWidthConstraint.constant = (double)caloriesEaten / ([caloriesGoal integerValue] + caloriesBurned)
			* self.summaryView.bounds.size.width;
		// If we have more calories to go, that is good for weight loss, bad for weight gain
		if (!goalIsMax) {
			self.greenBarView.backgroundColor = [UIColor BC_goldColor];
		}
	}
	else {
		// fill the entire area with the bar
		self.greenBarWidthConstraint.constant = self.summaryView.bounds.size.width;
		if (goalIsMax || (maxCaloriesGoalNumber && ([maxCaloriesGoalNumber integerValue] < (caloriesEaten - caloriesBurned)))) {
			self.greenBarView.backgroundColor = [UIColor BC_goldColor];
		}
	}

// TODO
#if 0
	// if the user has both foodlog and activitylog
	if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]
			&& [BCPermissionsDataModel userHasPermission:kPermissionActivitylog]) {
	}
	// if the user has both foodlog but not activitylog
	if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]
			&& ![BCPermissionsDataModel userHasPermission:kPermissionActivitylog]) {
	}
	// if the user has both activitylog but not foodlog
	if ([BCPermissionsDataModel userHasPermission:kPermissionActivitylog]
			&& ![BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
	}

	self.summaryCaloriesValueLabel.text = @"??";
	self.summaryActivitiesValueLabel.text = [NSString stringWithFormat:@"%d", caloriesBurned];
	self.summaryFoodValueLabel.text = [NSString stringWithFormat:@"%d", caloriesEaten];
	self.summaryRemainingValueLabel.text = @"??";
#endif

	[self updateSmiley];

	[self.view setNeedsUpdateConstraints];
	[self.view layoutIfNeeded];
}

- (void)getProductByBarcode:(NSString *)barcode andType:(NSString *)type
{
	// check for the item in the custom foods first
	id matchingItem = [CustomFoodMO foodByName:nil productId:nil barcode:barcode inMOC:self.managedObjectContext];
	if (matchingItem) {
		[self addLogWithGeneric:matchingItem atLogTime:[FoodLogMO guessMealTime]];
		return;
	}

	// then the shoppinglist
	matchingItem = [ShoppingListItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	if (matchingItem) {
		[self addLogWithGeneric:matchingItem atLogTime:[FoodLogMO guessMealTime]];
		return;
	}

	// then in pantry
	matchingItem = [PantryItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	if (matchingItem) {
		[self addLogWithGeneric:matchingItem atLogTime:[FoodLogMO guessMealTime]];
		return;
	}

	// then in purchase history
	matchingItem = [PurchaseHxItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	if (matchingItem) {
		[self addLogWithGeneric:matchingItem atLogTime:[FoodLogMO guessMealTime]];
		return;
	}

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	// if the item wasn't found in any local caches, do the search for the barcode
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory productSearchURLForBarcode:barcode andType:type]];
	[fetcher beginFetchWithCompletionHandler: ^(NSData *retrievedData, NSError * error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];
		if (error != nil) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			NSArray *results = [NSJSONSerialization JSONObjectWithData:retrievedData options:kNilOptions error:nil];

			if ([results count] > 0) {
				[self addLogWithDictionary:[results objectAtIndex:0] atLogTime:[FoodLogMO guessMealTime]];
			}
			else {
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Product not found."
					message:[NSString stringWithFormat:@"No matching product found with barcode %@, add this?", barcode]
					delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
				alert.tag = kTagAlertItemNotFound;
				alert.alertViewStyle = UIAlertViewStylePlainTextInput;

				// set the barcode into some storage associated with the alert view
				objc_setAssociatedObject(alert, &barcodeKey, barcode, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

				[alert show];
			}
		}
	}];
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];

	for (NSIndexPath *indexPath in visiblePaths) {
		if (indexPath.section >= SectionBreakfast && indexPath.section < SectionWater) {
			id logItem = [self logItemAtIndexPath:indexPath];
			if ([logItem respondsToSelector:@selector(imageId)] && [[logItem imageId] integerValue]) {
				[self.tableView downloadImageWithImageId:[logItem imageId] forIndexPath:indexPath withSize:kImageSizeMedium];
			}
		}
	}
}

- (id)logItemAtIndexPath:(NSIndexPath *)indexPath
{
	NSArray *subArray = [self.itemsBySection objectAtIndex:indexPath.section];
	
	NSUInteger objIndex = 0;

	// I'm not sure when this wouldn't be an array, but we can return nil if there are no items in the subarray
	if (![subArray isKindOfClass:[NSArray class]] || ![subArray count]) {
		return nil;
	}
	
	switch (indexPath.section) {
		case SectionBreakfast:
		case SectionMorningSnack:
		case SectionLunch:
		case SectionAfternoonSnack:
		case SectionDinner:
		case SectionEveningSnack:
		case SectionActivities:
		case SectionFoodNote:
		case SectionExerciseNote:
			// the sections here have a header and footer that are not in the array
			if (indexPath.row == 0 || indexPath.row > [subArray count]) {
				return nil;
			}
			// row must be adjusted
			objIndex = indexPath.row - 1;
			break;
		case SectionWater:
		case SectionSugaryDrinks:
		case SectionFruitsVeggies:
			// The sections here only have one row ever
			objIndex = indexPath.row;
			break;
	}

	return [subArray objectAtIndex:objIndex];
}

#pragma mark - ZBarReaderDelegate methods
- (void)imagePickerController:(UIImagePickerController *)reader
	didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];

	ZBarSymbol *symbol = nil;
	for (symbol in results) {
		break;
	}

	NSString *barcode = symbol.data;
	NSString *type = symbol.typeName;

	[self getProductByBarcode:barcode andType:type];

	[reader dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	DDLogInfo(@"logging view - view:%@ didUpdateObject:%@", viewController, object);

	if ([object isKindOfClass:[TrainingActivityMO class]]) {
		[object markForSync];

		[[object managedObjectContext] BL_save];

		[self.navigationController popViewControllerAnimated:YES];

		[self reloadDataCache:nil];
	}
	else if ([viewController isKindOfClass:[FoodLogDetailViewController class]]) {
		if (object) {
			FoodLogMO *newFoodLog = object;

			// If we were using a scratch context, save it out first
			if (newFoodLog.managedObjectContext != self.managedObjectContext) {
				[newFoodLog.managedObjectContext BL_save];
			}

			[self.managedObjectContext BL_save];

			NSString *labelText = [NSString stringWithFormat:@"%@ added to your log", newFoodLog.name];
			[BCProgressHUD notificationWithText:labelText onView:self.view];

			[BCObjectManager syncCheck:SendOnly];

			[self reloadDataCache:nil];
		}

		[self dismissViewControllerAnimated:YES completion:nil];
	}
	else {
		BCLoggingJustCaloriesViewController *caloriesView = viewController;
		NSNumber *calories = object;
		FoodLogMO *justCalories = caloriesView.foodLog;

		if (justCalories && [calories integerValue]) {
			justCalories.nutrition.calories = calories;
			[justCalories markForSync];

			[justCalories.managedObjectContext BL_save];

			[self reloadDataCache:nil];
		}

		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

#pragma mark - BCPopupViewDelegate
- (void)popupView:(BCPopupView *)inPopup didDismissWithInfo:(NSDictionary *)info
{
	if (info) {
		if (inPopup.tag == kTagCalendarPopup) {
			NSDate *selectedDate = [info objectForKey:@"selectedDate"];

			self.dataModel.dayOffset = [self.calendar BC_daysFromDate:[NSDate date] toDate:selectedDate];
			[self dayChanged];
		}
	}
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == kTagAlertItemNotFound) {
		if (buttonIndex != alertView.cancelButtonIndex) {
			NSString *name = [[alertView textFieldAtIndex:0] text];

			// check to see if the name is already used in custom foods
			CustomFoodMO *customFood = [CustomFoodMO foodByName:name productId:nil barcode:nil inMOC:self.managedObjectContext];
			if (customFood) {
				 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Duplicate name"
					 message:@"Please enter a unique product name"
					 delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
				 alert.tag = kTagAlertItemNotFound;
				 alert.alertViewStyle = UIAlertViewStylePlainTextInput;

				 [alert show];
			}
			else {
				// retrieve the barcode from the associated storage, then clear it
				NSString *barcode = (NSString *)objc_getAssociatedObject(alertView, &barcodeKey);
				objc_setAssociatedObject(alertView, &barcodeKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

				NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
				FoodLogMO *newLog = [FoodLogMO insertInManagedObjectContext:scratchContext];
				newLog.status = kStatusPost;
				newLog.loginId = [[User currentUser] loginId];
				newLog.date = self.dataModel.startOfLogDate;
				newLog.logTime = [FoodLogMO guessMealTime];
				newLog.servings = @1;
				newLog.nutritionFactor = @1;
				newLog.name = name;
				newLog.itemType = kItemTypeProduct;

				// Start to fill out a custom food, as it gives us a place to store the barcode
				newLog.customFood = [CustomFoodMO insertInManagedObjectContext:newLog.managedObjectContext];
				newLog.customFood.barcode = barcode;

				FoodLogDetailViewController *detailView = [self.storyboard instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
				detailView.delegate = self;
				detailView.managedObjectContext = scratchContext;
				detailView.item = newLog;

				// set a flag to create a custom food - this is sorta hacky
				detailView.createCustomFood = YES;
				detailView.detailViewMode = ItemDetailViewModeEditing;

				[self.navigationController presentViewController:detailView animated:YES completion:nil];
			}
		}
	}
	else {
		NSAssert(NO, @"Unknown alert");
	}
}

@end
