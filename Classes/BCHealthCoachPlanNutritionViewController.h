//
//  BCHealthCoachPlanNutritionViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 7/12/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NutritionMO.h"

@interface BCHealthCoachPlanNutritionViewController : UIViewController

@property (strong, nonatomic) NSDictionary *itemNutrition;
@property (strong, nonatomic) NutritionMO *minNutrition;
@property (strong, nonatomic) NutritionMO *maxNutrition;

@end
