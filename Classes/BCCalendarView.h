//
//  BCCalendarView.h
//  BettrLife
//
//  Created by Sef Tarbell on 8/23/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCPopupView.h"

@interface BCCalendarView : BCPopupView

@property (strong, nonatomic) NSDate* selectedWeek;
@property (strong, nonatomic) NSDate* selectedDay;

- (id)initWithBaseDate:(NSDate *)baseDate delegate:(id<BCPopupViewDelegate> )delegate;

@end
