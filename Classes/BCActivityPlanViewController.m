//
//  BCActivityPlanViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 2/7/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCActivityPlanViewController.h"

#import "ActivityPlanMO.h"
#import "BCDetailViewDelegate.h"
#import "NumberValue.h"
#import "TrackingMO.h"

@interface BCActivityPlanViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *caloriesLabel;

@property (strong, nonatomic) IBOutlet UITextField *hiddenTextField;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSNumber *lastWeight;
@property (assign, nonatomic) BOOL setWeight;

@end

@implementation BCActivityPlanViewController

static const NSInteger kRowDate = 1;
static const NSInteger kRowWeight = 2;
static const NSInteger kRowTime = 3;
//static const NSInteger kRowCalories = 4;

static const NSInteger kTagWeightPickerView = 11111;
static const NSInteger kTagTimePickerView = 11112;

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		self.dateFormatter = [[NSDateFormatter alloc] init];
		[self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[self.dateFormatter setDateStyle:NSDateFormatterShortStyle];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	[self configureView];

	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonTapped:)];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonTapped:)];
}

- (void)viewDidUnload {
	[self setNameLabel:nil];
	[self setHiddenTextField:nil];
	[self setWeightLabel:nil];
	[self setTimeLabel:nil];
	[self setCaloriesLabel:nil];

	[super viewDidUnload];
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == kRowDate) {
		UIDatePicker *aDatePicker = [[UIDatePicker alloc] init];
		aDatePicker.datePickerMode = UIDatePickerModeDate;
		[aDatePicker setDate:self.plan.date animated:NO];
		[aDatePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];

		self.hiddenTextField = [[UITextField alloc] initWithFrame:CGRectZero];

		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
		[cell.contentView addSubview:self.hiddenTextField];

		self.hiddenTextField.inputView = aDatePicker;
		self.hiddenTextField.inputAccessoryView = [self makeKeyboardToolbar];

		[self.hiddenTextField becomeFirstResponder];
	}
	else if (indexPath.row == kRowWeight) {
		UIPickerView *aPickerView = [[UIPickerView alloc] init];
		aPickerView.tag = kTagWeightPickerView;
		aPickerView.showsSelectionIndicator = YES;
		aPickerView.dataSource = self;
		aPickerView.delegate = self;

		[aPickerView reloadAllComponents];

		NSInteger firstComponentIndex = [self.lastWeight integerValue];
		NSInteger secondComponentIndex = ([self.lastWeight doubleValue] * 10) - (firstComponentIndex * 10);
		[aPickerView selectRow:firstComponentIndex inComponent:0 animated:NO];
		[aPickerView selectRow:secondComponentIndex inComponent:1 animated:NO];

		self.hiddenTextField = [[UITextField alloc] initWithFrame:CGRectZero];

		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
		[cell.contentView addSubview:self.hiddenTextField];

		self.hiddenTextField.inputView = aPickerView;
		self.hiddenTextField.inputAccessoryView = [self makeKeyboardToolbar];

		[self.hiddenTextField becomeFirstResponder];
	}
	else if (indexPath.row == kRowTime) {
		UIPickerView *aPickerView = [[UIPickerView alloc] init];
		aPickerView.tag = kTagTimePickerView;
		aPickerView.showsSelectionIndicator = YES;
		aPickerView.dataSource = self;
		aPickerView.delegate = self;

		[aPickerView reloadAllComponents];

		NSArray *timePieces = [self timePiecesFromString:self.plan.time];
		[aPickerView selectRow:[timePieces[0] integerValue] inComponent:0 animated:NO];
		[aPickerView selectRow:[timePieces[1] integerValue] inComponent:1 animated:NO];
		[aPickerView selectRow:[timePieces[2] integerValue] inComponent:2 animated:NO];

		self.hiddenTextField = [[UITextField alloc] initWithFrame:CGRectZero];

		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
		[cell.contentView addSubview:self.hiddenTextField];

		self.hiddenTextField.inputView = aPickerView;
		self.hiddenTextField.inputAccessoryView = [self makeKeyboardToolbar];

		[self.hiddenTextField becomeFirstResponder];
	}
}

#pragma mark - local
- (NSArray *)timePiecesFromString:(NSString *)time
{
	NSNumber *hours = @0;
	NSNumber *minutes = @0;
	NSNumber *seconds = @0;

	if (![self.plan.time isEqualToString:@"null"]) {
		NSArray *timePieces = [time componentsSeparatedByString:@":"];
		if ([timePieces count] == 3) {
			hours = [timePieces[0] numberValueDecimal];
			minutes = [timePieces[1] numberValueDecimal];
			seconds = [timePieces[2] numberValueDecimal];
		}
		else if ([timePieces count] == 2) {
			hours = [timePieces[0] numberValueDecimal];
			minutes = [timePieces[1] numberValueDecimal];
		}
		else if ([timePieces count] == 1) {
			minutes = [timePieces[0] numberValueDecimal];
		}
	}

	return @[hours, minutes, seconds];
}

- (void)configureView
{
	if (self.plan) {
		self.nameLabel.text = self.plan.name;
		self.dateLabel.text = [self.dateFormatter stringFromDate:self.plan.date];

		if (!self.lastWeight) {
			self.lastWeight = [TrackingMO getCurrentWeightInMOC:self.plan.managedObjectContext];
		}
		self.weightLabel.text = [self.lastWeight stringValue];

		if (![self.plan.time isEqualToString:@"null"]) {
			self.timeLabel.text = self.plan.time;

			NSArray *timePieces = [self timePiecesFromString:self.plan.time];
			double minutes = ([timePieces[0] doubleValue] * 60) + [timePieces[1] doubleValue] + ([timePieces[2] doubleValue] / 60.0f);
			NSInteger estimatedCaloriesBurned = (NSInteger)(minutes * [self.plan.factor doubleValue] * [self.lastWeight doubleValue]);

			self.caloriesLabel.text = [NSString stringWithFormat:@"%ld", (long)estimatedCaloriesBurned];
		}
		else {
			self.timeLabel.text = @"00:00:00";
			self.caloriesLabel.text = @"0";
		}
	}
}

- (UIToolbar *)makeKeyboardToolbar
{
	CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
	UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, appFrame.size.width, 35)];
	toolbar.barStyle = UIBarStyleBlack;
	toolbar.translucent = YES;

	UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
		target:nil action:nil];

	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
		initWithTitle:@"Done" style:UIBarButtonItemStyleDone
		target:self action:@selector(resignKeyboard:)];

	toolbar.items = @[flexibleSpace, doneButton];

	return toolbar;
}

- (void)dateChanged:(UIDatePicker *)sender 
{
	self.plan.date = sender.date;

	self.dateLabel.text = [self.dateFormatter stringFromDate:self.plan.date];
}

// resigns the keyboard
- (IBAction)resignKeyboard:(id)sender
{
	[self.hiddenTextField resignFirstResponder];
	[self setHiddenTextField:nil];
}

- (IBAction)cancelButtonTapped:(id)sender
{
	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCDetailViewDelegate)]) {
		[self.delegate view:self didUpdateObject:nil];
	}

}

- (IBAction)saveButtonTapped:(id)sender
{
	if (self.setWeight) {
		[TrackingMO addWeight:self.lastWeight onDate:[NSDate date] inMOC:self.plan.managedObjectContext];

		[self.plan.managedObjectContext BL_save];
	}

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCDetailViewDelegate)]) {
		[self.delegate view:self didUpdateObject:self.plan];
	}

}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	// feet and inches, or lbs
	NSInteger numberOfComponents = 0;

	if (pickerView.tag == kTagWeightPickerView) {
		numberOfComponents = 2;
	}
	else if (pickerView.tag == kTagTimePickerView) {
		numberOfComponents = 3;
	}

	return numberOfComponents;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	NSInteger numberOfRows = 0;

	if (pickerView.tag == kTagWeightPickerView) {
		switch (component) {
			case 0:
				numberOfRows = 1000;
				break;
			case 1:
				numberOfRows = 10;
				break;
		}
	}
	if (pickerView.tag == kTagTimePickerView) {
		switch (component) {
			case 0:
				numberOfRows = 24;
				break;
			case 1:
			case 2:
				numberOfRows = 60;
				break;
		}
	}

	return numberOfRows;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *title;

	if (pickerView.tag == kTagWeightPickerView) {
		title = [NSString stringWithFormat:@"%ld", (long)row];
	}
	else if (pickerView.tag == kTagTimePickerView) {
		title = [NSString stringWithFormat:@"%ld", (long)row];
	}

	return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	if (pickerView.tag == kTagWeightPickerView) {
		NSInteger pounds = [pickerView selectedRowInComponent:0];
		NSInteger tenths = [pickerView selectedRowInComponent:1];
		self.lastWeight = [NSNumber numberWithDouble:pounds + ((double)tenths / 10)];

		self.setWeight = YES;

		[self configureView];
	}
	else if (pickerView.tag == kTagTimePickerView) {
		NSInteger hours = [pickerView selectedRowInComponent:0];
		NSInteger minutes = [pickerView selectedRowInComponent:1];
		NSInteger seconds = [pickerView selectedRowInComponent:2];

		self.plan.time = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];

		[self configureView];
	}
}

@end
