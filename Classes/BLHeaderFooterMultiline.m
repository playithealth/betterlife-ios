//
//  BLHeaderFooterMultiline.m
//  BettrLife
//
//  Created by Greg Goodrich on 3/22/14.
//  Copyright (c) 2014 BettrLife Corp. All rights reserved.
//

#import "BLHeaderFooterMultiline.h"

#import "UIColor+Additions.h"

@interface BLHeaderFooterMultiline ()
@end

@implementation BLHeaderFooterMultiline

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.contentView.backgroundColor = [UIColor BC_tableViewHeaderColor];

    }
    return self;
}

- (UILabel *)multilineLabel
{
	if (_multilineLabel) {
		return _multilineLabel;
	}

	_multilineLabel = [[UILabel alloc] init];
	_multilineLabel.numberOfLines = 0;
	_multilineLabel.lineBreakMode = NSLineBreakByWordWrapping;
	_multilineLabel.font = [UIFont systemFontOfSize:14];
	_multilineLabel.textColor = [UIColor darkGrayColor];
	_multilineLabel.translatesAutoresizingMaskIntoConstraints = NO;
	[self.contentView addSubview:_multilineLabel];
	[self setConstraints];

    return _multilineLabel;
}

- (UILabel *)normalLabel
{
	if (_normalLabel) {
		return _normalLabel;
	}

	_normalLabel = [[UILabel alloc] init];
	_normalLabel.numberOfLines = 1;
	_normalLabel.font = [UIFont systemFontOfSize:14];
	_normalLabel.textColor = [UIColor darkGrayColor];
	_normalLabel.translatesAutoresizingMaskIntoConstraints = NO;
	[self.contentView addSubview:_normalLabel];
	[self setConstraints];
    
    return _normalLabel;
}

- (UIImageView *)headerImageView
{
	if (_headerImageView) {
		return _headerImageView;
	}

	_headerImageView = [[UIImageView alloc] init];
	_headerImageView.translatesAutoresizingMaskIntoConstraints = NO;
	[self.contentView addSubview:_headerImageView];
	[self setConstraints];
    
    return _headerImageView;
}

- (void)setConstraints
{
	NSMutableDictionary *bindingsDict = [[NSMutableDictionary alloc] init];

	// First, establish the entries in the bindings dictionary, and remove constraints along the way
	if (_multilineLabel) {
		[bindingsDict setObject:_multilineLabel forKey:@"multilineLabel"];
		[_multilineLabel removeConstraints:[_multilineLabel constraints]];
	}
	if (_normalLabel) {
		[bindingsDict setObject:_normalLabel forKey:@"normalLabel"];
		[_normalLabel removeConstraints:[_normalLabel constraints]];
	}
	if (_headerImageView) {
		[bindingsDict setObject:_headerImageView forKey:@"headerImageView"];
		[_headerImageView removeConstraints:[_headerImageView constraints]];
	}

	[self removeConstraints:[self constraints]];

	// Now re-establish proper constraints, based upon what elements are in play
	if (_multilineLabel) {
		// Horizontal constraints for the multi line label
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-16-[multilineLabel]-16-|"
			options:0 metrics:nil views:bindingsDict]];

		// Vertical constraints related to the multi line label
		if (_headerImageView) {
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[multilineLabel]-8-[headerImageView]"
				options:0 metrics:nil views:bindingsDict]];
		}
		else if (_normalLabel) {
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[multilineLabel]-8-[normalLabel]"
				options:0 metrics:nil views:bindingsDict]];
		}
		else {
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[multilineLabel]-2-|"
				options:0 metrics:nil views:bindingsDict]];
		}
	}

	if (_headerImageView) {
		// Vertical constraints for the header image view
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[headerImageView(20)]-2-|"
			options:0 metrics:nil views:bindingsDict]];

		if (_normalLabel) {
			// Constraints for the header image view in relation to the normal label
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[headerImageView(20)]-2-[normalLabel]-15-|"
				options:0 metrics:nil views:bindingsDict]];
		}
		else {
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[headerImageView(20)]-15-|"
				options:0 metrics:nil views:bindingsDict]];
		}
	}
	else if (_normalLabel) {
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[normalLabel]-15-|"
			options:0 metrics:nil views:bindingsDict]];
	}

	if (_normalLabel) {
		// Vertical constraints for the normal label
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[normalLabel]-3-|"
			options:0 metrics:nil views:bindingsDict]];
	}
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
