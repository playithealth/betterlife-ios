//
//  BCApplicationSettingsViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/13/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCApplicationSettingsViewController : UIViewController

@end
