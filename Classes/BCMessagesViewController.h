//
//  BCMessagesViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCObjectManager.h"
#import "BCTableViewController.h"
#import "InboxMessageCell.h"

@interface BCMessagesViewController : BCTableViewController 
<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate> 

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, unsafe_unretained) IBOutlet InboxMessageCell *customCell;

@end
