//
//  InboxMessageViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "InboxMessageViewController.h"

#import "BCUrlFactory.h"
#import "GTMHTTPFetcherAdditions.h"
#import "GTMHTTPFetcherLogging.h"
#import "BCProgressHUD.h"
#import "MessageComposerViewController.h"
#import "OAuthUtilities.h"
#import "UIColor+Additions.h"
#import "User.h"

@interface InboxMessageViewController ()
- (void)configureToolbarItems;
@end

@implementation InboxMessageViewController
@synthesize senderLabel;
@synthesize subjectLabel;
@synthesize dateLabel;
@synthesize messageWebView;
@synthesize actionBar;
@synthesize message = _message;

#pragma mark - Lifespan
- (void)messageTextFetcher:(GTMHTTPFetcher *)inFetcher 
finishedWithData:(NSData *)retrievedData 
error:(NSError *)error 
{
	if (error != nil) {
		DDLogError(@"failed - %@", error);
	} 
	else {
		// fetch succeeded
		NSString *response = [[NSString alloc] initWithData:retrievedData
			encoding:NSUTF8StringEncoding];

		self.message.text = response;

		NSError *error = nil;
		if (![self.message.managedObjectContext save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}

		[messageWebView loadHTMLString:self.message.text baseURL:nil];

	}

	// Hide the HUD in the main thread 
	[BCProgressHUD hideHUD:YES];
}

- (void)viewDidLoad
{
	self.actionBar.tintColor = [UIColor BC_mediumLightGreenColor];

	[self configureToolbarItems];

	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated 
{
	senderLabel.text = self.message.sender;
	subjectLabel.text = self.message.subject;

	NSNumber *messageId = self.message.cloudId;
	NSNumber *unixTime = self.message.createdOn;
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:[unixTime doubleValue]];    
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	dateLabel.text = [formatter stringFromDate:date];

	if (!self.message.text 
			|| [self.message.text isEqualToString:@"TEXT"]) {
		BCProgressHUD *hud = [BCProgressHUD showHUDAddedTo:self.view animated:YES];
		hud.labelText = @"loading...";

		GTMHTTPFetcher *messageTextFetcher = 
			[GTMHTTPFetcher signedFetcherWithURL:
			[BCUrlFactory messageUrlForMessage:[messageId integerValue]]];
		[messageTextFetcher beginFetchWithDelegate:self
			didFinishSelector:@selector(messageTextFetcher:finishedWithData:error:)];
	}
	else {
		[messageWebView loadHTMLString:self.message.text baseURL:nil];
	}

	[super viewWillAppear:animated];
}

- (void)viewDidUnload 
{
	self.subjectLabel = nil;
	self.dateLabel = nil;
	self.messageWebView = nil;
	self.message = nil;
	[self setSenderLabel:nil];
    [self setActionBar:nil];
	[super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"Reply"]) {
		MessageComposerViewController *composer = segue.destinationViewController;
		composer.replyMessage = self.message;
	}
}

#pragma mark - button handlers
- (IBAction)deleteTapped:(id)sender 
{
	// mark this item for deletion
	self.message.isRemoved = [NSNumber numberWithBool:YES];
	self.message.status = kStatusPut;

	// Save the context.
	NSError *error = nil;
	if (![self.message.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - local methods
- (void)configureToolbarItems
{
	UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
		target:nil action:nil];

	UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
		target:nil action:nil];
	fixedSpace.width = 105;

	UIBarButtonItem *trashButton = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
		target:self action:@selector(deleteTapped:)];

	UIBarButtonItem *replyButton = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemReply
		target:self action:@selector(replyTapped:)];

	if ([self.message.senderId integerValue] > 0) {
		actionBar.items = [NSArray arrayWithObjects:
			flexibleSpace,
			trashButton,
			fixedSpace,
			replyButton,
			nil];
	}
	else {
		actionBar.items = [NSArray arrayWithObjects:
			flexibleSpace,
			trashButton,
			flexibleSpace,
			nil];
	}

}

- (IBAction)replyTapped:(id)sender
{
}

@end
