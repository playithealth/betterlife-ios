//
//  BLLoggingWorkoutActivityViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 2/10/15.
//  Copyright (c) 2015 BettrLife Corporation. All rights reserved.
//

#import "BLLoggingWorkoutActivityViewController.h"

#import "ActivityLogMO.h"
#import "ActivityLogSetMO.h"
#import "AdvisorActivityPlanWorkoutActivitySetMO.h"
#import "BLLoggingWorkoutActionView.h"
#import "BLLoggingWorkoutActivityCell.h"
#import "User.h"

@interface BLLoggingWorkoutActivityViewController () <BLWorkoutActivityCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *activityNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *weightButton;
@property (weak, nonatomic) IBOutlet UIView *timedActivityControlView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timedActivityControlHeightConstraint;
@property (strong, nonatomic) BLAnimatingTextLayer *timeElapsedLayer;
@property (strong, nonatomic) BLAnimatingTextLayer *timeRemainingLayer;
@property (strong, nonatomic) NSMutableArray *activitySets;
@property (assign, nonatomic) NSUInteger activeRow; ///< The currently 'running' set/row for timed activities
@property (assign, nonatomic) BOOL isPlaying; ///< Whether the activity is being 'played' (timed activities only)
@property (assign, nonatomic) double elapsedDuration;
@property (assign, nonatomic) double totalDuration;
@property (strong, nonatomic) NSNumberFormatter *numberFormatter;
@end

@implementation BLLoggingWorkoutActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
	self.numberFormatter = [[NSNumberFormatter alloc] init];
	[self.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[self.numberFormatter setMinimumFractionDigits:1];
	[self.numberFormatter setMaximumFractionDigits:1];

	self.activityNameLabel.text = (self.activityLog ? self.activityLog.activityName : self.activityMO.activityName);

	// Load up the meta data sets, if any
	self.activitySets = [[self.activityMO.sets sortedArrayUsingDescriptors:@[[NSSortDescriptor
		sortDescriptorWithKey:ActivitySetMOAttributes.sortOrder ascending:YES]]] mutableCopy];
	
	// Now mesh in the logged sets
	if (self.activityLog && self.activityLog.sets) {
		NSArray *loggedSets = [self.activityLog.sets sortedArrayUsingDescriptors:@[[NSSortDescriptor
			sortDescriptorWithKey:ActivitySetMOAttributes.sortOrder ascending:YES]]];
		if (!self.activitySets) {
			self.activitySets = [loggedSets mutableCopy];
		}
		else {
			// Mesh logged sets with meta sets
			for (ActivitySetMO *existingSet in loggedSets) {
				if (existingSet.basedOnSet) {
					[self.activitySets replaceObjectAtIndex:[existingSet.basedOnSet.sortOrder integerValue] withObject:existingSet];
				}
				else {
					// Add this set to the end of the sets array
					[self.activitySets addObject:existingSet];
				}
			}
		}
	}

	if ([self isTimedActivity]) {
		// Tally up the total time for all the timed sets
		for (ActivitySetMO *set in self.activitySets) {
			self.totalDuration += [set.seconds doubleValue];
		}

		double width = self.timedActivityControlView.layer.frame.size.width;
		double buttonDiameter = 45.0;
		double fontSize = 18.0;
		double gap = (width / 2.0) - (buttonDiameter / 2.0);
		UIFont *myFont = [UIFont systemFontOfSize:fontSize];
		double layerHeight = ceil(myFont.lineHeight);

		self.timeElapsedLayer = [[BLAnimatingTextLayer alloc] init];
		self.timeElapsedLayer.frame = CGRectMake(0, 0, 120, layerHeight);
		self.timeElapsedLayer.foregroundColor = [[UIColor whiteColor] CGColor];
		self.timeElapsedLayer.position = CGPointMake(gap / 2.0, 0);
		self.timeElapsedLayer.contentsScale = [[UIScreen mainScreen] scale];
		self.timeElapsedLayer.fontSize = fontSize;
		self.timeElapsedLayer.alignmentMode = kCAAlignmentCenter;
		self.timeElapsedLayer.durationInSeconds = @(0);

		self.timeRemainingLayer = [[BLAnimatingTextLayer alloc] init];
		self.timeRemainingLayer.frame = CGRectMake(0, 0, 120, layerHeight);
		self.timeRemainingLayer.foregroundColor = [[UIColor whiteColor] CGColor];
		self.timeRemainingLayer.position = CGPointMake(width - (gap / 2.0), 0);
		self.timeRemainingLayer.contentsScale = [[UIScreen mainScreen] scale];
		self.timeRemainingLayer.fontSize = fontSize;
		self.timeRemainingLayer.alignmentMode = kCAAlignmentCenter;
		self.timeRemainingLayer.durationInSeconds = @(self.totalDuration);

		[self.timedActivityControlView.layer addSublayer:self.timeElapsedLayer];
		[self.timeElapsedLayer centerVertically];
		[self.timedActivityControlView.layer addSublayer:self.timeRemainingLayer];
		[self.timeRemainingLayer centerVertically];

		[self.timeElapsedLayer setNeedsDisplay];
		[self.timeRemainingLayer setNeedsDisplay];
	}
	else {
		// Hide this view, as this is not a timed activity
		self.timedActivityControlView.hidden = YES;
		self.timedActivityControlHeightConstraint.constant = 0;
	}

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - local
- (ActivityLogMO *)newActivityLog
{
	ActivityLogMO *activityLog = [ActivityLogMO activityLogFromWorkoutActivity:self.activityMO
		inManagedObjectContext:self.managedObjectContext];
	activityLog.loginId = [User loginId];

	// Populate the activity record portion
	activityLog.status = kStatusPost;
	#pragma message "TODO Populate the activity record portion of the activity log"
	activityLog.targetDate = self.logDate;
	// Do we need to fill this out?
	//activityLog.targetDateString = ;

	return activityLog;
}

- (BOOL)isTimedActivity {
	// Use the meta data if we have it, otherwise, fall back to the logged data, as this will avoid constructing a logged activity if
	// not needed, since the getter for the logged activity creates it if needed
	return (self.activityMO ? self.activityMO.isTimedActivity : self.activityLog.isTimedActivity);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.activitySets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BLLoggingWorkoutActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivitySet" forIndexPath:indexPath];
    
    // Configure the cell...
	ActivitySetMO *activitySet = [self.activitySets objectAtIndex:indexPath.row];
	cell.nameLabel.text = activitySet.name;
	// TODO gracefully handle hours/minutes/seconds display
	cell.descriptionLabel.text = [NSString stringWithFormat:@"%lu min", (unsigned long)([activitySet.seconds doubleValue] / 60)];
	cell.actionView.labelText = [NSString stringWithFormat:@"%lu", indexPath.row + 1];
	// Don't show the circle for timed sets on timed activities
	cell.actionView.showCircle = ([self isTimedActivity] && [activitySet.seconds doubleValue] ? NO : YES);
	cell.delegate = self;
	cell.actionView.delegate = self;

	cell.actionView.completed = (activitySet.basedOnSet ? YES : NO);

    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// TODO
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - BLWorkoutActionViewDelegate
- (void)startAction:(NSIndexPath *)indexPath {
	AdvisorActivityPlanWorkoutActivitySetMO *activitySet = [self.activitySets objectAtIndex:indexPath.row];
	BLLoggingWorkoutActivityCell *cell = (BLLoggingWorkoutActivityCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	// Sets only get saved once their time has completed

	// Start/Stop progress, pass in the duration
	[cell.actionView startProgressWithDuration:activitySet.seconds];
}

- (void)actionComplete:(BLLoggingWorkoutActionView *)actionView {
	// "Complete" the currently running set
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.activeRow inSection:0];
	ActivitySetMO *existingSet = [self.activitySets objectAtIndex:indexPath.row];
	ActivityLogSetMO *activitySet = [ActivityLogSetMO insertInManagedObjectContext:self.managedObjectContext];
	[activitySet copyFromActivitySet:existingSet];
	if (!self.activityLog) {
		self.activityLog = [self newActivityLog];
	}
	activitySet.activity = self.activityLog;
	activitySet.basedOnSet = existingSet;

	// TODO Make sure the remaining items are timed before running, skip untimed items
	NSUInteger numActions = [self.activitySets count];
	if (++self.activeRow < numActions) {
		// Fire the next action
		indexPath = [NSIndexPath indexPathForRow:self.activeRow inSection:0];
		[self startAction:indexPath];
	}
}

- (void)actionViewTapped:(BLLoggingWorkoutActionView *)actionView inCell:(UITableViewCell *)cell
{
	// TODO If this is a timed activity and it is currently being 'played', then pause it here and grab the accumulated time for the
	// current set to use, rather than the total time for the set, and adjust totalDuration as well, to reflect this
	// TODO Maybe can use this to our advatage: [self.timeElapsedLayer.presentationLayer durationInSeconds]

	NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
	ActivitySetMO *existingSet = [self.activitySets objectAtIndex:indexPath.row];

	// Toggle the completed status
	if ([self isTimedActivity] && [existingSet.seconds doubleValue]) {
		// TODO For now, just don't allow checking/unchecking timed sets in a timed activity
		return;

		// For timed activities, you can only check the 'next' one in line or uncheck the 'last' one completed
		if (actionView.completed) {
			// Special case for the first set, obviously there is no prior set
			ActivitySetMO *nextSet = ((indexPath.row + 1) < [self.activitySets count]
				? [self.activitySets objectAtIndex:indexPath.row + 1] : nil);
			// If they're trying to uncheck this set, see if the next set is checked, if so, don't allow it
			// Should this just check the next actionView rather than the class of the set in the array?
			if ([nextSet isKindOfClass:[ActivityLogSetMO class]]) {
				return;
			}
		}
		else {
			// Special case for the first set, obviously there is no prior set
			ActivitySetMO *priorSet = (indexPath.row ? [self.activitySets objectAtIndex:indexPath.row - 1] : nil);
			// If they're trying to check this set, see if the prior set is checked, if so, allow it
			// Should this just check the prior actionView rather than the class of the set in the array?
			if (priorSet && ![priorSet isKindOfClass:[ActivityLogSetMO class]]) {
				return;
			}
		}
	}

	actionView.completed = !actionView.completed;

	// Update the data
	if (actionView.completed) {
		ActivityLogSetMO *activitySet = [ActivityLogSetMO insertInManagedObjectContext:self.managedObjectContext];
		[activitySet copyFromActivitySet:existingSet];
		if (!self.activityLog) {
			self.activityLog = [self newActivityLog];
		}
		activitySet.activity = self.activityLog;
		activitySet.basedOnSet = existingSet;
		// Now replace the object in the sets array with this new 'logged' version. This will make it easier to manage this, in
		// case the user unchecks (uncompletes) this set
		[self.activitySets replaceObjectAtIndex:indexPath.row withObject:activitySet];
	}
	else {
		// Remove the set being 'uncompleted', if this set is not based upon a meta set, then it just goes away, otherwise the
		// meta set comes back to replace it
		if (existingSet.basedOnSet) {
			// This set is based upon a 'meta' set, replace the object in the sets array with the meta version.
			[self.activitySets replaceObjectAtIndex:indexPath.row withObject:existingSet.basedOnSet];
		}
		else {
			[self.activitySets removeObjectAtIndex:indexPath.row];
			
			[self.tableView beginUpdates];
			[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
			[self.tableView endUpdates];
		}

		[self.managedObjectContext deleteObject:existingSet];
	}
}

#pragma mark - responders & events
- (IBAction)playButtonTapped:(UIButton *)sender {
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.activeRow inSection:0];
	[self startAction:indexPath];
	self.isPlaying = !self.isPlaying;

	UIImage *image = (self.isPlaying ? [UIImage imageNamed:@"ActivityPause"] : [UIImage imageNamed:@"ActivityPlay"]);
	[sender setImage:image forState:UIControlStateNormal];

	if (self.isPlaying) {
		CFTimeInterval pausedTime = [self.timedActivityControlView.layer timeOffset];
		if (!pausedTime) {
			// Start
			CABasicAnimation *eAnimation = [CABasicAnimation animationWithKeyPath:@"durationInSeconds"];
			eAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
			eAnimation.duration = self.totalDuration;
			// Start fromValue out as what already may have elapsed when coming in (reentry of this activity view)
			eAnimation.fromValue = self.activityLog.elapsedDuration;
			eAnimation.toValue = @(self.totalDuration);
			self.timeElapsedLayer.durationInSeconds  = @(0);
			[self.timeElapsedLayer addAnimation:eAnimation forKey:nil];

			CABasicAnimation *rAnimation = [CABasicAnimation animationWithKeyPath:@"durationInSeconds"];
			rAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
			rAnimation.duration = self.totalDuration;
			// TODO Start fromValue out as what already may have elapsed when coming in (reentry of this activity view)
			rAnimation.fromValue = @(self.totalDuration - [self.activityLog.elapsedDuration doubleValue]);
			rAnimation.toValue = @(0);
			self.timeRemainingLayer.durationInSeconds  = @(0);
			[self.timeRemainingLayer addAnimation:rAnimation forKey:nil];
		}
		else {
			// Resume
			CFTimeInterval pausedTime = [self.timedActivityControlView.layer timeOffset];
			self.timedActivityControlView.layer.speed = 1.0;
			self.timedActivityControlView.layer.timeOffset = 0.0;
			self.timedActivityControlView.layer.beginTime = 0.0;
			CFTimeInterval timeSincePause = [self.timedActivityControlView.layer convertTime:CACurrentMediaTime() fromLayer:nil]
				- pausedTime;
			self.timedActivityControlView.layer.beginTime = timeSincePause;
		}
	}
	else {
		// Pause
		CFTimeInterval pausedTime = [self.timedActivityControlView.layer convertTime:CACurrentMediaTime() fromLayer:nil];
		self.timedActivityControlView.layer.speed = 0.0;
		self.timedActivityControlView.layer.timeOffset = pausedTime;
	}

}

- (IBAction)weightButtonTapped:(UIButton *)sender {
    // TODO
}

- (IBAction)notesTapped:(UITapGestureRecognizer *)sender {
    // TODO
}

@end
