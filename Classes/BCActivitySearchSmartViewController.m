//
//  BCActivitySearchSmartViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/18/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCActivitySearchSmartViewController.h"

#import "BCActivitySearchDataModel.h"
#import "BCProgressHUD.h"
#import "TrainingActivityMO.h"

@implementation BCActivitySearchSmartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataModel.trainingActivitiesGroupedByActivity.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"NormalCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];

	UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTapped:)];
	[cell.contentView addGestureRecognizer:recognizer];

	TrainingActivityMO *trainingActivity = self.dataModel.trainingActivitiesGroupedByActivity[indexPath.row][@"activity"];
    
    cell.textLabel.text = trainingActivity.activity;
	if ([trainingActivity.time isEqualToString:@"null"]) {
		cell.detailTextLabel.text = nil;
	}
	else {
		cell.detailTextLabel.text = trainingActivity.time;
	}
    
    return cell;
}

#pragma mark - responders
- (void)contentViewTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	TrainingActivityMO *original = self.dataModel.trainingActivitiesGroupedByActivity[indexPath.row][@"activity"];

	TrainingActivityMO *newActivity = [self.dataModel insertActivityBasedOnTrainingActivity:original];

	[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your log", newActivity.activity] 
		onView:self.view.superview];
}

@end
