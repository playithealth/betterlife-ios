//
//  AverageRatingCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/7/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StarRatingControl;

@interface AverageRatingCell : UITableViewCell {
    
}

@property (nonatomic, strong) IBOutlet UILabel *categoryLabel;
@property (nonatomic, strong) IBOutlet UILabel *ratingCountLabel;
@property (nonatomic, strong) StarRatingControl *ratingControl;

@end
