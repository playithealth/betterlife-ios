//
//  QuantityActionSheetViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/22/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShoppingListViewController;

@interface QuantityActionSheetViewController : UIViewController {
	UIView *actionSheetView;
}

//@property (nonatomic, retain) NSMutableDictionary *shoppingListItem;
@property (nonatomic, strong) IBOutlet UIView *actionSheetView;
@property (nonatomic, strong) IBOutlet UITextField *quantityField;

- (void)save;
- (IBAction)doneClicked;
- (IBAction)slideOut;
- (IBAction)numberClicked:(id)sender;
- (IBAction)decimalClicked;
- (IBAction)backspaceClicked;

@end
