//
//  BLSidebarCalendarButton.h
//  BettrLife
//
//  Created by Greg Goodrich on 10/28/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLSidebarCalendarButton : UIButton
@property (assign, nonatomic) BOOL hasData;
@end
