//
//  PantryViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/24/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCObjectManager.h"
#import "BCViewController.h"
#import "PantryItemViewController.h"
#import "PantryViewCell.h"

typedef NS_ENUM(NSInteger, PantrySortMode) {
	PantrySortByCategory,
	PantrySortByName,
	PantrySortByPurchaseDate
};

@interface PantryViewController : BCViewController 
<NSFetchedResultsControllerDelegate, PantryItemViewDelegate, UITableViewDataSource, UITableViewDelegate, ZBarReaderDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (unsafe_unretained, nonatomic) IBOutlet PantryViewCell* customCell;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end
