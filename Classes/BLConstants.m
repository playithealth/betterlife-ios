//
//  BLConstants.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 2/04/14.
//  Copyright 2014 BettrLife Corporation. All rights reserved.
//

#import "BLConstants.h"

// Types
NSString * const kItemTypeUserFemale = @"female";
NSString * const kItemTypeUserMale = @"male";
NSString * const kItemTypeProfileFemale = @"pfemale";
NSString * const kItemTypeProfileMale = @"pmale";
NSUInteger const kGenderMale = 2;
NSUInteger const kGenderFemale = 1;

// Stock images
NSString * const kStockImageRecipe = @"Recipe";
NSString * const kStockImageProduct = @"Product";
NSString * const kStockImageMenuItem = @"Menu-Item";
NSString * const kStockImageJustCalories = @"Just-Calories";
NSString * const kStockImageRecipeLarge = @"ItemDetail-Recipe";
NSString * const kStockImageProductLarge = @"ItemDetail-Product";
NSString * const kStockImageMenuItemLarge = @"ItemDetail-Menu-Item";
NSString * const kStockImageJustCaloriesLarge = @"JustCaloriesDetails";
NSString * const kStockImageUserFemale = @"MessagesNoImageFemale";
NSString * const kStockImageUserMale = @"MessagesNoImageMale";
NSString * const kStockImageProfileFemale = @"user-female";
NSString * const kStockImageProfileMale = @"user-male";

// Grayscale stock images
NSString * const kStockImageRecipeGrayscale = @"Recipe-Suggestion";
NSString * const kStockImageProductGrayscale = @"Product-Suggestion";
NSString * const kStockImageMenuItemGrayscale = @"Menu-Item-Suggestion";
