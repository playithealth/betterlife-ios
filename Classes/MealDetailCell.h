//
//  MealDetailCell.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 8/23/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StarRatingControl;

@interface MealDetailCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *mealImageView;
@property (nonatomic, strong) IBOutlet UILabel *mealNameLabel;
@property (nonatomic, strong) StarRatingControl *starRatingControl;
@property (nonatomic, strong) IBOutlet UILabel *servesLabel;

@end
