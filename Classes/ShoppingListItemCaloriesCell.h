//
//  ShoppingListItemCaloriesCell.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/6/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingListItemCaloriesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *caloriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *caloriesFromFatLabel;

@end
