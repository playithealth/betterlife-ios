//
//  BLConversationViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 5/21/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLConversationViewController.h"

#import "BCObjectManager.h"
#import "ConversationMO.h"
#import "MessageMO.h"
#import "MessageDeliveryMO.h"
#import "User.h"

@implementation MessageCell
@end

#pragma mark - Conversation View
@interface BLConversationViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) NSArray *messageDeliveries;
@property (weak, nonatomic) IBOutlet UITextField *sendMessageTextField;
@property (strong, nonatomic) IBOutlet MessageCell *senderCell;
@property (strong, nonatomic) IBOutlet MessageCell *receiverCell;
@property (assign, nonatomic) CGRect initialFrameRect;
@end

@implementation BLConversationViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:(NSCoder *)aDecoder];
	if (self) {
        // Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
		name:UIKeyboardWillShowNotification object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
		name:UIKeyboardWillHideNotification object:nil];

	[self reloadData];

	/*
	self.receiverCell = (MessageCell *)[self.tableView dequeueReusableCellWithIdentifier:@"ReceiveTextView"];
	self.senderCell = (MessageCell *)[self.tableView dequeueReusableCellWithIdentifier:@"SendTextView"];
	*/
	self.receiverCell = (MessageCell *)[self.tableView dequeueReusableCellWithIdentifier:@"Receive"];
	self.senderCell = (MessageCell *)[self.tableView dequeueReusableCellWithIdentifier:@"Send"];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:[MessageMO entityName] object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
	// For some reason, this will only move the view to the second to the last row, then the viewDidAppear moves it the rest of the way
	// I kept this here because the scrolling effect is much less if this moves it down first
	if ([self isMovingToParentViewController] && [self.messageDeliveries count]) {
		// Scroll to the bottom of the table
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messageDeliveries count] - 1 inSection:0]
			atScrollPosition:UITableViewScrollPositionBottom animated:NO];
	}

	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	if ([self isMovingToParentViewController] && [self.messageDeliveries count]) {
		// Scroll to the bottom of the table
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messageDeliveries count] - 1 inSection:0]
			atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}

	[super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.messageDeliveries count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	MessageDeliveryMO *messageDelivery = [self.messageDeliveries objectAtIndex:indexPath.row];

	MessageCell *cell = nil;
	if ([messageDelivery.loginId integerValue] == [[[User currentUser] loginId] integerValue]) {
		//cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"ReceiveTextView" forIndexPath:indexPath];
		cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"Receive" forIndexPath:indexPath];
	}
	else {
		//cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"SendTextView" forIndexPath:indexPath];
		cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"Send" forIndexPath:indexPath];
	}
	cell.bubbleView.layer.cornerRadius = 8.0f;
    
    // Configure the cell...
	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	MessageDeliveryMO *messageDelivery = [self.messageDeliveries objectAtIndex:indexPath.row];

	MessageCell *cell = nil;
	if ([messageDelivery.loginId integerValue] == [[[User currentUser] loginId] integerValue]) {
		cell = self.receiverCell;
	}
	else {
		cell = self.senderCell;
	}

	[self configureCell:cell atIndexPath:indexPath];

    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];

    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));

    [cell setNeedsLayout];
    [cell layoutIfNeeded];

	// Get the actual height required for the cell's contentView
	CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;

	// Add an extra point to the height to account for the cell separator, which is added between the bottom
	// of the cell's contentView and the bottom of the table view cell.
//	height += 18.0f;
	height += 1.0f;

    return height;
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 52;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	BLMessageFooterView *footerView = nil;

	footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MessageFooter"];
	footerView.message.delegate = self;

	return footerView;
}
*/

#pragma mark - local
- (void)reloadData
{
	NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"message.createdOn" ascending:YES];
	self.messageDeliveries = [self.conversation.newestMessage sortedArrayUsingDescriptors:@[descriptor]];

	if ([NSMutableAttributedString instancesRespondToSelector:@selector(initWithData:options:documentAttributes:error:)]) {
		self.messages = [NSMutableArray array];
		for (MessageDeliveryMO *messageDelivery in self.messageDeliveries) {
			NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
				initWithData:[messageDelivery.message.body dataUsingEncoding:NSUTF8StringEncoding]
				options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
				NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
			[attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15]
				range:NSMakeRange(0, [attributedString length])];

			[self.messages addObject:attributedString];
		}
	}
	else {
		self.messages = nil;
	}

	// Mark this thread 'read'
	self.conversation.isRead = @YES;

	[self.conversation.managedObjectContext BL_save];

	if (self.delegate) {
		[self.delegate updatedConversation:self.conversation];
	}
	[BCObjectManager syncCheck:SendOnly];
}

- (void)refreshView:(NSNotification *)notification
{
	[self reloadData];
	[self.tableView reloadData];
}

- (void)configureCell:(MessageCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	MessageDeliveryMO *messageDelivery = [self.messageDeliveries objectAtIndex:indexPath.row];

	NSString *messageDateString = [NSDateFormatter localizedStringFromDate:
		[NSDate dateWithTimeIntervalSince1970:[messageDelivery.message.createdOn doubleValue]]
		dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle];
	if ([messageDelivery.loginId integerValue] == [[[User currentUser] loginId] integerValue]) {
		cell.dateLabel.text = [NSString stringWithFormat:@"%@, %@", messageDelivery.conversation.conversationUserName, messageDateString];
	}
	else {
		cell.dateLabel.text = [NSString stringWithFormat:@"%@", messageDateString];
	}

	if (self.messages) {
		// Label - attributed
		cell.messageLabel.attributedText = [self.messages objectAtIndex:indexPath.row];
	}
	else {
		// Label - plain
		cell.messageLabel.text = messageDelivery.message.body;
	}
	[cell.messageLabel sizeToFit];


	//
	// TextView - attributed
	/*
	if ([NSMutableAttributedString instancesRespondToSelector:@selector(initWithData:options:documentAttributes:error:)]) {
		NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
			initWithData:[messageDelivery.message.body dataUsingEncoding:NSUTF8StringEncoding]
			options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
			documentAttributes:nil error:nil];
		[attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15]
			range:NSMakeRange(0, [attributedString length])];
		cell.messageTextView.attributedText = attributedString;
	}
	else {
		// TODO Punt for now on iOS6 when trying to parse out the html and just display it
		cell.messageTextView.text = messageDelivery.message.body;
	}

	[cell.messageTextView sizeToFit];
	*/

//	cell.messageTextView.text = messageDelivery.message.body;
}

- (IBAction)sendButtonPressed:(UIButton *)sender
{
	[self.sendMessageTextField resignFirstResponder];
	NSString *message = [self.sendMessageTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	self.sendMessageTextField.text = @"";
	if ([message length]) {
		// Save this message as a new sent message
		MessageMO *messageMO = [MessageMO insertInManagedObjectContext:self.conversation.managedObjectContext];
		messageMO.body = message;
		messageMO.loginId = [[User currentUser] loginId];
		messageMO.createdOn = @([[NSDate date] timeIntervalSince1970]);

		MessageDeliveryMO *messageDeliveryMO = [MessageDeliveryMO insertInManagedObjectContext:self.conversation.managedObjectContext];
		messageDeliveryMO.loginId = self.conversation.conversationUserId;
		messageDeliveryMO.status = kStatusPost;
		messageDeliveryMO.message = messageMO;

		[self.conversation addMessageDeliveriesObject:messageDeliveryMO];

		[self.conversation.managedObjectContext BL_save];
		// Make sure that the conversation MO is refreshed so that its fetched property of newestMessage is up to date
		[self.conversation.managedObjectContext refreshObject:self.conversation mergeChanges:NO];
		self.messageDeliveries = [self.messageDeliveries arrayByAddingObject:messageDeliveryMO];
		if (self.messages) {
			NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
				initWithData:[messageDeliveryMO.message.body dataUsingEncoding:NSUTF8StringEncoding]
				options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
				NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
			[attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15]
				range:NSMakeRange(0, [attributedString length])];

			[self.messages addObject:attributedString];
		}
		if (self.delegate) {
			[self.delegate updatedConversation:self.conversation];
		}
		[BCObjectManager syncCheck:SendOnly];

	//	[self.tableView reloadData];
		[self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[self.messageDeliveries count] - 1 inSection:0]]
			withRowAnimation:UITableViewRowAnimationAutomatic];
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messageDeliveries count] - 1 inSection:0]
			atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
 
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillBeShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
 
	/*
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
	*/
 
 	self.initialFrameRect = self.view.frame;
	CGRect aRect = self.view.frame;
	aRect.size.height -= kbSize.height;
	[UIView animateWithDuration:1.0
		animations:^{
			[self.view setFrame:aRect];
		}
		completion:^(BOOL finished){
		}];
 /*
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.tableView scrollRectToVisible:activeField.frame animated:YES];
    }
*/
}

/*
- (void)keyboardWasShown:(NSNotification*)aNotification
{
	NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    [activeField.superview setFrame:bkgndRect];
}
*/
 
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
/*
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
*/

	[UIView animateWithDuration:1.0
		animations:^{
			[self.view setFrame:self.initialFrameRect];
		}
		completion:^(BOOL finished){
		}];
}

@end
