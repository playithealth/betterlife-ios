//
//  BCPopupView.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/6/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol BCPopupViewDelegate;

@interface BCPopupView : UIView

@property (strong) UILabel *titleLabel;
@property (unsafe_unretained) id<BCPopupViewDelegate> delegate;
@property (strong) NSDictionary *properties;
@property (unsafe_unretained) CGRect outerFrame;
@property (unsafe_unretained) CGRect innerFrame;
@property (strong, nonatomic) CAGradientLayer *outerLayer;
@property (strong, nonatomic) CALayer *innerLayer;
@property (strong) UIView *customView;
@property (strong) UITableView *tableView;
@property (strong) NSArray *sectionHeaders;
@property (strong) NSArray *rowTitles;
@property (assign) BOOL drawCaret;

- (id)initWithView:(UIView *)customView
		caretPosition:(CGPoint)caretPosition
		title:(NSString *)title
		delegate:(id)delegate;
- (id)initWithTableViewStyle:(UITableViewStyle)style
		caretPosition:(CGPoint)caretPosition
		title:(NSString *)title
		delegate:(id)delegate;
- (id)initWithTableViewStyle:(UITableViewStyle)style
		caretPosition:(CGPoint)caretPosition
		title:(NSString *)title
		delegate:(id)delegate
		rowTitles:(NSArray *)inRowTitles;
- (id)initWithTableViewStyle:(UITableViewStyle)style
		caretPosition:(CGPoint)caretPosition
		title:(NSString *)title
		delegate:(id)delegate
		rowTitles:(NSArray *)inRowTitles
		sectionHeaders:(NSArray *)inSectionHeaders;

- (void)setProperty:(id)obj forKey:(NSString *)key;
- (id)propertyForKey:(NSString *)key;

@end

@protocol BCPopupViewDelegate <NSObject>
@optional
- (void)popupView:(BCPopupView *)popupView
	didDismissWithInfo:(NSDictionary *)userInfo;

- (void)popupView:(BCPopupView *)popupView
	didDismissWithSelectedIndexPath:(NSIndexPath *)indexPath;

@end
