//
//  StarRatingControl.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/23/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "StarRatingControl.h"

#import "QuartzCore/QuartzCore.h"

@implementation StarRatingControl
{
	UIImage *emptyStar;
	UIImage *fullStar;
 
	NSUInteger rating;
	BOOL animate;
 
	int numberOfStars;
	UIEdgeInsets edgeInsets;
}
@dynamic rating;
@synthesize delegate;
@synthesize emptyStar;
@synthesize fullStar;
@synthesize animate;
@synthesize numberOfStars;
@synthesize edgeInsets;

#pragma mark lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

		animate = YES;
		rating = -1;
		numberOfStars = 5;
	
		[self updateStars];
    }
    return self;
}
- (id)initWithFrame:(CGRect)inFrame
animateStars:(BOOL)inAnimate
emptyStarImage:(UIImage *)inEmptyStar
fullStarImage:(UIImage *)inFullStar
delegate:(id<StarRatingControlDelegate>)inDelegate
{
    self = [super initWithFrame:inFrame];
    if (self) {

		animate = inAnimate;
		rating = -1;
		numberOfStars = 5;
		self.emptyStar = inEmptyStar;
		self.fullStar = inFullStar;
		self.delegate = inDelegate;
	
		[self updateStars];
    }
    return self;
}
- (void)awakeFromNib
{
	numberOfStars = 5;

	[self updateStars];
	
	[super awakeFromNib];
}
- (void)dealloc
{
	emptyStar = nil;
	fullStar = nil;

}

#pragma mark accessor/mutators
- (void)setRating:(NSUInteger)newRating
{
	if (rating != newRating) {
		rating = newRating;
		[self updateStars];
		[self sendActionsForControlEvents:UIControlEventValueChanged];
	}
}
- (NSUInteger)rating
{
	return rating;
}

#pragma mark local methods
- (void)updateStars
{
	if (!fullStar || !emptyStar || rating == -1) {
		return;
	}

	NSArray *currentStars = [NSArray arrayWithArray:self.subviews];

	if ([currentStars count] != numberOfStars) {
		// remove all existing
		[currentStars makeObjectsPerformSelector:@selector(removeFromSuperview)];

		CGSize sizePerStar = fullStar.size;
		// NOTE commenting out the width stuff for now, it's only used to 
		// calculate the appropriate space between the stars
		// TODO make this configurable from the init
		//CGFloat totalWidth = sizePerStar.width * (CGFloat)numberOfStars;
		//CGFloat widthBetween = (self.bounds.size.width - edgeInsets.left - 
		//	edgeInsets.right - totalWidth) / (CGFloat)(numberOfStars - 1);
		
		CGFloat x = edgeInsets.left;
		CGFloat y = edgeInsets.top * (self.bounds.size.height - sizePerStar.height) / 2.0;

		for (int starIndex = 0; starIndex < numberOfStars; starIndex++) {
			UIImageView *starView = [[UIImageView alloc] initWithFrame:
				CGRectMake(x, y, sizePerStar.width, sizePerStar.height)];
			starView.userInteractionEnabled  = YES;
			starView.tag = starIndex;
			starView.layer.transform = CATransform3DIdentity;

			[self addSubview:starView];

			x += sizePerStar.width + 2;
		}
	}

	for (int starIndex = numberOfStars - 1; starIndex >= 0; starIndex--) {
		UIImageView *starView = [self.subviews objectAtIndex:starIndex];

		UIImage *newImage = nil;
		if (rating >= starIndex + 1) {
			newImage = fullStar;
		}
		else {
			newImage = emptyStar;
		}

		CAKeyframeAnimation *boundsOvershootAnimation = nil;

		if (starView.image && starView.image != newImage && animate) {
			// animate
			boundsOvershootAnimation = [CAKeyframeAnimation 
				animationWithKeyPath:@"transform"];

			CATransform3D startingScale = CATransform3DMakeScale(1, 1, 1);
			CATransform3D overshootScale = CATransform3DMakeScale(1.1, 1.1, 1);
			CATransform3D undershootScale = CATransform3DMakeScale(0.9, 0.9, 1);
			CATransform3D endingScale = CATransform3DIdentity;

			NSArray *boundsValues = [NSArray arrayWithObjects:
				[NSValue valueWithCATransform3D:startingScale],
				[NSValue valueWithCATransform3D:overshootScale],
				[NSValue valueWithCATransform3D:undershootScale],
				[NSValue valueWithCATransform3D:endingScale], nil];
			[boundsOvershootAnimation setValues:boundsValues];

			NSArray *times = [NSArray arrayWithObjects:
				[NSNumber numberWithFloat:0.0f],
				[NSNumber numberWithFloat:0.5f],
				[NSNumber numberWithFloat:0.9f],
				[NSNumber numberWithFloat:1.0f], nil];
			[boundsOvershootAnimation setKeyTimes:times];

			NSArray *timingFunctions = [NSArray arrayWithObjects:
				[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
				[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
				[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
				nil];
			[boundsOvershootAnimation setTimingFunctions:timingFunctions];
			boundsOvershootAnimation.fillMode = kCAFillModeForwards;
			boundsOvershootAnimation.removedOnCompletion = NO;
		}

		starView.image = newImage;

		if (boundsOvershootAnimation && animate) {
			[starView.layer addAnimation:boundsOvershootAnimation forKey:@"scale"];
		}
	}
}
- (void)updateStarsForTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGPoint locationInView = [touch locationInView:self];
	UIImageView *hitStar = (UIImageView *)[self hitTest:locationInView withEvent:event];

	NSUInteger oldRating = self.rating;
	if (hitStar && ((id)hitStar != (id)self)) {
		self.rating = hitStar.tag + 1;
	}
	else {
		if (locationInView.x <= edgeInsets.left) {
			self.rating = 0;
		}
		else if (locationInView.x >= self.bounds.size.width - edgeInsets.right) {
			self.rating = numberOfStars;
		}
	}

	[self.delegate starRatingControl:self didUpdateRating:self.rating fromRating:oldRating];
}

#pragma mark Touches
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self updateStarsForTouch:[touches anyObject] withEvent:event];
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self updateStarsForTouch:[touches anyObject] withEvent:event];
}

@end
