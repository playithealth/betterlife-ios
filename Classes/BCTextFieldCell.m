//
//  BCTextFieldCell.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/10/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCTextFieldCell.h"

#import "UIColor+Additions.h"

@implementation BCTextFieldCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
		self.bcTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 25)];
		self.bcTextLabel.textAlignment = NSTextAlignmentRight;
		self.bcTextLabel.adjustsFontSizeToFitWidth = YES;
		self.bcTextLabel.font = [UIFont boldSystemFontOfSize:14];
		self.bcTextLabel.textColor = [UIColor BC_nonEditableTextColor];
		self.bcTextLabel.backgroundColor = [UIColor BC_groupedTableViewCellBackgroundColor];
		[self.contentView addSubview:self.bcTextLabel];

		self.bcTextField = [[UITextField alloc] initWithFrame:CGRectMake(115, 12, 180, 25)];
		self.bcTextField.backgroundColor = [UIColor BC_groupedTableViewCellBackgroundColor];
		[self.contentView addSubview:self.bcTextField];
	}
	return self;
}

@end
