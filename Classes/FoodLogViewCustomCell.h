//
//  FoodLogViewCustomCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 12/14/2011.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodLogViewCustomCell : UITableViewCell { }

@property (strong, nonatomic) IBOutlet UIImageView *primaryImageView;
@property (strong, nonatomic) IBOutlet UILabel *primaryLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondaryLabel;
@property (strong, nonatomic) IBOutlet UILabel *tertiaryLabel;
@property (strong, nonatomic) IBOutlet UIView *topStripeView;
@property (strong, nonatomic) IBOutlet UIButton *playButton;

@end
