//
//  BLHeaderFooterWithSeparators.m
//  BettrLife
//
//  Created by Greg Goodrich on 6/27/14.
//  Copyright (c) 2014 BettrLife Corp. All rights reserved.
//

#import "BLHeaderFooterWithSeparators.h"

#import "UIColor+Additions.h"

@interface BLHeaderFooterWithSeparators ()
@property (assign, nonatomic) BOOL constraintsSet;
@end

@implementation BLHeaderFooterWithSeparators

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		_customLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		[self.contentView addSubview:_customLabel];

		// Default to black separators
		_separatorColor = [UIColor blackColor];
		self.contentView.backgroundColor = [UIColor BC_tableViewHeaderColor];

		_topSeparator = [[UIView alloc] initWithFrame:CGRectZero];
		_topSeparator.backgroundColor = _separatorColor;
		[self.contentView addSubview:_topSeparator];
		_bottomSeparator = [[UIView alloc] initWithFrame:CGRectZero];
		_bottomSeparator.backgroundColor = _separatorColor;
		[self.contentView addSubview:_bottomSeparator];

		[self setConstraints];
    }
    return self;
}

- (void)setSeparatorColor:(UIColor *)color
{
	_separatorColor = color;
	self.topSeparator.backgroundColor = _separatorColor;
	self.bottomSeparator.backgroundColor = _separatorColor;
}

- (void)setConstraints
{
	if (!self.constraintsSet) {
		CGFloat	separatorHeight = 1.0 / [[UIScreen mainScreen] scale];
		self.customLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.topSeparator.translatesAutoresizingMaskIntoConstraints = NO;
		self.bottomSeparator.translatesAutoresizingMaskIntoConstraints = NO;

		NSDictionary *bindingsDict = @{
			@"customLabel" : self.customLabel,
			@"topSeparator" : self.topSeparator,
			@"bottomSeparator" : self.bottomSeparator
		};

		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[customLabel]-|"
			options:0 metrics:nil views:bindingsDict]];
		[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeCenterY
			relatedBy:NSLayoutRelationEqual toItem:self.customLabel attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];

		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:
			[NSString stringWithFormat:@"V:|-0-[topSeparator(%f)]", separatorHeight]
			options:0 metrics:nil views:bindingsDict]];
		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[topSeparator]-0-|"
			options:0 metrics:nil views:bindingsDict]];
		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:
			[NSString stringWithFormat:@"V:[bottomSeparator(%f)]-0-|", separatorHeight] options:0 metrics:nil views:bindingsDict]];
		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottomSeparator]-0-|"
			options:0 metrics:nil views:bindingsDict]];

		self.constraintsSet = YES;
	}
}

@end
