//
//  NutritionCell.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 4/12/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NutritionCell : UITableViewCell {
    
    UIWebView *nutritionWebView;
}
@property (nonatomic, strong) IBOutlet UIWebView *nutritionWebView;

@end
