//
//  NSManagedObjectContext+Additions.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 2/22/14.
//  Copyright (c) 2014 BettrLife Corporation All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (BettrLife)
- (BOOL)BL_save; ///< Automatically logs using DDLogError
@end
