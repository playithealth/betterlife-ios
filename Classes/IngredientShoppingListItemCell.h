//
//  IngredientShoppingListItemCell.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 10/28/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IngredientShoppingListItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UIButton *quantityButton;
@property (strong, nonatomic) IBOutlet UILabel *quantityLabel;
@property (strong, nonatomic) IBOutlet UIButton *incrementButton;
@property (strong, nonatomic) IBOutlet UIButton *decrementButton;
@property (strong, nonatomic) IBOutlet UIButton *accessoryButton;
@property (strong, nonatomic) IBOutlet UIButton *clearButton;
@property (strong, nonatomic) IBOutlet UIButton *associateButton;

@end
