//
//  BCUserStoresViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/25/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCUserStoresViewController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
