//
//  BCSearchViewCell.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/26/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCSearchViewCell.h"

@implementation BCSearchViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
