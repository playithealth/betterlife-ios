//
//  BCUrlFactory.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/2/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kGet				@"GET"
#define kPost				@"POST"
#define kPut				@"PUT"
#define kDelete				@"DELETE"

@class CLLocation;

@interface BCUrlFactory : NSObject {

}

+ (NSString *)apiRootURLString;
+ (NSString *)apiVersionString;
+ (NSURL *)oauthConsumerURL;
+ (NSURL *)loginURL;
+ (NSURL *)inboxURL;
+ (NSURL *)messagesUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)messageUrlForMessage:(NSUInteger)messageId;
+ (NSURL *)communityURL;
+ (NSURL *)communityUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)threadsURL;
+ (NSURL *)threadsUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)threadMessagesURL;
+ (NSURL *)threadMessagesUrlNewerThan:(NSNumber *)timestamp emptyThreads:(NSString *)emptyThreads;
+ (NSURL *)shoppingListURL;
+ (NSURL *)shoppingListUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)evaluateURL;
+ (NSURL *)latestEvaluationsURLForStoreId:(NSNumber *)storeId;
+ (NSURL *)userEvaluationsURLForStoreId:(NSNumber *)storeId;
+ (NSURL *)evaluationsForItemId:(NSNumber *)itemId withTypeId:(NSNumber *)itemType;
+ (NSURL *)evaluationsForItemId:(NSNumber *)itemId withTypeName:(NSString *)itemType;
+ (NSURL *)purchaseHistoryURL;
+ (NSURL *)purchaseHistoryUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)purchaseHistoryWithRecommendations:(NSNumber *)timestamp;
+ (NSURL *)purchaseHistoryURLForProductId:(NSNumber *)productId limit:(NSNumber *)limit;
+ (NSURL *)purchaseHistoryURLForProductNamed:(NSString *)item limit:(NSNumber *)limit;
+ (NSURL *)entitiesURL;
+ (NSURL *)amenitiesURLForStoreId:(NSNumber *)storeId;
+ (NSURL *)userStoresUrl;
+ (NSURL *)searchStoresURLByZip:(NSString *)zip name:(NSString *)name showRows:(NSUInteger)rows rowOffset:(NSUInteger)offset;
+ (NSURL *)searchStoresURLByLocation:(CLLocation *)location name:(NSString *)name showRows:(NSUInteger)rows rowOffset:(NSUInteger)offset;
+ (NSURL *)quickListURL;
+ (NSURL *)quickListsAndItemsUrl;
+ (NSURL *)productsURL;
+ (NSURL *)productsURLInfoForProductId:(NSNumber *)productId;
+ (NSURL *)productsURLServingsForProductId:(NSNumber *)productId;
+ (NSURL *)productsURLWarningsForProductId:(NSNumber *)productId;
+ (NSURL *)productsURLNutritionForProductId:(NSNumber *)productId;
+ (NSURL *)productSearchURLForSearchTerm:(NSString *)searchTerm showRows:(NSUInteger)rows rowOffset:(NSUInteger)offset filters:(NSDictionary *)filters showOnlyFood:(BOOL)foodOnly;
+ (NSURL *)productSearchURLForUPC:(NSString *)upc;
+ (NSURL *)productSearchURLForBarcode:(NSString *)barcode andType:(NSString *)type;
+ (NSURL *)productSpeculateCategoryForFreetextItem:(NSString *)freetextItem;
+ (NSURL *)productRecommendationsURLShowHidden:(BOOL)showHidden;
+ (NSURL *)searchUrlForSearchTerm:(NSString *)searchTerm searchSections:(NSArray *)sections showRows:(NSUInteger)rows showOnlyFood:(BOOL)foodOnly;
+ (NSURL *)smartSearchURLForSearchTerm:(NSString *)searchTerm searchSections:(NSArray *)sections offset:(NSInteger)offset rows:(NSInteger)rows;
+ (NSURL *)unifiedSmartSearchURLForSearchTerm:(NSString *)searchTerm offset:(NSInteger)offset rows:(NSInteger)rows;
+ (NSURL *)activitySmartSearchURLForSearchTerm:(NSString *)searchTerm offset:(NSInteger)offset rows:(NSInteger)rows;
+ (NSURL *)imageURL;
+ (NSURL *)imageURLForImage:(NSNumber *)imageId imageSize:(NSString *)size;
+ (NSURL *)imageURLForProductId:(NSNumber *)productId imageSize:(NSString *)size;
+ (NSURL *)imageURLForCoachId:(NSNumber *)coachId imageSize:(NSString *)size;
+ (NSURL *)imageURLForUserId:(NSNumber *)userId inCommunity:(NSNumber *)communityId imageSize:(NSString *)size;
+ (NSURL *)categoriesURL;
+ (NSURL *)storeCategoriesUrl;
+ (NSURL *)storeCategoriesUrlForStoreId:(NSNumber *)storeId;
+ (NSURL *)pantryURL;
+ (NSURL *)pantryUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)singleUseURL;
+ (NSURL *)promotionsURL;
+ (NSURL *)promotionsSummariesForProductURL:(NSNumber *)productId;
+ (NSURL *)nutritionURL;
+ (NSURL *)nutritionForProductURL:(NSNumber *)productId;
+ (NSURL *)checkInUrl;
+ (NSURL *)mealsURL;
+ (NSURL *)mealsUrlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit;
+ (NSURL *)advisorMealPlansURL;
+ (NSURL *)advisorMealPlanTimelineURL; ///< GET to get the current timeline, PUT to start a plan, DELETE to stop a plan
+ (NSURL *)urlForMealPlanId:(NSNumber *)planId tagId:(NSNumber *)tagId sections:(NSNumber *)sections offset:(NSNumber *)offset limit:(NSNumber *)limit searchText:(NSString *)searchText;
+ (NSURL *)mealPlannerURL;
+ (NSURL *)mealPlannerURLNewerThan:(NSNumber *)timestamp;
+ (NSURL *)googleRecipeSearchForSearchTerm:(NSString *)searchTerms page:(NSUInteger)page includeDetails:(BOOL)includeDetails;
+ (NSURL *)recipeDetailsURLForRecipeId:(id)recipeId;
+ (NSURL *)publicRecipesForSearchTerm:(NSString *)searchTerm showRows:(NSUInteger)rows rowOffset:(NSUInteger)offset;
+ (NSURL *)accountURL;
+ (NSURL *)healthProfileURL;
+ (NSURL *)foodLogURL;
+ (NSURL *)foodLogURLstart:(NSNumber *)start end:(NSNumber *)end;
+ (NSURL *)foodLogURLNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit;
+ (NSURL *)loggingNotesURL;
+ (NSURL *)loggingNotesURLNewerThan:(NSNumber *)timestamp;
+ (NSURL *)mealPredictionsURL;
+ (NSURL *)mealSidePredictionsUrlForItemID:(NSNumber *)itemID itemType:(NSString *)itemType showDetails:(BOOL)showDetails;
+ (NSURL *)activityLogURL;
+ (NSURL *)activityLogUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)activityPlanURL;
+ (NSURL *)activityPlanUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)trainingActivityURL;
+ (NSURL *)trainingActivityUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)trainingAssessmentURL;
+ (NSURL *)trainingAssessmentUrlStart:(NSNumber *)start end:(NSNumber *)end;
+ (NSURL *)trainingAssessmentUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)trainingCardioURL;
+ (NSURL *)trainingCardioUrlStart:(NSNumber *)start end:(NSNumber *)end;
+ (NSURL *)trainingCardioUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)trainingStrengthURL;
+ (NSURL *)trainingStrengthUrlStart:(NSNumber *)start end:(NSNumber *)end;
+ (NSURL *)trainingStrengthUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)trackingURL;
+ (NSURL *)trackingUrlNewerThan:(NSNumber *)timestamp;
+ (NSURL *)trackingTypeURL;
+ (NSURL *)customFoodURL; ///< HTTP Put, Post, Delete
+ (NSURL *)customFoodURLNewerThan:(NSNumber *)timestamp; ///< HTTP Get
+ (NSURL *)restaurantChainURLNewerThan:(NSNumber *)timestamp; ///< HTTP Get
+ (NSURL *)restaurantChainURLSearch:(NSString *)search; ///< HTTP Get
+ (NSURL *)menuItemsURLForRestaurant:(NSNumber *)restaurantId; ///< HTTP Get
+ (NSURL *)menuItemsSearchURLForSearchTerm:(NSString *)searchTerm limit:(NSUInteger)limit offset:(NSUInteger)offset; ///<HTTP Get
+ (NSURL *)userProfileURL;
+ (NSURL *)userPermissionsURL;
+ (NSURL *)advisorsURL; ///< HTTP Get, Put, Post, Delete
+ (NSURL *)exerciseRoutinesURL;
+ (NSURL *)nutrientsURL;
+ (NSURL *)unitsURL;
+ (NSURL *)coachInvitesURL;
+ (NSURL *)activityPlanTimelineURL;
+ (NSURL *)advisorActivityPlansURL;
+ (NSURL *)syncCheckURLWithToken:(NSNumber *)syncToken; ///< HTTP Get
@end
