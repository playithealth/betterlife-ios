//
//  MealPlannerDetailViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/02/2012
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "MealPlannerDetailViewController.h"

#import "BCAppDelegate.h"
#import "BCDetailViewDelegate.h"
#import "BCImageCache.h"
#import "BCMoreLessFooter.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GenericValueDisplay.h"
#import "GTMHTTPFetcherAdditions.h"
#import "RecipeMO.h"
#import "MealIngredient.h"
#import "MealPlannerDay.h"
#import "RecipeTagMO.h"
#import "RecipeTagsViewController.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "UIImage+Additions.h"
#import "UserImageMO.h"
#import "User.h"

@interface MealPlannerDetailViewController () <BCMoreLessFooterDelegate, RecipeTagsViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *recipeImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *mealTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *servingsLabel;
@property (weak, nonatomic) IBOutlet UILabel *cookTimeLabel;

@property (strong, nonatomic) BCMoreLessFooter *footerView;

@property (weak, nonatomic) IBOutlet UITextField *caloriesTextField;
@property (weak, nonatomic) IBOutlet UITextField *caloriesFromFatTextField;
@property (weak, nonatomic) IBOutlet UITextField *saturatedFatCaloriesTextField;
@property (weak, nonatomic) IBOutlet UITextField *totalFatTextField;
@property (weak, nonatomic) IBOutlet UITextField *saturatedFatTextField;
@property (weak, nonatomic) IBOutlet UITextField *transFatTextField;
@property (weak, nonatomic) IBOutlet UITextField *polyunsaturatedFatTextField;
@property (weak, nonatomic) IBOutlet UITextField *monounsaturatedFatTextField;
@property (weak, nonatomic) IBOutlet UITextField *cholesterolTextField;
@property (weak, nonatomic) IBOutlet UITextField *sodiumTextField;
@property (weak, nonatomic) IBOutlet UITextField *potassiumTextField;
@property (weak, nonatomic) IBOutlet UITextField *totalCarbohydratesTextField;
@property (weak, nonatomic) IBOutlet UITextField *dietaryFiberTextField;
@property (weak, nonatomic) IBOutlet UITextField *solubleFiberTextField;
@property (weak, nonatomic) IBOutlet UITextField *insolubleFiberTextField;
@property (weak, nonatomic) IBOutlet UITextField *sugarsTextField;
@property (weak, nonatomic) IBOutlet UITextField *sugarAlcoholTextField;
@property (weak, nonatomic) IBOutlet UITextField *otherCarboydratesTextField;
@property (weak, nonatomic) IBOutlet UITextField *proteinTextField;
@property (weak, nonatomic) IBOutlet UITextField *vitaminATextField;
@property (weak, nonatomic) IBOutlet UITextField *vitaminCTextField;
@property (weak, nonatomic) IBOutlet UITextField *calciumTextField;
@property (weak, nonatomic) IBOutlet UITextField *ironTextField;

@property (weak, nonatomic) IBOutlet UITableViewCell *ingredientsCell;
@property (weak, nonatomic) IBOutlet UITextView *ingredientsTextView;
@property (weak, nonatomic) IBOutlet UITableViewCell *instructionsCell;
@property (weak, nonatomic) IBOutlet UITextView *instructionsTextView;
@property (weak, nonatomic) IBOutlet UILabel *tagsLabel;

@property (weak, nonatomic) IBOutlet UITextField *hiddenTextField;

@property (strong, nonatomic) NSArray *mealTimeNames;

@property (strong, nonatomic) NSNumberFormatter *decimalNumberFormatter;
@property (strong, nonatomic) NSNumberFormatter *integerNumberFormatter;

@end

@implementation MealPlannerDetailViewController

static const NSInteger kSectionDetail = 0;
static const NSInteger kSectionNutrition = 1;
static const NSInteger kSectionIngredients = 2;
static const NSInteger kSectionInstructions = 3;
static const NSInteger kSectionTags = 4;

static const NSInteger kRowDate = 1;
static const NSInteger kRowMealTime = 2;

static const NSInteger kTagDatePicker = 0;
//static const NSInteger kTagMealTimePicker = 1;

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		self.showAddButton = YES;
		self.mealTimeNames = @[@"Breakfast", @"Morning Snack", @"Lunch", @"Afternoon Snack", @"Dinner", @"Evening Snack"];
    }
    return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	DDLogVerbose(@"loading");

	if (self.mpd) {
		if (self.mpd.recipe) {
			self.nameLabel.text = self.mpd.recipe.name;
			self.navigationItem.title = self.mpd.recipe.name;
		}
		else {
			self.nameLabel.text = self.mpd.name;
			self.navigationItem.title = self.mpd.name;
		}

		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[dateFormatter setDateStyle:NSDateFormatterShortStyle];
		self.dateLabel.text = [dateFormatter stringFromDate:self.mpd.date];

		self.decimalNumberFormatter = [[NSNumberFormatter alloc] init];
		[self.decimalNumberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[self.decimalNumberFormatter setMinimumFractionDigits:1];
		[self.decimalNumberFormatter setMaximumFractionDigits:1];

		self.integerNumberFormatter = [[NSNumberFormatter alloc] init];
		[self.integerNumberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[self.integerNumberFormatter setMinimumFractionDigits:0];
		[self.integerNumberFormatter setMaximumFractionDigits:0];

		self.mealTimeLabel.text = self.mealTimeNames[self.mpd.logTime.integerValue];
		if ([self.mpd.recipe.serves integerValue] > 0) {
			self.servingsLabel.text = [NSString stringWithFormat:@"Serves: %@", self.mpd.recipe.serves];
		}
		else {
		}
		if ([self.mpd.recipe.cookTime integerValue] > 0 && [self.mpd.recipe.cookTimeUnit length] > 0) {
			self.cookTimeLabel.text = [NSString stringWithFormat:@"Cook time: %@ %@", self.mpd.recipe.cookTime, self.mpd.recipe.cookTimeUnit];
		}
		else {
			self.cookTimeLabel.text = nil;
		}
		
		if (self.mpd.recipe) {
			[self configureNutritionCells];

			if ([self.mpd.recipe.ingredients count] > 0) {
				[self configureIngredientsCell];
			}
			if ([self.mpd.recipe.instructions length] > 0) {
				[self configureInstructionsCellWithString:self.mpd.recipe.instructions];
			}

			self.tagsLabel.text = [self.mpd.recipe tagsString];

			if ([self.mpd.recipe.cloudId integerValue] > 0) {
				[self fetchRecipeDetailsForRecipeId:self.mpd.recipe.cloudId];
			}
			else if ([self.mpd.recipe.externalId length] > 0) {
				[self fetchRecipeDetailsForRecipeId:self.mpd.recipe.externalId];
			}
		}
		else {
			[self configureNutritionCells];
			
			self.caloriesTextField.enabled = NO;
			self.caloriesFromFatTextField.enabled = NO;
			self.saturatedFatCaloriesTextField.enabled = NO;
			self.totalFatTextField.enabled = NO;
			self.saturatedFatTextField.enabled = NO;
			self.transFatTextField.enabled = NO;
			self.polyunsaturatedFatTextField.enabled = NO;
			self.monounsaturatedFatTextField.enabled = NO;
			self.cholesterolTextField.enabled = NO;
			self.sodiumTextField.enabled = NO;
			self.potassiumTextField.enabled = NO;
			self.totalCarbohydratesTextField.enabled = NO;
			self.dietaryFiberTextField.enabled = NO;
			self.solubleFiberTextField.enabled = NO;
			self.insolubleFiberTextField.enabled = NO;
			self.sugarsTextField.enabled = NO;
			self.sugarAlcoholTextField.enabled = NO;
			self.otherCarboydratesTextField.enabled = NO;
			self.proteinTextField.enabled = NO;
			self.vitaminATextField.enabled = NO;
			self.vitaminCTextField.enabled = NO;
			self.calciumTextField.enabled = NO;
			self.ironTextField.enabled = NO;
		}
		if (!self.mpd.imageId && self.mpd.recipe.imageUrl) {
			// Special case for web recipes, should already be cached from the view that launched this view
			self.recipeImageView.image = [[BCImageCache sharedCache] imageWithImageURL:self.mpd.recipe.imageUrl];
		}
		else if (self.mpd.imageIdValue) {
			[self fetchImageWithImageId:self.mpd.imageId];
		}
		else {
			self.recipeImageView.image = [[BCImageCache sharedCache] stockImageWithItemType:self.mpd.itemType withSize:kImageSizeLarge];
		}
	}

	if (self.showAddButton) {
		UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addToMeals:)];
		self.navigationItem.rightBarButtonItem = addButton;
	}
	
	UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recipePictureTapped:)];
	[self.recipeImageView addGestureRecognizer:recognizer];
}

- (void)viewDidUnload
{
	[self setTagsLabel:nil];

	[super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowTags"]) {
		RecipeTagsViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;
		detailView.recipe = self.mpd.recipe;
	}
}

#pragma mark - local
- (NSString *)formatDecimalValue:(id)value withSuffix:(NSString *)suffix
{
	NSString *formatted = nil;

	if (value && ![value isKindOfClass:[NSNull class]]) {
		if (![value isKindOfClass:[NSNumber class]]) {
			value = [value numberValueDecimal];
		}
		formatted = [NSString stringWithFormat:@"%@%@%@", [self.decimalNumberFormatter stringFromNumber:value], [suffix length] ? @" " : @"", suffix];
	}
	else {
		formatted = @"-";
	}

	return formatted;
}

- (NSString *)formatIntegerValue:(id)value withSuffix:(NSString *)suffix
{
	NSString *formatted = nil;

	if (value && ![value isKindOfClass:[NSNull class]]) {
		if (![value isKindOfClass:[NSNumber class]]) {
			value = [value numberValueDecimal];
		}
		formatted = [NSString stringWithFormat:@"%@%@%@", [self.integerNumberFormatter stringFromNumber:value], [suffix length] ? @" " : @"", suffix];
	}
	else {
		formatted = @"-";
	}

	return formatted;
}

- (void)configureIngredientsCell
{
	NSArray *ingredients = [self.mpd.recipe.ingredients
		sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor
		sortDescriptorWithKey:@"sortOrder" ascending:YES]]];

	self.ingredientsTextView.text = [ingredients componentsJoinedByString:@"\n"];
}

- (void)configureInstructionsCellWithArray:(NSArray *)instructions
{
	self.instructionsTextView.text = [instructions componentsJoinedByString:@"\n"];
}

- (void)configureInstructionsCellWithString:(NSString *)instructions
{
	self.instructionsTextView.text = instructions;
}

- (void)configureNutritionCells
{
	if (self.mpd.recipe.nutrition) {
		self.caloriesTextField.text = [self formatIntegerValue:self.mpd.recipe.nutrition.calories withSuffix:@""];
		self.caloriesFromFatTextField.text = [self formatIntegerValue:self.mpd.recipe.nutrition.caloriesFromFat withSuffix:@""];
		self.saturatedFatCaloriesTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.saturatedFatCalories withSuffix:@"g"];
		self.totalFatTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.totalFat withSuffix:@"g"];
		self.saturatedFatTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.saturatedFat withSuffix:@"g"];
		self.transFatTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.transFat withSuffix:@"g"];
		self.polyunsaturatedFatTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.polyunsaturatedFat withSuffix:@"g"];
		self.monounsaturatedFatTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.monounsaturatedFat withSuffix:@"g"];
		self.cholesterolTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.cholesterol withSuffix:@"mg"];
		self.sodiumTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.sodium withSuffix:@"mg"];
		self.potassiumTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.potassium withSuffix:@"mg"];
		self.totalCarbohydratesTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.totalCarbohydrates withSuffix:@"g"];
		self.dietaryFiberTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.dietaryFiber withSuffix:@"g"];
		self.solubleFiberTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.solubleFiber withSuffix:@"g"];
		self.insolubleFiberTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.insolubleFiber withSuffix:@"g"];
		self.sugarsTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.sugars withSuffix:@"g"];
		self.sugarAlcoholTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.sugarsAlcohol withSuffix:@"g"];
		self.otherCarboydratesTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.otherCarbohydrates withSuffix:@"g"];
		self.proteinTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.protein withSuffix:@"g"];
		self.vitaminATextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.vitaminAPercent withSuffix:@"%"];
		self.vitaminCTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.vitaminCPercent withSuffix:@"%"];
		self.calciumTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.calciumPercent withSuffix:@"%"];
		self.ironTextField.text = [self formatDecimalValue:self.mpd.recipe.nutrition.ironPercent withSuffix:@"%"];
	}
	else if (self.mpd.recipe) {
		self.caloriesTextField.text = [self formatIntegerValue:self.mpd.recipe.calories withSuffix:@""];
		self.caloriesFromFatTextField.text = [self formatIntegerValue:self.mpd.recipe.caloriesFromFat withSuffix:@""];
		self.saturatedFatCaloriesTextField.text = @"-";
		self.totalFatTextField.text = [self formatDecimalValue:self.mpd.recipe.fat withSuffix:@"g"];
		self.saturatedFatTextField.text = [self formatDecimalValue:self.mpd.recipe.satfat withSuffix:@"g"];
		self.transFatTextField.text = @"-";
		self.polyunsaturatedFatTextField.text = @"-";
		self.monounsaturatedFatTextField.text = @"-";
		self.cholesterolTextField.text = [self formatDecimalValue:self.mpd.recipe.cholesterol withSuffix:@"mg"];
		self.sodiumTextField.text = [self formatDecimalValue:self.mpd.recipe.sodium withSuffix:@"mg"];
		self.potassiumTextField.text = @"-";
		self.totalCarbohydratesTextField.text = @"-";
		self.dietaryFiberTextField.text = @"-";
		self.solubleFiberTextField.text = @"-";
		self.insolubleFiberTextField.text = @"-";
		self.sugarsTextField.text = @"-";
		self.sugarAlcoholTextField.text = @"-";
		self.otherCarboydratesTextField.text = @"-";
		self.proteinTextField.text = @"-";
		self.vitaminATextField.text = @"-";
		self.vitaminCTextField.text = @"-";
		self.calciumTextField.text = @"-";
		self.ironTextField.text = @"-";
	}
	else {
		self.caloriesTextField.text = [self formatIntegerValue:self.mpd.nutrition.calories withSuffix:@""];
		self.caloriesFromFatTextField.text = [self formatIntegerValue:self.mpd.nutrition.caloriesFromFat withSuffix:@""];
		self.saturatedFatCaloriesTextField.text = [self formatDecimalValue:self.mpd.nutrition.saturatedFatCalories withSuffix:@"g"];
		self.totalFatTextField.text = [self formatDecimalValue:self.mpd.nutrition.totalFat withSuffix:@"g"];
		self.saturatedFatTextField.text = [self formatDecimalValue:self.mpd.nutrition.saturatedFat withSuffix:@"g"];
		self.transFatTextField.text = [self formatDecimalValue:self.mpd.nutrition.transFat withSuffix:@"g"];
		self.polyunsaturatedFatTextField.text = [self formatDecimalValue:self.mpd.nutrition.polyunsaturatedFat withSuffix:@"g"];
		self.monounsaturatedFatTextField.text = [self formatDecimalValue:self.mpd.nutrition.monounsaturatedFat withSuffix:@"g"];
		self.cholesterolTextField.text = [self formatDecimalValue:self.mpd.nutrition.cholesterol withSuffix:@"mg"];
		self.sodiumTextField.text = [self formatDecimalValue:self.mpd.nutrition.sodium withSuffix:@"mg"];
		self.potassiumTextField.text = [self formatDecimalValue:self.mpd.nutrition.potassium withSuffix:@"mg"];
		self.totalCarbohydratesTextField.text = [self formatDecimalValue:self.mpd.nutrition.totalCarbohydrates withSuffix:@"g"];
		self.dietaryFiberTextField.text = [self formatDecimalValue:self.mpd.nutrition.dietaryFiber withSuffix:@"g"];
		self.solubleFiberTextField.text = [self formatDecimalValue:self.mpd.nutrition.solubleFiber withSuffix:@"g"];
		self.insolubleFiberTextField.text = [self formatDecimalValue:self.mpd.nutrition.insolubleFiber withSuffix:@"g"];
		self.sugarsTextField.text = [self formatDecimalValue:self.mpd.nutrition.sugars withSuffix:@"g"];
		self.sugarAlcoholTextField.text = [self formatDecimalValue:self.mpd.nutrition.sugarsAlcohol withSuffix:@"g"];
		self.otherCarboydratesTextField.text = [self formatDecimalValue:self.mpd.nutrition.otherCarbohydrates withSuffix:@"g"];
		self.proteinTextField.text = [self formatDecimalValue:self.mpd.nutrition.protein withSuffix:@"g"];
		self.vitaminATextField.text = [self formatDecimalValue:self.mpd.nutrition.vitaminAPercent withSuffix:@"%"];
		self.vitaminCTextField.text = [self formatDecimalValue:self.mpd.nutrition.vitaminCPercent withSuffix:@"%"];
		self.calciumTextField.text = [self formatDecimalValue:self.mpd.nutrition.calciumPercent withSuffix:@"%"];
		self.ironTextField.text = [self formatDecimalValue:self.mpd.nutrition.ironPercent withSuffix:@"%"];
	}
}

- (void)fetchImageWithImageId:(NSNumber *)imageId
{
	if (imageId && [imageId integerValue] > 0) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory imageURLForImage:imageId imageSize:kImageSizeLarge]];

		[imageFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {            
			if (error) {
				if (error.code == 404) {
					DDLogInfo(@"missing image - imageId %@", imageId);

					[[BCImageCache sharedCache] setImage:[NSNull null] forImageId:imageId size:kImageSizeLarge];
				}
				else {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:imageFetcher retrievedData:retrievedData];
				}
			}
			else {
				id itemImage = [[UIImage alloc] initWithData:retrievedData];
				if (!itemImage) {
					itemImage = [NSNull null];
				}

				[[BCImageCache sharedCache] setImage:itemImage forImageId:imageId size:kImageSizeLarge];

				// Display the newly loaded image
				if (itemImage) {
					self.recipeImageView.image = itemImage;
				}
			}

			[appDelegate setNetworkActivityIndicatorVisible:NO];
		}];
	}
}

- (void)fetchRecipeDetailsForRecipeId:(id)recipeId
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory recipeDetailsURLForRecipeId:recipeId]];
	[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSDictionary *recipeDetailsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			NSSet *existingIngredients = self.mpd.recipe.ingredients;

			NSArray *ingredients = [recipeDetailsDict objectForKey:kRecipeIngredients];
			NSInteger sortOrder = 0;
			for (NSDictionary *ingredient in ingredients) {
				NSNumber *cloudId = [[ingredient valueForKey:kId] numberValueDecimal];

				// See if this ingredient has already been added
				BOOL (^SameCloudId)(id, BOOL *) = ^BOOL(id obj, BOOL *stop) {
					MealIngredient *ingr = obj;
					return ([ingr.cloudId isEqualToNumber:cloudId]);
				};

				NSSet *matches = [existingIngredients objectsPassingTest:SameCloudId];
				if (![matches count]) {
					MealIngredient *tempMealIngredient = [[MealIngredient alloc]
						initWithEntity:[MealIngredient entityInManagedObjectContext:self.mpd.managedObjectContext]
						insertIntoManagedObjectContext:self.mpd.managedObjectContext];
					[tempMealIngredient setWithRecipeDetailDictionary:ingredient];
					tempMealIngredient.sortOrder = @(sortOrder++);
					[self.mpd.recipe addIngredientsObject:tempMealIngredient];
				}
			}
			[self configureIngredientsCell];

			id instructions = [recipeDetailsDict objectForKey:kRecipeInstructions];
			if ([instructions isKindOfClass:[NSArray class]]) {
				self.mpd.recipe.instructions = [instructions componentsJoinedByString:@"\n"];
				[self configureInstructionsCellWithArray:instructions];
			}
			else if ([instructions isKindOfClass:[NSString class]]) {
				self.mpd.recipe.instructions = instructions;
				[self configureInstructionsCellWithString:instructions];
			}
			else {
				[self configureInstructionsCellWithArray:nil];
			}

			// do not stomp on the tags that are already here.
			NSMutableSet *tagsSet = [self.mpd.recipe.tags mutableCopy];
			for (NSString *tagName in [recipeDetailsDict objectForKey:kRecipeTags]) {
				RecipeTagMO *tag = [RecipeTagMO tagByName:tagName withManagedObjectContext:self.mpd.managedObjectContext];
				[tagsSet addObject:tag];
			}
			self.mpd.recipe.tags = tagsSet;
			self.tagsLabel.text = [self.mpd.recipe tagsString];

			if (!self.mpd.recipe.nutrition) {
				NutritionMO *nutrition = [NutritionMO insertInManagedObjectContext:self.mpd.managedObjectContext];

				[nutrition setWithUpdateDictionary:recipeDetailsDict];

				self.mpd.recipe.nutrition = nutrition;

				[self configureNutritionCells];
			}

			[self.tableView reloadData];
		}

		[appDelegate setNetworkActivityIndicatorVisible:NO];
	}];
}

- (void)moreLessFooterTapped:(BCMoreLessFooter *)footer
{
	NSArray *extraRows = @[
		[NSIndexPath indexPathForRow:1 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:2 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:3 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:4 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:5 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:6 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:7 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:8 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:9 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:10 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:11 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:12 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:13 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:14 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:15 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:16 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:17 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:18 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:19 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:20 inSection:kSectionNutrition],
		[NSIndexPath indexPathForRow:21 inSection:kSectionNutrition]];

	if ([self.footerView isCollapsed]) {
		// Show less
		[self.tableView beginUpdates];
		[self.tableView deleteRowsAtIndexPaths:extraRows withRowAnimation:UITableViewRowAnimationTop];
		[self.tableView endUpdates];
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:kSectionNutrition]
			atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}
	else {
		// Show more
		[self.tableView beginUpdates];
		[self.tableView insertRowsAtIndexPaths:extraRows withRowAnimation:UITableViewRowAnimationBottom];
		[self.tableView endUpdates];
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:21 inSection:kSectionNutrition]
			atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}

}

- (UIToolbar *)makeKeyboardToolbar
{
	CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
	UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, appFrame.size.width, 35)];
	toolbar.barStyle = UIBarStyleBlack;
	toolbar.translucent = YES;

	UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
		target:nil action:nil];

	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
		initWithTitle:@"Done" style:UIBarButtonItemStyleDone
		target:self action:@selector(done:)];

	toolbar.items = @[flexibleSpace, doneButton];

	return toolbar;
}

- (IBAction)done:(id)sender
{
	[self.hiddenTextField resignFirstResponder];
}

- (void)recipePictureTapped:(UITapGestureRecognizer *)recognizer
{
	UIActionSheet *actionSheet = [[UIActionSheet alloc]
		initWithTitle:@"Choose a source" 
		delegate:self
		cancelButtonTitle:@"Cancel" 
		destructiveButtonTitle:nil
		otherButtonTitles:@"Take a new picture", @"Choose from library", nil];
	[actionSheet showInView:self.view];
}

- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType 
{ 
	NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType]; 
	if ([UIImagePickerController isSourceTypeAvailable:sourceType] 
			&& [mediaTypes count] > 0) { 
		NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType]; 
		UIImagePickerController *picker = [[UIImagePickerController alloc] init]; 
		picker.mediaTypes = mediaTypes;
		picker.delegate = self;
		picker.allowsEditing = YES;
		picker.sourceType = sourceType;
		[self presentViewController:picker animated:YES completion:nil];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] 
			initWithTitle:@"Error accessing media"
			message:@"Device doesn't support that media source."
			delegate:self 
			cancelButtonTitle:@"OK"
			otherButtonTitles:nil];
		[alert show];
	}
}

- (void)addToMeals:(id)sender
{
	[self.mpd.managedObjectContext BL_save];

	[self.delegate view:self didUpdateObject:self.mpd];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;

	switch (section) {
		case kSectionDetail:
			numberOfRows = 3;
			break;
		case kSectionNutrition:
			if ([self.footerView isExpanded]) {
				numberOfRows = 22;
			}
			else {
				numberOfRows = 1;
			}
			break;
		case kSectionIngredients:
		case kSectionInstructions:
		case kSectionTags:
		{
			if (self.mpd.recipe) {
				numberOfRows = 1;
			}
			break;
		}
		default:
			break;
	}

	return numberOfRows;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat headerHeight = 0.0f;
	if (section == kSectionIngredients || section == kSectionInstructions || section == kSectionTags) {
		if (self.mpd.recipe) {
			headerHeight = 24.0f;
		}
	}
	return headerHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	CGFloat footerHeight = 0.0f;
	if (section == kSectionNutrition) {
		footerHeight = 34.0f;
	}
	return footerHeight;
}

- (UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = nil;
    switch (section) {
		case kSectionIngredients:
            sectionTitle = @"Ingredients";
            break;
		case kSectionInstructions:
            sectionTitle = @"Instructions";
            break;
		case kSectionTags:
            sectionTitle = @"Tags";
            break;
        case kSectionNutrition:
		case kSectionDetail:
        default:
            break;
    }
    
	return [BCTableViewController customViewForHeaderWithTitle:sectionTitle];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 44.0f;

	switch (indexPath.section) {
		case kSectionDetail:
			if (indexPath.row == 0) {
				rowHeight = 110.0f;
			}
			else {
				rowHeight = 44.0f;
			}
			break;
		case kSectionNutrition:
			if (indexPath.row == 0) {
				rowHeight = 84.0f;
			}
			else {
				rowHeight = 44.0f;
			}
			break;
		case kSectionIngredients:
		{
			if (self.ingredientsCell) {
				[self configureIngredientsCell];
				rowHeight = [self heightForTextView:self.ingredientsTextView];
			}
			break;
		}
		case kSectionInstructions:
		{
			if (self.instructionsCell) {
				[self configureInstructionsCellWithString:self.mpd.recipe.instructions];
				rowHeight = [self heightForTextView:self.instructionsTextView];
			}
			break;
		}
		case kSectionTags:
		default:
			rowHeight = 44.0f;
			break;
	}

    return rowHeight;
}

- (CGFloat)heightForTextView:(UITextView*)textView
{
    float horizontalPadding = 24;
    float verticalPadding = 16;
    float widthOfTextView = textView.contentSize.width - horizontalPadding;
    float height = [textView.text sizeWithFont:textView.font constrainedToSize:CGSizeMake(widthOfTextView, 999999.0f) lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
 
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.hiddenTextField resignFirstResponder];

	if (indexPath.section == kSectionDetail && indexPath.row == kRowDate) {
		UIDatePicker *aDatePicker = [[UIDatePicker alloc] init];
		aDatePicker.tag = kTagDatePicker;
		aDatePicker.datePickerMode = UIDatePickerModeDate;
		[aDatePicker setDate:self.mpd.date animated:NO];
		[aDatePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];

		self.hiddenTextField.inputView = aDatePicker;
		self.hiddenTextField.inputAccessoryView = [self makeKeyboardToolbar];

		[self.hiddenTextField becomeFirstResponder];
	}
	else if (indexPath.section == kSectionDetail && indexPath.row == kRowMealTime) {
		UIPickerView *aPickerView = [[UIPickerView alloc] init];
		aPickerView.showsSelectionIndicator = YES;
		aPickerView.dataSource = self;
		aPickerView.delegate = self;

		[aPickerView reloadAllComponents];

		[aPickerView selectRow:[self.mpd.logTime integerValue] inComponent:0 animated:NO];
		self.hiddenTextField.inputView = aPickerView;
		self.hiddenTextField.inputAccessoryView = [self makeKeyboardToolbar];

		[self.hiddenTextField becomeFirstResponder];
	}
}

- (UIView *)tableView:(UITableView *)theTableView viewForFooterInSection:(NSInteger)section
{
	UIImageView *sectionFooterView = nil;

	if (section == kSectionNutrition) {
		if (!self.footerView) {
			self.footerView = [[BCMoreLessFooter alloc] initWithDelegate:self];
		}

		sectionFooterView = self.footerView;
	}

	return sectionFooterView;
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [self.mealTimeNames count];
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	return self.mealTimeNames[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	self.mpd.logTime = @(row);
	self.mealTimeLabel.text = self.mealTimeNames[row];

	[self.mpd markForSync];

	// This seems hacky, but we're not going to save the MOC here, and that way, if we're in a scratch context,
	// we won't inadvertently save this item just by changing the meal time, as the delegate will save on the real context
	[self.delegate view:self didUpdateObject:self.mpd];
}

- (void)dateChanged:(UIDatePicker *)sender 
{
	self.mpd.date = sender.date;

	[self.mpd markForSync];

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	self.dateLabel.text = [dateFormatter stringFromDate:self.mpd.date];

	// This seems hacky, but we're not going to save the MOC here, and that way, if we're in a scratch context,
	// we won't inadvertently save this item just by changing the meal time, as the delegate will save on the real context
	[self.delegate view:self didUpdateObject:self.mpd];
}

#pragma mark - RecipeTagsViewDelegate
- (void)recipeTagsView:(RecipeTagsViewController *)recipeTagsView changedTags:(BOOL)changedTags
{
	if (changedTags) {
		self.tagsLabel.text = [self.mpd.recipe tagsString];
		[self.tableView reloadData];
	}

	// This seems hacky, but we're not going to save the MOC here, and that way, if we're in a scratch context,
	// we won't inadvertently save this item just by changing the meal time, as the delegate will save on the real context
	[self.delegate view:self didUpdateObject:self.mpd];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex != [actionSheet cancelButtonIndex]) {
		switch (buttonIndex) {
			case 0:
				[self getMediaFromSource:UIImagePickerControllerSourceTypeCamera];
				break;
			case 1:	
			default:
				[self getMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
				break;
		}		
	}
}

#pragma mark UIImagePickerController delegate methods
// TODO Should this method have a way of saving this image, even if the user cancels? Typically, this view controller is
// passed a scratch MOC, and thus the user could set the image, then cancel, and the image is gone
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info 
{
	User *thisUser = [User currentUser];
	UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage]; 

	UIImage *smallImage = [chosenImage BC_imageWithTargetSize:CGSizeMake(70, 70)];
	self.recipeImageView.image = smallImage;

	// create a 200x200 size image and save it to be uploaded
	UIImage *bigImage = [chosenImage BC_imageWithTargetSize:CGSizeMake(200, 200)];
	NSNumber *recipeId = nil;
	if ([self.mpd.recipe.cloudId integerValue] > 0) {
		recipeId = self.mpd.recipe.cloudId;
	}
	else if ([self.mpd.recipeId integerValue] > 0) {
		recipeId = self.mpd.recipeId;
	}
	UserImageMO *customImage = [UserImageMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K = %d AND %K = %@ AND %K = %@",
			UserImageMOAttributes.imageType, kUserImageTypeRecipe, UserImageMOAttributes.accountId, 
			thisUser.accountId, UserImageMOAttributes.imageId, recipeId]
		inContext:self.mpd.managedObjectContext];
	if (!customImage) {
		customImage = [UserImageMO insertInManagedObjectContext:self.mpd.managedObjectContext];
		customImage.accountId = thisUser.accountId;
		customImage.loginId = thisUser.loginId;
		customImage.imageType = @(kUserImageTypeRecipe);
		customImage.imageId = recipeId;
	}
	[customImage setUserImageWithUIImage:bigImage];
	[customImage markForSync];

	// This seems hacky, but we're not going to save the MOC here, and that way, if we're in a scratch context,
	// we won't inadvertently save this item just by changing the meal time, as the delegate will save on the real context
	[self.delegate view:self didUpdateObject:self.mpd];

	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker 
{ 
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
