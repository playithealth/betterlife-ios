//
//  BCTableViewCell
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/27/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "StarRatingControl.h"

@interface BCTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bcTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *bcDetailTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bcImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bcAlertImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bcLeafImageView;
@property (weak, nonatomic) IBOutlet UIButton *bcAccessoryButton;
@property (strong, nonatomic) IBOutlet StarRatingControl *ratingControl;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end
