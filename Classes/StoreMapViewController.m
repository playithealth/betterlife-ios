//
//  StoreMapViewController.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 5/28/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "StoreMapViewController.h"

#import "BCUtilities.h"
#import <QuartzCore/QuartzCore.h>
#import "NumberValue.h"
#import "StoreSearchViewController.h"
#import "StoreMapLocation.h"
#import "UIColor+Additions.h"


@implementation StoreMapViewController
@synthesize delegate;
@synthesize mapView;
@synthesize stores;
@synthesize startingLocation;

- (void)dealloc
{
	self.mapView.showsUserLocation = NO;
	self.mapView.delegate = nil;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

	self.mapView.delegate = self;

	double farthest = 0.0;
	NSInteger idx = 0;
	for (NSDictionary *storeDict in stores) {
		// TODO - account for favorite stores if the cloud is not available, different KVO mappings
		double latitude = [[[storeDict valueForKey:@"latitude"]
			numberValueWithNumberStyle:NSNumberFormatterDecimalStyle] doubleValue];
		double longitude = [[[storeDict valueForKey:@"longitude"]
			numberValueWithNumberStyle:NSNumberFormatterDecimalStyle] doubleValue] * -1;
		StoreMapLocation *mapLocation = [[StoreMapLocation alloc]
			initWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude)
			title:[storeDict valueForKey:@"store_name"]];
		mapLocation.subtitle = [storeDict valueForKey:@"address"];
		mapLocation.arrayIndex = idx;
		[self.mapView addAnnotation:mapLocation];

		double distance = [[[storeDict valueForKey:@"distcalc"]
			numberValueWithNumberStyle:NSNumberFormatterDecimalStyle] doubleValue];
		// Figure out which store is farthest from the current location to set the range
		// on the map
		if (distance > farthest) {
			farthest = distance;
		}

		idx++;
	}

	double distCalc = (farthest * kMetersPerMile);

	[self.mapView setRegion: MKCoordinateRegionMakeWithDistance([self.startingLocation
		coordinate], 1.5 * distCalc, distCalc)];
}

- (void)viewDidUnload
{
	self.mapView.showsUserLocation = NO;
	self.mapView.delegate = nil;
    [self setMapView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - StoreSelectionDelegate
- (void)didSelectStore:(id)sender
{
	if (self.delegate != nil 
			&& [self.delegate conformsToProtocol:@protocol(StoreSelectionDelegate)]) {

		[self.delegate view:self didSelectStore:[self.stores objectAtIndex:[sender tag]]];
	}
}

#pragma mark - MKMapView delegate

- (MKAnnotationView *)mapView:(MKMapView *)localMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
	MKPinAnnotationView *annView = nil;

	// Only return an MKAnnotationView if this is one of our store map locations, this allows the user's
	// location to remain as the blue sphere with the radiating circle
	if ([annotation isMemberOfClass:[StoreMapLocation class]]) {
		StoreMapLocation *storeMapLocation = annotation;
		annView = (MKPinAnnotationView *)[localMapView
			dequeueReusableAnnotationViewWithIdentifier:@"store"];
		if (!annView) {
			annView = [[MKPinAnnotationView alloc] initWithAnnotation:storeMapLocation reuseIdentifier:@"store"];
			UIButton *calloutButton = [UIButton buttonWithType:UIButtonTypeCustom];
			[calloutButton setImage:[UIImage imageNamed:@"checkin-ico.png"] forState:UIControlStateNormal];
			calloutButton.showsTouchWhenHighlighted = YES;
			calloutButton.frame = CGRectMake(0, 0, 32, 32);
			CAGradientLayer *gradient = [CAGradientLayer layer];
			gradient.frame = calloutButton.bounds;
			gradient.colors = [NSArray arrayWithObjects:
				(id)[[UIColor BC_colorWithRed:128 green:172 blue:105] CGColor],
				(id)[[UIColor BC_colorWithRed:60 green:130 blue:25] CGColor],
				(id)[[UIColor BC_colorWithRed:40 green:118 blue:0] CGColor],
				(id)[[UIColor BC_colorWithRed:39 green:113 blue:0] CGColor],
				nil];
			gradient.locations = [NSArray arrayWithObjects:
				[NSNumber numberWithFloat:0.0], 
				[NSNumber numberWithFloat:0.52], 
				[NSNumber numberWithFloat:0.52], 
				[NSNumber numberWithFloat:1.0], 
				nil];	
			gradient.cornerRadius = 7.0f;
			[calloutButton.layer insertSublayer:gradient atIndex:0];
			[calloutButton bringSubviewToFront:calloutButton.imageView];
			[calloutButton addTarget:self action:@selector(didSelectStore:)
				forControlEvents:UIControlEventTouchUpInside];
			annView.rightCalloutAccessoryView = calloutButton;
			// Is this correct? If this works like cellforrow in a tableview, 
			// you can't release this because it gets handled by the system
			// might cause an overrelease...
			//[annView autorelease];
		}
		annView.rightCalloutAccessoryView.tag = storeMapLocation.arrayIndex;

		annView.pinColor = MKPinAnnotationColorGreen;
		annView.animatesDrop=TRUE;
		annView.canShowCallout = YES;
	}

    return annView;
}


 - (void)mapView:(MKMapView *)localMapView didUpdateUserLocation:(MKUserLocation *)userLocation
 {
	if ([[localMapView userLocation] location]) {
		[localMapView setCenterCoordinate:[[[localMapView userLocation] location] coordinate]];
	}
 }
@end
