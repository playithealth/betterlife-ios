//
//  PantryViewCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/25/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CustomBadge.h"

@interface PantryViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *quantityButton;
@property (weak, nonatomic) IBOutlet UIButton *incrementButton;
@property (weak, nonatomic) IBOutlet UIButton *decrementButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) CustomBadge *shopQuantityBadge;

@end
