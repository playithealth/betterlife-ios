//
//  BCLoggingSearchRecentViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/9/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCLoggingSearchRecentViewController.h"

#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCProgressHUD.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "FoodLogMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "NSCalendar+Additions.h"
#import "RecipeMO.h"
#import "UIImage+Additions.h"
#import "UITableView+DownloadImage.h"
#import "User.h"

@interface BCLoggingSearchRecentViewController ()
@property (strong, nonatomic) NSMutableArray *recentMeals;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@end

@implementation BCLoggingSearchRecentViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
    if (self) {
		self.dateFormatter = [[NSDateFormatter alloc] init];
		[self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	[[NSNotificationCenter defaultCenter] addObserverForName:[FoodLogMO entityName] object:nil queue:nil
		usingBlock:^(NSNotification *notification) {
			_recentMeals = nil;
		}];
}

- (void)viewDidAppear:(BOOL)animated
{
    if ([self.recentMeals count]) {
		[self loadImagesForOnscreenRows];
	}

	[super viewDidAppear:animated];
}

- (void)viewWillDisappearAnimated:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - local
- (NSArray *)recentMeals
{
	if (_recentMeals) {
		return _recentMeals;
	}

	NSMutableArray *temp = [NSMutableArray array];

	NSFetchRequest *request = [FoodLogMO MR_createFetchRequestInContext:self.managedObjectContext];
	[request setFetchBatchSize:20];

	request.predicate = [NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@ AND %K <> %@ AND %K <> %@ AND %K <> %@",
		FoodLogMOAttributes.loginId, [[User currentUser] loginId],
		FoodLogMOAttributes.logTime, self.logTime,
		FoodLogMOAttributes.itemType, @"calories",
		FoodLogMOAttributes.itemType, @"water",
		FoodLogMOAttributes.status, kStatusDelete];
	
    // sort by item name
    NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:FoodLogMOAttributes.date ascending:NO];
	NSSortDescriptor *sortByCreationDate = [NSSortDescriptor sortDescriptorWithKey:FoodLogMOAttributes.creationDate ascending:YES];
    
    [request setSortDescriptors:@[sortByDate, sortByCreationDate]];

	NSDate *lastDate = nil;
	NSInteger mealCount = 0;
	NSArray *recentLogs = [self.managedObjectContext executeFetchRequest:request error:nil];
	for (FoodLogMO *log in recentLogs) {
		if ([log.date isEqualToDate:lastDate]) {
			// grab the last item in the array
			NSMutableDictionary *mealDict = [temp lastObject];

			// add this log to the embedded array
			NSMutableArray *logs = mealDict[@"logs"];
			[logs addObject:log];

			// remake the name string
			NSSet *logNames = [logs valueForKeyPath:FoodLogMOAttributes.name];
			mealDict[@"name"] = [[logNames allObjects] componentsJoinedByString:@", "];
		}
		else if (mealCount >= 30) {
			break;
		}
		else {
			lastDate = log.date;
			mealCount++;

			// create a new record
			NSMutableDictionary *mealDict = [NSMutableDictionary dictionary];

			// give it a name
			mealDict[@"name"] = log.name;

			// set the date string
			mealDict[@"date"] = [self.dateFormatter stringFromDate:log.date];

			// create the embedded array
			NSMutableArray *logs = [NSMutableArray array];
			[logs addObject:log];
			mealDict[@"logs"] = logs;

			// add this to the outer array
			[temp addObject:mealDict];
		}
	}

	_recentMeals = temp;

	return _recentMeals;
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];

	for (NSIndexPath *indexPath in visiblePaths) {
		// get the meal at this indexPath
		NSDictionary *recentMeal = self.recentMeals[indexPath.row];

		// get the embedded array
		NSArray *mealLogs = recentMeal[@"logs"];

		// use the first item 
		FoodLogMO *foodLog = mealLogs[0];

		if ([foodLog.imageId integerValue] > 0) {
			[self.tableView downloadImageWithImageId:foodLog.imageId forIndexPath:indexPath withSize:kImageSizeMedium];
		}
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	NSUInteger count = [self.recentMeals count];

	if (!count) {
		// Display the 'empty' message
		count = 1;
	}

    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *basicCellID = @"BasicCell";
    static NSString *firstTimeCellID = @"FirstTimeCell";
    UITableViewCell *cell = nil;

	if ([self.recentMeals count]) {
		// Normal row
		cell = [tableView dequeueReusableCellWithIdentifier:basicCellID forIndexPath:indexPath];

		[self configureCell:cell atIndexPath:indexPath];
	}
	else {
		// 'Empty' message row
		cell = [tableView dequeueReusableCellWithIdentifier:firstTimeCellID forIndexPath:indexPath];
	}
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	// get the meal at this indexPath
	NSDictionary *recentMeal = self.recentMeals[indexPath.row];

	// get the embedded array
	NSArray *mealLogs = recentMeal[@"logs"];

	// use the first item 
	FoodLogMO *entreeLog = mealLogs[0];

	UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:100];

	UILabel *textLabel = (UILabel *)[cell.contentView viewWithTag:101];
	textLabel.text = recentMeal[@"name"];

	UILabel *detailTextLabel = (UILabel *)[cell.contentView viewWithTag:102];
	detailTextLabel.text = [NSString stringWithFormat:@"%@ (%lu items)", recentMeal[@"date"], (unsigned long)[mealLogs count]];

	imageView.image = [[BCImageCache sharedCache] imageWithImageId:entreeLog.imageId withItemType:entreeLog.itemType
		withSize:kImageSizeMedium];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 0.0f;

	static UITableViewCell *cell; 
	if ([self.recentMeals count]) {
		if (cell == nil) {
			cell = [tableView dequeueReusableCellWithIdentifier: @"BasicCell"];
		}
		[self configureCell:cell atIndexPath:indexPath];
		height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1.0f;
	}
	else {
		// 'Empty' message
		height = self.tableView.bounds.size.height - self.tableView.contentInset.top - self.tableView.contentInset.bottom;
	}

	return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// get the meal at this indexPath
	NSDictionary *recentMeal = self.recentMeals[indexPath.row];

	// get the embedded array
	NSArray *mealLogs = recentMeal[@"logs"];

	for (FoodLogMO *original in mealLogs) {
		FoodLogMO *newFoodLog = [FoodLogMO insertFoodLogWithFoodLog:original inContext:self.managedObjectContext];
		newFoodLog.date = self.logDate;
		newFoodLog.logTime = self.logTime;
	}
	// Save them
	[self.managedObjectContext BL_save];

	NSString *labelText = [NSString stringWithFormat:@"%lu items added to your log", (unsigned long)[mealLogs count]];

	[BCProgressHUD notificationWithText:labelText onView:self.view.superview];

	// Deselect this cell now that we're done, use an animation to make this not abrupt
	[[tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - BLLoggingFoodSearchDelegate
- (void)didUpdateLoggedFoods
{
	[self.tableView reloadData];
}

@end
