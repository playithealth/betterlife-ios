//
//  BCAppDelegate.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "BCAppDelegate.h"

#import "BCDashboardViewController.h"
#import "BCPermissionsDataModel.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BLMigrationViewController.h"
#import "BLOnboardWelcomeViewController.h"
#import "CoachInviteMO.h"
#import "LoginViewController.h"
#import "MBProgressHUD.h"
#import "OAuthConsumer.h"
#import "PantryViewController.h"
#import "PromotionsViewController.h"
#import "ShoppingListViewController.h"
#import "Store.h"
#import "SyncUpdateMO.h"
#import "TaskQueueItems.h"
#import "TestFlight.h"
#import "UIColor+Additions.h"
#import "User.h"
#import "UserProfileMO.h"

int ddLogLevel;

@interface BCAppDelegate ()
- (void)syncCheckFinished:(NSNotification *)notification;
@end

@implementation BCAppDelegate
@synthesize window = _window;

static const NSInteger kTagFailedCoreDataReset = 1;
static const NSInteger kTagAuthenticationFalure = 2;
static NSInteger numberOfActivities = 0;

- (void)updateConnectedWithReachability:(Reachability *)curReach
{
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    switch ([curReach currentReachabilityStatus])
	{
		default:
		case NotReachable:
			{
				self.connected = NO;
				break;
			}

		case ReachableViaWWAN:
		case ReachableViaWiFi:
			{
				self.connected = YES;
				break;
			}
	}
}

// Called by Reachability whenever status changes.
- (void)reachabilityChanged:(NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
	[self updateConnectedWithReachability:curReach];
}

#pragma mark - Application lifecycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
{
	[[UINavigationBar appearance] setTintColor:[UIColor BC_mediumLightGreenColor]];
	[[UIToolbar appearance] setTintColor:[UIColor BC_mediumLightGreenColor]];
	[[UISegmentedControl appearance] setTintColor:[UIColor BC_mediumLightGreenColor]];

	// a more simply formatted log message, includes the log level as a single letter and no timestamp
	BCDebugFormatter *formatter = [[BCDebugFormatter alloc] init];
	[[DDTTYLogger sharedInstance] setLogFormatter:formatter];
	
#ifdef DEBUG
	// if we are debugging show everything
	// LOG_LEVELs ERROR, WARN, VERBOSE, DEBUG, INFO
	// Flags are LOG_FLAG_IMAGE_CACHE, LOG_FLAG_SYNC
	ddLogLevel = LOG_LEVEL_DEBUG;
	// Direct log messages to the console.
	[DDLog addLogger:[DDTTYLogger sharedInstance]];
	
	[[DDTTYLogger sharedInstance] setColorsEnabled:YES];
	[[DDTTYLogger sharedInstance] setForegroundColor:[UIColor BC_goldColor] backgroundColor:nil forFlag:LOG_FLAG_INFO];
	[[DDTTYLogger sharedInstance] setForegroundColor:[UIColor BC_blueColor] backgroundColor:nil forFlag:LOG_FLAG_DEBUG];
#else
	// otherwise, show only warnings and errors
	// and only use the file logger
	ddLogLevel = LOG_LEVEL_WARN;
		
	[TestFlight takeOff:@"bf0429a4-9818-4fc9-98ff-dd6506b3d31c"];
#endif
	
	self.fileLogger = [[DDFileLogger alloc] init];
	
	// Configure some sensible defaults for an iPhone application.
	// Roll the file when it gets to be 512 KB or 24 Hours old (whichever comes first).
	// Also, only keep up to 4 archived log files around at any given time.
	// We don't want to take up too much disk space.
	self.fileLogger.maximumFileSize = 1024 * 512;    // 512 KB
	self.fileLogger.rollingFrequency = 60 * 60 * 24; //  24 Hours
	self.fileLogger.logFileManager.maximumNumberOfLogFiles = 4;
	
	// Add our file logger to the logging system.
	[DDLog addLogger:self.fileLogger];
	
	self.connected = YES;

    // Observe the kNetworkReachabilityChangedNotification. When that notification is 
	// posted, the method "reachabilityChanged" will be called. 
    [[NSNotificationCenter defaultCenter] addObserver:self 
		selector:@selector(reachabilityChanged:) 
		name:kReachabilityChangedNotification object:nil];

	// Track the CoachInviteMO changes so that we can alert the user in the event of a new invitation arriving
	[[NSNotificationCenter defaultCenter] addObserverForName:[CoachInviteMO entityName] object:nil queue:nil
		usingBlock:^(NSNotification *notification) {
			if ([[notification userInfo] objectForKey:kFirstTimeInvites]) {
				UIAlertView *alert = [[UIAlertView alloc] 
					initWithTitle:@"Coach Invitation"
					message:@"You have received one or more coach invitations. Please access them via the sidebar menu."
					delegate:nil
					cancelButtonTitle:@"OK"
					otherButtonTitles:nil];
				[alert show];
			}
		}];

    //Change the host name here to change the server your monitoring
	self.hostReach = [Reachability reachabilityWithHostName:[[NSURL URLWithString:[BCUrlFactory apiRootURLString]] host]];
	//self.hostReach = [[Reachability reachabilityWithHostName:[BCUrlFactory apiRootURLString]] retain];
	[self.hostReach startNotifier];
	
    self.internetReach = [Reachability reachabilityForInternetConnection];
	[self.internetReach startNotifier];

    self.wifiReach = [Reachability reachabilityForLocalWiFi];
	[self.wifiReach startNotifier];

	// turn on auto-refresh by default, with 2 minutes as the interval
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSDictionary *appDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
		@"YES", kAutoRefreshEnabledSetting,
		[NSNumber numberWithInteger:120], kAutoRefreshIntervalSetting,
		[NSNumber numberWithInteger:0], kShoppingListSortSetting,
		@(YES), kSettingShowLoggingPredictions,
		nil];
	[defaults registerDefaults:appDefaults];

	self.facebook = [[Facebook alloc] initWithAppId:kFacebookAppId];
	
	// If the expected store doesn't exist, copy the default store.
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
	NSString *storePath = [documentsPath stringByAppendingPathComponent:@"BettrLife.sqlite"];
	//[[NSUserDefaults standardUserDefaults] removeObjectForKey:kSettingDBRefreshBuild];
	const NSUInteger refreshForBuild = 198;
	NSUInteger lastRefreshBuild = [[NSUserDefaults standardUserDefaults] integerForKey:kSettingDBRefreshBuild];
	// If we don't already have a db, or we need to re-sync the entire db (determined by whether we've refreshed since the target build)
	if (![fileManager fileExistsAtPath:storePath] || (lastRefreshBuild < refreshForBuild)) {
		NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:@"BettrLife" ofType:@"sqlite"];
		if (defaultStorePath) {
			NSError *error = nil;
			if ([fileManager fileExistsAtPath:storePath]) {
				// First remove all db related files
				[fileManager removeItemAtPath:storePath error:nil];
				[fileManager removeItemAtPath:[documentsPath stringByAppendingPathComponent:@"BettrLife.sqlite-shm"] error:nil];
				[fileManager removeItemAtPath:[documentsPath stringByAppendingPathComponent:@"BettrLife.sqlite-wal"] error:nil];
			}
			if ([fileManager copyItemAtPath:defaultStorePath toPath:storePath error:&error]) {
				[[NSUserDefaults standardUserDefaults] setInteger:refreshForBuild forKey:kSettingDBRefreshBuild];
			}
		}
	}

	__typeof__(self) __weak weakSelf = self;
	dispatch_async(dispatch_get_main_queue(), ^{
		[MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"BettrLife.sqlite"];
		weakSelf.adManagedObjectContext = [NSManagedObjectContext MR_rootSavingContext];

		// if we have a valid user and consumer, start the sync
		if ([[User currentUser] loginIsValid] && [[OAuthConsumer currentOAuthConsumer] consumerIsValid]) {
			DDLogInfo(@"Valid login at %@ - start sync", THIS_METHOD);
			[self performInitialSync];
		}
		else { // else force the login view
			DDLogInfo(@"Invalid login at %@ - goto login, do not pass go", THIS_METHOD);
			[weakSelf gotoLogin];
		}
	});

	return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */

	// turn off the syncCheck
	[self stopSyncCheck];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes
	 made on entering the background.
     */

	// if we have a valid user and consumer, restart the sync
	if ([[User currentUser] loginIsValid] && [[OAuthConsumer currentOAuthConsumer] consumerIsValid]) {
		DDLogInfo(@"Valid login at %@ - start sync", THIS_METHOD);
		[self startSyncCheck];
	}
	else { // else force the login view
		DDLogInfo(@"Invalid login at %@ - goto login, do not pass go", THIS_METHOD);
		[self gotoLogin];
	}

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */

}

- (void)applicationWillTerminate:(UIApplication *)application
{
	[MagicalRecord cleanUp];
}

// This works based upon Restoration IDs in the storyboard editor
-(BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
	return YES;
}

-(BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
	return YES;
}

#pragma mark - Memory management
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

#pragma mark - Local Methods
- (BOOL)resetCoreData
{
	NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
	NSString *storePath = [documentsPath stringByAppendingPathComponent:@"BettrLife.sqlite"];
    NSURL *storeURL = [NSURL fileURLWithPath:storePath];

	NSPersistentStore *currentStore = [[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] persistentStoreForURL:storeURL];

	// remove the current store
    NSError *error = nil;
	if (![[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] removePersistentStore:currentStore error:&error]) {
		UIAlertView *alert = [[UIAlertView alloc] 
			initWithTitle:@"Datastore Error"
			message:@"Failed to remove the persistent data store.\rTry restarting the application"
			delegate:nil
			cancelButtonTitle:@"OK"
			otherButtonTitles:nil];
		alert.tag = kTagFailedCoreDataReset;
		[alert show];

        DDLogError(@"Unresolved error trying to delete persistent store %@, %@", error, [error userInfo]);
		return NO;
	}

	NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager removeItemAtPath:storeURL.path error:&error];

	[MagicalRecord cleanUp];

	// copy the default store 
	NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:@"BettrLife" ofType:@"sqlite"];
	if (defaultStorePath) {
		[fileManager copyItemAtPath:defaultStorePath toPath:storePath error:NULL];
	}

	[MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"BettrLife.sqlite"];
	self.adManagedObjectContext = [NSManagedObjectContext MR_rootSavingContext];

	return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url 
{
    return [self.facebook handleOpenURL:url]; 
}

- (void)showSidebar:(BOOL)show disableClosing:(BOOL)disableClosing
{
	if (!self.sidebarView) {
		UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
		self.sidebarView = [navController.storyboard instantiateViewControllerWithIdentifier:@"Sidebar"];
		self.sidebarView.managedObjectContext = self.adManagedObjectContext;
		[self.window insertSubview:self.sidebarView.view atIndex:0];
	}
	
	self.sidebarView.disableClosing = disableClosing;

	__weak BCAppDelegate *weakself = self;
    
	if (show) {
		// Ensure that when showing the sidebar, the keyboard gets hidden (if it is open)
		[self.sidebarView becomeFirstResponder];
		[self.sidebarView updateBadges];
		self.sidebarView.view.hidden = NO;
		[UIView animateWithDuration:0.3f
			animations:^{
				UINavigationController *navController = (UINavigationController *)weakself.window.rootViewController;
				CGRect rect = navController.view.frame;
				rect.origin.x = weakself.sidebarView.tableView.frame.size.width;
				navController.view.frame = rect;
			}
			completion:^(BOOL finished){
				if (finished) {
					[weakself.window bringSubviewToFront:weakself.sidebarView.view];
				}
			}
		];
	}
	else {
		[self.window sendSubviewToBack:weakself.sidebarView.view];
		[UIView animateWithDuration:0.3f
			animations:^{
				UINavigationController *navController = (UINavigationController *)weakself.window.rootViewController;
				CGRect rect = navController.view.frame;
				rect.origin.x = 0;
				navController.view.frame = rect;
			}
			completion:^(BOOL finished){
			   if (finished) {
				   weakself.sidebarView.view.hidden = YES;
			   }
			}
		];
	}
}

- (void)performInitialSync
{
	if (![SyncUpdateMO hasPerformedInitialSync:[User loginId] inMOC:self.adManagedObjectContext]) {
		DDLogDebug(@"Performing initial sync");
		// Ensure that we handle the interval sync
		[[NSNotificationCenter defaultCenter] addObserver:self 
			selector:@selector(syncCheckFinished:) 
			name:@"syncCheck" object:nil];
		// If we're not on the migration/updating page, go there
		UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
		BLMigrationViewController *viewController = (BLMigrationViewController*)navController.topViewController;
		if (![viewController isKindOfClass:[BLMigrationViewController class]]) {
			viewController = (BLMigrationViewController *)[self gotoViewWithStoryboardId:@"Migration" inStoryboardNamed:@"Main"];
		}
		__typeof__(self) __weak weakSelf = self;
		BCObjectManager *manager = [BCObjectManager syncCheck:SendAndReceive inMOC:self.adManagedObjectContext
			withCompletionBlock:^{
				// NOTE: This block will not get called if there is an authentication failure inside the sync
				// Ask the sidebar to refresh itself
				[self.sidebarView userLoggedIn];

				[weakSelf gotoInitialViewController];
			}];
		[viewController showUpdateNotificationsFrom:manager];
	}
	else {
		DDLogDebug(@"Skipping initial sync, already completed");
		// Ask the sidebar to refresh itself
		[self.sidebarView userLoggedIn];

		[self gotoInitialViewController];
		[self startSyncCheck];
	}

}

- (void)syncCheckFinished:(NSNotification *)notification
{
	DDLogSync(@"Interval syncCheck Finished");
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"syncCheck" object:nil];

	if ([[NSUserDefaults standardUserDefaults] boolForKey:kAutoRefreshEnabledSetting]) {
		NSUInteger interval = [[NSUserDefaults standardUserDefaults] integerForKey:kAutoRefreshIntervalSetting];
		[self performSelector:@selector(startSyncCheck) withObject:nil afterDelay:interval];
	}
}

- (void)startSyncCheck
{
	DDLogSync(@"Firing interval syncCheck");
	// Be sure to synchronize the NSUserDefaults here, as we may be getting woken up, and if the user just changed
	// a setting, then they may not yet be in our NSUserDefaults cache
	[[NSUserDefaults standardUserDefaults] synchronize];

	[BCObjectManager syncCheck:SendAndReceive];

	[[NSNotificationCenter defaultCenter] addObserver:self 
		selector:@selector(syncCheckFinished:) 
		name:@"syncCheck" object:nil];
}

- (void)stopSyncCheck
{
	DDLogSync(@"Stopping interval syncCheck");

	// Cancel any queues that may be out there
	[TaskQueueItems cancelAllQueues];

	// Turn off the sync check timer
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startSyncCheck) object:nil];

	// Ensure the network activity indicator is reset and not being shown, we may have canceled tasks in the middle of an operation
	[self resetNetworkActivityIndicator];
}

- (void)resetNetworkActivityIndicator {
	numberOfActivities = 0;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)setNetworkActivityIndicatorVisible:(BOOL)visible
{
	if (visible) {
		numberOfActivities++;
	}
	else {
		numberOfActivities--;
	}

	if (numberOfActivities < 0) {
		DDLogWarn(@"Network Activity Indicator was asked to hide more often than shown");
	}
    
    // Display the indicator as long as our static counter is > 0.
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:(numberOfActivities > 0)];
}

- (UIViewController *)gotoInitialViewController
{
	// Check to see if they've not completed onboarding, and they have permissions to see at least part of it, then take them there first
	UserProfileMO *userProfileMO = [UserProfileMO getCurrentUserProfileInMOC:self.adManagedObjectContext];
	if (![userProfileMO.onboardingComplete boolValue] && [BLOnboardWelcomeViewController canUseOnboarding]) {
		return [self gotoInitialViewControllerInStoryboardNamed:@"Onboarding"];
	}
	else if (![[NSUserDefaults standardUserDefaults] boolForKey:kSettingWelcomeViewHasBeenShown]) {
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kSettingWelcomeViewHasBeenShown];
		return [self gotoViewWithStoryboardId:@"Welcome" inStoryboardNamed:@"Main"];
	}
	else {
		// This determines the precedence for initial views based upon existence of permissions, and is currently
		// following the order in the side bar, and must be manually changed to match the side bar!
		if ([BCPermissionsDataModel userHasPermission:kPermissionActivitylog]
				|| [BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
			return [self gotoViewWithStoryboardId:@"Logging" inStoryboardNamed:@"Logging"];
		}
		else if ([BCPermissionsDataModel userHasPermission:kPermissionMealplan]
				|| [BCPermissionsDataModel userHasPermission:kPermissionActivityplan]) {
			return [self gotoInitialViewControllerInStoryboardNamed:@"Planning"];
		}
		else if ([BCPermissionsDataModel userHasPermission:kPermissionShop]) {
			return [self gotoInitialViewControllerInStoryboardNamed:@"ShoppingList"];
		}
		else if ([BCPermissionsDataModel userHasPermission:kPermissionPantry]) {
			return [self gotoInitialViewControllerInStoryboardNamed:@"Pantry"];
		}
		else if ([BCPermissionsDataModel userHasPermission:kPermissionTracking] 
				|| [BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
			return [self gotoViewWithStoryboardId:@"Dashboard" inStoryboardNamed:@"Main"];
		}
		else {
			return [self gotoInitialViewControllerInStoryboardNamed:@"Messages"];
		}
	}
}

- (UIViewController *)gotoInitialViewControllerInStoryboardNamed:(NSString *)storyboardName
{
	return [self gotoInitialViewControllerInStoryboardNamed:storyboardName withProperties:nil];
}

- (UIViewController *)gotoInitialViewControllerInStoryboardNamed:(NSString *)storyboardName withProperties:(NSDictionary *)propertiesDict
{
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];

	id viewController = [storyboard instantiateInitialViewController];

	return [self gotoViewController:viewController withProperties:propertiesDict];
}

- (UIViewController *)gotoViewWithStoryboardId:(NSString *)storyboardId inStoryboardNamed:(NSString *)storyboardName
{
	return [self gotoViewWithStoryboardId:storyboardId inStoryboardNamed:storyboardName withProperties:nil];
}

- (UIViewController *)gotoViewWithStoryboardId:(NSString *)storyboardId inStoryboardNamed:(NSString *)storyboardName
withProperties:(NSDictionary *)propertiesDict
{
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];

	id viewController = [storyboard instantiateViewControllerWithIdentifier:storyboardId];

	return [self gotoViewController:viewController withProperties:propertiesDict];
}

- (UIViewController *)gotoViewController:(UIViewController *)viewController withProperties:(NSDictionary *)propertiesDict
{
	UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
	[navController setNavigationBarHidden:NO animated:NO];

	if ([viewController respondsToSelector:@selector(setManagedObjectContext:)]) {
		[(id)viewController setManagedObjectContext:self.adManagedObjectContext];
	}
	
	for (UIViewController *vc in [navController viewControllers])
	{
		if ([vc isKindOfClass:[BCViewController class]])
			[(BCViewController*)vc willNavigateAway];
	}

	if (propertiesDict) {
		[viewController setValuesForKeysWithDictionary:propertiesDict];
	}

	[navController setViewControllers:[NSArray arrayWithObject:viewController] animated:YES];

	return viewController;
}

- (void)authenticationFailure
{
	// show an error then pop to the login screen
	UIAlertView *alert = [[UIAlertView alloc] 
		initWithTitle:[BCObjectManager getErrorTitle:AuthorizationFailed]
		message:[BCObjectManager getErrorMessage:AuthorizationFailed]
		delegate:self 
		cancelButtonTitle:@"OK"
		otherButtonTitles:nil];
	alert.tag = kTagAuthenticationFalure;
	[alert show];

	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
		// Need to ensure there aren't any modal views open before alerting
		// the user and going to the login view
		UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
		UIViewController *topViewController = [navController topViewController];
		// If the top view controller is a navigation controller, keep nesting until we get to the very
		// top of all the navigation controller stacks
		while ([topViewController isKindOfClass:[UINavigationController class]]) {
			topViewController = ([(UINavigationController *)topViewController topViewController] ?: topViewController);
		}

		if (topViewController.presentedViewController) {
			[topViewController dismissViewControllerAnimated:YES completion:^{
				[self gotoLogin];
			}];
		}
		else {
			[self gotoLogin];
		}
	});
}

- (void)gotoLogin
{
	[self stopSyncCheck];

	// This is essentially a logout, so lets force the sidebar to refresh itself
	[self.sidebarView userLoggedOut];
	if (!self.sidebarView.view.hidden) {
		[self showSidebar:NO disableClosing:NO];
	}

	[User clearCurrentUser];
	[Store clearCurrentStore];

	UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
	[navController setNavigationBarHidden:YES animated:NO];

	LoginViewController *loginView = [navController.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
	loginView.delegate = self;
	loginView.managedObjectContext = self.adManagedObjectContext;
	
	[navController setViewControllers:[NSArray arrayWithObject:loginView] animated:YES];
}

#pragma mark - BCUserLoginDelegate
- (void)userDidLogin:(id)sender
{
	User *thisUser = [User currentUser];

	// we just returned from the login view
	// if we have a valid user, consumer and moc - sync
	if ([thisUser loginIsValid] && [[OAuthConsumer currentOAuthConsumer] consumerIsValid]) {
		[self performInitialSync];
	}
}

@end
