//
//  BLTrackingAddViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 10/22/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TrackingTypeMO;

@interface BLTrackingAddViewController : UIViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) TrackingTypeMO *trackingTypeMO;
@property (strong, nonatomic, readonly) NSDate *trackDate;
@property (strong, nonatomic, readonly) NSMutableDictionary *valueDictionary;
@end
