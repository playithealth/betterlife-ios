//
//  BCSegmentedControlCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/10/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCSegmentedControlCell : UITableViewCell

@property (strong, nonatomic) UILabel *bcTextLabel;
@property (strong, nonatomic) UISegmentedControl *segmentedControl;

- (id)initWithItems:(NSArray*)items reuseIdentifier:(NSString *)reuseIdentifier;

@end
