//
//  NSURL+AbsoluteNormalizedString.h.h
//

#import <Foundation/Foundation.h>

@interface NSURL (AbsoluteNormalizedString)

- (NSString *)absoluteNormalizedString;
- (NSURL *)urlWithoutQuery;

@end
