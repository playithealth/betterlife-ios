//
//  BCStrengthSearchDataModel.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/18/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

@class TrainingStrengthMO;

@interface BCStrengthSearchDataModel : NSObject

@property (strong, nonatomic, readonly) NSArray *trainingStrengthsGroupedByCount;
@property (strong, nonatomic, readonly) NSArray *trainingStrengthsGroupedByDate;
@property (strong, nonatomic, readonly) NSArray *trainingStrengthsOrderedByDate;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)moc withLogDate:(NSDate *)logDate;

- (id)insertStrengthBasedOnTrainingStrength:(TrainingStrengthMO *)strength;

@end
