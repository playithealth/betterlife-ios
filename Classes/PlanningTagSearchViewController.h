//
//  PlanningTagSearchViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/21/13.
//  Copyright (c) 2013 BettrLife Corporation. All rights reserved.
//

#import "AdvisorMealPlanTagMO.h"
#import "BCSearchViewDelegate.h"

@class MealPlannerDay;

@interface PlanningTagSearchViewController : UIViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) AdvisorMealPlanTagMO *tag;
@property (assign, nonatomic) BOOL loggingMode; ///< If we're being launch in a logging capacity vs. planning
@property (strong, nonatomic) NSDate *logDate; ///< This is really only used if we got here from logging
@property (strong, nonatomic) MealPlannerDay *selection; ///< For the unwind/return segue to leverage, the item picked

@end
