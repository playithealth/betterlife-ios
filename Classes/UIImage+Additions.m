//
//  UIImage+Additions.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 5/12/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "UIImage+Additions.h"


@implementation UIImage (Additions)

- (UIImage *)BC_imageTemplateWithColor:(UIColor *)color
{
	NSParameterAssert([color isKindOfClass: [UIColor class]]);

	UIImage *image;
	UIGraphicsBeginImageContextWithOptions([self size], NO, 0.0); // 0.0 for scale means "scale for device's main screen".
	CGRect rect = CGRectZero;
	rect.size = [self size];

	// tint the image
	[color set];
	UIRectFill(rect);

	[self drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1.0f];

	image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	return image;
}

- (UIImage *)BC_imageTintedWithColor:(UIColor *)color
{
	NSParameterAssert([color isKindOfClass: [UIColor class]]);

	UIImage *image;
	UIGraphicsBeginImageContextWithOptions([self size], NO, 0.0); // 0.0 for scale means "scale for device's main screen".
	CGRect rect = CGRectZero;
	rect.size = [self size];

	// tint the image
	[self drawInRect:rect];
	[color set];
	UIRectFillUsingBlendMode(rect, kCGBlendModeColor);

	// restore alpha channel
	[self drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1.0f];

	image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	return image;
}

- (UIImage *)BC_imageWithImageOverlay:(UIImage *)overlayImage
atPoint:(CGPoint)point atAngle:(CGFloat)angle
{
	UIImage *image;
	UIGraphicsBeginImageContextWithOptions([self size], NO, 0.0); // 0.0 for scale means "scale for device's main screen".
	CGRect rect = CGRectZero;
	rect.size = [self size];

	[self drawInRect:rect];

	// draw image
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextRotateCTM(context, angle * M_PI / 180);	

    [overlayImage drawAtPoint:point];

	image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	return image;
}

- (UIImage *)BC_imageWithTargetSize:(CGSize)targetSize
{
	// check to make sure this isn't the same as the original
	CGSize originalSize = self.size;
	if (CGSizeEqualToSize(originalSize, targetSize)) {
		// no-op
		return self;
	}

	CGFloat scale = [UIScreen mainScreen].scale; 
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGSize renderSize =	targetSize;
	if (originalSize.width > originalSize.height) {
		renderSize.height = targetSize.height * originalSize.height / originalSize.width;
	}
	else if (originalSize.width < originalSize.height) {
		renderSize.width = targetSize.width * originalSize.width / originalSize.height;
	}

	CGContextRef context = CGBitmapContextCreate(NULL, renderSize.width * scale, renderSize.height * scale, 8, 0, colorSpace, kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedFirst);
	CGContextDrawImage(context,	CGRectMake(0, 0, renderSize.width * scale, renderSize.height * scale), self.CGImage);
	CGImageRef resizedImage = CGBitmapContextCreateImage(context); 
	UIImage *final = [UIImage imageWithCGImage:resizedImage];

	CGColorSpaceRelease(colorSpace);
	CGContextRelease(context); 
	CGImageRelease(resizedImage);
	
	return final; 
}

- (UIImage *)BC_imageGrayscale
{
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, self.size.width * self.scale, self.size.height * self.scale);
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, self.size.width * self.scale, self.size.height * self.scale, 8, 0, colorSpace, kCGBitmapAlphaInfoMask & kCGImageAlphaNone);
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [self CGImage]);
    // Create bitmap image info from pixel data in current context
    CGImageRef grayImage = CGBitmapContextCreateImage(context);
    // release the colorspace and graphics context
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    // make a new alpha-only graphics context
    context = CGBitmapContextCreate(nil, self.size.width * self.scale, self.size.height * self.scale, 8, 0, nil, kCGBitmapAlphaInfoMask & kCGImageAlphaOnly);
    // draw image into context with no colorspace
    CGContextDrawImage(context, imageRect, [self CGImage]);
    // create alpha bitmap mask from current context
    CGImageRef mask = CGBitmapContextCreateImage(context);
    // release graphics context
    CGContextRelease(context);
    // make UIImage from grayscale image with alpha mask
    CGImageRef cgImage = CGImageCreateWithMask(grayImage, mask);
    UIImage *grayScaleImage = [UIImage imageWithCGImage:cgImage scale:self.scale orientation:self.imageOrientation];
    // release the CG images
	CGImageRelease(cgImage);
    CGImageRelease(grayImage);
    CGImageRelease(mask);
    // return the new grayscale image
    return grayScaleImage;
}

@end
