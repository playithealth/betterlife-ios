//
//  RestaurantSearchView.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/27/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "RestaurantSearchView.h"

#import <QuartzCore/QuartzCore.h>

#import "BCProgressHUD.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "FoodLogMO.h"
#import "NSArray+NestedArrays.h"
#import "NumberValue.h"
#import "GTMHTTPFetcherAdditions.h"
#import "RestaurantChainMO.h"
#import "User.h"

static const CGFloat kSectionHeaderHeight = 32.0;

@interface RestaurantSearchView () 
<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSArray *listItems;
@property (strong, nonatomic) NSArray *cachedRestaurants;
@property (strong, nonatomic) NSArray *restaurantSearchResults;
@property (strong, nonatomic) NSMutableArray *menuItems;
@property (strong, nonatomic) NSMutableArray *menuSectionHeaders;
@property (strong, nonatomic) NSDictionary *selectedRestaurantChain;
@end

@implementation RestaurantSearchView
@synthesize tableView;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize searchTextField = _searchTextField;
@synthesize restaurantButton = _restaurantButton;
@synthesize resetButton = _resetButton;
@synthesize scopeBackgroundImageView = _scopeBackgroundImageView;
@synthesize selectedRestaurantChain = _selectedRestaurantChain;
@synthesize cachedRestaurants;
@synthesize restaurantSearchResults;
@synthesize menuItems;
@synthesize menuSectionHeaders;
@synthesize listItems;
@synthesize delegate;

#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] 
        initWithBarButtonSystemItem:UIBarButtonSystemItemSave
        target:self action:@selector(saveButtonTapped:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    [self loadCachedRestaurants];
    self.listItems = self.cachedRestaurants;
}

- (void)viewWillAppear:(BOOL)animated
{
	self.navigationItem.title = @"Restaurants";

	[super viewWillAppear:animated];
}

- (void)viewDidUnload
{
	[self setRestaurantButton:nil];
	[self setResetButton:nil];
    [self setScopeBackgroundImageView:nil];
	[self setSearchTextField:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - local methods
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (self.listItems == self.cachedRestaurants) {
        RestaurantChainMO *chain = [self.listItems objectAtIndex:indexPath.row];
        cell.textLabel.text = chain.name;
        cell.detailTextLabel.text = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else if (self.listItems == self.restaurantSearchResults) {
        NSDictionary *chainDict = [self.listItems objectAtIndex:indexPath.row];
        cell.textLabel.text = [chainDict objectForKey:@"store_name"];
        cell.detailTextLabel.text = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else if (self.listItems == self.menuItems) {
        NSDictionary *menuItemDict = [self.listItems BC_nestedObjectAtIndexPath:indexPath];;
        cell.textLabel.text = [menuItemDict objectForKey:@"name"];
        NSString *servingSize = [menuItemDict objectForKey:@"serving_size"];
        if ([servingSize isEqualToString:@"whole serving"]) {
            cell.detailTextLabel.text = nil;
        }
        else {
            cell.detailTextLabel.text = servingSize;
        }
        BOOL selected = [[menuItemDict objectForKey:@"selected"] boolValue];
        if (selected) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }

    }
}

- (void)searchForRestaurant:(NSString *)searchTerm
{
	if ([searchTerm length] == 0) {
        return;
	}
    
    [BCProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    NSURL *restaurantSearchURL = [BCUrlFactory restaurantChainURLSearch:searchTerm];
    
    // perform fetch for menu items
    GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:restaurantSearchURL];
    
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
        NSString *response = [[NSString alloc] initWithData:retrievedData
                                                   encoding:NSUTF8StringEncoding];
        if (error) {
            [BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
                          withFetcher:fetcher response:response];
        }
        else {
            // fetch succeeded
            id responseData = [response JSONValue];
            
            if ([responseData isKindOfClass:[NSArray class]]) {
                self.restaurantSearchResults = responseData;
                self.listItems = self.restaurantSearchResults;
                
                [self.tableView reloadData];
                
                // Hide the HUD
                [BCProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            }
        }
        
    }];
}

- (void)getMenuForRestaurant:(NSNumber *)restaurantId
{
    if ([restaurantId integerValue] <= 0) {
        return;
    }
    
    [BCProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    NSURL *menuItemsURL = [BCUrlFactory menuItemsURLForRestaurant:restaurantId];
    
    // perform fetch for menu items
    GTMHTTPFetcher *fetcher =
    [GTMHTTPFetcher signedFetcherWithURL:menuItemsURL];
    
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
        NSString *response = [[NSString alloc] initWithData:retrievedData
                                                   encoding:NSUTF8StringEncoding];
        if (error) {
            [BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
                          withFetcher:fetcher response:response];
        }
        else {
            // fetch succeeded
            id responseData = [response JSONValue];
            
            if ([responseData isKindOfClass:[NSArray class]]) {
                self.menuSectionHeaders = [NSMutableArray array];
                self.menuItems = [NSMutableArray array];
                NSString *currentCategory = nil;
                for (NSDictionary *menuDict in responseData) {
                    NSString *menuCategory = [menuDict valueForKey:@"category"];
                    if (![currentCategory isEqualToString:menuCategory]) {
                        [self.menuSectionHeaders addObject:menuCategory];
                        [self.menuItems addObject:[NSMutableArray array]];
                        currentCategory = menuCategory;
                    }
                    NSMutableDictionary *menuItemDict = [menuDict mutableCopy];
                    [menuItemDict setValue:[NSNumber numberWithBool:NO] forKey:@"selected"];
                    [[self.menuItems lastObject] addObject:menuItemDict];
                }
                
                self.listItems = self.menuItems;
                
                [self.tableView reloadData];
                
                // Hide the HUD
                [BCProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            }
        }
        
    }];        
    
}

- (void)updateScopeBarForRestaurant:(NSString *)restaurant
{
    [self.restaurantButton setTitle:restaurant forState:UIControlStateNormal]; 
    
    if (restaurant) {
        self.navigationItem.title = [NSString stringWithFormat:@"Menu: %@", restaurant];
        self.scopeBackgroundImageView.image = [UIImage imageNamed:@"search-bar-end-active.png"];
        [self.resetButton setImage:[UIImage imageNamed:@"search-bar-icon-inactive.png"]
                          forState:UIControlStateNormal];
    }
    else {
        self.navigationItem.title = @"Restaurants";
        self.scopeBackgroundImageView.image = [UIImage imageNamed:@"search-bar-end-inactive.png"];
        [self.resetButton setImage:[UIImage imageNamed:@"search-bar-icon-active.png"]
                          forState:UIControlStateNormal];
    }
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numberOfSections = 1;
    if (self.listItems == self.menuItems) {
        numberOfSections = [self.menuSectionHeaders count];
    }
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = [self.listItems count];
    if (self.listItems == self.menuItems) {
        numberOfRows = [self.listItems BC_countOfNestedArrayAtIndex:section];
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellId = @"RestaurantsViewCellId";

	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
			reuseIdentifier:cellId];
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)theTableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.searchTextField resignFirstResponder];
    
    if (self.listItems == self.cachedRestaurants) {
        RestaurantChainMO *chain = [self.cachedRestaurants objectAtIndex:indexPath.row];
        [self updateScopeBarForRestaurant:chain.name];
        [self getMenuForRestaurant:chain.cloudId];
    }
    else if (self.listItems == self.restaurantSearchResults) {
        NSDictionary *chainDict = [self.listItems objectAtIndex:indexPath.row];
        [self updateScopeBarForRestaurant:[chainDict valueForKey:@"store_name"]];
        [self getMenuForRestaurant:[[chainDict objectForKey:kCloudId] numberValueDecimal]];
        
        // save this restaurant so that we can cache it later
        self.selectedRestaurantChain = chainDict;
    }
    else if (self.listItems == self.menuItems) {
		// add the item to the food log
        NSDictionary *menuItemDict = [self.listItems BC_nestedObjectAtIndexPath:indexPath];
        
        // reverse the current state of selection
        BOOL selected = ![[menuItemDict objectForKey:@"selected"] boolValue];
        [menuItemDict setValue:[NSNumber numberWithBool:selected] forKey:@"selected"];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (selected) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.listItems == self.menuItems) {
        return kSectionHeaderHeight;
    }
    else {
        return 0.0f;
    }
}

- (UIView *)tableView:(UITableView *)theTableView 
viewForHeaderInSection:(NSInteger)section
{
    if (self.listItems != self.menuItems) {
		return nil;
	}

	return [BCTableViewController customViewForHeaderWithTitle:[self.menuSectionHeaders objectAtIndex:section]];
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.listItems == self.cachedRestaurants 
            || self.listItems == self.restaurantSearchResults) {
        textField.returnKeyType = UIReturnKeySearch;
    }
    else {
        textField.returnKeyType = UIReturnKeyDone;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
    
    if (self.listItems == self.cachedRestaurants 
            || self.listItems == self.restaurantSearchResults) {
        [self searchForRestaurant:textField.text];
    }
    
	return YES;
}

- (IBAction)searchTextDidChange:(id)sender
{
    // TODO not sure what if anything we are going to do in here
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)loadCachedRestaurants
{
    User *thisUser = [User currentUser];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[RestaurantChainMO entityInManagedObjectContext:self.managedObjectContext]];
    [fetchRequest setFetchBatchSize:20];
    
    NSPredicate *loginIdPredicate = [NSPredicate predicateWithFormat:@"loginId = %@", thisUser.loginId];
    [fetchRequest setPredicate:loginIdPredicate];
    
    NSSortDescriptor *sortByDate = [[NSSortDescriptor alloc] initWithKey:RestaurantChainMOAttributes.date ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortByDate]];
    
    self.cachedRestaurants = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
}    

#pragma mark - button responders
- (IBAction)searchButtonTapped:(id)sender 
{
}

- (IBAction)saveButtonTapped:(id)sender
{
    NSInteger count = 0;
    for (NSArray *menuCategory in menuItems) {
        for (NSDictionary *menuItemDict in menuCategory) {
            BOOL selected = [[menuItemDict objectForKey:@"selected"] boolValue];
            if (selected) {
                if ([delegate conformsToProtocol:@protocol(RestaurantSearchViewDelegate)]) {
                    [delegate restaurantSearchView:self didSelectMenuItem:menuItemDict];
                    count++;
                }
            }
        }
    }
    
    if (count > 0) {
        [BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%ld items added to your food log", (long)count]
            onView:self.navigationController.view];
        
        if (self.selectedRestaurantChain) {
            NSNumber *cloudId = [[self.selectedRestaurantChain objectForKey:kCloudId] numberValueDecimal];
            
            BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                RestaurantChainMO *restaurant = (RestaurantChainMO *)obj;
                return ([restaurant.cloudId isEqualToNumber:cloudId]);
            };
            
            NSUInteger existingIndex = [self.cachedRestaurants indexOfObjectPassingTest:SameCloudId];
            RestaurantChainMO *chain = nil;
            if (existingIndex == NSNotFound) {
                chain = [RestaurantChainMO insertInManagedObjectContext:self.managedObjectContext];
                chain.name = [self.selectedRestaurantChain valueForKey:@"store_name"];
                chain.cloudId = cloudId;
                chain.loginId = [[User currentUser] loginId];
            }
            else {
                chain = [self.cachedRestaurants objectAtIndex:existingIndex];
            }
            chain.date = [NSDate date];
            [self.managedObjectContext save:nil];
        }
        
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)resetButtonTapped:(id)sender 
{
	self.navigationItem.title = @"Restaurants";
    
    [self updateScopeBarForRestaurant:nil];
    
    self.listItems = self.cachedRestaurants;

	[self.tableView reloadData];
	[self.searchTextField becomeFirstResponder];
}

- (IBAction)restaurantButtonTapped:(id)sender 
{
}

@end
