//
//  BLThreadViewController.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 9/11/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BLConversationViewController.h"

@class ThreadMO;

@interface BLThreadViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) ThreadMO *thread;
@end
