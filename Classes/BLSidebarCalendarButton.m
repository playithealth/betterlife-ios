//
//  BLSidebarCalendarButton.m
//  BettrLife
//
//  Created by Greg Goodrich on 10/28/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLSidebarCalendarButton.h"

@interface BLSidebarCalendarButton ()
@property (strong, nonatomic) CALayer *highlightLayer;
@end

@implementation BLSidebarCalendarButton

- (id)init
{
	return [self initWithCoder:nil];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		struct CGColor *borderColor = [[[UIColor whiteColor] colorWithAlphaComponent:0.4] CGColor];
		float borderWidth = 0.5f;
		float cornerRadius = 6.0f;

		self.layer.borderWidth = borderWidth;
		self.layer.borderColor = borderColor;
		self.layer.cornerRadius = cornerRadius;

		_highlightLayer = [CALayer layer];
		_highlightLayer.frame = CGRectInset(self.bounds, 3.0, 3.0);
		_highlightLayer.backgroundColor = [[UIColor whiteColor] CGColor];
		_highlightLayer.opacity = 0.4;
		_highlightLayer.cornerRadius = 4;
		_highlightLayer.name = @"selected";
		_highlightLayer.hidden = YES;
		[self.layer insertSublayer:_highlightLayer atIndex:0];
	}

	return self;
}

- (void)setHasData:(BOOL)hasData
{
	_hasData = hasData;

	// Show the layer if this calendar day has data, hide it if not
	self.highlightLayer.hidden = !hasData;
}

@end
