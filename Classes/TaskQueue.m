//
//  TaskQueue.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 8/1/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "TaskQueue.h"
#import "TaskQueueItems.h"

@implementation TaskQueueItem

- (id)initWithTarget:(id)inTarget selector:(SEL)inSelector identifier:(NSString *)inIdentifier
{
	if (![TaskQueueItems registerTaskWithIdentifier:inIdentifier]) {
		return nil;
	}
	if ((self = [super init])) {
		self.target = inTarget;
		self.selector = inSelector;
		self.identifier = inIdentifier;
	}

	return self;
}

- (id)initWithIdentifier:(NSString *)inIdentifier block:(void(^)())codeBlock
{
	if (![TaskQueueItems registerTaskWithIdentifier:inIdentifier]) {
		return nil;
	}
	if ((self = [super init])) {
		self.codeBlock = codeBlock;
		self.identifier = inIdentifier;
	}

	return self;
}

- (void)dealloc
{
	if ([self identifier]) {
		[TaskQueueItems unregisterTask:self.identifier];
	}
}

- (void)cancel
{
	// TODO Add any code here that could be used to cancel a task
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@, target: %@, selector: %@", [super description], _target, NSStringFromSelector(_selector)];
}

@end

@interface TaskQueue ()
@end

@implementation TaskQueue

#pragma mark - life cycle
- (id)initWithIdentifier:(NSString *)queueIdentifier completionBlock:(BC_TaskQueueCompletionBlock)completionBlock
{
	if ((self = [super init])) {
		self.handler = completionBlock;
		self.identifier = queueIdentifier;
	}

	return self;
}

+ (id)queueNamed:(NSString *)name completionBlock:(BC_TaskQueueCompletionBlock)completionBlock
{
	TaskQueue *newQueue = [[TaskQueue alloc] init];
	if (newQueue) {
		if (![TaskQueueItems registerQueue:newQueue withIdentifier:name]) {
			newQueue = nil;
		}
		else {
			newQueue.handler = completionBlock;
			newQueue.identifier = name;
		}
	}

	return newQueue;
}

- (void)dealloc
{
	if ([self identifier]) {
		[TaskQueueItems unregisterQueue:self.identifier];
	}
}

#pragma mark - local methods
/**
 * This method is used to add selectors to the queue, essentially to send a message
 * to a class.  It does NOT automatically construct a handler, and thus the selector needs
 * to finish by calling the queue's runQueue method
 */
- (void)addRequest:(id)target targetSelector:(SEL)targetSelector
{
	TaskQueueItem *newItem = [[TaskQueueItem alloc] initWithTarget:target
		selector:targetSelector identifier:NSStringFromSelector(targetSelector)];
	
	if (!newItem) {
		// This target must have already been registered from somewhere else, so do not queue it
		return;
	}

	if (!self.queuedRequests) {
		self.queuedRequests = [[NSMutableArray alloc] init];
	}

	[self.queuedRequests addObject:newItem];
}

- (void)addRequestWithIdentifier:(NSString *)identifier withBlock:(void(^)())codeBlock
{
	TaskQueueItem *newItem = [[TaskQueueItem alloc] initWithIdentifier:identifier block:codeBlock];
	
	if (!newItem) {
		// This target must have already been registered from somewhere else, so do not queue it
		return;
	}

	if (!self.queuedRequests) {
		self.queuedRequests = [[NSMutableArray alloc] init];
	}

	[self.queuedRequests addObject:newItem];
}

- (void)runQueue:(NSError *)error
{
	if (!error && [self.queuedRequests count]) {
		TaskQueueItem *item = [self.queuedRequests objectAtIndex:0];
		[self.queuedRequests removeObjectAtIndex:0];
		if (item.codeBlock) {
			dispatch_async(dispatch_get_main_queue(), item.codeBlock);
		}
		else if (item.selector && item.target) {
			dispatch_async(dispatch_get_main_queue(), ^{
// ARC hates performSelector when used with run-time binding
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
				[item.target performSelector:item.selector withObject:self];
#pragma clang diagnostic pop
			});
		}
		else {
			// TODO Set a specific error to be passed back that signifies that we're missing
			// the selector and/or the target
			[self finished:error];
		}
	}
	// Encountered a problem somewhere, or ran out of queued items,
	// either way, call finished to notify the delegate
	else {
		[self finished:error];
	}
}

- (void)finished:(NSError *)error
{
	// See if we have a delegate/selector
	if (self.delegate) {
		[self.delegate performSelector:@selector(queueDidFinish:withError:)
			withObject:self.identifier withObject:error];
	}
	// See if we have a handler code block
	else if (self.handler) {
		self.handler(self.identifier, error);
	}
	[TaskQueueItems unregisterQueue:self.identifier];
}

- (void)cancelTasks
{
	// If there is a request (task) running, attempt to stop it
	if (self.runningRequest) {
		// Cancel this task
		[self.runningRequest cancel];

		self.runningRequest = nil;
	}

	// Then remove all remaining tasks from the queue
	// TODO - should we call each request's handler?  If so, we need to ensure
	// that they don't actually run their code, but just notify that they're done?
	[self.queuedRequests removeAllObjects];
}

@end
