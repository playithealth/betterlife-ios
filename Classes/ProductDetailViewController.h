//
//  ProductDetailViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/26/10.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCViewController.h"

@interface ProductDetailItem : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *barcode;
@property (strong, nonatomic) NSNumber *productId;
@property (strong, nonatomic) NSNumber *categoryId;
@property (strong, nonatomic) NSNumber *imageId;
@end

@interface ProductDetailViewController : BCViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ProductDetailItem *productDetailItem;

@end



