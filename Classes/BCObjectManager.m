//
//  BCObjectManager.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 12/30/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "BCObjectManager.h"

#import "Achievement.h"
#import "ActivityLogMO.h"
#import "ActivityPlanMO.h"
#import "AdvisorActivityPlanMO.h"
#import "AdvisorActivityPlanTimelineMO.h"
#import "AdvisorGroup.h"
#import "AdvisorMO.h"
#import "AdvisorMealPlanMO.h"
#import "AdvisorMealPlanTimelineMO.h"
#import "BCAppDelegate.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "CategoryMO.h"
#import "CoachInviteMO.h"
#import "CommunityMO.h"
#import "CustomFoodMO.h"
#import "ExerciseRoutineMO.h"
#import "FoodLogMO.h"
#import "MealPredictionMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "HTTPQueue.h"
#import "HealthChoiceMO.h"
#import "LoggingNoteMO.h"
#import "LoginViewController.h"
#import "MealPlannerDay.h"
#import "MessageMO.h"
#import "MessageDeliveryMO.h"
#import "NSCalendar+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NumberValue.h"
#import "NutrientMO.h"
#import "PantryItem.h"
#import "Promotion.h"
#import "PurchaseHxItem.h"
#import "RecipeMO.h"
#import "Recommendation.h"
#import "ShoppingListItem.h"
#import "SingleUseToken.h"
#import "Store.h"
#import "StoreCategory.h"
#import "SyncUpdateMO.h"
#import "TaskQueueItems.h"
#import "ThreadMO.h"
#import "ThreadMessageMO.h"
#import "TrackingTypeMO.h"
#import "TrackingMO.h"
#import "TrainingActivityMO.h"
#import "TrainingAssessmentMO.h"
#import "TrainingStrengthMO.h"
#import "UnitMO.h"
#import "User.h"
#import "UserHealthChoice.h"
#import "UserImageMO.h"
#import "UserPermissionMO.h"
#import "UserProfileMO.h"
#import "UserStoreMO.h"

@interface BCObjectManager ()
@end

@implementation BCObjectManager 

#pragma mark - init
- (id)initWithManagedObjectContext:(NSManagedObjectContext *)context
{
	if ((self = [super init])) {
		self.managedObjectContext = context;
	}
	return(self);
}

#pragma mark - Error handling
+ (NSString *)getErrorTitle:(NSInteger)errorCode {
	if (errorCode == AuthorizationFailed) {
		return @"Authorization Failed";
	}
	else if (errorCode == CoreDataError) {
		return @"Core Data Error";
	}
	else if (errorCode == BadRequest) {
		return @"Bad Request";
	}
	else if (errorCode == NoResultsReturned) {
		return @"No results were returned.";
	}
	else if (errorCode == UnspecifiedError) {
		return @"Unspecified Error";
	}
	else if (errorCode == UnexpectedDataTypeReturned) {
		return @"Unexpected Data Type";
	}
	else if (errorCode == NoConnectivity) {
		return @"No Connection";
	}
	else if (errorCode == NSValidationMultipleErrorsError
			|| errorCode == NSManagedObjectValidationError
			|| errorCode == NSValidationMissingMandatoryPropertyError) {
		return @"Data Validation Error";
	}
	else {
		return @"Error";
	}
}

+ (NSString *)getErrorMessage:(NSInteger)errorCode {
	if (errorCode == AuthorizationFailed) {
		return @"Your session has expired, try logging in again.";
	}
	else if (errorCode == CoreDataError) {
		return @"Your data failed to save properly, try restarting the application.";
	}
	else if (errorCode == BadRequest) {
		return @"The API request was malformed or lacked some data, try restarting the application.";
	}
	else if (errorCode == NoResultsReturned) {
		return @"No results were returned.";
	}
	else if (errorCode == UnspecifiedError) {
		return @"An unspecified error has occurred.";
	}
	else if (errorCode == UnexpectedDataTypeReturned) {
		return @"Unexpected Data Type returned in response body.";
	}
	else if (errorCode == NoConnectivity) {
		return @"Failed to connect to BettrLife.com";
	}
	else if (errorCode == NSValidationMultipleErrorsError
			|| errorCode == NSManagedObjectValidationError
			|| errorCode == NSValidationMissingMandatoryPropertyError) {
		return @"Some data failed to validate properly, "
			"try restarting the application. If the problem persists "
			"send an email to issues@bettrlife.com";
	}
	else {
		return nil;
	}
}

#pragma mark - TaskQueue Delegate
// Standard method to use as the selector when constructing an TaskQueue within a BCOM sync method
- (void)queueDidFinish:(NSString *)identifier forUser:(NSNumber *)loginId withError:(NSError *)error
withCompletionBlock:(void(^)(void))completionBlock
{
	DDLogSync(@"queueDidFinish:%@, error:%@", THIS_METHOD, error);
	NSInteger errorCode = 0;

	if (error) {
		errorCode = [error code];
	}

	if (identifier) {
		NSDictionary *userInfo = nil;
		if (error) {
			userInfo = [NSDictionary dictionaryWithObject:error forKey:@"error"];
		}
		// Post a notification that we're done with this queue
		[[NSNotificationCenter defaultCenter]
			postNotificationName:identifier
			object:self userInfo:userInfo];
	}

	if (errorCode) {
		// if the error was an authentication problem, force login
		if (errorCode == NSURLErrorUserAuthenticationRequired || errorCode == AuthorizationFailed) {
			BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
			// Do not process further, as we've only one choice, which is to notify the app delegate that we no longer
			// have credentials to be running the application
			[appDelegate authenticationFailure];
			return;
		}
		// if this isn't one of the errors that we have decided to ignore, throw up an alert
		else if (errorCode != NoResultsReturned
				&& errorCode != NoConnectivity
				&& errorCode != NSURLErrorNotConnectedToInternet) {
			dispatch_async(dispatch_get_main_queue(), ^{
				NSString *title = [BCObjectManager getErrorTitle:errorCode];
				NSString *message = [BCObjectManager getErrorMessage:errorCode];
				if (!message) {
					message = [error localizedDescription];
				}

				// NOTE: This seems to cause a warning from setNetworkActivityIndicatorVisible, since there shouldn't
				// be any more ref counts, so I'm commenting this out for now, and will investigate further
				//BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
				//[appDelegate setNetworkActivityIndicatorVisible:NO];

				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
																message:message
																delegate:nil 
														cancelButtonTitle:@"OK"
														otherButtonTitles:nil];
				[alert show];
				DDLogError(@"%@", error);
			});
		}
	}
	else {
		[SyncUpdateMO syncSucceededForUser:loginId inMOC:self.managedObjectContext];
	}

	if (completionBlock) {
		completionBlock();
	}
}
	

#pragma mark - ShoppingList
- (BCObjectManagerReturnCode)sendUpdatesForShoppingList:(TaskQueue *)taskQueue
{
	NSArray *updatedItems = [ShoppingListItem MR_findAllWithPredicate:
		[NSPredicate predicateWithFormat:@"accountId = %@ and status <> %@",
		 [[User currentUser] accountId], kStatusOk] inContext:self.managedObjectContext];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForShoppingList" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	for (ShoppingListItem *anItem in updatedItems) {
		NSString *status = anItem.status;

		NSURL *url = nil;
		if ([anItem.isRecommendation boolValue]) {
			url = [BCUrlFactory productRecommendationsURLShowHidden:NO];
		}
		else {
			url = [BCUrlFactory shoppingListURL];
		}

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[anItem toJSON]];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete] 
							|| [anItem.purchased boolValue] 
							|| [anItem.isRecommendation boolValue]) {
						[self.managedObjectContext deleteObject:anItem];
					}
					else {
						anItem.status = kStatusOk;
						if ([resultsDict objectForKey:kCloudId] != nil) {
							anItem.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForShoppingList"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];

	}

	[subQueue runQueue:nil];

	return Success;
}

- (void)getUpdatesForShoppingList:(id)taskQueue
{
	static NSString *shoppingListFetcherId = @"ShoppingListFetcher";

	NSNumber *loginId = [User loginId];
	NSNumber *accountId = [User accountId];

	// set up a fetcher with the latest updatedOn for ShoppingListItem
	NSPredicate *accountIdPredicate = [NSPredicate predicateWithFormat:@"accountId = %@", accountId];
	NSString *table = kSyncShopping;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [ShoppingListItem MR_aggregateOperation:@"max:" onAttribute:ShoppingListItemAttributes.updatedOn
			withPredicate:accountIdPredicate inContext:self.managedObjectContext];
	}
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory shoppingListUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:shoppingListFetcherId];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:self userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_ShoppingList, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:shoppingListFetcherId];

		if (!error) {
			// fetch succeeded
			NSArray *resultsArray = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			NSArray *shoppingListUpdates = nil;
			NSArray *shoppingListRecommendations = nil;
			if ([resultsArray isKindOfClass:[NSArray class]]) {
				shoppingListUpdates = [resultsArray 
					filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K != NULL", kCloudId]];
				shoppingListRecommendations = [resultsArray 
					filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == NULL", kCloudId]];
			}
			else {
				error = [NSError errorWithDomain:@"Cloud API Error" code:UnexpectedDataTypeReturned
					userInfo:nil];
			}

			// fetch all the shoppinglist items for this account
			NSArray *cachedSLItems = [ShoppingListItem MR_findAllWithPredicate:accountIdPredicate inContext:self.managedObjectContext];

			// process the updates
			if ([shoppingListUpdates count] > 0) {
				for (NSMutableDictionary *updateObject in shoppingListUpdates) {
					NSNumber *cloudId = [[updateObject valueForKey:kCloudId] numberValueDecimal];

					// this code block is used below to grab the index of the matching item by cloudid
					// cloud_id comes back from the cloud as a string, so just querying the array using
					// a predicate wasn't working
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						ShoppingListItem *item = (ShoppingListItem *)obj;
						return ([item.cloudId isEqualToNumber:cloudId]);
					};

					NSUInteger index = [cachedSLItems indexOfObjectPassingTest:SameCloudId];

					BOOL deleted = [[updateObject valueForKey:kDeleted] boolValue];
					BOOL purchased = [[updateObject valueForKey:kPurchased] boolValue];

					ShoppingListItem *shoppingListItem = nil;
					if (index == NSNotFound) {
						// this has to be checked inside NSNotFound - because the item may not exist on the phone
						// but has been marked deleted on the web
						if (!deleted && !purchased) {
							shoppingListItem = [ShoppingListItem insertInManagedObjectContext:self.managedObjectContext];
							shoppingListItem.accountId = accountId;
						}
					}
					else {
						shoppingListItem = [cachedSLItems objectAtIndex:index];

						if (deleted || purchased) {
							[self.managedObjectContext deleteObject:shoppingListItem];
							continue;
						}
					}

					[shoppingListItem setWithUpdateDictionary:updateObject];
				}
			}

			// process the recommendations
			NSPredicate *currentRecommendationPredicate = [NSPredicate predicateWithFormat:@"isRecommendation = YES"];
			NSMutableArray *currentRecommendations = [[cachedSLItems filteredArrayUsingPredicate:currentRecommendationPredicate] mutableCopy];

			for (NSMutableDictionary *recommendation in shoppingListRecommendations) {
				NSString *name = [recommendation valueForKey:kItemName];
				NSNumber *productId = [[recommendation valueForKey:kProductId] numberValueDecimal];

				// this code block is used below to grab the index of the matching item by name and productId
				BOOL (^SameNameAndProductId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					ShoppingListItem *item = (ShoppingListItem *)obj;
					return ([item.name isEqualToString:name] && [item.productId isEqualToNumber:productId]);
				};

				NSUInteger recommendationIndex = [currentRecommendations indexOfObjectPassingTest:SameNameAndProductId];
				NSUInteger slItemIndex = [cachedSLItems indexOfObjectPassingTest:SameNameAndProductId];

				ShoppingListItem *shoppingListItem = nil;
				// item is in the shoppinglist as a recommendation - update
				if (recommendationIndex != NSNotFound) {
					shoppingListItem = [currentRecommendations objectAtIndex:recommendationIndex];
					[currentRecommendations removeObjectAtIndex:recommendationIndex];
				}
				// item is not found - insert
				else if (slItemIndex == NSNotFound) {
					shoppingListItem = [ShoppingListItem insertInManagedObjectContext:self.managedObjectContext];
				}
				// item is in the shoppinglist, and not a recommendation - ignore
				else {
					continue;
				}

				shoppingListItem.accountId = accountId;
				[shoppingListItem setWithRecommendationDictionary:recommendation];
			}

			// remove any remaining recommendations
			for (ShoppingListItem* deleteItem in currentRecommendations) {
				[self.managedObjectContext deleteObject:deleteItem];
			}

			// Save the context.
			if (!error) {

				// Save the context.
				[self.managedObjectContext BL_save];

				// Get the new lastUpdate
				NSNumber *lastUpdate = [ShoppingListItem MR_aggregateOperation:@"max:" onAttribute:ShoppingListItemAttributes.updatedOn
					withPredicate:accountIdPredicate inContext:self.managedObjectContext];
				// Update the lastUpdate in syncUpdate
				[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

				[self.managedObjectContext BL_save];
			}
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForShoppingList"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the shopping list
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[ShoppingListItem entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];
}

+ (void)adjustStoreCategoryOrderByShoppingOrder:(NSArray *)purchasedItems forStore:(Store *)store inMOC:(NSManagedObjectContext *)inMOC
{
	// For now, only allow this on user stores.  Eventually we may want to extend
	// this to any store, but right now there isn't a model in the iPhone app that supports
	// that concept.
	if (![store isUserStore]) {
		return;
	}

	// Don't use the data in Store passed in, as it was likely loaded into a different MOC,
	// which will give troubles here.  Load our own UserStoreMO from the info within our store
	// singleton copy
	UserStoreMO *userStore = (UserStoreMO *)[inMOC objectWithID:[[store userStore] objectID]];

	// Check to ensure the setting is turned on that allows us to do this auto sorting
	if (![userStore.autoSort boolValue]) {
		return;
	}
	
	// Ensure the store is up to date with all the categories available
	[userStore populateStoreCategories];

	NSSortDescriptor *sortByCartTime = [[NSSortDescriptor alloc] initWithKey:@"inCartTime" ascending:YES];
	NSArray *purchaseOrder = [purchasedItems sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortByCartTime]];

	NSSortDescriptor *sortBySortOrder = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSArray *currentOrder = [[userStore categories] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortBySortOrder]];
	if (LOG_VERBOSE) {
		for (StoreCategory *myCategory in currentOrder) {
			DDLogVerbose(@"Current Category %@", myCategory.category.name);
		}
	}

	NSMutableDictionary *categoriesWithPurchasesDict = [[NSMutableDictionary alloc] init];
	NSNumber *currentCategoryId = nil;
	// First, build a dictionary of all the categories that the user purchased items within,
	// keyed on id
	for (ShoppingListItem *purchasedItem in purchaseOrder) {
		// TODO Probably should be verifying that they purchased this item at the
		// current store via the entityId, but right now that isn't set

		if ([currentCategoryId isEqualToNumber:purchasedItem.category.cloudId]) {
			// Already processed this category, skip
			continue;
		}
		currentCategoryId = purchasedItem.category.cloudId;
		DDLogVerbose(@"Purchased items in category %@", purchasedItem.category.name);

		// Ensure this category isn't already in the dictionary
		if (![categoriesWithPurchasesDict objectForKey:currentCategoryId]
				&& purchasedItem.storeCategory) {
			// Add this to the dictionary, using its id as the key to the dictionary
			[categoriesWithPurchasesDict setObject:purchasedItem.storeCategory
				forKey:currentCategoryId];
		}
	}

	// Now iterate again on the current order and update the sort order on the store cats
	BOOL dirty = NO;
	NSUInteger purchasedIdx = 0;
	unsigned sortOrder = 0;
	NSMutableDictionary *updatedDict = [[NSMutableDictionary alloc] init];
	for (StoreCategory *storeCat in currentOrder) {
		// First, check the updatedDict to ensure we don't process the same category twice
		if ([updatedDict objectForKey:storeCat.category.cloudId]) {
			DDLogVerbose(@"Skipping %@, already processed", storeCat.category.name);
			continue;
		}
		// Now check to see if this is a category that something got purchased within
		if (![categoriesWithPurchasesDict objectForKey:storeCat.category.cloudId]) {
			// This category not in the dictionary, thus no items were purchased in this
			// category, just set its sortOrder
			storeCat.sortOrder = [NSNumber numberWithInt:sortOrder++];
			// Add it to the dictionary as it has now been processed
			[updatedDict setObject:storeCat.sortOrder forKey:storeCat.category.cloudId];
			DDLogVerbose(@"Updating %@ and placing in position %u", storeCat.category.name, sortOrder - 1);
			continue;
		}

		// This category had items purchased within it, walk through the purchaseOrder
		// array, setting the sortorder for all categories up to and including this one
		// and update the purchasedIdx as we go so that they do not get processed again
		BOOL found = NO;
		while (!found && (purchasedIdx < [purchaseOrder count])) {
			ShoppingListItem *purchasedItem = [purchaseOrder objectAtIndex:purchasedIdx++];
			// See if we've already processed this category, if so, skip
			if ([updatedDict objectForKey:purchasedItem.category.cloudId]) {
				DDLogVerbose(@"Skipping %@, already processed", purchasedItem.category.name);
				continue;
			}
			// Set its sortOrder
			purchasedItem.storeCategory.sortOrder = [NSNumber numberWithInt:sortOrder++];
			// Add it to the updatedDict
			[updatedDict setObject:storeCat.sortOrder forKey:purchasedItem.category.cloudId];

			// See if this is our 'target' category we are looking for
			if ([purchasedItem.category.cloudId isEqualToNumber:storeCat.category.cloudId]) {
				DDLogVerbose(@"Moving %@ and placing in position %u", purchasedItem.category.name, sortOrder - 1);
				found = YES;
			}
			else {
				dirty = YES;
				DDLogVerbose(@"Injecting %@ in front of %@ and placing in position %u",
					purchasedItem.category.name, storeCat.category.name, sortOrder - 1);
			}
		}
		// TODO - should not happen?
		if (!found) {
			DDLogVerbose(@"Error looking for %@, ran past purchaseOrder boundary", storeCat.category.name);
		}
	}

	if (dirty) {
		// We modified some rows, so set the UserStore to dirty so that it syncs
		userStore.storeCategoryStatus = kStatusPut;
	}

	// Now save
	[inMOC BL_save];
}

+ (void)markItemsInCartPurchasedInMOC:(NSManagedObjectContext *)inMOC
{
	NSNumber *accountId = [User accountId];
	Store *store = [Store currentStore];

	// find the items in this user's cart
	NSFetchRequest *userCartRequest = [[NSFetchRequest alloc] init];
	userCartRequest.entity = [ShoppingListItem entityInManagedObjectContext:inMOC];
	userCartRequest.predicate = [NSPredicate predicateWithFormat:@"accountId = %@ AND inCart = YES", accountId];

	NSArray *cartItems = [inMOC executeFetchRequest:userCartRequest error:nil];

	for (ShoppingListItem *anItem in cartItems) {
		anItem.inCart = @NO;
		anItem.purchased = @YES;
		anItem.storeId = store.storeId;
		// Make sure that any item that gets purchased is no longer a recommendation,
		// as that will cause the sendUpdatesForShoppingList to send a snooze/hide
		// instead of an update to this shopping list item.
		if ([anItem.isRecommendation boolValue]) {
			anItem.isRecommendation = @NO;
			// Needs to be a POST, as recommendations don't exist on the cloud as actual SL items
			anItem.status = kStatusPost;
		}
		else if ([anItem.status isEqualToString:kStatusOk]) {
			anItem.status = kStatusPut;
		}
	}

	[inMOC BL_save];

	[BCObjectManager adjustStoreCategoryOrderByShoppingOrder:cartItems forStore:store inMOC:inMOC];

	// Sync the pieces that have changed
	[BCObjectManager syncCheck:SendAndReceive];
}

#pragma mark - Messages
- (BCObjectManagerReturnCode)sendUpdatesForMessages:(TaskQueue *)taskQueue
{
	// find the updated items
	// NOTE: Hopefully this message isn't going to multiple recipients, as we're having to query the deliveries table, which would return
	// multiple results per message, and wouldn't work properly.
	NSPredicate *updatedPredicate =
		[NSPredicate predicateWithFormat:@"(loginId = %@ and status <> %@) or (message.loginId = %@ and status <> %@)", 
		[[User currentUser] loginId], kStatusOk, [[User currentUser] loginId], kStatusOk];

	NSFetchRequest *updatedItemsRequest = [[NSFetchRequest alloc] init];
	updatedItemsRequest.entity = [MessageDeliveryMO entityInManagedObjectContext:self.managedObjectContext];
	updatedItemsRequest.predicate = updatedPredicate;

	NSArray *updatedMessages = [self.managedObjectContext executeFetchRequest:updatedItemsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForMessages" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory inboxURL];
	for (MessageDeliveryMO *aDelivery in updatedMessages) {
		NSString *status = aDelivery.status;

		NSData *postData;
		// If this is a POST, then it is a new message
		if ([aDelivery.status isEqualToString:kStatusPost]) {
			postData = [aDelivery.message toJSON];
		}
		else {
			postData = [aDelivery toJSON];
		}
		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:postData];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				aDelivery.status = kStatusOk;
				if ([resultsDict objectForKey:kMessageId]) {
					aDelivery.message.cloudId = [resultsDict BC_numberForKey:kMessageId];
				}
				if ([resultsDict objectForKey:kCreatedOn]) {
					aDelivery.message.createdOn = [resultsDict BC_numberForKey:kCreatedOn];
				}
				if ([resultsDict objectForKey:kUpdatedOn]) {
					aDelivery.message.updatedOn = [resultsDict BC_numberForKey:kUpdatedOn];
				}
				for (NSDictionary *deliveryDict in [resultsDict objectForKey:kMessageDeliveries]) {
					if ([[deliveryDict BC_numberOrNilForKey:kUserId] isEqualToNumber:aDelivery.loginId]) {
						aDelivery.cloudId = [deliveryDict BC_numberForKey:kId];
						aDelivery.updatedOn = [deliveryDict BC_numberForKey:kUpdatedOn];
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForMessages"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}
- (BCObjectManagerReturnCode)getUpdatesForMessages:(TaskQueue *)taskQueue
{
	static NSString *messagesFetcher = @"MessagesFetcher";

	NSNumber *loginId = [User loginId];

	NSPredicate *loginIdPredicate = [NSPredicate predicateWithFormat:@"loginId = %@", loginId];
	NSString *table = kSyncMessages;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory messagesUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:messagesFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:self userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Messages, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:messagesFetcher];

		if (!error) {
			// fetch succeeded
			NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			NSArray *messages = [resultsDict objectForKey:kMessages];

			// process any updates
			if ([messages count] > 0) {
				NSArray *cachedSentMessages = [MessageMO MR_findByAttribute:MessageMOAttributes.loginId withValue:loginId
					inContext:self.managedObjectContext];
				NSArray *cachedReceivedMessages = [MessageDeliveryMO MR_findByAttribute:MessageDeliveryMOAttributes.loginId
					withValue:loginId inContext:self.managedObjectContext];
				NSArray *cachedMessages = [cachedSentMessages arrayByAddingObjectsFromArray:[cachedReceivedMessages valueForKeyPath:@"message"]];
				for (NSDictionary *aMessage in messages) {
					NSNumber *cloudId = [[aMessage valueForKey:kMessageId] numberValueDecimal];

					// this code block is used below to grab the index of the matching item by cloudid
					// cloud_id comes back from the cloud as a string, so just querying the array using
					// a predicate wasn't working
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						return ([[obj cloudId] isEqualToNumber:cloudId]);
					};

					NSUInteger index = [cachedMessages indexOfObjectPassingTest:SameCloudId];

					MessageMO *message = nil;
					if (index == NSNotFound) {
						message = [MessageMO insertInManagedObjectContext:self.managedObjectContext];
					}
					else {
						message = [cachedMessages objectAtIndex:index];
					}

					[message setWithUpdateDictionary:aMessage];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// We need to get the max of both messages and deliveries
			// Get the new lastUpdate
			NSNumber *lastUpdatedMessage = [MessageMO MR_aggregateOperation:@"max:" onAttribute:MessageMOAttributes.updatedOn
				withPredicate:loginIdPredicate inContext:self.managedObjectContext];
			NSNumber *lastUpdatedDelivery = [MessageDeliveryMO MR_aggregateOperation:@"max:" onAttribute:MessageDeliveryMOAttributes.updatedOn
				withPredicate:loginIdPredicate inContext:self.managedObjectContext];
			NSNumber *lastUpdate = @(MAX([lastUpdatedMessage integerValue], [lastUpdatedDelivery integerValue]));
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForMessages"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the messages
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[MessageMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];

	return Success;
}

#pragma mark - Fav Stores
- (BCObjectManagerReturnCode)sendUpdatesForUserStores:(TaskQueue *)taskQueue
{
	NSFetchRequest *updatedItemsRequest = [[NSFetchRequest alloc] init];
	updatedItemsRequest.entity = [UserStoreMO entityInManagedObjectContext:self.managedObjectContext];
	updatedItemsRequest.predicate = [NSPredicate predicateWithFormat:@"accountId = %@ and status <> %@", 
		[[User currentUser] accountId], kStatusOk];

	NSArray *updatedStores = [self.managedObjectContext executeFetchRequest:updatedItemsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForUserStores" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory entitiesURL];
	for (UserStoreMO *aStore in updatedStores) {
		NSString *status = aStore.status;

		NSData *jsonData = nil;
		if ([aStore.storeEntityId integerValue] == 0) {
			jsonData = [aStore toJSONWithEntityData];
		}
		else {
			jsonData = [aStore toJSON];
		}

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:jsonData];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *storeDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([storeDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:aStore];
					}
					else {
						aStore.status = kStatusOk;
						if ([storeDict objectForKey:kCloudId] != nil) {
							aStore.cloudId = [[storeDict valueForKey:kCloudId] numberValueDecimal];
						}
						if ([storeDict objectForKey:kEntityId] != nil) {
							aStore.storeEntityId = [[storeDict valueForKey:kEntityId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForUserStores" 
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}
- (BCObjectManagerReturnCode)getUpdatesForUserStores:(TaskQueue *)taskQueue
{
	static NSString *userStoresFetcher = @"UserStoresFetcher";
	
	NSNumber *loginId = [User loginId];
	NSNumber *accountId = [User accountId];

	NSURL *url = [BCUrlFactory userStoresUrl];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:userStoresFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Stores, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:userStoresFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *storeUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			// process any updates
			if ([storeUpdates count] > 0) {
				for (NSMutableDictionary *anItem in storeUpdates) {
					NSNumber *cloudId = [[anItem valueForKey:kId] numberValueDecimal];

					UserStoreMO *store = [UserStoreMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@",
							UserStoreMOAttributes.cloudId, cloudId, UserStoreMOAttributes.accountId, accountId]
						inContext:self.managedObjectContext];

					if (!store) {
						store = [UserStoreMO insertInManagedObjectContext:self.managedObjectContext];
					}

					[store setWithDictionary:anItem];
				}
			}
			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:kSyncUserStores forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForUserStores" 
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the user stores
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[UserStoreMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - Purchase Hx
- (BCObjectManagerReturnCode)sendUpdatesForPurchaseHx:(TaskQueue *)taskQueue
{
	// at this time, we don't send purchase hx 
	[taskQueue runQueue:nil];
	return Success;
}
- (BCObjectManagerReturnCode)getUpdatesForPurchaseHx:(TaskQueue *)taskQueue
{
	static NSString *purchaseHxFetcher = @"PurchaseHxFetcher";

	NSNumber *loginId = [User loginId];
	NSNumber *accountId = [User accountId];

	NSPredicate *accountIdPredicate = [NSPredicate predicateWithFormat:@"accountId = %@", accountId];
	NSString *table = kSyncPurchaseHx;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [PurchaseHxItem MR_aggregateOperation:@"max:" onAttribute:PurchaseHxItemAttributes.updatedOn
			withPredicate:accountIdPredicate inContext:self.managedObjectContext];
	}
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory purchaseHistoryUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:purchaseHxFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_PurchaseHistory, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];
		
		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:purchaseHxFetcher];

		if (!error) {
			NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			NSArray *purchaseUpdates = [resultsDict objectForKey:kPurchaseHx];
			// process any updates
			if ([purchaseUpdates count] > 0) {
				NSArray *cachedPurchaseHxItems = [PurchaseHxItem MR_findAllWithPredicate:accountIdPredicate inContext:self.managedObjectContext];

				for (NSMutableDictionary *aPurchase in purchaseUpdates) {
					NSString *name = [aPurchase valueForKey:kItemName];
					NSNumber *productId = [[aPurchase valueForKey:kProductId] numberValueDecimal];

					// this code block is used below to grab the index of the matching item by name and productId
					BOOL (^SameNameAndProductId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						PurchaseHxItem *item = (PurchaseHxItem *)obj;
						return ([item.name isEqualToString:name] && [item.productId isEqualToNumber:productId]);
					};

					NSUInteger index = [cachedPurchaseHxItems indexOfObjectPassingTest:SameNameAndProductId];

					PurchaseHxItem *purchaseHxItem = nil;
					if (index == NSNotFound) {
						purchaseHxItem = [PurchaseHxItem insertInManagedObjectContext:self.managedObjectContext];
						purchaseHxItem.accountId = accountId;
					}
					else {
						purchaseHxItem = [cachedPurchaseHxItems objectAtIndex:index];
					}

					[purchaseHxItem setWithUpdateDictionary:aPurchase];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [PurchaseHxItem MR_aggregateOperation:@"max:" onAttribute:PurchaseHxItemAttributes.updatedOn
				withPredicate:accountIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForPurchaseHx"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the purchase history
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[PurchaseHxItem entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - Categories
- (BCObjectManagerReturnCode)sendUpdatesForCategories:(TaskQueue *)taskQueue
{
	// at this time, no updates are allowed on the phone
	[taskQueue runQueue:nil];
	return Success;
}
- (BCObjectManagerReturnCode)getUpdatesForCategories:(TaskQueue *)taskQueue
{
	static NSString *categoriesFetcher = @"CategoriesFetcher";

	// get the Category entity
	NSEntityDescription *entity = [CategoryMO entityInManagedObjectContext:self.managedObjectContext];

	NSURL *url = [BCUrlFactory categoriesURL];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:categoriesFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_ProductCategories, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:categoriesFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *categoryUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			// process any updates
			if ([categoryUpdates count] > 0) {
				// build a predicate to find all the items matching the items sent down
				NSPredicate *matchingIdPredicate = [NSPredicate predicateWithFormat:@"cloudId in %@", [categoryUpdates valueForKeyPath:kId]];
				NSArray *matchingCategories = [CategoryMO MR_findAllWithPredicate:matchingIdPredicate inContext:self.managedObjectContext];

				NSMutableDictionary *managedObjectsByCloudId = [NSMutableDictionary dictionary];
				for (CategoryMO *category in matchingCategories) {
					[managedObjectsByCloudId setObject:category forKey:category.cloudId];
				}

				for (NSMutableDictionary *aCategory in categoryUpdates) {
					NSNumber *cloudId = [[aCategory valueForKey:kId] numberValueDecimal];

					CategoryMO *category = [managedObjectsByCloudId objectForKey:cloudId];

					if (!category) {
						category = [NSEntityDescription insertNewObjectForEntityForName:
							[entity name] inManagedObjectContext:self.managedObjectContext];
					}

					category.cloudId = cloudId;
					category.name = [aCategory valueForKey:@"name"];
				}
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:kSyncCategories forUser:nil inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForCategories"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the categories
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[CategoryMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

- (BCObjectManagerReturnCode)sendUpdatesForStoreCategories:(TaskQueue *)taskQueue
{
	NSFetchRequest *updatedItemsRequest = [[NSFetchRequest alloc] init];
	updatedItemsRequest.entity = [UserStoreMO entityInManagedObjectContext:self.managedObjectContext];
	updatedItemsRequest.predicate = [NSPredicate predicateWithFormat:@"accountId = %@ and storeCategoryStatus <> %@", 
		[[User currentUser] accountId], kStatusOk];

	NSArray *updatedStores = [self.managedObjectContext executeFetchRequest:updatedItemsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForStoreCategories" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory storeCategoriesUrl];
	for (UserStoreMO *aStore in updatedStores) {
		// Send an update to the cloud for this UserStoreMO's storecategories
		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:aStore.storeCategoryStatus postData:[aStore toJSONWithCategories]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				aStore.storeCategoryStatus = kStatusOk;
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForStoreCategories" 
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForStoreCategories:(TaskQueue *)taskQueue
{
	// NOTE:  user stores should be updated prior to this call
	static NSString *storeCategoriesFetcher = @"StoreCategoriesFetcher";

	NSNumber *loginId = [User loginId];
	NSNumber *accountId = [User accountId];
	NSArray *userStores = [UserStoreMO MR_findByAttribute:UserStoreMOAttributes.accountId withValue:accountId inContext:self.managedObjectContext];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"getUpdatesForStoreCategories" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Post a notification that we've updated the store categories
			[[NSNotificationCenter defaultCenter]
				postNotificationName:[StoreCategory entityName]
				object:self userInfo:nil];

			if (!error) {
				// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
				[SyncUpdateMO setLastUpdate:@0 forTable:kSyncCategorySorting forUser:loginId inContext:self.managedObjectContext];

				// Save the context.
				[self.managedObjectContext BL_save];
			}

			[taskQueue runQueue:error];
		}];

	for (UserStoreMO *aStore in userStores) {
		// Fetch the storecategories for this UserStoreMO and see if they need
		// to be updated in the persistent store
		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory storeCategoriesUrlForStoreId:aStore.storeEntityId]];

		// Register this fetcher task so that if the app gets put into the background during this call,
		// we have a way to cancel the call
		[TaskQueueItems registerTaskWithFetcher:fetcher identifier:storeCategoriesFetcher];

		[appDelegate setNetworkActivityIndicatorVisible:YES];
		
		[[NSNotificationCenter defaultCenter]
		 postNotificationName:kSyncUpdateUserStatusNotification
		 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_StoreCategories, nil)}];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			// Finished with this fetch, unregister
			[TaskQueueItems unregisterTask:storeCategoriesFetcher];

			if (!error) {
				// fetch succeeded
				NSArray *cloudStoreCategories = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if (![cloudStoreCategories count]) {
					// Nothing came from the cloud, so skip
					return;
				}

				// Save the store categories
				NSArray *storeCategories = [aStore.categories allObjects];
				for (NSDictionary *cloudStoreCat in cloudStoreCategories) {
					NSNumber *cloudCatId = [[cloudStoreCat valueForKey:kStoreCategoryId] numberValueDecimal];
					NSNumber *cloudSortOrder = [[cloudStoreCat valueForKey:kStoreCategorySortOrder] numberValueDecimal];

					// Code block for finding our core-data store category
					BOOL (^SameCategory)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						StoreCategory *item = (StoreCategory *)obj;
						return ([item.category.cloudId isEqualToNumber:cloudCatId]);
					};

					NSUInteger index = [storeCategories indexOfObjectPassingTest:SameCategory];
					if (index == NSNotFound) {
						StoreCategory *newStoreCat = [StoreCategory insertInManagedObjectContext:self.managedObjectContext];
						newStoreCat.sortOrder = cloudSortOrder;
						newStoreCat.store = aStore;
						newStoreCat.category = [CategoryMO categoryById:cloudCatId withManagedObjectContext:self.managedObjectContext];
					}
					else {
						StoreCategory *storeCat = [storeCategories objectAtIndex:index];

						if (![storeCat.sortOrder isEqualToNumber:cloudSortOrder]) {
							storeCat.sortOrder = cloudSortOrder;
						}
					}
				}

				// Save the context.
				[self.managedObjectContext BL_save];

				// Send a notification that this store's categories got altered
				[[NSNotificationCenter defaultCenter]
					postNotificationName:UserStoresDidChangeStoreCategories
					object:self userInfo:@{UserStoresDidChangeStoreId : aStore.storeEntityId}];
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForStoreCategories" withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

#pragma mark - Pantry
- (BCObjectManagerReturnCode)sendUpdatesForPantry:(TaskQueue *)taskQueue
{
	// get the PantryItem entity
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"PantryItem" inManagedObjectContext:self.managedObjectContext];

	// find the updated items
	NSPredicate *updatedPredicate =
		[NSPredicate predicateWithFormat:@"accountId = %@ and status <> %@", 
		[[User currentUser] accountId], kStatusOk];

	NSFetchRequest *updatedItemsRequest = [[NSFetchRequest alloc] init];
	updatedItemsRequest.entity = entity;
	updatedItemsRequest.predicate = updatedPredicate;

	NSArray *updatedPantryItems =
		[self.managedObjectContext executeFetchRequest:updatedItemsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForPantry" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory pantryURL];
	for (PantryItem *anItem in updatedPantryItems) {
		NSString *status = anItem.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[anItem toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:anItem];
					}
					else {
						anItem.status = kStatusOk;
						if ([resultsDict objectForKey:kCloudId] != nil) {
							anItem.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForPantry"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}
- (BCObjectManagerReturnCode)getUpdatesForPantry:(TaskQueue *)taskQueue
{
	static NSString *pantryFetcher = @"PantryFetcher";

	NSNumber *loginId = [User loginId];
	NSNumber *accountId = [User accountId];

	NSPredicate *accountIdPredicate = [NSPredicate predicateWithFormat:@"accountId = %@", accountId];
	NSString *table = kSyncPantry;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [PantryItem MR_aggregateOperation:@"max:" onAttribute:PantryItemAttributes.updatedOn
			withPredicate:accountIdPredicate inContext:self.managedObjectContext];
	}

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory pantryUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:pantryFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Pantry, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:pantryFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *pantryUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			// process any updates
			if ([pantryUpdates count] > 0) {
				NSArray *cachedPantryItems = [PantryItem MR_findAllWithPredicate:accountIdPredicate inContext:self.managedObjectContext];
				for (NSMutableDictionary *updateObject in pantryUpdates) {
					NSNumber *cloudId = [[updateObject valueForKey:kId] numberValueDecimal];

					// this code block is used below to grab the index of the matching item by cloudid
					// cloud_id comes back from the cloud as a string, so just querying the array using
					// a predicate wasn't working
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						PantryItem *item = (PantryItem *)obj;
						return ([item.cloudId isEqualToNumber:cloudId]);
					};

					NSUInteger index = [cachedPantryItems indexOfObjectPassingTest:SameCloudId];

					BOOL deleted = [[updateObject valueForKey:kDeleted] boolValue];

					PantryItem *pantryItem = nil;
					if (index == NSNotFound) {
						// this has to be checked inside NSNotFound - because the item may not exist on 
						// the phone but has been marked deleted on the web
						if (!deleted) {
							pantryItem = [PantryItem insertInManagedObjectContext:self.managedObjectContext];
							pantryItem.accountId = accountId;
						}
					}
					else {
						pantryItem = [cachedPantryItems objectAtIndex:index];

						if (deleted) {
							[self.managedObjectContext deleteObject:pantryItem];
							continue;
						}
					}

					[pantryItem setWithUpdateDictionary:updateObject];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [PantryItem MR_aggregateOperation:@"max:" onAttribute:PantryItemAttributes.updatedOn
				withPredicate:accountIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForPantry"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the pantry items
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[PantryItem entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - Single Use IDs
- (BCObjectManagerReturnCode)sendUpdatesForSingleUseTokens:(TaskQueue *)taskQueue
{
	// get the SingleUseToken entity
	NSEntityDescription *entity = [NSEntityDescription
		entityForName:@"SingleUseToken" inManagedObjectContext:self.managedObjectContext];

	// find any viewed tokens
	NSPredicate *viewedPredicate =
		[NSPredicate predicateWithFormat:@"timeViewed <> 0"];

	NSFetchRequest *viewedItemsRequest = [[NSFetchRequest alloc] init];
	viewedItemsRequest.entity = entity;
	viewedItemsRequest.predicate = viewedPredicate;

	NSArray *viewedTokens =
		[self.managedObjectContext executeFetchRequest:viewedItemsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForSingleUseTokens" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory singleUseURL];
	for (SingleUseToken *aToken in viewedTokens) {
		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:kPut postData:[aToken toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				[self.managedObjectContext deleteObject:aToken];
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForSingleUseTokens"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForSingleUseTokens:(TaskQueue *)taskQueue
{
	static NSString *singleuseTokenFetcher = @"SingleUseTokenFetcher";

	NSNumber *accountId = [User accountId];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory singleUseURL]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:singleuseTokenFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Tokens, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:singleuseTokenFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *newTokens = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			// process any updates
			if ([newTokens count] > 0) {
				// delete all of the single use tokens that aren't returned by the API
				NSPredicate *unmatchedTokenPredicate = [NSPredicate predicateWithFormat:@"NOT (token in %@)", [newTokens valueForKeyPath:@"su_id"]];
				NSArray *unmatchedTokens = [SingleUseToken MR_findAllWithPredicate:unmatchedTokenPredicate inContext:self.managedObjectContext];
				[unmatchedTokens enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
					[self.managedObjectContext deleteObject:obj];
				}];

				// build a dictionary with all the items matching the items returned by the API
				NSPredicate *matchingTokenPredicate = [NSPredicate predicateWithFormat:@"token in %@", [newTokens valueForKeyPath:@"su_id"]];
				NSArray *matchingTokens = [SingleUseToken MR_findAllWithPredicate:matchingTokenPredicate inContext:self.managedObjectContext];
				NSMutableDictionary *managedObjectsByToken = [NSMutableDictionary dictionary];
				for (SingleUseToken *existingToken in matchingTokens) {
					[managedObjectsByToken setObject:existingToken forKey:existingToken.token];
				}

				for (NSMutableDictionary *newToken in newTokens) {
					NSString *token = [newToken valueForKey:kSingleUseTokenKey];

					SingleUseToken *singleUseToken = [managedObjectsByToken objectForKey:token];

					// if the token doesn't exist yet, insert it
					if (!singleUseToken) {
						singleUseToken = [SingleUseToken insertInManagedObjectContext:self.managedObjectContext];
						singleUseToken.token = token;
						singleUseToken.accountId = accountId;
					}
				}

				// Save the context.
				[self.managedObjectContext BL_save];
			}
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForSingleUseTokens"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the single use ids
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[SingleUseToken entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - Promotions
- (BCObjectManagerReturnCode)sendUpdatesForPromotions:(TaskQueue *)taskQueue
{
	// get the Promotion entity
	NSEntityDescription *entity = [NSEntityDescription
		entityForName:@"Promotion" inManagedObjectContext:self.managedObjectContext];

	// find any updated promotions
	NSPredicate *updatedPredicate =
		[NSPredicate predicateWithFormat:@"shoppingListItem.accountId = %@ and status <> %@", 
		[[User currentUser] accountId], kStatusOk];

	NSFetchRequest *updatedPromotionsRequest = [[NSFetchRequest alloc] init];
	updatedPromotionsRequest.entity = entity;
	updatedPromotionsRequest.predicate = updatedPredicate;

	NSArray *updatedPromotions =
		[self.managedObjectContext executeFetchRequest:updatedPromotionsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForPromotions" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory promotionsURL];
	for (Promotion *aPromotion in updatedPromotions) {
		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:kPost postData:[aPromotion toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					aPromotion.status = kStatusOk;
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForPromotions"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];

	}

	[subQueue runQueue:nil];

	return Success;
}

#pragma mark - Meals
- (BCObjectManagerReturnCode)sendUpdatesForMeals:(TaskQueue *)taskQueue
{
	// fetch all the updated meals in the planner for this user
	NSArray *updatedMeals = [RecipeMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"accountId = %@ and status <> %@", 
			[[User currentUser] accountId], kStatusOk]
		inContext:self.managedObjectContext];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForMeals" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory mealsURL];
	for (RecipeMO *aMeal in updatedMeals) {
		NSString *status = aMeal.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[aMeal toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:aMeal];
					}
					else {
						aMeal.status = kStatusOk;
						if ([resultsDict objectForKey:kCloudId] != nil) {
							aMeal.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForMeals"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForMeals:(TaskQueue *)taskQueue
{
	static NSString *mealsFetcherId = @"MealsFetcher";
	NSUInteger limit = 250;

	NSNumber *loginId = [User loginId];
	NSNumber *accountId = [User accountId];

	NSPredicate *accountIdPredicate = [NSPredicate predicateWithFormat:@"accountId = %@", accountId];
	NSString *table = kSyncRecipes;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [RecipeMO MR_aggregateOperation:@"max:" onAttribute:RecipeMOAttributes.updatedOn
			withPredicate:accountIdPredicate inContext:self.managedObjectContext];
	}

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Recipes, nil)}];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"getUpdatesForMeals" completionBlock:
		^void(NSString *identifier, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			// Finished with this fetch, unregister
			[TaskQueueItems unregisterTask:mealsFetcherId];

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [RecipeMO MR_aggregateOperation:@"max:" onAttribute:RecipeMOAttributes.updatedOn
				withPredicate:accountIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];

			// Post a notification that we've updated the meals
			[[NSNotificationCenter defaultCenter]
				postNotificationName:[RecipeMO entityName]
				object:self userInfo:nil];

			// Tell the queue that we're done, and it can continue running
			[taskQueue runQueue:error];
		}];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory mealsUrlNewerThan:lastUpdate withOffset:0 andLimit:limit]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:mealsFetcherId];

	__block __weak void (^weakCompletionHandler)(NSData *data, NSError *error);
	void (^CompletionHandler)(NSData *data, NSError *error);
	weakCompletionHandler = CompletionHandler = ^void(NSData *retrievedData, NSError *error) {

		if (!error) {
			// fetch succeeded
			NSDictionary *updates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			NSArray *mealUpdates = [updates objectForKey:kRecipes];
			// Make this fetch in batches, and iterate
			BOOL more = [[updates BC_numberForKey:kMore] boolValue];

			// process any updates
			if ([mealUpdates count] > 0) {
				NSArray *cachedMeals = [RecipeMO MR_findAllWithPredicate:accountIdPredicate inContext:self.managedObjectContext];
				for (NSMutableDictionary *updateObject in mealUpdates) {
					NSNumber *cloudId = [[updateObject valueForKey:kId] numberValueDecimal];

					// this code block is used below to grab the index of the matching item by cloudid
					// cloud_id comes back from the cloud as a string, so just querying the array using
					// a predicate wasn't working
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						RecipeMO *item = (RecipeMO *)obj;
						return ([item.cloudId isEqualToNumber:cloudId]);
					};

					NSUInteger index = [cachedMeals indexOfObjectPassingTest:SameCloudId];

					// NOTE we are not checking the delete flag on Meals and this is intentional
					// in order to maintain ties with meal planner days... I don't like it, 
					// but haven't found a better solution yet.

					RecipeMO *recipe = nil;
					if (index == NSNotFound) {
						recipe = [RecipeMO insertInManagedObjectContext:self.managedObjectContext];
						recipe.accountId = accountId;
					}
					else {
						recipe = [cachedMeals objectAtIndex:index];
					}

					[recipe setWithUpdateDictionary:updateObject];
				}

			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// If there are more rows to be fetched, add a new sub queue item to perform this fetch
			if (more) {
				NSUInteger offset = [[updates BC_numberForKey:kOffset] integerValue];
				GTMHTTPFetcher *iterFetcher = [GTMHTTPFetcher signedFetcherWithURL:
					[BCUrlFactory mealsUrlNewerThan:lastUpdate withOffset:offset andLimit:limit]];

				[subQueue addRequest:iterFetcher completionBlock:weakCompletionHandler];
			}
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForMeals"
				withFetcher:fetcher retrievedData:retrievedData];
		}
	};

	// Add the first request, then run the sub queue
	[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	[subQueue runQueue:nil];

	return Success;
}

#pragma mark - ActivityPlan
- (void)sendUpdatesForActivityPlan:(TaskQueue *)taskQueue
{
	// TODO no ability to send updates for activity plan yet
	[taskQueue runQueue:nil];
}

#pragma mark - MealPlanners
- (BCObjectManagerReturnCode)sendUpdatesForMealPlanners:(TaskQueue *)taskQueue
{
	// fetch all the updated meals in the planner for this user
	NSFetchRequest *updatedMealsRequest = [MealPlannerDay MR_createFetchRequestInContext:self.managedObjectContext];
	updatedMealsRequest.predicate = [NSPredicate predicateWithFormat:@"accountId = %@ and status <> %@", 
		[[User currentUser] accountId], kStatusOk];

	NSSortDescriptor *sortByDay = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
	NSSortDescriptor *sortBySortOrder = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];
	updatedMealsRequest.sortDescriptors = @[sortByDay, sortBySortOrder];

	NSArray *updatedMealPlans = [self.managedObjectContext executeFetchRequest:updatedMealsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForMealPlanners" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory mealPlannerURL];
	for (MealPlannerDay *mpd in updatedMealPlans) {
		NSString *status = mpd.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[mpd toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:mpd];
					}
					else {
						mpd.status = kStatusOk;
						if ([resultsDict objectForKey:kCloudId] != nil) {
							mpd.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForMealPlanners"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForMealPlanners:(TaskQueue *)taskQueue
{
	static NSString *mealPlannerFetcher = @"MealPlannerFetcher";

	NSNumber *loginId = [User loginId];
	NSNumber *accountId = [User accountId];

	NSPredicate *accountIdPredicate = [NSPredicate predicateWithFormat:@"accountId = %@", accountId];
	NSString *table = kSyncMealPlan;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [MealPlannerDay MR_aggregateOperation:@"max:" onAttribute:MealPlannerDayAttributes.updatedOn
			withPredicate:accountIdPredicate inContext:self.managedObjectContext];
	}

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory mealPlannerURLNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:mealPlannerFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_MealPlanner, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:mealPlannerFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *plannerUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			// process any updates
			if ([plannerUpdates count] > 0) {
				NSArray *cachedMPDs = [MealPlannerDay MR_findAllWithPredicate:accountIdPredicate inContext:self.managedObjectContext];
				for (NSMutableDictionary *updateObject in plannerUpdates) {
					NSNumber *cloudId = [[updateObject valueForKey:kId] numberValueDecimal];

					// this code block is used below to grab the index of the matching item by cloudid
					// cloud_id comes back from the cloud as a string, so just querying the array using
					// a predicate wasn't working
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						MealPlannerDay *item = (MealPlannerDay *)obj;
						return ([item.cloudId isEqualToNumber:cloudId]);
					};

					NSUInteger index = [cachedMPDs indexOfObjectPassingTest:SameCloudId];

					BOOL deleted = [[updateObject valueForKey:kDeleted] boolValue];

					MealPlannerDay *mealPlannerDay = nil;
					if (index == NSNotFound) {
						// this has to be checked inside NSNotFound - because the item may not exist on 
						// the phone but has been marked deleted on the web
						if (!deleted) {
							mealPlannerDay = [MealPlannerDay insertInManagedObjectContext:self.managedObjectContext];
							mealPlannerDay.accountId = accountId;
						}
					}
					else {
						mealPlannerDay = [cachedMPDs objectAtIndex:index];

						if (deleted) {
							[self.managedObjectContext deleteObject:mealPlannerDay];
							continue;
						}
					}

					[mealPlannerDay setWithUpdateDictionary:updateObject];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [MealPlannerDay MR_aggregateOperation:@"max:" onAttribute:MealPlannerDayAttributes.updatedOn
				withPredicate:accountIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForMealPlanners"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the meals
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[MealPlannerDay entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - HealthChoices - part of the health profile
// NOTE this is a HACK
- (void)fixHealthChoices
{
	// since I am deleting the maxChoices, I am using them as the check to see if this has already been done
	NSArray *maxChoices = [HealthChoiceMO MR_findAllSortedBy:HealthChoiceMOAttributes.name ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"filter BEGINSWITH %@", @"max_"] inContext:self.managedObjectContext];
	if (maxChoices.count) {
		for (HealthChoiceMO *choice in maxChoices) {
			[self.managedObjectContext deleteObject:choice];
		}

		NSArray *lifestyleChoices = [HealthChoiceMO MR_findAllSortedBy:HealthChoiceMOAttributes.name ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"filter contains %@", @"lifestyle"] inContext:self.managedObjectContext];
		for (HealthChoiceMO *choice in lifestyleChoices) {
			choice.choiceType = @1;
		}

		NSArray *ingredientChoices = [HealthChoiceMO MR_findAllSortedBy:HealthChoiceMOAttributes.name ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"filter BEGINSWITH %@", @"ingredient_"] inContext:self.managedObjectContext];
		for (HealthChoiceMO *choice in ingredientChoices) {
			choice.choiceType = @2;
		}

		[self.managedObjectContext BL_save];
	}
}

- (void)sendUpdatesForHealthChoices:(TaskQueue *)taskQueue
{
	[self fixHealthChoices];

	// check if there are any userHealthChoices marked PUT
	NSFetchRequest *updatedHealthChoicesRequest = [UserHealthChoice MR_requestAllSortedBy:@"choice.filter" ascending:YES 
		withPredicate:[NSPredicate predicateWithFormat:@"loginId = %@ and status = %@", 
			[[User currentUser] loginId], kStatusPut] inContext:self.managedObjectContext];

	BOOL updatesToSend = ([self.managedObjectContext countForFetchRequest:updatedHealthChoicesRequest error:nil] > 0);

	if (updatesToSend) {
		NSArray *updatedChoices = [[self.managedObjectContext executeFetchRequest:updatedHealthChoicesRequest error:nil] mutableCopy];

		NSMutableDictionary *healthFiltersDict = [[NSMutableDictionary alloc] init];

		// "allergies":{"filter_eggs":0}
		// null - default OFF
		// 0/1 - means ON (1 is possible, but not set anymore - legacy)
		NSArray *allergiesON = [UserHealthChoice MR_findAllWithPredicate:[NSPredicate predicateWithFormat:
			@"loginId = %@ AND choice.choiceType = 0 AND value != nil AND status != %@", [[User currentUser] loginId], kStatusDelete] 
				inContext:self.managedObjectContext];

		if ([allergiesON count]) {
			NSMutableDictionary *allergiesDict = [[NSMutableDictionary alloc] init];
			for (UserHealthChoice *allergy in allergiesON) {
				[allergiesDict setValue:allergy.value ? allergy.value : [NSNull null] forKey:allergy.choice.filter];
			}
			[healthFiltersDict setValue:allergiesDict forKey:kHealthFilterAllergies];
		}
		else {
			[healthFiltersDict setValue:[NSNull null] forKey:kHealthFilterAllergies];
		}

		// "lifestyle":{"filter_calcium":1,"filter_fatfree":0}
		// null - default OFF
		// 0/1 - means ON
		NSArray *updatedLifestyles = [UserHealthChoice MR_findAllWithPredicate:[NSPredicate predicateWithFormat:
			@"loginId = %@ AND choice.choiceType = 1 AND value != nil AND status != %@", [[User currentUser] loginId], kStatusDelete] 
				inContext:self.managedObjectContext];

		if ([updatedLifestyles count]) {
			NSMutableDictionary *lifestyleDict = [[NSMutableDictionary alloc] init];
			for (UserHealthChoice *lifestyle in updatedLifestyles) {
				[lifestyleDict setValue:lifestyle.value ? lifestyle.value : [NSNull null] forKey:lifestyle.choice.filter];
			}
			[healthFiltersDict setValue:lifestyleDict forKey:kHealthFilterLifestyles];
		}
		else {
			[healthFiltersDict setValue:[NSNull null] forKey:kHealthFilterLifestyles];
		}

		// "required_lifestyle":1
		// 0 - not required
		// 1 - required
		UserHealthChoice *lifestyleRequireAll = [UserHealthChoice MR_findFirstWithPredicate:
			[NSPredicate predicateWithFormat:@"loginId = %@ AND choice.choiceType = 1 AND choice.filter = %@",
			[[User currentUser] loginId], kHealthFilterLifestyleRequireAll] inContext:self.managedObjectContext];

		if (lifestyleRequireAll) {
			[healthFiltersDict setValue:[NSNumber numberWithInteger:1] forKey:kHealthFilterLifestyleRequireAll];
		}
		else {
			[healthFiltersDict setValue:[NSNumber numberWithInteger:0] forKey:kHealthFilterLifestyleRequireAll];
		}

		// "ingredients":{"Rennet", "Monosodium Glutamate"}
		// get all of the ingredients that aren't marked delete
		NSArray *updatedIngredientWarnings = [UserHealthChoice MR_findAllWithPredicate:[NSPredicate predicateWithFormat:
			@"loginId = %@ AND choice.choiceType = 2 AND status != %@", 
				[[User currentUser] loginId], kStatusDelete] inContext:self.managedObjectContext];

		if ([updatedIngredientWarnings count]) {
			[healthFiltersDict setValue:[updatedIngredientWarnings valueForKeyPath:@"name"] forKey:kHealthFilterIngredientWarnings];
		}
		else {
			[healthFiltersDict setValue:@"clear" forKey:kHealthFilterIngredientWarnings];
		}

		NSURL *url = [BCUrlFactory healthProfileURL];
		NSDictionary *updateDict = [NSDictionary dictionaryWithObject:healthFiltersDict forKey:kHealthFilters];
		NSData *jsonData = [NSJSONSerialization dataWithJSONObject:updateDict options:kNilOptions error:nil];

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:kStatusPut postData:jsonData];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					for (UserHealthChoice *choice in updatedChoices) {
						if ([choice.status isEqualToString:kStatusDelete]) {
							[self.managedObjectContext deleteObject:choice];
						}
						else {
							choice.status = kStatusOk;
						}
					}

					[self.managedObjectContext BL_save];
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForHealthChoices" withFetcher:fetcher retrievedData:retrievedData];
			}

			// Tell the queue that we're done, and it can continue running
			[taskQueue runQueue:error];
		};

		[fetcher beginFetchWithCompletionHandler:CompletionHandler];
	}
	else {
		[taskQueue runQueue:nil];
	}
}

- (BCObjectManagerReturnCode)getUpdatesForHealthChoices:(TaskQueue *)taskQueue
{
	static NSString *healthChoicesFetcher = @"HealthChoicesFetcher";

	NSNumber *loginId = [User loginId];

	// fetch all the health choices for this user
	NSArray *allHealthChoices = [HealthChoiceMO MR_findAllInContext:self.managedObjectContext];
	
	// get the current web health profile
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory healthProfileURL]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:healthChoicesFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_HealthChoices, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:healthChoicesFetcher];

		if (!error) {
			NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			NSMutableArray *userHealthChoices = [[UserHealthChoice 
				MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"loginId = %@", loginId] 
				inContext:self.managedObjectContext] mutableCopy];

			void (^SaveChoicesFromDictionary)(NSDictionary *) = ^(NSDictionary *dict) {
				for (NSString *key in dict) {
					BOOL (^SameChoiceFilter)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						UserHealthChoice *item = (UserHealthChoice *)obj;
						return ([item.choice.filter isEqualToString:key]);
					};

					BOOL (^SameFilter)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						HealthChoiceMO *item = (HealthChoiceMO *)obj;
						return ([item.filter isEqualToString:key]);
					};

					NSNumber *newValue = nil;
					if ((NSNull *)[dict objectForKey:key] != [NSNull null]) {
						newValue = [[dict valueForKey:key] numberValueDecimal];
					}

					NSUInteger allIndex = [allHealthChoices indexOfObjectPassingTest:SameFilter];
					NSAssert1((allIndex != NSNotFound), @"%@ not found in healthChoices", key);
					if (allIndex == NSNotFound) {
						// this shouldn't happen, but if a new health choice is added,
						// we currently have that list sort of hard coded and this won't 
						// save properly - so skip it until we find a dynamic solution
						continue;

						// TODO the right answer could be to just add it here, but we need a name and I would have to pass in a type
						//HealthChoiceMO *newChoice = [HealthChoiceMO insertInManagedObjectContext:self.managedObjectContext];
						//newChoice.name = ??
						//newChoice.filter = key;
						//newChoice.type = type;
					}

					NSUInteger userIndex = [userHealthChoices indexOfObjectPassingTest:SameChoiceFilter];
					if (userIndex == NSNotFound) {
						// create a new user health choice
						UserHealthChoice *newUserChoice = 
							[UserHealthChoice insertInManagedObjectContext:self.managedObjectContext];
						newUserChoice.value = newValue;
						newUserChoice.loginId = loginId;
						newUserChoice.status = kStatusOk;
						newUserChoice.choice = [allHealthChoices objectAtIndex:allIndex];
					}
					else {
						// modify the existing choice
						UserHealthChoice *userChoice = [userHealthChoices objectAtIndex:userIndex];
						userChoice.value = newValue;
						[userHealthChoices removeObjectAtIndex:userIndex];
					}
				}
			};

			SaveChoicesFromDictionary([resultsDict objectForKey:kHealthFilterAllergies]);
			SaveChoicesFromDictionary([resultsDict objectForKey:kHealthFilterLifestyles]);

			NSNumber *requireAll = [resultsDict objectForKey:kHealthFilterLifestyleRequireAll];
			if ([requireAll integerValue] == 0) {
				SaveChoicesFromDictionary([NSDictionary dictionaryWithObject:[NSNull null] forKey:kHealthFilterLifestyleRequireAll]);
			}
			else {
				SaveChoicesFromDictionary([NSDictionary dictionaryWithObject:requireAll forKey:kHealthFilterLifestyleRequireAll]);
			}

			// Ingredient warnings are different... 
			if ((NSNull *)[resultsDict objectForKey:kHealthFilterIngredientWarnings] != [NSNull null]) {
				for (NSString *ingredient in [resultsDict objectForKey:kHealthFilterIngredientWarnings]) {
					BOOL (^SameIngredientName)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						UserHealthChoice *item = (UserHealthChoice *)obj;
						return ([item.name isEqualToString:ingredient]);
					};
					BOOL (^SameFilter)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						HealthChoiceMO *item = (HealthChoiceMO *)obj;
						return ([item.filter isEqualToString:kHealthFilterIngredientWarnings]);
					};

					NSUInteger allIndex = [allHealthChoices indexOfObjectPassingTest:SameFilter];
					NSUInteger userIndex = [userHealthChoices indexOfObjectPassingTest:SameIngredientName];
					if (userIndex == NSNotFound) {
						// create a new user health choice
						UserHealthChoice *newUserChoice = 
							[UserHealthChoice insertInManagedObjectContext:self.managedObjectContext];
						newUserChoice.name = ingredient;
						newUserChoice.value = [NSNumber numberWithInteger:1];
						newUserChoice.loginId = loginId;
						newUserChoice.status = kStatusOk;
						newUserChoice.choice = [allHealthChoices objectAtIndex:allIndex];
					}
					else {
						// nothing to modify, just remove it from the tracking container
						//UserHealthChoice *userChoice = [userHealthChoices objectAtIndex:userIndex];
						//userChoice.value = ingredient;
						[userHealthChoices removeObjectAtIndex:userIndex];
					}
				}
			}

			// remove any remaining user health choices, this may be only useful for the
			// ingredient warnings, because I think we will get everything else every time.
			for (UserHealthChoice* deleteChoice in userHealthChoices) {
				[self.managedObjectContext deleteObject:deleteChoice];
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:kSyncUserHealthFilters forUser:loginId inContext:self.managedObjectContext];

			[self.managedObjectContext BL_save];

			// Post a notification that we've updated the healthchoices
			[[NSNotificationCenter defaultCenter]
				postNotificationName:[UserHealthChoice entityName]
				object:self userInfo:nil];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForHealthChoices" withFetcher:fetcher retrievedData:retrievedData];
		}

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - Recommendations
- (BCObjectManagerReturnCode)sendUpdatesForRecommendations:(TaskQueue *)taskQueue
{
	NSEntityDescription *entity = [NSEntityDescription
		entityForName:@"Recommendation" inManagedObjectContext:
		self.managedObjectContext];

	// find the updated items
	NSPredicate *updatedPredicate =
		[NSPredicate predicateWithFormat:@"accountId = %@ and status <> %@", 
		[[User currentUser] accountId], kStatusOk];

	NSFetchRequest *updatedItemsRequest = [[NSFetchRequest alloc] init];
	updatedItemsRequest.entity = entity;
	updatedItemsRequest.predicate = updatedPredicate;

	NSArray *updatedRecommendations =
		[self.managedObjectContext executeFetchRequest:updatedItemsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForRecommendations" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory productRecommendationsURLShowHidden:NO];
	for (Recommendation *aRecommendation in updatedRecommendations) {
		NSString *status = aRecommendation.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[aRecommendation toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					aRecommendation.status = kStatusOk;
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForRecommendations"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}
- (BCObjectManagerReturnCode)getUpdatesForRecommendations:(TaskQueue *)taskQueue
{
	static NSString *recommendationsFetcher = @"RecommendationsFetcher";

	NSNumber *loginId = [User loginId];
	NSNumber *accountId = [User accountId];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory productRecommendationsURLShowHidden:YES]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:recommendationsFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Recommendations, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:recommendationsFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *recommendationUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// process any updates
			if ([recommendationUpdates count] > 0) {
				// fetch all the cached recommendations for this user
				NSPredicate *accountIdPredicate = [NSPredicate predicateWithFormat:@"accountId = %@ AND status <> %@", accountId, kStatusDelete];
				NSArray *cachedRecommendations = [Recommendation MR_findAllWithPredicate:accountIdPredicate inContext:self.managedObjectContext];

				for (NSMutableDictionary *updateDict in recommendationUpdates) {
					NSString *name = [updateDict valueForKey:kItemName];
					NSNumber *productId = [[updateDict valueForKey:kProductId] numberValueDecimal];

					// this code block is used below to grab the index of the matching recommendation by name and productId
					BOOL (^SameNameAndProductId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						Recommendation *rec = (Recommendation *)obj;
						return ([rec.name isEqualToString:name] && [rec.productId isEqualToNumber:productId]);
					};

					NSUInteger cacheIndex = [cachedRecommendations indexOfObjectPassingTest:SameNameAndProductId];

					Recommendation *recommendation = nil;
					if (cacheIndex == NSNotFound) {
						// this has to be checked inside NSNotFound - because the item may not exist on 
						// the phone
						recommendation = [Recommendation insertInManagedObjectContext:self.managedObjectContext];
						recommendation.accountId = accountId;
					}
					else {
						recommendation = [cachedRecommendations objectAtIndex:cacheIndex];
					}

					[recommendation setWithUpdateDictionary:updateDict];
				}
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:kSyncShoppingSuggestions forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForRecommendations"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the recommendations
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[Recommendation entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - Food Log
- (BCObjectManagerReturnCode)sendUpdatesForFoodLog:(TaskQueue *)taskQueue
{
	// get the updated FoodLog items
	NSArray *updatedFoodLogs = [FoodLogMO MR_findAllSortedBy:FoodLogMOAttributes.creationDate ascending:YES
		withPredicate:[NSPredicate predicateWithFormat:@"loginId = %@ and status <> %@", [[User currentUser] loginId], kStatusOk]
		inContext:self.managedObjectContext];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForFoodLog" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory foodLogURL];
	for (FoodLogMO *aFoodLog in updatedFoodLogs) {
		NSString *status = aFoodLog.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[aFoodLog toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:aFoodLog];
					}
					else {
						aFoodLog.status = kStatusOk;
						if ([aFoodLog.cloudId integerValue] == 0 && [resultsDict objectForKey:kCloudId] != nil) {
							aFoodLog.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForFoodLog"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

#pragma mark - Logging Notes
- (void)sendUpdatesForLoggingNotes:(TaskQueue *)taskQueue
{
	// get the updated LoggingNotes
	NSArray *updatedLoggingNotes = [LoggingNoteMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"loginId = %@ and status <> %@", [[User currentUser] loginId], kStatusOk]
															inContext:self.managedObjectContext];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForLoggingNotes" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext save:&error];

			[taskQueue runQueue:error];
		}];

	for (LoggingNoteMO *aNote in updatedLoggingNotes) {
		NSString *status = aNote.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory loggingNotesURL] httpMethod:status postData:[aNote toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		[subQueue addRequest:fetcher completionBlock:^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (error) {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForLoggingNotes" withFetcher:fetcher retrievedData:retrievedData];
			}
			else {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:aNote];
					}
					else {
						aNote.status = kStatusOk;
						if (!aNote.cloudIdValue && [resultsDict BC_numberOrNilForKey:kCloudId]) {
							aNote.cloudId = [resultsDict BC_numberOrNilForKey:kCloudId];
						}
					}
				}
			}
		}];
	}

	[subQueue runQueue:nil];
}
- (void)getUpdatesForLoggingNotes:(TaskQueue *)taskQueue
{
	static NSString *loggingNotesFetcher = @"LoggingNotesFetcher";

	NSNumber *loginId = [User loginId];

	NSPredicate *loginIdPredicate = [NSPredicate predicateWithFormat:@"loginId = %@", loginId];
	NSString *table = kSyncLogNotes;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [LoggingNoteMO MR_aggregateOperation:@"max:" onAttribute:FoodLogMOAttributes.updatedOn
			withPredicate:loginIdPredicate inContext:self.managedObjectContext];
	}
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
			[BCUrlFactory loggingNotesURLNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:loggingNotesFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_FoodLogNotes, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:loggingNotesFetcher];

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForLoggingNotes" withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *loggingNotesUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// process any updates
			if (loggingNotesUpdates.count) {
				// fetch all the cached foodlogs for this user
				for (NSMutableDictionary *updateDict in loggingNotesUpdates) {
					NSNumber *cloudId = [updateDict BC_numberOrNilForKey:kId];

					LoggingNoteMO *note = [LoggingNoteMO MR_findFirstByAttribute:LoggingNoteMOAttributes.cloudId withValue:cloudId inContext:self.managedObjectContext];

					BOOL deleted = [[updateDict valueForKey:kDeleted] boolValue];

					if (!note) {
						// this has to be checked inside NSNotFound - because the item may not exist on the phone
						if (!deleted) {
							note = [LoggingNoteMO insertInManagedObjectContext:self.managedObjectContext];
							note.loginId = loginId;
							note.cloudId = cloudId;
						}
					}
					else {
						if (deleted) {
							[self.managedObjectContext deleteObject:note];
							continue;
						}
					}

					[note setWithUpdateDictionary:updateDict];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [LoggingNoteMO MR_aggregateOperation:@"max:" onAttribute:FoodLogMOAttributes.updatedOn
				withPredicate:loginIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}

		// Post a notification that we've updated the FoodLogMO
		[[NSNotificationCenter defaultCenter] postNotificationName:[LoggingNoteMO entityName]
												object:self
												userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];
}

#pragma mark - Food Log Recommendations
- (void)getUpdatesForMealPredictions:(TaskQueue *)taskQueue
{
	static NSString *mealPredictionFetcher = @"MealPredictionsFetcher";

	NSNumber *loginId = [User loginId];

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy.MM.dd"];
	NSString *todayString = [dateFormatter stringFromDate:[NSDate date]];

	// skip this if it has been done today already
	MealPredictionMO *aRecommendation = [MealPredictionMO MR_findFirstByAttribute:MealPredictionMOAttributes.login_id withValue:loginId
		inContext:self.managedObjectContext];
	if ([aRecommendation.date isEqualToString:todayString]) {
		[taskQueue runQueue:nil];
		return;
	}

	NSURL *url = [BCUrlFactory mealPredictionsURL];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:mealPredictionFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_MealPredictions, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:mealPredictionFetcher];

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:"fetchMealPredictions" withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// delete all the old rows - using a delete strategy here
			NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
			NSArray *oldRecommendations = [MealPredictionMO MR_findAllWithPredicate:
				[NSPredicate predicateWithFormat:@"login_id = %@", loginId] inContext:scratchContext];

			[oldRecommendations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
				[scratchContext deleteObject:obj];
			}];

			// fetch succeeded
			NSDictionary *mealPredictions = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			NSDictionary *userMealPredictions = mealPredictions[@"User"];

			if (userMealPredictions && [userMealPredictions isKindOfClass:[NSDictionary class]]) {
				NSArray *mealKeys = @[@"Breakfast", @"Morning Snack", @"Lunch", @"Afternoon Snack", @"Dinner", @"Evening Snack"];
				for (NSString *mealKey in mealKeys) {
					NSArray *recommendations = userMealPredictions[mealKey];
					for (NSDictionary *recommendation in recommendations) {
						MealPredictionMO *recommendationMO = [MealPredictionMO insertInManagedObjectContext:scratchContext];
						recommendationMO.login_id = loginId;
						recommendationMO.date = todayString;

						[recommendationMO setWithRecommendationDictionary:recommendation];
					}
				}
			}
			// Push the changes to the main context all at once
			[scratchContext BL_save];

			// Save the context.
			[self.managedObjectContext BL_save];
		}

		// Post a notification that we've updated the MealPredictionMO
		[[NSNotificationCenter defaultCenter] postNotificationName:[MealPredictionMO entityName] object:self
														  userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];
}

#pragma mark - Training Activities
- (BCObjectManagerReturnCode)sendUpdatesForTrainingActivity:(TaskQueue *)taskQueue
{
	// find the updated activity logs
	NSPredicate *updatedPredicate =
		[NSPredicate predicateWithFormat:@"loginId = %@ and status <> %@", 
		[[User currentUser] loginId], kStatusOk];

	NSFetchRequest *updatedItemsRequest = [[NSFetchRequest alloc] init];
	updatedItemsRequest.entity = [TrainingActivityMO 
		entityInManagedObjectContext:self.managedObjectContext];
	updatedItemsRequest.predicate = updatedPredicate;

	NSArray *updatedTrainingActivityLogs =
		[self.managedObjectContext executeFetchRequest:updatedItemsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForTrainingActivity" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory trainingActivityURL];
	for (TrainingActivityMO *aActivityLog in updatedTrainingActivityLogs) {
		NSString *status = aActivityLog.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[aActivityLog toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:aActivityLog];
					}
					else {
						aActivityLog.status = kStatusOk;
						if ([aActivityLog.cloudId integerValue] == 0 && [resultsDict objectForKey:kCloudId] != nil) {
							aActivityLog.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForTrainingActivity"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}
- (BCObjectManagerReturnCode)getUpdatesForTrainingActivity:(TaskQueue *)taskQueue
{
	static NSString *trainingActivityFetcher = @"trainingActivityFetcher";

	NSNumber *loginId = [User loginId];

	// fetch all the Activity logs for this user
	NSPredicate *loginIdPredicate = [NSPredicate predicateWithFormat:@"loginId = %@", loginId];
	NSString *table = kSyncExerciseActivities;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [TrainingActivityMO MR_aggregateOperation:@"max:" onAttribute:TrainingActivityMOAttributes.updatedOn
			withPredicate:loginIdPredicate inContext:self.managedObjectContext];
	}

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory trainingActivityUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:trainingActivityFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Activities, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:trainingActivityFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *activityLogUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// process any updates
			if ([activityLogUpdates count] > 0) {
				// fetch all the cached Activity logs for this user
				NSArray *cachedActivityLogs = [TrainingActivityMO MR_findAllWithPredicate:loginIdPredicate inContext:self.managedObjectContext];

				for (NSMutableDictionary *updateDict in activityLogUpdates) {
					NSNumber *cloudId = [[updateDict valueForKey:kId] numberValueDecimal];

					// this code block is used below to grab the index of the matching Activity log by id
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						TrainingActivityMO *activity = (TrainingActivityMO *)obj;
						return ([activity.cloudId isEqualToNumber:cloudId]);
					};

					NSUInteger cacheIndex = [cachedActivityLogs indexOfObjectPassingTest:SameCloudId];

					BOOL deleted = [[updateDict valueForKey:kDeleted] boolValue];

					TrainingActivityMO *activity = nil;
					if (cacheIndex == NSNotFound) {
						// this has to be checked inside NSNotFound - because the item may not exist on 
						// the phone
						if (!deleted) {
							activity = [TrainingActivityMO insertInManagedObjectContext:self.managedObjectContext];
							activity.loginId = loginId;
						}
					}
					else {
						activity = [cachedActivityLogs objectAtIndex:cacheIndex];

						if (deleted) {
							[self.managedObjectContext deleteObject:activity];
							continue;
						}
					}

					activity.cloudId = cloudId;
					[activity setWithUpdateDictionary:updateDict];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [TrainingActivityMO MR_aggregateOperation:@"max:" onAttribute:TrainingActivityMOAttributes.updatedOn
				withPredicate:loginIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForTrainingActivity"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the activities
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[TrainingActivityMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - Training Assessment
- (BCObjectManagerReturnCode)sendUpdatesForTrainingAssessment:(TaskQueue *)taskQueue
{
	// find the updated items
	NSPredicate *updatedPredicate =
		[NSPredicate predicateWithFormat:@"loginId = %@ and status <> %@", 
		[[User currentUser] loginId], kStatusOk];

	NSFetchRequest *updatedItemsRequest = [[NSFetchRequest alloc] init];
	updatedItemsRequest.entity = [TrainingAssessmentMO entityInManagedObjectContext:self.managedObjectContext];
	updatedItemsRequest.predicate = updatedPredicate;

	NSArray *updatedTrainingAssessmentLogs =
		[self.managedObjectContext executeFetchRequest:updatedItemsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForTrainingAssessment" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory trainingAssessmentURL];
	for (TrainingAssessmentMO *aAssessmentLog in updatedTrainingAssessmentLogs) {
		NSString *status = aAssessmentLog.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[aAssessmentLog toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:aAssessmentLog];
					}
					else {
						aAssessmentLog.status = kStatusOk;
						if ([aAssessmentLog.cloudId integerValue] == 0 && [resultsDict objectForKey:kCloudId] != nil) {
							aAssessmentLog.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForTrainingAssessment"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForTrainingAssessment:(TaskQueue *)taskQueue
{
	static NSString *trainingAssessmentFetcher = @"trainingAssessmentFetcher";

	NSNumber *loginId = [User loginId];

	// fetch all the logs for this user
	NSPredicate *loginIdPredicate = [NSPredicate predicateWithFormat:@"loginId = %@", loginId];
	NSString *table = kSyncExerciseAssessment;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [TrainingAssessmentMO MR_aggregateOperation:@"max:" onAttribute:TrainingAssessmentMOAttributes.updatedOn
			withPredicate:loginIdPredicate inContext:self.managedObjectContext];
	}
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory trainingAssessmentUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:trainingAssessmentFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_TrainingAssessment, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:trainingAssessmentFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *assessmentUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// process any updates
			if ([assessmentUpdates count] > 0) {
				NSArray *cachedAssessmentLogs = [TrainingAssessmentMO MR_findAllWithPredicate:loginIdPredicate inContext:self.managedObjectContext];

				for (NSMutableDictionary *updateDict in assessmentUpdates) {
					NSNumber *cloudId = [[updateDict valueForKey:kId] numberValueDecimal];

					// this code block is used below to grab the index of the matching log by id
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						TrainingAssessmentMO *log = (TrainingAssessmentMO *)obj;
						return ([log.cloudId isEqualToNumber:cloudId]);
					};

					NSUInteger cacheIndex = [cachedAssessmentLogs indexOfObjectPassingTest:SameCloudId];

					BOOL deleted = [[updateDict valueForKey:kDeleted] boolValue];

					TrainingAssessmentMO *log = nil;
					if (cacheIndex == NSNotFound) {
						// this has to be checked inside NSNotFound - because the item may not exist on 
						// the phone
						if (!deleted) {
							log = [TrainingAssessmentMO insertInManagedObjectContext:self.managedObjectContext];
							log.loginId = loginId;
						}
					}
					else {
						log = [cachedAssessmentLogs objectAtIndex:cacheIndex];

						if (deleted) {
							[self.managedObjectContext deleteObject:log];
							continue;
						}
					}

					log.cloudId = cloudId;
					[log setWithUpdateDictionary:updateDict];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [TrainingAssessmentMO MR_aggregateOperation:@"max:" onAttribute:TrainingAssessmentMOAttributes.updatedOn
				withPredicate:loginIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForTrainingAssessment"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the assessments
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[TrainingAssessmentMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - Training Strength
- (BCObjectManagerReturnCode)sendUpdatesForTrainingStrength:(TaskQueue *)taskQueue
{
	// find the updated items
	NSPredicate *updatedPredicate =
		[NSPredicate predicateWithFormat:@"loginId = %@ and status <> %@", 
		[[User currentUser] loginId], kStatusOk];

	NSFetchRequest *updatedItemsRequest = [[NSFetchRequest alloc] init];
	updatedItemsRequest.entity = [TrainingStrengthMO entityInManagedObjectContext:self.managedObjectContext];
	updatedItemsRequest.predicate = updatedPredicate;

	NSArray *updatedTrainingStrengthLogs =
		[self.managedObjectContext executeFetchRequest:updatedItemsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForTrainingStrength" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory trainingStrengthURL];
	for (TrainingStrengthMO *aStrengthLog in updatedTrainingStrengthLogs) {
		NSString *status = aStrengthLog.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[aStrengthLog toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:aStrengthLog];
					}
					else {
						aStrengthLog.status = kStatusOk;
						if ([aStrengthLog.cloudId integerValue] == 0 && [resultsDict objectForKey:kCloudId] != nil) {
							aStrengthLog.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForTrainingStrength"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}
- (BCObjectManagerReturnCode)getUpdatesForTrainingStrength:(TaskQueue *)taskQueue
{
	static NSString *trainingStrengthFetcher = @"trainingStrengthFetcher";

	NSNumber *loginId = [User loginId];

	// fetch all the Strength logs for this user
	NSPredicate *loginIdPredicate = [NSPredicate predicateWithFormat:@"loginId = %@", loginId];
	NSString *table = kSyncExerciseStrength;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [TrainingStrengthMO MR_aggregateOperation:@"max:" onAttribute:TrainingStrengthMOAttributes.updatedOn
			withPredicate:loginIdPredicate inContext:self.managedObjectContext];
	}
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory trainingStrengthUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:trainingStrengthFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Strengths, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:trainingStrengthFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *strengthLogUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// process any updates
			if ([strengthLogUpdates count] > 0) {
				// fetch all the cached strength logs for this user
				NSArray *cachedStrengthLogs = [TrainingStrengthMO MR_findAllWithPredicate:loginIdPredicate inContext:self.managedObjectContext];

				for (NSMutableDictionary *updateDict in strengthLogUpdates) {
					NSNumber *cloudId = [[updateDict valueForKey:kId] numberValueDecimal];

					// this code block is used below to grab the index of the matching strength log by id
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						TrainingStrengthMO *log = (TrainingStrengthMO *)obj;
						return ([log.cloudId isEqualToNumber:cloudId]);
					};

					NSUInteger cacheIndex = [cachedStrengthLogs indexOfObjectPassingTest:SameCloudId];

					BOOL deleted = [[updateDict valueForKey:kDeleted] boolValue];

					TrainingStrengthMO *log = nil;
					if (cacheIndex == NSNotFound) {
						// this has to be checked inside NSNotFound - because the item may not exist on 
						// the phone
						if (!deleted) {
							log = [TrainingStrengthMO insertInManagedObjectContext:self.managedObjectContext];
							log.loginId = loginId;
						}
					}
					else {
						log = [cachedStrengthLogs objectAtIndex:cacheIndex];

						if (deleted) {
							[self.managedObjectContext deleteObject:log];
							continue;
						}
					}

					log.cloudId = cloudId;
					[log setWithUpdateDictionary:updateDict];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [TrainingStrengthMO MR_aggregateOperation:@"max:" onAttribute:TrainingStrengthMOAttributes.updatedOn
				withPredicate:loginIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForTrainingStrength"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the strengths
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[TrainingStrengthMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - Tracking
- (BCObjectManagerReturnCode)sendUpdatesForTracking:(TaskQueue *)taskQueue
{
	// find the updated tracking logs
	NSPredicate *updatedPredicate =
		[NSPredicate predicateWithFormat:@"loginId = %@ and status <> %@", 
		[[User currentUser] loginId], kStatusOk];

	NSFetchRequest *updatedLogsRequest = [[NSFetchRequest alloc] init];
	updatedLogsRequest.entity = [TrackingMO entityInManagedObjectContext:self.managedObjectContext];
	updatedLogsRequest.predicate = updatedPredicate;

	NSArray *updatedTrackingLogs =
		[self.managedObjectContext executeFetchRequest:updatedLogsRequest error:nil];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForTracking" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory trackingURL];
	for (TrackingMO *trackingLog in updatedTrackingLogs) {
		NSString *status = trackingLog.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[trackingLog toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:trackingLog];
					}
					else {
						trackingLog.status = kStatusOk;
						if ([trackingLog.cloudId integerValue] == 0 && [resultsDict objectForKey:kCloudId] != nil) {
							trackingLog.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForTracking"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForTracking:(TaskQueue *)taskQueue
{
	static NSString *trackingFetcher = @"trackingFetcher";

	NSNumber *loginId = [User loginId];

	NSPredicate *loginIdPredicate = [NSPredicate predicateWithFormat:@"loginId = %@", loginId];
	NSString *table = kSyncTracking;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [TrackingMO MR_aggregateOperation:@"max:" onAttribute:TrackingMOAttributes.updatedOn
			withPredicate:loginIdPredicate inContext:self.managedObjectContext];
	}
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory trackingUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:trackingFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Tracking, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:trackingFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *trackingLogUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// process any updates
			if ([trackingLogUpdates count] > 0) {
				// fetch all the cached tracking logs for this user
				NSArray *cachedTracking = [TrackingMO MR_findAllWithPredicate:loginIdPredicate inContext:self.managedObjectContext];

				for (NSMutableDictionary *updateDict in trackingLogUpdates) {
					NSNumber *cloudId = [[updateDict valueForKey:kId] numberValueDecimal];

					// this code block is used below to grab the index of the matching log by id
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						TrackingMO *log = (TrackingMO *)obj;
						return ([log.cloudId isEqualToNumber:cloudId]);
					};

					NSUInteger cacheIndex = [cachedTracking indexOfObjectPassingTest:SameCloudId];

					BOOL deleted = [[updateDict valueForKey:kDeleted] boolValue];

					TrackingMO *log = nil;
					if (cacheIndex == NSNotFound) {
						// this has to be checked inside NSNotFound - because the item may not exist on 
						// the phone
						if (!deleted) {
							log = [TrackingMO insertInManagedObjectContext:self.managedObjectContext];
							log.loginId = loginId;
						}
					}
					else {
						log = [cachedTracking objectAtIndex:cacheIndex];

						if (deleted) {
							[self.managedObjectContext deleteObject:log];
							continue;
						}
					}

					log.cloudId = cloudId;
					[log setWithUpdateDictionary:updateDict];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [TrackingMO MR_aggregateOperation:@"max:" onAttribute:TrackingMOAttributes.updatedOn
				withPredicate:loginIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForTracking"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the tracking
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[TrackingMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

- (void)getUpdatesForTrackingType:(TaskQueue *)taskQueue
{
	static NSString *taskName = @"TrackingTypeFetcher";

	NSNumber *loginId = [User loginId];
	NSString *table = kSyncTrackingType;

	// skip this if it has been done today already
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	NSCalendar *calendar = [NSCalendar currentCalendar];
	if ([calendar BC_daysFromDate:[NSDate date] toDate:[NSDate dateWithTimeIntervalSince1970:[lastUpdate integerValue]]] >= 0) {
		DDLogSync(@"Already got updates for Tracking Types today, skipping");
		[taskQueue runQueue:nil];
		return;
	}

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory trackingTypeURL]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:taskName];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_TrackingTypes, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:taskName];

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForTrackingType" withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *updates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
			NSMutableArray *allMOs = [[TrackingTypeMO MR_findByAttribute:TrackingTypeMOAttributes.loginId withValue:loginId
				inContext:scratchContext] mutableCopy];

			NSUInteger sortOrder = 0;
			for (NSDictionary *updateDict in updates) {
				NSNumber *cloudId = [updateDict BC_numberForKey:kTrackingTypeId];

				// this code block is used below to grab the index of the matching managedObject by id
				BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					TrackingTypeMO *managedObject = (TrackingTypeMO *)obj;
					return ([managedObject.cloudId isEqualToNumber:cloudId]);
				};

				NSUInteger cacheIndex = [allMOs indexOfObjectPassingTest:SameCloudId];

				TrackingTypeMO *managedObject = nil;
				if (cacheIndex == NSNotFound) {
					managedObject = [TrackingTypeMO insertInManagedObjectContext:scratchContext];
					managedObject.loginId = loginId;
				}
				else {
					managedObject = [allMOs objectAtIndex:cacheIndex];
					[allMOs removeObjectAtIndex:cacheIndex];
				}

				managedObject.cloudId = cloudId;
				[managedObject setWithUpdateDictionary:updateDict];
				managedObject.sortOrder = @(sortOrder);

				sortOrder++;
			}

			// Now remove any MOs that didn't get updated, as these must have been removed
			for (NSManagedObject *managedObject in allMOs) {
				[scratchContext deleteObject:managedObject];
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@([[NSDate date] timeIntervalSince1970]) forTable:table forUser:loginId
				inContext:scratchContext];
			// Push the changes to the main context all at once
			[scratchContext BL_save];

			// Save the context.
			[self.managedObjectContext BL_save];
		}

		// Post a notification that we've updated the entity
		[[NSNotificationCenter defaultCenter] postNotificationName:[TrackingTypeMO entityName] object:self
														  userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];
}

#pragma mark - CustomFoods
- (BCObjectManagerReturnCode)sendUpdatesForCustomFoods:(TaskQueue *)taskQueue
{
	// fetch all the updated foods for this user
	NSArray *updatedCustomFoods = [CustomFoodMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"accountId = %@ and status <> %@", 
			[[User currentUser] accountId], kStatusOk]
		inContext:self.managedObjectContext];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForCustomFoods" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory customFoodURL];
	for (CustomFoodMO *anItem in updatedCustomFoods) {
		NSString *status = anItem.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[anItem toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:anItem];
					}
					else {
						anItem.status = kStatusOk;
						if ([resultsDict objectForKey:kCloudId] != nil) {
							anItem.cloudId = [[resultsDict valueForKey:kCloudId] numberValueDecimal];

							[anItem.foodLogs enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
								FoodLogMO *foodLog = obj;
								foodLog.productId = anItem.cloudId;
								[foodLog markForSync];
							}];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForCustomFoods"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForCustomFoods:(TaskQueue *)taskQueue
{
	static NSString *customFoodsFetcher = @"CustomFoodsFetcher";

	NSNumber *loginId = [User loginId];
	NSNumber *accountId = [User accountId];

	NSPredicate *accountIdPredicate = [NSPredicate predicateWithFormat:@"accountId = %@", accountId];
	NSString *table = kSyncProducts;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:loginId inContext:self.managedObjectContext];
	if (!lastUpdate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [CustomFoodMO MR_aggregateOperation:@"max:" onAttribute:CustomFoodMOAttributes.updatedOn
			withPredicate:accountIdPredicate inContext:self.managedObjectContext];
	}
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory customFoodURLNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:customFoodsFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_CustomFoods, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:customFoodsFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *customFoodUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			// process any updates
			if ([customFoodUpdates count] > 0) {
				NSArray *cachedCustomFoods = [CustomFoodMO MR_findAllWithPredicate:accountIdPredicate inContext:self.managedObjectContext];

				for (NSMutableDictionary *updateObject in customFoodUpdates) {
					NSNumber *cloudId = [[updateObject valueForKey:kId] numberValueDecimal];

					// this code block is used below to grab the index of the matching item by cloudid
					// cloud_id comes back from the cloud as a string, so just querying the array using
					// a predicate wasn't working
					BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
						CustomFoodMO *item = (CustomFoodMO *)obj;
						return ([item.cloudId isEqualToNumber:cloudId]);
					};

					NSUInteger index = [cachedCustomFoods indexOfObjectPassingTest:SameCloudId];

					BOOL deleted = [[updateObject valueForKey:kDeleted] boolValue];

					CustomFoodMO *customFood = nil;
					if (index == NSNotFound) {
						// this has to be checked inside NSNotFound - because the item may not exist on 
						// the phone but has been marked deleted on the web
						if (!deleted) {
							customFood = [CustomFoodMO insertInManagedObjectContext:self.managedObjectContext];
							customFood.accountId = accountId;
						}
					}
					else {
						customFood = [cachedCustomFoods objectAtIndex:index];

						if (deleted) {
							[self.managedObjectContext deleteObject:customFood];
							continue;
						}
					}

					[customFood setWithUpdateDictionary:updateObject];
				}
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = [CustomFoodMO MR_aggregateOperation:@"max:" onAttribute:CustomFoodMOAttributes.updatedOn
				withPredicate:accountIdPredicate inContext:self.managedObjectContext];
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForCustomFoods"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the custom foods
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[CustomFoodMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - UserProfile
- (BCObjectManagerReturnCode)sendUpdatedUserProfile:(TaskQueue *)taskQueue
{
	// get the UserProfileMO for this user
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatedUserProfile" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory userProfileURL];

	if (userProfile) {
		NSString *status = userProfile.status;
		if (![status isEqualToString:kStatusOk]) {
			GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[userProfile toJSON]];

			BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
			[appDelegate setNetworkActivityIndicatorVisible:YES];

			void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
				[appDelegate setNetworkActivityIndicatorVisible:NO];

				if (!error) {
					// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
						userProfile.status = kStatusOk;
					}
				}
				else {
					[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatedUserProfile"
						withFetcher:fetcher retrievedData:retrievedData];
				}
			};

			[subQueue addRequest:fetcher completionBlock:CompletionHandler];
		}
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatedUserProfile:(TaskQueue *)taskQueue
{
	static NSString *userProfileFetcher = @"userProfileFetcher";

	NSNumber *loginId = [User loginId];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory userProfileURL]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:userProfileFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_UserProfile, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:userProfileFetcher];

		if (!error) {
			// fetch succeeded
			NSDictionary *profileDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// process any updates
			if (profileDict) {
				// get the UserProfileMO for this user
				UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

				if (!userProfile) {
					userProfile = [UserProfileMO createUserProfileInMOC:self.managedObjectContext];
				}

				[userProfile setWithUpdateDictionary:profileDict];
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:kSyncUsers forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatedUserProfile"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the user profile
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[UserProfileMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForUserPermissions:(TaskQueue *)taskQueue
{
	static NSString *userPermissionFetcher = @"UserPermissionFetcher";

	NSNumber *loginId = [User loginId];

	// get the UserProfileMO for this user
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];
	if (!userProfile) {
		DDLogWarn(@"Warning:  There is no UserProfile row for this account/login!!!  Cannot get user permissions from server.");

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:[NSError errorWithDomain:@"Cloud API Error" code:UnspecifiedError userInfo:nil]];
	}

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory userPermissionsURL]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:userPermissionFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_UserPermissions, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:userPermissionFetcher];

		if (!error) {
			// fetch succeeded
			NSDictionary *permissions = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// process any updates
			if (permissions) {
				[userProfile updatePermissions:permissions];
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:kSyncPermissions forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForUserPermissions" withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the user profile
		[[NSNotificationCenter defaultCenter] postNotificationName:[UserPermissionMO entityName] object:self
														  userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];

	return Success;
}

#pragma mark - Advisor Meal Plans, aka Health Coach Plans
- (void)sendUpdatesForAdvisorMealPlans:(TaskQueue *)taskQueue
{
	// TODO no ability to send updates yet
	[taskQueue runQueue:nil];
}

#pragma mark - Exercise Routines
- (BCObjectManagerReturnCode)getUpdatesForExerciseRoutines:(TaskQueue *)taskQueue
{
	static NSString *exerciseRoutinesFetcher = @"ExerciseRoutinesFetcher";

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory exerciseRoutinesURL]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:exerciseRoutinesFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_ExerciseRoutines, nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:exerciseRoutinesFetcher];

		if (!error) {
			// fetch succeeded
			NSArray *routineUpdates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			// process any updates
			if ([routineUpdates count] > 0) {
				for (NSMutableDictionary *routineDict in routineUpdates) {
					NSNumber *cloudId = [[routineDict valueForKey:kId] numberValueDecimal];
					ExerciseRoutineMO *routineMO = [ExerciseRoutineMO MR_findFirstByAttribute:ExerciseRoutineMOAttributes.cloudId withValue:cloudId inContext:self.managedObjectContext];

					if (!routineMO) {
						routineMO = [ExerciseRoutineMO insertInManagedObjectContext:self.managedObjectContext];
					}

					routineMO.cloudId = cloudId;
					routineMO.activity = [routineDict valueForKey:@"activity"];
					routineMO.factor = [[routineDict valueForKey:@"factor"] numberValueDecimal];
				}
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:kSyncExerciseRoutines forUser:nil inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForExerciseRoutines"
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Post a notification that we've updated the categories
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[ExerciseRoutineMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];

	return Success;
}

#pragma mark - User images
- (void)sendUpdatesForUserImages:(TaskQueue *)taskQueue
{
	// fetch all the updated foods for this user
	NSArray *updatedUserImages = [UserImageMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"accountId = %@ and status <> %@", 
			[[User currentUser] accountId], kStatusOk]
		inContext:self.managedObjectContext];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForUserImages" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory imageURL];
	for (UserImageMO *anItem in updatedUserImages) {
		NSString *status = anItem.status;

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:[anItem toJSON]];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					// If this is a user profile image, update the user profile with the new image id
					if ((anItem.imageTypeValue == kUserImageTypeUserProfile)  && [resultsDict objectForKey:kCloudId]) {
						UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];
						userProfile.imageId = [resultsDict BC_numberOrNilForKey:kCloudId];
					}

					// NOTE this table is just acting like an update queue, once the image has been 
					// successfully sent to the cloud, we are done with it.
					[self.managedObjectContext deleteObject:anItem];
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForUserImages"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];
}

#pragma mark - Nutrients
- (void)getUpdatesForNutrients:(TaskQueue *)taskQueue
{
	static NSString *nutrientsFetcher = @"NutrientsFetcher";

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory nutrientsURL]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:nutrientsFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Nutrients, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:nutrientsFetcher];

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForNutrients" withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *updates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// Delete strategy, remove rows that aren't in the updates
			NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
			NSMutableArray *allMOs = [[NutrientMO MR_findAllSortedBy:NutrientMOAttributes.sortOrder ascending:YES
				inContext:scratchContext] mutableCopy];

			// Add all rows from cloud
			for (NSDictionary *updateDict in updates) {
				NSString *name = [updateDict valueForKey:kName];
				BOOL (^SameCloudId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					return ([[obj name] isEqualToString:name]);
				};
				NSUInteger cacheIndex = [allMOs indexOfObjectPassingTest:SameCloudId];

				NutrientMO *managedObject = nil;
				if (cacheIndex == NSNotFound) {
					managedObject = [NutrientMO insertInManagedObjectContext:scratchContext];
				}
				else {
					managedObject = [allMOs objectAtIndex:cacheIndex];
					[allMOs removeObjectAtIndex:cacheIndex];
				}
				[managedObject setWithUpdateDictionary:updateDict];
			}

			// Now remove any MOs that didn't get updated, as these must have been removed
			for (NSManagedObject *managedObject in allMOs) {
				[scratchContext deleteObject:managedObject];
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:kSyncNutrients forUser:nil inContext:scratchContext];
			// Push the changes to the main context all at once
			[scratchContext BL_save];

			// Save the context.
			[self.managedObjectContext BL_save];
		}

		// Post a notification that we've updated the NutrientMO
		[[NSNotificationCenter defaultCenter] postNotificationName:[NutrientMO entityName] object:self
														  userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];
}

#pragma mark - Units
- (void)getUpdatesForUnits:(TaskQueue *)taskQueue
{
	static NSString *unitsFetcher = @"UnitsFetcher";

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory unitsURL]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:unitsFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_MeasurementUnits, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:unitsFetcher];

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForUnits" withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *units = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// Delete strategy, remove all existing rows
			NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
			NSArray *allUnits = [UnitMO MR_findAllSortedBy:UnitMOAttributes.sortOrder ascending:YES
				inContext:scratchContext];

			for (UnitMO *unitMO in allUnits) {
				[scratchContext deleteObject:unitMO];
			}

			// Add all rows from cloud
			for (NSDictionary *unitDict in units) {
				UnitMO *unitMO = [UnitMO insertInManagedObjectContext:scratchContext];
				[unitMO setWithUpdateDictionary:unitDict];
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:kSyncUnits forUser:nil inContext:scratchContext];

			// Push the changes to the main context all at once
			[scratchContext BL_save];

			// Save the context.
			[self.managedObjectContext BL_save];
		}

		// Post a notification that we've updated the UnitMO
		[[NSNotificationCenter defaultCenter] postNotificationName:[UnitMO entityName] object:self
														  userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];
}

#pragma mark - CoachInvites
- (BCObjectManagerReturnCode)sendUpdatesForCoachInvites:(TaskQueue *)taskQueue
{
	// find the updated items
	NSArray *updatedCoachInvites = [CoachInviteMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"loginId = %@ and status <> %@", 
			[[User currentUser] loginId], kStatusOk]
		inContext:self.managedObjectContext];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForCoachInvites" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory coachInvitesURL];
	for (CoachInviteMO *aCoachInvite in updatedCoachInvites) {
		NSString *status = aCoachInvite.status;

		NSData *postData = [aCoachInvite toJSON];
		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:postData];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				aCoachInvite.status = kStatusOk;
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForCoachInvites"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForCoachInvites:(TaskQueue *)taskQueue
{
	static NSString *coachInvitesFetcher = @"CoachInvitesFetcher";

	NSNumber *loginId = [User loginId];

	NSString *table = kSyncCoachInvites;
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory coachInvitesURL]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:coachInvitesFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_CoachInvites, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:coachInvitesFetcher];

		NSMutableArray *firstTimeInvites = nil;
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForCoachInvites" withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *invitations = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			
			// Delete strategy, remove all existing rows
			NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
			NSMutableArray *allInvites = [[CoachInviteMO MR_findByAttribute:CoachInviteMOAttributes.loginId withValue:loginId
				inContext:scratchContext] mutableCopy];

			for (NSDictionary *invitationDict in invitations) {
				// See if this invite already exists, if not, create it
				NSString *code = [invitationDict valueForKey:kInviteCode];
				NSUInteger index = [allInvites indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					CoachInviteMO *item = obj;
					return ([item.code isEqualToString:code]);
				}];

				CoachInviteMO *coachInviteMO = nil;
				if (index == NSNotFound) {
					coachInviteMO = [CoachInviteMO insertInManagedObjectContext:scratchContext];

					// Keep track of any new invites so that the user may be alerted
					if (!firstTimeInvites) {
						firstTimeInvites = [[NSMutableArray alloc] init];
					}
					[firstTimeInvites addObject:coachInviteMO];
				}
				else {
					coachInviteMO = [allInvites objectAtIndex:index];

					// Remove it from the allInvites array, as we'll delete any coachInviteMOs that remain in allInvites at the end
					[allInvites removeObjectAtIndex:index];
				}

				// Update the invite
				[coachInviteMO setWithUpdateDictionary:invitationDict];
			}

			// Delete any coachInviteMOs that still exist in allInvites, as these weren't sent down from the cloud, thus they must
			// have been deleted
			for (CoachInviteMO *coachInviteMO in allInvites) {
				[scratchContext deleteObject:coachInviteMO];
			}

			// Update the lastUpdate in syncUpdate - just use zero since this doesn't use a newer_than
			[SyncUpdateMO setLastUpdate:@0 forTable:table forUser:loginId inContext:scratchContext];

			// Push the changes to the main context all at once
			[scratchContext BL_save];

			// Save the context.
			[self.managedObjectContext BL_save];
		}

		// Post a notification that we've updated the coach invites, include a user info of the first time invites if there is one,
		// so that observers might do something with this info. The intent is for the app to pop up an alert for these
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[CoachInviteMO entityName]
			object:self
			userInfo:(firstTimeInvites ? @{kFirstTimeInvites : firstTimeInvites} : nil)];
		
		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];

	return Success;
}

#pragma mark - Community
- (BCObjectManagerReturnCode)sendUpdatesForCommunities:(TaskQueue *)taskQueue
{
	// get the UserProfileMO for this user
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	// find the updated items
	NSSet *updatedCommunities = [userProfile.communities filteredSetUsingPredicate:
		[NSPredicate predicateWithFormat:@"status <> %@", kStatusOk]];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForCommunities" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory communityURL];
	for (CommunityMO *aCommunityMO in updatedCommunities) {
		NSString *status = aCommunityMO.status;

		NSData *postData = [aCommunityMO toJSON];
		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:postData];
		DDLogSync(@"****** Communities SEND ******\n\nStatus:%@\nCommunity:%@", status, aCommunityMO);

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				if ([aCommunityMO.communityStatus isEqualToString:kCommunityStatusDeclined]) {
					// Remove declined communities
					[self.managedObjectContext deleteObject:aCommunityMO];
				}
				else {
					aCommunityMO.status = kStatusOk;
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForCommunities"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForCommunities:(TaskQueue *)taskQueue
{
	static NSString *communitiesFetcher = @"CommunitiesFetcher";

	// get the UserProfileMO for this user
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	NSString *table = kSyncCommunities;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:userProfile.loginId inContext:self.managedObjectContext];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory communityUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:communitiesFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_Communities, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:communitiesFetcher];

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForCommunities" withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *communities = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			DDLogSync(@"****** Communities ******\n\n%@", communities);
			
			for (NSDictionary *communityDict in communities) {
				// See if this community already exists, if not, create it
				NSNumber *cloudId = [communityDict BC_numberOrNilForKey:kId];
				CommunityMO *communityMO = nil;
				if (cloudId) {
					NSSet *objs = [userProfile.communities objectsPassingTest:^BOOL(id obj, BOOL *stop) {
						CommunityMO *item = obj;
						if ([item.cloudId isEqualToNumber:cloudId]) {
							// Only want one, should only be one
							*stop = YES;
							return YES;
						}
						else {
							return NO;
						}
					}];
					communityMO = [objs anyObject];
				}

				NSDictionary *memberDict = [communityDict objectForKey:kCommunityMember];
				BOOL deleted = ([[communityDict valueForKey:kDeleted] boolValue] || [[memberDict valueForKey:kDeleted] boolValue]);

				if (!communityMO && !deleted) {
					communityMO = [CommunityMO insertInManagedObjectContext:self.managedObjectContext];
				}
				else if (communityMO && deleted) {
					[self.managedObjectContext deleteObject:communityMO];
					continue;
				}

				// Update the community
				[communityMO setWithUpdateDictionary:communityDict];
			}

			// Get the new lastUpdate
			NSNumber *lastUpdate = ([userProfile.communities valueForKeyPath:@"@max.updatedOn"] ?: @0);

			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:userProfile.loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}

		// Post a notification that we've updated the communities
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[CommunityMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];

	return Success;
}

#pragma mark - Message Threads
- (BCObjectManagerReturnCode)sendUpdatesForThreads:(TaskQueue *)taskQueue
{
	// get the UserProfileMO for this user
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	// find the updated items
	NSSet *updatedThreads = [userProfile.threads filteredSetUsingPredicate:
		[NSPredicate predicateWithFormat:@"status <> %@", kStatusOk]];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForThreads" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory threadsURL];
	for (ThreadMO *aThreadMO in updatedThreads) {
		NSString *status = aThreadMO.status;

		NSData *postData = [aThreadMO toJSON];
		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:postData];
		DDLogSync(@"****** Threads SEND ******\n\nStatus:%@\nThread:%@, JSON:%@", status, aThreadMO,
			[NSJSONSerialization JSONObjectWithData:postData options:0 error:nil]);

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						// NOTE: This is not going to actually delete the thread, as we want the row to remain so that
						// we know of its existence, instead, the unsubscribed attribute will reflect whether or not we
						// are subscribed to this thread
						//[self.managedObjectContext deleteObject:aThreadMO];
						aThreadMO.unsubscribed = @(YES);
						aThreadMO.status = kStatusOk;
						// TODO Do we want to remove all messages related to this thread? The cascade will never happen since
						// we're not deleting the thread itself.
					}
					else {
						aThreadMO.status = kStatusOk;
						if (!aThreadMO.cloudIdValue && [resultsDict BC_numberOrNilForKey:kCloudId]) {
							aThreadMO.cloudId = [resultsDict BC_numberOrNilForKey:kCloudId];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForThreads"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForThreads:(TaskQueue *)taskQueue
{
	static NSString *threadsFetcher = @"ThreadFetcher";

	// get the UserProfileMO for this user
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	NSString *table = kSyncThreads;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:userProfile.loginId inContext:self.managedObjectContext];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory threadsUrlNewerThan:lastUpdate]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:threadsFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_CommunityThreads, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:threadsFetcher];

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForThreads" withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *threads = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			DDLogSync(@"****** Threads ******\n\n%@", threads);
			for (NSDictionary *threadDict in threads) {
				// See if this thread already exists, if not, create it
				NSNumber *cloudId = [threadDict BC_numberOrNilForKey:kThreadId];
				ThreadMO *threadMO = nil;
				if (cloudId) {
					NSSet *objs = [userProfile.threads objectsPassingTest:^BOOL(id obj, BOOL *stop) {
						ThreadMO *item = obj;
						if ([item.cloudId isEqualToNumber:cloudId]) {
							// Only want one, should only be one
							*stop = YES;
							return YES;
						}
						else {
							return NO;
						}
					}];
					threadMO = [objs anyObject];
				}

				BOOL deleted = [[threadDict valueForKey:kDeleted] boolValue];

				if (!threadMO) {
					threadMO = [ThreadMO insertInManagedObjectContext:self.managedObjectContext];
				}
				else if (threadMO && deleted) {
					// NOTE: This is not going to actually delete the thread, as we want the row to remain so that
					// we know of its existence, instead, the unsubscribed attribute will reflect whether or not we
					// are subscribed to this thread
					//[self.managedObjectContext deleteObject:threadMO];
					// TODO Do we want to remove all messages related to this thread? The cascade will never happen since
					// we're not deleting the thread itself.
				}

				// Update the thread
				[threadMO setWithUpdateDictionary:threadDict];
			}

			// Get the new lastUpdate
			NSNumber *lastUpdate = (MAX([userProfile.threads valueForKeyPath:@"@max.updatedOn"],
				[userProfile.threads valueForKeyPath:@"@max.threadUpdatedOn"]) ?: @0);

			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:userProfile.loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}

		// Post a notification that we've updated the threads
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[ThreadMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];

	return Success;
}

#pragma mark - Thread Messages
- (BCObjectManagerReturnCode)sendUpdatesForThreadMessages:(TaskQueue *)taskQueue
{
	// get the UserProfileMO for this user
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	// find the updated items
	NSSet *updatedMessages = [userProfile.threadMessages filteredSetUsingPredicate:
		[NSPredicate predicateWithFormat:@"status <> %@", kStatusOk]];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:@"sendUpdatesForThreadMessages" completionBlock:
		^void(NSString *identifier, NSError *error) {
			// Save the context.
			[self.managedObjectContext BL_save];

			[taskQueue runQueue:error];
		}];

	NSURL *url = [BCUrlFactory threadMessagesURL];
	for (ThreadMessageMO *aThreadMessageMO in updatedMessages) {
		NSString *status = aThreadMessageMO.status;

		NSData *postData = [aThreadMessageMO toJSON];
		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:status postData:postData];
		DDLogSync(@"****** Thread Messages SEND ******\n\nStatus:%@\nMessage:%@", status, aThreadMessageMO);

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			if (!error) {
				// fetch succeeded
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([resultsDict objectForKey:kSuccess] != nil) {
					if ([status isEqualToString:kStatusDelete]) {
						[self.managedObjectContext deleteObject:aThreadMessageMO];
					}
					else {
						aThreadMessageMO.status = kStatusOk;
						if (!aThreadMessageMO.cloudIdValue && [resultsDict BC_numberOrNilForKey:kCloudId]) {
							aThreadMessageMO.cloudId = [resultsDict BC_numberOrNilForKey:kCloudId];
						}
					}
				}
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"sendUpdatesForThreadMessages"
					withFetcher:fetcher retrievedData:retrievedData];
			}
		};

		[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	}

	[subQueue runQueue:nil];

	return Success;
}

- (BCObjectManagerReturnCode)getUpdatesForThreadMessages:(TaskQueue *)taskQueue
{
	static NSString *messagesFetcher = @"ThreadMessagesFetcher";

	// get the UserProfileMO for this user
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	NSString *table = kSyncThreadMessages;
	NSNumber *lastUpdate = [SyncUpdateMO lastUpdateForTable:table forUser:userProfile.loginId inContext:self.managedObjectContext];

	// Get the list of empty local threads to request all messages for these threads, in case we've not yet synced them, but
	// only do this if the last update isn't already zero, as that will fetch everything anyhow
	NSString *emptyThreadString = @"";
	if ([lastUpdate integerValue]) {
		NSSet *emptyThreads = [userProfile.threads filteredSetUsingPredicate:
			[NSPredicate predicateWithFormat:@"%K.@count == 0", ThreadMORelationships.messages]];
		emptyThreadString = [[[emptyThreads valueForKeyPath:ThreadMOAttributes.cloudId] allObjects] componentsJoinedByString:@","];
	}
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory threadMessagesUrlNewerThan:lastUpdate
		emptyThreads:emptyThreadString]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:messagesFetcher];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kSyncUpdateUserStatusNotification
	 object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString(kSyncUpdateHumanReadable_CommunityThreadMessages, nil)}];

	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:messagesFetcher];

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:"getUpdatesForThreadMessages" withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *messages = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			DDLogSync(@"****** Messages ******\n\nFetcher:%@\n\n%@", fetcher, messages);
			for (NSDictionary *messageDict in messages) {
				// See if this message already exists, if not, create it
				NSNumber *cloudId = [messageDict BC_numberOrNilForKey:kId];
				ThreadMessageMO *threadMessageMO = nil;
				if (cloudId) {
					NSSet *objs = [userProfile.threadMessages objectsPassingTest:^BOOL(id obj, BOOL *stop) {
						ThreadMessageMO *item = obj;
						if ([item.cloudId isEqualToNumber:cloudId]) {
							// Only want one, should only be one
							*stop = YES;
							return YES;
						}
						else {
							return NO;
						}
					}];
					threadMessageMO = [objs anyObject];
				}

				BOOL deleted = [[messageDict valueForKey:kDeleted] boolValue];

				if (!threadMessageMO && !deleted) {
					threadMessageMO = [ThreadMessageMO insertInManagedObjectContext:self.managedObjectContext];
				}
				else if (threadMessageMO && deleted) {
					[self.managedObjectContext deleteObject:threadMessageMO];
					continue;
				}

				// Update the thread message
				[threadMessageMO setWithUpdateDictionary:messageDict];
			}

			// Get the new lastUpdate
			NSNumber *lastUpdate = ([userProfile.threadMessages valueForKeyPath:@"@max.updatedOn"] ?: @0);

			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:table forUser:userProfile.loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];
		}

		// Post a notification that we've updated the thread messages
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[ThreadMessageMO entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	}];

	return Success;
}

#pragma mark - Generic sync methods
- (void)getUpdatesForMO:(Class)moClass withTaskQueue:(TaskQueue *)taskQueue
{
	assert([moClass isSubclassOfClass:[NSManagedObject class]] && "Unexpected class passed in, not an NSManagedObject subclass");
	assert([moClass conformsToProtocol:@protocol(BLSyncEntity)] && "Unexpected class passed in, does not conform to BLSyncEntity");

	NSString *className = NSStringFromClass(moClass);
	NSNumber *loginId = [User loginId];

	NSPredicate *userPredicate = [moClass userPredicate];
	NSNumber *lastUpdate = ([SyncUpdateMO lastUpdateForTable:[moClass syncName] forUser:loginId inContext:self.managedObjectContext] ?: @0);
	if (!lastUpdate && [moClass instancesRespondToSelector:@selector(updatedOn)] && userPredicate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [moClass MR_aggregateOperation:@"max:" onAttribute:@"updatedOn"
			withPredicate:userPredicate inContext:self.managedObjectContext];
	}

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[moClass urlNewerThan:lastUpdate withOffset:0 andLimit:0]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:className];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kSyncUpdateUserStatusNotification
		object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString([moClass statusString], nil)}];

	void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		// Finished with this fetch, unregister
		[TaskQueueItems unregisterTask:className];

		if (!error) {
			// fetch succeeded
			NSArray *updates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			[moClass setWithUpdatesArray:updates forLogin:loginId inMOC:self.managedObjectContext];
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:
				[[NSString stringWithFormat:@"getUpdatesForMO:%@", className] UTF8String]
				withFetcher:fetcher retrievedData:retrievedData];
		}

		// Get the new lastUpdate
		NSNumber *lastUpdate = @0; // MO Classes that don't follow 'newer_than' will likely just save zero to SyncUpdate
		if ([moClass instancesRespondToSelector:@selector(updatedOn)] && userPredicate) {
			// This class has updatedOn, so we can get the newest updatedOn time
			lastUpdate = [moClass MR_aggregateOperation:@"max:" onAttribute:@"updatedOn"
				withPredicate:userPredicate inContext:self.managedObjectContext];
		}
		// Update the lastUpdate in syncUpdate
		[SyncUpdateMO setLastUpdate:lastUpdate forTable:[moClass syncName] forUser:loginId inContext:self.managedObjectContext];

		// Save the context.
		[self.managedObjectContext BL_save];

		// Post a notification that we've updated the advisors
		[[NSNotificationCenter defaultCenter]
			postNotificationName:[moClass entityName]
			object:self userInfo:nil];

		// Tell the queue that we're done, and it can continue running
		[taskQueue runQueue:error];
	};

	[fetcher beginFetchWithCompletionHandler:CompletionHandler];
}

- (void)getUpdatesWithPagingForMO:(Class)moClass withTaskQueue:(TaskQueue *)taskQueue
{
	assert([moClass isSubclassOfClass:[NSManagedObject class]] && "Unexpected class passed in, not an NSManagedObject subclass");
	assert([moClass conformsToProtocol:@protocol(BLPagingSyncEntity)] && "Unexpected class passed in, does not conform to BLSyncEntity");

	NSString *className = NSStringFromClass(moClass);
	NSNumber *loginId = [User loginId];
	NSUInteger limit = 250;

	NSPredicate *userPredicate = [moClass userPredicate];
	NSNumber *lastUpdate = ([SyncUpdateMO lastUpdateForTable:[moClass syncName] forUser:loginId inContext:self.managedObjectContext] ?: @0);
	if (!lastUpdate && [moClass instancesRespondToSelector:@selector(updatedOn)] && userPredicate) {
		// This ensures that we don't do a full sync of this table unnecessarily, when the SyncUpdate entity is new
		lastUpdate = [moClass MR_aggregateOperation:@"max:" onAttribute:@"updatedOn"
			withPredicate:userPredicate inContext:self.managedObjectContext];
	}

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kSyncUpdateUserStatusNotification
		object:nil userInfo:@{kSyncUpdateHumanReadableStatusKey : NSLocalizedString([moClass statusString], nil)}];

	// Define the handler that will get called when the subqueue is finished processing
	HTTPQueue *subQueue = [HTTPQueue queueNamed:[NSString stringWithFormat:@"pagingUpdatesFor%@", className] completionBlock:
		^void(NSString *identifier, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			// Finished with this fetch, unregister
			[TaskQueueItems unregisterTask:className];

			// Save the context.
			[self.managedObjectContext BL_save];

			// Get the new lastUpdate
			NSNumber *lastUpdate = @0; // MO Classes that don't follow 'newer_than' will likely just save zero to SyncUpdate
			if ([moClass instancesRespondToSelector:@selector(updatedOn)] && userPredicate) {
				// This class has updatedOn, so we can get the newest updatedOn time
				lastUpdate = [moClass MR_aggregateOperation:@"max:" onAttribute:@"updatedOn"
					withPredicate:userPredicate inContext:self.managedObjectContext];
			}
			// Update the lastUpdate in syncUpdate
			[SyncUpdateMO setLastUpdate:lastUpdate forTable:[moClass syncName] forUser:loginId inContext:self.managedObjectContext];

			// Save the context.
			[self.managedObjectContext BL_save];

			// Post a notification that we've updated the FoodLogMO
			[[NSNotificationCenter defaultCenter]
				postNotificationName:[moClass entityName]
				object:self userInfo:nil];

			[taskQueue runQueue:error];
		}];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[moClass urlNewerThan:lastUpdate withOffset:0 andLimit:limit]];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	[TaskQueueItems registerTaskWithFetcher:fetcher identifier:className];

	__block __weak void (^weakCompletionHandler)(NSData *data, NSError *error);
	void (^CompletionHandler)(NSData *data, NSError *error);
	weakCompletionHandler = CompletionHandler = ^void(NSData *retrievedData, NSError *error) {

		if (!error) {
			// fetch succeeded
			NSDictionary *updateDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			// Make this fetch in batches, and iterate
			BOOL more = [[updateDict BC_numberForKey:kMore] boolValue];
			
			// process any updates
			NSArray *updates = [updateDict objectForKey:[moClass updatesSubkey]];
			[moClass setWithUpdatesArray:updates forLogin:loginId inMOC:self.managedObjectContext];

			// If there are more rows to be fetched, add a new sub queue item to perform this fetch
			if (more) {
				NSUInteger offset = [[updateDict BC_numberForKey:kOffset] integerValue];
				GTMHTTPFetcher *iterFetcher = [GTMHTTPFetcher signedFetcherWithURL:[moClass urlNewerThan:lastUpdate
					withOffset:offset andLimit:limit]];

				[subQueue addRequest:iterFetcher completionBlock:weakCompletionHandler];
			}
		}
		else {
			[BCUtilities logHttpError:error inFunctionNamed:
				[[NSString stringWithFormat:@"getUpdatesWithPagingForMO:%@", className] UTF8String]
				withFetcher:fetcher retrievedData:retrievedData];
		}
	};

	// Add the first request, then run the sub queue
	[subQueue addRequest:fetcher completionBlock:CompletionHandler];
	[subQueue runQueue:nil];
}

#pragma mark - Sync Methods
+ (void)syncCheck:(BCObjectManagerSyncMode)syncMode
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];

	[BCObjectManager syncCheck:syncMode inMOC:appDelegate.adManagedObjectContext withCompletionBlock:nil];
}

+ (BCObjectManager *)syncCheck:(BCObjectManagerSyncMode)syncMode inMOC:(NSManagedObjectContext *)moc
withCompletionBlock:(void(^)(void))completionBlock
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];

	if (!appDelegate.connected) {
		// TODO Fire a notification?
		DDLogSync(@"No Connectivity in %@", THIS_METHOD);
		return nil;
	}

	/*
	NSManagedObjectContext *syncContext = [NSManagedObjectContext MR_rootSavingContext];
	BCObjectManager *manager = [[BCObjectManager alloc] initWithManagedObjectContext:syncContext];
	*/
	BCObjectManager *manager = [[BCObjectManager alloc] initWithManagedObjectContext:moc];

	NSNumber *loginId = [User loginId];

	void (^completionBlockCopy)() = [completionBlock copy];

	// Create a queue that will process all our calls, and our code block 'SyncCheckHandlerBlock' will
	// handle the 'response' from running the queue, since we need to update the sync token on success.  It will then
	// call the standard queueDidFinish: method to ensure consistent handling of the task queue.
	// This also registers that we're running syncCheck so that we don't enter this call again from
	// another location while we're already running this code
	DDLogSync(@"Attempting to create sync task queue");
	TaskQueue *taskQueue = [TaskQueue queueNamed:@"syncCheck" completionBlock:^(NSString *identifier, NSError *error) {
		DDLogSync(@"Sync task queue called its completion block");
		[manager queueDidFinish:identifier forUser:loginId withError:error withCompletionBlock:completionBlockCopy];
	}];

	if (taskQueue) {
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForCategories:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForUserStores:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForSingleUseTokens:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForCoachInvites:)];
		// TODO - Add the ability to make changes to advisors and advisor settings (permissions)
		//[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForAdvisors:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForUserImages:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatedUserProfile:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForCommunities:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForThreads:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForThreadMessages:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForMessages:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForStoreCategories:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForCustomFoods:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForMeals:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForFoodLog:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForLoggingNotes:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForRecommendations:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForShoppingList:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForTrainingActivity:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForTrainingStrength:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForTrainingAssessment:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForTracking:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForPromotions:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForPurchaseHx:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForMealPlanners:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForActivityPlan:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForAdvisorMealPlans:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForHealthChoices:)];
		[taskQueue addRequest:manager targetSelector:@selector(sendUpdatesForPantry:)];

		// This is done once per day (constrained inside the method)
		// This was moved down here because it relies on other get methods to fire first, namely recipe and custom food
		[taskQueue addRequest:manager targetSelector:@selector(getUpdatesForTrackingType:)];

		if (syncMode == SendAndReceive) {
			// Queue up the sync 'receive' function, which will query the cloud for the necessary
			[taskQueue addRequest:manager targetSelector:@selector(getUpdates:)];
		}

		// This is done once per day (constrained inside the method)
		// This was moved down here because it relies on other get methods to fire first, namely recipe and custom food
		[taskQueue addRequest:manager targetSelector:@selector(getUpdatesForMealPredictions:)];

		[taskQueue runQueue:nil];
	}
	else {
		DDLogSync(@"%@ - queue already queued, skipping", THIS_METHOD);
	}

	return manager;
}

- (void)getUpdates:(TaskQueue *)taskQueue
{
	NSNumber *oldSyncToken = nil;

	// Fetch this user's sync token.  This could be placed in the User singleton, but for now
	// we'll just query it from the UserProfileMO entity
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:[NSManagedObjectContext MR_rootSavingContext]];
	NSArray *syncUpdates = [[SyncUpdateMO syncUpdatesForUser:[User loginId] inMOC:[NSManagedObjectContext MR_rootSavingContext]]
		valueForKey:@"tableName"];

	if (userProfile && userProfile.syncToken) {
		oldSyncToken = userProfile.syncToken;
	}

	DDLogSync(@"SyncCheck with token %@", oldSyncToken);
	DDLogVerbose(@"SyncCheck with syncUpdates %@", syncUpdates);

	// Call the cloud synccheck API
	static NSString *syncCheckFetcher = @"SyncCheckFetcher";

	oldSyncToken = (oldSyncToken ?: @0);
	NSURL *url = [BCUrlFactory syncCheckURLWithToken:oldSyncToken];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url];

	// Register this fetcher task so that if the app gets put into the background during this call,
	// we have a way to cancel the call
	if ([TaskQueueItems registerTaskWithFetcher:fetcher identifier:syncCheckFetcher]) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		void (^CompletionHandler)(NSData *data, NSError *error) = ^void(NSData *retrievedData, NSError *error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			// Finished with this fetch, unregister
			[TaskQueueItems unregisterTask:syncCheckFetcher];

			if (!error) {
				// fetch succeeded
				NSDictionary *syncCheckCloudResponse = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
				NSArray *updatedTables = nil;
				NSNumber *newSyncToken = nil;
				if (syncCheckCloudResponse) {
					updatedTables = [syncCheckCloudResponse objectForKey:kUpdatedTables];
					newSyncToken = [[syncCheckCloudResponse objectForKey:kSyncToken] numberValueDecimal];
				}
				DDLogSync(@"SyncCheck with updatedTables %@", updatedTables);

				// Define the handler that will get called when the subqueue is finished processing
				TaskQueue *subQueue = [TaskQueue queueNamed:@"getUpdates" completionBlock:
					^void(NSString *identifier, NSError *error) {
						if (newSyncToken && (!error || ([error code] == Success) || ([error code] == NoResultsReturned))) {
							UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

							if (userProfile) {
								userProfile.syncToken = newSyncToken;
								////DDLogSync(@"Firing queueDidFinish with old token %@ and new token %@", oldSyncToken, newSyncToken);
							}
							else {
								DDLogWarn(@"Warning:  There is no UserProfile row for this account/login!!!  Cannot update syncToken.");
							}
						}

						// Save the context.
						[self.managedObjectContext BL_save];

						[taskQueue runQueue:error];
					}];

				[self syncGet:subQueue updatedTables:updatedTables syncUpdates:syncUpdates];

				[subQueue runQueue:nil];
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:"syncCheck" withFetcher:fetcher retrievedData:retrievedData];

				// Let the taskQueue continue running
				[taskQueue runQueue:error];
			}
		};

		[fetcher beginFetchWithCompletionHandler:CompletionHandler];
	}
	else {
		DDLogSync(@"%@ already registered, skipping", syncCheckFetcher);
	}
}

- (void)syncGet:(TaskQueue *)taskQueue updatedTables:(NSArray *)updatedTables syncUpdates:(NSArray *)syncUpdates
{
    __weak TaskQueue *weakTaskQueue = taskQueue;
	DDLogSync(@"Sync check returned: %@", updatedTables);
	if ([updatedTables containsObject:kSyncCategories] || ![syncUpdates containsObject:kSyncCategories]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForCategories:)];
	}
	if ([updatedTables containsObject:kSyncNutrients] || ![syncUpdates containsObject:kSyncNutrients]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForNutrients:)];
	}
	if ([updatedTables containsObject:kSyncUnits] || ![syncUpdates containsObject:kSyncUnits]) {
		[taskQueue addRequestWithIdentifier:[UnitMO entityName] withBlock:^{
			[self getUpdatesForMO:[UnitMO class] withTaskQueue:weakTaskQueue];
		}];
	}
	if ([updatedTables containsObject:kSyncUsers] || [updatedTables containsObject:kSyncUserGroups]
			|| ![syncUpdates containsObject:kSyncUsers]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatedUserProfile:)];
	}
	if ([updatedTables containsObject:kSyncPermissions] || ![syncUpdates containsObject:kSyncPermissions]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForUserPermissions:)];
	}
	/* Being done once daily (ugh)
	if ([updatedTables containsObject:kSyncTrackingType] || ![syncUpdates containsObject:kSyncTrackingType]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForTrackingType:)];
	}
	*/
	if ([updatedTables containsObject:kSyncUserStores] || ![syncUpdates containsObject:kSyncUserStores]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForUserStores:)];
	}
	if ([updatedTables containsObject:kSyncCoachInvites] || ![syncUpdates containsObject:kSyncCoachInvites]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForCoachInvites:)];
	}
	if ([updatedTables containsObject:kSyncAdvisorUsers] || [updatedTables containsObject:kSyncAdvisorGroupUsers]
			|| ![syncUpdates containsObject:kSyncAdvisorUsers]) {
		//[taskQueue addRequest:self targetSelector:@selector(getUpdatesForAdvisors:)];
		[taskQueue addRequestWithIdentifier:[AdvisorMO entityName] withBlock:^{
			[self getUpdatesForMO:[AdvisorMO class] withTaskQueue:weakTaskQueue];
		}];
	}
	if ([updatedTables containsObject:kSyncAdvisorMealPlans] || ![syncUpdates containsObject:kSyncAdvisorMealPlans]) {
		[taskQueue addRequestWithIdentifier:[AdvisorMealPlanMO entityName] withBlock:^{
			[self getUpdatesForMO:[AdvisorMealPlanMO class] withTaskQueue:weakTaskQueue];
		}];
		[taskQueue addRequestWithIdentifier:[AdvisorMealPlanTimelineMO entityName] withBlock:^{
			[self getUpdatesForMO:[AdvisorMealPlanTimelineMO class] withTaskQueue:weakTaskQueue];
		}];
	}
	if ([updatedTables containsObject:kSyncAdvisorActivityPlans] || ![syncUpdates containsObject:kSyncAdvisorActivityPlans]) {
		[taskQueue addRequestWithIdentifier:[AdvisorActivityPlanMO entityName] withBlock:^{
			[self getUpdatesForMO:[AdvisorActivityPlanMO class] withTaskQueue:weakTaskQueue];
		}];
		[taskQueue addRequestWithIdentifier:[AdvisorActivityPlanTimelineMO entityName] withBlock:^{
			[self getUpdatesForMO:[AdvisorActivityPlanTimelineMO class] withTaskQueue:weakTaskQueue];
		}];
	}
	if ([updatedTables containsObject:kSyncCommunities] || ![syncUpdates containsObject:kSyncCommunities]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForCommunities:)];
	}
	if ([updatedTables containsObject:kSyncThreads] || ![syncUpdates containsObject:kSyncThreads]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForThreads:)];
	}
	if ([updatedTables containsObject:kSyncThreadMessages] || ![syncUpdates containsObject:kSyncThreadMessages]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForThreadMessages:)];
	}
	if ([updatedTables containsObject:kSyncMessages] || ![syncUpdates containsObject:kSyncMessages]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForMessages:)];
	}
	if ([updatedTables containsObject:kSyncCategorySorting] || ![syncUpdates containsObject:kSyncCategorySorting]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForStoreCategories:)];
	}
	if ([updatedTables containsObject:kSyncProducts] || ![syncUpdates containsObject:kSyncProducts]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForCustomFoods:)];
	}
	if ([updatedTables containsObject:kSyncRecipes] || ![syncUpdates containsObject:kSyncRecipes]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForMeals:)];
	}
	if ([updatedTables containsObject:kSyncFoodLog] || ![syncUpdates containsObject:kSyncFoodLog]) {
		[taskQueue addRequestWithIdentifier:[FoodLogMO entityName] withBlock:^{
			[self getUpdatesWithPagingForMO:[FoodLogMO class] withTaskQueue:weakTaskQueue];
		}];
	}
	if ([updatedTables containsObject:kSyncLogNotes] || ![syncUpdates containsObject:kSyncLogNotes]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForLoggingNotes:)];
	}
	if ([updatedTables containsObject:kSyncShoppingSuggestions] || ![syncUpdates containsObject:kSyncShoppingSuggestions]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForRecommendations:)];
	}
	if ([updatedTables containsObject:kSyncShopping] || ![syncUpdates containsObject:kSyncShopping]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForShoppingList:)];
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForPurchaseHx:)];
	}
	if ([updatedTables containsObject:kSyncExerciseRoutines] || ![syncUpdates containsObject:kSyncExerciseRoutines]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForExerciseRoutines:)];
	}
	if ([updatedTables containsObject:kSyncExerciseStrength] || ![syncUpdates containsObject:kSyncExerciseStrength]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForTrainingStrength:)];
	}
	if ([updatedTables containsObject:kSyncExerciseAssessment] || ![syncUpdates containsObject:kSyncExerciseAssessment]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForTrainingAssessment:)];
	}
	if ([updatedTables containsObject:kSyncExerciseActivities] || ![syncUpdates containsObject:kSyncExerciseActivities]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForTrainingActivity:)];
	}
	if ([updatedTables containsObject:kSyncTracking] || ![syncUpdates containsObject:kSyncTracking]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForTracking:)];
	}
	if ([updatedTables containsObject:kSyncMealPlan] || ![syncUpdates containsObject:kSyncMealPlan]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForMealPlanners:)];
	}
	if ([updatedTables containsObject:kSyncActivityPlan] || ![syncUpdates containsObject:kSyncActivityPlan]) {
		[taskQueue addRequestWithIdentifier:[ActivityPlanMO entityName] withBlock:^{
			[self getUpdatesForMO:[ActivityPlanMO class] withTaskQueue:weakTaskQueue];
		}];
	}
	if ([updatedTables containsObject:kSyncActivityLog] || ![syncUpdates containsObject:kSyncActivityLog]) {
		[taskQueue addRequestWithIdentifier:[ActivityLogMO entityName] withBlock:^{
			[self getUpdatesForMO:[ActivityLogMO class] withTaskQueue:weakTaskQueue];
		}];
	}
	if ([updatedTables containsObject:kSyncUserHealthFilters] || ![syncUpdates containsObject:kSyncUserHealthFilters]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForHealthChoices:)];
	}
	if ([updatedTables containsObject:kSyncPantry] || ![syncUpdates containsObject:kSyncPantry]) {
		[taskQueue addRequest:self targetSelector:@selector(getUpdatesForPantry:)];
	}
}

@end
