//
//  BCActivitySearchSearchViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/18/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCActivitySearchSearchViewController.h"

#import "ActivityLogMO.h"
#import "BCActivitySearchDataModel.h"
#import "BCProgressHUD.h"
#import "ExerciseRoutineMO.h"
#import "TrainingActivityMO.h"
#import "User.h"

@interface BCActivitySearchSearchViewController () <UISearchBarDelegate, UISearchDisplayDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@end

@implementation BCActivitySearchSearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.tableView.tableHeaderView = self.searchBar;
}

- (void)viewDidAppear:(BOOL)animated
{
	[self.searchBar becomeFirstResponder];

	[super viewDidAppear:animated];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = [self.dataModel.filteredActivities count];
	if (self.dataModel.moreResults) {
		numberOfRows += 1;
	}
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *normalCellId = @"NormalCell";
    static NSString *moreCellId = @"MoreCell";
    UITableViewCell *cell = nil;

	if (indexPath.row < [self.dataModel.filteredActivities count]) {
		cell = [tableView dequeueReusableCellWithIdentifier:normalCellId forIndexPath:indexPath];
		NSDictionary *activityDict = [self.dataModel.filteredActivities objectAtIndex:indexPath.row];
		cell.textLabel.text = [activityDict valueForKey:kName];
	}
	else {
		cell = [tableView dequeueReusableCellWithIdentifier:moreCellId forIndexPath:indexPath];
	}

    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row < [self.dataModel.filteredActivities count]) {
		ActivityLogMO *activityLogMO = [ActivityLogMO insertFromSearchDict:[self.dataModel.filteredActivities objectAtIndex:indexPath.row]
			inManagedObjectContext:self.dataModel.managedObjectContext];
		activityLogMO.loginId = [User loginId];
		activityLogMO.targetDate = self.dataModel.logDate;
		activityLogMO.status = kStatusPost;

		[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your log", activityLogMO.activityName]
			onView:self.view.superview];
	}
	else {
		// More search
		MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
		hud.labelText = @"searching...";
		[self.dataModel activitySmartSearchMoreWithCompletionBlock:^(NSUInteger resultCount, BOOL more, NSError *error) {
			[MBProgressHUD hideHUDForView:self.view animated:YES];
			if (!error) {
				// Ensure to adapt the count based upon whether or not we're showing the 'more' row
				NSUInteger oldCount = [self.dataModel.filteredActivities count] - resultCount - (more ? 0 : 1);
				NSMutableArray *indexPaths = [NSMutableArray array];
				for (int idx = 0; idx < resultCount; idx++) {
					[indexPaths addObject:[NSIndexPath indexPathForRow:(oldCount + idx) inSection:0]];
				}
				[self.tableView beginUpdates];
				if (!more) {
					// Remove the 'more' row
					[self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(oldCount - 1) inSection:0]]
						withRowAnimation:UITableViewRowAnimationAutomatic];
				}
				[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
				[self.tableView endUpdates];
			}
		}];
	}
}

#pragma mark - responders

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	[searchBar resignFirstResponder];

	MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.labelText = @"searching...";

	[self.dataModel activitySmartSearch:searchBar.text completionBlock:^(NSUInteger resultCount, BOOL more, NSError *error) {
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		if (!error) {
			[self.tableView reloadData];
		}
	}];
}

@end
