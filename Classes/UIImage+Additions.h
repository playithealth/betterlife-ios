//
//  UIImage+Additions.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 5/12/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIImage (Additions)
    
- (UIImage *)BC_imageTintedWithColor:(UIColor *)color;
- (UIImage *)BC_imageTemplateWithColor:(UIColor *)color;
- (UIImage *)BC_imageWithImageOverlay:(UIImage *)overlayImage atPoint:(CGPoint)point atAngle:(CGFloat)angle;
- (UIImage *)BC_imageWithTargetSize:(CGSize)targetSize;
- (UIImage *)BC_imageGrayscale;

@end
