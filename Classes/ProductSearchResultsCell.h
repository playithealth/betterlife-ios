//
//  ProductSearchResultsCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/14/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StarRatingControl;

@interface ProductSearchResultsCell : UITableViewCell {
    
	UILabel *nameLabel;
	UILabel *sizeLabel;
	UIImageView *productImageView;
	UIImageView *alertImageView;
	UIImageView *lifestyleImageView;
	StarRatingControl *ratingControl;
}

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *sizeLabel;
@property (nonatomic, strong) IBOutlet UIImageView *productImageView;
@property (nonatomic, strong) IBOutlet UIImageView *alertImageView;
@property (nonatomic, strong) IBOutlet UIImageView *lifestyleImageView;
@property (nonatomic, strong) IBOutlet StarRatingControl *ratingControl;

@end
