//
//  ShoppingListItemViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "ShoppingListItemViewController.h"

#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BCAppDelegate.h"
#import "CategorySelectViewController.h"
#import "GTMHTTPFetcherAdditions.h"
#import "GenericValueDisplay.h"
#import "HTTPQueue.h"
#import "NSArray+NestedArrays.h"
#import "NumberValue.h"
#import "NutritionCell.h"
#import "ProductPurchaseHistoryView.h"
#import "ProductSearchView.h"
#import "Store.h"

static const NSUInteger kTagName = 10001;
static const NSUInteger kTagQuantity = 10002;
static const NSUInteger kTagCategory = 10003;
static const NSUInteger kTagBarcode = 10004;
static const NSUInteger kTagLabel = 11111;
static const NSUInteger kTagSwitch = 11112;

static const NSInteger kSectionItem = 0;
static const NSInteger kSectionQuantity = 1;
static const NSInteger kSectionCategory = 2;
static const NSInteger kSectionNutrition = 3;
static const NSInteger kSectionDescription = 4;

@interface ShoppingListItemViewController ()
<CategorySelectViewDelegate, ProductSearchViewDelegate, UITextFieldDelegate, UIWebViewDelegate, ZBarReaderDelegate>

/// the nutrition html returned from the api for the related product
@property (strong, nonatomic) NSString *itemNutrition;
/// flag that tells whether the item needs to be saved or not
/// maybe should be renamed hasChanges or something
@property (assign, nonatomic) BOOL changedItem;
/// the text field currently being edited
@property (strong, nonatomic) UITextField *editingTextField;
/// height of the nutrition row which is dynamic
@property (assign, nonatomic) NSInteger nutritionRowHeight;
/// the web view which displays the nutrition html
/// the spinner shown on the nutrition row while loading
@property (strong, nonatomic) UIActivityIndicatorView *nutritionSpinner;
@property (strong, nonatomic) UIImage *productImage;
@property (strong, nonatomic) NSIndexPath *editingIndexPath;

/// a helper function which configures the UI elements in the cell
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

- (void)refreshProductImage;
- (void)keyboardDidShowNotification:(NSNotification*)aNotification;
- (void)keyboardWillHideNotification:(NSNotification*)aNotification;
@end

@implementation ShoppingListItemViewController

#pragma mark - view lifecycle
- (void)viewDidLoad
{
	// create the spinner that is used to show that it is loading nutrition data
	UIActivityIndicatorView *aSpinner = [[UIActivityIndicatorView alloc]
		initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	aSpinner.hidesWhenStopped = YES;
	self.nutritionSpinner = aSpinner;
	
	HTTPQueue *detailsQueue = [HTTPQueue queueWithCompletionHandler:
		^(NSString *identifier, NSError *error) {
			QuietLog(@"completion block for %@, error:%@", identifier, error);
			[self.tableView reloadData];
		}
		identifier:@"shoppingListItemDetails"];

	if (self.shoppingListItem.imageId) {
		[self refreshProductImage];
	}

	if ([self.shoppingListItem.productId integerValue] != 0) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		if (appDelegate.connected) {

			__block ShoppingListItemViewController *me = self;

			// put something in the nutrition so that the section shows up
			// this gets overridden in the nutritionFetcher
			self.itemNutrition = @"loading";

			GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
				[BCUrlFactory nutritionForProductURL:self.shoppingListItem.productId]];
			HTTPQueueHandler nutritionHandler = ^void(NSData *retrievedData, NSError *error) {
				NSIndexPath *nutritionIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
				UITableViewCell *cell = [me.tableView cellForRowAtIndexPath:nutritionIndexPath];

				NSString *response = [[NSString alloc] initWithData:retrievedData
					encoding:NSUTF8StringEncoding];
				if (!error) {
					// fetch succeeded

					if ([response isKindOfClass:[NSString class]]) {
						me.itemNutrition = response;
						me.shoppingListItem.nutritionHtml = me.itemNutrition;

						// Save the context.
						[me.shoppingListItem.managedObjectContext save:nil];

						[me.nutritionSpinner stopAnimating];
						[me setNutritionSpinner:nil];
						cell.accessoryView = nil;

						cell.textLabel.text = nil;

						me.nutritionWebView.hidden = NO;
						me.nutritionWebView.delegate = me;
						me.nutritionRowHeight = 44;
						[me.nutritionWebView loadHTMLString:me.itemNutrition baseURL:nil];
					}
				}
				else {
					[me.nutritionSpinner stopAnimating];
					[me setNutritionSpinner:nil];
					cell.accessoryView = nil;

					cell.textLabel.text = @"Failed to load nutrition";

					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
						withFetcher:fetcher response:response];
				}
			};
			[detailsQueue addRequest:fetcher withCompletionHandler:nutritionHandler];
		}
		else {
			NSIndexPath *nutritionIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
			UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:nutritionIndexPath];
			cell.textLabel.text = @"Failed to load nutrition";
			[self.nutritionSpinner stopAnimating];
			[self setNutritionSpinner:nil];
			cell.accessoryView = nil;
		}
	}

	[detailsQueue runQueue:nil];

	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
	self.title = @"Item Details";

	[self.tableView reloadData];

	// Register for notification when the keyboard will be shown
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardDidShowNotification:)
		name:UIKeyboardDidShowNotification
		object:nil];

	// Register for notification when the keyboard will be hidden
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardWillHideNotification:)
		name:UIKeyboardWillHideNotification
		object:nil];

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// tell the shopping list view that it needs to save
	if (self.changedItem) {
		self.shoppingListItem.isRecommendation = [NSNumber numberWithBool:NO];
		if (self.delegate && [self.delegate conformsToProtocol:@protocol(ShoppingListItemViewDelegate)]) {
			[self.delegate shoppingListItemView:self didUpdateItem:self.shoppingListItem];
		}
	}
	self.changedItem = NO;

	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:nil object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:UIKeyboardWillHideNotification object:nil];

	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
	self.shoppingListItem = nil;

	// Unsure if these are necessary or correct being here
	self.itemNutrition = nil;

    [self setTableView:nil];
    [self setProductImageView:nil];
    [self setNameLabel:nil];
    [self setSizeLabel:nil];
    [self setHealthImageView:nil];
    [self setAlertImageView:nil];
    [self setQuantityLabel:nil];
    [self setCategoryLabel:nil];
    [self setNutritionWebView:nil];
	[super viewDidUnload];
}


#pragma mark - local methods
- (void)resignKeyboard:(id)sender
{
	self.editingTextField.enabled = NO;
	[self.editingTextField resignFirstResponder];
	self.editingTextField = nil;
}

- (void)refreshProductImage
{
	NSURL *url = [BCUrlFactory imageURLForImage:self.shoppingListItem.imageId
		imageSize:@"medium"];
	GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher 
		signedFetcherWithURL:url];
	[imageFetcher beginFetchWithCompletionHandler:
		^(NSData *retrievedData, NSError *error) {
			if (!error) {
				UIImage *tempImage = [[UIImage alloc] initWithData:retrievedData];
				self.productImage = tempImage;
				NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
				UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
				[self configureCell:cell atIndexPath:indexPath];
			}
			else {
				[BCUtilities logHttpError:[error code] inFunctionNamed:__FUNCTION__
					withType:kGet withURL:[[imageFetcher.mutableRequest URL] absoluteString]];
			}
		}];
}

/*
- (void)purchaseHxButtonTapped:(UITapGestureRecognizer *)recognizer
{
	ProductPurchaseHistoryView *view = [[ProductPurchaseHistoryView alloc]
		initWithNibName:nil bundle:nil];
	view.purchasedItem = (id<PurchasedItem>)self.shoppingListItem;
	[self.navigationController pushViewController:view animated:YES];
}
*/

- (IBAction)incrementButtonTapped:(id)sender 
{
	NSNumber *currentQuantity = [self.editingTextField.text
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	// this uses integerValue so that it will step to the next highest whole number
	NSNumber *incrementedQuantity = [NSNumber numberWithInteger:
		[currentQuantity integerValue] + 1];
	self.shoppingListItem.quantity = incrementedQuantity;
	self.editingTextField.text = [incrementedQuantity stringValue];
	self.changedItem = YES;
}

- (IBAction)decrementButtonTapped:(id)sender 
{
	if ([self.shoppingListItem.quantity compare:[NSNumber numberWithInteger:1]] ==
			NSOrderedDescending) {
		NSNumber *currentQuantity = [self.editingTextField.text
			numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
		NSNumber *decrementedQuantity = nil;
		// if this has a remainder, just chop that off
		if ([currentQuantity doubleValue] - [currentQuantity integerValue] > 0) {
			decrementedQuantity = [NSNumber numberWithInteger:
				[currentQuantity integerValue]];
		}
		// otherwise step down one
		else {
			decrementedQuantity = [NSNumber numberWithInteger:
				[currentQuantity integerValue] - 1];
		}
		self.shoppingListItem.quantity = decrementedQuantity;
		self.editingTextField.text = [decrementedQuantity stringValue];
	}
	self.changedItem = YES;
}

#pragma mark - UITableView data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Item, Quantity, Category, Nutrition, Description
	return 5;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 2) {
		return @"Category";
	}
	else {
		return nil;
	}
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//	CGFloat headerHeight = 0.0f;
//	if (section > 0) {
//		headerHeight = 25.0f;
//	}
//	return headerHeight;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView
   cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;

	switch (indexPath.section) {
		case kSectionItem :
		{
			static NSString *itemCellId = @"ShoppingListItemCellId";
			cell = [theTableView dequeueReusableCellWithIdentifier:itemCellId];
			// TODO load a custom cell
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:itemCellId];
			}
			break;
		}
			
		case kSectionQuantity :
		{
			static NSString *quantityCellId = @"ShoppingListItemQuantityCellId";
			cell = [theTableView dequeueReusableCellWithIdentifier:quantityCellId];
			// TODO load a custom cell
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:quantityCellId];
			}
			break;
		}

		case kSectionCategory:
		{
			static NSString *categoryCellId = @"ShoppingListItemCategoryCellId";

			cell = [theTableView dequeueReusableCellWithIdentifier:categoryCellId];
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:categoryCellId];
			}
			break;
		}

		case kSectionNutrition:
		{
			static NSString *nutritionCellId = @"ShoppingListItemNutritionCellId";

			cell = [theTableView dequeueReusableCellWithIdentifier:nutritionCellId];
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
					reuseIdentifier:nutritionCellId];

				cell.accessoryView = self.nutritionSpinner;
				[self.nutritionSpinner startAnimating];

				cell.textLabel.text = @"checking for nutrition info...";

				[cell.contentView addSubview:self.nutritionWebView];
			}
			break;
		}
			
		case kSectionDescription:
		{
			static NSString *descriptionCellId = @"ShoppingListItemDescriptionCellId";
			
			cell = [theTableView dequeueReusableCellWithIdentifier:descriptionCellId];
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:descriptionCellId];
			}
			break;
		}

		default:
			break;
	}

	if (cell) {
		[self configureCell:cell atIndexPath:indexPath];
	}

	return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	switch (indexPath.section) {
		case kSectionItem:
			cell.textLabel.text = self.shoppingListItem.name;
//			cell.detailTextLabel.text = self.shoppingListItem.size;
			if (self.productImage) {
				cell.imageView.image = self.productImage;
			}
			else {
				cell.imageView.image = [UIImage imageNamed:@"placeholder.png"];
			}
			break;
			
		case kSectionQuantity:
			cell.textLabel.text = [self.shoppingListItem.quantity stringValue];
			break;

		case kSectionCategory:
			cell.textLabel.text = self.shoppingListItem.category.name;
			break;

		case kSectionNutrition :
			// the nutrition html is loaded from the nutritionFetcher
			break;

		case kSectionDescription:
			cell.textLabel.text = @"Some item details";
			break;

		default:
			break;
	}
}

#pragma mark - UITableView delegates
- (NSIndexPath *)tableView:(UITableView *)tableView
   willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	return nil;
}

- (void)tableView:(UITableView *)theTableView
   didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// TODO do something when the user selects?
}

- (CGFloat)tableView:(UITableView *)tableView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight;

	switch (indexPath.section) {
		case kSectionItem:
		{
			CGFloat imageViewWidth = 80;
			CGFloat spacing = 10;
			CGSize constraintSize = CGSizeMake(self.tableView.frame.size.width - imageViewWidth - (spacing * 3), CGFLOAT_MAX);
			
			CGSize labelSize = [self.shoppingListItem.name sizeWithFont:[UIFont boldSystemFontOfSize:22] constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
			CGFloat detailHeight = 20;
			CGFloat ratingControlHeight = 15;
			
			rowHeight = MAX(44.0, (labelSize.height + spacing + detailHeight + spacing + ratingControlHeight + spacing));
			QuietLogFloat(rowHeight);
			break;
		}	

		case kSectionNutrition:
			// This gets set in the web view delegate
			rowHeight = self.nutritionRowHeight;
			break;

		case kSectionDescription:
		case kSectionQuantity:
		case kSectionCategory:
		default:
			rowHeight = 44;
			break;
	}

	return rowHeight;
}

#pragma mark - ProductSearchViewDelegate
- (void)productSearchView:(ProductSearchView *)productSearchView
   didSelectProduct:(NSDictionary *)product
{
	QuietLog(@"%s: %@", __FUNCTION__, [product description]);
	self.shoppingListItem.name = [product valueForKey:kItemName];
	self.shoppingListItem.productId = [[product valueForKey:kProductId]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	self.shoppingListItem.category = [CategoryMO categoryById:
		[[product valueForKey:kProductCategoryId]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle]
		withManagedObjectContext:self.shoppingListItem.managedObjectContext];
	self.shoppingListItem.barcode = [product valueForKey:kBarcode];
	self.shoppingListItem.imageId = [product valueForKey:kImageId];

	[self.tableView reloadData];

	[self refreshProductImage];
}

#pragma mark - CategorySelectViewControllerdelegate
- (void)categorySelectView:(CategorySelectViewController *)categoryView
   didSelectCategory:(CategoryMO *)category
{
	self.shoppingListItem.category = category;
	self.shoppingListItem.storeCategory = [[Store currentStore] storeCategoryForCategory:category];
	self.changedItem = YES;

	NSIndexPath *categoryRowPath = [NSIndexPath indexPathForRow:2 inSection:0];
	
	[self.navigationController popViewControllerAnimated:YES];

	[self configureCell:[self.tableView cellForRowAtIndexPath:categoryRowPath] atIndexPath:categoryRowPath];
}

#pragma mark - ZBarReaderDelegate
- (void)imagePickerController:(UIImagePickerController *)reader
   didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	id <NSFastEnumeration> results =
		[info objectForKey:ZBarReaderControllerResults];

	ZBarSymbol *symbol = nil;
	for (symbol in results) {
		break;
	}

	self.shoppingListItem.barcode = symbol.data;
	self.changedItem = YES;

	NSIndexPath *barcodeRowPath = [NSIndexPath indexPathForRow:3 inSection:0];

	[self configureCell:[self.tableView cellForRowAtIndexPath:barcodeRowPath]
		atIndexPath:barcodeRowPath];

	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
	NSNumber *currentQuantity = [textField.text
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	self.shoppingListItem.quantity = currentQuantity;
	self.changedItem = YES;
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	CGFloat webViewHeight = 0.0f;
	if (self.nutritionWebView.subviews.count > 0) {
		UIView *scrollerView = [self.nutritionWebView.subviews objectAtIndex:0];
		if (scrollerView.subviews.count > 0) {
			UIView *webDocView = scrollerView.subviews.lastObject;
			if ([webDocView isKindOfClass:[NSClassFromString (@"UIWebDocumentView")class]]) {
				webViewHeight = webDocView.frame.size.height;
				self.nutritionRowHeight = webViewHeight + 16;
				self.nutritionWebView.frame = CGRectMake(0, 8, 300, webViewHeight);
				self.nutritionWebView.delegate = nil;
				[self.tableView reloadData];
			}
		}
	}
	QuietLog(@"height %f", webViewHeight);
}

#pragma mark - Notifications for keyboard hide/show
- (void)keyboardDidShowNotification:(NSNotification*)aNotification
{
	NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
 
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;

	[self.tableView scrollToRowAtIndexPath:self.editingIndexPath 
		atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)keyboardWillHideNotification:(NSNotification*)aNotification 
{
	self.tableView.contentInset = UIEdgeInsetsZero;
	self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}   

@end
