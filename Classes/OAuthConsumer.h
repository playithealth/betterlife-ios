//
//  OAuthConsumer.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/22/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "KeychainItemWrapper.h"

@interface OAuthConsumer : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *oauthConsumerKey;
@property (nonatomic, strong) NSString *oauthConsumerSecret;
@property (nonatomic, strong) NSString *oauthConsumerHost;

- (BOOL)consumerIsValid;
+ (OAuthConsumer *)currentOAuthConsumer;
+ (void)refreshOAuthConsumerWithSuccess:(void(^)(void))success failure:(void(^)(NSError *))failure;

@end
