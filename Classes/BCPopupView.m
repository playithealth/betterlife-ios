//
//  BCPopupView.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/6/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCPopupView.h"

#import "NSArray+NestedArrays.h"
#import "UIColor+Additions.h"

static const CGFloat kCaretHeight = 12.0f;
static const CGFloat kCaretWidth = 20.0f;
static const CGFloat kSpacing = 10.0f;
static const CGFloat kInnerFrameInset = 1.0f;

@interface BCPopupView () <UITableViewDataSource, UITableViewDelegate>
{
	NSMutableDictionary *_properties;
}

@end

@implementation BCPopupView
@dynamic properties;

- (id)initWithView:(UIView *)inView
		caretPosition:(CGPoint)caretPosition
		title:(NSString *)title
		delegate:(id)delegate
{
    self = [self initWithFrame:CGRectMake(0, 20, 320, 460)];
    if (self) {
		self.backgroundColor = [UIColor clearColor];

		self.outerFrame = CGRectMake(kSpacing, caretPosition.y + kCaretHeight,
			self.frame.size.width - kSpacing * 2, 
			self.frame.size.height - caretPosition.y - kCaretHeight - kSpacing);
		self.outerLayer = [CAGradientLayer layer];
		self.outerLayer.frame = self.outerFrame;
		self.outerLayer.colors = [NSArray arrayWithObjects:
			(id)[[UIColor BC_lightGreenColor] CGColor],
			(id)[[UIColor BC_darkGreenColor] CGColor],
			(id)[[UIColor BC_darkGreenColor] CGColor],
			nil];
		self.outerLayer.locations = [NSArray arrayWithObjects:
			[NSNumber numberWithFloat:0.0f],
			[NSNumber numberWithFloat:0.05f],
			[NSNumber numberWithFloat:1.0f],
			nil];
		self.outerLayer.cornerRadius = 4.0f;
		self.outerLayer.shadowOffset = CGSizeMake(0, 3);
		self.outerLayer.shadowRadius = 4.0f;
		self.outerLayer.shadowColor = [[UIColor blackColor] CGColor];
		self.outerLayer.shadowOpacity = 0.8f;
		self.outerLayer.borderColor = [[UIColor BC_darkGreenColor] CGColor];
		self.outerLayer.borderWidth = 1.0f;
		[self.layer addSublayer:self.outerLayer];

		CGFloat headerHeight = 0.0;
		if (title) {
			headerHeight = 30.0;
		}
		self.innerFrame = CGRectMake(kSpacing + kInnerFrameInset, 
			caretPosition.y + kCaretHeight + kInnerFrameInset + headerHeight,
			self.outerFrame.size.width - kInnerFrameInset * 2, 
			self.outerFrame.size.height - kInnerFrameInset * 2 - 30);
		self.innerLayer = [CALayer layer];
		self.innerLayer.frame = self.innerFrame;
		self.innerLayer.backgroundColor = [[UIColor whiteColor] CGColor];
		self.innerLayer.cornerRadius = 4.0f;
		[self.layer addSublayer:self.innerLayer];

		if (self.drawCaret) {
			// draw the caret outline
			CGMutablePathRef caretOutlinePath = CGPathCreateMutable();
			CGPathMoveToPoint(caretOutlinePath, nil, caretPosition.x, caretPosition.y);
			CGPathAddLineToPoint(caretOutlinePath, nil, caretPosition.x - (kCaretWidth / 2), caretPosition.y + kCaretHeight);
			CGPathAddLineToPoint(caretOutlinePath, nil, caretPosition.x + (kCaretWidth / 2), caretPosition.y + kCaretHeight);
			CGPathAddLineToPoint(caretOutlinePath, nil, caretPosition.x, caretPosition.y);
			CGPathCloseSubpath(caretOutlinePath);

			CAShapeLayer *caretOutlineLayer = [CAShapeLayer layer];
			caretOutlineLayer.path = caretOutlinePath;
			caretOutlineLayer.fillColor = [[UIColor BC_darkGreenColor] CGColor];
			[self.layer addSublayer:caretOutlineLayer];
			CGPathRelease(caretOutlinePath);

			// draw the caret inside
			CGMutablePathRef caretPath = CGPathCreateMutable();
			CGPathMoveToPoint(caretPath, nil, caretPosition.x, caretPosition.y + 1);
			CGPathAddLineToPoint(caretPath, nil, caretPosition.x - (kCaretWidth / 2), caretPosition.y + kCaretHeight + 1);
			CGPathAddLineToPoint(caretPath, nil, caretPosition.x + (kCaretWidth / 2), caretPosition.y + kCaretHeight + 1);
			CGPathAddLineToPoint(caretPath, nil, caretPosition.x, caretPosition.y + 1);
			CGPathCloseSubpath(caretPath);

			CAShapeLayer *caretLayer = [CAShapeLayer layer];
			caretLayer.path = caretPath;
			caretLayer.fillColor = [[UIColor BC_lightGreenColor] CGColor];
			[self.layer addSublayer:caretLayer];
			CGPathRelease(caretPath);
		}

		// Header
		if (title) {
			UILabel *aLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.outerFrame.origin.x + kSpacing, 
					self.outerFrame.origin.y + 1.0f, self.innerLayer.frame.size.width - kSpacing * 2.0f, 30.0f)];
			aLabel.adjustsFontSizeToFitWidth = YES;
			aLabel.textAlignment = NSTextAlignmentCenter;
			aLabel.font = [UIFont boldSystemFontOfSize:18.0f];
			aLabel.text = title;
			aLabel.textColor = [UIColor whiteColor];
			aLabel.shadowColor = [UIColor blackColor];
			aLabel.backgroundColor = [UIColor clearColor];
			self.titleLabel = aLabel;
			[self addSubview:aLabel];
		}

		if (inView) {
			self.customView = inView;
			CGRect customViewFrame = self.innerFrame;
			if ([inView isKindOfClass:[UIPickerView class]]) {
				customViewFrame = CGRectMake(self.innerFrame.origin.x, self.innerFrame.origin.y,
                    self.innerFrame.size.width, 216);
			}
			inView.frame = customViewFrame;
			[self addSubview:inView];
		}
 
		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(handleTapBackground:)];
		[self addGestureRecognizer:recognizer];

		self.delegate = delegate;
	}

    return self;
}

- (id)initWithTableViewStyle:(UITableViewStyle)style
		caretPosition:(CGPoint)caretPosition
		title:(NSString *)title
		delegate:(id)delegate
{
    self = [self initWithView:nil caretPosition:caretPosition title:title delegate:delegate];
    if (self) {
		UITableView *table = [[UITableView alloc]
				initWithFrame:CGRectMake(self.innerFrame.origin.x, 
						self.innerFrame.origin.y + kInnerFrameInset,
						self.innerFrame.size.width, 
						self.innerFrame.size.height - kInnerFrameInset * 2)
				style:UITableViewStylePlain];

		table.dataSource = self;
		table.delegate = self;

		table.frame = self.innerFrame;
		[self addSubview:table];
		self.tableView = table;
	}

    return self;
}

- (id)initWithTableViewStyle:(UITableViewStyle)style
		caretPosition:(CGPoint)caretPosition
		title:(NSString *)title
		delegate:(id)delegate
		rowTitles:(NSArray *)inRowTitles
{
    self = [self initWithTableViewStyle:style caretPosition:caretPosition title:title delegate:delegate];
    if (self) {
		if ([inRowTitles count] > 0) {
			if ([[inRowTitles objectAtIndex:0] isKindOfClass:[NSArray class]]) {
				self.rowTitles = [inRowTitles copy];
			}
			else {
				self.rowTitles = [NSArray arrayWithObject:inRowTitles];
			}
 		}
	}

    return self;
}

- (id)initWithTableViewStyle:(UITableViewStyle)style
		caretPosition:(CGPoint)caretPosition
		title:(NSString *)title
		delegate:(id)delegate
		rowTitles:(NSArray *)inRowTitles
		sectionHeaders:(NSArray *)inSectionHeaders
{
    self = [self initWithTableViewStyle:style 
		caretPosition:caretPosition 
		title:title 
		delegate:delegate
		rowTitles:inRowTitles];
    if (self) {
		if (inSectionHeaders) {
			self.sectionHeaders = [inSectionHeaders copy];
 		}
	}

    return self;
}

#pragma mark - event handlers
- (void)handleTapCell:(UITapGestureRecognizer *)recognizer
{
	// this will only get called from a popup view with a table, so this should be safe
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *selectedIndexPath = [self.tableView indexPathForRowAtPoint:location];
	if (self.delegate 
			&& [self.delegate conformsToProtocol:@protocol(BCPopupViewDelegate)]) {
		[self.delegate popupView:self didDismissWithSelectedIndexPath:selectedIndexPath];
	}
	[self removeFromSuperview];
}

- (void)handleTapBackground:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self];
	if (!CGRectContainsPoint(self.outerFrame, location)) {
		if (self.delegate 
				&& [self.delegate conformsToProtocol:@protocol(BCPopupViewDelegate)]) {
			if ([self.delegate respondsToSelector:@selector(popupView:didDismissWithInfo:)]) {
				[self.delegate popupView:self didDismissWithInfo:nil];
			}
			else if ([self.delegate respondsToSelector:@selector(popupView:didDismissWithSelectedIndexPath:)]) {
				[self.delegate popupView:self didDismissWithSelectedIndexPath:nil];
			}
		}
		[self removeFromSuperview];
	}
}

- (void)handleDoneButton:(UITapGestureRecognizer *)recognizer
{
	
	if (self.delegate 
			&& [self.delegate conformsToProtocol:@protocol(BCPopupViewDelegate)]) {
		[self.delegate popupView:self didDismissWithInfo:self.properties];
	}
	[self removeFromSuperview];
}

#pragma mark - property management
- (void)setProperties:(NSDictionary *)dict
{
	_properties = [dict mutableCopy];
}

- (NSDictionary *)properties
{
	return _properties;
}

- (void)setProperty:(id)obj forKey:(NSString *)key
{
	if (_properties == nil && obj != nil)
	{
		[self setProperties:[NSDictionary dictionary]];
	}

	[_properties setValue:obj forKey:key];
}

- (id)propertyForKey:(NSString *)key
{
	return [_properties objectForKey:key];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)theTableView 
cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *popupViewCellId = @"BCPopupViewCellId";

	id rowInfo = [self.rowTitles BC_nestedObjectAtIndexPath:indexPath];

	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:popupViewCellId];
	if (cell == nil) {
		if ([rowInfo isKindOfClass:[NSString class]]) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
				reuseIdentifier:popupViewCellId];
		}
		else if ([rowInfo isKindOfClass:[NSDictionary class]]) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
				reuseIdentifier:popupViewCellId];
		}

		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(handleTapCell:)];
		[cell.contentView addGestureRecognizer:recognizer];
	}

	// configure the cell
	if ([rowInfo isKindOfClass:[NSString class]]) {
		cell.textLabel.text = rowInfo;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0f];
	}
	else if ([rowInfo isKindOfClass:[NSDictionary class]]) {
		cell.textLabel.text = [rowInfo valueForKey:@"labelText"];
		cell.detailTextLabel.text = [rowInfo valueForKey:@"detailLabelText"];
		cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
	}

	return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return self.sectionHeaders ? [self.sectionHeaders count] : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.rowTitles BC_countOfNestedArrayAtIndex:section];
}

#pragma mark - UITableViewDelegate
- (NSString *)tableView:(UITableView *)tableView 
titleForHeaderInSection:(NSInteger)section
{
	id header = [self.sectionHeaders objectAtIndex:section];
	if ((NSNull *)header == [NSNull null]) {
		return nil;
	}
	return header;
}

@end
