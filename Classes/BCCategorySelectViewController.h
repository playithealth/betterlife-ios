//
//  BCCategorySelectViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/7/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CategoryMO;

@protocol BCCategorySelectDelegate;

@interface BCCategorySelectViewController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) id<BCCategorySelectDelegate> delegate;

@end

@protocol BCCategorySelectDelegate <NSObject>
@required
- (void)categorySelectView:(BCCategorySelectViewController *)categorySelectView didSelectCategory:(CategoryMO *)category;
@end
