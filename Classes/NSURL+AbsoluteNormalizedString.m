//
//  NSURL+AbsoluteNormalizedString.m
//

#import "NSURL+AbsoluteNormalizedString.h"

@implementation NSURL (AbsoluteNormalizedString)

- (NSString *)absoluteNormalizedString {
	NSString *normalizedString = [self absoluteString];

	if ([[self path] length] == 0 && [[self query] length] == 0) {
		normalizedString = [NSString stringWithFormat:@"%@/", [self absoluteString]];
	}
	
	return normalizedString;
}
- (NSURL *)urlWithoutQuery {
	NSURL *composedURL = self;
	NSString *absoluteString = [self absoluteString];
	NSRange queryRange = [absoluteString rangeOfString:@"?"];
	
	if (queryRange.location != NSNotFound) {
		NSString *urlSansQuery = [absoluteString substringToIndex:queryRange.location];
		composedURL = [NSURL URLWithString:urlSansQuery];
	}
	
	return composedURL;
}

@end
