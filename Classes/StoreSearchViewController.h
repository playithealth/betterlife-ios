//
//  StoreSearchViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "StoreSelectionDelegate.h"

@interface StoreSearchViewController : UIViewController
<StoreSelectionDelegate, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate,
UITextFieldDelegate, CLLocationManagerDelegate> 

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) id<StoreSelectionDelegate> delegate;

@end
