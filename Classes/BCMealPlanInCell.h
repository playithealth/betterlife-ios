//
//  BCMealPlanInCell.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 7/3/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCTableViewCell.h"

@interface BCMealPlanInCell : BCTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *servingsLabel;
@property (weak, nonatomic) IBOutlet UILabel *servingsUnitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *caloriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *caloriesUnitsLabel;

@end
