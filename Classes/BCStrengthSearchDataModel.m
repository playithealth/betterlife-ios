//
//  BCStrengthSearchDataModel.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/18/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCStrengthSearchDataModel.h"

#import "TrainingStrengthMO.h"
#import "TrainingStrengthSetMO.h"
#import "User.h"

@interface BCStrengthSearchDataModel ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSDate *logDate;
@property (strong, nonatomic) NSMutableArray *trainingStrengths;
@property (strong, nonatomic) NSMutableArray *trainingStrengthsGroupedByCount;
@property (strong, nonatomic) NSMutableArray *trainingStrengthsGroupedByDate;
@property (strong, nonatomic) NSMutableArray *trainingStrengthsOrderedByDate;

@end


@implementation BCStrengthSearchDataModel

#pragma mark - lifecycle
- (id)initWithManagedObjectContext:(NSManagedObjectContext *)moc withLogDate:(NSDate *)logDate
{
	if ((self = [super init])) {
		_managedObjectContext = moc;
		_logDate = logDate;
		_trainingStrengths = [[TrainingStrengthMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"%K = %@ AND %K <> %@", 
				TrainingStrengthMOAttributes.loginId, [[User currentUser] loginId],
				TrainingStrengthMOAttributes.status, kStatusDelete]
			inContext:_managedObjectContext] mutableCopy];

	}
	return self;
}

- (id)insertStrengthBasedOnTrainingStrength:(TrainingStrengthMO *)strength
{
	TrainingStrengthMO *newStrength = [TrainingStrengthMO insertInManagedObjectContext:self.managedObjectContext];
	newStrength.loginId = [[User currentUser] loginId];
	newStrength.date = self.logDate;
	newStrength.name = strength.name;

	for (TrainingStrengthSetMO *set in strength.sets) {
		TrainingStrengthSetMO *newSet = [TrainingStrengthSetMO insertInManagedObjectContext:self.managedObjectContext];

		newSet.sortOrder = set.sortOrder;
		newSet.weight = set.weight;
		newSet.reps = set.reps;

		newSet.trainingStrength = newStrength;
	}

	newStrength.status = kStatusPost;

	NSError *error = nil;
	if (![self.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	[_trainingStrengths addObject:newStrength];

	return newStrength;
}

- (NSArray *)trainingStrengthsGroupedByCount
{
	if (_trainingStrengthsGroupedByCount) {
		return _trainingStrengthsGroupedByCount;
	}

	if ([self.trainingStrengths count]) {
		NSMutableArray* strengthsWithCount = [[NSMutableArray alloc] init];
		for (TrainingStrengthMO *loggedStrength in self.trainingStrengths) {
			NSUInteger matchingStrengthIndex = [strengthsWithCount indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					NSDictionary *strengthDict = obj;
					TrainingStrengthMO *strength = [strengthDict objectForKey:@"strength"];
					if ([loggedStrength.name isEqualToString:strength.name]) {
						*stop = YES;
						return YES;
					}
					return NO;
				}];

			if (matchingStrengthIndex == NSNotFound) {
				NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

				[dict setObject:@1 forKey:@"count"];
				[dict setObject:loggedStrength forKey:@"strength"];

				[strengthsWithCount addObject:dict];
			}
			else {
				NSMutableDictionary *dict = [strengthsWithCount objectAtIndex:matchingStrengthIndex];
				NSNumber *count = [dict objectForKey:@"count"];
				[dict setObject:@([count integerValue] + 1)  forKey:@"count"];
			}
		}

		NSSortDescriptor *sortByCount = [NSSortDescriptor sortDescriptorWithKey:@"count" ascending:NO];
		NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"strength.name" ascending:YES];
		_trainingStrengthsGroupedByCount = [strengthsWithCount sortedArrayUsingDescriptors:@[sortByCount, sortByName]];
	}
	else {
		_trainingStrengthsGroupedByCount = [NSArray array];
	}

	return _trainingStrengthsGroupedByCount;
}

- (NSArray *)trainingStrengthsGroupedByDate
{
	if (_trainingStrengthsGroupedByDate) {
		return _trainingStrengthsGroupedByDate;
	}

	if ([self.trainingStrengths count]) {
		NSMutableArray *temp = [NSMutableArray array];

		NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:TrainingStrengthMOAttributes.date ascending:NO];
		NSArray *strengthsSortedByDate = [self.trainingStrengths sortedArrayUsingDescriptors:@[sortByDate]];

		NSDate *lastDate = nil;
		NSInteger strengthCount = 0;
		for (TrainingStrengthMO *strength in strengthsSortedByDate) {
			if ([strength.date isEqualToDate:lastDate]) {
				// grab the last item in the array
				NSMutableDictionary *strengthDict = [temp lastObject];

				// add this strength to the embedded array
				NSMutableArray *strengths = strengthDict[@"strengths"];
				[strengths addObject:strength];

				// remake the name string
				NSSet *names = [strengths valueForKeyPath:TrainingStrengthMOAttributes.name];
				strengthDict[@"name"] = [[names allObjects] componentsJoinedByString:@", "];
			}
			else if (strengthCount >= 30) {
				break;
			}
			else {
				lastDate = strength.date;
				strengthCount++;

				// create a new record
				NSMutableDictionary *strengthDict = [NSMutableDictionary dictionary];

				// give it a name
				strengthDict[@"name"] = strength.name;

				// set the date string
				strengthDict[TrainingStrengthMOAttributes.date] = [self stringFromDate:strength.date];

				// create the embedded array
				NSMutableArray *strengths = [NSMutableArray array];
				[strengths addObject:strength];
				strengthDict[@"strengths"] = strengths;

				// add this to the outer array
				[temp addObject:strengthDict];
			}
		}
		_trainingStrengthsGroupedByDate = temp;
	}
	else {
		_trainingStrengthsGroupedByDate = [NSArray array];
	}

	return _trainingStrengthsGroupedByDate;
}

- (NSArray *)trainingStrengthsOrderedByDate
{
	if (_trainingStrengthsGroupedByDate) {
		return _trainingStrengthsGroupedByDate;
	}

	if ([self.trainingStrengths count]) {

		NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:TrainingStrengthMOAttributes.date ascending:NO];
		NSArray *strengthsSortedByDate = [self.trainingStrengths sortedArrayUsingDescriptors:@[sortByDate]];

		if ([strengthsSortedByDate count] > 30) {
			_trainingStrengthsGroupedByDate = [strengthsSortedByDate subarrayWithRange:NSMakeRange(0, 30)];
		}
		else {
			_trainingStrengthsGroupedByDate = strengthsSortedByDate;
		}
	}
	else {
		_trainingStrengthsGroupedByDate = [NSArray array];
	}

	return _trainingStrengthsGroupedByDate;
}

#pragma mark - local
- (NSString *)stringFromDate:(NSDate *)date
{
	static NSDateFormatter *_dateFormatter = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
			_dateFormatter = [[NSDateFormatter alloc] init];
			[_dateFormatter setTimeStyle:NSDateFormatterNoStyle];
			[_dateFormatter setDateStyle:NSDateFormatterShortStyle];
		});

	NSString *string = [_dateFormatter stringFromDate:date];
	return string;
}

@end
