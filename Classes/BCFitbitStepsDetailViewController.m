//
//  BCFitbitStepsDetailViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 7/29/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCFitbitStepsDetailViewController.h"

#import "TrainingActivityMO.h"
#import "NumberValue.h"

@interface BCFitbitStepsDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepsLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *caloriesLabel;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@end

@implementation BCFitbitStepsDetailViewController
#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		self.dateFormatter = [[NSDateFormatter alloc] init];
		[self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[self.dateFormatter setDateStyle:NSDateFormatterShortStyle];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	[self configureView];
}

- (void)configureView
{
	if (self.activityLog) {
		self.dateLabel.text = [self.dateFormatter stringFromDate:self.activityLog.date];
		// TODO Deal with whether or not we want to display time
		self.timeLabel.hidden = YES;
		//self.timeLabel.text = self.activityLog.time;

		// TODO Add the steps back in once we have the new activities
		//self.stepsLabel.text = [self.activityLog.distance substringToIndex:range.location];

		self.caloriesLabel.text = [NSString stringWithFormat:@"%@", self.activityLog.calories];
	}
}

@end
