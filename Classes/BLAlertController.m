//
//  BLAlertController.m
//  BettrLife
//
//  Created by Greg Goodrich on 11/7/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLAlertController.h"
#import <objc/runtime.h>

//
// BLAlertAction
//
@interface BLAlertAction ()
@property (strong, nonatomic, readwrite) NSString *title;
@property (assign, nonatomic, readwrite) UIAlertActionStyle style;
@property (strong, nonatomic, readwrite) UIAlertAction *alertAction;
@property (copy, nonatomic) void (^handler)(UIAlertAction *);
@end

@implementation BLAlertAction

@synthesize title = _title;

static char objcRetainKey;

+ (BLAlertAction *)actionWithTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(void (^)(UIAlertAction *action))handler
{
	BLAlertAction *blAlertAction = [[BLAlertAction alloc] init];
	if (NSClassFromString(@"UIAlertAction")) {
		blAlertAction.alertAction = [UIAlertAction actionWithTitle:title style:style handler:handler];
	}
	else {
		blAlertAction.title = title;
		blAlertAction.style = style;
		blAlertAction.handler = handler;
	}

	return blAlertAction;
}

- (NSString *)title {
	return (self.alertAction ? self.alertAction.title : _title);
}

- (UIAlertActionStyle)style {
	return (self.alertAction ? self.alertAction.style : _style);
}

- (void)setEnabled:(BOOL)enabled {
	if (self.alertAction) {
		[self.alertAction setEnabled:enabled];
	}
	else {
		_enabled = enabled;
	}
}

@end

@interface BLAlertController ()
@property (assign, nonatomic) UIAlertControllerStyle style;
@property (strong, nonatomic) UIAlertController *alertController;
@property (strong, nonatomic) UIAlertView *alertView;
@property (strong, nonatomic, readwrite) NSArray *textFields;
@property (strong, nonatomic, readwrite) NSArray *actions;
@property (weak, nonatomic) UIViewController *presentingViewController;
@end

@implementation BLAlertController

- (id)init
{
	self = [super init];
	if (self) {
		_style = UIAlertControllerStyleAlert;
	}
	return self;
}

- (NSString *)title {
	return (self.alertController ? self.alertController.title : self.alertView.title);
}

- (void)setTitle:(NSString *)title {
	if (self.alertController) {
		self.alertController.title = title;
	}
	else {
		self.alertView.title = title;
	}
}

- (NSString *)message {
	return (self.alertController ? self.alertController.message : self.alertView.message);
}

- (void)setMessage:(NSString *)message {
	if (self.alertController) {
		self.alertController.message = message;
	}
	else {
		self.alertView.message = message;
	}
}

- (NSArray *)textFields
{
	return (self.alertController ? self.alertController.textFields : @[[self.alertView textFieldAtIndex:0]]);
}

- (NSArray *)actions
{
	return (self.alertController ? self.alertController.actions : _actions);
}

+ (BLAlertController *)alertControllerWithTitle:(NSString *)title message:(NSString *)message
preferredStyle:(UIAlertControllerStyle)style
{
	BLAlertController *blAlertController = [[BLAlertController alloc] init];

	if (NSClassFromString(@"UIAlertController")) {
		blAlertController.alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
	}
	else {
		blAlertController.alertView = [[UIAlertView alloc] initWithTitle:title message:message
			delegate:blAlertController cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
		blAlertController.alertView.alertViewStyle = UIAlertViewStyleDefault;
		blAlertController.style = style;
		blAlertController.title = title;
		blAlertController.message = message;
	}

	return blAlertController;
}

- (void)addAction:(BLAlertAction *)action
{
	if (self.alertController) {
		[self.alertController addAction:action.alertAction];
	}
	else {
		if (_actions) {
			_actions = [_actions arrayByAddingObject:action];
			// Add the button that corresponds to this action
			[self.alertView addButtonWithTitle:action.title];
		}
		else {
			_actions = [NSArray arrayWithObject:action];
			// No need to add a button, as the first button is automatic
			// However, if the button title doesn't match our default that was set on creation, then recreate the alert view
			if (![action.title isEqualToString:[self.alertView buttonTitleAtIndex:0]]) {
				UIAlertViewStyle currentStyle = self.alertView.alertViewStyle;
				self.alertView = [[UIAlertView alloc] initWithTitle:self.alertView.title message:self.alertView.message
					delegate:self cancelButtonTitle:action.title otherButtonTitles:nil];
				self.alertView.alertViewStyle = currentStyle;
			}
		}

	}
}

- (void)addTextFieldWithConfigurationHandler:(void (^)(UITextField *textField))configurationHandler
{
	if (self.alertController) {
		[self.alertController addTextFieldWithConfigurationHandler:configurationHandler];
	}
	else {
		// Only one text field is applicable with an alert view (except for a password field), so BLAlertController only supports one
		self.alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
		UITextField *textField = [self.alertView textFieldAtIndex:0];
		if (configurationHandler) {
			configurationHandler(textField);
		}
	}
}

- (void)presentInViewController:(UIViewController *)viewController animated:(BOOL)animated completion:(void (^)(void))completion
{
	// This is a bit strange, but we need to retain this object in some manner until the alert goes away, then it can be released
	// AlertViews and AlertControllers are retained by being added as subviews, so we need our wrapper object to also be retained
	self.presentingViewController = viewController;
	objc_setAssociatedObject(viewController, &objcRetainKey, self, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

	if (self.alertController) {
		[viewController presentViewController:self.alertController animated:animated completion:completion];
	}
	else {
		[self.alertView show];
	}
}

#pragma mark - UIAlertViewDelegate methods
- (void)didPresentAlertView:(UIAlertView *)alertView
{
	// For some reason, the keyboard isn't appearing automatically with this alert view. This is supposed to fix it, but isn't
	// Make sure there is a text field in play, if the style isn't default, there should be
	if (alertView.alertViewStyle != UIAlertViewStyleDefault) {
		UITextField *textField = [alertView textFieldAtIndex:0];
		[textField becomeFirstResponder];
	}
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	BLAlertAction *blAlertAction = [self.actions objectAtIndex:buttonIndex];

	if (blAlertAction.handler) {
		blAlertAction.handler(nil);
	}
	objc_setAssociatedObject(self.presentingViewController, &objcRetainKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end

