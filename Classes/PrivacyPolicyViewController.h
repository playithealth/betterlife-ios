//
//  PrivacyPolicyViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/21/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PrivacyPolicyViewControllerDelegate;

@interface PrivacyPolicyViewController : UIViewController

@property (assign, nonatomic) id<PrivacyPolicyViewControllerDelegate> delegate;

@end

@protocol PrivacyPolicyViewControllerDelegate <NSObject>
- (void)privacyPolicyViewDidAccept:(PrivacyPolicyViewController*)controller;
@end
