//
//  RecipeTagsViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/7/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "RecipeTagsViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "BCTextFieldCell.h"
#import "RecipeMO.h"
#import "RecipeTagMO.h"
#import "UIImage+Additions.h"
#import "UIColor+Additions.h"

@interface RecipeTagsViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) UITextField *editingTextField;
@property (strong, nonatomic) NSMutableArray *allTags;
@property (assign, nonatomic) BOOL showAddTagRow;

- (IBAction)addTag:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (void)saveTextFieldValue:(UITextField *)textField;

@end

@implementation RecipeTagsViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

	NSMutableSet *allTagsSet = [NSMutableSet set];
	NSArray *tags = [RecipeTagMO MR_findAllInContext:self.recipe.managedObjectContext];
	for (RecipeTagMO *tag in tags) {
		NSMutableDictionary *tagDict = [NSMutableDictionary dictionary];
		[tagDict setObject:tag.name forKey:@"tagName"];
		[tagDict setObject:tag forKey:@"tagMO"];
		if ([self.recipe.tags containsObject:tag]) {
			[tagDict setObject:[NSNumber numberWithBool:YES] forKey:@"selected"];
		}
		else {
			[tagDict setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
		}
		[allTagsSet addObject:tagDict];
	}

	NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"tagName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
	NSArray *sortedTags = [allTagsSet sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortByName]];
	self.allTags = [sortedTags mutableCopy];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows;
	if (section == 0) {
		numberOfRows = self.showAddTagRow ? 1 : 0;
	} 
	else {
		numberOfRows = [self.allTags count];
	}
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *addTagCellId = @"AddTagCell";
    static NSString *tagCellId = @"TagCell";
    
	UITableViewCell *cell = nil;
	if (indexPath.section == 0) {
		cell = [tableView dequeueReusableCellWithIdentifier:addTagCellId];
	}
	else {
		cell = [tableView dequeueReusableCellWithIdentifier:tagCellId];
	}
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 1) {
		NSMutableDictionary *tagDict = [self.allTags objectAtIndex:indexPath.row];
		BOOL selected = [[tagDict objectForKey:@"selected"] boolValue];
		[tagDict setObject:[NSNumber numberWithBool:!selected] forKey:@"selected"];

		[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];

		[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
	}
}

#pragma mark - Local methods
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 1) {
		NSDictionary *tagDict = [self.allTags objectAtIndex:indexPath.row];

		BOOL selected = [[tagDict objectForKey:@"selected"] boolValue];
		if (selected) {
			cell.imageView.image = [UIImage imageNamed:@"tag"];
		}
		else {
			cell.imageView.image = [UIImage imageNamed:@"white"];
		}

		cell.textLabel.text = [tagDict valueForKey:@"tagName"];
	}
}

- (IBAction)addTag:(id)sender
{
	self.showAddTagRow = YES;
	[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];

	BCTextFieldCell *tfCell = (BCTextFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

	self.editingTextField = tfCell.bcTextField;
	self.editingTextField.enabled = YES;
	self.editingTextField.delegate = self;
	[self.editingTextField becomeFirstResponder];
}

- (IBAction)cancel:(id)sender
{
	if (self.delegate != nil 
			&& [self.delegate conformsToProtocol:@protocol(RecipeTagsViewDelegate)]) {
		[self.delegate recipeTagsView:self changedTags:NO];
	}
}

- (IBAction)done:(id)sender
{
	if (self.editingTextField != nil) {
		[self saveTextFieldValue:self.editingTextField];
		self.editingTextField = nil;
	}

	NSMutableSet *newTags = [NSMutableSet set];
	for (NSDictionary *tagDict in self.allTags) {
		if ([[tagDict objectForKey:@"selected"] boolValue]) {
			[newTags addObject:[tagDict objectForKey:@"tagMO"]];
		}
	}

	BOOL changedTags = NO;
	if (![self.recipe.tags isEqualToSet:newTags]) {
		changedTags = YES;

		self.recipe.tags = newTags;
		[self.recipe setStatus:kStatusPut];

		NSError *error = nil;
		if (![self.recipe.managedObjectContext save:&error]) {
			DDLogError(@"(%@)[%ld] %@", error.domain, (long)error.code, [error localizedDescription]);
		}
	}

	if (self.delegate != nil 
			&& [self.delegate conformsToProtocol:@protocol(RecipeTagsViewDelegate)]) {
		[self.delegate recipeTagsView:self changedTags:changedTags];
	}
}

- (void)saveTextFieldValue:(UITextField *)textField
{
	NSString *tagText = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@"_"];

	if ([tagText length]) {
		RecipeTagMO *newTag = [RecipeTagMO insertInManagedObjectContext:self.recipe.managedObjectContext];
		newTag.name = tagText;
		NSError *error = nil;
		if (![newTag.managedObjectContext save:&error]) {
			DDLogError(@"(%@)[%ld] %@", error.domain, (long)error.code, [error localizedDescription]);
		}

		NSMutableDictionary *tagDict = [NSMutableDictionary dictionary];
		[tagDict setObject:newTag.name forKey:@"tagName"];
		[tagDict setObject:newTag forKey:@"tagMO"];
		[tagDict setObject:[NSNumber numberWithBool:YES] forKey:@"selected"];
		[self.allTags insertObject:tagDict atIndex:0];
	}
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self.editingTextField resignFirstResponder];

	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	if (self.editingTextField != nil) {
		[self saveTextFieldValue:self.editingTextField];
		self.editingTextField = nil;
	}

	self.showAddTagRow = NO;
	[self.tableView reloadData];
}

@end
