//
//  PromotionCell.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 4/11/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "PromotionCell.h"


@implementation PromotionCell
@synthesize discountLabel;
@synthesize nameLabel;
@synthesize descriptionLabel;
@synthesize disclaimerLabel;
@synthesize backgroundImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
