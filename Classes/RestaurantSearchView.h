//
//  RestaurantSearchView.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/27/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCViewController.h"

@protocol RestaurantSearchViewDelegate;

@interface RestaurantSearchView : BCViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *restaurantButton;
@property (strong, nonatomic) IBOutlet UIButton *resetButton;
@property (strong, nonatomic) IBOutlet UIImageView *scopeBackgroundImageView;
@property (unsafe_unretained) id<RestaurantSearchViewDelegate> delegate;

- (IBAction)searchButtonTapped:(id)sender;
- (IBAction)resetButtonTapped:(id)sender;
- (IBAction)restaurantButtonTapped:(id)sender;
- (IBAction)searchTextDidChange:(id)sender;
@end

@protocol RestaurantSearchViewDelegate <NSObject>
- (void)restaurantSearchView:(id)searchView didSelectMenuItem:(id)menuItem;
@end
