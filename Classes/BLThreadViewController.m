//
//  BLThreadViewController.m
//  BuyerCompass
//
//  Created by Greg Goodrich on 9/11/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLThreadViewController.h"

#import "BCImageCache.h"
#import "BCObjectManager.h"
#import "BLCommunitySettingsViewController.h"
#import "CommunityMO.h"
#import "SyncUpdateMO.h"
#import "ThreadMessageMO.h"
#import "ThreadMO.h"
#import "UITableView+DownloadImage.h"
#import "UIViewController+BLSidebarView.h"
#import "User.h"
#import "UserProfileMO.h"

@interface BLThreadViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *messages;
@property (weak, nonatomic) IBOutlet UITextField *sendMessageTextField;
@property (strong, nonatomic) IBOutlet MessageCell *senderCell;
@property (strong, nonatomic) IBOutlet MessageCell *receiverCell;
@property (assign, nonatomic) CGRect initialFrameRect;
@property (strong, nonatomic) UserProfileMO *userProfileMO;
@end

@implementation BLThreadViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:(NSCoder *)aDecoder];
	if (self) {
        // Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self BL_implementSidebar];
    
	self.title = (self.thread.name ?: self.thread.community.name);

	self.userProfileMO = [UserProfileMO getCurrentUserProfileInMOC:self.thread.managedObjectContext];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"CommunitySettings"]
		style:UIBarButtonItemStylePlain target:self action:@selector(settingsTapped:)];   

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
		name:UIKeyboardWillShowNotification object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
		name:UIKeyboardWillHideNotification object:nil];

	[self reloadData];

	self.receiverCell = (MessageCell *)[self.tableView dequeueReusableCellWithIdentifier:@"Receive"];
	self.senderCell = (MessageCell *)[self.tableView dequeueReusableCellWithIdentifier:@"Send"];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:[ThreadMessageMO entityName] object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
	// For some reason, this will only move the view to the second to the last row, then the viewDidAppear moves it the rest of the way
	// I kept this here because the scrolling effect is much less if this moves it down first
	if (([self isMovingToParentViewController] || [self isBeingPresented]) && [self.messages count]) {
		// Scroll to the bottom of the table
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messages count] - 1 inSection:0]
			atScrollPosition:UITableViewScrollPositionBottom animated:NO];
	}

	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	if (([self isMovingToParentViewController] || [self isBeingPresented]) && [self.messages count]) {
		// Scroll to the bottom of the table
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messages count] - 1 inSection:0]
			atScrollPosition:UITableViewScrollPositionBottom animated:YES];
		[self loadImagesForOnscreenRows];
	}

	[super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];

	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - local
- (void)reloadData
{
	NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"createdOn" ascending:YES];
	self.messages = [self.thread.messages sortedArrayUsingDescriptors:@[descriptor]];

	// Update this thread with the updatedOn flag from syncUpdate, as that will reflect the newest info that we have read
	self.thread.readOn = [SyncUpdateMO lastUpdateForTable:kSyncThreadMessages forUser:[User loginId] inContext:self.thread.managedObjectContext];
	self.thread.status = kStatusPut;

	[self.thread.managedObjectContext BL_save];
	[BCObjectManager syncCheck:SendOnly];
}

- (void)refreshView:(NSNotification *)notification
{
	[self reloadData];
	[self.tableView reloadData];
	[self loadImagesForOnscreenRows];
}

- (IBAction)sendButtonPressed:(UIButton *)sender
{
	[self.sendMessageTextField resignFirstResponder];
	NSString *message = [self.sendMessageTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	self.sendMessageTextField.text = @"";
	if ([message length]) {
		// Save this message as a new sent message
		ThreadMessageMO *threadMessageMO = [ThreadMessageMO insertInManagedObjectContext:self.thread.managedObjectContext];
		threadMessageMO.body = message;
		threadMessageMO.senderId = [[User currentUser] loginId];
		threadMessageMO.senderName = [[User currentUser] name];
		// This is being filled out so that the local data can be properly sorted (hopefully), the cloud will ignore it
		threadMessageMO.createdOn = @([[NSDate date] timeIntervalSince1970]);
		threadMessageMO.thread = self.thread;
		threadMessageMO.userProfile = self.userProfileMO;
		threadMessageMO.status = kStatusPost;

		[self.thread.managedObjectContext BL_save];
		self.messages = [self.messages arrayByAddingObject:threadMessageMO];
		[BCObjectManager syncCheck:SendOnly];

		[self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[self.messages count] - 1 inSection:0]]
			withRowAnimation:UITableViewRowAnimationAutomatic];
//		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messages count] - 1 inSection:0]
//			atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];

	for (NSIndexPath *indexPath in visiblePaths) {
		ThreadMessageMO *threadMessageMO = [self.messages objectAtIndex:indexPath.row];
		if ([threadMessageMO.senderId integerValue] != [[[User currentUser] loginId] integerValue]) {
			[self.tableView downloadImageWithLoginId:threadMessageMO.senderId inCommunity:self.thread.community.cloudId
				forIndexPath:indexPath withSize:kImageSizeMedium];
		}
		else if (self.userProfileMO.imageIdValue) {
			[self.tableView downloadImageWithImageId:self.userProfileMO.imageId forIndexPath:indexPath withSize:kImageSizeMedium];
		}
	}
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.messages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	ThreadMessageMO *threadMessageMO = [self.messages objectAtIndex:indexPath.row];

	MessageCell *cell = nil;
	if ([threadMessageMO.senderId integerValue] == [[[User currentUser] loginId] integerValue]) {
		//cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"SendTextView" forIndexPath:indexPath];
		cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"Send" forIndexPath:indexPath];
	}
	else {
		//cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"ReceiveTextView" forIndexPath:indexPath];
		cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"Receive" forIndexPath:indexPath];
	}
	cell.bubbleView.layer.cornerRadius = 8.0f;
    
    // Configure the cell...
	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(MessageCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	ThreadMessageMO *threadMessageMO = [self.messages objectAtIndex:indexPath.row];

	NSString *messageDateString = [NSDateFormatter localizedStringFromDate:
		[NSDate dateWithTimeIntervalSince1970:[threadMessageMO.createdOn doubleValue]]
		dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle];
	UIImage *profileImage = nil;
	if ([threadMessageMO.senderId integerValue] == [[[User currentUser] loginId] integerValue]) {
		cell.dateLabel.text = [NSString stringWithFormat:@"%@", messageDateString];
		profileImage = [[BCImageCache sharedCache] userImageWithImageId:self.userProfileMO.imageId
			withGender:self.userProfileMO.gender withSize:kImageSizeMedium];
	}
	else {
		cell.dateLabel.text = [NSString stringWithFormat:@"%@, %@", threadMessageMO.senderName, messageDateString];
		profileImage = [[BCImageCache sharedCache] userImageWithLoginId:threadMessageMO.senderId forCommunity:self.thread.community.cloudId
			withGender:@(kGenderMale) withSize:kImageSizeMedium];
	}
	
	cell.profileImageView.image = profileImage;


	/*
	if ([NSMutableAttributedString instancesRespondToSelector:@selector(initWithData:options:documentAttributes:error:)]) {
		NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
			initWithData:[threadMessageMO.body dataUsingEncoding:NSUTF8StringEncoding]
			options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
			documentAttributes:nil error:nil];
		[attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15]
			range:NSMakeRange(0, [attributedString length])];
		cell.messageTextView.attributedText = attributedString;
	}
	else {
		// TODO Punt for now on iOS6 when trying to parse out the html and just display it
		cell.messageTextView.text = threadMessageMO.body;
	}

	[cell.messageTextView sizeToFit];
	*/

	cell.messageLabel.text = threadMessageMO.body;
//	cell.messageLabel.attributedText = attributedString;
//	cell.messageTextView.text = threadMessageMO.body;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	ThreadMessageMO *threadMessageMO = [self.messages objectAtIndex:indexPath.row];

	MessageCell *cell = nil;
	if ([threadMessageMO.senderId integerValue] == [[[User currentUser] loginId] integerValue]) {
		cell = self.receiverCell;
	}
	else {
		cell = self.senderCell;
	}

	[self configureCell:cell atIndexPath:indexPath];

    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];

    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));

    [cell setNeedsLayout];
    [cell layoutIfNeeded];

	// Get the actual height required for the cell's contentView
	CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;

	// Add an extra point to the height to account for the cell separator, which is added between the bottom
	// of the cell's contentView and the bottom of the table view cell.
//	height += 18.0f;
	height += 1.0f;

    return height;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillBeShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
 
	/*
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
	*/
 
 	self.initialFrameRect = self.view.frame;
	CGRect aRect = self.view.frame;
	aRect.size.height -= kbSize.height;
	[UIView animateWithDuration:1.0
		animations:^{
			[self.view setFrame:aRect];
		}
		completion:^(BOOL finished){
		}];
 /*
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.tableView scrollRectToVisible:activeField.frame animated:YES];
    }
*/
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
/*
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
*/

	[UIView animateWithDuration:1.0
		animations:^{
			[self.view setFrame:self.initialFrameRect];
		}
		completion:^(BOOL finished){
			[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messages count] - 1 inSection:0]
				atScrollPosition:UITableViewScrollPositionBottom animated:YES];
		}];
}

- (IBAction)settingsTapped:(id)sender
{
	BLCommunitySettingsViewController *settingsView = [self.storyboard instantiateViewControllerWithIdentifier:@"CommunitySettings"];
	settingsView.community = self.thread.community;
	[self.navigationController presentViewController:settingsView animated:YES completion:nil];
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

@end
