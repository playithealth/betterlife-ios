//
//  NSDictionary+NumberValue.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/20/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (BC_NumberValue)
- (NSNumber *)BC_numberForKey:(id)aKey;
- (NSNumber *)BC_numberOrNilForKey:(id)aKey;
- (NSString *)BC_stringOrNilForKey:(id)aKey;
@end
