//
//  BCChoiceTableViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on July 30, 2012.
//  Copyright 2012 BettrLife Corporation. All rights reserved.
//

#import "BCChoiceTableViewController.h"

@interface BCChoiceTableViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, readwrite, nonatomic) NSMutableArray *sections;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
@end

@implementation BCChoiceTableViewController

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.sections = [[NSMutableArray alloc] init];
	}
	return self;
}

#pragma mark - setup
- (void)addSection:(NSDictionary*)section
{
	NSArray *inRows = [section objectForKey:@"rows"];
	NSString *title = [section objectForKey:@"title"];
	NSMutableArray *rows = [[NSMutableArray alloc] initWithCapacity:[inRows count]];
	for (NSDictionary *row in inRows) {
		[rows addObject:[row mutableCopy]];
	}
	NSMutableDictionary *newSection = [NSMutableDictionary dictionaryWithObjectsAndKeys:
		rows, @"rows", title, @"title", nil];
	[self.sections addObject:newSection];
}

#pragma mark - Local
- (NSDictionary *)rowDataAtIndexPath:(NSIndexPath *)indexPath
{
	return [[[self.sections objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.row];
}

- (IBAction)cancel:(id)sender 
{
	[self.delegate BCChoiceView:self didFinish:NO];
}

- (IBAction)done:(id)sender 
{
	[self.delegate BCChoiceView:self didFinish:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView 
numberOfRowsInSection:(NSInteger)section 
{
	return [(NSArray*)[[self.sections objectAtIndex:section] objectForKey:@"rows"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *reuseId = @"BCChoiceTableCellId";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseId];
    }

	NSDictionary *rowData = [self rowDataAtIndexPath:indexPath];
    
	cell.textLabel.text = [rowData valueForKey:@"labelText"];
	if ([rowData objectForKey:@"detailLabelText"] != [NSNull null]) {
		cell.detailTextLabel.text = [rowData valueForKey:@"detailLabelText"];
	}

	if ([[rowData valueForKey:@"selected"] boolValue]) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	else {
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
    
    return cell;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSDictionary *rowData = [self rowDataAtIndexPath:indexPath];

	BOOL selected = [[rowData valueForKey:@"selected"] boolValue];

	[rowData setValue:[NSNumber numberWithBool:!selected] forKey:@"selected"];

	[self.tableView reloadData];

	if ([self.delegate respondsToSelector:@selector(BCChoiceView:didSelectRowAtIndexPath:)]) {
		[self.delegate BCChoiceView:self didSelectRowAtIndexPath:indexPath];
	}
}

@end

