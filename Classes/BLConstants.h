//
//  BLConstants.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 2/04/14.
//  Copyright 2014 BettrLife Corporation. All rights reserved.
//

// Types
extern NSString * const kItemTypeUserFemale;
extern NSString * const kItemTypeUserMale;
extern NSString * const kItemTypeProfileFemale;
extern NSString * const kItemTypeProfileMale;
extern NSUInteger const kGenderMale;
extern NSUInteger const kGenderFemale;

// Stock images
extern NSString * const kStockImageRecipe;
extern NSString * const kStockImageProduct;
extern NSString * const kStockImageMenuItem;
extern NSString * const kStockImageJustCalories;
extern NSString * const kStockImageRecipeLarge;
extern NSString * const kStockImageProductLarge;
extern NSString * const kStockImageMenuItemLarge;
extern NSString * const kStockImageJustCaloriesLarge;
extern NSString * const kStockImageUserFemale;
extern NSString * const kStockImageUserMale;
extern NSString * const kStockImageProfileFemale;
extern NSString * const kStockImageProfileMale;

// Grayscale stock images
extern NSString * const kStockImageRecipeGrayscale;
extern NSString * const kStockImageProductGrayscale;
extern NSString * const kStockImageMenuItemGrayscale;
