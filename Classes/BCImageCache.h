//
//  BCImageCache.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 1/3/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCImageCache : NSCache

+ (BCImageCache *)sharedCache;

#pragma mark - core methods
- (void)setImage:(id)image forKey:key;
- (id)imageForKey:(NSString *)key;

#pragma mark - stock image methods
- (id)stockImageWithItemType:(NSString *)itemType withSize:(NSString *)size;

#pragma mark - key generator methods
+ (NSString *)imageKeyForImageId:(NSNumber *)imageId withSize:(NSString *)size;
+ (NSString *)imageKeyForProductId:(NSNumber *)productId withSize:(NSString *)size;
+ (NSString *)imageKeyForLoginId:(NSNumber *)loginId inCommunity:(NSNumber *)communityId withSize:(NSString *)size;
+ (NSString *)imageKeyForCoachId:(NSNumber *)coachId withSize:(NSString *)size;
+ (NSString *)imageKeyForGrayscaleImageId:(NSNumber *)imageId withSize:(NSString *)size;
+ (NSString *)imageKeyForGrayscaleProductId:(NSNumber *)productId withSize:(NSString *)size;
+ (NSString *)imageKeyForImageURL:(NSString *)imageURLString withSize:(NSString *)size;

#pragma mark - image fetch methods
- (id)imageWithImageId:(NSNumber *)imageId withItemType:(NSString *)itemType withSize:(NSString *)size;
- (id)userImageWithImageId:(NSNumber *)imageId withGender:(NSNumber *)gender withSize:(NSString *)size;
- (id)userImageWithLoginId:(NSNumber *)loginId forCommunity:(NSNumber *)communityId withGender:(NSNumber *)gender withSize:(NSString *)size;
- (void)setUserImage:(UIImage *)image withLoginId:(NSNumber *)loginId forCommunity:(NSNumber *)communityId withSize:(NSString *)size;
- (id)profileImageWithImageId:(NSNumber *)imageId withGender:(NSNumber *)gender withSize:(NSString *)size;

- (id)imageWithImageId:(NSNumber *)imageId;
- (void)setImage:(id)image forImageId:(NSNumber *)imageId;

- (id)imageWithImageId:(NSNumber *)imageId size:(NSString *)size;
- (void)setImage:(id)image forImageId:(NSNumber *)imageId size:(NSString *)size;

- (id)grayscaleImageWithImageId:(NSNumber *)imageId;
- (id)grayscaleImageWithImageId:(NSNumber *)imageId withItemType:(NSString *)itemType;
- (void)setGrayscaleImage:(id)image forImageId:(NSNumber *)imageId;

- (void)removeImageWithImageId:(NSNumber *)imageId size:(NSString *)size;

- (id)imageWithProductId:(NSNumber *)productId;
- (void)setImage:(id)image forProductId:(NSNumber *)productId;

- (id)imageWithProductId:(NSNumber *)productId size:(NSString *)size;
- (void)setImage:(id)image forProductId:(NSNumber *)imageId size:(NSString *)size;

- (id)grayscaleImageWithProductId:(NSNumber *)productId;
- (void)setGrayscaleImage:(id)image forProductId:(NSNumber *)productId;

- (id)imageWithImageURL:(NSString *)imageURL;
- (void)setImage:(id)image forImageURL:(NSString *)imageURL;

- (id)imageWithImageURL:(NSString *)imageURL size:(NSString *)size;
- (void)setImage:(id)image forImageURL:(NSString *)imageURL size:(NSString *)size;

- (id)imageWithCoachId:(NSNumber *)coachId size:(NSString *)size;
- (void)setImage:(id)image forCoachId:(NSNumber *)coachId size:(NSString *)size;

@end
