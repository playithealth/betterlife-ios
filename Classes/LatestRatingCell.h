//
//  LatestRatingCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/6/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StarRatingControl;

@interface LatestRatingCell : UITableViewCell {
    
}

@property (nonatomic, strong) IBOutlet UILabel *categoryLabel;
@property (nonatomic, strong) IBOutlet UILabel *commentLabel;
@property (nonatomic, strong) IBOutlet UILabel *userDateLabel;
@property (nonatomic, strong) StarRatingControl *ratingControl;

@end
