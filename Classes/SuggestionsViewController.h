//
//  SuggestionsViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/2/2011.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCViewController.h"

@interface SuggestionsViewController : BCViewController
@end

