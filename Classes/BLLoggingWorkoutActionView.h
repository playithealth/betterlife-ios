//
//  BLLoggingWorkoutActionView.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 2/11/15.
//  Copyright (c) 2015 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BLLoggingWorkoutActionView;

@protocol BLWorkoutActionViewDelegate <NSObject>
- (void)actionComplete:(BLLoggingWorkoutActionView *)actionView;
@end

@interface BLAnimatingTextLayer : CATextLayer
@property (nonatomic, strong) NSNumber *durationInSeconds;
@property (assign, nonatomic) CGFloat maximumFontSize; ///< Set this if you want the font to shrink to fit. non-animated property
- (void)centerVertically; ///< Call this after the layer has been added to a superlayer, and the font size has been set
@end

IB_DESIGNABLE

@interface BLLoggingWorkoutActionView : UIControl
@property (strong, nonatomic) IBInspectable UIColor *progressColor;
@property (assign, nonatomic) BOOL showCircle;
@property (strong, nonatomic) IBInspectable IBOutlet UIImageView *selectedImageView;
@property (weak, nonatomic) id delegate;
- (void)setLabelText:(NSString *)text;
- (void)startProgressWithDuration:(NSNumber *)seconds;
@property (assign, nonatomic) BOOL completed;
@end
