//
//  GenericValueDisplay.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/23/2010
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "GenericValueDisplay.h"

@implementation NSString (GenericValueDisplay)
- (NSString *)genericValueDisplay {
	return self;
}
@end

@implementation NSNumber (GenericValueDisplay) 
- (NSString *)genericValueDisplay {
    return [self descriptionWithLocale:[NSLocale currentLocale]];
}
@end

@implementation NSDate (GenericValueDisplay) 
- (NSString *)genericValueDisplay {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	NSString *formattedDateString = [dateFormatter stringFromDate:self];
	return formattedDateString;
}
@end

@implementation NSNull (GenericValueDisplay) 
- (NSString *)genericValueDisplay {
    return @"";
}
@end
