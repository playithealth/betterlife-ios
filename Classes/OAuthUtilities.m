//
//  OAuthUtilities.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/25/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "OAuthUtilities.h"

#include <CommonCrypto/CommonHMAC.h>
#include "Base64Transcoder.h"

#import "NSString+BCAdditions.h"
#import "NSURL+AbsoluteNormalizedString.h"
#import "OAuthConsumer.h"
#import "OAuthRequestParameter.h"
#import "User.h"


@implementation OAuthUtilities

+ (NSString *)makeMD5Hash:(NSString*)string 
{
	// create pointer to the string as UTF8
    const char *c_string = [string UTF8String];
	// create byte array of unsigned chars
    unsigned char buffer[CC_MD5_DIGEST_LENGTH];
	// create MD5 hash, store in buffer
    CC_MD5(c_string, (CC_LONG)strlen(c_string), buffer);
	// convert hash to NSString of hex values
	NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < 16; i++) {
        [output appendFormat:@"%02X", buffer[i]];
	}
    return output;
}

+ (NSDictionary *)createOAuthParametersDictionaryWithConsumerKey:(NSString *)consumerKey 
accessToken:(NSString *)accessToken 
{
	NSMutableDictionary *oauthParams = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
		@"1.0", kOAuthVersion, @"HMAC-SHA1", kOAuthSignatureMethod, nil];
	[oauthParams setObject:consumerKey forKey:kOAuthConsumerKey];

	if (accessToken) {
		[oauthParams setObject:accessToken forKey:kOAuthToken];
	}

	// construct a unix time
	NSString *timestamp = [NSString stringWithFormat:@"%0.0f", [[NSDate date] timeIntervalSince1970]];
	[oauthParams setObject:timestamp forKey:kOAuthTimeStamp];

	// generate nOnce as a random, unique string
	NSString* nOnce = [[OAuthUtilities makeMD5Hash:[NSString stringWithFormat:@"%@%i", timestamp, arc4random()]] BC_escapeURIReservedCharacters];
	[oauthParams setObject:nOnce forKey:kOAuthNOnce];

	return oauthParams;
}

+ (void)signRequest:(NSMutableURLRequest*)request withOAuthParameters:(NSDictionary *)oauthParams 
consumerSecret:(NSString *)consumerSecret tokenSecret:(NSString *)tokenSecret 
{
	// concatenate the secrets
	// the token secret may be empty
	NSString *secrets = [NSString stringWithFormat:@"%@&%@", consumerSecret, (tokenSecret ? tokenSecret : @"")];
	DDLogVerbose(@"%@ %@\n\tsecrets: %@", THIS_FILE, THIS_METHOD, secrets);

	// build the initial parameter string
	NSMutableArray *paramArray = [[OAuthRequestParameter parametersFromDictionary:oauthParams] mutableCopy];
	NSArray *queryParams = [OAuthRequestParameter parametersFromString:[[request URL] query]];
	[paramArray addObjectsFromArray:queryParams];
	[paramArray sortUsingSelector:@selector(compare:)];
	DDLogVerbose(@"\tparameters: %@", [paramArray description]);
	NSString *parameterString = [OAuthRequestParameter parameterStringForParameters:paramArray];

	// create the signature
	NSString *signatureBaseString = [OAuthUtilities signatureBaseStringUsingParameterString:parameterString forRequest:request];
	DDLogVerbose(@"\tSignatureBaseString:%@", signatureBaseString);
	NSString *signature = [[OAuthUtilities HMAC_SHA1SignatureForText:signatureBaseString usingSecret:secrets] BC_escapeSpecialCharacters];
	DDLogVerbose(@"\tSignature=%@", signature);

	// make the header
	NSMutableString *oauthString = [NSMutableString string];
	[oauthString appendFormat:@"OAuth realm=\"%@\",", [request URL]];
	[oauthString appendFormat:@"oauth_consumer_key=\"%@\",", [oauthParams valueForKey:kOAuthConsumerKey]];
	if (tokenSecret) {
		[oauthString appendFormat:@"oauth_token=\"%@\",", [oauthParams valueForKey:kOAuthToken]];
	}
	[oauthString appendFormat:@"oauth_nonce=\"%@\",", [oauthParams valueForKey:kOAuthNOnce]];
	[oauthString appendFormat:@"oauth_signature_method=\"%@\",", [oauthParams valueForKey:kOAuthSignatureMethod]];
	[oauthString appendFormat:@"oauth_timestamp=\"%@\",", [oauthParams valueForKey:kOAuthTimeStamp]];
	[oauthString appendFormat:@"oauth_version=\"%@\",", [oauthParams valueForKey:kOAuthVersion]];
	[oauthString appendFormat:@"oauth_signature=\"%@\"", signature];
	DDLogVerbose(@"\tAuthorization=%@", oauthString);

	[request setValue:oauthString forHTTPHeaderField:@"Authorization"];
}

+ (void)signRequest:(NSMutableURLRequest *)request withConsumer:(OAuthConsumer *)consumer andUser:(User *)user
{
	[OAuthUtilities signRequest:request withOAuthParameters:
		[OAuthUtilities createOAuthParametersDictionaryWithConsumerKey:consumer.oauthConsumerKey
				accessToken:user.oauthAccessToken]
			consumerSecret:consumer.oauthConsumerSecret
			tokenSecret:user.oauthSecret];
}

+ (NSString *)signatureBaseStringUsingParameterString:(NSString *)inParameterString forRequest:(NSURLRequest *)inRequest 
{
	return [NSString stringWithFormat:@"%@&%@&%@", 
		[inRequest HTTPMethod],
		[[[[inRequest URL] urlWithoutQuery] absoluteNormalizedString] BC_escapeURIReservedCharacters],
		[inParameterString BC_escapeURIReservedCharacters]];
}

+ (NSString *)HMAC_SHA1SignatureForText:(NSString *)inText usingSecret:(NSString *)inSecret 
{
	NSData *secretData = [inSecret dataUsingEncoding:NSUTF8StringEncoding];
	NSData *textData = [inText dataUsingEncoding:NSUTF8StringEncoding];
	unsigned char result[CC_SHA1_DIGEST_LENGTH];

	CCHmacContext hmacContext;
	bzero(&hmacContext, sizeof(CCHmacContext));
    CCHmacInit(&hmacContext, kCCHmacAlgSHA1, secretData.bytes, secretData.length);
    CCHmacUpdate(&hmacContext, textData.bytes, textData.length);
    CCHmacFinal(&hmacContext, result);
	
	//Base64 Encoding
	char base64Result[32];
	size_t theResultLength = 32;
	Base64EncodeData(result, 20, base64Result, &theResultLength);
	NSData *theData = [NSData dataWithBytes:base64Result length:theResultLength];
	NSString *base64EncodedResult = [[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding];
	
	return base64EncodedResult;
}

+ (NSString *)generateNOnce:(NSString *)prefix
{
	return [OAuthUtilities makeMD5Hash:[NSString stringWithFormat:@"%@%i", prefix, arc4random()]];
}

@end
