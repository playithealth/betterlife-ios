//
//  BLOnboardActivitySelectionViewController.m
//  BuyerCompass
//
//  Created by Greg Goodrich on 3/26/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLOnboardActivitySelectionViewController.h"

#import "BLHeaderFooterMultiline.h"
#import "UIColor+Additions.h"

@interface BLOnboardActivitySelectionViewController ()
@property (strong, nonatomic) NSMutableAttributedString *activityFooterAttributedString;
@property (strong, nonatomic) NSMutableAttributedString *activityHeaderAttributedString;
@end

@implementation BLOnboardActivitySelectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	NSString *headerTitle = nil;
	if (self.isActivityLevel) {
		headerTitle = @"Activity Level";

		// Color the 'BMR' in blue to designate a link
		NSString *headerString = @"Activity Level, along with your basic info (via BMR), is used to calculate your maximum Calories per day\n\n"
			"If you will log your activities by hand, you should select Sedentary/Manual. "
			"If you use a fitness tracking device, you should select Using Fitness Tracking Device. "
			"If you don't plan to log any activities, select the option that matches your typical level of activity.";
		self.activityHeaderAttributedString = [[NSMutableAttributedString alloc] initWithString:headerString];
		[self.activityHeaderAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14]
			range:NSMakeRange(0, [self.activityHeaderAttributedString length])];
		[self.activityHeaderAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor BC_colorWithHex:0x157efb]
			range:[headerString rangeOfString:@"BMR"]];
	}
	else {
		headerTitle = @"Nutrition Effort";
		NSString *footerString = @"How much you want your weight to change per week. Also adjusts up/down your maximum Calories per day.";
		self.activityFooterAttributedString = [[NSMutableAttributedString alloc] initWithString:footerString];
		[self.activityFooterAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14]
			range:NSMakeRange(0, [self.activityFooterAttributedString length])];
	}
	self.navigationItem.title = headerTitle;

	[self.tableView registerClass:[BLHeaderFooterMultiline class] forHeaderFooterViewReuseIdentifier:@"Multiline"];


}

- (void)viewWillDisappear:(BOOL)animated
{
	NSDictionary *selection = [self.items objectAtIndex:self.selectedRow];
	[self.delegate view:self didUpdateObject:selection];

	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Selection" forIndexPath:indexPath];

	NSDictionary *infoDict = [self.items objectAtIndex:[indexPath row]];
	cell.textLabel.text = [infoDict valueForKey:@"title"];
	cell.detailTextLabel.text = [infoDict valueForKey:@"description"];
	cell.accessoryType = (self.selectedRow == [indexPath row] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.selectedRow != [indexPath row]) {
		// Turn off the old selection
		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedRow inSection:0]];
		cell.accessoryType = UITableViewCellAccessoryNone;

		// Turn on the new selection
		cell = [self.tableView cellForRowAtIndexPath:indexPath];
		cell.accessoryType = UITableViewCellAccessoryCheckmark;

		// Update the property
		self.selectedRow = [indexPath row];
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (self.isActivityLevel) {
		return 180;
	}
	else {
		return 22;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	if (self.isActivityLevel) {
		return 0;
	}
	else {
		return 60;
	}
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
	if (self.isActivityLevel) {
		headerView.multilineLabel.attributedText = self.activityHeaderAttributedString;
		if (![headerView.contentView.gestureRecognizers count]) {
			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bmrTapped:)];
			[headerView.contentView addGestureRecognizer:recognizer];
		}
	}

	return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
	if (!self.isActivityLevel) {
		footerView.multilineLabel.attributedText = self.activityFooterAttributedString;
		if (![footerView.contentView.gestureRecognizers count]) {
			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bmrTapped:)];
			[footerView.contentView addGestureRecognizer:recognizer];
		}
	}

	return footerView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - local methods

#pragma mark - responders
- (void)bmrTapped:(UITapGestureRecognizer *)recognizer
{
	UIViewController *bmrView = [self.storyboard instantiateViewControllerWithIdentifier:@"BMR Info Modal"];
	[self presentViewController:bmrView animated:YES completion:nil];
}

@end
