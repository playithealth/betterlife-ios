//
//  CategoryOrderViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 5/2/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UserStoreMO.h"

@interface BCCategoryOrderViewController : UITableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) UserStoreMO *userStore;

@end
