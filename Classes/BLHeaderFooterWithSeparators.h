//
//  BLHeaderFooterWithSeparators.h
//  BettrLife
//
//  Created by Greg Goodrich on 6/27/14.
//  Copyright (c) 2014 BettrLife Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLHeaderFooterWithSeparators : UITableViewHeaderFooterView
@property (strong, nonatomic) UILabel *customLabel;
@property (strong, nonatomic) UIColor *separatorColor;
@property (strong, nonatomic) UIView *topSeparator;
@property (strong, nonatomic) UIView *bottomSeparator;
@end
