//
//  BCSidebarUserInfoCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/16/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BLSidebarCalendarButton;
@class BLSeparatorView;
@protocol BLMyZoneCalendarDelegate;

@interface BCSidebarUserInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryLabel;
@property (weak, nonatomic) IBOutlet id<BLMyZoneCalendarDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *poweredByView;
@property (weak, nonatomic) IBOutlet BLSeparatorView *poweredBySeparator;
@property (weak, nonatomic) IBOutlet UIView *myzoneView;
@property (weak, nonatomic) IBOutlet BLSeparatorView *myzoneSeparator;
@property (weak, nonatomic) IBOutlet UIView *myzoneWeekView;
@property (weak, nonatomic) IBOutlet UIView *myzoneCalendarView;
@property (weak, nonatomic) IBOutlet UILabel *mepsWeekLabel;
@property (weak, nonatomic) IBOutlet UILabel *mepsMonthLabel;
@property (weak, nonatomic) IBOutlet UILabel *mepsYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *calsWeekLabel;
@property (weak, nonatomic) IBOutlet UILabel *calsMonthLabel;
@property (weak, nonatomic) IBOutlet UILabel *calsYearLabel;
@property (weak, nonatomic) IBOutlet BLSidebarCalendarButton *sundayButton;
@property (weak, nonatomic) IBOutlet BLSidebarCalendarButton *mondayButton;
@property (weak, nonatomic) IBOutlet BLSidebarCalendarButton *tuesdayButton;
@property (weak, nonatomic) IBOutlet BLSidebarCalendarButton *wednesdayButton;
@property (weak, nonatomic) IBOutlet BLSidebarCalendarButton *thursdayButton;
@property (weak, nonatomic) IBOutlet BLSidebarCalendarButton *fridayButton;
@property (weak, nonatomic) IBOutlet BLSidebarCalendarButton *saturdayButton;
@property (assign, nonatomic, getter=isCalendarShown) BOOL calendarShown;
@property (strong, nonatomic) IBOutletCollection(BLSidebarCalendarButton) NSArray *calendarDays;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UIButton *previousMonthButton;
@property (weak, nonatomic) IBOutlet UIButton *nextMonthButton;
@property (strong, nonatomic) NSDate *showingMonth;

- (void)useGradientWithTopColor:(UIColor *)topColor bottomColor:(UIColor *)bottomColor;
- (void)hideGradient;
- (void)showPoweredBy:(BOOL)show;
- (void)showMyZone:(BOOL)show;
- (NSDate *)dateForButton:(UIButton *)dayButton;
- (UIButton *)buttonForDate:(NSDate *)targetDate;
@end

@interface BLSeparatorView : UIView
@end

@protocol BLMyZoneCalendarDelegate <NSObject>
@required
- (BOOL)hasDataAtIndex:(NSUInteger)index;
@optional
- (void)didSelectDate:(NSDate *)selectedDate;
- (void)didChangeCalendarStartDate:(NSDate *)newStartDate;
@end
