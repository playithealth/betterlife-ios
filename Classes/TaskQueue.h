//
//  TaskQueue.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 8/1/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^BC_TaskQueueCompletionBlock)(NSString *identifier, NSError *error);

@interface TaskQueueItem : NSObject
@property (nonatomic, strong) id target;
@property (nonatomic, assign) SEL selector;
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, copy) void(^codeBlock)();

- (void)cancel;
@end // TaskQueueItem

@protocol TaskQueueDelegate <NSObject>
@required
- (void)queueDidFinish:(NSString *)identifier withError:(NSError *)error;
@end

@interface TaskQueue : NSObject

@property (nonatomic, unsafe_unretained) id delegate;
@property (nonatomic, copy) void (^handler)(NSString *identifier, NSError *error);
@property (nonatomic, strong) NSMutableArray *queuedRequests;
@property (nonatomic, strong) NSString *identifier;
@property (strong, nonatomic) TaskQueueItem *runningRequest;

/**
 * Note that if you use this method instead of queueWithCompletionHandler,
 * you will have to call [TaskQueueItems registerQueue yourself to get your queue tracked
 */
- (id)initWithIdentifier:(NSString *)queueIdentifier completionBlock:(BC_TaskQueueCompletionBlock)completionBlock;
+ (id)queueNamed:(NSString *)name completionBlock:(BC_TaskQueueCompletionBlock)completion;
- (void)addRequest:(id)target targetSelector:(SEL)targetSelector;
- (void)addRequestWithIdentifier:(NSString *)identifier withBlock:(void(^)())codeBlock;
- (void)runQueue:(NSError *)error;
- (void)finished:(NSError *)error;
- (void)cancelTasks;

@end
