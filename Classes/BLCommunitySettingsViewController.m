//
//  BLCommunitySettingsViewController.m
//  BuyerCompass
//
//  Created by Greg Goodrich on 9/19/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLCommunitySettingsViewController.h"

#import "BCAppDelegate.h"
#import "CommunityMO.h"

@interface BLCommunitySettingsViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *showPictureSwitch;
@property (weak, nonatomic) IBOutlet UIButton *leaveButton;
@property (weak, nonatomic) IBOutlet UINavigationBar *modalNavBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *modalNavigationItem;
@end

@implementation BLCommunitySettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
	//self.modalNavBar. = self.community.name;
	//self.title = self.community.name;
	//self.navigationItem.title = self.community.name;
	self.modalNavigationItem.title = self.community.name;

    self.showPictureSwitch.on = [self.community.usePhoto boolValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - actions
- (IBAction)showPictureChanged:(UISwitch *)sender {
    self.community.usePhoto = @(sender.on);
    self.community.status = kStatusPut;
}

- (IBAction)leaveCommunityPressed:(UIButton *)sender {
    self.community.communityStatus = kCommunityStatusLeft;
    self.community.status = kStatusPut;

	[self.community.managedObjectContext BL_save];

	// Post a notification that we've updated the communities so that the side bar can update itself
	[[NSNotificationCenter defaultCenter] postNotificationName:[CommunityMO entityName]
		object:self userInfo:nil];

	[self dismissViewControllerAnimated:NO completion:^{
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate showSidebar:YES disableClosing:YES];
	}];
}

- (IBAction)doneTapped:(id)sender
{
	[self.community.managedObjectContext BL_save];

	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
