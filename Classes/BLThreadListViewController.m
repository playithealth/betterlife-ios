//
//  BLThreadListViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 9/18/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLThreadListViewController.h"

#import "BLCommunitySettingsViewController.h"
#import "BLThreadViewController.h"
#import "CommunityMO.h"
#import "ThreadMO.h"
#import "UIViewController+BLSidebarView.h"

@interface BLThreadListViewController ()
@property (strong, nonatomic) NSArray *threads;
@end

@implementation BLThreadListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self BL_implementSidebar];
    
	self.title = self.community.name;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"CommunitySettings"]
		style:UIBarButtonItemStylePlain target:self action:@selector(settingsTapped:)];   

	[self reloadData];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:[ThreadMO entityName] object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.threads count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ThreadCell" forIndexPath:indexPath];
    
    // Configure the cell...
	ThreadMO *threadMO = [self.threads objectAtIndex:indexPath.row];
	if (threadMO.name) {
		cell.textLabel.text = threadMO.name;
	}
	else {
		// No thread name, use the community name
		cell.textLabel.text = self.community.name;
	}
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

	if ([segue.identifier isEqualToString:@"Messages"]) {
		BLThreadViewController *viewController = segue.destinationViewController;
		viewController.thread = [self.threads objectAtIndex:indexPath.row];
	}
}

- (IBAction)settingsTapped:(id)sender
{
	BLCommunitySettingsViewController *settingsView = [self.storyboard instantiateViewControllerWithIdentifier:@"CommunitySettings"];
	settingsView.community = self.community;
	[self.navigationController presentViewController:settingsView animated:YES completion:nil];
}

#pragma mark - local
- (void)reloadData
{
	NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
	self.threads = [self.community.threads sortedArrayUsingDescriptors:@[descriptor]];
}

- (void)refreshView:(NSNotification *)notification
{
	[self reloadData];
	[self.tableView reloadData];
}

@end
