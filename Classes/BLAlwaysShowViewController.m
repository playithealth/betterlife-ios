//
//  BLAlwaysShowViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 11/9/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLAlwaysShowViewController.h"

#import "BCObjectManager.h"
#import "NutrientMO.h"
#import "User.h"

@interface BLASNutrientCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nutrientLabel;
@property (weak, nonatomic) IBOutlet UISwitch *nutrientSwitch;
@end
@implementation BLASNutrientCell
@end

@interface BLAlwaysShowViewController ()
@property (strong, nonatomic) NSArray *nutrients;
@property (assign, nonatomic) BOOL changesMade;
@end

@implementation BLAlwaysShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

	[self loadAlwaysShowNutrients];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - local
- (void)loadAlwaysShowNutrients
{
	self.nutrients = [NutrientMO MR_findAllSortedBy:NutrientMOAttributes.sortOrder ascending:YES
		inContext:self.managedObjectContext];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.nutrients count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *nutrientCell = @"NutrientCell";

	NutrientMO *nutrientMO = [self.nutrients objectAtIndex:indexPath.row];
	BLASNutrientCell *cell = [tableView dequeueReusableCellWithIdentifier:nutrientCell];
	cell.nutrientLabel.text = nutrientMO.displayName;

	// See if this nutrient is turned on for this user
	cell.nutrientSwitch.on = [nutrientMO.userProfilesShowing containsObject:[User currentUser].userProfile];
	// Use the tag of the switch to track the row number, so we can quickly find this nutrient
	cell.nutrientSwitch.tag = indexPath.row;
    
    return cell;
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

	// Sync on the unwind segue
	if ([segue.identifier isEqualToString:@"UnwindAlwaysShow"]) {
		if (self.changesMade) {
			// Post a notification that we've updated the user profile
			[[NSNotificationCenter defaultCenter] postNotificationName:[UserProfileMO entityName]
				object:self userInfo:nil];

			[BCObjectManager syncCheck:SendAndReceive];
		}
	}
}

#pragma mark - Responders
- (IBAction)switchValueChanged:(UISwitch *)sender {
	NutrientMO *nutrientMO = [self.nutrients objectAtIndex:sender.tag];

	UserProfileMO *userProfile = [User currentUser].userProfile;
	if ([sender isOn]) {
		[nutrientMO addUserProfilesShowingObject:userProfile];
	}
	else {
		[nutrientMO removeUserProfilesShowingObject:userProfile];
	}

	[userProfile setStatus:kStatusPut];
	[self.managedObjectContext BL_save];

	self.changesMade = YES;
}

@end
