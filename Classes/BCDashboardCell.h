//
//  BCDashboardCell.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 2/27/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BCDrawBlockView;

@interface BCDashboardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet BCDrawBlockView *graphView;

@end
