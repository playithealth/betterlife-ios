//
//  BLNutritionStatusViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 4/16/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BLLoggingDataModel;

@interface BLNutritionStatusViewController : UIViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) BLLoggingDataModel *dataModel;
@end
