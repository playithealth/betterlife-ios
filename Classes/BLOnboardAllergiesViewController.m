//
//  BLOnboardAllergiesViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 3/29/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLOnboardAllergiesViewController.h"

#import "BLHeaderFooterMultiline.h"
#import "BLOnboardWelcomeViewController.h"

@interface BLOnboardAllergiesViewController ()

@end

@implementation BLOnboardAllergiesViewController

static const NSInteger kSectionAllergies = 0;
static const NSInteger kSectionAllergiesFooter = 1;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

	[self.tableView registerClass:[BLHeaderFooterMultiline class] forHeaderFooterViewReuseIdentifier:@"Multiline"];

	self.title = [NSString stringWithFormat:@"Welcome: %ld of %ld",
		(long)[[self.navigationController viewControllers] count] - 1, (long)[self.viewStack count] - 1];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat height = UITableViewAutomaticDimension;
	switch (section) {
		case kSectionAllergies:
			height = 84;
			break;
		case kSectionAllergiesFooter:
			height = 754;
			break;
	}

	return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	CGFloat height = UITableViewAutomaticDimension;
	switch (section) {
		case kSectionAllergies:
			height = 60;
			break;
		case kSectionAllergiesFooter:
			height = 0;
			break;
	}

	return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
	switch (section) {
		case kSectionAllergies:
			headerView.multilineLabel.text = @"All of this information is optional, but the more you enter, "
				"the more we can do to help you manage your health.";
			headerView.normalLabel.text = @"SHOW ALLERGY WARNINGS";
			headerView.headerImageView.image = [UIImage imageNamed:@"alert"];
			break;
		case kSectionAllergiesFooter:
			headerView.multilineLabel.text = @"Custom ingredient warnings can be set in the Account section of this app.\n\n"
				"While we work to ensure that product information is correct, on occasion manufacturers may alter their "
				"ingredient lists. Actual product packaging and materials may contain more and/or different information than "
				"that shown on our Web site. For these reasons you should not rely on the information presented, but should "
				"always read labels, warnings, and directions before using or consuming a product. For additional information "
				"about a product, please contact the manufacturer. Content on this site is for reference purposes and is not "
				"intended to substitute for advice given by a physician, pharmacist, or other licensed health-care professional. "
				"You should not use this information as self-diagnosis or for treating a health problem or disease. "
				"Contact your health-care provider immediately if you suspect that you have a medical problem. "
				"Information and statements regarding dietary supplements have not been evaluated by the Food and Drug "
				"Administration and are not intended to diagnose, treat, cure, or prevent any disease or health condition. "
				"Neither BettrLife Corporation, its Content provider(s), nor product manufacturers assume any liability for "
				"inaccuracies, misstatements, or omissions. BETTRLIFE CORPORATION PROVIDES NO WARRANTY, EXPRESS OR IMPLIED, "
				"REGARDING THE INFORMATION PROVIDED. INDIVIDUALS WHO HAVE ALLERGIES OR OTHER REACTIONS TO FOODS OR OTHER "
				"SUBSTANCES DESCRIBED ON THE WEBSITE SHOULD NOT RELY ON THE INFORMATION PROVIDED AS BEING ACCURATE OR COMPLETE "
				"AND SHOULD CONDUCT THEIR OWN INVESTIGATION REGARDING THE INFORMATION PRESENTED.";
			break;
	}

	return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *footerView = nil;
	switch (section) {
		case kSectionAllergies:
			footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
			footerView.multilineLabel.text = @"A warning icon will be shown for selected allergies during "
				"product searches and in product info.";
			break;
		case kSectionAllergiesFooter:
			break;
	}
    
    return footerView;
}

#pragma mark - responders
- (IBAction)nextViewController:(id)sender {
	if (self.delegate) {
		[self.delegate nextView];
	}
}

@end
