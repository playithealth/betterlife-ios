//
//  BCLoggingSearchSmartViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/9/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCDetailViewDelegate.h"

@interface BCLoggingSearchSmartViewController : UITableViewController <BCDetailViewDelegate>
@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic, readonly) NSArray *loggedFoods;
@property (strong, nonatomic, readonly) NSArray *mealPredictions;
@property (strong, nonatomic) NSDate *logDate;
@property (assign, nonatomic) NSNumber *logTime;
@end
