//
//  BLThreadListViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 9/18/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CommunityMO;

@interface BLThreadListViewController : UITableViewController
@property (strong, nonatomic) CommunityMO *community;
@end
