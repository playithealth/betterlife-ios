//
//  UNUSED!?
//  MealsSearchViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/4/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import "MealsSearchViewController.h"

#import "BCAppDelegate.h"
#import "BCDetailViewDelegate.h"
#import "BCImageCache.h"
#import "BCTableViewController.h"
#import "BCTableViewCell.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"
#import "HTTPQueue.h"
#import "RecipeMO.h"
#import "MealPlannerDay.h"
#import "MealPlannerDetailViewController.h"
#import "NSString+BCAdditions.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "UITableView+DownloadImage.h"
#import "User.h"

static const NSInteger kSectionAddCustomRecipe = 0;
static const NSInteger kSectionLocalRecipes = 1;
static const NSInteger kSectionPublicRecipes = 2;
static const NSInteger kSectionProducts = 3;
static const NSInteger kSectionMenuItems = 4;
static const NSInteger kSectionWebRecipes = 5;

static const NSInteger kInitialRowsPerSection = 3;
static const NSInteger kSearchPageSize = 10;

static const CGFloat kSectionHeaderHeight = 22;

@interface MealsSearchViewController () <BCDetailViewDelegate>

@property (strong, nonatomic) NSArray *localRecipes;
@property (strong, nonatomic) NSMutableArray *publicRecipes;
@property (strong, nonatomic) NSMutableArray *webRecipes;
@property (strong, nonatomic) NSMutableArray *products;
@property (strong, nonatomic) NSMutableArray *menuItems;
@property (strong, nonatomic) NSArray *extraRecipes;
@property (assign, nonatomic) NSInteger morePage;
@property (assign, nonatomic) BOOL moreLocalRecipes;
@property (assign, nonatomic) BOOL morePublicRecipes;
@property (assign, nonatomic) BOOL moreWebRecipes;
@property (assign, nonatomic) BOOL moreProducts;
@property (assign, nonatomic) BOOL moreMenuItems;
@property (strong, nonatomic) NSString *searchTerm;
@property (strong, nonatomic) NSManagedObjectContext *scratchContext;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end

@implementation MealsSearchViewController

#pragma mark - view lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.moreLocalRecipes = NO;
		self.morePublicRecipes = NO;
		self.moreWebRecipes = NO;
		self.scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	UIToolbar *aToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
	aToolbar.barStyle = UIBarStyleBlack;
	aToolbar.translucent = YES;

	UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
		target:nil action:nil];

	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemDone
		target:self action:@selector(resignKeyboard:)];

	[aToolbar setItems:@[flexibleSpace, doneButton] animated:NO];

	self.searchTextField.inputAccessoryView = aToolbar;

    [self.searchTextField becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:YES animated:NO];

	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	[self fetchInitialResults];
	[self reloadData];

	[super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    // terminate all pending download connections
	[self.tableView cancelAllImageTrackers];
	[self.tableView cancelAllImageFetchers];

	[super viewDidDisappear:animated];
}

- (void)viewDidUnload 
{
	[self setBackLabel:nil];
	[self setBackButton:nil];
	[super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

	// terminate all pending download connections
	[self.tableView cancelAllImageFetchers];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    User *thisUser = [User currentUser];

	if ([segue.identifier isEqualToString:@"ShowUserRecipeDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.scratchContext];
		tempMPD.accountId = thisUser.accountId;
		tempMPD.date = [NSDate date];
		tempMPD.recipe = [self.localRecipes objectAtIndex:[indexPath row]];

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;

		[self.scratchContext BL_save];
	}
	else if ([segue.identifier isEqualToString:@"ShowPublicRecipeDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		NSDictionary *publicRecipe = [self.publicRecipes objectAtIndex:[indexPath row]];

		RecipeMO *tempMeal = [RecipeMO insertInManagedObjectContext:self.scratchContext];
		tempMeal.accountId = thisUser.accountId;
		tempMeal.cloudId = [[publicRecipe objectForKey:kRecipeId] numberValueDecimal];
		[tempMeal setWithRecipeDictionary:publicRecipe];

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.scratchContext];
		tempMPD.accountId = thisUser.accountId;
		tempMPD.date = [NSDate date];
		tempMPD.recipe = tempMeal;

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;

		[self.scratchContext BL_save];
	}
	else if ([segue.identifier isEqualToString:@"ShowWebRecipeDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		NSDictionary *webRecipe = [self.webRecipes objectAtIndex:[indexPath row]];

		RecipeMO *tempMeal = [RecipeMO insertInManagedObjectContext:self.scratchContext];
		tempMeal.accountId = thisUser.accountId;
		tempMeal.externalId = [webRecipe objectForKey:kId];
		tempMeal.name = [webRecipe objectForKey:kName];
		tempMeal.calories = [[webRecipe objectForKey:kNutritionCalories] numberValueDecimal];
		if ((NSNull *)[webRecipe objectForKey:kRecipeImageURL] != [NSNull null]) {
			tempMeal.imageUrl = [webRecipe objectForKey:kRecipeImageURL];
		}

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.scratchContext];
		tempMPD.accountId = thisUser.accountId;
		tempMPD.date = [NSDate date];
		tempMPD.recipe = tempMeal;

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;

		[self.scratchContext BL_save];
	}
	else if ([segue.identifier isEqualToString:@"AddCustomRecipe"]) {
		RecipeMO *tempMeal = [RecipeMO insertInManagedObjectContext:self.scratchContext];
		tempMeal.accountId = thisUser.accountId;
		tempMeal.name = self.searchTextField.text;

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.scratchContext];
		tempMPD.accountId = thisUser.accountId;
		tempMPD.date = [NSDate date];
		tempMPD.recipe = tempMeal;

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;

		[self.scratchContext BL_save];
	}
	else if ([segue.identifier isEqualToString:@"ShowProductDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		id product = [self.products objectAtIndex:indexPath.row];

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.scratchContext];
		tempMPD.accountId = [[User currentUser] accountId];
		tempMPD.date = [NSDate date];
		tempMPD.productId = [[product objectForKey:kId] numberValueDecimal];
		tempMPD.name = [product objectForKey:kName];
		tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:product inContext:self.scratchContext];

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;

		[self.scratchContext BL_save];
	}
	else if ([segue.identifier isEqualToString:@"ShowMenuItemDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		id menuItem = [self.menuItems objectAtIndex:indexPath.row];

		MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.scratchContext];
		tempMPD.accountId = [[User currentUser] accountId];
		tempMPD.date = [NSDate date];
		tempMPD.menuId = [[menuItem objectForKey:kId] numberValueDecimal];
		tempMPD.name = [menuItem objectForKey:kName];
		tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:menuItem inContext:self.scratchContext];

		MealPlannerDetailViewController *detailView = segue.destinationViewController;
		detailView.mpd = tempMPD;
		detailView.delegate = self;

		[self.scratchContext BL_save];
	}
}

#pragma mark - local methods
- (void)reloadData
{
	// Ensure that the image trackers don't try to update indexPaths until after we refresh
	[self.tableView clearTrackerIndexPaths];
	[self.tableView reloadData];
}

- (void)sendSearchRequests
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	HTTPQueue *searchQueue = [HTTPQueue queueNamed:@"MealSearch" completionBlock:
		^(NSString *identifier, NSError *error) {
			[self reloadData];
			[self loadImagesForOnscreenRows];

			[appDelegate setNetworkActivityIndicatorVisible:NO];
		}];

	NSURL *googleSearchURL = [BCUrlFactory googleRecipeSearchForSearchTerm:self.searchTerm page:1 includeDetails:NO];
	GTMHTTPFetcher *googleSearchFetcher = [GTMHTTPFetcher signedFetcherWithURL:googleSearchURL];
	[searchQueue addRequest:googleSearchFetcher completionBlock:
		^(NSData *retrievedData, NSError *error) {
			if (error) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:googleSearchFetcher retrievedData:retrievedData];
			}
			else {
				NSArray *googleRecipes = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				// NOTE this API actually returns up to 10 results, so this is kind of hacky to just truncate the results here
				// but it's the best option I could come up with -- see the sendMore for a continued hack
				NSInteger resultsCount = [googleRecipes count];
				if (resultsCount > kInitialRowsPerSection) {
					self.webRecipes = [[googleRecipes subarrayWithRange:NSMakeRange(0, kInitialRowsPerSection)] mutableCopy];
					self.extraRecipes = [googleRecipes subarrayWithRange:NSMakeRange(kInitialRowsPerSection, resultsCount - kInitialRowsPerSection)];
					self.moreWebRecipes = YES;
				}
				else {
					self.moreWebRecipes = NO;
				}
			}

			[self.searchTextField resignFirstResponder];
		}];

	NSURL *bcSearchURL = [BCUrlFactory searchUrlForSearchTerm:[self.searchTextField text] searchSections:@[@"products",@"menuitems",@"publicrecipes"] showRows:kInitialRowsPerSection showOnlyFood:YES];
	GTMHTTPFetcher *bcSearchFetcher = [GTMHTTPFetcher signedFetcherWithURL:bcSearchURL];
	[searchQueue addRequest:bcSearchFetcher completionBlock:
		^(NSData *retrievedData, NSError *error) {
			if (error) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:bcSearchFetcher retrievedData:retrievedData];
			}
			else {
				NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				NSDictionary *recipeDict = [resultsDict objectForKey:@"publicrecipes"];
				self.publicRecipes = [[recipeDict objectForKey:@"results"] mutableCopy];
				self.morePublicRecipes = [[recipeDict objectForKey:@"more"] boolValue];

				NSDictionary *productsDict = [resultsDict objectForKey:@"products"];
				self.products = [[productsDict objectForKey:@"results"] mutableCopy];
				self.moreProducts = [[productsDict objectForKey:@"more"] boolValue];

				NSDictionary *menuItemsDict = [resultsDict objectForKey:@"menuitems"];
				self.menuItems = [[menuItemsDict objectForKey:@"results"] mutableCopy];
				self.moreMenuItems = [[menuItemsDict objectForKey:@"more"] boolValue];
			}

			[self.searchTextField resignFirstResponder];
		}];

	[searchQueue runQueue:nil];
}

- (NSArray *)fetchLocalRecipes:(BOOL)loadMore filter:(BOOL)filter
{    
    User *thisUser = [User currentUser];

    NSFetchRequest *fetchRequest = nil;
	if (filter) {
		fetchRequest = [RecipeMO MR_requestAllSortedBy:RecipeMOAttributes.name ascending:YES
			withPredicate:[NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@ AND status <> %@ AND accountId = %@", 
			RecipeMOAttributes.name, self.searchTerm, kStatusDelete, thisUser.accountId] inContext:self.managedObjectContext];
	}
	else {
		fetchRequest = [RecipeMO MR_requestAllSortedBy:RecipeMOAttributes.name ascending:YES
			withPredicate:[NSPredicate predicateWithFormat:@"status <> %@ AND accountId = %@", kStatusDelete, thisUser.accountId]
			inContext:self.managedObjectContext];
	}

	NSInteger totalCount = [self.scratchContext countForFetchRequest:fetchRequest error:nil];

	if (loadMore) {
		self.morePage += 1;
	}

	NSInteger displayCount = MIN(totalCount, self.morePage * kSearchPageSize + kInitialRowsPerSection);
	fetchRequest.fetchLimit = displayCount;

	self.moreLocalRecipes = (displayCount < totalCount);

	return [self.scratchContext executeFetchRequest:fetchRequest error:nil];
}

- (void)fetchLocalResults 
{
	self.localRecipes = [self fetchLocalRecipes:NO filter:YES];

	[self reloadData];
	[self loadImagesForOnscreenRows];
}

- (void)fetchInitialResults
{
	self.localRecipes = [self fetchLocalRecipes:NO filter:NO];

	[self reloadData];
	[self loadImagesForOnscreenRows];
}

- (void)sendMorePublicRecipesRequest 
{ 
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	if (!self.morePage) {
		[self showBackView:YES withLabelText:@"All results for BettrLife Recipes"];
		[self resetResultArraysExcluding:self.publicRecipes];
		self.morePublicRecipes = YES;
		[self reloadData];
	}
	self.morePage += 1;
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.publicRecipes count] inSection:kSectionPublicRecipes];
	BCTableViewCell *cell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[cell.spinner startAnimating];
	cell.bcTextLabel.text = @"Loading...";

	NSURL *bcSearchURL = [BCUrlFactory publicRecipesForSearchTerm:self.searchTerm showRows:kSearchPageSize + 1 
		rowOffset:self.morePage * kSearchPageSize + kInitialRowsPerSection];
	GTMHTTPFetcher *bcSearchFetcher = [GTMHTTPFetcher signedFetcherWithURL:bcSearchURL];
    [bcSearchFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:bcSearchFetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *recipesArray = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			self.morePublicRecipes = ([recipesArray count] > kSearchPageSize);
			[self insertRowsFromArray:recipesArray toArray:self.publicRecipes inSection:kSectionPublicRecipes more:self.morePublicRecipes];
		}

		[appDelegate setNetworkActivityIndicatorVisible:NO];
		[cell.spinner stopAnimating];
		cell.bcTextLabel.text = @"Load more results...";
	}];
}

- (void)sendMoreWebRecipesRequest 
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	if (!self.morePage) {
		[self showBackView:YES withLabelText:@"All results for Web Recipes"];
		[self resetResultArraysExcluding:self.webRecipes];
		[self reloadData];
	}
	self.morePage += 1;
	
	if (self.morePage == 1 && self.extraRecipes) {
		// always assume there are more
		self.moreWebRecipes = YES;
		// start by inserting the rows left over from the first request
		[self.webRecipes addObjectsFromArray:self.extraRecipes];
		self.extraRecipes = nil;
		[self reloadData];
	}
	else {
		NSIndexPath *moreIndexPath = [NSIndexPath indexPathForRow:[self.webRecipes count] inSection:kSectionWebRecipes];
		BCTableViewCell *moreCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:moreIndexPath];
		[moreCell.spinner startAnimating];
		moreCell.bcTextLabel.text = @"Loading...";

		NSURL *googleSearchURL = [BCUrlFactory googleRecipeSearchForSearchTerm:self.searchTerm page:self.morePage + 1 includeDetails:NO];

		GTMHTTPFetcher *googleSearchFetcher = [GTMHTTPFetcher signedFetcherWithURL:googleSearchURL];
		[googleSearchFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
			if (error) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:googleSearchFetcher retrievedData:retrievedData];
			}
			else {
				NSArray *googleRecipes = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

				if ([googleRecipes count] > 0) {
					// for the google search, assume that there are more unless we received no results
					self.moreWebRecipes = YES;
					[self insertRowsFromArray:googleRecipes toArray:self.webRecipes inSection:kSectionWebRecipes more:self.moreWebRecipes];
				}
				else {
					self.moreWebRecipes = NO;
					[self.tableView beginUpdates];
					[self.tableView deleteRowsAtIndexPaths:@[moreIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
					[self.tableView endUpdates];
				}
			}

			[appDelegate setNetworkActivityIndicatorVisible:NO];
			[moreCell.spinner stopAnimating];
			moreCell.bcTextLabel.text = @"Load more results...";
		}];
	}
}

- (void)sendMoreProductsRequest
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	if (!self.morePage) {
		[self showBackView:YES withLabelText:@"All results for Products"];
		[self resetResultArraysExcluding:self.products];
		// default this to YES so that the load more row will show while it's loading
		self.moreProducts = YES;
		[self reloadData];
	}
	self.morePage += 1;
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.products count] inSection:kSectionProducts];
	BCTableViewCell *cell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[cell.spinner startAnimating];
	cell.bcTextLabel.text = @"Loading...";

	NSURL *searchURL = [BCUrlFactory productSearchURLForSearchTerm:self.searchTerm
		showRows:kSearchPageSize + 1 rowOffset:self.morePage * kSearchPageSize + kInitialRowsPerSection filters:nil showOnlyFood:YES];
    
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSDictionary *searchResultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			NSArray *productsArray = [searchResultsDict objectForKey:kProductResults];
			self.moreProducts = ([productsArray count] > kSearchPageSize);
			[self insertRowsFromArray:productsArray toArray:self.products inSection:kSectionProducts more:self.moreProducts];
		}

		[self loadImagesForOnscreenRows];

		[appDelegate setNetworkActivityIndicatorVisible:NO];
		[cell.spinner stopAnimating];
		cell.bcTextLabel.text = @"Load more results...";
	}];
}

- (void)sendMoreMenuItemsRequest
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	if (!self.morePage) {
		[self showBackView:YES withLabelText:@"All results for Menu Items"];
		[self resetResultArraysExcluding:self.menuItems];
		// default this to YES so that the load more row will show while it's loading
		self.moreMenuItems = YES;
		[self reloadData];
	}
	self.morePage += 1;
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.menuItems count] inSection:kSectionMenuItems];
	BCTableViewCell *cell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[cell.spinner startAnimating];
	cell.bcTextLabel.text = @"Loading...";

	NSURL *searchURL = [BCUrlFactory menuItemsSearchURLForSearchTerm:self.searchTerm
		limit:kSearchPageSize + 1 offset:self.morePage * kSearchPageSize + kInitialRowsPerSection];
    
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSArray *returnedMenuItems = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			self.moreMenuItems = ([returnedMenuItems count] > kSearchPageSize);
			[self insertRowsFromArray:returnedMenuItems toArray:self.menuItems inSection:kSectionMenuItems more:self.moreMenuItems];
		}

		[self loadImagesForOnscreenRows];

		[appDelegate setNetworkActivityIndicatorVisible:NO];
		[cell.spinner stopAnimating];
		cell.bcTextLabel.text = @"Load more results...";
	}];
}

- (void)resetResultArraysExcluding:(NSArray *)exclude
{
	if (exclude != self.localRecipes) {
		self.localRecipes = nil;
	}
	if (exclude != self.publicRecipes) {
		self.publicRecipes = [NSMutableArray array];
	}
	if (exclude != self.webRecipes) {
		self.webRecipes = [NSMutableArray array];
	}
	if (exclude != self.products) {
		self.products = [NSMutableArray array];
	}
	if (exclude != self.menuItems) {
		self.menuItems = [NSMutableArray array];
	}

	self.morePage = 0;

	self.moreLocalRecipes = NO;
	self.morePublicRecipes = NO;
	self.moreWebRecipes = NO;
	self.moreProducts = NO;
	self.moreMenuItems = NO;
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
	for (NSIndexPath *indexPath in visiblePaths) {
		if (indexPath.section == kSectionLocalRecipes) {
			if (indexPath.row < [self.localRecipes count]) {
				RecipeMO *recipe = [self.localRecipes objectAtIndex:indexPath.row];

				if ([recipe.imageId integerValue]) {
					[self.tableView downloadImageWithImageId:recipe.imageId forIndexPath:indexPath withSize:kImageSizeMedium];
				}
				else if ([recipe.imageUrl length]) {
					UIImage *recipeImage = [[BCImageCache sharedCache] imageWithImageURL:recipe.imageUrl];
					if (!recipeImage) {
						[self.tableView downloadImageWithImageURL:recipe.imageUrl forIndexPath:indexPath completionBlock:
							^(NSError *error, UIImage *image) {
								if (image) {
									BCTableViewCell *bcCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
									bcCell.bcImageView.image = image;
								}
							}];
					}
				}
			}
		}
		else if (indexPath.section == kSectionPublicRecipes) {
			if (indexPath.row < [self.publicRecipes count]) {
				NSDictionary *recipeDict = [self.publicRecipes objectAtIndex:[indexPath row]];

				NSNumber *imageId = [[recipeDict objectForKey:kImageId] numberValueDecimal];
				NSString *imageUrl = [recipeDict objectForKey:kRecipeImageURL];

				if ([imageId integerValue]) {
					[self.tableView downloadImageWithImageId:imageId forIndexPath:indexPath withSize:kImageSizeMedium];
				}
				else if ([imageUrl length]) {
					UIImage *recipeImage = [[BCImageCache sharedCache] imageWithImageURL:imageUrl];
					if (!recipeImage) {
						[self.tableView downloadImageWithImageURL:imageUrl forIndexPath:indexPath completionBlock:
							^(NSError *error, UIImage *image) {
								if (image) {
									BCTableViewCell *bcCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
									bcCell.bcImageView.image = image;
								}
							}];
					}
				}
			}
		}
		else if (indexPath.section == kSectionWebRecipes) {
			if (indexPath.row < [self.webRecipes count]) {
				NSDictionary *recipeDict = [self.webRecipes objectAtIndex:[indexPath row]];

				NSString *imageUrl = [recipeDict objectForKey:kWebRecipeImageURL];

				if ((NSNull *)imageUrl != [NSNull null] && [imageUrl length]) {
					UIImage *recipeImage = [[BCImageCache sharedCache] imageWithImageURL:imageUrl];
					if (!recipeImage) {
						[self.tableView downloadImageWithImageURL:imageUrl forIndexPath:indexPath completionBlock:
							^(NSError *error, UIImage *image) {
								if (image) {
									BCTableViewCell *bcCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
									bcCell.bcImageView.image = image;
								}
							}];
					}
				}
			}
		}
	}
}

- (void)resignKeyboard:(id)sender
{
	[self.searchTextField resignFirstResponder];
}

- (IBAction)backButtonTapped:(id)sender
{
	[self showBackView:NO withLabelText:nil];

	self.searchTextField.text = self.searchTerm;
	
	[self resetResultArraysExcluding:nil];

	self.localRecipes = [self fetchLocalRecipes:NO filter:YES];
	[self reloadData];
	[self sendSearchRequests];
}

- (void)showBackView:(BOOL)show withLabelText:(NSString *)labelText
{
	self.searchTextField.text = nil;

	self.backButton.hidden = !show;
	self.backLabel.hidden = !show;

	self.backLabel.text = labelText;

	self.headerImageView.hidden = show;
	self.cancelButton.hidden = show;
	self.searchTextField.hidden = show;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self resetResultArraysExcluding:nil];
	[self reloadData];
    
	if ([self.searchTextField.text length] > 0) {
		self.searchTerm = self.searchTextField.text;
		[self.searchTextField resignFirstResponder];

		self.localRecipes = [self fetchLocalRecipes:NO filter:YES];
		[self reloadData];
		[self sendSearchRequests];
    }

    return YES;
}

#pragma mark - button responders
- (void)quickAddLocalRecipeButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
	id recipe = [self.localRecipes objectAtIndex:indexPath.row];

	if (self.delegate != nil
			&& [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:recipe];
	}
}

- (void)quickAddPublicRecipeButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
	NSDictionary *publicRecipe = [self.publicRecipes objectAtIndex:indexPath.row];

	RecipeMO *recipe = [RecipeMO insertInManagedObjectContext:self.scratchContext];
	recipe.accountId = [[User currentUser] accountId];
	[recipe setWithRecipeDictionary:publicRecipe];


	if (self.delegate != nil
			&& [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:recipe];
	}
}

- (void)quickAddWebRecipeButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
	id recipe = [self.webRecipes objectAtIndex:indexPath.row];
    
	if (self.delegate != nil
			&& [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:recipe];
	}
}

- (void)quickAddProductButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
	
	id product = [self.products objectAtIndex:indexPath.row];

	MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.scratchContext];
	tempMPD.accountId = [[User currentUser] accountId];
	tempMPD.date = [NSDate date];
	tempMPD.productId = [[product objectForKey:kId] numberValueDecimal];
	tempMPD.name = [product objectForKey:kName];
	tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:product inContext:self.scratchContext];

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:tempMPD];
	}
}

- (void)quickAddMenuItemButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
	
	id menuItem = [self.menuItems objectAtIndex:indexPath.row];

	MealPlannerDay *tempMPD = [MealPlannerDay insertInManagedObjectContext:self.scratchContext];
	tempMPD.accountId = [[User currentUser] accountId];
	tempMPD.date = [NSDate date];
	tempMPD.menuId = [[menuItem objectForKey:kId] numberValueDecimal];
	tempMPD.name = [menuItem objectForKey:kName];
	tempMPD.nutrition = [NutritionMO insertNutritionWithDictionary:menuItem inContext:self.scratchContext];

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:tempMPD];
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numberOfSections = 6;
        
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    
    if (section == kSectionAddCustomRecipe) {
        if ([self.searchTextField.text length] > 0) {
            numberOfRows = 1;
        }
    }
    else if (section == kSectionLocalRecipes) {
        numberOfRows = [self.localRecipes count];
		if (self.moreLocalRecipes) {
			numberOfRows++;
		}
    }
    else if (section == kSectionPublicRecipes) {
        numberOfRows = [self.publicRecipes count];
		if (self.morePublicRecipes) {
			numberOfRows++;
		}
    }
    else if (section == kSectionWebRecipes) {
        numberOfRows = [self.webRecipes count];
		if (self.moreWebRecipes) {
			numberOfRows++;
		}
    }
    else if (section == kSectionProducts) {
        numberOfRows = [self.products count];
		if (self.moreProducts) {
			numberOfRows++;
		}
    }
    else if (section == kSectionMenuItems) {
        numberOfRows = [self.menuItems count];
		if (self.moreMenuItems) {
			numberOfRows++;
		}
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *addCustomCellId = @"AddCustomCell";
    static NSString *userRecipeCellId = @"UserRecipeCell";
    static NSString *publicRecipeCellId = @"PublicRecipeCell";
    static NSString *webRecipeCellId = @"WebRecipeCell";
    static NSString *menuItemCellId = @"MenuItemCell";
    static NSString *productCellId = @"ProductCell";
    static NSString *showMoreCellId = @"ShowMoreCell";

    UITableViewCell *cell = nil;
    if (indexPath.section == kSectionAddCustomRecipe) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:addCustomCellId];
    }
	else if (indexPath.section == kSectionLocalRecipes) {
		if (indexPath.row < [self.localRecipes count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:userRecipeCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddLocalRecipeButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }
	else if (indexPath.section == kSectionPublicRecipes) {
		if (indexPath.row < [self.publicRecipes count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:publicRecipeCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddPublicRecipeButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
	}
	else if (indexPath.section == kSectionWebRecipes) {
		if (indexPath.row < [self.webRecipes count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:webRecipeCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddWebRecipeButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
	}
    else if (indexPath.section == kSectionProducts) {
		if (indexPath.row < [self.products count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:productCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddProductButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }
    else if (indexPath.section == kSectionMenuItems) {
		if (indexPath.row < [self.menuItems count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:menuItemCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddMenuItemButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }

	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionAddCustomRecipe) {
        cell.textLabel.text = [NSString stringWithFormat:@"Add “%@” as a custom recipe", self.searchTextField.text];
    }
    else if (indexPath.section == kSectionLocalRecipes) {
		if (indexPath.row < [self.localRecipes count]) {
			RecipeMO *recipe = [self.localRecipes objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = recipe.name;

			UIImage *recipeImage = nil;
			if ([recipe.imageId integerValue]) {
				recipeImage = [[BCImageCache sharedCache] imageWithImageId:recipe.imageId size:kImageSizeMedium];
			}
			if (!recipeImage && [recipe.imageUrl length]) {
				recipeImage = [[BCImageCache sharedCache] imageWithImageURL:recipe.imageUrl size:kImageSizeMedium];
			}
			if (!recipeImage) {
				recipeImage = [UIImage imageNamed:kStockImageRecipe];
			}
			bcCell.bcImageView.image = recipeImage;
		}
	}
	else if (indexPath.section == kSectionPublicRecipes) {
		if (indexPath.row < [self.publicRecipes count]) {
			NSDictionary *recipeDict = [self.publicRecipes objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = [recipeDict objectForKey:kName];

			UIImage *recipeImage = nil;
			if ((NSNull *)[recipeDict objectForKey:kImageId] != [NSNull null]
					&& [[recipeDict objectForKey:kImageId] integerValue]) {
				recipeImage = [[BCImageCache sharedCache] imageWithImageId:[recipeDict objectForKey:kImageId] size:kImageSizeMedium];
			}
			if (!recipeImage && [[recipeDict objectForKey:kRecipeImageURL] length]) {
				recipeImage = [[BCImageCache sharedCache] imageWithImageURL:[recipeDict objectForKey:kRecipeImageURL] size:kImageSizeMedium];
			}
			if (!recipeImage) {
				recipeImage = [UIImage imageNamed:kStockImageRecipe];
			}
			bcCell.bcImageView.image = recipeImage;
		}
	}
	else if (indexPath.section == kSectionWebRecipes) {
		if (indexPath.row < [self.webRecipes count]) {
			NSDictionary *recipeDict = [self.webRecipes objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = [recipeDict objectForKey:kName];

			NSString *imageUrl = [recipeDict objectForKey:kWebRecipeImageURL];

			UIImage *recipeImage = nil;
			if ((NSNull *)imageUrl != [NSNull null] && [imageUrl length]) {
				recipeImage = [[BCImageCache sharedCache] imageWithImageURL:imageUrl size:kImageSizeMedium];
			}
			if (!recipeImage) {
				recipeImage = [UIImage imageNamed:kStockImageRecipe];
			}
			bcCell.bcImageView.image = recipeImage;
		}
	}
    else if (indexPath.section == kSectionProducts) {
		if (indexPath.row < [self.products count]) {
			NSDictionary *product = [self.products objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = [product objectForKey:kName];
			bcCell.bcAlertImageView.hidden = [[product valueForKey:kAllergyCount] integerValue] == 0;
			bcCell.bcLeafImageView.hidden = [[product valueForKey:kLifestyleCount] integerValue] == 0;

			NSNumber *imageId = [[product objectForKey:kImageId] numberValueDecimal];
			UIImage *productImage = [[BCImageCache sharedCache] imageWithImageId:imageId size:kImageSizeMedium];
			if (productImage) {
				bcCell.bcImageView.image = productImage;
			}
			else {
				bcCell.bcImageView.image = [UIImage imageNamed:@"placeholder"];
			}
		}
    }
    else if (indexPath.section == kSectionMenuItems) {
		if (indexPath.row < [self.menuItems count]) {
			NSDictionary *menuItem = [self.menuItems objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = [menuItem objectForKey:kMenuItemName];
			bcCell.bcDetailTextLabel.text = [menuItem objectForKey:kMenuRestaurantName];
			bcCell.bcImageView.image = [UIImage imageNamed:kStockImageRecipe];
		}
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 0.0f;
    switch (section) {
        case kSectionLocalRecipes:
            if ([self.localRecipes count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
        case kSectionPublicRecipes:
            if ([self.publicRecipes count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
		case kSectionProducts:
            if ([self.products count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
		case kSectionMenuItems:
            if ([self.menuItems count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
        case kSectionWebRecipes:
            if ([self.webRecipes count] > 0) {
                headerHeight = kSectionHeaderHeight;
            }
            break;
        case kSectionAddCustomRecipe:
        default:
            break;
    }
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = nil;
    switch (section) {
        case kSectionLocalRecipes:
            sectionTitle = @"Your recipes";
            break;
        case kSectionPublicRecipes:
            sectionTitle = @"BettrLife recipes";
            break;
		case kSectionProducts:
            sectionTitle = @"Products";
            break;
		case kSectionMenuItems:
            sectionTitle = @"Restaurant Menu Items";
            break;
        case kSectionWebRecipes:
            sectionTitle = @"Web search";
            break;
        case kSectionAddCustomRecipe:
        default:
            break;
    }
    
	return [BCTableViewController customViewForHeaderWithTitle:sectionTitle];
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
		case kSectionLocalRecipes:
			if (indexPath.row == [self.localRecipes count]) {
				[self showBackView:YES withLabelText:@"All results for User Recipes"];
				[self resetResultArraysExcluding:self.localRecipes];
				[self reloadData];
				self.localRecipes = [self fetchLocalRecipes:YES filter:YES];
				[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionLocalRecipes] withRowAnimation:UITableViewRowAnimationAutomatic];
				[self loadImagesForOnscreenRows];
			}
			break;
		case kSectionPublicRecipes:
			if (indexPath.row == [self.publicRecipes count]) {
				[self sendMorePublicRecipesRequest];
			}
			break;
		case kSectionWebRecipes:
			if (indexPath.row == [self.publicRecipes count]) {
				[self sendMoreWebRecipesRequest];
			}
			break;
		case kSectionProducts:
			if (indexPath.row == [self.products count]) {
				[self sendMoreProductsRequest];
			}
			break;
		case kSectionMenuItems:
			if (indexPath.row == [self.menuItems count]) {
				[self sendMoreMenuItemsRequest];
			}
			break;
        case kSectionAddCustomRecipe:
        default:
			// do nothing
			break;
	}
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - MealPlannerDetailViewDelegate
- (void)mealPlannerDetailView:(MealPlannerDetailViewController *)mealPlannerDetailView didUpdate:(BOOL)changedMeal mealPlannerDay:(MealPlannerDay *)mpd
{
	if (changedMeal) {
		if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
			[self.delegate searchView:self didFinish:NO withSelection:mpd];
		}
	}
	else {
		[self.scratchContext rollback];
	}
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)view didUpdateObject:(id)object
{
}

@end
