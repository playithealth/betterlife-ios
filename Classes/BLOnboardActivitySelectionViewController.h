//
//  BLOnboardActivitySelectionViewController.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 3/26/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCDetailViewDelegate.h"

@interface BLOnboardActivitySelectionViewController : UITableViewController
@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;
@property (strong, nonatomic) NSArray *items;
@property (assign, nonatomic) BOOL isActivityLevel;
@property (assign, nonatomic) NSInteger selectedRow;
@end
