//
//  BLOnboardLifestyleViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 3/29/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BLOnboardViewDelegate.h"

@protocol BLOnboardDelegate;

@interface BLOnboardLifestyleViewController : UITableViewController <BLOnboardViewDelegate>
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (assign, nonatomic) BOOL isLifestyle; ///< This is showing either lifestyle or allergies
@property (strong, nonatomic) NSArray *viewStack;
@property (weak, nonatomic) id<BLOnboardDelegate> delegate;
@end
