//
//  BLCommunityInviteViewController.m
//  BuyerCompass
//
//  Created by Greg Goodrich on 9/9/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLCommunityInviteViewController.h"

#import "BCAppDelegate.h"
#import "BCObjectManager.h"
#import "BLHeaderFooterMultiline.h"
#import "BLThreadListViewController.h"
#import "BLThreadViewController.h"
#import "CommunityMO.h"
#import "ThreadMO.h"
#import "UIViewController+BLSidebarView.h"

@interface BLCommunityInviteViewController ()
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UISwitch *acceptSwitch;
@end

@implementation BLCommunityInviteViewController

static const NSInteger kSectionIntro = 0;
static const NSInteger kSectionShowPicture = 1;
static const NSInteger kSectionShareAgreement = 2;
static const NSInteger kSectionAcceptInvite = 3;

static const NSInteger kRowShowPicture = 0;
static const NSInteger kRowHidePicture = 1;

//static const NSInteger kRowShareAgreement = 0;

//static const NSInteger kRowJoinCommunity = 0;
static const NSInteger kRowDeclineInvite = 1;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self BL_implementSidebar];
    
	[self.tableView registerClass:[BLHeaderFooterMultiline class] forHeaderFooterViewReuseIdentifier:@"Multiline"];

	self.title = self.community.name;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

	// They've now viewed this community invite, so reflect that
	self.community.communityStatus = kCommunityStatusReviewed;
	self.community.status = kStatusPut;

	[self.managedObjectContext BL_save];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = UITableViewAutomaticDimension;

	if (indexPath.section == kSectionShareAgreement) {
		// TODO
		rowHeight = 120;
	}
	else if ((indexPath.section == kSectionAcceptInvite) && (indexPath.row == kRowDeclineInvite)) {
		if ([self.community.type isEqualToString:kCommunityTypeCoach]) {
			// Hide the decline button for coach communities
			rowHeight = 0;
		}
	}

	return rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat height = 0;
	switch (section) {
		case kSectionIntro:
			height = 130;
			break;
		case kSectionShowPicture:
			height = 90;
			break;
		case kSectionShareAgreement:
			height = 100;
			break;
		case kSectionAcceptInvite:
			height = 30;
			break;
	}

	return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];

	switch (section) {
		case kSectionIntro:
			headerView.multilineLabel.text = [NSString stringWithFormat:@"You have been invited by Coach %@ to join to the %@ community.\n\n"
				"To join, click Join Community below. You don’t have to decide right now. "
				"This invitation will still be available later.\n\n", self.community.ownerName, self.community.name];
			break;
		case kSectionShowPicture:
			headerView.multilineLabel.text = @"Your name, as you have entered it in the Account Settings screen, "
				"will be visible in this community. You can specify whether your picture should be shown.";
			break;
		case kSectionShareAgreement:
			headerView.multilineLabel.text = @"Your personal health information (weight, blood pressure, HbA1c, etc.) "
				"is entirely under your control. Choosing to share or not share such information in this community is your choice.";
			break;
		case kSectionAcceptInvite:
			headerView.multilineLabel.text = @"Do you want to join this community?";
			break;
	}

	return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	switch (indexPath.section) {
		case kSectionShowPicture: {
			UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
			if (cell.accessoryType != UITableViewCellAccessoryCheckmark) {
				[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
				NSInteger otherRow = kRowShowPicture;
				if (indexPath.row == kRowShowPicture) {
					otherRow = kRowHidePicture;
				}
				cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:otherRow inSection:kSectionShowPicture]];
				[cell setAccessoryType:UITableViewCellAccessoryNone];
			}
			break;
		}
		case kSectionAcceptInvite:
			break;
	}
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions
- (IBAction)acceptSwitchChanged:(UISwitch *)sender
{
	[self.joinButton setEnabled:[sender isOn]];
}

// Special method designed to pop out of this view, including going to the initial view controller if there is nothing to pop to
- (void)popView
{
	// Check the nav stack, and if this view is the only thing on it, then send the user to the initial view controller
	if ([self.navigationController.viewControllers count] == 1) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate gotoInitialViewController];
	}
	else {
		// Pop back to the prior view
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (IBAction)joinCommunityButtonPressed:(id)sender
{
	// See if use photo is enabled
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:
		[NSIndexPath indexPathForRow:kRowShowPicture inSection:kSectionShowPicture]];
	self.community.usePhoto = @(cell.accessoryType == UITableViewCellAccessoryCheckmark);
	self.community.communityStatus = kCommunityStatusAccepted;
	self.community.status = kStatusPut;

	[self.managedObjectContext BL_save];

	// Post a notification that we've updated the communities so that the side bar can update itself
	[[NSNotificationCenter defaultCenter] postNotificationName:[CommunityMO entityName]
		object:self userInfo:nil];

	[BCObjectManager syncCheck:SendAndReceive];

	// If there is more than one thread, then show the thread list, otherwise, take them directly to the only thread.
	// If, for some reason, there are no threads, then jump to the initial view controller
	NSUInteger threadCount = [self.community.threads count];
	if (threadCount == 1) {
		BLThreadViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ThreadMessage"];
		viewController.thread = [self.community.threads anyObject];
		[self.navigationController setViewControllers:@[viewController] animated:YES];
	}
	else if (threadCount > 1) {
		BLThreadListViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ThreadList"];
		viewController.community = self.community;
		[self.navigationController setViewControllers:@[viewController] animated:YES];
	}
	else {
		// Should never happen, just in case
		[self popView];
	}
}

- (IBAction)deleteInviteButtonPressed:(id)sender
{
	self.community.communityStatus = kCommunityStatusDeclined;
	self.community.status = kStatusPut;

	[self.managedObjectContext BL_save];

	// Post a notification that we've updated the communities so that the side bar can update itself
	[[NSNotificationCenter defaultCenter] postNotificationName:[CommunityMO entityName]
		object:self userInfo:nil];

	[BCObjectManager syncCheck:SendAndReceive];

	[self popView];
}

@end
