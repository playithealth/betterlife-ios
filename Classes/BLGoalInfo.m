//
//  BLGoalInfo.m
//  BettrLife
//
//  Created by Greg Goodrich on 5/2/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLGoalInfo.h"

@implementation BLGoalInfo

- (id)init
{
	self = [super init];
	if (self) {
		// Default to weight loss instead of weight gain
		self.isWeightLoss = YES;
	}

	return self;
}

@end
