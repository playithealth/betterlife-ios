//
//  BCObjectManager.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 12/30/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TaskQueue.h"

typedef enum {
	Success = 0,
	CoreDataError = 1,
	UnspecifiedError = 2,
    NoResultsReturned = 3,
	UnexpectedDataTypeReturned = 4,
	NoConnectivity = 5,
	BadRequest = 400,
	AuthorizationFailed = 401,
	Forbidden = 403,
	NotFound = 404,
	MethodNotAllowed = 405
} BCObjectManagerReturnCode;

typedef enum {
	SendOnly = 0,
	SendAndReceive = 1,
} BCObjectManagerSyncMode;

@class Store;
@class UserStoreMO;
@class BCObjectManager;
@class ShoppingListItem;

/*!
 * @class BCObjectManager
 * @abstract Class that does all cloud based synchronization to/from the iOS app.
 */
@interface BCObjectManager : NSObject
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)context;

+ (NSString *)getErrorTitle:(NSInteger)errorCode;
+ (NSString *)getErrorMessage:(NSInteger)errorCode;

+ (void)markItemsInCartPurchasedInMOC:(NSManagedObjectContext *)moc;
+ (void)syncCheck:(BCObjectManagerSyncMode)syncMode;
+ (BCObjectManager *)syncCheck:(BCObjectManagerSyncMode)syncMode inMOC:(NSManagedObjectContext *)moc
withCompletionBlock:(void(^)(void))completionBlock;

@end

