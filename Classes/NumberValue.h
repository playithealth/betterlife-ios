//
//  NumberFromString.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/12/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NumberValue
- (NSNumber *)numberValueWithNumberStyle:(NSNumberFormatterStyle)style;
- (NSNumber *)numberValueDecimal;
@end

@interface NSNumber (NumberFromString)
- (NSNumber *)numberValueWithNumberStyle:(NSNumberFormatterStyle)style;
- (NSNumber *)numberValueDecimal;
@end

@interface NSString (NumberFromString)
- (NSNumber *)numberValueWithNumberStyle:(NSNumberFormatterStyle)style;
- (NSNumber *)numberValueDecimal;
@end

@interface NSNull (NumberFromString)
- (NSNumber *)numberValueWithNumberStyle:(NSNumberFormatterStyle)style;
- (NSNumber *)numberValueDecimal;
@end

