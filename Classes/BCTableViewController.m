//
//  BCTableViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/15/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCTableViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "BCAppDelegate.h"
#import "BCObjectManager.h"
#import "UIColor+Additions.h"
#import "UIViewController+BLSidebarView.h"

#define kRefreshHeaderHeight 52

@interface BCTableViewController ()

@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end


@implementation BCTableViewController

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self BL_implementSidebar];
	[self setupUIRefreshControl];
}

#pragma mark - local methods
- (void)setupUIRefreshControl 
{
	self.refreshControl = [[UIRefreshControl alloc] init];

	[self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];

	[self.tableView addSubview:self.refreshControl];
}

- (void)reloadBCTableViewData 
{
	[self refreshLastUpdatedDate:[NSDate date]];
}

- (IBAction)refresh:(id)sender
{
	// Want to receive notifications regarding syncCheck finishing
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(syncCheckDidFinish:)
		name:@"syncCheck" object:nil];

	[BCObjectManager syncCheck:SendAndReceive];
}

- (void)syncCheckDidFinish:(NSNotification *)notification
{
	// Remove the notification observer for changes to ShoppingList
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:@"syncCheck" object:nil];

	[self reloadBCTableViewData];
	[self.refreshControl endRefreshing];
}


- (void)refreshLastUpdatedDate:(NSDate *)date
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setAMSymbol:@"AM"];
	[formatter setPMSymbol:@"PM"];
	[formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
	
	NSString *lastUpdated = [NSString stringWithFormat:@"Last Updated: %@", [formatter stringFromDate:date]];
	self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
}

+ (UIView *)customViewForHeaderWithTitle:(NSString *)title
{
	CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, appFrame.size.width, 24)];
    
	CAGradientLayer *gradient = [CAGradientLayer layer];
	gradient.frame = headerView.bounds;
	gradient.colors = [NSArray arrayWithObjects:
       (id)[[UIColor BC_mediumDarkGrayColor] CGColor],
       (id)[[UIColor BC_lightGrayColor] CGColor],
       (id)[[UIColor BC_lightGrayColor] CGColor],
       (id)[[UIColor BC_mediumDarkGrayColor] CGColor],
       nil];
	gradient.locations = [NSArray arrayWithObjects:
      [NSNumber numberWithFloat:0.0],
      [NSNumber numberWithFloat:0.04],
      [NSNumber numberWithFloat:0.96],
      [NSNumber numberWithFloat:1.0],
      nil];
    
	[headerView.layer insertSublayer:gradient atIndex:0];
    
	UILabel *label = [[UILabel alloc]
          initWithFrame:CGRectMake(6, 6, 308, 12)];
	label.text = title;
	label.textColor = [UIColor BC_mediumDarkGrayColor];
	label.backgroundColor = [UIColor BC_lightGrayColor];
	label.font = [UIFont boldSystemFontOfSize:12];
	[headerView addSubview:label];
    
    return headerView;
}

#pragma mark - UIViewController override
- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];

	[self.tableView setEditing:editing animated:animated];
}

#pragma mark - UITableDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

@end
