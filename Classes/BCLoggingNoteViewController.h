//
//  BCLoggingNoteViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 12/6/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoggingNoteMO;

@interface BCLoggingNoteViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) LoggingNoteMO *loggingNote;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end
