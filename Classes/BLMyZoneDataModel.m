//
//  BLMyZoneDataModel.m
//  BuyerCompass
//
//  Created by Greg Goodrich on 10/29/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLMyZoneDataModel.h"

#import "NSCalendar+Additions.h"
#import "TrackingMO.h"
#import "TrainingActivityMO.h"
#import "User.h"

@interface BLMyZoneDataModel ()
@property (strong, nonatomic) NSCalendar *calendar;
@end

@implementation BLMyZoneDataModel

- (id)initWithMOC:(NSManagedObjectContext *)moc;
{
	self = [super init];
	if (self) {
		_managedObjectContext = moc;
		_calendar = [NSCalendar currentCalendar];
	}

	return self;
}

// Should not be calling this, as it will not get a MOC
- (id)init
{
	return [self initWithMOC:nil];
}

- (void)loadDailyLogsForCalendarWithStartDate:(NSDate *)startDate
{
	// If no start date was passed in, calculate the calendar based upon today
	if (!startDate) {
		// Get the start date by first getting the first day of this month, then get the first day of that week, then get midnight
		startDate = [self.calendar BC_startOfDate:[self.calendar BC_firstOfWeekForDate:
			[self.calendar BC_firstOfMonthForDate:[NSDate date]]]];
	}

	// Calendar grid is 6 rows * 7 days each, for 42, minus one for the math
	int capacity = 42;
	NSDate *endDate = [self.calendar BC_endOfDate:[self.calendar BC_dateByAddingDays:(capacity - 1) toDate:startDate]];
	NSMutableArray *dailyLogs = [NSMutableArray arrayWithCapacity:capacity];
	for (int idx = 0; idx < capacity; idx++) {
		[dailyLogs addObject:@NO];
	}
	NSArray *activityLogs = [TrainingActivityMO MR_findAllSortedBy:TrainingActivityMOAttributes.date ascending:YES
		withPredicate:[NSPredicate predicateWithFormat:@"source = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
		BLActivitySourceMyZone, startDate, endDate, kStatusDelete, [User loginId]] inContext:self.managedObjectContext];
	for (TrainingActivityMO *activityLog in activityLogs) {
		[dailyLogs setObject:@YES atIndexedSubscript:[self.calendar BC_daysFromDate:startDate toDate:activityLog.date]];
	}
	self.daysWithData = dailyLogs;
}

- (void)reloadData
{
	if (![[User currentUser].userProfile.hasMyZone boolValue]) {
		return;
	}

	NSDate *now = [NSDate date];
	NSDate *midnightToday = [self.calendar BC_startOfDate:now];
	NSDate *startDate;
	NSDate *endDate = [self.calendar BC_endOfDate:now]; // Hedge our timezone bets and set our end date to the end of today
	NSString *mepsKeyPath = [NSString stringWithFormat:@"@sum.valueDictionary.%@", kTrackingValue6];

	//
	// Week MEPs data
	// NOTE: There should only be one row per day for TrackingTypes.myzone, as this is intended as a summary tracking type
	NSArray *trackingLogs;
	startDate = [self.calendar BC_firstOfWeekForDate:midnightToday];
	trackingLogs = [TrackingMO MR_findAllSortedBy:TrackingMOAttributes.date ascending:YES
		withPredicate:[NSPredicate predicateWithFormat:@"trackingType = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
		BCTrackingTypeMyZone, startDate, endDate, kStatusDelete, [User loginId]] inContext:self.managedObjectContext];
	self.weekMEPs = [trackingLogs valueForKeyPath:mepsKeyPath];

	//
	// Week Cals data - reuse the startDate, since it is already established for this time period
	// Create an array of booleans that determines if the user has myzone Cals for that day of the week, initialized to NO, will update
	// within the loop if YES
	NSMutableArray *dailyLogs = [@[ @NO, @NO, @NO, @NO, @NO, @NO, @NO ] mutableCopy];
	NSArray *activityLogs = [TrainingActivityMO MR_findAllSortedBy:TrainingActivityMOAttributes.date ascending:YES
		withPredicate:[NSPredicate predicateWithFormat:@"source = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
		BLActivitySourceMyZone, startDate, endDate, kStatusDelete, [User loginId]] inContext:self.managedObjectContext];
	double weeklySum = 0;
	for (TrainingActivityMO *activityLog in activityLogs) {
		NSDateComponents *components = [self.calendar components:(NSWeekdayCalendarUnit) fromDate:activityLog.date];
		[dailyLogs setObject:@YES atIndexedSubscript:components.weekday - 1];
		weeklySum += [activityLog.calories doubleValue];
	}
	self.weekdaysWithData = dailyLogs;
	self.weekCals = @(weeklySum);

	//
	// Month MEPs data
	startDate = [self.calendar BC_firstOfMonthForDate:midnightToday];
	trackingLogs = [TrackingMO MR_findAllSortedBy:TrackingMOAttributes.date ascending:YES
		withPredicate:[NSPredicate predicateWithFormat:@"trackingType = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
		BCTrackingTypeMyZone, startDate, endDate, kStatusDelete, [User loginId]] inContext:self.managedObjectContext];
	self.monthMEPs = [trackingLogs valueForKeyPath:mepsKeyPath];

	//
	// Month Cals data - reuse the startDate, since it is already established for this time period
	self.monthCals = [TrainingActivityMO MR_aggregateOperation:@"sum:" onAttribute:TrainingActivityMOAttributes.calories
		withPredicate:[NSPredicate predicateWithFormat:@"source = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
		BLActivitySourceMyZone, startDate, endDate, kStatusDelete, [User loginId]] inContext:self.managedObjectContext];

	//
	// Year MEPs data
	startDate = [self.calendar BC_firstOfYearForDate:midnightToday];
	trackingLogs = [TrackingMO MR_findAllSortedBy:TrackingMOAttributes.date ascending:YES
		withPredicate:[NSPredicate predicateWithFormat:@"trackingType = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
		BCTrackingTypeMyZone, startDate, endDate, kStatusDelete, [User loginId]] inContext:self.managedObjectContext];
	self.yearMEPs = [trackingLogs valueForKeyPath:mepsKeyPath];

	//
	// Year Cals data - reuse the startDate, since it is already established for this time period
	self.yearCals = [TrainingActivityMO MR_aggregateOperation:@"sum:" onAttribute:TrainingActivityMOAttributes.calories
		withPredicate:[NSPredicate predicateWithFormat:@"source = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
		BLActivitySourceMyZone, startDate, endDate, kStatusDelete, [User loginId]] inContext:self.managedObjectContext];

	// Now load the daily log info for the calendar
	[self loadDailyLogsForCalendarWithStartDate:nil];
}

@end
