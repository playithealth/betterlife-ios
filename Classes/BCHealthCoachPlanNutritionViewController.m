//
//  BCHealthCoachPlanNutritionViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 7/12/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCHealthCoachPlanNutritionViewController.h"


@interface BCHealthCoachPlanNutritionViewController ()

@property (strong, nonatomic) NSArray *nutrientKeys;
@property (strong, nonatomic) NSArray *nutrientNames;
@property (strong, nonatomic) NSArray *nutrientIndices;

@end

@implementation BCHealthCoachPlanNutritionViewController



- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
    if (self) {
		self.nutrientKeys = @[@"calories", @"calories_from_fat", @"saturated_fat_calories",
			@"total_fat", @"saturated_fat", @"polyunsaturated_fat", @"monounsaturated_fat",
			@"trans_fat", @"cholesterol", @"sodium", @"potassium", @"total_carbohydrate",
			@"dietary_fiber", @"soluble_fiber", @"insoluble_fiber", @"sugars", @"sugar_alcohol",
			@"other_carbohydrate", @"protein", @"vitamin_a_pct", @"vitamin_c_pct", @"calcium_pct", @"iron_pct"];
		self.nutrientNames = @[@"Calories", @"Calories From Fat", @"Calories From Saturated Fat",
			@"Total Fat", @"Saturated Fat", @"Polyunsaturated Fat", @"Monounsaturated Fat",
			@"Trans Fat", @"Cholesterol", @"Sodium", @"Potassium", @"Total Carbohydrate",
			@"Dietary Fiber", @"Soluble Fiber", @"Insoluble Fiber", @"Sugars", @"Sugar Alcohol",
			@"Other Carbohydrate", @"Protein", @"Vitamin A Pct", @"Vitamin C Pct", @"Calcium Pct", @"Iron Pct"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// build nutrition array
	NSMutableArray *nutrientIndices = [NSMutableArray array];
	for (NSInteger nutrientIndex = 0; nutrientIndex < 23; nutrientIndex++) {
		id itemValue = [self.itemNutrition valueForKey:self.nutrientKeys[nutrientIndex]];
		id minValue = [self.minNutrition valueForKey:self.nutrientKeys[nutrientIndex]];
		id maxValue = [self.maxNutrition valueForKey:self.nutrientKeys[nutrientIndex]];

		if (itemValue || minValue || maxValue) {
			[nutrientIndices addObject:@(nutrientIndex)];
		}
	}
	self.nutrientIndices = nutrientIndices;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	if (section == 0) {
		numberOfRows = [self.nutrientIndices count];
	}
	else {
		// TODO the food section
		numberOfRows = 0;
	}
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
	NSInteger nutrientIndex = [self.nutrientIndices[indexPath.row] integerValue];

	cell.textLabel.text = self.nutrientNames[nutrientIndex];

	NSNumber *itemValue = [self.itemNutrition valueForKey:self.nutrientKeys[nutrientIndex]];
	NSNumber *minValue = [self.minNutrition valueForKey:self.nutrientKeys[nutrientIndex]];
	NSNumber *maxValue = [self.maxNutrition valueForKey:self.nutrientKeys[nutrientIndex]];

	NSMutableString *nutritionString = [NSMutableString string];

	if (itemValue && (NSNull *)itemValue != [NSNull null]) {
		[nutritionString appendFormat:@"%@", itemValue];
	}

	if (minValue && [nutritionString length] > 0) {
		[nutritionString appendString:@"/"];
	}

	if (minValue && !maxValue) {
		[nutritionString appendString:@">"];
	}

	if (minValue) {
		[nutritionString appendString:[minValue stringValue]];
	}

	if (maxValue && !minValue) {
		[nutritionString appendString:@"<"];
	}

	if (maxValue && minValue) {
		[nutritionString appendString:@"-"];
	}

	if (maxValue) {
		[nutritionString appendString:[maxValue stringValue]];
	}

	cell.detailTextLabel.text = nutritionString;
    
    return cell;
}

#pragma mark - Table view delegate
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString *title = nil;
	if (section != 0) {
		title = @"Food";
	}
	return title;
}

@end
