//
//  NSManagedObject+BLSync.h
//  BettrLife
//
//  Created by Greg Goodrich on 12/2/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (BLSync)
+ (NSPredicate *)BL_loginIdPredicate;
+ (NSPredicate *)BL_accountIdPredicate;
+ (void)BL_syncUpdateUsingModifiedDeleteStrategyForLogin:(NSNumber *)loginId withUpdates:(NSArray *)updates
inMOC:(NSManagedObjectContext *)moc;
+ (void)BL_syncUpdateForLogin:(NSNumber *)loginId withUpdates:(NSArray *)updates inMOC:(NSManagedObjectContext *)moc;
+ (void)BL_syncUpdateUsingDeleteStrategyForLogin:(NSNumber *)loginId withUpdates:(NSArray *)updates inMOC:(NSManagedObjectContext *)moc;
@end
