//
//  NSDictionary+NumberValue.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/20/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "NSDictionary+NumberValue.h"

@implementation NSDictionary (BC_NumberValue)
- (NSNumber *)BC_numberForKey:(id)aKey
{
	id anObject = [self objectForKey:aKey];
	NSNumber *aNumber = nil;

	if ([anObject isKindOfClass:[NSNumber class]]) {
		aNumber = anObject;
	}
	else if ([anObject isKindOfClass:[NSString class]]) {
		NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
		formatter.numberStyle = NSNumberFormatterDecimalStyle;
		aNumber = [formatter numberFromString:anObject];
	}
	else {
		aNumber = @0;
	}
	
	return aNumber;
}
- (NSNumber *)BC_numberOrNilForKey:(id)aKey
{
	id anObject = [self objectForKey:aKey];
	NSNumber *aNumber = nil;

	if ([anObject isKindOfClass:[NSNumber class]]) {
		aNumber = anObject;
	}
	else if ([anObject isKindOfClass:[NSString class]]) {
		NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
		formatter.numberStyle = NSNumberFormatterDecimalStyle;
		aNumber = [formatter numberFromString:anObject];
	}
	
	return aNumber;
}
- (NSString *)BC_stringOrNilForKey:(id)aKey
{
	id anObject = [self objectForKey:aKey];
	NSString *aString = nil;

	if ([anObject isKindOfClass:[NSString class]]) {
		aString = anObject;
	}
	
	return aString;
}
@end
