//
//  MealIngredientEditCell.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 9/15/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MealIngredientEditCell : UITableViewCell {
    UITextField *quantity;
    UITextField *name;
    UITextField *units;
}

@property (nonatomic, strong) IBOutlet UITextField *quantity;
@property (nonatomic, strong) IBOutlet UITextField *name;
@property (nonatomic, strong) IBOutlet UITextField *units;

@end
