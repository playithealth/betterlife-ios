//
//  FoodLogDetailViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/31/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCDetailViewDelegate.h"
#import "BLFoodItem.h"
#import "FoodLogMO.h"

@class NutritionMO;

typedef NS_ENUM(NSInteger, ItemDetailViewMode) {
	ItemDetailViewModeLogging,
	ItemDetailViewModePlanning,
	ItemDetailViewModeCoachTagPlanning,
	ItemDetailViewModeCoachTagLogging,
	ItemDetailViewModeShopping,
	ItemDetailViewModeEditing
};

@interface FoodLogDetailViewController : UIViewController

@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;
@property (weak, nonatomic) id<BLFoodItem> item;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
//@property (strong, nonatomic) FoodLogMO *foodLog;
@property (assign, nonatomic) BOOL createCustomFood;
@property (assign, nonatomic) ItemDetailViewMode detailViewMode;

@end
