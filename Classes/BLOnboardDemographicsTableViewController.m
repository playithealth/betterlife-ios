//
//  BLOnboardDemographicsTableViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 3/22/14.
//  Copyright (c) 2014 BettrLife Corp. All rights reserved.
//

#import "BLOnboardDemographicsTableViewController.h"

#import "BCUtilities.h"
#import "BLHeaderFooterMultiline.h"
#import "BLHeaderFooterRightDetail.h"
#import "BLOnboardActivitySelectionViewController.h"
#import "BLOnboardActivityViewController.h"
#import "BLOnboardWelcomeViewController.h"
#import "NSCalendar+Additions.h"
#import "TrackingMO.h"
#import "UIColor+Additions.h"
#import "UserProfileMO.h"

#pragma mark - Custom Cells
@interface SelectorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *selectorLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@end
@implementation SelectorCell
@end

@interface PickerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@end
@implementation PickerCell
@end

@interface BirthdayCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@end
@implementation BirthdayCell
@end

typedef NS_ENUM(NSUInteger, ActivePicker) { APNone, APBirthday, APHeight, APWeight, APGoalWeight };

@interface BLOnboardDemographicsTableViewController () <UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) UserProfileMO *userProfileMO;
@property (strong, nonatomic) NSNumber *currentWeight;
@property (strong, nonatomic) NSNumber *bmr;
@property (strong, nonatomic) NSNumber *activityLevelCredit;
@property (strong, nonatomic) NSNumber *nutritionEffortCredit;
@property (strong, nonatomic) NSNumber *dailyCalories;
@property (assign, nonatomic) BOOL weightUpdated;
@property (assign, nonatomic) ActivePicker activePicker;
@property (strong, nonatomic) NSArray *activityLevels;
@property (strong, nonatomic) NSArray *nutritionEfforts;
@property (assign, nonatomic) NSInteger selectedActivityLevel;
@property (assign, nonatomic) NSInteger selectedNutritionEffort;

@property (strong, nonatomic) NSMutableAttributedString *basicsFooterAttributedString;
@end

@implementation BLOnboardDemographicsTableViewController

static const NSInteger kSectionBasics = 0;
static const NSInteger kSectionActivityLevel = 1;
static const NSInteger kSectionGoalWeight = 2;
static const NSInteger kSectionNutritionEffort = 3;
static const NSInteger kSectionTotals = 4;

static const NSInteger kRowBirthday = 0;
static const NSInteger kRowBirthdayPicker = 1;
static const NSInteger kRowGender = 2;
static const NSInteger kRowHeight = 3;
static const NSInteger kRowHeightPicker = 4;
static const NSInteger kRowWeight = 5;
static const NSInteger kRowWeightPicker = 6;
//static const NSInteger kRowActivityLevel = 0;
static const NSInteger kRowGoalWeight = 0;
static const NSInteger kRowGoalWeightPicker = 1;

static const CGFloat kPickerRowHeight = 180.0;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	[self.tableView registerClass:[BLHeaderFooterMultiline class] forHeaderFooterViewReuseIdentifier:@"Multiline"];
	[self.tableView registerClass:[BLHeaderFooterRightDetail class] forHeaderFooterViewReuseIdentifier:@"RightDetail"];

	if (self.viewStack) {
		self.title = [NSString stringWithFormat:@"Welcome: %ld of %ld",
			(long)[[self.navigationController viewControllers] count] - 1, (long)[self.viewStack count] - 1];
	}
	else {
		self.title = @"Calorie Calculator";
		self.navigationItem.rightBarButtonItem = nil;
	}

	self.activityLevels = @[
		@{ @"title" : @"Using Fitness Tracking Device", @"description" : @"Activities logged from fitness tracking device",
			@"value" : @(kActivityLevelValueTrackingDevice) },
		@{ @"title" : @"Sedentary/Manual", @"description" : @"Little or no exercise/Log activities by hand",
			@"value" : @(kActivityLevelValueSedentary) },
		@{ @"title" : @"Light", @"description" : @"Light exercise/sports 1-3 days per week", @"value" : @(kActivityLevelValueLight) },
		@{ @"title" : @"Moderate", @"description" : @"Moderate exercise/sports 3-5 days per week", @"value"
			: @(kActivityLevelValueModerate) },
		@{ @"title" : @"Active", @"description" : @"Hard exercise/sports 5-7 days per week", @"value" : @(kActivityLevelValueActive) },
		@{ @"title" : @"Very Active", @"description" : @"Very hard exercise/sports, physical job, or 2x training",
			@"value" : @(kActivityLevelValueVeryActive) }
	];
	self.nutritionEfforts = @[
		@{ @"title" : @"None", @"description" : @"Maintain same weight", @"value" : @(0) },
		@{ @"title" : @"Easy", @"description" : @"0.5 lb. per week, 250 Calories per day", @"value" : @(250) },
		@{ @"title" : @"Moderate", @"description" : @"1.0 lb. per week, 500 Calories per day", @"value" : @(500) },
		@{ @"title" : @"Hard", @"description" : @"1.5 lb. per week, 750 Calories per day", @"value" : @(750) },
		@{ @"title" : @"Very Hard", @"description" : @"2.0 lb. per week, 1000 Calories per day", @"value" : @(1000) }
	];

	self.userProfileMO = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];
	self.currentWeight = [TrackingMO getCurrentWeightInMOC:self.managedObjectContext];

	if (!self.userProfileMO.activityLevel) {
		self.userProfileMO.activityLevel = [[self.activityLevels objectAtIndex:1] valueForKey:@"value"];
	}
	if (!self.userProfileMO.nutritionEffort) {
		self.userProfileMO.nutritionEffort = [[self.nutritionEfforts objectAtIndex:2] valueForKey:@"value"];
	}

	// Color the 'BMR' in blue to designate a link
	//NSString *footerString = @"Entering all of the above allows your maximum Calories per day to be calculated automatically (via BMR).";
	NSString *footerString = @"BMR Daily Calories";
	NSMutableAttributedString *footerAttributedString = [[NSMutableAttributedString alloc] initWithString:footerString];
	[footerAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15]
		range:NSMakeRange(0, [footerAttributedString length])];
	[footerAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor BC_colorWithHex:0x157efb]
		range:[footerString rangeOfString:@"BMR"]];
	self.basicsFooterAttributedString = footerAttributedString;

	// Quasi-hack to cause the table view to not show 'blank' rows at the bottom
	self.tableView.tableFooterView = [[UIView alloc] init];

	[self updateView];
}

- (void)viewWillDisappear:(BOOL)animated
{
	if (self.weightUpdated) {
		[TrackingMO addWeight:self.currentWeight onDate:[NSDate date] inMOC:self.managedObjectContext];
	}
	self.weightUpdated = NO;

	if ([self.managedObjectContext hasChanges]) {
		[self.managedObjectContext BL_save];
	}

	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rows = 0;

	if (section == kSectionBasics) {
		rows = 7;
	}
	else if (section == kSectionActivityLevel) {
		rows = 1;
	}
	else if (section == kSectionGoalWeight) {
		rows = 2;
	}
	else if (section == kSectionNutritionEffort) {
		rows = 1;
	}

    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
	NSString *cellId = @"Basic"; // Default to basic cell, as most are this, and we will change it if necessary

	switch ([indexPath section]) {
		case kSectionBasics:
			switch ([indexPath row]) {
				case kRowBirthday: {
					cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
					NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
					[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
					[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
					NSString *birthday = [dateFormatter stringFromDate:self.userProfileMO.birthday];
					cell.textLabel.text = @"Birthday";
					cell.detailTextLabel.text = birthday;
					break;
				}
				case kRowBirthdayPicker:
					if (self.activePicker == APBirthday) {
						cellId = @"DatePicker";
						cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
						BirthdayCell *birthdayCell = (BirthdayCell *)cell;
						if (self.userProfileMO.birthday) {
							birthdayCell.datePicker.date = self.userProfileMO.birthday;
						}
						[birthdayCell.datePicker addTarget:self action:@selector(datePickerValueChanged:)
							forControlEvents:UIControlEventValueChanged];
					}
					else {
						cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
					}
					break;
				case kRowGender: {
					cellId = @"Selector";
					cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
					SelectorCell *selectorCell = (SelectorCell *)cell;
					selectorCell.selectorLabel.text = @"Gender";
					if (self.userProfileMO.gender) {
						selectorCell.segmentedControl.selectedSegmentIndex = (self.userProfileMO.genderValue == kGenderMale ? 0 : 1);
					}
					[selectorCell.segmentedControl addTarget:self action:@selector(genderChanged:)
						forControlEvents:UIControlEventValueChanged];
					break;
                }
				case kRowHeight: {
					cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
					NSInteger totalInches = [self.userProfileMO.height integerValue];
					NSInteger feet = totalInches / 12;
					NSInteger inches = totalInches % 12;
					cell.textLabel.text = @"Height";
					cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld' %ld\"", (long)feet, (long)inches];
					break;
                }
				case kRowHeightPicker: {
					if (self.activePicker == APHeight) {
						cellId = @"Picker";
						cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
						PickerCell *pickerCell = (PickerCell *)cell;
						pickerCell.pickerView.tag = 1;
						pickerCell.pickerView.delegate = self;
						pickerCell.pickerView.dataSource = self;
						pickerCell.pickerView.showsSelectionIndicator = YES;
						if ([self.userProfileMO.height integerValue] > 0) {
							NSInteger totalInches = [self.userProfileMO.height integerValue];
							NSInteger feet = totalInches / 12;
							NSInteger inches = totalInches % 12;
							[pickerCell.pickerView selectRow:feet - 3 inComponent:0 animated:NO];
							[pickerCell.pickerView selectRow:inches inComponent:1 animated:NO];
						}
						else {
							[pickerCell.pickerView selectRow:0 inComponent:0 animated:NO];
							[pickerCell.pickerView selectRow:0 inComponent:1 animated:NO];
						}
					}
					else {
						cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
					}
					break;
                }
				case kRowWeight:
					cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
					cell.textLabel.text = @"Weight";
					cell.detailTextLabel.text = [self.currentWeight stringValue];
					break;
				case kRowWeightPicker: {
					if (self.activePicker == APWeight) {
						cellId = @"Picker";
						cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
						PickerCell *pickerCell = (PickerCell *)cell;
						pickerCell.pickerView.tag = 2;
						pickerCell.pickerView.delegate = self;
						pickerCell.pickerView.dataSource = self;
						pickerCell.pickerView.showsSelectionIndicator = YES;
						if ([self.currentWeight integerValue] > 0) {
							double weight = [self.currentWeight doubleValue];
							NSInteger lbs = floor(weight);
							NSInteger tenths = round((weight - lbs) * 10);
							[pickerCell.pickerView selectRow:lbs inComponent:0 animated:NO];
							[pickerCell.pickerView selectRow:tenths inComponent:1 animated:NO];
						}
						else {
							// Start out at something more convenient than 0 lbs
							[pickerCell.pickerView selectRow:130 inComponent:0 animated:NO];
							[pickerCell.pickerView selectRow:0 inComponent:1 animated:NO];
						}
					}
					else {
						cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
					}
					break;
                }
			}
			break;
		case kSectionActivityLevel: {
			cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
			cell.textLabel.text = @"Activity Level";
			NSDictionary *activityLevel = nil;
			for (NSInteger idx = 0; idx < [self.activityLevels count]; idx++) {
				NSDictionary *anActivityLevel = [self.activityLevels objectAtIndex:idx];
				if ([[anActivityLevel valueForKey:@"value"] doubleValue] == [self.userProfileMO.activityLevel doubleValue]) {
					self.selectedActivityLevel = idx;
					activityLevel = anActivityLevel;
					break;
				}
			}
			if (!activityLevel) {
				activityLevel = [self.activityLevels objectAtIndex:0];
			}
			cell.detailTextLabel.text = [activityLevel valueForKey:@"title"];
			break;
		}
		case kSectionGoalWeight:
			switch ([indexPath row]) {
				case kRowGoalWeight:
					cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
					cell.textLabel.text = @"Goal Weight";
					cell.detailTextLabel.text = [self.userProfileMO.goalWeight stringValue];
					break;
				case kRowGoalWeightPicker: {
					if (self.activePicker == APGoalWeight) {
						cellId = @"Picker";
						cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
						PickerCell *pickerCell = (PickerCell *)cell;
						pickerCell.pickerView.tag = 3;
						pickerCell.pickerView.delegate = self;
						pickerCell.pickerView.dataSource = self;
						pickerCell.pickerView.showsSelectionIndicator = YES;
						if ([self.userProfileMO.goalWeight integerValue] > 0) {
							[pickerCell.pickerView selectRow:[self.userProfileMO.goalWeight integerValue] inComponent:0 animated:NO];
						}
						else {
							// We don't have a goal weight, but if we have a current weight, then select that row to get them close
							if ([self.currentWeight integerValue] > 0) {
								[pickerCell.pickerView selectRow:[self.currentWeight integerValue] inComponent:0 animated:NO];
							}
							else {
								[pickerCell.pickerView selectRow:0 inComponent:0 animated:NO];
							}
						}
					}
					else {
						cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
					}
					break;
				}
			}
			break;
		case kSectionNutritionEffort: {
			cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
			cell.textLabel.text = @"Weight Effort";
			NSDictionary *nutritionEffort = nil;
			for (NSInteger idx = 0; idx < [self.nutritionEfforts count]; idx++) {
				NSDictionary *anNutritionEffort = [self.nutritionEfforts objectAtIndex:idx];
				if ([[anNutritionEffort valueForKey:@"value"] integerValue] == [self.userProfileMO.nutritionEffort integerValue]) {
					self.selectedNutritionEffort = idx;
					nutritionEffort = anNutritionEffort;
					break;
				}
			}
			if (!nutritionEffort) {
				nutritionEffort = [self.nutritionEfforts objectAtIndex:0];
			}
			cell.detailTextLabel.text = [nutritionEffort valueForKey:@"title"];
			break;
		}
	}
    
    return cell;
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = UITableViewAutomaticDimension;

	switch ([indexPath section]) {
		case kSectionBasics:
			switch ([indexPath row]) {
				case kRowBirthdayPicker:
					rowHeight = (self.activePicker == APBirthday ? kPickerRowHeight : 0);
					break;
				case kRowHeightPicker:
					rowHeight = (self.activePicker == APHeight ? kPickerRowHeight : 0);
					break;
				case kRowWeightPicker:
					rowHeight = (self.activePicker == APWeight ? kPickerRowHeight : 0);
					break;
			}
			break;
		case kSectionActivityLevel:
			break;
		case kSectionGoalWeight:
			if ([indexPath row] == kRowGoalWeightPicker) {
				rowHeight = (self.activePicker == APGoalWeight ? kPickerRowHeight : 0);
				break;
			}
			break;
		case kSectionNutritionEffort:
			break;
	}
    
    return rowHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat height = UITableViewAutomaticDimension;
	switch (section) {
		case kSectionBasics:
			height = 84;
			break;
	}

	return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	CGFloat height = UITableViewAutomaticDimension;
	switch (section) {
		case kSectionBasics:
		case kSectionActivityLevel:
		case kSectionNutritionEffort:
		case kSectionTotals:
			height = 36;
			break;
		case kSectionGoalWeight:
			height = 0;
			break;
	}

	return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UITableViewHeaderFooterView *headerView = nil;

	switch (section) {
		case kSectionBasics: {
			headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
			BLHeaderFooterMultiline *mlHeaderView = (BLHeaderFooterMultiline *)headerView;
			mlHeaderView.multilineLabel.text = @"All of this information is optional, but the more you enter, "
				"the more we can do to help you manage your health.";
			mlHeaderView.normalLabel.text = @"DAILY CALORIE CALCULATOR";
			break;
		}
		case kSectionActivityLevel: {
			break;
		}
		case kSectionGoalWeight: {
			break;
		}
		case kSectionNutritionEffort: {
			break;
		}
	}

	return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UITableViewHeaderFooterView *footerView = nil;

	switch (section) {
		case kSectionBasics: {
			footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"RightDetail"];
			BLHeaderFooterRightDetail *rdFooterView = (BLHeaderFooterRightDetail *)footerView;
			rdFooterView.nameLabel.attributedText = self.basicsFooterAttributedString;
			rdFooterView.valueLabel.text = [NSString stringWithFormat:@"%@", (self.bmr ? @((int)([self.bmr doubleValue] + 0.5)) : @"??")];
			if (![footerView.contentView.gestureRecognizers count]) {
				UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bmrTapped:)];
				[footerView.contentView addGestureRecognizer:recognizer];
			}
			break;
		}
		case kSectionActivityLevel: {
			footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"RightDetail"];
			BLHeaderFooterRightDetail *rdFooterView = (BLHeaderFooterRightDetail *)footerView;
			rdFooterView.nameLabel.text = @"Activity Level Credit";
			rdFooterView.valueLabel.text = (self.activityLevelCredit
				? [NSString stringWithFormat:@"+%@", self.activityLevelCredit] : @"No BMR");
			break;
		}
		case kSectionGoalWeight:
			break;
		case kSectionNutritionEffort: {
			footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"RightDetail"];
			BLHeaderFooterRightDetail *rdFooterView = (BLHeaderFooterRightDetail *)footerView;
			rdFooterView.nameLabel.text = @"Weight Effort Adjustment";
			rdFooterView.valueLabel.text = [self.nutritionEffortCredit stringValue];
			break;
		}
		case kSectionTotals: {
			footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"RightDetail"];
			BLHeaderFooterRightDetail *rdFooterView = (BLHeaderFooterRightDetail *)footerView;
			rdFooterView.nameLabel.text = @"Daily Calories";
			rdFooterView.nameLabel.textColor = [UIColor blackColor];
			rdFooterView.valueLabel.text = (self.dailyCalories ? [self.dailyCalories stringValue] : @"??");
			break;
		}
	}

	return footerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionActivityLevel) {
		[self performSegueWithIdentifier:@"ActivityLevel" sender:self];
	}
	else if (indexPath.section == kSectionNutritionEffort) {
		[self performSegueWithIdentifier:@"NutritionEffort" sender:self];
	}
	else {
		NSIndexPath *pickerIndexPath = [NSIndexPath indexPathForRow:([indexPath row] + 1) inSection:[indexPath section]];
		NSIndexPath *currentPath = nil;
		// We will reload from here to the end of the table to avoid glitches in footers, see comment down farther
		NSUInteger lowestSection = [self numberOfSectionsInTableView:self.tableView];

		switch (self.activePicker) {
			case APNone:
				break;
			case APBirthday:
				currentPath = [NSIndexPath indexPathForRow:kRowBirthdayPicker inSection:kSectionBasics];
				break;
			case APHeight:
				currentPath = [NSIndexPath indexPathForRow:kRowHeightPicker inSection:kSectionBasics];
				break;
			case APWeight:
				currentPath = [NSIndexPath indexPathForRow:kRowWeightPicker inSection:kSectionBasics];
				break;
			case APGoalWeight:
				currentPath = [NSIndexPath indexPathForRow:kRowGoalWeightPicker inSection:kSectionGoalWeight];
				break;
		}

		BOOL isOnlyCollapsing = NO;
		// If there is an active picker, we will be collapsing it, so add its index path to our reload
		if (currentPath) {
			lowestSection = currentPath.section;
			// If the click was on the same row that activates this picker, then the user is simply collapsing the current picker,
			// and not opening a different one, so no point in doing more
			isOnlyCollapsing = ([pickerIndexPath compare:currentPath] == NSOrderedSame ? YES : NO);
		}

		BOOL reload = YES;
		self.activePicker = APNone;
		if (!isOnlyCollapsing) {
			switch ([indexPath section]) {
				case kSectionBasics:
					switch ([indexPath row]) {
						case kRowBirthday:
							self.activePicker = APBirthday;
							break;
						case kRowHeight:
							self.activePicker = APHeight;
							break;
						case kRowWeight:
							self.activePicker = APWeight;
							break;
						default:
							reload = NO;
							break;
					}
					break;
				case kSectionGoalWeight:
					if ([indexPath row] == kRowGoalWeight) {
						self.activePicker = APGoalWeight;
					}
					else {
						reload = NO;
					}
					break;
			}
			if (reload) {
				lowestSection = MIN(lowestSection, pickerIndexPath.section);
			}
		}

		if (reload) {
			// This is really hacky, but if I don't do it this way, then the section footers disappear when collapsing a picker, and
			// they don't reappear until you scroll the view or do some other operation that causes a refresh. I cannot figure out
			// another way to make this work, and keep the animations intact.
			NSMutableIndexSet *reloadSections = [[NSMutableIndexSet alloc] init];
			for (NSUInteger idx = lowestSection; idx < 5; idx++) {
				[reloadSections addIndex:idx];
			}
			[tableView reloadSections:reloadSections withRowAnimation:UITableViewRowAnimationAutomatic];
		}
	}
}

#pragma mark - responders
- (IBAction)nextViewController:(id)sender {
	if (self.delegate) {
		[self.delegate nextView];
	}
}

- (IBAction)datePickerValueChanged:(UIDatePicker *)sender
{
	// Birthday
	self.userProfileMO.birthday = sender.date;

	[self updateView];
	[self.tableView reloadData];
}

- (IBAction)genderChanged:(UISegmentedControl *)sender
{
	self.userProfileMO.gender = (sender.selectedSegmentIndex ? @1 : @2);

	[self updateView];
	[self.tableView reloadData];
}

- (void)bmrTapped:(UITapGestureRecognizer *)recognizer
{
	UIViewController *bmrView = [self.storyboard instantiateViewControllerWithIdentifier:@"BMR Info Modal"];
	[self presentViewController:bmrView animated:YES completion:nil];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	NSInteger components = 1;

	switch (pickerView.tag) {
		case 1:
		case 2:
			components = 2;
			break;
		case 3:
			components = 1;
			break;
	}

	return components;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	NSInteger numberOfRows = 0;

	switch (pickerView.tag) {
		case 1:
			// Height
			if (component == 0) {
				numberOfRows = 5;
			}
			else {
				numberOfRows = 12;
			}
			break;
		case 2:
			// Weight
			if (component == 0) {
				numberOfRows = 450;
			}
			else {
				numberOfRows = 10;
			}
			break;
		case 3:
			// Goal Weight
			if (component == 0) {
				numberOfRows = 450;
			}
			break;
	}

	return numberOfRows;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *title = nil;

	switch (pickerView.tag) {
		case 1:
			// Height
			if (component == 0) {
				title = [NSString stringWithFormat:@"%ld feet", (long)row + 3];
			}
			else {
				title = [NSString stringWithFormat:@"%ld inches", (long)row];
			}
			break;
		case 2:
			// Weight
			if (component == 0) {
				title = [NSString stringWithFormat:@"%ld lbs", (long)row];
			}
			else {
				title = [NSString stringWithFormat:@".%ld", (long)row];
			}
			break;
		case 3:
			// Goal Weight
			if (component == 0) {
				title = [NSString stringWithFormat:@"%ld lbs", (long)row];
			}
			break;
	}

	return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	//NSArray *reloadIndexPaths = nil;
	switch (pickerView.tag) {
		case 1: {
			// Height
			NSInteger feet = [pickerView selectedRowInComponent:0] + 3;
			NSInteger inches = [pickerView selectedRowInComponent:1];
			self.userProfileMO.height = @(feet * 12 + inches);
			//reloadIndexPaths = @[[NSIndexPath indexPathForRow:kRowHeight inSection:kSectionBasics]];
			break;
		}
		case 2: {
			// Weight
			double lbs = [pickerView selectedRowInComponent:0];
			double tenths = (double)[pickerView selectedRowInComponent:1] / 10.0;
			self.currentWeight = @(lbs + tenths);
			self.weightUpdated = YES;
			//reloadIndexPaths = @[[NSIndexPath indexPathForRow:kRowWeight inSection:kSectionBasics]];
			break;
		}
		case 3: {
			// Goal Weight
			double lbs = [pickerView selectedRowInComponent:0];
			self.userProfileMO.goalWeight = @(lbs);
			//reloadIndexPaths = @[[NSIndexPath indexPathForRow:kRowGoalWeight inSection:kSectionGoalWeight]];
			break;
		}
	}

	//[self.tableView reloadRowsAtIndexPaths:reloadIndexPaths withRowAnimation:UITableViewRowAnimationNone];
	[self updateView];
	[self.tableView reloadData];
}

#pragma mark - local
- (void)updateView
{
	if ([self.currentWeight doubleValue] < [self.userProfileMO.goalWeight doubleValue]) {
		// Weight gain
		self.nutritionEffortCredit = self.userProfileMO.nutritionEffort;
	}
	else {
		// Weight loss (don't support 'maintain', so this is interpreted as loss)
		self.nutritionEffortCredit = @(-1 * [self.userProfileMO.nutritionEffort integerValue]);
	}

	// No point in attempting to perform BMR if we don't at least have the following things
	if (self.userProfileMO.height && self.userProfileMO.gender && self.userProfileMO.birthday && self.currentWeight) {

		NSCalendar *calendar = [NSCalendar currentCalendar];
		self.bmr = [BCUtilities calculateBmrForGender:[self.userProfileMO.gender integerValue]
			ageInYears:[calendar BC_yearsFromDate:self.userProfileMO.birthday toDate:[NSDate date]]
			weightInPounds:[self.currentWeight doubleValue]
			heightInInches:[self.userProfileMO.height doubleValue] usingEquation:kBMREquationMifflinStJeor];

		self.activityLevelCredit = @((int)([self.bmr doubleValue] * [self.userProfileMO.activityLevel doubleValue]
			- [self.bmr doubleValue] + 0.5));
		self.dailyCalories = @((int)([self.bmr doubleValue] * [self.userProfileMO.activityLevel doubleValue]
			+ [self.nutritionEffortCredit integerValue] + 0.5));
	}
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ActivityLevel"]) {
		BLOnboardActivitySelectionViewController *controller = [segue destinationViewController];
		controller.delegate = self;
		controller.items = self.activityLevels;
		controller.isActivityLevel = YES;
		controller.selectedRow = self.selectedActivityLevel;
	}
	else if ([segue.identifier isEqualToString:@"NutritionEffort"]) {
		BLOnboardActivitySelectionViewController *controller = [segue destinationViewController];
		controller.delegate = self;
		controller.items = self.nutritionEfforts;
		controller.isActivityLevel = NO;
		controller.selectedRow = self.selectedNutritionEffort;
	}
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	if (object && [object isKindOfClass:[NSDictionary class]]) {
		NSInteger idx = [self.activityLevels indexOfObject:object];
		if (idx != NSNotFound) {
			self.selectedActivityLevel = idx;
			self.userProfileMO.activityLevel = [object valueForKey:@"value"];
		}
		else {
			idx = [self.nutritionEfforts indexOfObject:object];
			if (idx != NSNotFound) {
				self.selectedNutritionEffort = idx;
				self.userProfileMO.nutritionEffort = [object valueForKey:@"value"];
			}
		}

		self.userProfileMO.status = kStatusPut;
		[self.managedObjectContext BL_save];

		[self updateView];
		[self.tableView reloadData];
	}
}

@end
