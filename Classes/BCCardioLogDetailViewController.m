//
//  BCCardioLogDetailViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/5/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCCardioLogDetailViewController.h"

#import "NSDate+Additions.h"
#import "NumberValue.h"
#import "TrackingMO.h"
#import "TrainingActivityMO.h"
#import "User.h"

@interface BCCardioLogDetailViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIPickerView *durationPicker;

@property (weak, nonatomic) UITextField *activeField;
@property (assign, nonatomic) BOOL durationPickerShown;
@property (strong, nonatomic) NSNumber *lastWeight;

- (void)keyboardWillShowNotification:(NSNotification*)aNotification;
- (void)keyboardWillHideNotification:(NSNotification*)aNotification;

@end

@implementation BCCardioLogDetailViewController

static const CGFloat kSectionHeaderHeight = 20.0;

static const NSInteger kSectionHeader = 0;
static const NSInteger kSectionData = 1;
static const NSInteger kSectionFooter = 2;
static const NSInteger kNumberOfSections = 3;

static const CGFloat kDataRowHeight = 44.0;
static const CGFloat kPickerRowHeight = 164.0;
static const CGFloat kHeaderRowHeight = 100.0;
static const CGFloat kFooterRowHeight = 90.0;

static const NSInteger kRowName = 0;
static const NSInteger kRowDuration = 1;
static const NSInteger kRowDurationPicker = 2;
static const NSInteger kRowDistance = 3;
static const NSInteger kRowWeight = 4;
static const NSInteger kRowCalories = 5;
static const NSInteger kNumberOfRows = 6;

static const NSInteger kHoursValueComponent	= 0;
static const NSInteger kHoursLabelComponent	= 1;
static const NSInteger kMinutesValueComponent = 2;
static const NSInteger kMinutesLabelComponent = 3;
static const NSInteger kSecondsValueComponent = 4;
static const NSInteger kSecondsLabelComponent = 5;
static const NSInteger kNumberOfComponents = 6;

static const NSInteger kNumberOfHourRows = 100;
static const NSInteger kNumberOfMinuteRows = 3000;
static const NSInteger kNumberOfSecondRows = 3000;

static const NSInteger kTagValueLabel = 1001;
static const NSInteger kTagTextField = 1002;
static const NSInteger kTagDurationPicker = 1003;

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

	self.lastWeight = [TrackingMO getCurrentWeightInMOC:self.activityLog.managedObjectContext];
}

- (void)viewWillAppear:(BOOL)animated
{
	// Register for notification when the keyboard will be shown
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardWillShowNotification:)
		name:UIKeyboardWillShowNotification
		object:nil];

	// Register for notification when the keyboard will be hidden
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardWillHideNotification:)
		name:UIKeyboardWillHideNotification
		object:nil];

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// remove all observers
	[[NSNotificationCenter defaultCenter] removeObserver:self];

    [super viewWillDisappear:animated];
}

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
	return UIBarPositionTopAttached;
}

#pragma mark - local
- (TrainingActivityMO *)activityLog
{
	if (_activityLog) {
		return _activityLog;
	}

	_activityLog = [TrainingActivityMO insertInManagedObjectContext:self.managedObjectContext];
	_activityLog.loginId = [[User currentUser] loginId];
	_activityLog.date = self.logDate;

	return _activityLog;
}

- (void)updateCalories
{
	double factor;
	if (!self.activityLog.factorValue) {
		factor = [TrainingActivityMO calculateFactorForTime:[self.activityLog.timeInMinutes floatValue]
			andDistance:[self.activityLog.distance doubleValue]];
	}
	else {
		factor = [self.activityLog.factor doubleValue];
	}

	self.activityLog.calories = @((NSInteger)([self.activityLog.timeInMinutes floatValue] * factor * [self.lastWeight doubleValue]));
}

- (void)updateTimeWithHours:(NSInteger)hours minutes:(NSInteger)minutes seconds:(NSInteger)seconds
{
	NSMutableArray *timeComponents = [NSMutableArray array];
	[timeComponents addObject:[NSString stringWithFormat:@"%2ld", (long)hours]];
	[timeComponents addObject:[NSString stringWithFormat:@"%2ld", (long)minutes]];
	[timeComponents addObject:[NSString stringWithFormat:@"%2ld", (long)seconds]];
	
	self.activityLog.time = [timeComponents componentsJoinedByString:@":"];

	[self updateCalories];
}

- (void)updateDistance:(NSNumber *)distance
{
	self.activityLog.distance = distance;

	[self updateCalories];
}

- (void)updateWeight:(NSNumber *)weight
{
	// Really nothing we can do if they've cleared this field, the user cannot save a null weight
	if (!weight) {
		return;
	}

	TrackingMO *newTrack = [TrackingMO insertInManagedObjectContext:self.activityLog.managedObjectContext];
	newTrack.loginId = [[User currentUser] loginId];
	newTrack.trackingType = @(TrackingTypes.weight);
	newTrack.status = kStatusPost;

    newTrack.valueDictionary = @{ kTrackingWeight : [weight stringValue] };
	if ([self.logDate BC_isSameDayAs:[NSDate date]]) {
		newTrack.date = self.logDate;
	}
	else {
		NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
		NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:self.logDate];

		components.hour = 12;
		components.minute = 0;
		components.second = 0;

		NSDate *logDateNoon = [calendar dateFromComponents:components];

		newTrack.date = logDateNoon;
	}

	self.lastWeight = weight;

	[self updateCalories];
}

- (void)showDurationPicker
{
	if (self.activeField) {
		[self.activeField resignFirstResponder];
	}

	self.tableView.scrollEnabled = NO;

	self.durationPickerShown = YES;
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowDurationPicker inSection:kSectionData]]
		withRowAnimation:UITableViewRowAnimationAutomatic];

	UITableViewCell *durationPickerCell = [self.tableView cellForRowAtIndexPath:
		[NSIndexPath indexPathForRow:kRowDurationPicker inSection:kSectionData]];
	UIPickerView *aPickerView = (UIPickerView *)[durationPickerCell viewWithTag:kTagDurationPicker];

	NSDictionary *timeInfo = [self.activityLog timeInfo];
	[aPickerView selectRow:[timeInfo[kTimeInfoHours] integerValue] inComponent:kHoursValueComponent animated:NO];
	[aPickerView selectRow:[timeInfo[kTimeInfoMinutes] integerValue] inComponent:kMinutesValueComponent animated:NO];
	[aPickerView selectRow:[timeInfo[kTimeInfoSeconds] integerValue] inComponent:kSecondsValueComponent animated:NO];
}

- (void)hideDurationPicker
{
	self.tableView.scrollEnabled = YES;

	self.durationPickerShown = NO;
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowDurationPicker inSection:kSectionData]]
		withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return kNumberOfComponents;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	NSInteger numberOfRows = 0;

	switch (component) {
		case kHoursValueComponent:
			numberOfRows = kNumberOfHourRows;
			break;
		case kHoursLabelComponent:
			numberOfRows = 1;
			break;
		case kMinutesValueComponent:
			numberOfRows = kNumberOfMinuteRows;
			break;
		case kMinutesLabelComponent:
			numberOfRows = 1;
			break;
		case kSecondsValueComponent:
			numberOfRows = kNumberOfSecondRows;
			break;
		case kSecondsLabelComponent:
			numberOfRows = 1;
			break;
	}

	return numberOfRows;
}

#pragma mark - UIPickerViewDelegate
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)reusedView
{
	UILabel* label = (UILabel*)reusedView;
	if (!label && (component == kHoursValueComponent || component == kMinutesValueComponent || component == kSecondsValueComponent)) {
		label = [[UILabel alloc] init];
		[label setFont:[UIFont systemFontOfSize:20]];
		[label setTextAlignment:NSTextAlignmentRight];

		[label setText:[self pickerView:pickerView titleForRow:row forComponent:component]];
	}

	return label;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *title = nil;

	if (component == kHoursValueComponent) {
		title = [NSString stringWithFormat:@"%li", (long)row];
	}
	else if (component == kMinutesValueComponent) {
		title = [NSString stringWithFormat:@"%li", (long)row % 60];
	}
	else if (component == kSecondsValueComponent) {
		title = [NSString stringWithFormat:@"%li", (long)row % 60];
	}

	return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	// when the spinner comes to a stop, center it again
	if (component == kMinutesValueComponent) {
		[pickerView selectRow:((kNumberOfMinuteRows / 2) + (row % 60)) inComponent:kMinutesValueComponent animated:NO];
	}
	else if (component == kSecondsValueComponent) {
		[pickerView selectRow:((kNumberOfSecondRows / 2) + (row % 60)) inComponent:kSecondsValueComponent animated:NO];
	}

	// calculate the selection for each segment and make the time string
	NSInteger hours = [pickerView selectedRowInComponent:kHoursValueComponent];
	NSInteger minutes = ([pickerView selectedRowInComponent:kMinutesValueComponent] % 60);
	NSInteger seconds = ([pickerView selectedRowInComponent:kSecondsValueComponent] % 60);
	[self updateTimeWithHours:hours minutes:minutes seconds:seconds];

	// update the duration cell
	NSIndexPath *durationIndexPath = [NSIndexPath indexPathForRow:kRowDuration inSection:kSectionData];
	[self configureCell:[self.tableView cellForRowAtIndexPath:durationIndexPath] atIndexPath:durationIndexPath];

	// this affects the calculated calories, so update that cell too
	NSIndexPath *caloriesIndexPath = [NSIndexPath indexPathForRow:kRowCalories inSection:kSectionData];
	[self configureCell:[self.tableView cellForRowAtIndexPath:caloriesIndexPath] atIndexPath:caloriesIndexPath];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return kNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	switch (section) {
		case kSectionHeader:
		case kSectionFooter:
		{
			numberOfRows = 1;
			break;
		}
		case kSectionData:
		{
			numberOfRows = kNumberOfRows;
			break;
		}
		default:
			break;
	}

	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *headerCellId = @"HeaderCell";
	static NSString *nameCellId = @"NameCell";
	static NSString *durationCellId = @"DurationCell";
	static NSString *durationPickerCellId = @"DurationPickerCell";
	static NSString *distanceCellId = @"DistanceCell";
	static NSString *caloriesCellId = @"CaloriesCell";
	static NSString *weightCellId = @"WeightCell";
	static NSString *footerCellId = @"FooterCell";

	UITableViewCell *cell = nil;

	switch (indexPath.section) {
		case kSectionHeader:
		{
			cell = [self.tableView dequeueReusableCellWithIdentifier:headerCellId];
			break;
		}
		default:
		case kSectionData:
		{
			if (indexPath.row == kRowName) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:nameCellId];
			}
			else if (indexPath.row == kRowDurationPicker) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:durationPickerCellId];
			}
			else if (indexPath.row == kRowDuration) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:durationCellId];
			}
			else if (indexPath.row == kRowDistance) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:distanceCellId];
			}
			else if (indexPath.row == kRowCalories) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:caloriesCellId];
			}
			else if (indexPath.row == kRowWeight) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:weightCellId];
			}
			break;
		}
		case kSectionFooter:
		{
			cell = [self.tableView dequeueReusableCellWithIdentifier:footerCellId];
			break;
		}
	}

	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionData) {
		UITextField *textField = (UITextField *)[cell viewWithTag:kTagTextField];
		switch (indexPath.row) {
			case kRowName:
			{
				textField.text = self.activityLog.activity;
				break;
			}
			case kRowDuration:
			{
				UILabel *valueLabel = (UILabel *)[cell viewWithTag:kTagValueLabel];
				valueLabel.text = [self.activityLog timeString];
				break;
			}
			case kRowDistance:
			{
				textField.text = [self.activityLog.distance stringValue];
				break;
			}
			case kRowCalories:
			{
				textField.text = [NSString stringWithFormat:@"%d", self.activityLog.caloriesValue];
				break;
			}
			case kRowWeight:
			{
				textField.text = [self.lastWeight stringValue];
				break;
			}
		}
	}
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat headerHeight = 0.0f;
	if (section == kSectionData) {
		headerHeight = kSectionHeaderHeight;
	}
	return headerHeight;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString *headerText = nil;
	if (section == kSectionData) {
		headerText = @"ACTIVITY";
	}
	return headerText;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44.0f;
	switch (indexPath.section) {
		case kSectionHeader:
		{
			rowHeight = kHeaderRowHeight;
			break;
		}
		case kSectionFooter:
		{
			rowHeight = kFooterRowHeight;
			break;
		}
		default:
		case kSectionData:
		{
			if (indexPath.row == kRowDurationPicker) {
				rowHeight = self.durationPickerShown ? kPickerRowHeight : 0.0f;
			}
			else {
				rowHeight = kDataRowHeight;
			}
			break;
		}
	}
	return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionData) {
		switch (indexPath.row) {
			case kRowName:
			case kRowDistance:
			case kRowWeight:
			case kRowCalories: {
				if (self.durationPickerShown) {
					[self hideDurationPicker];
				}
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                UITextField *textField = (UITextField *)[cell viewWithTag:kTagTextField];
                [textField becomeFirstResponder];
                break;
			}
			case kRowDuration: {
				if (self.durationPickerShown) {
					[self hideDurationPicker];
				}
				else {
					[self showDurationPicker];
				}
				break;
			}
		}
	}
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	CGPoint textFieldPosition = [textField convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath* indexPath = [self.tableView indexPathForRowAtPoint:textFieldPosition];

	[self saveValueFromTextField:textField atIndexPath:indexPath];

	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	if (self.durationPickerShown) {
		[self hideDurationPicker];
	}
	self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	self.activeField = nil;
	CGPoint textFieldPosition = [textField convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath* indexPath = [self.tableView indexPathForRowAtPoint:textFieldPosition];

	[self saveValueFromTextField:textField atIndexPath:indexPath];
}

- (void)finishEditing
{
	if (self.activeField) {
		[self textFieldDidEndEditing:self.activeField];
	}
}

- (void)saveValueFromTextField:(UITextField *)textField atIndexPath:(NSIndexPath *)indexPath
{
	switch (indexPath.row) {
		case kRowName:
		{
			self.activityLog.activity = textField.text;
			break;
		}
		case kRowDistance:
		{
			[self updateDistance:[textField.text numberValueDecimal]];
			break;
		}
		case kRowCalories:
		{
			self.activityLog.calories = [textField.text numberValueDecimal];
			break;
		}
		case kRowWeight:
		{
			[self updateWeight:[textField.text numberValueDecimal]];
			break;
		}
	}

	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];

	NSIndexPath *caloriesIndexPath = [NSIndexPath indexPathForRow:kRowCalories inSection:kSectionData];
	[self configureCell:[self.tableView cellForRowAtIndexPath:caloriesIndexPath] atIndexPath:caloriesIndexPath];
}

#pragma mark - Notifications for keyboard hide/show
- (void)keyboardWillShowNotification:(NSNotification*)aNotification
{
	NSDictionary *keyboardAnimationDetail = [aNotification userInfo];

	CGRect keyboardEndFrameWindow = [keyboardAnimationDetail[UIKeyboardFrameEndUserInfoKey] CGRectValue];

	double keyboardTransitionDuration  = [keyboardAnimationDetail[UIKeyboardAnimationDurationUserInfoKey] doubleValue];

	CGRect keyboardEndFrameView = [self.view convertRect:keyboardEndFrameWindow fromView:nil];

	[UIView animateWithDuration:keyboardTransitionDuration 
		animations:^{
			self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, 0, self.view.frame.size.height - keyboardEndFrameView.origin.y, 0);
			self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(self.tableView.scrollIndicatorInsets.top, 0, self.view.frame.size.height - keyboardEndFrameView.origin.y, 0);
			[self.view layoutIfNeeded];
		}];
}

- (void)keyboardWillHideNotification:(NSNotification*)aNotification
{
	NSDictionary *keyboardAnimationDetail = [aNotification userInfo];

	double keyboardTransitionDuration  = [keyboardAnimationDetail[UIKeyboardAnimationDurationUserInfoKey] doubleValue];

	[UIView animateWithDuration:keyboardTransitionDuration 
		animations:^{
			self.tableView.contentInset = UIEdgeInsetsZero;
			self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
			[self.view layoutIfNeeded];
		}];
}   

@end
