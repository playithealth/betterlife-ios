//
//  QuantityActionSheetViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/22/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "QuantityActionSheetViewController.h"

@interface QuantityActionSheetViewController ()
- (void) slideIn;
@end

@implementation QuantityActionSheetViewController
@synthesize actionSheetView;
@synthesize quantityField;

- (IBAction)numberClicked:(id)sender {
	NSUInteger tag = [sender tag];
	quantityField.text = [quantityField.text stringByAppendingFormat:@"%lu", (unsigned long)tag];
}
- (IBAction)decimalClicked {
	quantityField.text = [quantityField.text stringByAppendingString:@"."];
}
- (IBAction)backspaceClicked {
	NSUInteger length = [quantityField.text length];
	if (length > 0) {
		quantityField.text = [quantityField.text substringToIndex:[quantityField.text length] - 1];
	}
}
- (void)save {
	/*
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterDecimalStyle];
	NSNumber *quantity = [formatter numberFromString:quantityField.text];
	[formatter release];
	
	[shoppingListItem setObject:quantity forKey:kItemQuantity];
	[parentView.tableView reloadData];
	*/
}
- (IBAction)doneClicked {
	[self save];

	[self.navigationController popViewControllerAnimated:YES];

	[self slideOut];
}

#pragma mark -
#pragma mark animation in/out
- (IBAction)slideOut {
    [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
        
    // Set delegate and selector to remove from superview when animation completes
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];

	//self.view.alpha = 0.0;
        
    // Move this view to bottom of superview
    CGRect frame = self.actionSheetView.frame;
    frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
    self.actionSheetView.frame = frame;
        
    [UIView commitAnimations]; 
}
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if ([animationID isEqualToString:@"removeFromSuperviewWithAnimation"]) {
        [self.view removeFromSuperview];
    }
}
- (void)slideIn {
    //set initial location at bottom of view
    CGRect frame = self.actionSheetView.frame;
    frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
    self.actionSheetView.frame = frame;
    [self.view addSubview:self.actionSheetView];

    //animate to new location, determined by height of the view in the NIB
    [UIView beginAnimations:@"presentWithSuperview" context:nil];
    frame.origin = CGPointMake(0.0, 
        self.view.bounds.size.height - self.actionSheetView.bounds.size.height);
        
	//self.view.alpha = 1.0;
        
    self.actionSheetView.frame = frame;
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark lifecycle
- (void)viewWillAppear:(BOOL)animated {
	NSNumber *quantity = [NSNumber numberWithInteger:1];
	quantityField.text = [quantity description];

	//self.view.alpha = 0.0;
	[self slideIn];

	[super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.actionSheetView = nil;
}

@end
