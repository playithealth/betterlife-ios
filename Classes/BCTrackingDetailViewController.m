//
//  BCTrackingDetailViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 2/28/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCTrackingDetailViewController.h"

#import "BCDashboardViewController.h"
#import "BCDrawBlockView.h"
#import "BLAlertController.h"
#import "BLTrackingAddViewController.h"
#import "NSCalendar+Additions.h"
#import "NSDate+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NumberValue.h"
#import "TrackingMO.h"
#import "TrackingTypeMO.h"
#import "TrackingValueMO.h"
#import "UIColor+Additions.h"
#import "User.h"

@interface BCTrackingDetailViewController ()
@property (assign, nonatomic) NSInteger trackType; ///< This is used for reloading if sync changes the TrackingTypeMO
@property (strong, nonatomic) NSArray *trackingValues; ///< Filtered and sorted tracking values for the current tracking type
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (weak, nonatomic) IBOutlet BCDrawBlockView *graphView;
@property (weak, nonatomic) IBOutlet UILabel *noEntriesLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *scopeControl;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSArray *trackingLogs;
@property (strong, nonatomic) NSCalendar *calendar;
@property (assign, nonatomic) NSUInteger customScopeValue;

- (IBAction)scopeChanged:(id)sender;
@end

@implementation BCTrackingDetailViewController

enum { ScopeWeek = 0, ScopeMonth, ScopeYear, ScopeCustom, ScopeConfigure };
enum { SectionLegend = 0, SectionData };

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:(NSCoder *)aDecoder];
	if (self) {
		self.scopeIndex = ScopeWeek;

		self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

		self.dateFormatter = [[NSDateFormatter alloc] init];
		[self.dateFormatter setTimeStyle:NSDateFormatterShortStyle];
		[self.dateFormatter setDateStyle:NSDateFormatterShortStyle];
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];

	self.trackType = self.trackingTypeMO.cloudIdValue;

	// Get an array of tracking values that are not optional, sorted by their field names, track_value1, track_value2, etc...
	self.trackingValues = [[self.trackingTypeMO.trackingValues filteredSetUsingPredicate:
		[NSPredicate predicateWithFormat:@"%K = NO", TrackingValueMOAttributes.optional]]
		sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:TrackingValueMOAttributes.field ascending:YES]]];

	if ([self.trackingTypeMO.readOnly boolValue]) {
		self.navigationItem.rightBarButtonItem = nil;
	}
	//self.navigationItem.rightBarButtonItem = ([self.trackingTypeMO.readOnly boolValue] ? nil
	//	: [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonTapped:)]);

	// Make sure that if this TrackingTypeMO gets updated, we reload to avoid problems
	__typeof__(self) __weak weakSelf = self;
	[[NSNotificationCenter defaultCenter] addObserverForName:[TrackingTypeMO entityName] object:nil queue:nil
		usingBlock:^(NSNotification *notification) {
			weakSelf.trackingTypeMO = [TrackingTypeMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@",
					TrackingTypeMOAttributes.loginId, [User loginId], TrackingTypeMOAttributes.cloudId, @(self.trackType)]
				inContext:self.managedObjectContext];

			// Get an array of tracking values that are not optional, sorted by their field names, track_value1, track_value2, etc...
			weakSelf.trackingValues = [[self.trackingTypeMO.trackingValues filteredSetUsingPredicate:
				[NSPredicate predicateWithFormat:@"%K = NO", TrackingValueMOAttributes.optional]]
				sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:TrackingValueMOAttributes.field ascending:YES]]];

			[weakSelf.tableView reloadData];
			[weakSelf configureView];
		}];

	self.customScopeValue = [[User currentUser].userProfile.customTrackingTimeframe integerValue];
	[self.scopeControl setTitle:[NSString stringWithFormat:@"%lu days", (unsigned long)self.customScopeValue] forSegmentAtIndex:ScopeCustom];
	[self.scopeControl setSelectedSegmentIndex:self.scopeIndex];

	[self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	[super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AddTracking"]) {
		BLTrackingAddViewController *addView = segue.destinationViewController;
		addView.managedObjectContext = self.managedObjectContext;
		addView.trackingTypeMO = self.trackingTypeMO;
	}
}

#pragma mark - local
- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == SectionLegend) {
		for (int idx = 0; idx < 6; idx++) {
			UILabel *legendLabel = (UILabel *)[cell.contentView viewWithTag:idx + 1];
			NSString *text = @"";
			if (idx < [self.trackingValues count]) {
				TrackingValueMO *trackingValue = [self.trackingValues objectAtIndex:idx];
				text = trackingValue.title;
			}
			legendLabel.text = text;
		}
	}
	else {
		TrackingMO *track = [self.trackingLogs objectAtIndex:indexPath.row];
		NSDictionary *trackDict = track.valueDictionary;

		UILabel *valueLabel = (UILabel *)[cell.contentView viewWithTag:1];
		UILabel *unitsLabel = (UILabel *)[cell.contentView viewWithTag:2];
		UILabel *dateLabel = (UILabel *)[cell.contentView viewWithTag:3];

		dateLabel.text = [self.dateFormatter stringFromDate:track.date];

		// Using sameUnits and priorUnits, try to determine if all the units for this tracking type are the same, if so, just show
		// the units once in the label, otherwise, show each value's units, separated by a separator
		BOOL sameUnits = YES;
		NSString *priorUnits = [[self.trackingValues firstObject] unitAbbrev];
		for (TrackingValueMO *trackingValue in self.trackingValues) {

			if (sameUnits && (!priorUnits || ![trackingValue.unitAbbrev isEqualToString:priorUnits])) {
				sameUnits = NO;
			}
		}

		if (sameUnits) {
			unitsLabel.text = priorUnits;
		}
		else {
			unitsLabel.text = [[self.trackingValues valueForKeyPath:TrackingValueMOAttributes.unitAbbrev] componentsJoinedByString:@"/"];
		}

		unitsLabel.text = [self.trackingTypeMO unitString];
		valueLabel.text = [self.trackingTypeMO valueStringForTrackingValueDictionary:trackDict];
	}
}

- (IBAction)scopeChanged:(id)sender
{
	NSUInteger scopeIndex = [sender selectedSegmentIndex];
	if (scopeIndex == ScopeConfigure) {
		BLAlertController* alert = [BLAlertController alertControllerWithTitle:@"Custom range" message:@"Please enter a range, in days"
			preferredStyle:UIAlertControllerStyleAlert];

		[alert addAction:[BLAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
			handler:^(UIAlertAction * action) {
				// Re-select the prior scope index, since they canceled
				[self.scopeControl setSelectedSegmentIndex:self.scopeIndex];
			}]];

		[alert addAction:[BLAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
			handler:^(UIAlertAction * action) {
				NSNumber *value = [[[alert.textFields objectAtIndex:0] text] numberValueDecimal];
				self.customScopeValue = MIN([value integerValue], 365);
				UserProfileMO *userProfile = [User currentUser].userProfile;
				userProfile.customTrackingTimeframe = @(self.customScopeValue);
				userProfile.status = kStatusPut;
				[userProfile.managedObjectContext BL_save];
				// Post a notification that we've updated the user profile
				[[NSNotificationCenter defaultCenter] postNotificationName:[UserProfileMO entityName]
					object:self userInfo:nil];
				self.scopeIndex = ScopeCustom;
				[self.scopeControl setTitle:[NSString stringWithFormat:@"%lu days", (long)self.customScopeValue]
					forSegmentAtIndex:ScopeCustom];
				[self reloadTracking];

				[self.scopeControl setSelectedSegmentIndex:self.scopeIndex];
			}]];

		[alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
			textField.text = [NSString stringWithFormat:@"%lu", (long)self.customScopeValue];
			textField.placeholder = @"days";
			textField.keyboardType = UIKeyboardTypeNumberPad;
		}];

		[alert presentInViewController:self animated:YES completion:nil];
	}
	else {
		self.scopeIndex = scopeIndex;
		[self reloadTracking];
	}
}

- (void)reloadTracking
{
	self.trackingLogs = nil;
	[self.tableView reloadData];

	[self configureView];
}

- (void)configureView
{
	BOOL hasLogs = [self.trackingLogs count];
	self.noEntriesLabel.hidden = hasLogs;
	self.graphView.hidden = !hasLogs;
	self.tableView.hidden = !hasLogs;

	if ([self.trackingTypeMO.readOnly boolValue]) {
		self.noEntriesLabel.text = @"You have no information yet for this screen. "
			"Over time data from your log or fitness device will fill in this screen.";
	}
	else {
		self.noEntriesLabel.text = @"You haven't added any data for this period.";
	}

	[self buildGraph];
	[self.graphView setNeedsDisplay];
}

- (NSArray *)oneLogPerDay:(NSArray *)logs
{
	NSMutableArray *filteredLogs = [NSMutableArray array];
	NSInteger dayIndex = -1;
	for (TrackingMO *log in logs) {
		NSInteger days = [self.calendar BC_daysFromDate:log.date toDate:[NSDate date]];
		if (days > dayIndex) {
			dayIndex = days;

			NSMutableDictionary *dailyDict = [log.valueDictionary mutableCopy];
            if (dailyDict) {
                [dailyDict setObject:@(dayIndex) forKey:@"dayOffset"];
                [filteredLogs addObject:dailyDict];
            }
        }
	}
	return filteredLogs;
}

- (NSArray *)oneLogPerMonth:(NSArray *)logs
{
	NSMutableArray *filteredLogs = [NSMutableArray array];
	NSDateComponents *logDateComponents = [self.calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit)
		fromDate:[[logs firstObject] date]];
	NSInteger monthIndex = logDateComponents.month;
	NSInteger yearIndex = logDateComponents.year;
	NSDate *iterDate;
	// Need to get the average of each value per month
	NSMutableArray *monthArray = [NSMutableArray array];
	for (TrackingMO *log in logs) {
		logDateComponents = [self.calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:log.date];
		if (logDateComponents.month != monthIndex || logDateComponents.year != yearIndex) {
			// Tally up the prior month and move to the next month
			monthIndex = logDateComponents.month;
			yearIndex = logDateComponents.year;

			// @avg on monthArray
			NSMutableDictionary *valueAveragesDict = [NSMutableDictionary dictionary];
			for (TrackingValueMO *trackingValueMO in self.trackingTypeMO.trackingValues) {
				NSNumber *averageValue = [monthArray valueForKeyPath:[NSString stringWithFormat:@"@avg.%@", trackingValueMO.field]];
				if (averageValue) {
					[valueAveragesDict setObject:averageValue forKey:trackingValueMO.field];
				}
			}
			[valueAveragesDict setObject:@([self.calendar BC_monthsFromDate:iterDate toDate:[NSDate date]]) forKey:@"dayOffset"];
			[filteredLogs addObject:valueAveragesDict];

			// Reset monthArray
			monthArray = [NSMutableArray array];
		}

		// Add this element to the monthArray
		iterDate = log.date;
		NSDictionary *valueDictionary = log.valueDictionary;
		if (valueDictionary) {
			[monthArray addObject:valueDictionary];
		}
	}

	if ([monthArray count]) {
		// @avg on monthArray
		NSMutableDictionary *valueAveragesDict = [NSMutableDictionary dictionary];
		for (TrackingValueMO *trackingValueMO in self.trackingTypeMO.trackingValues) {
			NSNumber *averageValue = [monthArray valueForKeyPath:[NSString stringWithFormat:@"@avg.%@", trackingValueMO.field]];
			if (averageValue) {
				[valueAveragesDict setObject:averageValue forKey:trackingValueMO.field];
			}
		}
		[valueAveragesDict setObject:@([self.calendar BC_monthsFromDate:iterDate toDate:[NSDate date]]) forKey:@"dayOffset"];
		[filteredLogs addObject:valueAveragesDict];
	}

	return filteredLogs;
}

- (void)buildGraph
{
	NSArray *trackingLogsByDay = nil;

	NSDate *leftDate = [NSDate date];
	NSDate *middleDate = [NSDate date];
	NSInteger maxPointsToShow = 1;
	switch (self.scopeIndex) {
		case ScopeWeek:
			leftDate = [self.calendar BC_dateByAddingDays:-6 toDate:leftDate];
			middleDate = [self.calendar BC_dateByAddingDays:3 toDate:leftDate];
			maxPointsToShow = 7;
			break;
		case ScopeMonth:
			leftDate = [self.calendar BC_dateByAddingDays:-29 toDate:leftDate];
			middleDate = [self.calendar BC_dateByAddingDays:15 toDate:leftDate];
			maxPointsToShow = 30;
			break;
		case ScopeYear:
			leftDate = [self.calendar BC_dateByAddingMonths:-11 toDate:leftDate];
			middleDate = [self.calendar BC_dateByAddingMonths:5 toDate:leftDate];
			maxPointsToShow = 12;
			break;
		case ScopeCustom:
			leftDate = [self.calendar BC_dateByAddingDays:-1 * (self.customScopeValue - 1) toDate:leftDate];
			middleDate = [self.calendar BC_dateByAddingDays:(NSUInteger)(self.customScopeValue / 2) toDate:leftDate];
			maxPointsToShow = self.customScopeValue;
			break;
	}

	double circleRadius = 4.0;
	double circleLineWidth = 1.5;
	double graphWidth = 320;
	double graphLeftPad = 40;
	NSInteger graphHeight = 190;
	NSInteger topOffset = graphHeight / 7;
	NSInteger maxGraphHeight = graphHeight * 5 / 7;

	// Total width / gaps between points
	double xStep = (graphWidth - graphLeftPad - ((2.0 * circleRadius) + (2.0 * circleLineWidth))) / (maxPointsToShow - 1);

	double maxValue = -DBL_MAX;
	double minValue = DBL_MAX;

	NSMutableDictionary *graphData = [NSMutableDictionary dictionary];

	if ([self.trackingLogs count]) {
		switch (self.scopeIndex) {
			case ScopeWeek:
				trackingLogsByDay = [self oneLogPerDay:self.trackingLogs];
				break;
			case ScopeMonth:
				trackingLogsByDay = [self oneLogPerDay:self.trackingLogs];
				break;
			case ScopeYear:
				trackingLogsByDay = [self oneLogPerMonth:self.trackingLogs];
				break;
			case ScopeCustom:
				trackingLogsByDay = [self oneLogPerDay:self.trackingLogs];
				break;
		}

		NSMutableArray *dataSets = [NSMutableArray array];

		// First things first, we need to know what the min and max are for this particular tracking value field, so that
		// we have the proper screen ratio for calculating the data points for each data element, thus we end up looping twice
		for (TrackingValueMO *trackingValue in self.trackingValues) {
			for (NSDictionary *valuesDict in trackingLogsByDay) {
				NSNumber *value = [valuesDict BC_numberOrNilForKey:trackingValue.field];
				if (!value) {
					// Don't factor any entry that lacks this data point
					continue;
				}
				double dblValue = [value doubleValue];
				minValue = (dblValue < minValue ? dblValue : minValue);
				maxValue = (dblValue > maxValue ? dblValue : maxValue);
			}
			minValue = (minValue == DBL_MAX ? 0 : minValue);
			maxValue = (maxValue == -DBL_MAX ? 0 : maxValue);

		}

		double range = maxValue - minValue;

		// Special code for weight
		if ((self.trackingTypeMO.cloudIdValue == BCTrackingTypeWeight) && [[[User currentUser] userProfile] goalWeight]) {
			// add a line for the goal weight
			double goalWeight = [[[[User currentUser] userProfile] goalWeight] doubleValue];
			// Goal weight needs to be factored into the min and/or max values so that it shows up
			maxValue = (goalWeight > maxValue ? goalWeight : maxValue);
			minValue = (goalWeight < minValue ? goalWeight : minValue);
			// Alter the range as well
			range = maxValue - minValue;
			double maxGoalY = maxGraphHeight * (maxValue - goalWeight) / range + topOffset;
			[graphData setValue:[NSNumber numberWithDouble:maxGoalY] forKey:@"maxGoalY"];
		}

		for (TrackingValueMO *trackingValue in self.trackingValues) {
			NSMutableArray *graphDataPoints = [NSMutableArray array];

			for (NSDictionary *valuesDict in trackingLogsByDay) {
				NSNumber *day = [valuesDict objectForKey:@"dayOffset"];
				NSNumber *value = [valuesDict BC_numberOrNilForKey:trackingValue.field];
				if (!value) {
					// Don't add a point for any day that didn't have a data value for it
					continue;
				}

				NSNumber *numberY;
				if (range == 0) {
					// No range, all points are on the same y coordinate, so just center the points in the graph
					numberY = @(maxGraphHeight / 2 + topOffset);
				}
				else {
					numberY = @(maxGraphHeight * (maxValue - [value doubleValue]) / range + topOffset);
				}
				[graphDataPoints addObject:@{
					@"x" : @(graphWidth - ([day integerValue] * xStep + (circleRadius + circleLineWidth))),
					@"y" : numberY }];
			}

			[dataSets addObject:graphDataPoints];
		}
		[graphData setObject:dataSets forKey:@"points"];

	}

	self.graphView.drawBlock = ^(UIView* v, CGContextRef context) {
		/*
		 * draw background gradient
		 */
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGFloat gradientColors[] = { 
			246/255.0, 249/255.0, 253/255.0, 1.0, 
			232/255.0, 240/255.0, 251/255.0, 1.0
		};
		CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, gradientColors, NULL, 2);
		CGColorSpaceRelease(colorSpace);
		colorSpace = NULL;
		CGPoint startPoint = CGPointMake(CGRectGetMidX(self.graphView.frame), CGRectGetMinY(self.graphView.frame));
		CGPoint endPoint = CGPointMake(CGRectGetMidX(self.graphView.frame), CGRectGetMaxY(self.graphView.frame));
		CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
		CGGradientRelease(gradient);
		gradient = NULL;

		/* 
		 * Draw the line at the bottom per Eric's request
		 */
		CGContextSetLineWidth(context, 1.0);
		CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
		CGContextBeginPath(context);
		CGContextMoveToPoint(context, CGRectGetMinX(self.graphView.frame), CGRectGetMaxY(self.graphView.frame));
		CGContextAddLineToPoint(context, CGRectGetMaxX(self.graphView.frame), CGRectGetMaxY(self.graphView.frame));
		CGContextDrawPath(context, kCGPathStroke);

		/*
		 * draw horizontal lines
		 */
		CGContextSetLineWidth(context, 1.0);
		CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
		for (NSInteger lineIndex = 1; lineIndex < 7; lineIndex++) {
			CGContextBeginPath(context);
			CGPoint firstPoint = CGPointMake(35, graphHeight / 7 * lineIndex);
			CGPoint lastPoint = CGPointMake(graphWidth, graphHeight / 7 * lineIndex);
			CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
			CGContextAddLineToPoint(context, lastPoint.x, lastPoint.y);
			CGContextDrawPath(context, kCGPathStroke);
		}

		/* 
		 * draw y axis labels
		 */
		CGContextSelectFont(context, "Helvetica Neue Bold", 10, kCGEncodingMacRoman);
		CGContextSetTextDrawingMode(context, kCGTextFill);
		CGContextSetFillColorWithColor(context, [[UIColor darkGrayColor] CGColor]);
		CGContextSetTextMatrix(context, CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0));
		UIFont *labelFont = [UIFont boldSystemFontOfSize:10];
		NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
		[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[numberFormatter setMinimumFractionDigits:0];
		[numberFormatter setMaximumFractionDigits:1];

		NSNumber *theNumber = nil;
		NSString *theText =  nil;
		CGSize labelSize;
		for (NSInteger textIndex = 0; textIndex < 6; textIndex++) {
			if (minValue == maxValue) {
				theNumber = @(maxValue * 0.95f + (maxValue * 0.02f * textIndex));
			}
			else {
				theNumber = @(maxValue + (minValue - maxValue) / 5 * textIndex);
			}
			theText = [numberFormatter stringFromNumber:theNumber];
			labelSize = [theText sizeWithFont:labelFont];
			CGContextShowTextAtPoint(context, 6, (graphHeight / 7 * (textIndex + 1)) + (labelSize.height / 3),
				[theText cStringUsingEncoding:NSUTF8StringEncoding], [theText length]);
		}

		/*
		 * draw x axis labels
		 */
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[dateFormatter setDateFormat:@"EEE, MM/dd/yyyy"];
		theText = [dateFormatter stringFromDate:leftDate];
		labelSize = [theText sizeWithFont:labelFont];
		CGContextShowTextAtPoint(context, 40, graphHeight - labelSize.height, [theText cStringUsingEncoding:NSUTF8StringEncoding],
			[theText length]);
		theText = [dateFormatter stringFromDate:middleDate];
		labelSize = [theText sizeWithFont:labelFont];
		CGContextShowTextAtPoint(context, graphWidth / 2 - labelSize.width / 2 + 20, graphHeight - labelSize.height,
			[theText cStringUsingEncoding:NSUTF8StringEncoding], [theText length]);
		theText = [dateFormatter stringFromDate:[NSDate date]];
		labelSize = [theText sizeWithFont:labelFont];
		CGContextShowTextAtPoint(context, graphWidth - (labelSize.width + 4), graphHeight - labelSize.height,
			[theText cStringUsingEncoding:NSUTF8StringEncoding], [theText length]);

		// Clip the line drawing so that it remains inside the chart area, and not over onto the axis labels, etc
		CGContextClipToRect(context, CGRectMake(35, 0, graphWidth - 35, graphHeight));

		NSArray *dataSets = [graphData objectForKey:@"points"];
		for (NSInteger setIndex = 0; setIndex < [dataSets count]; setIndex++) {
			NSArray *dataPoints = [dataSets objectAtIndex:setIndex];

			// If, somehow, we get here with no data points, then do not continue, as we can't do anything productive.
			// Should not happen, defensive coding
			if (![dataPoints count]) {
				continue;
			}

			CGPoint firstPoint = CGPointMake([[[dataPoints objectAtIndex:0] valueForKey:@"x"] floatValue],
				[[[dataPoints objectAtIndex:0] valueForKey:@"y"] floatValue]);

			/* 
			 * fill below the line
			 */
			/* NOTE: We're no longer filling below the line due to the potential for multiple values, and them obscuring them
			CGPoint lastPoint = CGPointMake([[[dataPoints lastObject] valueForKey:@"x"] floatValue],
				[[[dataPoints lastObject] valueForKey:@"y"] floatValue]);
			CGContextSetFillColorWithColor(context, [[[[BCDashboardViewController graphColors] objectAtIndex:setIndex]
				colorWithAlphaComponent:0.5] CGColor]);
			CGContextBeginPath(context);
			// bottom right corner of fill
			CGContextMoveToPoint(context, firstPoint.x, maxGraphHeight + topOffset);
			CGContextAddLineToPoint(context, firstPoint.x, firstPoint.y);
			// loop thru all the other points
			for (int pointIndex = 1; pointIndex < [dataPoints count]; pointIndex++) {
				CGPoint destinationPoint = CGPointMake([[[dataPoints objectAtIndex:pointIndex] valueForKey:@"x"] floatValue],
					[[[dataPoints objectAtIndex:pointIndex] valueForKey:@"y"] floatValue]);
				CGPoint previousPoint = CGPointMake([[[dataPoints objectAtIndex:pointIndex - 1] valueForKey:@"x"] floatValue],
					[[[dataPoints objectAtIndex:pointIndex - 1] valueForKey:@"y"] floatValue]);
				if (previousPoint.y == destinationPoint.y) {
					CGContextAddLineToPoint(context, destinationPoint.x, destinationPoint.y);
				}
				else {
					CGPoint controlPoint1 = CGPointMake(previousPoint.x - xStep * 0.75, previousPoint.y);
					CGPoint controlPoint2 = CGPointMake(destinationPoint.x + xStep * 0.75, destinationPoint.y);
					CGContextAddCurveToPoint(context, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y,
						destinationPoint.x, destinationPoint.y);
				}
			}
			// bottom right corner of fill
			CGContextAddLineToPoint(context, lastPoint.x, maxGraphHeight + topOffset);
			CGContextClosePath(context);
			CGContextDrawPath(context, kCGPathFill);
			*/

			/*
			 * Draw the line
			 */
			CGContextSetLineWidth(context, 2.0);
			CGContextSetStrokeColorWithColor(context, [[[BCDashboardViewController graphColors] objectAtIndex:setIndex] CGColor]);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
			for (int pointIndex = 1; pointIndex < [dataPoints count]; pointIndex++) {
				CGPoint destinationPoint = CGPointMake([[[dataPoints objectAtIndex:pointIndex] valueForKey:@"x"] floatValue],
					[[[dataPoints objectAtIndex:pointIndex] valueForKey:@"y"] floatValue]);
				CGPoint previousPoint = CGPointMake([[[dataPoints objectAtIndex:pointIndex - 1] valueForKey:@"x"] floatValue],
					[[[dataPoints objectAtIndex:pointIndex - 1] valueForKey:@"y"] floatValue]);
				if (previousPoint.y == destinationPoint.y) {
					CGContextAddLineToPoint(context, destinationPoint.x, destinationPoint.y);
				}
				else {
					CGPoint controlPoint1 = CGPointMake(previousPoint.x - xStep * 0.75, previousPoint.y);
					CGPoint controlPoint2 = CGPointMake(destinationPoint.x + xStep * 0.75, destinationPoint.y);
					CGContextAddCurveToPoint(context, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y,
						destinationPoint.x, destinationPoint.y);
				}
			}
			CGContextDrawPath(context, kCGPathStroke);

			// Don't draw the circles if they would be too close together
			if (maxPointsToShow <= 30) {
				/*
				 * add the circles at the data points
				 */
				NSInteger circleRadius = 4;
				CGRect rect;
				for (int pointIndex = 0; pointIndex < [dataPoints count]; pointIndex++) {
					float x = [[[dataPoints objectAtIndex:pointIndex] valueForKey:@"x"] floatValue];
					float y = [[[dataPoints objectAtIndex:pointIndex] valueForKey:@"y"] floatValue];
					rect = CGRectMake(x - circleRadius, y - circleRadius, 2 * circleRadius, 2 * circleRadius);
					CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
					CGContextAddEllipseInRect(context, rect);
				}
				CGContextDrawPath(context, kCGPathFillStroke);

				// the last circle should be filled
				rect = CGRectMake(firstPoint.x - circleRadius, firstPoint.y - circleRadius, 2 * circleRadius, 2 * circleRadius);
				CGContextSetFillColorWithColor(context, [[[BCDashboardViewController graphColors] objectAtIndex:setIndex] CGColor]);
				CGContextAddEllipseInRect(context, rect);
				CGContextDrawPath(context, kCGPathFillStroke);
			}
		}
	};
}

#pragma mark - fetch
- (NSArray *)trackingLogs
{
    if (_trackingLogs != nil) {
        return _trackingLogs;
    }

	User *thisUser = [User currentUser];

	NSDate *endDate = [NSDate date];
	NSDate *startDate = endDate;

	switch (self.scopeIndex) {
		case ScopeWeek:
			startDate = [self.calendar BC_dateByAddingDays:-6 toDate:startDate];
			break;
		case ScopeMonth:
			startDate = [self.calendar BC_dateByAddingDays:-29 toDate:startDate];
			break;
		case ScopeYear:
			startDate = [self.calendar BC_dateByAddingMonths:-11 toDate:startDate];
			startDate = [self.calendar BC_firstOfMonthForDate:startDate];
			break;
		case ScopeCustom:
			startDate = [self.calendar BC_dateByAddingDays:-(self.customScopeValue - 1) toDate:startDate];
			break;
	}

	startDate = [self.calendar BC_startOfDate:startDate];
	_trackingLogs = [TrackingMO MR_findAllSortedBy:TrackingMOAttributes.date ascending:NO
		withPredicate:[NSPredicate predicateWithFormat:@"trackingType = %d AND date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
		self.trackType, startDate, endDate, kStatusDelete, thisUser.loginId] inContext:self.managedObjectContext];

	// Attempt to fetch the value just prior to the start of our period to allow the graph to run off the left edge,
	// but only do this if there are other data points to show for this period
	if ([_trackingLogs count]) {
		TrackingMO *priorTracking = [TrackingMO MR_findFirstWithPredicate:
			[NSPredicate predicateWithFormat:@"trackingType = %d AND date < %@ AND status <> %@ and loginId = %@",
				self.trackType, startDate, kStatusDelete, thisUser.loginId]
			sortedBy:TrackingMOAttributes.date ascending:NO inContext:self.managedObjectContext];
		
		if (priorTracking) {
			_trackingLogs = [_trackingLogs arrayByAddingObject:priorTracking];
		}
	}

	return _trackingLogs;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rows = 0;
	if (section == SectionLegend) {
		// No real need for a legend if there is only one data element
		if ([self.trackingValues count] > 1) {
			rows = 1;
		}
	}
	else {
		rows = [self.trackingLogs count];
	}

	return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *normalCellId = @"NormalCell";
    static NSString *smallLegendCellId = @"SmallLegendCell";
    static NSString *largeLegendCellId = @"LargeLegendCell";

    UITableViewCell *cell = nil;
	if (indexPath.section == SectionLegend) {
		if ([self.trackingValues count] < 4) {
			cell = [tableView dequeueReusableCellWithIdentifier:smallLegendCellId];
		}
		else {
			cell = [tableView dequeueReusableCellWithIdentifier:largeLegendCellId];
		}
	}
	else {
		cell = [tableView dequeueReusableCellWithIdentifier:normalCellId];
	}
    
    [self configureCell:cell inTableView:tableView atIndexPath:indexPath];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	BOOL canEdit = NO;
	// Cannot delete rows where the tracking type is readonly, or when the source of data is external
	if (indexPath.section == SectionData) {
		TrackingMO *track = [self.trackingLogs objectAtIndex:indexPath.row];
		canEdit = ![self.trackingTypeMO.readOnly boolValue] && ![track.source integerValue];
	}

	return canEdit;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
	forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ((indexPath.section == SectionData) && (editingStyle == UITableViewCellEditingStyleDelete)) {
		// Delete the managed object for the given index path
		TrackingMO *track = [self.trackingLogs objectAtIndex:indexPath.row];

		if (![self.trackingTypeMO.readOnly boolValue] && ![track.source integerValue]) {
			track.status = kStatusDelete;

			// Save the context.
			[track.managedObjectContext BL_save];

			// Post a notification that we've updated the tracking so that the side bar can update itself
			[[NSNotificationCenter defaultCenter] postNotificationName:[TrackingMO entityName]
				object:self userInfo:nil];

			[self reloadTracking];
		}
	}
}

#pragma mark - actions
- (IBAction)trackingAddViewDidSave:(UIStoryboardSegue *)segue
{
	DDLogInfo(@"tracking add save");
	// notify delegate
	if (self.delegate && [self.delegate conformsToProtocol:@protocol(BCDetailViewDelegate)]) {
        [self.delegate view:self didUpdateObject:nil];
	}

	// Post a notification that we've updated the tracking so that the side bar can update itself
	[[NSNotificationCenter defaultCenter] postNotificationName:[TrackingMO entityName]
		object:self userInfo:nil];

	[self reloadTracking];
}

- (IBAction)trackingAddViewDidCancel:(UIStoryboardSegue *)segue
{
	DDLogInfo(@"tracking add cancel");
}

@end
