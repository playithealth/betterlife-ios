//
//  BCLoggingSearchMainViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/9/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCLoggingSearchMainViewController.h"
#import <objc/runtime.h>

#import "BCAppDelegate.h"
#import "BCActivitySearchDataModel.h"
#import "BCActivitySearchRecentViewController.h"
#import "BCActivitySearchSearchViewController.h"
#import "BCActivitySearchSmartViewController.h"
#import "BCCardioLogDetailViewController.h"
#import "BCDetailViewDelegate.h"
#import "BCLoggingJustCaloriesViewController.h"
#import "BCLoggingSearchRecentViewController.h"
#import "BCLoggingSearchSearchViewController.h"
#import "BCLoggingSearchSmartViewController.h"
#import "BCObjectManager.h"
#import "BCProgressHUD.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "CategoryMO.h"
#import "CustomFoodMO.h"
#import "FoodLogMO.h"
#import "FoodLogDetailViewController.h"
#import "GTMHTTPFetcherAdditions.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "PantryItem.h"
#import "PurchaseHxItem.h"
#import "ShoppingListItem.h"
#import "User.h"

@interface BCLoggingSearchMainViewController () <UINavigationBarDelegate, UIScrollViewDelegate, ZBarReaderDelegate>

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UIView *childView;

@property (strong, nonatomic) NSMutableArray *viewControllers;
@property (assign, nonatomic) NSUInteger activeIndex;

@end

@implementation BCLoggingSearchMainViewController

static const NSInteger kTagAlertItemNotFound = 1001;
static char barcodeKey;

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.viewControllers = [NSMutableArray array];
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	NSInteger pageToShow = 0;

	if (self.loggingType == BCLoggingTypeFood) {
		self.navigationBar.topItem.title = self.headerText;

		BCLoggingSearchSmartViewController *smartViewController = [self.storyboard
			instantiateViewControllerWithIdentifier:@"LoggingSearchSmart"];
		smartViewController.delegate = self.delegate;
		smartViewController.managedObjectContext = self.managedObjectContext;
		smartViewController.logDate = self.logDate;
		smartViewController.logTime = self.logTime;
		[self addChildViewController:smartViewController atIndex:0];

		BCLoggingSearchRecentViewController *recentViewController = [self.storyboard
			instantiateViewControllerWithIdentifier:@"LoggingSearchRecent"];
		recentViewController.managedObjectContext = self.managedObjectContext;
		recentViewController.logDate = self.logDate;
		recentViewController.logTime = self.logTime;
		[self addChildViewController:recentViewController atIndex:1];

		BCLoggingSearchSearchViewController *searchViewController = [self.storyboard
			instantiateViewControllerWithIdentifier:@"LoggingSearchSearch"];
		searchViewController.delegate = self.delegate;
		searchViewController.managedObjectContext = self.managedObjectContext;
		searchViewController.logDate = self.logDate;
		searchViewController.logTime = self.logTime;
		[self addChildViewController:searchViewController atIndex:2];
		
		// If there are no items shown in the smart view, then go to the search view
		if (!smartViewController.loggedFoods.count && !smartViewController.mealPredictions.count) {
			self.segmentedControl.selectedSegmentIndex = 2;
			pageToShow = 2;
		}
	}
	else if (self.loggingType == BCLoggingTypeActivity) {
		self.navigationBar.topItem.title = @"Cardio";

		BCActivitySearchDataModel *dataModel = [[BCActivitySearchDataModel alloc] initWithManagedObjectContext:self.managedObjectContext
			withLogDate:self.logDate];

		BCActivitySearchSmartViewController *smartViewController = [self.storyboard
			instantiateViewControllerWithIdentifier:@"ActivitySearchSmart"];
		smartViewController.dataModel = dataModel;
		[self addChildViewController:smartViewController atIndex:0];

		BCActivitySearchRecentViewController *recentViewController = [self.storyboard
			instantiateViewControllerWithIdentifier:@"ActivitySearchRecent"];
		recentViewController.dataModel = dataModel;
		[self addChildViewController:recentViewController atIndex:1];

		BCActivitySearchSearchViewController *searchViewController = [self.storyboard
			instantiateViewControllerWithIdentifier:@"ActivitySearchSearch"];
		searchViewController.dataModel = dataModel;
		[self addChildViewController:searchViewController atIndex:2];
	}
	/*
	else if (self.loggingType == BCLoggingTypeStrength) {
		self.navigationBar.topItem.title = @"Strength";

		BCStrengthSearchDataModel *dataModel = [[BCStrengthSearchDataModel alloc] initWithManagedObjectContext:self.managedObjectContext
			withLogDate:self.logDate];

		BCStrengthSearchSmartViewController *smartViewController = [self.storyboard
			instantiateViewControllerWithIdentifier:@"StrengthSearchSmart"];
		smartViewController.dataModel = dataModel;
		[self addChildViewController:smartViewController atIndex:0];

		BCStrengthSearchRecentViewController *recentViewController = [self.storyboard
			instantiateViewControllerWithIdentifier:@"StrengthSearchRecent"];
		recentViewController.dataModel = dataModel;
		[self addChildViewController:recentViewController atIndex:1];
	}
	*/

	self.activeIndex = NSNotFound;
	[self switchToViewControllerAtIndex:pageToShow];
}

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
	return UIBarPositionTopAttached;
}

- (void)addChildViewController:(UIViewController *)childController atIndex:(NSUInteger)index
{
	childController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

	[self addChildViewController:childController];
	[childController didMoveToParentViewController:self];
	self.viewControllers[index] = childController;
}

- (void)switchToViewControllerAtIndex:(NSUInteger)index
{
	if (self.activeIndex != NSNotFound) {
		UIView *fromView = [self.viewControllers[self.activeIndex] view];
		[fromView removeFromSuperview];
	}
	self.activeIndex = index;

	UIView *toView = [self.viewControllers[self.activeIndex] view];
	toView.frame = self.childView.bounds;
	[self.childView insertSubview:toView atIndex:0];
}

- (IBAction)segmentTapped:(UISegmentedControl *)sender
{
	[self switchToViewControllerAtIndex:sender.selectedSegmentIndex];
}

#pragma mark - navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"CreateCardio"]) {
		BCCardioLogDetailViewController *detailView = segue.destinationViewController;
		detailView.managedObjectContext = self.managedObjectContext;
		detailView.logDate = self.logDate;
	}
	else if ([segue.identifier isEqualToString:@"CreateJustCalories"]) {
		BCLoggingJustCaloriesViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;
	}
	/*
	else if ([segue.identifier isEqualToString:@"CreateStrength"]) {
		BCStrengthLogDetailViewController *detailView = segue.destinationViewController;
		detailView.managedObjectContext = self.managedObjectContext;
		detailView.logDate = self.logDate;
	}
	*/
	else if ([segue.identifier isEqualToString:@"CreateFood"]) {
		FoodLogDetailViewController *detailView = segue.destinationViewController;
		detailView.createCustomFood = YES;
		detailView.detailViewMode = ItemDetailViewModeEditing;
		detailView.delegate = self;

		NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];

		FoodLogMO *newLog = [FoodLogMO insertInManagedObjectContext:scratchContext];
		newLog.status = kStatusPost;
		newLog.loginId = [[User currentUser] loginId];
		newLog.date = self.logDate;
		newLog.logTime = self.logTime;
		newLog.servings = @1;
		newLog.nutritionFactor = @1;
		newLog.itemType = kItemTypeProduct;

		detailView.managedObjectContext = scratchContext;
		detailView.item = newLog;
	}

}

- (IBAction)close:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ZBarReaderDelegate methods
- (IBAction)scanButtonTapped:(id)sender 
{
	ZBarReaderViewController *reader = [ZBarReaderViewController new];

	reader.readerDelegate = self;
	[reader.scanner setSymbology:ZBAR_NONE config:ZBAR_CFG_ENABLE to:0];
	[reader.scanner setSymbology:ZBAR_EAN8 config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_EAN13 config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_UPCA config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_UPCE config:ZBAR_CFG_ENABLE to:1];
	[self presentViewController:reader animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)reader
	didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];

	ZBarSymbol *symbol = nil;
	for (symbol in results) {
		break;
	}

	NSString *barcode = symbol.data;
	NSString *type = symbol.typeName;

	[self getProductByBarcode:barcode andType:type];

	[reader dismissViewControllerAnimated:YES completion:nil];
}

// FIXME!!: This method will NOT fully fill out a FoodLogMO, such as imageId, and maybe other columns
- (void)getProductByBarcode:(NSString *)barcode andType:(NSString *)type
{
	// check for the item in the custom foods first
	id matchingItem = [CustomFoodMO foodByName:nil productId:nil barcode:barcode inMOC:self.managedObjectContext];

	// then the shoppinglist
	if (!matchingItem) {
		matchingItem = [ShoppingListItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	}

	// then in pantry
	if (!matchingItem) {
		matchingItem = [PantryItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	}

	// then in purchase history
	if (!matchingItem) {
		matchingItem = [PurchaseHxItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	}

	// if a match was found in the cache, add it
	if (matchingItem) {
		FoodLogMO *foodLog = [FoodLogMO insertInManagedObjectContext:self.managedObjectContext];
		foodLog.date = self.logDate;
		foodLog.logTime = self.logTime;
		[foodLog setWithManagedObject:matchingItem];

		[foodLog markForSync];
		NSError *error = nil;
		if (![foodLog.managedObjectContext save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}
		
		[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your log", foodLog.name] onView:self.view];
	}
	// if the item wasn't found in any local caches, do the search for the barcode
	else {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory productSearchURLForBarcode:barcode andType:type]];
		[fetcher beginFetchWithCompletionHandler: ^(NSData *retrievedData, NSError * error) {
			[appDelegate setNetworkActivityIndicatorVisible:NO];
			if (error != nil) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
			}
			else {
				NSArray *results = [NSJSONSerialization JSONObjectWithData:retrievedData options:kNilOptions error:nil];

				if ([results count] > 0) {
					NSDictionary *matchingItem = [results objectAtIndex:0];
					FoodLogMO *foodLog = [FoodLogMO insertInManagedObjectContext:self.managedObjectContext];
					foodLog.loginId = [[User currentUser] loginId];
					foodLog.date = self.logDate;
					foodLog.logTime = self.logTime;
					[foodLog setWithProductDictionary:matchingItem];

					[foodLog markForSync];

					NSError *error = nil;
					if (![foodLog.managedObjectContext save:&error]) {
						DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
					}

					[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your log", foodLog.name] onView:self.view];
				}
				else {
					UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Product not found."
						message:[NSString stringWithFormat:@"No matching product found with barcode %@, add this?", barcode]
						delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
					alert.tag = kTagAlertItemNotFound;
					alert.alertViewStyle = UIAlertViewStylePlainTextInput;

					// set the barcode into some storage associated with the alert view
					objc_setAssociatedObject(alert, &barcodeKey, barcode, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

					[alert show];
				}
			}
		}];
	}
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == kTagAlertItemNotFound) {
		if (buttonIndex != alertView.cancelButtonIndex) {
			NSString *name = [[alertView textFieldAtIndex:0] text];

			// check to see if the name is already used in custom foods
			CustomFoodMO *customFood = [CustomFoodMO foodByName:name productId:nil barcode:nil inMOC:self.managedObjectContext];
			if (customFood) {
				 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Duplicate name"
					 message:@"Please enter a unique product name"
					 delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
				 alert.tag = kTagAlertItemNotFound;
				 alert.alertViewStyle = UIAlertViewStylePlainTextInput;

				 [alert show];
			}
			else {
				// retrieve the barcode from the associated storage, then clear it
				NSString *barcode = (NSString *)objc_getAssociatedObject(alertView, &barcodeKey);
				objc_setAssociatedObject(alertView, &barcodeKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

				NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
				FoodLogMO *newLog = [FoodLogMO insertInManagedObjectContext:scratchContext];
				newLog.status = kStatusPost;
				newLog.loginId = [[User currentUser] loginId];
				newLog.date = self.logDate;
				newLog.logTime = [FoodLogMO guessMealTime];
				newLog.servings = @1;
				newLog.nutritionFactor = @1;
				newLog.name = name;
				newLog.itemType = kItemTypeProduct;

				// Start to fill out a custom food, as it gives us a place to store the barcode
				newLog.customFood = [CustomFoodMO insertInManagedObjectContext:newLog.managedObjectContext];
				newLog.customFood.barcode = barcode;

				FoodLogDetailViewController *detailView = [self.storyboard instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
				detailView.delegate = self;
				detailView.managedObjectContext = scratchContext;
				detailView.item = newLog;

				// set a flag to create a custom food - this is sorta hacky
				detailView.createCustomFood = YES;
				detailView.detailViewMode = ItemDetailViewModeEditing;

				[self presentViewController:detailView animated:YES completion:nil];
			}
		}
	}
	else {
		NSAssert(NO, @"Unknown alert");
	}
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	DDLogInfo(@"logging search main - view:%@ didUpdateObject:%@", viewController, object);

	if ([viewController isKindOfClass:[BCLoggingJustCaloriesViewController class]]) {
		BCLoggingJustCaloriesViewController *caloriesView = viewController;
		NSNumber *calories = object;

		if ([calories integerValue]) {
			FoodLogMO *foodLog = [FoodLogMO insertInManagedObjectContext:self.managedObjectContext];

			foodLog.loginId = [[User currentUser] loginId];
			foodLog.logTime = self.logTime;
			foodLog.date = self.logDate;
			foodLog.name = @"Just Calories";
			foodLog.servings = @1;
			foodLog.nutritionFactor = @1;
			foodLog.itemType = @"calories";

			foodLog.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
			foodLog.nutrition.calories = [caloriesView.caloriesField.text numberValueDecimal];

			[foodLog markForSync];

			[foodLog.managedObjectContext BL_save];

			NSString *labelText = [NSString stringWithFormat:@"%@ calories added to your log", foodLog.nutrition.calories];

			[BCProgressHUD notificationWithText:labelText onView:self.view];
		}
	}
	else if ([viewController isKindOfClass:[FoodLogDetailViewController class]]) {
		if (object) {
			FoodLogMO *foodLog = object;

			[foodLog.managedObjectContext BL_save];
			// Now save in this object's MOC, as the detail view may have used a child context
			if (foodLog.managedObjectContext != self.managedObjectContext) {
				[self.managedObjectContext BL_save];
			}

			NSString *labelText = [NSString stringWithFormat:@"%@ added to your log", foodLog.name];

			[BCProgressHUD notificationWithText:labelText onView:self.view];
		}
	}

	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
