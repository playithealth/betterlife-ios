//
//  UIBarButtonItemAdditions.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/19/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "UIBarButtonItemAdditions.h"


@implementation UIBarButtonItem (UIBarButtonItemAdditions)
+ (id)barButtonItemWithImageName:(NSString *)imageName 
target:(id)target action:(SEL)selector
{
	UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc]
		initWithImageName:imageName target:target action:selector];
	return buttonItem;
}

- (id)initWithImageName:(NSString *)imageName 
target:(id)target action:(SEL)selector
{
	UIImage *normalImage = [UIImage imageNamed:
		[NSString stringWithFormat:@"%@.png", imageName]];
	UIImage *selectedImage = [UIImage imageNamed:
		[NSString stringWithFormat:@"%@-tap.png", imageName]];
	// create Button
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom]; 
	// Set its background image for some states...
	[button setBackgroundImage:normalImage forState:UIControlStateNormal];  
	[button setBackgroundImage:selectedImage forState:UIControlStateHighlighted];  
	// Add target to receive Action
	[button addTarget:target action:selector
		forControlEvents:UIControlEventTouchUpInside];
	// Set frame width, height
	button.frame = CGRectMake(0, 0, 40, 35);  
	// create Button Item
	return [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (id)initWithImageName:(NSString *)imageName 
labelText:(NSString *)text target:(id)target action:(SEL)selector
{
	UIImage *normalImage = [UIImage imageNamed:
		[NSString stringWithFormat:@"%@.png", imageName]];
	UIImage *selectedImage = [UIImage imageNamed:
		[NSString stringWithFormat:@"%@-tap.png", imageName]];

	// create Button
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom]; 
	// Set its background image for some states...
	[button setBackgroundImage:normalImage forState:UIControlStateNormal];  
	[button setBackgroundImage:selectedImage forState:UIControlStateHighlighted];  
	// Add target to receive Action
	[button addTarget:target action:selector
		forControlEvents:UIControlEventTouchUpInside];
	// Set frame width, height
	button.frame = CGRectMake(0, 0, 40, 30);  

	// create label
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 26, 40, 14)];
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont systemFontOfSize:12];
	label.textColor = [UIColor whiteColor];
	label.textAlignment = NSTextAlignmentCenter;
	label.text = text;

	UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
	[buttonView addSubview:button];
	[buttonView addSubview:label];

	// create Button Item
	return [[UIBarButtonItem alloc] initWithCustomView:buttonView];
}


@end
