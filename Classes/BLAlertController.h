//
//  BLAlertController.h
//  BettrLife
//
//  Created by Greg Goodrich on 11/7/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BLAlertAction;

@interface BLAlertController : NSObject
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic, readonly) NSArray *textFields;
@property (strong, nonatomic, readonly) NSArray *actions;

+ (BLAlertController *)alertControllerWithTitle:(NSString *)title message:(NSString *)message
preferredStyle:(UIAlertControllerStyle)style;
- (void)addAction:(BLAlertAction *)action;
- (void)addTextFieldWithConfigurationHandler:(void (^)(UITextField *textField))configurationHandler;
- (void)presentInViewController:(UIViewController *)viewController animated:(BOOL)animated completion:(void (^)(void))completion;
@end

@interface BLAlertAction : NSObject
@property (strong, nonatomic, readonly) NSString *title;
@property (assign, nonatomic, readonly) UIAlertActionStyle style;
@property (assign, nonatomic, getter=isEnabled) BOOL enabled;
@property (strong, nonatomic, readonly) UIAlertAction *alertAction;

+ (BLAlertAction *)actionWithTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(void (^)(UIAlertAction *action))handler;
@end
