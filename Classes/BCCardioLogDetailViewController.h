//
//  BCCardioLogDetailViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/5/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TrainingActivityMO;

@interface BCCardioLogDetailViewController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) TrainingActivityMO *activityLog;
@property (strong, nonatomic) NSDate *logDate;
- (void)finishEditing;

@end
