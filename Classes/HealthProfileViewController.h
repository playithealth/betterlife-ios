//
//  HealthProfileViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/15/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HealthProfileViewController : UIViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end
