//
//  AverageRatingCell.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/7/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "AverageRatingCell.h"

#import "StarRatingControl.h"

@implementation AverageRatingCell
@synthesize categoryLabel;
@synthesize ratingCountLabel;
@synthesize ratingControl;

- (void)dealloc
{
    categoryLabel = nil;
    ratingCountLabel = nil;
    ratingControl = nil;

}

@end
