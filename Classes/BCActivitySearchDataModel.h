//
//  BCActivitySearchDataModel.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/18/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

@class ExerciseRoutineMO;
@class TrainingActivityMO;

@interface BCActivitySearchDataModel : NSObject

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSDate *logDate;
@property (strong, nonatomic, readonly) NSArray *allExerciseRoutines;
@property (strong, nonatomic, readonly) NSArray *allMyTrainingActivities;
@property (strong, nonatomic, readonly) NSMutableArray *filteredActivities;
@property (strong, nonatomic, readonly) NSArray *trainingActivitiesGroupedByActivity;
@property (strong, nonatomic, readonly) NSArray *trainingActivitiesGroupedByDate;
@property (assign, nonatomic) BOOL moreResults;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)moc withLogDate:(NSDate *)logDate;

- (void)activitySmartSearch:(NSString *)searchTerm
completionBlock:(void (^)(NSUInteger resultCount, BOOL more, NSError *error))completionBlock;
- (void)activitySmartSearchMoreWithCompletionBlock:(void (^)(NSUInteger resultCount, BOOL more, NSError *error))completionBlock;

- (id)insertActivityBasedOnExerciseRoutine:(ExerciseRoutineMO *)routine;
- (id)insertActivityBasedOnTrainingActivity:(TrainingActivityMO *)activity;

- (NSArray *)filterTrainingActivities:(NSString *)searchText;

- (void)save;

@end
