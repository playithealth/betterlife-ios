//
//  BCRecipeBoxViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 8/19/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCRecipeBoxViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) BOOL singleSelect;
@property (assign, nonatomic) BOOL needsSync;

- (IBAction)searchTextDidChange:(id)sender;
- (IBAction)searchButtonTapped:(id)sender;

@end
