//
//  ShoppingListSearchViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/6/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "ShoppingListSearchViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCProgressHUD.h"
#import "BCTableViewCell.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "CategoryMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "ProductSearchRecord.h"
#import "PurchaseHxItem.h"
#import "ProductSearchFilterViewController.h"
#import "ShoppingListItemDetailViewController.h"
#import "ShoppingListItem.h"
#import "UIColor+Additions.h"
#import "UIImage+Additions.h"
#import "UITableView+DownloadImage.h"
#import "User.h"

static const NSInteger kSectionAddCustomItem = 0;
static const NSInteger kSectionPurchaseHxResults = 1;
static const NSInteger kSectionProductResults = 2;

static const NSInteger kInitialRowsPerSection = 3;
static const NSInteger kSearchPageSize = 10;

@interface ShoppingListSearchViewController () <ProductSearchFilterViewDelegate, ShoppingListItemDetailViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIImageView *footerImageView;
@property (weak, nonatomic) IBOutlet UILabel *filtersLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerViewBottomConstraint;

@property (strong, nonatomic) NSManagedObjectContext *scratchContext;
@property (strong, nonatomic) NSMutableArray *productResults;
@property (strong, nonatomic) NSArray *searchBrands;
@property (strong, nonatomic) NSArray *searchCategories;
@property (strong, nonatomic) NSMutableArray *purchaseHxResults;
@property (assign, nonatomic) BOOL morePurchaseHx;
@property (assign, nonatomic) BOOL moreProducts;
@property (assign, nonatomic) NSInteger morePage;
@property (strong, nonatomic) NSArray *searchFilters;
@property (strong, nonatomic) NSString *searchTerm;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)addFreeTextItem;

@end

@implementation ShoppingListSearchViewController

#pragma mark - view lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
	}
	return self;
}

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
    
	// terminate all pending download connections
	[self.tableView cancelAllImageTrackers];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
	self.filtersLabel.text = @"";
}

- (void)viewDidAppear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
	[super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    // terminate all pending download connections
	[self.tableView cancelAllImageTrackers];

	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [self setSearchTextField:nil];
    [self setCancelButton:nil];
    [self setTableView:nil];
	[self setFooterImageView:nil];
	[self setFiltersLabel:nil];

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowFilters"]) {
		ProductSearchFilterViewController *filterView = segue.destinationViewController;
		filterView.managedObjectContext = self.scratchContext;
		filterView.delegate = self;
		filterView.searchBrands = self.searchBrands;
		filterView.searchCategories = self.searchCategories;
	}
}

#pragma mark - local methods
- (void)reloadData
{
	// Ensure that the image trackers don't try to update indexPaths until after we refresh
	[self.tableView clearTrackerIndexPaths];
	[self.tableView reloadData];
}

- (void)sendFirstSearchRequest
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	NSURL *searchURL = [BCUrlFactory searchUrlForSearchTerm:self.searchTerm searchSections:@[@"products"] showRows:kInitialRowsPerSection showOnlyFood:NO];
    
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
	
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:nil];
		}
		else {
			// fetch succeeded
			NSDictionary *searchResultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
            
			NSDictionary *productsDict = [searchResultsDict objectForKey:@"products"];
			NSArray *productsArray = [productsDict objectForKey:@"results"];
			self.moreProducts = [[productsDict objectForKey:@"more"] boolValue];

			NSMutableArray *psrArray = [NSMutableArray array];
			for (NSDictionary *product in productsArray) {
				[psrArray addObject:[ProductSearchRecord recordWithSearchDictionary:product withManagedObjectContext:self.scratchContext]];
			}

			self.productResults = psrArray;
			[self reloadData];
			[self loadImagesForOnscreenRows];

			[appDelegate setNetworkActivityIndicatorVisible:NO];
		}

		[self.searchTextField resignFirstResponder];
	}];
}

- (void)sendMoreProductsRequest
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	[self.searchTextField resignFirstResponder];

	if (!self.morePage) {
		[self showBackView:YES withLabelText:@"All results for Products"];
		[self resetResultArraysExcluding:self.productResults];
		// default this to YES so that the load more row will show while it's loading
		self.moreProducts = YES;
		[self reloadData];
	}
	self.morePage += 1;
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.productResults count] inSection:kSectionProductResults];
	BCTableViewCell *cell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[cell.spinner startAnimating];
	cell.bcTextLabel.text = @"Loading...";
	
	NSMutableDictionary *filterDict = [NSMutableDictionary dictionary];
	if (self.searchFilters) {
		NSMutableArray *categoryFilters = [self.searchFilters objectAtIndex:0];
		if ([categoryFilters count]) {
			[filterDict setObject:categoryFilters forKey:@"categories"];
		}
		
		NSMutableArray *brandFilters = [self.searchFilters objectAtIndex:1];
		if ([brandFilters count]) {
			[filterDict setObject:brandFilters forKey:@"brands"];
		}
		
		NSMutableArray *ratingsFilters = [self.searchFilters objectAtIndex:2];
		if ([ratingsFilters count]) {
			[filterDict setObject:ratingsFilters forKey:@"ratings"];
		}
	}

	NSURL *searchURL = [BCUrlFactory productSearchURLForSearchTerm:self.searchTerm showRows:kSearchPageSize + 1
		rowOffset:((self.morePage - 1) * kSearchPageSize) filters:filterDict showOnlyFood:NO];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSDictionary *searchResultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			NSArray *productsArray = [searchResultsDict objectForKey:kProductResults];
			self.moreProducts = [productsArray count] > kSearchPageSize;

			// process the results into ProductSearchRecord objects
			NSMutableArray *psrArray = [NSMutableArray array];
			for (NSDictionary *product in productsArray) {
				[psrArray addObject:[ProductSearchRecord recordWithSearchDictionary:product withManagedObjectContext:self.scratchContext]];
			}
			[self insertRowsFromArray:psrArray toArray:self.productResults inSection:kSectionProductResults more:self.moreProducts];
			[self loadImagesForOnscreenRows];
			
			if ([searchResultsDict objectForKey:kProductBrands]) {
				self.searchBrands = [searchResultsDict objectForKey:kProductBrands];
			}
			if ([searchResultsDict objectForKey:kProductCategories]) {
				self.searchCategories = [searchResultsDict objectForKey:kProductCategories];			
			}

			if (self.footerView.hidden) {
				self.footerView.hidden = NO;

				// remove the constraint tying the table view to the main view and 
				// tie it to the top of the footer
				[self.view removeConstraint:self.tableViewBottomConstraint];
				self.tableViewBottomConstraint = 
					[NSLayoutConstraint constraintWithItem:self.tableView
												 attribute:NSLayoutAttributeBottom
												 relatedBy:NSLayoutRelationEqual
													toItem:self.footerView
												 attribute:NSLayoutAttributeTop
												multiplier:1.0
												  constant:0];
				[self.view addConstraint:self.tableViewBottomConstraint];

				// 'show' the footer view at the bottom of the view
				self.footerViewBottomConstraint.constant = 0;

				[UIView animateWithDuration:0.3f animations:^{
					[self.view layoutIfNeeded];

					self.footerImageView.image = [UIImage imageNamed:@"product-search-footer"];
					self.filtersLabel.text = @"";
				}];
			}
		}
		
		[appDelegate setNetworkActivityIndicatorVisible:NO];
		[cell.spinner stopAnimating];
		cell.bcTextLabel.text = @"Load more results...";
	}];
}

- (void)resetResultArraysExcluding:(NSArray *)exclude
{
	if (exclude != self.purchaseHxResults) {
		self.purchaseHxResults = nil;
	}
	if (exclude != self.productResults) {
		self.productResults = [NSMutableArray array];
	}

	self.morePage = 0;

	self.morePurchaseHx = NO;
	self.moreProducts = NO;
}

- (void)fetchInitialResults
{
	// load ALL purchase history
    User *thisUser = [User currentUser];
	self.purchaseHxResults = [[PurchaseHxItem MR_findAllSortedBy:PurchaseHxItemAttributes.purchasedOn ascending:NO 
		withPredicate:[NSPredicate predicateWithFormat:@"accountId = %@", thisUser.accountId] inContext:self.scratchContext] mutableCopy];

	[self reloadData];
}

- (void)fetchLocalResults
{
	// wipe out any previous entries and grab the first few rows
	self.purchaseHxResults = [NSMutableArray array];
	self.morePurchaseHx = YES;
	[self reloadData];
	[self fetchPurchaseHxMore:NO containing:self.searchTerm];
}

- (void)fetchPurchaseHxMore:(BOOL)more containing:(NSString *)searchTerm
{    
	if (more && !self.morePage) {
		[self showBackView:YES withLabelText:@"All results for Purchase History"];
		[self resetResultArraysExcluding:self.purchaseHxResults];
		self.morePurchaseHx = YES;
		[self reloadData];
	}

    User *thisUser = [User currentUser];

	NSPredicate *predicate = nil;
	if ([searchTerm length] > 0) {
		predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@ AND accountId = %@", 
			PurchaseHxItemAttributes.name, searchTerm, thisUser.accountId];
	}
	else {
		predicate = [NSPredicate predicateWithFormat:@"accountId = %@", thisUser.accountId];
	}

	NSFetchRequest *fetchRequest = [PurchaseHxItem MR_requestAllSortedBy:PurchaseHxItemAttributes.purchasedOn ascending:NO
		withPredicate:predicate inContext:self.scratchContext];

	NSInteger totalNumberOfResults = [self.scratchContext countForFetchRequest:fetchRequest error:nil];
	NSInteger existingRows = [self.purchaseHxResults count];
	
	NSIndexPath *moreIndexPath = [NSIndexPath indexPathForRow:existingRows inSection:kSectionPurchaseHxResults];
	BCTableViewCell *moreCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:moreIndexPath];
	[moreCell.spinner startAnimating];
	moreCell.bcTextLabel.text = @"Loading...";

	NSInteger displayCount = kInitialRowsPerSection;
	fetchRequest.fetchLimit = displayCount;
	if (more) {
		self.morePage += 1;
		displayCount = MIN(totalNumberOfResults, self.morePage * kSearchPageSize + kInitialRowsPerSection);
		fetchRequest.fetchLimit = displayCount - existingRows;
	}
	fetchRequest.fetchOffset = existingRows;

	self.morePurchaseHx = (displayCount < totalNumberOfResults);

	NSArray *moreRows = [self.scratchContext executeFetchRequest:fetchRequest error:nil];
	[self insertRowsFromArray:moreRows toArray:self.purchaseHxResults inSection:kSectionPurchaseHxResults more:self.morePurchaseHx];

	[moreCell.spinner stopAnimating];
	moreCell.bcTextLabel.text = @"Load more results...";
}

- (void)addFreeTextItem
{
	ShoppingListItem *customItem = [ShoppingListItem addOrIncrementItemNamed:self.searchTerm productId:@0
		category:nil barcode:nil quantity:1 inMOC:self.scratchContext];

	[self.scratchContext BL_save];
    
	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:customItem];
	}
}

- (void)loadImagesForOnscreenRows
{
	// check to make sure the search tab is active
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
	for (NSIndexPath *indexPath in visiblePaths) {
		if (indexPath.section == kSectionPurchaseHxResults) {
			if (indexPath.row < [self.purchaseHxResults count]) {
				PurchaseHxItem *purchaseHxItem = [self.purchaseHxResults objectAtIndex:indexPath.row];

				if (purchaseHxItem.imageId && purchaseHxItem.imageIdValue) {
					[self.tableView downloadImageWithImageId:purchaseHxItem.imageId forIndexPath:indexPath withSize:kImageSizeMedium];
				}
			}
		}
		else if (indexPath.section == kSectionProductResults) {
			if (indexPath.row < [self.productResults count]) {
				ProductSearchRecord *record = [self.productResults objectAtIndex:[indexPath row]];

				if (record.imageId && [record.imageId integerValue]) {
					[self.tableView downloadImageWithImageId:record.imageId forIndexPath:indexPath withSize:kImageSizeMedium];
				}
			}
		}
	}
}

#pragma mark - button responders
- (void)addFreeTextButtonTapped:(UITapGestureRecognizer *)recognizer
{
	if ([self.searchTerm length] == 0) {
		return;
	}
    
	[self.searchTextField resignFirstResponder];
    
    [self addFreeTextItem];
}

- (void)quickAddPurchaseHxButtonTapped:(UITapGestureRecognizer *)recognizer
{
	[self.searchTextField resignFirstResponder];
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	PurchaseHxItem *purchaseHxItem = [self.purchaseHxResults objectAtIndex:indexPath.row];

	ShoppingListItem *newItem = [ShoppingListItem addOrIncrementItemNamed:purchaseHxItem.name productId:purchaseHxItem.productId
		category:purchaseHxItem.category barcode:purchaseHxItem.barcode quantity:1 inMOC:self.scratchContext];

	[self.scratchContext BL_save];

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:newItem];
	}
}

- (void)purchaseHxCellTapped:(UITapGestureRecognizer *)recognizer
{
}

- (void)quickAddProductButtonTapped:(UITapGestureRecognizer *)recognizer
{
	[self.searchTextField resignFirstResponder];
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    ProductSearchRecord *searchRecord = [self.productResults objectAtIndex:indexPath.row];

	ShoppingListItem *newItem = [ShoppingListItem addOrIncrementItemNamed:searchRecord.name productId:searchRecord.productId
		category:searchRecord.category barcode:searchRecord.barcode quantity:1 inMOC:self.scratchContext];
    
	[self.scratchContext BL_save];

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:newItem];
	}
}

#pragma mark - UITableDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numberOfSections = 3;
        
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    
    if (section == kSectionAddCustomItem) {
        if ([self.searchTerm length] > 0) {
            numberOfRows = 1;
        }
    }
    else if (section == kSectionPurchaseHxResults) {
        numberOfRows = [self.purchaseHxResults count];
		if (self.morePurchaseHx) {
			numberOfRows++;
		}
    }
    else if (section == kSectionProductResults) {
        numberOfRows = [self.productResults count];
		if (self.moreProducts) {
			numberOfRows++;
		}
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *addCustomCellId = @"AddCustomCell";
    static NSString *purchaseHxCellId = @"PurchaseHxCell";
    static NSString *productCellId = @"ProductCell";
    static NSString *showMoreCellId = @"ShowMoreCell";

    UITableViewCell *cell = nil;
    if (indexPath.section == kSectionAddCustomItem) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:addCustomCellId];

		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addFreeTextButtonTapped:)];
		[cell.imageView addGestureRecognizer:recognizer];
    }
	else if (indexPath.section == kSectionPurchaseHxResults) {
		if (indexPath.row < [self.purchaseHxResults count]) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:purchaseHxCellId];

			UIImageView *addImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"green-plus"]];
			addImageView.userInteractionEnabled = YES;
			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddPurchaseHxButtonTapped:)];
			[addImageView addGestureRecognizer:recognizer];
			cell.accessoryView = addImageView;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }
	else if (indexPath.section == kSectionProductResults) {
		if (indexPath.row < [self.productResults count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:productCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddProductButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
	}

	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionAddCustomItem) {
		NSString *addString = [NSString stringWithFormat:@"Add “%@” to shopping list", self.searchTerm];
		if ([cell.textLabel respondsToSelector:@selector(setAttributedText:)]) {
			NSMutableAttributedString *addAttrString = [[NSMutableAttributedString alloc] initWithString:addString 
				attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_mediumDarkGrayColor] }];
			[addAttrString setAttributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_darkGrayColor] }
				range:NSMakeRange(4, [self.searchTerm length] + 2)];
			[cell.textLabel setAttributedText:addAttrString];
		}
		else {
			cell.textLabel.text = addString;
		}
    }
    else if (indexPath.section == kSectionPurchaseHxResults) {
		if (indexPath.row < [self.purchaseHxResults count]) {
			PurchaseHxItem *purchaseHxItem = [self.purchaseHxResults objectAtIndex:indexPath.row];

			cell.textLabel.text = purchaseHxItem.name;

			cell.imageView.image = [[BCImageCache sharedCache] imageWithImageId:purchaseHxItem.imageId
				withItemType:kItemTypeProduct withSize:kImageSizeMedium];
		}
    }
    else if (indexPath.section == kSectionProductResults) {
		if (indexPath.row < [self.productResults count]) {
			ProductSearchRecord *record = [self.productResults objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = record.name;
			bcCell.bcAlertImageView.hidden = [record.allergyCount integerValue] == 0;
			bcCell.bcLeafImageView.hidden = [record.lifestyleCount integerValue] == 0;

			bcCell.bcImageView.image = [[BCImageCache sharedCache] imageWithImageId:record.imageId
				withItemType:kItemTypeProduct withSize:kImageSizeMedium];
		}
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 0.0f;
    switch (section) {
        case kSectionPurchaseHxResults:
            if ([self.purchaseHxResults count] > 0) {
                headerHeight = 22;
            }
            break;
        case kSectionProductResults:
            if ([self.productResults count] > 0) {
                headerHeight = 22;
            }
            break;
        case kSectionAddCustomItem:
        default:
            break;
    }
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = nil;
    switch (section) {
        case kSectionPurchaseHxResults:
            sectionTitle = @"Previously purchased";
            break;
        case kSectionProductResults:
            sectionTitle = @"Search BettrLife";
            break;
        case kSectionAddCustomItem:
        default:
            break;
    }
    
	return [BCTableViewController customViewForHeaderWithTitle:sectionTitle];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44.0f;
    switch (indexPath.section) {
        case kSectionPurchaseHxResults:
			if (indexPath.row == [self.purchaseHxResults count] && self.morePurchaseHx) {
				rowHeight = 44.0f;
			}
			else {
				rowHeight = 66.0f;
			}
			break;
        case kSectionProductResults:
			if (indexPath.row == [self.productResults count] && self.moreProducts) {
				rowHeight = 44.0f;
			}
			else {
				rowHeight = 66.0f;
			}
			break;
		default:
        case kSectionAddCustomItem:
			rowHeight = 44.0f;
			break;
	}	
	return rowHeight;
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.searchTextField resignFirstResponder];
    [theTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case kSectionAddCustomItem:
            // add a custom item with this text
			[self.searchTextField resignFirstResponder];
            [self addFreeTextItem];
            break;
        case kSectionPurchaseHxResults:
		{
			if (indexPath.row < [self.purchaseHxResults count]) {
				PurchaseHxItem *purchaseHxItem = [self.purchaseHxResults objectAtIndex:indexPath.row];

				ShoppingListItem *item = [ShoppingListItem addOrIncrementItemNamed:purchaseHxItem.name productId:purchaseHxItem.productId
					category:purchaseHxItem.category barcode:purchaseHxItem.barcode quantity:1 inMOC:self.scratchContext];

				ShoppingListItemDetailViewController *detailView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingListItemDetail"];
				detailView.delegate = self;
				detailView.shoppingListItem = item;

				[self.navigationController pushViewController:detailView animated:YES];
			}
			else {
				[self fetchPurchaseHxMore:YES containing:self.searchTerm];
			}

			break;
		}
        case kSectionProductResults:
		{
			if (indexPath.row < [self.productResults count]) {
				ProductSearchRecord *searchRecord = [self.productResults objectAtIndex:[indexPath row]];

				ShoppingListItem *item = [ShoppingListItem addOrIncrementItemNamed:searchRecord.name productId:searchRecord.productId
					category:searchRecord.category barcode:searchRecord.barcode quantity:1 inMOC:self.scratchContext];

				ShoppingListItemDetailViewController *detailView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingListItemDetail"];
				detailView.delegate = self;
				detailView.shoppingListItem = item;

				[self.navigationController pushViewController:detailView animated:YES];
			}
			else {
				[self showBackView:YES withLabelText:@"All results for Products"];
				[self sendMoreProductsRequest];
			}

			break;
		}
        default:
            break;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	self.searchTerm = [self.searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	[self.searchTextField resignFirstResponder];
    
	if ([self.searchTerm length] > 0) {
        // clear search results and send a new search request
		[self resetResultArraysExcluding:nil];

		[self fetchLocalResults];
        [self sendFirstSearchRequest];

		// remove the constraint tying the table view to the footer view and 
		// tie it to the bottom of the view instead
		[self.view removeConstraint:self.tableViewBottomConstraint];
		self.tableViewBottomConstraint = 
			[NSLayoutConstraint constraintWithItem:self.tableView
										 attribute:NSLayoutAttributeBottom
										 relatedBy:NSLayoutRelationEqual
											toItem:self.view
										 attribute:NSLayoutAttributeBottom
										multiplier:1.0
										  constant:0];
		[self.view addConstraint:self.tableViewBottomConstraint];

		// 'hide' the footer view under the bottom of the view
		self.footerViewBottomConstraint.constant = -44;

		[UIView animateWithDuration:0.3f animations:^{
			[self.view layoutIfNeeded];
		}];

		self.footerImageView.image = [UIImage imageNamed:@"product-search-footer"];
		self.filtersLabel.text = @"";
		self.footerView.hidden = YES;
	}

	return YES;
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - ShoppingListItemDetailViewDelegate
- (void)detailView:(ShoppingListItemDetailViewController *)shoppingListItemView	didUpdateItem:(ShoppingListItem *)item
{
	DDLogInfo(@"%@:%@", THIS_FILE, THIS_METHOD);

	[self.navigationController popViewControllerAnimated:YES];

	[self.scratchContext BL_save];

	// shouldn't do much here, just let the shopping list view know about the item
	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:item];
	}
}

#pragma mark - ProductSearchFilterViewDelegate
- (void)productSearchFilterView:(ProductSearchFilterViewController *)filterView didFinish:(BOOL)done
{
	self.searchFilters = filterView.filters;
	
	// the filters have changes, so we should start over
	[self resetResultArraysExcluding:nil];

	[self sendMoreProductsRequest];
}

@end
