//
//  BLTrackingAddViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 10/22/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLTrackingAddViewController.h"

#import "NSDictionary+NumberValue.h"
#import "NSString+BCAdditions.h"
#import "TrackingMO.h"
#import "TrackingTypeMO.h"
#import "TrackingValueMO.h"
#import "User.h"

@interface BLTrackingDateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end
@implementation BLTrackingDateCell
@end

@interface BLTrackingDatePickerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@end
@implementation BLTrackingDatePickerCell
@end

@interface BLTrackingEntryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *entryLabel;
@property (weak, nonatomic) IBOutlet UITextField *entryTextField;
@end
@implementation BLTrackingEntryCell
@end

@interface BLTrackingAddViewController ()
@property (assign, nonatomic) NSInteger trackType;
@property (strong, nonatomic) NSArray *trackingValues;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) UITextField *activeField;
@property (assign, nonatomic) BOOL datePickerShown;
@property (strong, nonatomic) NSDate *trackDate;
@property (strong, nonatomic) NSMutableDictionary *valueDictionary;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@end

@implementation BLTrackingAddViewController

enum {
	imageSection = 0,
	dateSection,
	entrySection
};

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		self.dateFormatter = [[NSDateFormatter alloc] init];
		[self.dateFormatter setTimeStyle:NSDateFormatterShortStyle];
		[self.dateFormatter setDateStyle:NSDateFormatterLongStyle];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

	[self registerForKeyboardNotifications];
	self.trackDate = [NSDate date];
	self.trackType = self.trackingTypeMO.cloudIdValue;
	self.trackingValues = [[self.trackingTypeMO.trackingValues filteredSetUsingPredicate:
		[NSPredicate predicateWithFormat:@"%K = NO", TrackingValueMOAttributes.optional]]
		sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:TrackingValueMOAttributes.field ascending:YES]]];
	self.valueDictionary = [NSMutableDictionary dictionary];

	// Make sure that if this TrackingTypeMO gets updated, we reload to avoid problems
	__typeof__(self) __weak weakSelf = self;
	[[NSNotificationCenter defaultCenter] addObserverForName:[TrackingTypeMO entityName] object:nil queue:nil
		usingBlock:^(NSNotification *notification) {
			weakSelf.trackingTypeMO = [TrackingTypeMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@",
					TrackingTypeMOAttributes.loginId, [User loginId], TrackingTypeMOAttributes.cloudId, @(self.trackType)]
				inContext:self.managedObjectContext];

			weakSelf.trackingValues = [[self.trackingTypeMO.trackingValues filteredSetUsingPredicate:
				[NSPredicate predicateWithFormat:@"%K = NO", TrackingValueMOAttributes.optional]]
				sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:TrackingValueMOAttributes.field ascending:YES]]];

			[weakSelf.tableView reloadData];
		}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
	BOOL shouldPerform = YES;

    if ([identifier isEqualToString:@"TrackingAddSave"]) {
		// Validate
		// If all fields are empty, then we cannot allow saving, maybe the tracking meta data should specify requirements somehow?
		if (self.activeField) {
			// There is an active edit going on, grab its value first
			//[self finishEditing];
			CGPoint textFieldPosition = [self.activeField convertPoint:CGPointZero toView:self.tableView];
			NSIndexPath* indexPath = [self.tableView indexPathForRowAtPoint:textFieldPosition];

			[self saveValueFromTextField:self.activeField atIndexPath:indexPath];
		}

		if (![self.valueDictionary count]) {
			// Nothing to save, disallow
			shouldPerform = NO;
		}
		else {
			// If we get here, default to !shouldPerform, and we'll toggle that if we determine it is okay
			shouldPerform = NO;
			// Check to see that there is something to save
			for (NSString *fieldname in self.valueDictionary) {
				if ([self.valueDictionary objectForKey:fieldname]) {
					// There is at least one value in the value dictionary, okay to save
					shouldPerform = YES;
					break;
				}
			}
		}

		if (!shouldPerform) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Values"
				message:@"You must first enter a value to save"
				delegate:self
				cancelButtonTitle:@"OK"
				otherButtonTitles:nil];
			[alert show];
		}
	}

	return shouldPerform;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"TrackingAddSave"]) {
		[self finishEditing];
		// First, massage any values that may need it, such as hours to minutes due to IOS-1166
		for (TrackingValueMO *trackingValue in self.trackingValues) {
			if ([trackingValue.units isEqualToString:@"minutes"]) {
				NSNumber *value = [self.valueDictionary objectForKey:trackingValue.field];
				if (value && [value doubleValue]) {
					// Convert from hours to minutes
					NSInteger newValue = [value doubleValue] * 60;
					[self.valueDictionary setObject:@(newValue) forKey:trackingValue.field];
				}
			}
		}

		[TrackingMO addTrackOfType:self.trackType onDate:self.trackDate withValueDictionary:self.valueDictionary
			inMOC:self.managedObjectContext];

		[self.managedObjectContext BL_save];
	}

}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	switch (section) {
		case imageSection:
			numberOfRows = 1;
			break;
		case dateSection:
			numberOfRows = 2;
			break;
		case entrySection:
			numberOfRows = [self.trackingValues count];
			break;
	}

	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *imageCellId = @"ImageCell";
    static NSString *dateCellId = @"DateCell";
    static NSString *datePickerCellId = @"DatePickerCell";
    static NSString *entryCellId = @"EntryCell";

	UITableViewCell *cell = nil;
	switch (indexPath.section) {
		case imageSection:
			cell = [tableView dequeueReusableCellWithIdentifier:imageCellId];
			break;
        case dateSection: {
			if (indexPath.row == 0) {
				cell = [tableView dequeueReusableCellWithIdentifier:dateCellId];
				BLTrackingDateCell *dateCell = (BLTrackingDateCell *)cell;
				dateCell.dateLabel.text = [self.dateFormatter stringFromDate:self.trackDate];
			}
			else {
				cell = [tableView dequeueReusableCellWithIdentifier:datePickerCellId];
				BLTrackingDatePickerCell *datePickerCell = (BLTrackingDatePickerCell *)cell;
				datePickerCell.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
				datePickerCell.datePicker.date = self.trackDate;
			}
			break;
        }
        case entrySection: {
			cell = [tableView dequeueReusableCellWithIdentifier:entryCellId];
			BLTrackingEntryCell *entryCell = (BLTrackingEntryCell *)cell;
			TrackingValueMO *trackingValueMO = [self.trackingValues objectAtIndex:indexPath.row];

			if ([trackingValueMO.dataType isEqualToString:@"integer"]) {
				entryCell.entryTextField.keyboardType = UIKeyboardTypeNumberPad;
			}
			else if ([trackingValueMO.dataType isEqualToString:@"float"]) {
				entryCell.entryTextField.keyboardType = UIKeyboardTypeDecimalPad;
			}
			else {
				entryCell.entryTextField.keyboardType = UIKeyboardTypeDefault;
			}

			NSString *units = trackingValueMO.units;
			if (units) {
				// Special case for "minutes", allow the user to enter the info in "hours" instead - IOS-1166
				if ([units isEqualToString:@"minutes"]) {
					units = @"hours";
					// Allow decimals, since we're having them enter hours, they may want to put 1.5 or somesuch
					entryCell.entryTextField.keyboardType = UIKeyboardTypeDecimalPad;
				}
				entryCell.entryLabel.text = [NSString stringWithFormat:@"%@ (%@)", trackingValueMO.title, units];
			}
			else {
				entryCell.entryLabel.text = trackingValueMO.title;
			}
			entryCell.entryTextField.placeholder = (units ?: @"");

			break;
        }
	}
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = UITableViewAutomaticDimension;
	if (indexPath.section == imageSection) {
		height = 160;
	}
	else if ((indexPath.section == dateSection) && (indexPath.row == 1)) {
		height = (self.datePickerShown ? 162 : 0);
	}

	return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == dateSection) {
		[self showDatePicker:!self.datePickerShown];

		// Deselect the row, as it looks odd to keep a selection there after expanding/contracting the date picker row
		[[self.tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
	}
}

- (void)showDatePicker:(BOOL)show
{
	if (show && self.activeField) {
		[self.activeField resignFirstResponder];
	}

	self.tableView.scrollEnabled = !show; // Don't allow scrolling while we've got the date picker open

	self.datePickerShown = show;
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:dateSection]]
		withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	CGPoint textFieldPosition = [textField convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath* indexPath = [self.tableView indexPathForRowAtPoint:textFieldPosition];

	[self saveValueFromTextField:textField atIndexPath:indexPath];

	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	if (self.datePickerShown) {
		[self showDatePicker:NO];
	}
	self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	self.activeField = nil;
	CGPoint textFieldPosition = [textField convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath* indexPath = [self.tableView indexPathForRowAtPoint:textFieldPosition];

	[self saveValueFromTextField:textField atIndexPath:indexPath];
}

- (void)finishEditing
{
	if (self.activeField) {
		[self textFieldDidEndEditing:self.activeField];
	}
}

- (void)saveValueFromTextField:(UITextField *)textField atIndexPath:(NSIndexPath *)indexPath
{
	TrackingValueMO *trackingValueMO = [self.trackingValues objectAtIndex:indexPath.row];
	NSNumber *number = [textField.text BL_numberWithPrecision:[trackingValueMO.precision integerValue]];
	NSString *newValue = @"";
	if (number) {
		[self.valueDictionary setObject:number forKey:trackingValueMO.field];
		newValue = [number stringValue];
	}
	textField.text = newValue;
}

#pragma mark - Notifications for keyboard hide/show
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:)
		name:UIKeyboardDidShowNotification object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
		name:UIKeyboardWillHideNotification object:nil];
 
}
 
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
 
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
 
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
        [self.tableView scrollRectToVisible:self.activeField.frame animated:YES];
    }
}
 
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillShowNotification:(NSNotification*)aNotification
{
	NSDictionary *keyboardAnimationDetail = [aNotification userInfo];

	CGRect keyboardEndFrameWindow = [keyboardAnimationDetail[UIKeyboardFrameEndUserInfoKey] CGRectValue];

	double keyboardTransitionDuration  = [keyboardAnimationDetail[UIKeyboardAnimationDurationUserInfoKey] doubleValue];

	CGRect keyboardEndFrameView = [self.view convertRect:keyboardEndFrameWindow fromView:nil];

	[UIView animateWithDuration:keyboardTransitionDuration 
		animations:^{
			self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, 0, self.view.frame.size.height - keyboardEndFrameView.origin.y, 0);
			self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(self.tableView.scrollIndicatorInsets.top, 0, self.view.frame.size.height - keyboardEndFrameView.origin.y, 0);
			[self.view layoutIfNeeded];
		}];
}

- (void)keyboardWillHideNotification:(NSNotification*)aNotification
{
	NSDictionary *keyboardAnimationDetail = [aNotification userInfo];

	double keyboardTransitionDuration  = [keyboardAnimationDetail[UIKeyboardAnimationDurationUserInfoKey] doubleValue];

	[UIView animateWithDuration:keyboardTransitionDuration 
		animations:^{
			self.tableView.contentInset = UIEdgeInsetsZero;
			self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
			[self.view layoutIfNeeded];
		}];
}   
- (IBAction)dateChanged:(UIDatePicker *)sender
{
	self.trackDate = sender.date;

	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:dateSection]]
		withRowAnimation:UITableViewRowAnimationNone];
}

@end
