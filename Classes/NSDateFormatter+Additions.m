//
//  NSDateFormatter+Additions.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 6/24/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "NSDateFormatter+Additions.h"
#import "NSCalendar+Additions.h"

@implementation NSDateFormatter (Additions)
+ (NSString *)relativeStringFromDate:(NSDate *)date
{
	// Initialize the formatter.
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

	[formatter setDateStyle:NSDateFormatterShortStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];

	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDate *baseDate = [calendar BC_startOfDate:date];

	// Iterate through the eight days (tomorrow, today, and the last six).
	int i;
	for (i = 0; i < 7; i++) {
		NSDate *compareDate = [calendar BC_dateByAddingDays:-i toDate:baseDate];
		// Get week day (starts at 1).
		NSInteger weekday = [[calendar components:(NSWeekdayCalendarUnit) fromDate:compareDate] weekday] - 1;

		if ([baseDate compare:compareDate] == NSOrderedSame && i == 0) {
			// return just the time
			[formatter setDateStyle:NSDateFormatterNoStyle];
			[formatter setTimeStyle:NSDateFormatterShortStyle];
			return [formatter stringFromDate:date];
		}
		else if ([baseDate compare:compareDate] == NSOrderedSame && i == 1) {
			// Yesterday
			return @"Yesterday";
		}
		else if ([baseDate compare:compareDate] == NSOrderedSame) {
			// Day of the week
			NSString *day = [[formatter weekdaySymbols] objectAtIndex:weekday];
			return day;
		}
	}

	// It's not in those eight days.
	NSString *defaultDate = [formatter stringFromDate:date];
	return defaultDate;
}
@end
