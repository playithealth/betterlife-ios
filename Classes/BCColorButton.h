//
//  BCColorButton.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/6/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface BCColorButton : UIButton
{
    UIColor *_lowColor;
    UIColor *_highColor;
}

@property (assign) UIColor *highColor;
@property (assign) UIColor *lowColor;
@property (nonatomic, retain) CAGradientLayer *gradientLayer;

- (void)setHighColor:(UIColor*)color;
- (void)setLowColor:(UIColor*)color;

@end