//
//  BLLoggingDataModel.h
//  BettrLife
//
//  Created by Greg Goodrich on 04/22/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BLGoalInfo.h"
#import "BCUtilities.h"
#import "FoodLogMO.h"

@class AdvisorActivityPlanMO;
@class AdvisorMealPlanMO;

@interface BLLoggingDataModel : NSObject

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) AdvisorMealPlanMO *advisorMealPlanMO;
@property (strong, nonatomic) AdvisorActivityPlanMO *advisorActivityPlanMO;
@property (strong, nonatomic) NSArray *nutrients;
@property (strong, nonatomic) BLGoalInfo *goalInfo;
@property (strong, nonatomic) NSMutableArray *foodLogsByLogTime;
@property (strong, nonatomic) NSMutableArray *waterLogs;
@property (strong, nonatomic) NSMutableArray *sugaryDrinkLogs;
@property (strong, nonatomic) NSMutableArray *fruitVeggieLogs;
@property (strong, nonatomic) NSMutableArray *activityLogs;
@property (strong, nonatomic) NSMutableArray *strengthLogs;
@property (strong, nonatomic) NSMutableArray *foodNotes;
@property (strong, nonatomic) NSMutableArray *activityNotes;
@property (strong, nonatomic) NSMutableArray *nutritionByLogTime; ///< An array of dictionaries keyed by nutrients
@property (strong, nonatomic) NSDictionary *totalNutrition;
@property (strong, nonatomic) NSMutableArray *proRatedByLogTime; ///< Array of dictionaries containing min/max dictionaries
@property (assign, nonatomic) NSInteger dayOffset;
@property (strong, nonatomic, readonly) NSDate *startOfLogDate;
@property (strong, nonatomic, readonly) NSDate *endOfLogDate;
@property (assign, nonatomic, readonly) BLLogTime latestLoggedTime;

- (id)initWithMOC:(NSManagedObjectContext *)moc;
- (NSMutableArray *)loadFoodLogsForLogTime:(NSNumber *)logTime;
- (NSMutableArray *)loadWaterLogs;
- (NSMutableArray *)loadSugaryDrinkLogs;
- (NSMutableArray *)loadFruitVeggieLogs;
- (NSMutableArray *)loadActivityLogs;
- (NSMutableArray *)loadFoodNotes;
- (NSMutableArray *)loadActivityNotes;
- (BLSmiley)complianceThroughLogTime:(BLLogTime)logTime;
- (BLSmiley)currentCompliance;

@end
