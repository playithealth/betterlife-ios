//
//  PromotionCell.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 4/11/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PromotionCell : UITableViewCell {
    
    UILabel *discountLabel;
    UILabel *nameLabel;
    UILabel *descriptionLabel;
    UILabel *disclaimerLabel;
    UIImageView *backgroundImageView;
}
@property (nonatomic, strong) IBOutlet UILabel *discountLabel;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) IBOutlet UILabel *disclaimerLabel;
@property (nonatomic, strong) IBOutlet UIImageView *backgroundImageView;

@end
