//
//  ShoppingListItemDetailViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 8/31/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShoppingListItem.h"

@protocol ShoppingListItemDetailViewDelegate;

@interface ShoppingListItemDetailViewController : UIViewController

@property (strong, nonatomic) ShoppingListItem *shoppingListItem;
@property (weak, nonatomic) id<ShoppingListItemDetailViewDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@protocol ShoppingListItemDetailViewDelegate <NSObject>

- (void)detailView:(ShoppingListItemDetailViewController *)detailView didUpdateItem:(ShoppingListItem *)item;

@end
