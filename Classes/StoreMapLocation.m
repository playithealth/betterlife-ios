//
//  StoreMapLocation.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 5/23/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "StoreMapLocation.h"


@implementation StoreMapLocation {
}
@synthesize coordinate = _coordinate;
@synthesize title;
@synthesize subtitle;
@synthesize arrayIndex;

#pragma mark - lifecycle
- (StoreMapLocation *)initWithCoordinate:(CLLocationCoordinate2D)initCoordinate title:(NSString *)initTitle
{
    _coordinate = initCoordinate;
    self.title = initTitle;
    return self;
}


- (CLLocation *)location
{
	return [[CLLocation alloc] initWithLatitude:self.coordinate.latitude
		longitude:self.coordinate.longitude];
}

@end
