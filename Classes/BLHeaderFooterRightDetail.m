//
//  BLHeaderFooterRightDetail.m
//  BettrLife
//
//  Created by Greg Goodrich on 1/07/15.
//  Copyright (c) 2015 BettrLife Corp. All rights reserved.
//

#import "BLHeaderFooterRightDetail.h"

#import "UIColor+Additions.h"

@interface BLHeaderFooterRightDetail ()
@end

@implementation BLHeaderFooterRightDetail

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.contentView.backgroundColor = [UIColor BC_tableViewHeaderColor];

    }
    return self;
}

- (UILabel *)nameLabel
{
	if (_nameLabel) {
		return _nameLabel;
	}

	_nameLabel = [[UILabel alloc] init];
	_nameLabel.numberOfLines = 1;
	_nameLabel.font = [UIFont systemFontOfSize:15];
	_nameLabel.textColor = [UIColor darkGrayColor];
	_nameLabel.textAlignment = NSTextAlignmentRight;
	_nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
	[self.contentView addSubview:_nameLabel];
	[self setConstraints];

    return _nameLabel;
}

- (UILabel *)valueLabel
{
	if (_valueLabel) {
		return _valueLabel;
	}

	_valueLabel = [[UILabel alloc] init];
	_valueLabel.numberOfLines = 1;
	_valueLabel.font = [UIFont systemFontOfSize:15];
	_valueLabel.textColor = [UIColor blackColor];
	_valueLabel.textAlignment = NSTextAlignmentRight;
	_valueLabel.translatesAutoresizingMaskIntoConstraints = NO;
	[self.contentView addSubview:_valueLabel];
	[self setConstraints];
    
    return _valueLabel;
}

- (void)setConstraints
{
	NSMutableDictionary *bindingsDict = [[NSMutableDictionary alloc] init];

	// First, establish the entries in the bindings dictionary, and remove constraints along the way
	if (_nameLabel) {
		[bindingsDict setObject:_nameLabel forKey:@"nameLabel"];
		[_nameLabel removeConstraints:[_nameLabel constraints]];
	}
	if (_valueLabel) {
		[bindingsDict setObject:_valueLabel forKey:@"valueLabel"];
		[_valueLabel removeConstraints:[_valueLabel constraints]];
	}

	[self removeConstraints:[self constraints]];

	// Now re-establish proper constraints, based upon what elements are in play
	if (_nameLabel) {
		// Horizontal constraints for the multi line label
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[nameLabel]-80-|"
			options:0 metrics:nil views:bindingsDict]];
		[self addConstraints:@[[NSLayoutConstraint constraintWithItem:_nameLabel attribute:NSLayoutAttributeCenterY
			relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:-5]]];
	}

	if (_valueLabel) {
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[valueLabel(60)]-16-|"
			options:0 metrics:nil views:bindingsDict]];
		[self addConstraints:@[[NSLayoutConstraint constraintWithItem:_valueLabel attribute:NSLayoutAttributeCenterY
			relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:-5]]];
	}
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
