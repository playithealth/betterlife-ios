//
//  ShoppingListSearchViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/6/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCSearchViewController.h"


@interface ShoppingListSearchViewController : BCSearchViewController

@end
