//
//  ProductSearchView.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/21/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCViewController.h"
#import "CategoryMO.h"
#import "ProductSearchResultsCell.h"

@class ProductSearchView;

@protocol ProductSearchViewDelegate;

@interface ProductSearchView : BCViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
/// Used to pass back messages to the calling view
@property (unsafe_unretained, nonatomic) id <ProductSearchViewDelegate> delegate;
/// Used to pass in a starting search phrase
@property (unsafe_unretained, nonatomic) NSString *inputText;
/// Used to pass in a starting search category, only used if #inputText is also set
@property (unsafe_unretained, nonatomic) CategoryMO *inputCategory;
/// If true, return after one selection, otherwise, user has to use 'back' button
@property (assign, nonatomic) BOOL singleSelect;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet ProductSearchResultsCell *customCell;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIImageView *activeTabImageView;
@property (strong, nonatomic) IBOutlet UIView *scopeBarView;
@property (strong, nonatomic) IBOutlet UIButton *brandScopeButton;
@property (strong, nonatomic) IBOutlet UIButton *categoryScopeButton;
@property (strong, nonatomic) IBOutlet UILabel *categoryScopeLabel;
@property (strong, nonatomic) IBOutlet UILabel *brandScopeLabel;
@property (strong, nonatomic) IBOutlet UIButton *searchScopeButton;

- (IBAction)searchTextDidChange:(id)sender;
- (IBAction)addFreeTextItem:(id)sender;
- (IBAction)searchTabSelected:(id)sender;
- (IBAction)historyTabSelected:(id)sender;
- (IBAction)pantryTabSelected:(id)sender;
- (IBAction)searchScopeTapped:(id)sender;
- (IBAction)categoryScopeTapped:(id)sender;
- (IBAction)brandScopeTapped:(id)sender;
- (IBAction)searchButtonTapped:(id)sender;
- (IBAction)freeTextButtonTapped:(id)sender;
@end

@protocol ProductSearchViewDelegate <NSObject>
@required
- (void)productSearchView:(id) productSearchView
   didSelectProduct:(NSDictionary *)product;
@end
