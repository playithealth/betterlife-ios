//
//  NSNumber+BettrLife.h
//  BettrLife
//
//  Created by Greg Goodrich on 4/17/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (BettrLife)
- (NSNumber *)BL_numberWithPrecision:(NSNumber *)precision;
@end
