//
//  BLConversationViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 5/21/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConversationMO;
@protocol BLConversationDelegate;

@interface BLConversationViewController : UIViewController <UITextFieldDelegate>
//@interface BLConversationViewController : UITableViewController <UITextFieldDelegate>
@property (strong, nonatomic) ConversationMO *conversation;
@property (weak, nonatomic) id<BLConversationDelegate> delegate;
@end

@protocol BLConversationDelegate <NSObject>
@required
- (void)updatedConversation:(ConversationMO *)conversation;
@end

#pragma mark - Custom Cells
@interface MessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bubbleView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@end
