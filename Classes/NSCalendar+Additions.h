//
//  NSCalendar+Additions.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 6/5/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define BCWeekdaySunday 1
#define BCWeekdayMonday 2
#define BCWeekdayTuesday 3
#define BCWeekdayWednesday 4
#define BCWeekdayThursday 5
#define BCWeekdayFriday 6
#define BCWeekdaySaturday 7

@interface NSCalendar (Additions)
- (NSDate *)BC_startOfDate:(NSDate *)date;
- (NSDate *)BC_endOfDate:(NSDate *)date;

- (NSInteger)BC_daysFromDate:(NSDate *)startDate toDate:(NSDate *)endDate;
- (NSInteger)BC_monthsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate;
- (NSInteger)BC_yearsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate;

- (NSDate *)BC_firstOfYearForDate:(NSDate *)baseDate;
- (NSDate *)BC_firstOfYearForToday;
- (NSDate *)BC_lastOfYearForDate:(NSDate *)baseDate;
- (NSDate *)BC_lastOfYearForToday;

- (NSDate *)BC_firstOfMonthForDate:(NSDate *)date;
- (NSDate *)BC_lastOfMonthForDate:(NSDate *)date;

- (NSDate *)BC_firstOfMonthForToday;
- (NSDate *)BC_lastOfMonthForToday;

- (NSDate *)BC_firstOfWeekForDate:(NSDate *)date;
- (NSDate *)BC_lastOfWeekForDate:(NSDate *)date;

- (NSDate *)BC_firstOfWeekForToday;
- (NSDate *)BC_lastOfWeekForToday;

- (NSDate *)BC_dateByAddingDays:(NSInteger)days toDate:(NSDate *)baseDate;
- (NSDate *)BC_dateByAddingMonths:(NSInteger)days toDate:(NSDate *)baseDate;
- (NSDate *)BC_dateByAddingYears:(NSInteger)days toDate:(NSDate *)baseDate;

@end
