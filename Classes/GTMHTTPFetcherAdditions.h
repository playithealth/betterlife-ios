//
//  GTMHTTPFetcherAdditions.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/20/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GTMHTTPFetcher.h"

@interface GTMHTTPFetcher (GTMHTTPFetcherAdditions)
+ (GTMHTTPFetcher *)signedFetcherWithURL:(NSURL *)url;
+ (GTMHTTPFetcher *)signedFetcherWithURL:(NSURL *)url httpMethod:(NSString *)method postData:(NSData *)postData;
+ (GTMHTTPFetcher *)signedFetcherWithURL:(NSURL *)url httpMethod:(NSString *)method body:(NSString *)body;
+ (GTMHTTPFetcher *)signedFetcherWithLoginCredentials:(NSDictionary *)credentials;
+ (GTMHTTPFetcher *)fetcherWithURL:(NSURL *)url httpMethod:(NSString *)method postData:(NSData *)postData;
+ (GTMHTTPFetcher *)fetcherWithURL:(NSURL *)url httpMethod:(NSString *)method body:(NSString *)body;
+ (GTMHTTPFetcher *)oauthConsumerFetcher;
@end
