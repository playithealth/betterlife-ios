//
//  RestaurantMenuCategoriesViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/27/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCTableViewController.h"

#import "RestaurantChain.h"

@interface RestaurantMenuCategoriesViewController : BCTableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) RestaurantChain *restaurantChain;

- (id)initWithRestaurantChain:(RestaurantChain *)restaurantChain;

@end
