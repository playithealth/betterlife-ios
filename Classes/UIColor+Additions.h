//
//  UIColor+Additions.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/1/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "TargetConditionals.h"

#if TARGET_OS_IPHONE
	#import <UIKit/UIKit.h>
	@interface UIColor (Additions)
	#ifndef COLOR_CLASS
		#define COLOR_CLASS UIColor
	#endif
#else
	#import <Cocoa/Cocoa.h>
	@interface NSColor (Additions)
	#ifndef COLOR_CLASS
		#define COLOR_CLASS NSColor
	#endif
#endif

+ (COLOR_CLASS *)BC_colorWithRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue;
+ (COLOR_CLASS *)BC_colorWithRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue alpha:(CGFloat)alpha;
+ (COLOR_CLASS *)BC_colorWithHex:(NSInteger)hex;

+ (COLOR_CLASS *)BC_darkGreenColor;
+ (COLOR_CLASS *)BC_mediumDarkGreenColor;
+ (COLOR_CLASS *)BC_mediumLightGreenColor;
+ (COLOR_CLASS *)BC_lightGreenColor;
+ (COLOR_CLASS *)BC_blueColor;
+ (COLOR_CLASS *)BC_redColor;
+ (COLOR_CLASS *)BC_goldColor;
+ (COLOR_CLASS *)BC_backgroundColor;
+ (COLOR_CLASS *)BC_lightGrayColor;
+ (COLOR_CLASS *)BC_mediumLightGrayColor;
+ (COLOR_CLASS *)BC_mediumDarkGrayColor;
+ (COLOR_CLASS *)BC_darkGrayColor;
+ (COLOR_CLASS *)BC_groupedTableViewCellBackgroundColor;
+ (COLOR_CLASS *)BC_tableViewHeaderColor;
+ (COLOR_CLASS *)BC_tableViewHeaderTextColor;
+ (COLOR_CLASS *)BC_nonEditableTextColor;
+ (COLOR_CLASS *)BL_goalRangeColor;
+ (COLOR_CLASS *)BL_mehTextColor;
+ (COLOR_CLASS *)BL_darkLineColor;
+ (COLOR_CLASS *)BL_lightLineColor;
+ (COLOR_CLASS *)BL_tabColor;
+ (COLOR_CLASS *)BL_linkColor;
+ (COLOR_CLASS *)BL_powderBlueColor;
+ (COLOR_CLASS *)BL_happySmileyColor;
+ (COLOR_CLASS *)BL_mehSmileyColor;
+ (COLOR_CLASS *)BL_sadSmileyColor;

// Graph colors
+ (COLOR_CLASS *)BL_graphBlue;
+ (COLOR_CLASS *)BL_graphGreen;
+ (COLOR_CLASS *)BL_graphRed;
+ (COLOR_CLASS *)BL_graphOrange;
+ (COLOR_CLASS *)BL_graphPurple;
+ (COLOR_CLASS *)BL_graphGray;
+ (COLOR_CLASS *)BL_graphGoal;

// Kosama Theme colors
+ (COLOR_CLASS *)BL_topKosamaBlue;
+ (COLOR_CLASS *)BL_bottomKosamaBlue;

// Food Log colors
+ (COLOR_CLASS *)BL_foodlogPurple;

// Activity Plan colors
+ (COLOR_CLASS *)BL_activityOrange;
+ (COLOR_CLASS *)BL_activityGreen;
@end
