//
//  OAuthRequestParameter.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/28/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OAuthRequestParameter : NSObject { }

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *value;

+ (NSArray *)parametersFromDictionary:(NSDictionary *)inDictionary;
+ (NSString *)parameterStringForParameters:(NSArray *)inParameters;
+ (NSArray *)parametersFromString:(NSString *)inString;

- (NSString *)URLEncodedParameterString;

@end
