//
//  BCActivityLogDetailViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/11/12.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TrainingActivityMO;
@protocol BCDetailViewDelegate;

@interface BCActivityLogDetailViewController : UITableViewController

@property (strong, nonatomic) TrainingActivityMO *activityLog;
@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;

@end
