//
//  QuantityView.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/22/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "QuantityView.h"

#import "NumberValue.h"

@implementation QuantityView
@synthesize delegate;
@synthesize textField;
@synthesize startingQuantity;

#pragma mark - lifecycle
- (void)viewWillAppear:(BOOL)animated {
	textField.text = [startingQuantity stringValue];

	[super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated {
	NSNumber *quantity = [textField.text numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	if (delegate != nil
			&& [delegate conformsToProtocol:@protocol(QuantityViewDelegate)]) {
		[delegate quantityView:self finishedWithQuantity:quantity];
		//[delegate performSelector:@selector(quantityView:finishedWithQuantity:) withObject:self withObject:quantity];
	}
    [super viewWillDisappear:animated];
}
- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	self.textField = nil;
}
- (void)dealloc {
	textField = nil;
}

- (IBAction)numberClicked:(id)sender {
	NSNumber *quantity = [textField.text 
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	if ([quantity isEqualToNumber:startingQuantity]) {
		textField.text = @"";
	}
	NSUInteger tag = [sender tag];
	textField.text = [textField.text stringByAppendingFormat:@"%lu", (unsigned long)tag];
}
- (IBAction)decimalClicked {
	textField.text = [textField.text stringByAppendingString:@"."];
}
- (IBAction)backspaceClicked {
	NSUInteger length = [textField.text length];
	if (length > 0) {
		textField.text = [textField.text substringToIndex:[textField.text length] - 1];
	}
}


@end
