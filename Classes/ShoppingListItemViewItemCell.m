//
//  ShoppingListItemViewItemCell.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/1/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "ShoppingListItemViewItemCell.h"

#import "StarRatingControl.h"

@implementation ShoppingListItemViewItemCell
@synthesize imageView;
@synthesize textLabel;
@synthesize ratingControl;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    //[imageView release], imageView = nil;
    textLabel = nil;
    ratingControl = nil;

}

@end
