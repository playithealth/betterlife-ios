//
//  HTTPQueue.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 7/7/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "HTTPQueue.h"
#import "BCObjectManager.h"
#import "BCAppDelegate.h"
#import "TaskQueueItems.h"

@interface HTTPQueueItem ()
@property (strong, nonatomic) GTMHTTPFetcher *fetcher;
@property (copy, nonatomic) BC_HTTPQueueCompletionBlock completionBlock;
@end

@implementation HTTPQueueItem

- (id)initWithFetcher:(GTMHTTPFetcher *)inFetcher
{
	if ((self = [super init])) {
		self.fetcher = inFetcher;
	}

	return self;
}

- (void)cancel
{
	// Cancel this http request, probably should only be called if this task is running,
	// but I believe it will be okay either way
	if (self.fetcher) {
		[self.fetcher stopFetching];
	}
}

@end

@interface HTTPQueue()
@end

@implementation HTTPQueue

#pragma mark - life cycle
+ (id)queueNamed:(NSString *)queueIdentifier completionBlock:(BC_TaskQueueCompletionBlock)completionBlock
{
	HTTPQueue *newQueue = [[HTTPQueue alloc] initWithIdentifier:queueIdentifier completionBlock:completionBlock];
	if (newQueue && ![TaskQueueItems registerQueue:newQueue withIdentifier:queueIdentifier]) {
		newQueue = nil;
	}

	return newQueue;
}

#pragma mark - local methods
- (void)addRequest:(GTMHTTPFetcher *)fetcher completionBlock:(BC_HTTPQueueCompletionBlock)completionBlock
{
	HTTPQueueItem *newItem = [[HTTPQueueItem alloc] initWithFetcher:fetcher];

	newItem.completionBlock = ^void(NSData *data, NSError *error) {
		if (completionBlock) {
			completionBlock(data, error);
		}

		// We're finished with this request
		self.runningRequest = nil;

		// We want to ignore 400 errors, and just continue on
		if (error && (error.code == 400)) {
			error = nil;
		}

		// Invoke the method to process the next item
		[self runQueue:error];
	};
	
	if (!self.queuedRequests) {
		self.queuedRequests = [[NSMutableArray alloc] init];
	}

	[self.queuedRequests addObject:newItem];
}

- (void)runQueue:(NSError *)error
{
	if ([self.queuedRequests count]) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		HTTPQueueItem *nextRequest = [self.queuedRequests objectAtIndex:0];
		self.runningRequest = nextRequest;
		[self.queuedRequests removeObjectAtIndex:0];
		if (error) {
			// Came into runQueue with an error, just fire the completion block and pass it the error,
			// which will effectively ensure that all item completion blocks get called, but no more
			// requests get fired
			nextRequest.completionBlock(nil, error);
		}
		else if (!appDelegate.connected) {
			error = [NSError errorWithDomain:@"Cloud API Error" code:NoConnectivity userInfo:nil];
			nextRequest.completionBlock(nil, error);
		}
		else if (!nextRequest.fetcher) {
			error = [NSError errorWithDomain:@"Cloud API Error" code:UnspecifiedError userInfo:nil];
			nextRequest.completionBlock(nil, error);
		}
		else {
			// We have a fetcher and connectivity, go ahead and fire the call
			DDLogVerbose(@"Firing %@", nextRequest.fetcher.mutableRequest.URL);
			[nextRequest.fetcher beginFetchWithCompletionHandler:nextRequest.completionBlock];
		}
	}
	// Ran out of queued items, call finished to notify the delegate
	else {
		[self finished:error];
	}
}

@end
