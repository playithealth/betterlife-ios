//
//  UIBarButtonItemAdditions.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/19/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIBarButtonItem (UIBarButtonItemAdditions)
+ (id)barButtonItemWithImageName:(NSString *)imageName 
	target:(id)target action:(SEL)selector;
- (id)initWithImageName:(NSString *)imageName 
	target:(id)target action:(SEL)selector;
- (id)initWithImageName:(NSString *)imageName 
	labelText:(NSString *)text target:(id)target action:(SEL)selector;
@end
