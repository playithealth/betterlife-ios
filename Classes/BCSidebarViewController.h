//
//  BCSidebarViewController.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 12/1/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;

#define kSBDisplayName			@"name"
#define kSBImage				@"image"
#define kSBStoryboardName		@"storyboardName"
#define kSBStoryboardID			@"storyboardID"
#define kSBBadgeCallback		@"badgeCallback"
#define kSBBadgeCountCallback	@"badgeCountCallback"
#define kSBCallbackContextObj	@"callbackContextObj"
#define kSBPropertiesDictionary	@"propertiesDict"

typedef NSUInteger (^BadgeCountCallback) (NSManagedObjectContext *moc, User *user);
typedef BOOL (^BadgeCallback) (NSManagedObjectContext *moc, User *user, id callbackContextObj);

@interface BCSidebarViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (assign, nonatomic) BOOL disableClosing; ///< This is a special flag that can stop the sidebar from being closed unless a selection is made

- (void)updateBadges;
- (NSDictionary *)currentViewInfo;
- (void)userLoggedOut;
- (void)userLoggedIn;

@end
