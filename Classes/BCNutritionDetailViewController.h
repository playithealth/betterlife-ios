//
//  BCNutritionDetailViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 5/21/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCDetailViewDelegate.h"

#import "BLGoalInfo.h"

@class NutritionMO;

@interface BCNutritionDetailViewController : UIViewController

@property (weak, nonatomic) id<BCDetailViewDelegate>delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSString *nutrientName; ///< Not using a nutrientMO object because of sync possibly deleting it, safer this way
@property (strong, nonatomic) NSString *nutrientUnits;
@property (strong, nonatomic) NSString *nutrientDisplayName;
@property (strong, nonatomic) BLGoalInfo *goalInfo;
@property (assign, nonatomic) NSInteger scopeIndex;
@property (strong, nonatomic) NSArray *foodLogEntries;

@end
