//
//  BLProductItem.h
//  BettrLife
//
//  Created by Greg Goodrich on 9/15/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

@class CategoryMO;

@protocol BLProductItem <NSObject>
@property (strong, nonatomic) CategoryMO *category;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *productId;
@end
