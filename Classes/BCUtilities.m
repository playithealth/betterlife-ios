//
//  BCUtilities.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/6/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "BCUtilities.h"

#import "BCUrlFactory.h"
#import "NSCalendar+Additions.h"
#import "OAuthConsumer.h"
#import "OAuthRequestParameter.h"
#import "OAuthUtilities.h"
#import "User.h"

#define kOAuthSignature		  @"oauth_signature"
#define kOAuthNOnce			  @"oauth_nonce"
#define kOAuthToken			  @"oauth_token"
#define kOAuthConsumerKey	  @"oauth_consumer_key"
#define kOAuthTimeStamp		  @"oauth_timestamp"
#define kOAuthVersion		  @"oauth_version"
#define kOAuthSignatureMethod @"oauth_signature_method"


@implementation BCUtilities

+ (NSString *)applicationDocumentsDirectory
{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+ (void)logHttpError:(NSError *)error inFunctionNamed:(const char *)functionName withFetcher:(GTMHTTPFetcher *)fetcher response:(NSString *)response
{
	NSString *authHeaders = @"";
	if ([error code] == 401) {
        authHeaders = [NSString stringWithFormat:@", recv headers - %@", [[fetcher responseHeaders] description]];
	}

	if ([fetcher.mutableRequest HTTPBody]) {
		NSString *body = [[NSString alloc] initWithData:[fetcher.mutableRequest HTTPBody] encoding:NSUTF8StringEncoding];
		NSLog(@"%s - Method: %@, URL:%@ received a \"%ld - %@\" response %@message body: %@",
			  functionName, [fetcher.mutableRequest HTTPMethod],
			  [[fetcher.mutableRequest URL] absoluteString], (long)[error code],
			  [error localizedDescription], authHeaders, body);
	}
	else {
		NSLog(@"%s - Method: %@, URL:%@ received a \"%ld - %@\" response %@",
			  functionName, [fetcher.mutableRequest HTTPMethod],
			  [[fetcher.mutableRequest URL] absoluteString], (long)[error code],
			  [error localizedDescription], authHeaders);
	}
}

+ (void)logHttpError:(NSError *)error inFunctionNamed:(const char *)functionName withFetcher:(GTMHTTPFetcher *)fetcher retrievedData:(NSData *)data
{
	[BCUtilities logHttpError:error inFunctionNamed:functionName withFetcher:fetcher response:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
}

+ (CGRect)fixRect:(CGRect)rect
{
	CGRect newRect;

	newRect.origin.x = rect.origin.y;
	newRect.origin.y = rect.origin.x;
	newRect.size.width = rect.size.height;
	newRect.size.height = rect.size.width;
	return newRect;
}

+ (CGRect)fixOriginRotationOfRect:(CGRect)rect orientation:(UIInterfaceOrientation)orientation
	parentWidth:(int)parentWidth parentHeight:(int)parentHeight
{
	CGRect newRect;

	switch (orientation) {
		case UIInterfaceOrientationLandscapeLeft:
			newRect = CGRectMake(parentWidth - (rect.size.width + rect.origin.x), rect.origin.y, rect.size.width, rect.size.height);
			break;
		case UIInterfaceOrientationLandscapeRight:
			newRect = CGRectMake(rect.origin.x, parentHeight - (rect.size.height + rect.origin.y), rect.size.width, rect.size.height);
			break;
		case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationUnknown:
			newRect = rect;
			break;
		case UIInterfaceOrientationPortraitUpsideDown:
			newRect = CGRectMake(parentWidth - (rect.size.width + rect.origin.x), parentHeight - (rect.size.height + rect.origin.y), rect.size.width, rect.size.height);
			break;
	}
	return newRect;
}

+ (NSNumber *)calculateBmiForWeightInPounds:(NSNumber *)weight heightInInches:(NSNumber *)height
{
	// english: bmi = weigh in lbs / height in inches squared x 703
	float calc = [weight floatValue] / pow([height floatValue], 2) * 703;

	return [NSNumber numberWithFloat:calc];
}

+ (NSNumber *)calculateBmiForMassInKg:(NSNumber *)mass heightInCm:(NSNumber *)height
{
	// metric: bmi = weigh in kilograms / height in meters squared
	float calc = [mass floatValue] / pow([height floatValue], 2);

	return [NSNumber numberWithFloat:calc];
}

+ (NSNumber *)calculateBmrForGender:(NSInteger)gender ageInYears:(NSInteger)years weightInPounds:(float)pounds heightInInches:(float)inches usingEquation:(NSInteger)equation
{
	return [BCUtilities calculateBmrForGender:gender ageInYears:years massInKilograms:(pounds * 0.45359237) heightInCentimeters:(inches * 2.54) usingEquation:equation];
}

+ (NSNumber *)calculateBmrForGender:(NSInteger)gender ageInYears:(NSInteger)years massInKilograms:(float)kilograms heightInCentimeters:(float)centimeters usingEquation:(NSInteger)equation
{
	float calc = 0.0;

	if (equation == kBMREquationRevisedHarrisBenedict) {
		if (gender == 1) {     // female
			// metric: BMR = 655 + ( 9.6 x mass in kilograms ) + ( 1.8 x height in centimeters ) - ( 4.7 x age in years )
			calc = 655 + (9.6 * kilograms) + (1.8 * centimeters) - (4.7 * years);
		}
		else if (gender == 2) {     // male
			// metric: BMR = 66 + ( 13.7 x mass in kilograms ) + ( 5 x height in centimeters ) - ( 6.8 x age in years )
			calc = 66 + (13.7 * kilograms) + (5.0 * centimeters) - (6.8 * years);
		}
	}
	else if (equation == kBMREquationMifflinStJeor) {
		NSInteger genderAdjustment = 0;

		if (gender == 1) { // female
			genderAdjustment = -161;
		}
		else if (gender == 2) { // male
			genderAdjustment = 5;
		}

		// metric: BMR = ( 10.0 * mass in kilograms ) + ( 6.25 * height in centimeters ) - ( 5 x age in years ) + genderAdjustment
		calc = (10.0 * kilograms) + (6.25 * centimeters) - (5 * years) + genderAdjustment;
	}
	else {
		calc = 0;
	}

	return [NSNumber numberWithFloat:calc];
}

@end
