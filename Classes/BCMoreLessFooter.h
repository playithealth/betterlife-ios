//
//  BCMoreLessFooter.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 1/18/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BCMoreLessFooterDelegate;

@interface BCMoreLessFooter : UIImageView

- (id)initWithExpandText:(NSString *)inExpandText collapseText:(NSString *)inCollapseText
delegate:(id<BCMoreLessFooterDelegate>)delegate;
- (id)initWithDelegate:(id<BCMoreLessFooterDelegate>)delegate;

- (BOOL)isExpanded;
- (BOOL)isCollapsed;
@end

@protocol BCMoreLessFooterDelegate <NSObject>
-(void)moreLessFooterTapped:(BCMoreLessFooter *)footer;
@end
