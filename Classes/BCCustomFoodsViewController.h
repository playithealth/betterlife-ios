//
//  BCCustomFoodsViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 12/16/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCDetailViewDelegate.h"

@interface BCCustomFoodsViewController : UITableViewController <BCDetailViewDelegate>
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end
