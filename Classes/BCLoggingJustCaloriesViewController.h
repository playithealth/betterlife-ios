//
//  BCLoggingJustCaloriesViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/25/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BCDetailViewDelegate;
@class FoodLogMO;

@interface BCLoggingJustCaloriesViewController : UIViewController
@property (strong, nonatomic) FoodLogMO *foodLog;
@property (weak, nonatomic) IBOutlet UITextField *caloriesField;
@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;
@end
