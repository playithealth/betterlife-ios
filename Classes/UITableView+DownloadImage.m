//
//  UITableView+DownloadImage.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 12/13/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "UITableView+DownloadImage.h"
#import <objc/runtime.h>

#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"

@interface BLImageTracker : NSObject
@property (nonatomic, strong) NSMutableDictionary *sectionIndices;
@property (nonatomic, strong) GTMHTTPFetcher *imageFetcher;
- (void)clearIndexPaths;
@end // interface BLImageTracker

@implementation BLImageTracker
- (BLImageTracker *)initWithIndexPath:(NSIndexPath *)indexPath
{
	self = [super init];
	if (self) {
		if (indexPath) {
			NSMutableIndexSet *rowIndices = [NSMutableIndexSet indexSetWithIndex:[indexPath row]];
			_sectionIndices = [NSMutableDictionary dictionaryWithObject:rowIndices forKey:@([indexPath section])];
		}
		else {
			_sectionIndices = [NSMutableDictionary dictionary];
		}
	}

	return self;
}


- (void)addIndexPath:(NSIndexPath *)indexPath
{
	if (!indexPath) {
		return;
	}

	NSNumber *curSection = @([indexPath section]);
	NSMutableIndexSet *rowIndices = [self.sectionIndices objectForKey:curSection];
	if (!rowIndices) {
		rowIndices = [NSMutableIndexSet indexSetWithIndex:[indexPath row]];
		[self.sectionIndices setObject:rowIndices forKey:curSection];
	}
	else {
		[rowIndices addIndex:[indexPath row]];
	}
}

- (NSMutableArray *)indexPaths
{
	// Build an array of index paths from the internal structure
	NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
	[self.sectionIndices enumerateKeysAndObjectsUsingBlock:^(id key, id rowSet, BOOL *stop) {
		NSNumber *section = key;
		[rowSet enumerateIndexesUsingBlock:^(NSUInteger row, BOOL *stop) {
			[indexPaths addObject:[NSIndexPath indexPathForRow:row inSection:[section integerValue]]];
		}];
	}];

	return indexPaths;
}

- (void)clearIndexPaths
{
	self.sectionIndices = nil;
}
@end // implementation BLImageTracker

@implementation UITableView (DownloadImage)

static char imageFetchersKey;
static char *imageTrackersKey = "trackers key";

// This category method will attempt to download an image, given the imageKey, and set it into the cache, and
// then reload the table cell for the given index path when done. It will filter out duplicate attempts, and deals
// with them by adding the indexPath to the list of indexPaths that need to be refreshed once the image is downloaded
// cellForRowAtIndexPath should be fetching the image via the image cache ONLY
// This method uses MEDIUM sized images
- (void)downloadImageWithImageURL:(NSURL *)imageURL withImageKey:(NSString *)imageKey forIndexPath:(NSIndexPath *)indexPath
{
	// If this image is already in the image cache, then nothing to do
	if ([[BCImageCache sharedCache] imageForKey:imageKey]) {
		return;
	}

	NSMutableDictionary *imageTrackers = (NSMutableDictionary *)objc_getAssociatedObject(self, &imageTrackersKey);
	if (!imageTrackers) {
		imageTrackers = [[NSMutableDictionary alloc] init];
		objc_setAssociatedObject(self, &imageTrackersKey, imageTrackers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	}

	// See if our imageKey is already in the trackers, this keeps us from firing two fetches for the same image
	BLImageTracker *tracker = [imageTrackers objectForKey:imageKey];
	if (!tracker) {
		// Our imageKey is not in the trackers, need to download once we add our IndexPath
		DDLogImageCache(@"Need to download imageKey %@, adding indexPath %@", imageKey, indexPath);
		tracker = [[BLImageTracker alloc] initWithIndexPath:indexPath];
		[imageTrackers setObject:tracker forKey:imageKey];

		DDLogImageCache(@"Fetching image from %@", imageURL);
		tracker.imageFetcher = [GTMHTTPFetcher signedFetcherWithURL:imageURL];

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		__typeof__(self) __weak weakself = self;
		[tracker.imageFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {            
			[appDelegate setNetworkActivityIndicatorVisible:NO];

			UIImage *image = nil;
			if (error != nil) {
				if (error.code == 404) {
					// If we got a 404, then leave a null placeholder, which tells us that there is no image
					[[BCImageCache sharedCache] setImage:[NSNull null] forKey:imageKey];
				}
				else {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:tracker.imageFetcher retrievedData:retrievedData];
				}
			}
			else {
				image = [[UIImage alloc] initWithData:retrievedData];
				if (image) {
					DDLogImageCache(@"Storing image from %@", imageURL);
					[[BCImageCache sharedCache] setImage:image forKey:imageKey];
				}
				else {
					[[BCImageCache sharedCache] setImage:[NSNull null] forKey:imageKey];
				}
			}

			NSMutableArray *indexPaths = [tracker indexPaths];
			if ([indexPaths count]) {
				DDLogImageCache(@"Reloading rows for imageKey %@ at index paths: %@", imageKey, indexPaths);
				NSArray *visibleIndexPaths = [weakself indexPathsForVisibleRows];
				// Only reload the row if it is visible
				NSArray *indexPathsCopy = [indexPaths copy];
				for (NSIndexPath *indexPath in indexPathsCopy) {
					if (![visibleIndexPaths containsObject:indexPath]) {
						// This one wasn't found in the visible index paths, remove it so it doesn't get reloaded
						[indexPaths removeObject:indexPath];
					}
				}
				if ([indexPaths count]) {
					[weakself reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
				}
			}
			[imageTrackers removeObjectForKey:imageKey];
		}];
	}
	else {
		// Our imageKey is in the dictionary, so a download has already been initiated, just add our IndexPath
		DDLogImageCache(@"Already downloading imageKey %@, adding indexPath %@", imageKey, indexPath);
		[tracker addIndexPath:indexPath];
	}
}

- (void)downloadImageWithLoginId:(NSNumber *)loginId inCommunity:(NSNumber *)communityId forIndexPath:(NSIndexPath *)indexPath
	withSize:(NSString *)size
{
	// If no loginId or communityId was given, then nothing to do
	if (!loginId || !communityId) {
		return;
	}

	NSURL *url = [BCUrlFactory imageURLForUserId:loginId inCommunity:communityId imageSize:size];
	NSString *imageKey = [BCImageCache imageKeyForLoginId:loginId inCommunity:communityId withSize:size];

	[self downloadImageWithImageURL:url withImageKey:imageKey forIndexPath:indexPath];
}

// This category method will attempt to download an image, given the imageId, and set it into the cache, and
// then reload the table cell for the given index path when done. It will filter out duplicate attempts, and deals
// with them by adding the indexPath to the list of indexPaths that need to be refreshed once the image is downloaded
// cellForRowAtIndexPath should be fetching the image via the image cache ONLY
// This method uses MEDIUM sized images
- (void)downloadImageWithImageId:(NSNumber *)imageId forIndexPath:(NSIndexPath *)indexPath withSize:(NSString *)size
{
	// If no imageId was given, then nothing to do
	if (!imageId) {
		return;
	}

	NSURL *url = [BCUrlFactory imageURLForImage:imageId imageSize:size];
	NSString *imageKey = [BCImageCache imageKeyForImageId:imageId withSize:size];

	[self downloadImageWithImageURL:url withImageKey:imageKey forIndexPath:indexPath];
}

- (void)downloadImageWithCoachId:(NSNumber *)coachId forIndexPath:(NSIndexPath *)indexPath withSize:(NSString *)size
{
	// If no imageId was given, then nothing to do
	if (!coachId) {
		return;
	}

	NSURL *url = [BCUrlFactory imageURLForCoachId:coachId imageSize:size];
	NSString *imageKey = [BCImageCache imageKeyForCoachId:coachId withSize:size];

	[self downloadImageWithImageURL:url withImageKey:imageKey forIndexPath:indexPath];
}

- (void)downloadImageWithImageURL:(NSString *)imageURL forIndexPath:(NSIndexPath *)indexPath completionBlock:(BC_DownloadImageBlock)completionBlock
{
	NSMutableDictionary *imageFetchers = (NSMutableDictionary *)objc_getAssociatedObject(self, &imageFetchersKey);
	if (!imageFetchers) {
		imageFetchers = [[NSMutableDictionary alloc] init];
		objc_setAssociatedObject(self, &imageFetchersKey, imageFetchers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	}

	GTMHTTPFetcher *imageFetcher = [imageFetchers objectForKey:indexPath];
	if (imageFetcher == nil) {
		// NOTE this can't be a signed fetcher because the images are from outside sources
		imageFetcher = [GTMHTTPFetcher fetcherWithURLString:imageURL];
		[imageFetchers setObject:imageFetcher forKey:indexPath];
        
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

        [imageFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
			UIImage *image = nil;
			if (error != nil) {
				if (error.code != 404) {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:imageFetcher retrievedData:retrievedData];
				}
			}
			else {
				image = [[UIImage alloc] initWithData:retrievedData];
				if (image) {
					[[BCImageCache sharedCache] setImage:image forImageURL:imageURL size:kImageSizeMedium];
				}
				else {
					[[BCImageCache sharedCache] setImage:[NSNull null] forImageURL:imageURL size:kImageSizeMedium];
				}
			}

			completionBlock(error, image);

            [imageFetchers removeObjectForKey:indexPath];
        }];
	}
}

- (void)clearTrackerIndexPaths
{
	NSMutableDictionary *imageTrackers = (NSMutableDictionary *)objc_getAssociatedObject(self, &imageTrackersKey);
	for (NSString *imageKey in imageTrackers) {
		DDLogImageCache(@"Clearing indexPaths for key %@", imageKey);
		[[imageTrackers objectForKey:imageKey] clearIndexPaths];
	}
}

- (void)cancelAllImageTrackers
{
	NSMutableDictionary *imageTrackers = (NSMutableDictionary *)objc_getAssociatedObject(self, &imageTrackersKey);
	if (imageTrackers) {
		DDLogImageCache(@"Cancelling all image trackers - count %ld", (long)[imageTrackers count]);
	}
	NSArray *allFetchers = [imageTrackers valueForKeyPath:@"imageFetcher"];
	[allFetchers makeObjectsPerformSelector:@selector(stopFetching)];

	objc_setAssociatedObject(self, &imageTrackersKey, NULL, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)cancelAllImageFetchers
{
	NSMutableDictionary *imageFetchers = (NSMutableDictionary *)objc_getAssociatedObject(self, &imageFetchersKey);
	NSArray *allDownloads = [imageFetchers allValues];
	[allDownloads makeObjectsPerformSelector:@selector(stopFetching)];

	objc_setAssociatedObject(self, &imageFetchersKey, NULL, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
