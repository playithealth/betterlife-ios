//
//  BLOnboardActivityViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 3/26/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLOnboardActivityViewController.h"

#import "BLHeaderFooterMultiline.h"
#import "BLOnboardActivitySelectionViewController.h"
#import "BLOnboardWelcomeViewController.h"
#import "UIColor+Additions.h"
#import "UserProfileMO.h"

@interface BLOnboardActivityViewController ()
@property (weak, nonatomic) IBOutlet UILabel *activityLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityLevelDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *nutritionEffortLabel;
@property (weak, nonatomic) IBOutlet UILabel *nutritionLevelDescriptionLabel;

@property (strong, nonatomic) UserProfileMO *userProfileMO;
@property (strong, nonatomic) NSArray *activityLevels;
@property (strong, nonatomic) NSArray *nutritionEfforts;
@property (assign, nonatomic) NSInteger selectedActivityLevel;
@property (assign, nonatomic) NSInteger selectedNutritionEffort;

@property (strong, nonatomic) NSMutableAttributedString *activityFooterAttributedString;
@end

@implementation BLOnboardActivityViewController

static const NSInteger kSectionActivityLevel = 0;
static const NSInteger kSectionNutritionEffort = 1;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	[self.tableView registerClass:[BLHeaderFooterMultiline class] forHeaderFooterViewReuseIdentifier:@"Multiline"];

	self.title = [NSString stringWithFormat:@"Welcome: %ld of %ld",
		(long)[[self.navigationController viewControllers] count] - 1, (long)[self.viewStack count] - 1];

	self.activityLevels = @[
		@{ @"title" : @"Using Fitness Tracking Device", @"description" : @"Activities logged from fitness tracking device",
			@"value" : @(kActivityLevelValueTrackingDevice) },
		@{ @"title" : @"Sedentary/Manual", @"description" : @"Little or no exercise/Log activities by hand",
			@"value" : @(kActivityLevelValueSedentary) },
		@{ @"title" : @"Light", @"description" : @"Light exercise/sports 1-3 days per week", @"value" : @(kActivityLevelValueLight) },
		@{ @"title" : @"Moderate", @"description" : @"Moderate exercise/sports 3-5 days per week", @"value" : @(kActivityLevelValueModerate) },
		@{ @"title" : @"Active", @"description" : @"Hard exercise/sports 5-7 days per week", @"value" : @(kActivityLevelValueActive) },
		@{ @"title" : @"Very Active", @"description" : @"Very hard exercise/sports, physical job, or 2x training",
			@"value" : @(kActivityLevelValueVeryActive) }
	];
	self.nutritionEfforts = @[
		@{ @"title" : @"None", @"description" : @"Maintain same weight", @"value" : @(0) },
		@{ @"title" : @"Easy", @"description" : @"0.5 lb. per week, 250 Calories per day", @"value" : @(250) },
		@{ @"title" : @"Moderate", @"description" : @"1.0 lb. per week, 500 Calories per day", @"value" : @(500) },
		@{ @"title" : @"Hard", @"description" : @"1.5 lb. per week, 750 Calories per day", @"value" : @(750) },
		@{ @"title" : @"Very Hard", @"description" : @"2.0 lb. per week, 1000 Calories per day", @"value" : @(1000) }
	];

	self.userProfileMO = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	// Color the 'BMR' in blue to designate a link
	NSString *footerString = @"This value along with your basic info is used to calculate your maximum Calories per day (via BMR)";
	self.activityFooterAttributedString = [[NSMutableAttributedString alloc] initWithString:footerString];
	[self.activityFooterAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14]
		range:NSMakeRange(0, [self.activityFooterAttributedString length])];
	[self.activityFooterAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor BC_colorWithHex:0x157efb]
		range:[footerString rangeOfString:@"BMR"]];

	if (!self.userProfileMO.activityLevel) {
		self.userProfileMO.activityLevel = [[self.activityLevels objectAtIndex:1] valueForKey:@"value"];
	}
	if (!self.userProfileMO.nutritionEffort) {
		self.userProfileMO.nutritionEffort = [[self.nutritionEfforts objectAtIndex:2] valueForKey:@"value"];
	}

	[self updateView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - local
- (void)updateView
{
	if (self.userProfileMO) {
		NSDictionary *activityLevel = nil;
		for (NSInteger idx = 0; idx < [self.activityLevels count]; idx++) {
			NSDictionary *anActivityLevel = [self.activityLevels objectAtIndex:idx];
			if ([[anActivityLevel valueForKey:@"value"] doubleValue] == [self.userProfileMO.activityLevel doubleValue]) {
				self.selectedActivityLevel = idx;
				activityLevel = anActivityLevel;
				break;
			}
		}
		if (!activityLevel) {
			activityLevel = [self.activityLevels objectAtIndex:0];
		}
		self.activityLevelLabel.text = [activityLevel valueForKey:@"title"];
		self.activityLevelDescriptionLabel.text = [activityLevel valueForKey:@"description"];

		NSDictionary *nutritionEffort = nil;
		for (NSInteger idx = 0; idx < [self.nutritionEfforts count]; idx++) {
			NSDictionary *anNutritionEffort = [self.nutritionEfforts objectAtIndex:idx];
			if ([[anNutritionEffort valueForKey:@"value"] integerValue] == [self.userProfileMO.nutritionEffort integerValue]) {
				self.selectedNutritionEffort = idx;
				nutritionEffort = anNutritionEffort;
				break;
			}
		}
		if (!nutritionEffort) {
			nutritionEffort = [self.nutritionEfforts objectAtIndex:0];
		}
		self.nutritionEffortLabel.text = [nutritionEffort valueForKey:@"title"];
		self.nutritionLevelDescriptionLabel.text = [nutritionEffort valueForKey:@"description"];
	}
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat height = UITableViewAutomaticDimension;
	switch (section) {
		case kSectionActivityLevel:
			height = 84;
			break;
		case kSectionNutritionEffort:
			height = 22;
			break;
	}

	return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	CGFloat height = UITableViewAutomaticDimension;
	switch (section) {
		case kSectionActivityLevel:
			height = 60;
			break;
		case kSectionNutritionEffort:
			height = 60;
			break;
	}

	return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];

	switch (section) {
		case kSectionActivityLevel:
			headerView.multilineLabel.text = @"All of this information is optional, but the more you enter, "
				"the more we can do to help you manage your health.";
			headerView.normalLabel.text = @"ACTIVITY";
			break;
		case kSectionNutritionEffort:
			headerView.normalLabel.text = @"EFFORT";
			break;
	}

	return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];

	switch (section) {
		case kSectionActivityLevel:
			footerView.multilineLabel.attributedText = self.activityFooterAttributedString;
			if (![footerView.contentView.gestureRecognizers count]) {
				UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bmrTapped:)];
				[footerView.contentView addGestureRecognizer:recognizer];
			}
			break;
		case kSectionNutritionEffort:
			footerView.multilineLabel.text = @"How much you want your weight to change per week. "
				"Also adjusts up/down your maximum Calories per day.";
			break;
	}

	return footerView;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"Activity"]) {
		BLOnboardActivitySelectionViewController *controller = [segue destinationViewController];
		controller.delegate = self;
		controller.items = self.activityLevels;
		controller.isActivityLevel = YES;
		controller.selectedRow = self.selectedActivityLevel;
	}
	else if ([segue.identifier isEqualToString:@"Effort"]) {
		BLOnboardActivitySelectionViewController *controller = [segue destinationViewController];
		controller.delegate = self;
		controller.items = self.nutritionEfforts;
		controller.isActivityLevel = NO;
		controller.selectedRow = self.selectedNutritionEffort;
	}
}

#pragma mark - responders
- (IBAction)nextViewController:(id)sender {
	if (self.delegate) {
		[self.delegate nextView];
	}
}

- (void)bmrTapped:(UITapGestureRecognizer *)recognizer
{
	UIViewController *bmrView = [self.storyboard instantiateViewControllerWithIdentifier:@"BMR Info Modal"];
	[self presentViewController:bmrView animated:YES completion:nil];
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	if (object && [object isKindOfClass:[NSDictionary class]]) {
		NSInteger idx = [self.activityLevels indexOfObject:object];
		if (idx != NSNotFound) {
			self.selectedActivityLevel = idx;
			self.userProfileMO.activityLevel = [object valueForKey:@"value"];
		}
		else {
			idx = [self.nutritionEfforts indexOfObject:object];
			if (idx != NSNotFound) {
				self.selectedNutritionEffort = idx;
				self.userProfileMO.nutritionEffort = [object valueForKey:@"value"];
			}
		}

		self.userProfileMO.status = kStatusPut;
		[self.managedObjectContext BL_save];

		[self updateView];
	}
}

@end
