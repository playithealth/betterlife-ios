//
//  RestaurantMenuCategoriesViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/27/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "RestaurantMenuCategoriesViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "FoodLogMO.h"
#import "RestaurantMenuItem.h"

static const CGFloat kSectionHeaderHeight = 32.0;

@interface RestaurantMenuCategoriesViewController () 
<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation RestaurantMenuCategoriesViewController
@synthesize restaurantChain = _restaurantChain;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithRestaurantChain:(RestaurantChain *)restaurantChain
{
    self = [super init];
    if (self) {
        self.restaurantChain = restaurantChain;
		self.managedObjectContext = restaurantChain.managedObjectContext;
    }
    return self;
}

- (void)loadView
{
	UITableView *aTableView =
		[[UITableView alloc]
			initWithFrame:CGRectZero
			style:UITableViewStylePlain];
	self.view = aTableView;
	self.tableView = aTableView;
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellId = @"RestaurantMenuCategoriesCellId";

	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
			reuseIdentifier:cellId];
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)theTableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	RestaurantMenuItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];

	// add the item to the food log
	FoodLogMO *newLog = [FoodLogMO addFoodLogWithMenuItem:item onDate:[NSDate date] logTime:1 inMOC:self.managedObjectContext];

	// send a notification so that the food log can reload
	[[NSNotificationCenter defaultCenter] postNotificationName:[FoodLogMO entityName] object:newLog userInfo:nil];
}

- (CGFloat)tableView:(UITableView *)tableView 
heightForHeaderInSection:(NSInteger)section
{
	CGFloat headerHeight = kSectionHeaderHeight;
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	if (![sectionInfo numberOfObjects] || [sectionInfo.name length] == 0) {
		headerHeight = 0.0;
	}
	return headerHeight;
}

#if 0
- (NSString *)tableView:(UITableView *)theTableView 
titleForHeaderInSection:(NSInteger)section
{
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	return sectionInfo.name;
}
#endif

- (UIView *)tableView:(UITableView *)theTableView 
viewForHeaderInSection:(NSInteger)section
{
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	if (![sectionInfo numberOfObjects] || [sectionInfo.name length] == 0) {
		return nil;
	}

	return [BCTableViewController customViewForHeaderWithTitle:sectionInfo.name];
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [RestaurantMenuItem entityInManagedObjectContext:self.managedObjectContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"ANY restaurantChain == %@", self.restaurantChain];
    
    NSSortDescriptor *sortByCategory = [NSSortDescriptor sortDescriptorWithKey:RestaurantMenuItemAttributes.category ascending:YES];
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:RestaurantMenuItemAttributes.name ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByName, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] 
		initWithFetchRequest:fetchRequest 
		managedObjectContext:self.managedObjectContext 
		sectionNameKeyPath:@"category"
		cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	     // Replace this implementation with code to handle the error appropriately.
	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}    

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. 
// If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}
 */

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    RestaurantMenuItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = item.name;
	if (![item.servingSize isEqualToString:@"whole serving"]) {
		cell.detailTextLabel.text = item.servingSize;
	}
}

@end
