//
//  PantryItemViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 5/20/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "PantryItemViewController.h"

#import "GenericValueDisplay.h"
#import "NumberValue.h"
#import "UIBarButtonItemAdditions.h"

static const NSInteger kRowName = 0;
static const NSInteger kRowQuantity = 1;

@interface PantryItemViewController ()
@property (assign, nonatomic) BOOL changedItem;
@property (strong, nonatomic) IBOutlet UIToolbar *keyboardToolbar;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *quantityTextField;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;
@end

@implementation PantryItemViewController

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.changedItem = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	DDLogInfo(@"%@", self.pantryItem);

	self.nameTextField.text = self.pantryItem.name;
	self.quantityTextField.text = [self.pantryItem.quantity stringValue];
	self.categoryLabel.text = self.pantryItem.category.name;

	if (self.keyboardToolbar == nil) {
		self.keyboardToolbar = [[UIToolbar alloc] initWithFrame:
			CGRectMake(0, 0, self.tableView.bounds.size.width, 44)];

		UIBarButtonItem *incrementButton = [[UIBarButtonItem alloc]
			initWithImageName:@"green-plus.png"
			target:self action:@selector(incrementButtonTapped:)];
		//incrementButton.imageInsets = UIEdgeInsetsMake(0, 2, 0, 3);

		UIBarButtonItem *decrementButton = [[UIBarButtonItem alloc]
			initWithImageName:@"green-minus.png"
			target:self action:@selector(decrementButtonTapped:)];
		//decrementButton.imageInsets = UIEdgeInsetsMake(0, 2, 0, 3);

		UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
			initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
			target:nil action:nil];

		UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
			initWithTitle:@"done" style:UIBarButtonItemStyleDone
			target:self action:@selector(resignKeyboard:)];

		NSArray *buttons = [NSArray arrayWithObjects:
			decrementButton, incrementButton, flexibleSpace, doneButton, nil];

		self.keyboardToolbar.items = buttons;
	}	
	self.quantityTextField.inputAccessoryView = self.keyboardToolbar;
}

- (void)viewWillDisappear:(BOOL)animated
{
	// tell the shopping list view that it needs to save
	if (self.changedItem && self.delegate
			&& [self.delegate conformsToProtocol:@protocol(PantryItemViewDelegate)]) {
		[self.delegate pantryItemView:self didUpdateItem:self.pantryItem];
	}
	self.changedItem = NO;

    [super viewWillDisappear:animated];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"SetCategory"]) {
		BCCategorySelectViewController *viewController = segue.destinationViewController;
		viewController.managedObjectContext = self.managedObjectContext;
		viewController.delegate = self;
	}
}

#pragma mark - local methods
- (void)incrementButtonTapped:(id)sender
{
	self.pantryItem.quantity = [NSNumber numberWithInteger:[self.pantryItem.quantity integerValue] + 1];
	
	self.quantityTextField.text = [self.pantryItem.quantity stringValue];
	
	self.changedItem = YES;
}

- (void)decrementButtonTapped:(id)sender
{
	// if this has a remainder, just chop that off
	if ([self.pantryItem.quantity doubleValue] - [self.pantryItem.quantity integerValue] > 0) {
		self.pantryItem.quantity = [NSNumber numberWithInteger:[self.pantryItem.quantity integerValue]];
	}
	// otherwise step down one
	else if ([self.pantryItem.quantity compare:[NSNumber numberWithInteger:0]] == NSOrderedDescending) {
		self.pantryItem.quantity = [NSNumber numberWithInteger:[self.pantryItem.quantity integerValue] - 1];
	}

	self.quantityTextField.text = [self.pantryItem.quantity stringValue];

	self.changedItem = YES;
}

- (void)resignKeyboard:(id)sender
{
	[self.nameTextField becomeFirstResponder];
	[self.nameTextField resignFirstResponder];
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == kRowName) {
		[self.nameTextField becomeFirstResponder];
	}
	else if (indexPath.row == kRowQuantity) {
		[self.quantityTextField becomeFirstResponder];
	}
}

#pragma mark - ProductSearchView delegate
- (void)productSearchView:(ProductSearchView *)productSearchView didSelectProduct:(NSDictionary *)product
{
	DDLogInfo(@"%@", product);
}

#pragma mark - CategorySelectViewController delegate
- (void)categorySelectView:(BCCategorySelectViewController *)categoryView
didSelectCategory:(CategoryMO *)category
{
	self.pantryItem.category = category;
	//self.pantryItem.categorySortOrder = [[Store currentStore] storeCategorySortOrder:category];
	self.changedItem = YES;

	self.categoryLabel.text = category.name;

	[self.navigationController popViewControllerAnimated:YES];
}

@end
