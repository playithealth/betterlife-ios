//
//  TaskQueueItems.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 8/3/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "TaskQueueItems.h"


@interface TaskQueueItems ()
- (void)unregisterTask:(NSString *)taskIdentifier;
@end

@implementation TaskQueueItems
{
@private
	NSMutableDictionary *registeredTasks;
	NSMutableDictionary *registeredQueues;
}

#pragma mark - life cycle
- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
		registeredTasks = [[NSMutableDictionary alloc] init];
		registeredQueues = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

#pragma mark - queue methods
- (BOOL)isQueueRegistered:(NSString *)queueIdentifier
{
	return ([registeredQueues objectForKey:queueIdentifier] ? YES : NO);
}

- (BOOL)registerQueue:(TaskQueue *)queue withIdentifier:(NSString *)queueIdentifier
{
	if (![self isQueueRegistered:queueIdentifier]) {
		[registeredQueues setObject:queue forKey:queueIdentifier];
		DDLogVerbose(@"Added %@ to the queues", queueIdentifier);
		return YES;
	}
	else {
		return NO;
	}
}

- (void)unregisterQueue:(NSString *)queueIdentifier
{
	DDLogVerbose(@"Removing %@ from the queues", queueIdentifier);
	[registeredQueues removeObjectForKey:queueIdentifier];
}

- (void)cancelAllQueues
{
	// Iterate on all queues and cancel each one's tasks
	for (NSString *queueId in registeredQueues) {
		[[registeredQueues objectForKey:queueId] cancelTasks];
	}

	// Then remove all queues
	[registeredQueues removeAllObjects];
}

- (void)cancelFetchers
{
	// Iterate on all queues and cancel each one's tasks
	// Don't iterate on registeredTasks itself, as we're removing things from it as we go, and cannot mutate it while iterating
	NSArray *tasks = [registeredTasks allKeys];
	for (NSString *taskId in tasks) {
		id task = [registeredTasks objectForKey:taskId];

		if ([task isKindOfClass:[GTMHTTPFetcher class]]) {
			// TODO Do we need a way to call this fetcher's completion handler?
			[task stopFetching];
			[self unregisterTask:taskId];
		}
	}
}

#pragma mark - task methods
- (BOOL)isTaskRegistered:(NSString *)taskIdentifier
{
	return ([registeredTasks objectForKey:taskIdentifier] ? YES : NO);
}

- (BOOL)registerTaskWithIdentifier:(NSString *)taskIdentifier
{
	if (![self isTaskRegistered:taskIdentifier]) {
		[registeredTasks setObject:[NSNumber numberWithInt:1] forKey:taskIdentifier];
		DDLogVerbose(@"Added %@ to the tasks", taskIdentifier);
		return YES;
	}
	else {
		return NO;
	}
}

- (BOOL)registerTaskWithFetcher:(GTMHTTPFetcher *)taskFetcher identifier:(NSString *)taskIdentifier
{
	if (![self isTaskRegistered:taskIdentifier]) {
		[registeredTasks setObject:taskFetcher forKey:taskIdentifier];
		DDLogVerbose(@"Added %@ to the tasks", taskIdentifier);
		return YES;
	}
	else {
		return NO;
	}
}

- (void)unregisterTask:(NSString *)taskIdentifier
{
	[registeredTasks removeObjectForKey:taskIdentifier];
	DDLogVerbose(@"Removed %@ from the tasks", taskIdentifier);
}

#pragma mark - class methods
+ (TaskQueueItems *)currentTasks
{
	static TaskQueueItems *currentTasks = nil;
	
	@synchronized(self) {
		if (!currentTasks) {
			currentTasks = [[TaskQueueItems alloc] init];
		}
		return currentTasks;
	}
}

+ (BOOL)registerQueue:(TaskQueue *)queue withIdentifier:(NSString *)queueIdentifier
{
	return [[TaskQueueItems currentTasks] registerQueue:queue withIdentifier:queueIdentifier];
}

+ (BOOL)isQueueRegistered:(NSString *)queueIdentifier
{
	return [[TaskQueueItems currentTasks] isQueueRegistered:queueIdentifier];
}

+ (void)unregisterQueue:(NSString *)queueIdentifier
{
	[[TaskQueueItems currentTasks] unregisterQueue:queueIdentifier];
}

+ (BOOL)registerTaskWithIdentifier:(NSString *)taskIdentifier
{
	return [[TaskQueueItems currentTasks] registerTaskWithIdentifier:taskIdentifier];
}

+ (BOOL)registerTaskWithFetcher:(GTMHTTPFetcher *)taskFetcher identifier:(NSString *)taskIdentifier
{
	return [[TaskQueueItems currentTasks] registerTaskWithFetcher:taskFetcher identifier:taskIdentifier];
}

+ (BOOL)isTaskRegistered:(NSString *)taskIdentifier
{
	return [[TaskQueueItems currentTasks] isTaskRegistered:taskIdentifier];
}

+ (void)unregisterTask:(NSString *)taskIdentifier
{
	[[TaskQueueItems currentTasks] unregisterTask:taskIdentifier];
}

+ (void)cancelAllQueues
{
	TaskQueueItems *tracker = [TaskQueueItems currentTasks];
	[tracker cancelAllQueues];

	// Also cancel any tasks that have fetchers attached to them, these are special fetchers
	// that the TaskQueue and/or HTTPQueue don't know about
	[tracker cancelFetchers];
}

@end
