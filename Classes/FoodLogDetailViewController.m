//
//  FoodLogDetailViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/31/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "FoodLogDetailViewController.h"

#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BLAlertController.h"
#import "CustomFoodMO.h"
#import "GenericValueDisplay.h"
#import "GTMHTTPFetcherAdditions.h"
#import "MBProgressHUD.h"
#import "NumberValue.h"
#import "NutrientMO.h"
#import "NutritionMO.h"
#import "RecipeMO.h"
#import "MealIngredient.h"
#import "StarRatingControl.h"
#import "NSDictionary+NumberValue.h"
#import "UIColor+Additions.h"
#import "NSString+BCAdditions.h"

#pragma mark - Custom Cells
@interface ItemDetailImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@end
@implementation ItemDetailImageCell
@end

@interface ItemDetailNameCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@end
@implementation ItemDetailNameCell
@end

@interface ItemDetailNameEditCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *name;
@end
@implementation ItemDetailNameEditCell
@end

@interface ItemDetailRatingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet StarRatingControl *ratingControl;
@end
@implementation ItemDetailRatingCell
@end

@interface ItemDetailPickerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@end
@implementation ItemDetailPickerCell
@end

@interface ItemDetailNameValueEditCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextField *value;
@end
@implementation ItemDetailNameValueEditCell
@end

@interface ItemDetailSegmentedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@end
@implementation ItemDetailSegmentedCell
@end

@interface PrimaryNutrientCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *value;
@end
@implementation PrimaryNutrientCell
@end

@interface SecondaryNutrientCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *value;
@end
@implementation SecondaryNutrientCell
@end

@interface EditNutrientCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextField *value;
@property (weak, nonatomic) IBOutlet UILabel *units;
@end
@implementation EditNutrientCell
@end

@interface ItemDetailServingSizeFooterView : UITableViewHeaderFooterView
@property (strong, nonatomic) UILabel *servingSizeLabel;
@end
@implementation ItemDetailServingSizeFooterView
@end

@interface FoodLogDetailViewController () <UINavigationBarDelegate, UIPickerViewDataSource, UIPickerViewDelegate, StarRatingControlDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) UITextField *activeField;

@property (strong, nonatomic) NSMutableArray *viewControllers;
@property (assign, nonatomic) NSUInteger activeIndex;

@property (strong, nonatomic) UITextField *hiddenTextField;

@property (strong, nonatomic) NSArray *mealNames;
@property (strong, nonatomic) NSArray *fractions;
@property (strong, nonatomic) NSArray *units;
@property (strong, nonatomic) NSMutableArray *servingUnitNames;
@property (strong, nonatomic) NSMutableArray *servingUnitFactors;
@property (assign, nonatomic) BOOL hasIngredientWarnings;
@property (assign, nonatomic) BOOL hasLifestyles;
@property (assign, nonatomic) BOOL showIngredients;
@property (strong, nonatomic) NSString *lifestyleText;
@property (strong, nonatomic) NSString *warningsText;
@property (strong, nonatomic) NSAttributedString *ingredientsAttributedText;
@property (strong, nonatomic) NSString *servingsPerContainer;
@property (strong, nonatomic) NSNumber *servingSize;
@property (strong, nonatomic) NSString *servingSizeUnit;
@property (assign, nonatomic) NSUInteger ingredientCellHeight;
@property (strong, nonatomic) NSArray *nutrients; ///< All the nutrients from the Nutrients entity for dynamic cell creation
@property (assign, nonatomic) NSUInteger userRating;
@property (assign, nonatomic) NSUInteger averageRating;
@property (assign, nonatomic) NSUInteger averageRatingCount;
@property (assign, nonatomic) BOOL nameFirstResponder; ///< This gives us a way to have the name cell be the first responder the first time in
@end

@implementation FoodLogDetailViewController

static const NSInteger kNumberOfSections = 5;

static const NSInteger kSectionItemInfo = 0;
static const NSInteger kSectionPlannerInfo = 1;
static const NSInteger kSectionLifestyleWarnings = 2;
static const NSInteger kSectionLifestyleRecommend = 3;
static const NSInteger kSectionNutritionInfo = 4;

// kSectionItemInfo
static const NSInteger kRowImage = 0;
static const NSInteger kRowName = 1;
static const NSInteger kRowRating = 2;

// kSectionPlannerInfo
static const NSInteger kRowDate = 0;
static const NSInteger kRowLogTime = 1;
static const NSInteger kRowServings = 2;

// kSectionNutritionInfo
static const NSInteger kRowInfoSegmented = 0;
static const NSInteger kRowServingSize = 1;
static const NSInteger kRowServingsPerContainer = 2;
static const NSInteger kRowIngredients = 3;
static const NSInteger kRowNutrientStartRow = 4; // 23 nutrient rows starting here

static const NSInteger kTagMealDatePicker = 0;
static const NSInteger kTagMealTimePicker = 1;
static const NSInteger kTagServingsPicker = 2;
static const NSInteger kTagServingSizePicker = 3;

//static const NSInteger kPickerComponentMealName = 0;
static const NSInteger kPickerComponentWhole = 0;
static const NSInteger kPickerComponentFraction = 1;
static const NSInteger kPickerComponentUnit = 2;

static NSString * const kHeaderViewId = @"IDVCH";
static NSString * const kFooterViewId = @"IDVCF";
//static const CGFloat kSectionHeaderHeight = 40.0;
static const CGFloat kStockSectionHeaderHeight = 22.0;

// The sizing on the attributed strings seems to not work properly if you don't first set a font/size
// for the entirety of the string first, so lets use an 18 pt font for now
static const NSUInteger ingredientsFontSize = 18;
// The standard remaining width for the ingredients cell seems to be 290, and the top/bottom margin
// is likely around 16 (8 each), so adjust for these
static const NSUInteger ingredientsCellUsableWidth = 290;
static const NSUInteger ingredientsCellVertialMargins = 16;

// Text field tag values
enum {
	nameFieldTag = 1,
	servingSizeFieldTag = 2,
	servingsPerContainerFieldTag = 3,
	nutrientStartFieldTag = 4,
	nutrientEndFieldTag = 27
};

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		self.detailViewMode = ItemDetailViewModeLogging;

		self.mealNames = @[@"Breakfast", @"Morning Snack", @"Lunch", @"Afternoon Snack", @"Dinner", @"Evening Snack"];
		self.fractions = @[@"0",@"⅛",@"¼",@"⅜",@"½",@"⅝",@"¾",@"⅞"];
		self.units = @[@"", @"bags", @"bottles", @"boxes", @"bulbs", @"bunches", @"cups", @"cans", @"cartons", @"cubic centimeters", @"cloves", @"containers", @"cubic", @"dashes", @"fillets", @"fluid ounces", @"grams", @"gallons", @"heads", @"jars", @"kilograms", @"liters", @"pounds", @"leafs", @"milligrams", @"milliliters", @"ounces", @"packages", @"packets", @"pieces", @"pinches", @"pouches", @"pints", @"quarts", @"slices", @"sprigs", @"stalks", @"tablespoons", @"teaspoons"]; // TODO Find a way to keep this in sync with cloud - Units::$display_names
		self.servingUnitNames = [NSMutableArray array];
		[self.servingUnitNames addObject:@"servings"];
		self.servingUnitFactors = [NSMutableArray array];
		[self.servingUnitFactors addObject:@1];
		self.hasIngredientWarnings = NO;
		self.hasLifestyles = NO;
		self.showIngredients = NO;
		self.ingredientCellHeight = 0;
		self.nameFirstResponder = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];


	self.hiddenTextField = [[UITextField alloc] initWithFrame:CGRectZero];
	self.hiddenTextField.hidden = YES;
	[self.view addSubview: self.hiddenTextField];

	[self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:kHeaderViewId];
	[self.tableView registerClass:[ItemDetailServingSizeFooterView class] forHeaderFooterViewReuseIdentifier:kFooterViewId];

	[self registerForKeyboardNotifications];

	switch (self.detailViewMode) {
		case ItemDetailViewModeLogging:
			self.navigationBar.topItem.title = @"Logging Item Info";
			break;
		case ItemDetailViewModePlanning:
			self.navigationBar.topItem.title = @"Planning Item Info";
			break;
		case ItemDetailViewModeCoachTagPlanning:
		case ItemDetailViewModeCoachTagLogging:
			self.navigationBar.topItem.title = @"Coach Item Info";
			break;
		case ItemDetailViewModeShopping:
			self.navigationBar.topItem.title = @"Shopping Item Info";
			break;
		case ItemDetailViewModeEditing:
			self.navigationBar.topItem.title = @"Custom Food";
			break;
	}

	// TODO
	if (self.createCustomFood) {
		self.item.itemType = kItemTypeProduct;
	}

	// This is important, as nutrient sync uses a delete strategy, so these can just go away if they re-sync
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadNutrients:) name:[NutrientMO entityName] object:nil];
	// Load up the nutrients
	self.nutrients = [NutrientMO MR_findAllSortedBy:NutrientMOAttributes.sortOrder ascending:YES
		inContext:self.managedObjectContext];
}

- (void)viewWillAppear:(BOOL)animated
{
	// Default the lifestyle warnings
	self.warningsText = @"No data available";

	if (self.detailViewMode == ItemDetailViewModeEditing) {
		if (self.item.customFood.servingsPerContainer) {
			self.servingsPerContainer = [self.item.customFood.servingsPerContainer stringValue];
		}
		if (self.item.customFood.servingSize) {
			self.servingSize = self.item.customFood.servingSize;
		}
		if (self.item.customFood.servingSizeUnit) {
			self.servingSizeUnit = self.item.customFood.servingSizeUnit;
		}
	}

	if (self.item) {
		if (self.item.itemId && self.item.itemType) {
			GTMHTTPFetcher *ratingsFetcher = [GTMHTTPFetcher signedFetcherWithURL:
				[BCUrlFactory evaluationsForItemId:self.item.itemId withTypeName:self.item.itemType]];

			// Fetch the rating
			[ratingsFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
				if (error) {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:ratingsFetcher retrievedData:retrievedData];
				}
				else {
					NSDictionary *ratingDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
					NSArray *userRatings = ratingDict[kEvalUser];
					__typeof__(self) __weak weakself = self;
					[userRatings enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
						NSDictionary *userDict = obj;
						if ([userDict[kEvalUserCategory] isEqualToString:kEvalUserCategoryGeneral]) {
							*stop = YES;
							weakself.userRating = [userDict[kEvalUserRating] integerValue];
						}
					}];

					NSDictionary *averageDict = ratingDict[kEvalAggregate];
					self.averageRating = [[averageDict BC_numberForKey:kEvalAggregateAverage] integerValue];
					self.averageRatingCount = [[averageDict BC_numberForKey:kEvalAggregateCount] integerValue];

					// Display the newly loaded data
					[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowRating inSection:kSectionItemInfo]]
						withRowAnimation:UITableViewRowAnimationNone];
				}
			}];
		}

		// Product
		if ([self.item.itemType isEqualToString:kUnifiedSearchItemTypeProduct]) {
			[self downloadServingsForProductId:self.item.itemId];

			// Lifestyle Allergy and Ingredient warnings
			GTMHTTPFetcher *ingredientWarningsFetcher = [GTMHTTPFetcher signedFetcherWithURL:
				[BCUrlFactory productsURLWarningsForProductId:self.item.itemId]];

			[ingredientWarningsFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
				if (error) {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:ingredientWarningsFetcher
						retrievedData:retrievedData];
				}
				else {
					NSDictionary *warningsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

					NSArray *lifestyles = [warningsDict objectForKey:kProductWarningsLifestyle];
					NSArray *allergies = [warningsDict objectForKey:kProductWarningsAllergies];
					NSArray *ingredientWarnings = [warningsDict objectForKey:kProductWarningsIngredients];

					if ([allergies count] || [ingredientWarnings count]) {
						self.hasIngredientWarnings = YES;
						self.warningsText = [[allergies arrayByAddingObjectsFromArray:ingredientWarnings]
							componentsJoinedByString:@","];
					}
					else {
						self.warningsText = @"";
					}

					if ([lifestyles count]) {
						self.hasLifestyles = YES;
						self.lifestyleText = [lifestyles componentsJoinedByString:@","];
					}
					[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionLifestyleRecommend]
						withRowAnimation:UITableViewRowAnimationNone];
					[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionLifestyleWarnings]
						withRowAnimation:UITableViewRowAnimationNone];
				}
			}];

			// Product allergens, warnings, and ingredients
			GTMHTTPFetcher *productInfoFetcher = [GTMHTTPFetcher signedFetcherWithURL:
				[BCUrlFactory productsURLInfoForProductId:self.item.itemId]];

			[productInfoFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
				if (error) {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:productInfoFetcher
						retrievedData:retrievedData];
				}
				else {
					NSDictionary *productDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

					NSString *ingredients = [productDict BC_stringOrNilForKey:kProductInfoIngredients];
					NSString *allergies = [productDict BC_stringOrNilForKey:kProductInfoAllergies];
					NSString *warnings = [productDict BC_stringOrNilForKey:kProductInfoWarnings];


					NSMutableString *composite = [NSMutableString string];
					NSString *allergiesString = nil;
					if ([allergies length]) {
						allergiesString = [NSString stringWithFormat:@"Allergies %@\n\n", allergies];
						[composite appendString:allergiesString];
					}
					if ([warnings length]) {
						 [composite appendFormat:@"Warnings %@\n\n", warnings];
					}
					if ([ingredients length]) {
						[composite appendString:ingredients];
					}

					NSMutableAttributedString *compositeAttributed = [[NSMutableAttributedString alloc]
						initWithString:composite];

					[compositeAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:ingredientsFontSize]
						range:NSMakeRange(0, [compositeAttributed length])];
					if (allergiesString) {
						[compositeAttributed addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:ingredientsFontSize]
							range:NSMakeRange(0, 9)];
					}
					if ([warnings length]) {
						[compositeAttributed addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:ingredientsFontSize]
							range:NSMakeRange([allergiesString length], 8)];
					}

					self.ingredientsAttributedText = compositeAttributed;

					CGRect rect = [compositeAttributed boundingRectWithSize:CGSizeMake(ingredientsCellUsableWidth, CGFLOAT_MAX)
						options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin context:nil];
					self.ingredientCellHeight = rect.size.height + ingredientsCellVertialMargins;
				}
			}];
		}
		// Recipe
		else if ([self.item.itemType isEqualToString:kUnifiedSearchItemTypeRecipe]) {
			// Determine whether we need to fetch ingredients and instructions from the cloud,
			// if the recipe isn't stored locally, then fetch
			// This is going to be done by checking updatedOn, as that only comes down on a sync, and not a search
			// This may or may not be a reliable way to detect this, but haven't found another criteria at this point
			// Maybe could also check to ensure that both instructions and ingredients are empty?
			if (self.item.recipe.updatedOnValue == 0) {
				// Fetch instructions and ingredients
				GTMHTTPFetcher *recipeDetailsFetcher = [GTMHTTPFetcher signedFetcherWithURL:
					[BCUrlFactory recipeDetailsURLForRecipeId:self.item.itemId]];

				[recipeDetailsFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
					if (error) {
						[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:recipeDetailsFetcher
							retrievedData:retrievedData];
					}
					else {
						NSDictionary *recipeDetailsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
						// Stuff the ingredients and instructions into the recipe MO
						self.item.recipe.instructions = [recipeDetailsDict objectForKey:kRecipeInstructions];

						for (NSDictionary *instructionDict in [recipeDetailsDict objectForKey:kRecipeIngredients]) {
							MealIngredient *ingredient = [MealIngredient insertInManagedObjectContext:self.managedObjectContext];
							[ingredient setWithRecipeDetailDictionary:instructionDict];
							[self.item.recipe addIngredientsObject:ingredient];
						}

						[self formatRecipeIngredients];
					}
				}];
			}
			else {
				[self formatRecipeIngredients];
			}
		}

		[self loadFoodLogImage];
	}

	[super viewWillAppear:animated];
}

- (void)loadFoodLogImage
{
	// Only attempt to load an image if there is an imageId Value, otherwise, this item has no image and should continue
	// using a stock image, which should be set in cellForRowAtIndexPath
	if ([self.item.imageId integerValue]) {
		UIImage *image = [[BCImageCache sharedCache] imageWithImageId:self.item.imageId size:kImageSizeLarge];
		if (!image) {
			[self downloadImageWithImageId:self.item.imageId];
		}
	}
}

- (void)downloadServingsForProductId:(NSNumber *)productId
{
	if ([productId integerValue] > 0) {
		MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
		hud.labelText = @"Loading serving info";

		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory productsURLServingsForProductId:productId]];
		[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {            
			if (error) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:nil];
			}
			else {
				NSArray *servingUnitNamesAndFactors = [NSJSONSerialization JSONObjectWithData:retrievedData
					options:NSJSONReadingMutableContainers error:nil];

				self.servingUnitNames = [NSMutableArray array];
				self.servingUnitFactors = [NSMutableArray array];

				for (NSDictionary *servingDict in servingUnitNamesAndFactors) {
					[self.servingUnitNames addObject:[servingDict objectForKey:@"serving_name"]];
					[self.servingUnitFactors addObject:[servingDict objectForKey:@"serving_factor"]];
				}
			}

			[MBProgressHUD hideHUDForView:self.view animated:YES];
			[appDelegate setNetworkActivityIndicatorVisible:NO];
		}];
	}
}

- (void)downloadImageWithImageId:(NSNumber *)imageId
{
	GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory imageURLForImage:imageId imageSize:kImageSizeLarge]];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	[imageFetcher beginFetchWithCompletionHandler: ^(NSData *retrievedData, NSError *error) {
		if (error != nil) {
			if (error.code == 404) {
				DDLogInfo(@"missing image - imageId %@", imageId);

				[[BCImageCache sharedCache] setImage:[NSNull null] forImageId:imageId size:kImageSizeLarge];
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:imageFetcher retrievedData:retrievedData];
			}
		}
		else {
			id itemImage = [[UIImage alloc] initWithData:retrievedData];
			if (!itemImage) {
				itemImage = [NSNull null];
			}

			[[BCImageCache sharedCache] setImage:itemImage forImageId:imageId size:kImageSizeLarge];

			// Display the newly loaded image
			[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowImage inSection:kSectionItemInfo]]
				withRowAnimation:UITableViewRowAnimationNone];
		}
		[appDelegate setNetworkActivityIndicatorVisible:NO];
	}];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - local

- (void)formatRecipeIngredients
{
	// Format the instructions and ingredients
	NSString *composite = nil;

	NSString *ingredientsString = nil;
	NSArray *sortedIngredients = [self.item.recipe.ingredients
		sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:MealIngredientAttributes.sortOrder ascending:YES]]];
	for (MealIngredient *ingredient in sortedIngredients) {
		if (!ingredientsString) {
			ingredientsString = [NSString stringWithFormat:@"Ingredients\n%@", ingredient.fullText];
		}
		else {
			ingredientsString = [ingredientsString stringByAppendingFormat:@"\n%@", ingredient.fullText];
		}
	}

	NSString *instructionsString = nil;
	if ([self.item.recipe.instructions length]) {
		instructionsString = [NSString stringWithFormat:@"Instructions\n%@", self.item.recipe.instructions];
	}

	if (ingredientsString) {
		if (instructionsString) {
			composite = [NSString stringWithFormat:@"%@\n\n%@", ingredientsString, instructionsString];
		}
		else {
			composite = ingredientsString;
		}
	}
	else {
		composite = instructionsString;
	}

	if (composite) {
		NSMutableAttributedString *compositeAttributed = [[NSMutableAttributedString alloc]
			initWithString:composite];

		[compositeAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:ingredientsFontSize]
			range:NSMakeRange(0, [compositeAttributed length])];

		if ([ingredientsString length]) {
			[compositeAttributed addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:ingredientsFontSize]
				range:NSMakeRange(0, 12)];
		}

		if ([instructionsString length]) {
			[compositeAttributed addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:ingredientsFontSize]
				range:NSMakeRange([ingredientsString length] + 1, 13)];
		}

		CGRect rect = [compositeAttributed boundingRectWithSize:CGSizeMake(ingredientsCellUsableWidth, CGFLOAT_MAX)
			options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin context:nil];
		self.ingredientCellHeight = rect.size.height + ingredientsCellVertialMargins;

		self.ingredientsAttributedText = compositeAttributed;
	}
}


// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:)
		name:UIKeyboardDidShowNotification object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
		name:UIKeyboardWillHideNotification object:nil];
 
}
 
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
 
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
 
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
        [self.tableView scrollRectToVisible:self.activeField.frame animated:YES];
    }
}
 
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}


- (UIToolbar *)makeKeyboardToolbar
{
	CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
	UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, appFrame.size.width, 35)];
	toolbar.barStyle = UIBarStyleBlack;
	toolbar.translucent = YES;

	UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
		target:nil action:nil];

	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
		initWithTitle:@"Done" style:UIBarButtonItemStyleDone
		target:self action:@selector(done:)];

	toolbar.items = @[flexibleSpace, doneButton];

	return toolbar;
}

- (IBAction)infoSegmentedControlValueChanged:(id)sender
{
	// Show either nutrition (0) or ingredients (1)
	self.showIngredients = ([sender selectedSegmentIndex] == 0 ? NO : YES);
    
	[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionNutritionInfo]
                  withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)saveNutrientFromTextField:(UITextField *)sender
{
	// Determine the nutrient that changed via the tag
	NutrientMO *nutrient = [self.nutrients objectAtIndex:sender.tag - nutrientStartFieldTag];

	if (!self.item.nutrition) {
		self.item.nutrition = [NutritionMO insertInManagedObjectContext:self.managedObjectContext];
	}

	[self.item.nutrition setValue:[sender.text BC_nutrientNumber] forKey:nutrient.name];
}

- (void)serializeTextField:(UITextField *)textField
{
	switch (textField.tag) {
		case nameFieldTag:
			self.item.name = textField.text;
			break;
		case servingSizeFieldTag:
			self.item.servingSize = textField.text;
			break;
		case servingsPerContainerFieldTag:
			self.servingsPerContainer = textField.text;
			break;
		default:
			if ((textField.tag >= nutrientStartFieldTag) && (textField.tag <= nutrientEndFieldTag)) {
				[self saveNutrientFromTextField:textField];
			}
			break;
	}
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
	self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	self.activeField = nil;
	[self serializeTextField:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return NO;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return kNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	switch (section) {
		case kSectionItemInfo:
			numberOfRows = 3;
			break;
		case kSectionPlannerInfo:
			numberOfRows = 3;
			break;
		case kSectionLifestyleWarnings:
			numberOfRows = 1;
			break;
		case kSectionLifestyleRecommend:
			numberOfRows = 1;
			break;
		case kSectionNutritionInfo:
			numberOfRows = kRowNutrientStartRow + [self.nutrients count];
			break;
		default:
			break;
	}
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *itemImageCellId = @"ItemImage";
	static NSString *itemNameCellId = @"ItemName";
	static NSString *itemNameEditCellId = @"ItemNameEdit";
	static NSString *ratingCellId = @"Rating";
	static NSString *plannerSelectorCellId = @"PlannerSelector";
	static NSString *nameValueEditCellId = @"NameValueEdit"; // Used for serving size and servings per container
	static NSString *segmentedCellId = @"Segmented";
	static NSString *primaryNutrientCellId = @"PrimaryNutrient";
	static NSString *secondaryNutrientCellId = @"SecondaryNutrient";
	static NSString *editNutrientCellId = @"EditNutrient";
	static NSString *basicCellId = @"Basic";
	static NSString *basicAttributedCellId = @"BasicAttributed";

	UITableViewCell *cell = nil;
	NSString *cellId = basicCellId;
	
	switch ([indexPath section]) {
		case kSectionItemInfo:
			switch ([indexPath row]) {
				case kRowImage: {
						cell = [self.tableView dequeueReusableCellWithIdentifier:itemImageCellId];
						ItemDetailImageCell *imageCell = (ItemDetailImageCell *)cell;
						imageCell.itemImageView.image = [[BCImageCache sharedCache] imageWithImageId:self.item.imageId
							withItemType:self.item.itemType withSize:kImageSizeLarge];
					}
					break;
				case kRowName: {
						if (self.detailViewMode == ItemDetailViewModeEditing) {
							cell = [self.tableView dequeueReusableCellWithIdentifier:itemNameEditCellId];
							ItemDetailNameEditCell *nameCell = (ItemDetailNameEditCell *)cell;
							nameCell.name.text = self.item.name;
							nameCell.name.tag = nameFieldTag;
							if (self.nameFirstResponder) {
								self.nameFirstResponder = NO;
								[nameCell.name becomeFirstResponder];
							}
						}
						else {
							cell = [self.tableView dequeueReusableCellWithIdentifier:itemNameCellId];
							ItemDetailNameCell *nameCell = (ItemDetailNameCell *)cell;
							nameCell.name.text = self.item.name;
						}
					}
					break;
				case kRowRating: {
						cell = [self.tableView dequeueReusableCellWithIdentifier:ratingCellId];
						ItemDetailRatingCell *ratingCell = (ItemDetailRatingCell *)cell;
						if (!ratingCell.ratingControl) {
							StarRatingControl *ratingControl = [[StarRatingControl alloc] initWithFrame:CGRectZero];
							ratingControl.delegate = self;
							ratingControl.translatesAutoresizingMaskIntoConstraints = NO;
							ratingControl.userInteractionEnabled = YES;
							ratingControl.emptyStar = [UIImage imageNamed:@"RatingStar-Empty.png"];
							ratingCell.ratingControl = ratingControl;
							[ratingCell.contentView addSubview:ratingControl];
							NSDictionary *dict = NSDictionaryOfVariableBindings(ratingControl);
							[ratingCell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[ratingControl(22)]"
								options:0 metrics:nil views:dict]];
							[ratingCell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[ratingControl(108)]"
								options:0 metrics:nil views:dict]];
							[ratingCell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:ratingCell.ratingControl
								attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:ratingCell.contentView
								attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
						}

						// Default to gold stars unless we're showing average rating
						ratingCell.ratingControl.fullStar = [UIImage imageNamed:@"RatingStar-Gold.png"];
						if (self.userRating) {
							ratingCell.ratingLabel.text = @"You rated this";
							ratingCell.ratingControl.rating = self.userRating;
						}
						else if (self.averageRating) {
							ratingCell.ratingControl.fullStar = [UIImage imageNamed:@"RatingStar-Green.png"];
							ratingCell.ratingControl.rating = self.averageRating;
							ratingCell.ratingLabel.text = [NSString stringWithFormat:@"Average - %lu ratings", (unsigned long)self.averageRatingCount];
						}
						else {
							ratingCell.ratingControl.rating = 0;
							ratingCell.ratingLabel.text = @"Tap to rate";
						}
					}
					break;
			}
			break;
		case kSectionPlannerInfo: {
				cell = [self.tableView dequeueReusableCellWithIdentifier:plannerSelectorCellId];
				NSString *nameText = nil;
				NSString *valueText = nil;
				switch ([indexPath row]) {
					case kRowDate: {
						nameText = @"Date";
						NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
						[dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"EdMMMYYYY" options:0
							locale:[NSLocale currentLocale]]];
						valueText = [dateFormatter stringFromDate:self.item.date];
						// Don't allow selecting this row when viewing a tag, since that would stray from the coach meal plan
						if ((self.detailViewMode == ItemDetailViewModeCoachTagPlanning)
								|| (self.detailViewMode == ItemDetailViewModeCoachTagLogging)) {
							cell.accessoryType = UITableViewCellAccessoryNone;
							cell.userInteractionEnabled = NO;
						}
						else {
							cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
							cell.userInteractionEnabled = YES;
						}
						break;
					}
					case kRowLogTime:
						nameText = @"Time";
						valueText = [self.mealNames objectAtIndex:[self.item.logTime integerValue]];
						// Don't allow selecting this row when viewing a tag, since that would stray from the coach meal plan
						if ((self.detailViewMode == ItemDetailViewModeCoachTagPlanning)
								|| (self.detailViewMode == ItemDetailViewModeCoachTagLogging)) {
							cell.accessoryType = UITableViewCellAccessoryNone;
							cell.userInteractionEnabled = NO;
						}
						else {
							cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
							cell.userInteractionEnabled = YES;
						}
						break;
					case kRowServings:
						nameText = @"Portion";
						valueText = [NSString stringWithFormat:@"%@ %@", self.item.servings, self.item.servingsUnit];
						// Don't allow selecting this row when planning or viewing a tag
						if ((self.detailViewMode == ItemDetailViewModePlanning)
								|| (self.detailViewMode == ItemDetailViewModeCoachTagPlanning)) {
							cell.accessoryType = UITableViewCellAccessoryNone;
							cell.userInteractionEnabled = NO;
						}
						else {
							cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
							cell.userInteractionEnabled = YES;
						}
						break;
				}
				ItemDetailPickerCell *pickerCell = (ItemDetailPickerCell *)cell;
				pickerCell.nameLabel.text = nameText;
				pickerCell.valueLabel.text = valueText;
			}
			break;
		case kSectionLifestyleWarnings:
			cell = [self.tableView dequeueReusableCellWithIdentifier:basicCellId];
			cell.textLabel.text = self.warningsText;
			break;
		case kSectionLifestyleRecommend:
			cell = [self.tableView dequeueReusableCellWithIdentifier:basicCellId];
			cell.textLabel.text = self.lifestyleText;
			break;
		case kSectionNutritionInfo:
			switch ([indexPath row]) {
				case kRowInfoSegmented:
					if (self.detailViewMode != ItemDetailViewModeEditing) {
						cell = [self.tableView dequeueReusableCellWithIdentifier:segmentedCellId];
						ItemDetailSegmentedCell *segmentedCell = (ItemDetailSegmentedCell *)cell;
						[segmentedCell.segmentedControl setSelectedSegmentIndex:(self.showIngredients ? 1 : 0)];
						if ([self.item.itemType isEqualToString:kUnifiedSearchItemTypeRecipe]) {
							[segmentedCell.segmentedControl setTitle:@"Ingredients & Instructions" forSegmentAtIndex:1];
							segmentedCell.segmentedControl.apportionsSegmentWidthsByContent = YES;
						}
						else {
							[segmentedCell.segmentedControl setTitle:@"Ingredients" forSegmentAtIndex:1];
							segmentedCell.segmentedControl.apportionsSegmentWidthsByContent = NO;
						}
					}
					break;
				case kRowServingSize:
					if (self.detailViewMode == ItemDetailViewModeEditing) {
						cell = [self.tableView dequeueReusableCellWithIdentifier:plannerSelectorCellId];
						ItemDetailPickerCell *pickerCell = (ItemDetailPickerCell *)cell;
						pickerCell.nameLabel.text = @"Serving size";
						pickerCell.valueLabel.text = @"";
						pickerCell.valueLabel.text = [NSString stringWithFormat:@"%@ %@",
							(self.servingSize ?: @""), (self.servingSizeUnit ?: @"")];
					}
					break;
				case kRowServingsPerContainer:
					if (self.detailViewMode == ItemDetailViewModeEditing) {
						cell = [self.tableView dequeueReusableCellWithIdentifier:nameValueEditCellId];
						ItemDetailNameValueEditCell *numberOfServingsCell = (ItemDetailNameValueEditCell *)cell;
						numberOfServingsCell.name.text = @"Servings per container";
						numberOfServingsCell.value.placeholder = @"4";
						numberOfServingsCell.value.text = self.servingsPerContainer;
						numberOfServingsCell.value.tag = servingsPerContainerFieldTag;
					}
					break;
				case kRowIngredients: {
						cell = [self.tableView dequeueReusableCellWithIdentifier:basicAttributedCellId];
						cell.textLabel.numberOfLines = 0;
						cell.textLabel.attributedText = self.ingredientsAttributedText;
					}
					break;
				default: { ///< Nutrients
						// Access the nutrient metadata that will tell us how to display this nutrient row properly
						NSInteger nutrientIndex = [indexPath row] - kRowNutrientStartRow;
						NutrientMO *nutrient = [self.nutrients objectAtIndex:nutrientIndex];

						NSNumber *value = [self.item.nutrition valueForKey:nutrient.name];

						if (self.detailViewMode == ItemDetailViewModeEditing) {
							cell = [self.tableView dequeueReusableCellWithIdentifier:editNutrientCellId];
							EditNutrientCell *nutrientCell = (EditNutrientCell *)cell;
							nutrientCell.name.text = nutrient.displayName;
							nutrientCell.value.text = [value stringValue];
							nutrientCell.units.text = nutrient.units;
							nutrientCell.value.tag = nutrientIndex + nutrientStartFieldTag;
						}
						else {
							double nutritionFactor = 1;
							if ([self.item respondsToSelector:@selector(nutritionFactor)]) {
								nutritionFactor = [self.item.nutritionFactor doubleValue];
							}
							double dblValue = [value doubleValue] * nutritionFactor * [self.item.servings doubleValue];
							NSString *valueText = [NSString stringWithFormat:@"%@%@",
								[NSString BC_stringWithNutrientNumber:[NSNumber numberWithDouble:dblValue]
								withPrecision:[nutrient.precision integerValue]], nutrient.units];
							// Determine primary/secondary
							if ([nutrient.isSubNutrient boolValue]) {
								cell = [self.tableView dequeueReusableCellWithIdentifier:secondaryNutrientCellId];
								SecondaryNutrientCell *nutrientCell = (SecondaryNutrientCell *)cell;
								nutrientCell.name.text = nutrient.displayName;
								if (value) {
									nutrientCell.value.text = valueText;
									nutrientCell.name.textColor = [UIColor blackColor];
								}
								else {
									nutrientCell.value.text = @"";
									nutrientCell.name.textColor = [UIColor lightGrayColor];
								}
							}
							else {
								cell = [self.tableView dequeueReusableCellWithIdentifier:primaryNutrientCellId];
								PrimaryNutrientCell *nutrientCell = (PrimaryNutrientCell *)cell;
								nutrientCell.name.text = nutrient.displayName;
								if (value) {
									nutrientCell.value.text = valueText;
									nutrientCell.name.textColor = [UIColor blackColor];
								}
								else {
									nutrientCell.value.text = @"";
									nutrientCell.name.textColor = [UIColor lightGrayColor];
								}
							}
						}
					}
					break;
			}
			break;
	}

	// If we didn't manually set the cell in the switch above, then load it with a cellId
	if (!cell) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:cellId];
	}

	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.hiddenTextField resignFirstResponder];

	if (indexPath.section == kSectionItemInfo) {
		if (indexPath.row == kRowName) {
		}
	}
	else if (indexPath.section == kSectionPlannerInfo) {
		if ((indexPath.row == kRowDate) && ((self.detailViewMode != ItemDetailViewModeCoachTagPlanning)
				|| (self.detailViewMode != ItemDetailViewModeCoachTagLogging))) {
			if (self.item.tag) {
				// TODO Eric wants a warning here that this will disassociate this item from their coach meal plan, with a confirmation UI!
			}
			UIDatePicker *aDatePicker = [[UIDatePicker alloc] init];
			aDatePicker.tag = kTagMealDatePicker;
			aDatePicker.datePickerMode = UIDatePickerModeDate;
			[aDatePicker setDate:self.item.date animated:NO];
			[aDatePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];

			self.hiddenTextField.inputView = aDatePicker;
			self.hiddenTextField.inputAccessoryView = [self makeKeyboardToolbar];

			[self.hiddenTextField becomeFirstResponder];
		}
		else if ((indexPath.row == kRowLogTime) && ((self.detailViewMode != ItemDetailViewModeCoachTagPlanning)
				|| (self.detailViewMode != ItemDetailViewModeCoachTagLogging))) {
			UIPickerView *aPickerView = [[UIPickerView alloc] init];
			aPickerView.showsSelectionIndicator = YES;
			aPickerView.dataSource = self;
			aPickerView.delegate = self;
			aPickerView.tag = kTagMealTimePicker;

			[aPickerView reloadAllComponents];

			[aPickerView selectRow:[self.item.logTime integerValue] inComponent:kPickerComponentWhole animated:NO];
			self.hiddenTextField.inputView = aPickerView;
			self.hiddenTextField.inputAccessoryView = [self makeKeyboardToolbar];

			[self.hiddenTextField becomeFirstResponder];
		}
		else if ((indexPath.row == kRowServings) && ((self.detailViewMode != ItemDetailViewModePlanning)
				|| (self.detailViewMode != ItemDetailViewModeCoachTagPlanning))) {
			UIPickerView *aPickerView = [[UIPickerView alloc] init];
			aPickerView.showsSelectionIndicator = YES;
			aPickerView.dataSource = self;
			aPickerView.delegate = self;
			aPickerView.tag = kTagServingsPicker;

			[aPickerView reloadAllComponents];

			NSInteger whole = [self.item.servings integerValue];
			double remainder = [self.item.servings doubleValue] - whole;

			if (whole >= 0 && whole < 100) {
				[aPickerView selectRow:whole inComponent:kPickerComponentWhole animated:NO];
			}
			if (remainder > 0) {
				NSInteger fraction = 8 * remainder;
				[aPickerView selectRow:fraction inComponent:kPickerComponentFraction animated:NO];
			}

			NSInteger idx = [self.servingUnitNames indexOfObject:self.item.servingsUnit];
			if (idx != NSNotFound) {
				[aPickerView selectRow:idx inComponent:kPickerComponentUnit animated:NO];
			}
			self.hiddenTextField.inputView = aPickerView;
			self.hiddenTextField.inputAccessoryView = [self makeKeyboardToolbar];

			[self.hiddenTextField becomeFirstResponder];
		}
	}
	else if (indexPath.section == kSectionNutritionInfo) {
		if (indexPath.row == kRowServingSize) {
			UIPickerView *aPickerView = [[UIPickerView alloc] init];
			aPickerView.showsSelectionIndicator = YES;
			aPickerView.dataSource = self;
			aPickerView.delegate = self;
			aPickerView.tag = kTagServingSizePicker;

			[aPickerView reloadAllComponents];

			NSInteger whole = [self.servingSize integerValue];
			double remainder = [self.servingSize doubleValue] - whole;

			if (whole >= 0 && whole < 100) {
				[aPickerView selectRow:whole inComponent:kPickerComponentWhole animated:NO];
			}
			if (remainder > 0) {
				NSInteger fraction = 8 * remainder;
				[aPickerView selectRow:fraction inComponent:kPickerComponentFraction animated:NO];
			}

			NSInteger idx = [self.units indexOfObject:self.servingSizeUnit];
			if (idx != NSNotFound) {
				[aPickerView selectRow:idx inComponent:kPickerComponentUnit animated:NO];
			}
			self.hiddenTextField.inputView = aPickerView;
			self.hiddenTextField.inputAccessoryView = [self makeKeyboardToolbar];

			[self.hiddenTextField becomeFirstResponder];
		}
		// These only happen if we're editing
		else if (self.detailViewMode == ItemDetailViewModeEditing) {
			UITextField *textFieldToEdit = nil;
			if (indexPath.row >= kRowNutrientStartRow) {
				EditNutrientCell *nutrientCell = (EditNutrientCell *)[self.tableView cellForRowAtIndexPath:indexPath];
				textFieldToEdit = nutrientCell.value;
			}
			else if (indexPath.row == kRowServingsPerContainer) {
				ItemDetailNameValueEditCell *numberOfServingsCell = (ItemDetailNameValueEditCell *)[self.tableView
					cellForRowAtIndexPath:indexPath];
				textFieldToEdit = numberOfServingsCell.value;
			}

			// Make this text field the first responder to start editing and show the keyboard
			[textFieldToEdit becomeFirstResponder];
		}
	}

	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)previous:(id)sender
{
	
}

- (IBAction)next:(id)sender
{
	
}

- (IBAction)done:(id)sender
{
	[self.hiddenTextField resignFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = UITableViewAutomaticDimension;

	if (indexPath.section == kSectionItemInfo) {
		switch (indexPath.row) {
			case kRowImage:
				rowHeight = 120;
				break;
			case kRowName:
				// TODO Calculate the height of the name, up to 2 rows tall?
				break;
			case kRowRating:
				// TODO Find a clever way to actually rate a newly created custom food.
				// Right now, allowing this control in this situation will try to push the rating to the cloud before
				// the product actually exists, causing an error.
				if (self.createCustomFood) {
					rowHeight = 0;
				}
				else {
					rowHeight = 55;
				}
				break;
		}
	}
	else if (indexPath.section == kSectionPlannerInfo) {
		// These are not shown when viewing details from a Health Coach Plan Tag from planning
		if (self.detailViewMode == ItemDetailViewModeCoachTagPlanning) {
			if ((indexPath.row == kRowDate) || (indexPath.row == kRowLogTime)) {
				rowHeight = 0;
			}
		}
		// Don't show the servings row in planning mode, as the user can't set this, and it should be zero (or 1 by a default)
		else if ((self.detailViewMode == ItemDetailViewModePlanning) && (indexPath.row == kRowServings)) {
			rowHeight = 0;
		}
	}
	else if ((indexPath.section == kSectionLifestyleRecommend) && !self.hasLifestyles) {
		rowHeight = 0;
	}
	else if ((indexPath.section == kSectionLifestyleWarnings) && !self.hasIngredientWarnings) {
		rowHeight = 0;
	}
	else if (indexPath.section == kSectionNutritionInfo) {
		switch (indexPath.row) {
			case kRowInfoSegmented:
				// This is not shown when editing or viewing a custom food
				if ((self.detailViewMode == ItemDetailViewModeEditing)
						|| ([self.item respondsToSelector:@selector(customFood)] && self.item.customFood)) {
					rowHeight = 0;
				}
				break;
			case kRowServingSize:
			case kRowServingsPerContainer:
				// These are only shown when editing a custom food
				if (self.detailViewMode != ItemDetailViewModeEditing) {
					rowHeight = 0;
				}
				break;
			case kRowIngredients:
				if (self.showIngredients) {
					// TODO Try to make # of rows = 0 and word wrap do this automatically, rather than manually sizing it
					rowHeight = self.ingredientCellHeight;
				}
				else {
					rowHeight = 0;
				}
				break;
			default: ///< Nutrients
				if (self.showIngredients) {
					rowHeight = 0;
				}
				else {
					if (self.detailViewMode != ItemDetailViewModeEditing) {
						rowHeight = 25;
					}
				}
				break;
		}
	}

	return rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	//CGFloat headerHeight = UITableViewAutomaticDimension;
	//CGFloat headerHeight = kSectionHeaderHeight;
	CGFloat headerHeight = kStockSectionHeaderHeight;
	if (section == kSectionItemInfo) {
		headerHeight = 0.0f;
	}
	else if ((section == kSectionLifestyleWarnings) && !self.hasIngredientWarnings) {
		headerHeight = 0.0f;
	}
	else if ((section == kSectionLifestyleRecommend) && !self.hasLifestyles) {
		headerHeight = 0.0f;
	}

	return headerHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	CGFloat footerHeight = 0.0;

	if (section == kSectionPlannerInfo) {
		footerHeight = kStockSectionHeaderHeight;
	}

	return footerHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kHeaderViewId];
	headerView.contentView.backgroundColor = [UIColor BC_tableViewHeaderColor];
	headerView.textLabel.font = [UIFont boldSystemFontOfSize:15];
	headerView.textLabel.textColor = [UIColor darkGrayColor];

	switch (section) {
		case kSectionItemInfo:
			break;
		case kSectionPlannerInfo:
			headerView.textLabel.text = @"MEAL";
			break;
		case kSectionLifestyleWarnings:
			headerView.textLabel.text = @"INGREDIENT & ALLERGY WARNINGS";
			if (self.hasIngredientWarnings) {
				headerView.textLabel.textColor = [UIColor BC_redColor];
			}
			break;
		case kSectionLifestyleRecommend:
			headerView.textLabel.text = @"LIFESTYLE RECOMMENDED";
			headerView.textLabel.textColor = [UIColor BC_mediumLightGreenColor];
			break;
		case kSectionNutritionInfo:
			headerView.textLabel.text = @"INFO";
			break;
	}

	/*
	if (headerView) {
		headerView.backgroundColor = [UIColor BC_tableViewHeaderColor];
		UILabel *label = [[UILabel alloc] init];
		label.text = labelText;
		label.textColor = labelColor;
		label.font = [UIFont boldSystemFontOfSize:15];
		label.translatesAutoresizingMaskIntoConstraints = NO;
		[headerView addSubview:label];
		NSDictionary *dict = NSDictionaryOfVariableBindings(label);
		[headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[label]"
			options:0 metrics:nil views:dict]];
		[headerView addConstraint:[NSLayoutConstraint constraintWithItem:label
			attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:headerView
			attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
	}
	*/

	return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	ItemDetailServingSizeFooterView *footerView = nil;

	if (section == kSectionPlannerInfo) {
		footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kFooterViewId];
		if (!footerView.servingSizeLabel) {
			footerView.servingSizeLabel = [[UILabel alloc] init];
			footerView.contentView.backgroundColor = [UIColor BC_tableViewHeaderColor];

			footerView.servingSizeLabel.font = [UIFont boldSystemFontOfSize:15];
			footerView.servingSizeLabel.textColor = [UIColor darkGrayColor];
			footerView.servingSizeLabel.textAlignment = NSTextAlignmentCenter;
			footerView.servingSizeLabel.translatesAutoresizingMaskIntoConstraints = NO; //?
			[footerView.contentView addSubview:footerView.servingSizeLabel];

			[footerView addConstraint:[NSLayoutConstraint constraintWithItem:footerView.servingSizeLabel
				attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:footerView.contentView
				attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
			[footerView addConstraint:[NSLayoutConstraint constraintWithItem:footerView.servingSizeLabel
				attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:footerView.contentView
				attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
		}

		if (self.item.servingSize) {
			footerView.servingSizeLabel.text = [NSString stringWithFormat:@"1 Serving = %@", self.item.servingSize];
		}
		else {
			footerView.servingSizeLabel.text = @"No serving information available";
		}
	}

	return footerView;
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	NSInteger numberOfComponents = 0;
	if (pickerView.tag == kTagMealTimePicker) {
		numberOfComponents = 1;
	}
	else if (pickerView.tag == kTagServingsPicker) {
		numberOfComponents = 3;
	}
	else if (pickerView.tag == kTagServingSizePicker) {
		numberOfComponents = 3;
	}
	return numberOfComponents;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	NSInteger numberOfRows = 0;
	if (pickerView.tag == kTagMealTimePicker) {
		numberOfRows = [self.mealNames count];
	}
	else if (pickerView.tag == kTagServingsPicker) {
		if (component == kPickerComponentWhole) {
			numberOfRows = 100;
		}
		else if (component == kPickerComponentFraction) {
			numberOfRows = 8;// 0,⅛,¼,⅜,½,⅝,¾,⅞
		}
		else if (component == kPickerComponentUnit) {
			numberOfRows = [self.servingUnitNames count];
		}
	}
	else if (pickerView.tag == kTagServingSizePicker) {
		if (component == kPickerComponentWhole) {
			numberOfRows = 100;
		}
		else if (component == kPickerComponentFraction) {
			numberOfRows = 8;// 0,⅛,¼,⅜,½,⅝,¾,⅞
		}
		else if (component == kPickerComponentUnit) {
			numberOfRows = [self.units count];
		}
	}
	return numberOfRows;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *title;

	if (pickerView.tag == kTagMealTimePicker) {
		title = [self.mealNames objectAtIndex:row];
	}
	else if (pickerView.tag == kTagServingsPicker) {
		if (component == kPickerComponentWhole) {
			title = [NSString stringWithFormat:@"%ld", (long)row];
		}
		else if (component == kPickerComponentFraction) {
			title = [self.fractions objectAtIndex:row];
		}
		else if (component == kPickerComponentUnit) {
			title = [self.servingUnitNames objectAtIndex:row];
		}
	}
	else if (pickerView.tag == kTagServingSizePicker) {
		if (component == kPickerComponentWhole) {
			title = [NSString stringWithFormat:@"%ld", (long)row];
		}
		else if (component == kPickerComponentFraction) {
			title = [self.fractions objectAtIndex:row];
		}
		else if (component == kPickerComponentUnit) {
			title = [self.units objectAtIndex:row];
		}
	}

	return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	if (pickerView.tag == kTagMealTimePicker) {
		self.item.logTime = @(row);
		[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowLogTime inSection:kSectionPlannerInfo]]
			withRowAnimation:UITableViewRowAnimationNone];
	}
	else if (pickerView.tag == kTagServingsPicker) {
		if ((component == kPickerComponentWhole) || (component == kPickerComponentFraction)) {
			NSInteger whole = [pickerView selectedRowInComponent:kPickerComponentWhole];
			NSInteger fraction = [pickerView selectedRowInComponent:kPickerComponentFraction];
			self.item.servings = [NSNumber numberWithDouble:whole + ((double)fraction / 8)];
		}
		else if (component == kPickerComponentUnit) {
			self.item.servingsUnit = [self.servingUnitNames objectAtIndex:[pickerView selectedRowInComponent:kPickerComponentUnit]];
			self.item.nutritionFactor = [self.servingUnitFactors objectAtIndex:[pickerView selectedRowInComponent:kPickerComponentUnit]];
		}

		[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowServings inSection:kSectionPlannerInfo]]
			withRowAnimation:UITableViewRowAnimationNone];

		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionNutritionInfo] withRowAnimation:UITableViewRowAnimationNone];
	}
	else if (pickerView.tag == kTagServingSizePicker) {
		if ((component == kPickerComponentWhole) || (component == kPickerComponentFraction)) {
			NSInteger whole = [pickerView selectedRowInComponent:kPickerComponentWhole];
			NSInteger fraction = [pickerView selectedRowInComponent:kPickerComponentFraction];
			self.servingSize = [NSNumber numberWithDouble:whole + ((double)fraction / 8)];
		}
		else if (component == kPickerComponentUnit) {
			self.servingSizeUnit = [self.units objectAtIndex:[pickerView selectedRowInComponent:kPickerComponentUnit]];
		}
		self.item.servingSize = [NSString stringWithFormat:@"%@ %@", (self.servingSize ?: @""), (self.servingSizeUnit ?: @"")];

		[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowServingSize inSection:kSectionNutritionInfo]]
			withRowAnimation:UITableViewRowAnimationNone];
		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionNutritionInfo] withRowAnimation:UITableViewRowAnimationNone];
	}
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	CGFloat width = 0.0;

	CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
	if (pickerView.tag == kTagMealTimePicker) {
		width = appFrame.size.width - 30;
	}
	else if ((pickerView.tag == kTagServingsPicker) || (pickerView.tag == kTagServingSizePicker)) {
		if (component == kPickerComponentWhole) {
			width = 60.0;
		}
		else if (component == kPickerComponentFraction) {
			width = 60.0;
		}
		else if (component == kPickerComponentUnit) {
			width = appFrame.size.width - 150;
		}
	}
	return width;
}

- (void)dateChanged:(UIDatePicker *)sender 
{
	self.item.date = sender.date;

	// Make sure that we don't keep any tags for items that the date has changed on
	self.item.tag = nil;

	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowDate inSection:kSectionPlannerInfo]]
		withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - StarRatingControlDelegate
- (void)starRatingControl:(StarRatingControl *)control didUpdateRating:(NSUInteger)rating fromRating:(NSUInteger)fromRating
{
	// Don't need to do anything if the rating didn't change, be sure to check if they clicked the user rating control
	// by seeing if the full star image is the gold one, if they clicked on the green ones, then they don't yet
	// have a rating, and we need to continue on to save their new rating
	if ((control.fullStar == [UIImage imageNamed:@"RatingStar-Gold.png"]) && (fromRating == rating)) {
		return;
	}

	// Update the table view
	self.userRating = rating;
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowRating inSection:kSectionItemInfo]]
		withRowAnimation:UITableViewRowAnimationNone];

	// Push the new rating up
	NSUInteger typeId = 0;
	if ([self.item.itemType isEqualToString:kUnifiedSearchItemTypeProduct]) {
		typeId = kEvalItemTypeProduct;
	}
	else if ([self.item.itemType isEqualToString:kUnifiedSearchItemTypeRecipe]) {
		typeId = kEvalItemTypeRecipe;
	}
	else if ([self.item.itemType isEqualToString:kUnifiedSearchItemTypeMenuItem]) {
		typeId = kEvalItemTypeMenuItem;
	}
	NSDictionary *newRatingDict = @{
		kEvalItemTypeId: @(typeId),
		kEvalItemId: self.item.itemId,
		kEvaluationCategory: kEvalUserCategoryGeneral,
		kRating: @(rating)
	};

	GTMHTTPFetcher *ratingsSender = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory evaluateURL] httpMethod:kPost 
		postData:[NSJSONSerialization dataWithJSONObject:@[newRatingDict] options:kNilOptions error:nil]];
	[ratingsSender beginFetchWithCompletionHandler:
		^(NSData * retrievedData, NSError * error) {
			if (error != nil) {
				// status code or network error
				DDLogError (@"failed %@", error);
			}
		}];
}

- (void)addChildViewController:(UIViewController *)childController atIndex:(NSUInteger)index
{
	childController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

	[self addChildViewController:childController];
	[childController didMoveToParentViewController:self];
	self.viewControllers[index] = childController;
}

- (void)switchToViewControllerAtIndex:(NSUInteger)index
{
	if (self.activeIndex != NSNotFound) {
		UIView *fromView = [self.viewControllers[self.activeIndex] view];
		[fromView removeFromSuperview];
	}
	self.activeIndex = index;

	UIView *toView = [self.viewControllers[self.activeIndex] view];
	toView.frame = self.contentView.bounds;
	[self.contentView insertSubview:toView atIndex:0];
}

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
	return UIBarPositionTopAttached;
}

- (IBAction)save:(id)sender
{
	// Check to see if we were in the midst of editing a text field, so that if the save event is fired prior to
	// the end editing event firing, we can still preserve the value they had entered
	if (self.activeField) {
		[self serializeTextField:self.activeField];
		self.activeField = nil;
	}

	// If we're editing, then duplicate our values out to the customfood
	if (self.detailViewMode == ItemDetailViewModeEditing) {
		// Validate the custom food
		// Must have a name
		if (![self.item.name length]) {
			// Alert the user and keep them here
			BLAlertController* alert = [BLAlertController alertControllerWithTitle:@"Name is required"
				message:@"Please enter a name for this food" preferredStyle:UIAlertControllerStyleAlert];

			[alert addAction:[BLAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
				handler:^(UIAlertAction * action) {
					// Put them on the name cell
					ItemDetailNameEditCell *nameCell = (ItemDetailNameEditCell *)[self.tableView cellForRowAtIndexPath:
						[NSIndexPath indexPathForRow:kRowName inSection:kSectionItemInfo]];
					[nameCell.name becomeFirstResponder];
				}]];
			[alert presentInViewController:self animated:YES completion:nil];
			// Bail out of here, as we failed validation
			return;
		}

		if (!self.item.customFood) {
			self.item.customFood = [CustomFoodMO insertInManagedObjectContext:self.managedObjectContext];
		}

		[self.item.customFood setWithFoodItem:self.item];

		// Now deal with the things that the food log does differently and/or need adjusting
		self.item.customFood.servingsPerContainer = [self.servingsPerContainer numberValueDecimal];
		self.item.customFood.servingSize = self.servingSize;
		self.item.customFood.servingSizeUnit = self.servingSizeUnit;

		[self.item.customFood markForSync];
	}

	[self.item markForSync];

	[self.delegate view:self didUpdateObject:self.item];
}

- (IBAction)cancel:(id)sender
{
	[self.delegate view:self didUpdateObject:nil];
}

- (void)reloadNutrients:(NSNotification *)notification
{
	self.nutrients = [NutrientMO MR_findAllSortedBy:NutrientMOAttributes.sortOrder ascending:YES
		inContext:self.managedObjectContext];

	[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionNutritionInfo] withRowAnimation:UITableViewRowAnimationNone];
}

@end
