//
//  MealIngredientEditView.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 10/18/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "MealIngredientEditView.h"

#import "GenericValueDisplay.h"
#import "RecipeMO.h"
#import "NSArray+NestedArrays.h"
#import "NumberValue.h"
#import "ProductDetailViewController.h"

@interface MealIngredientEditView ()
@property (strong, nonatomic) UITableViewCell *ingredientCell;
@property (strong, nonatomic) UITableViewCell *quantityCell;
@property (strong, nonatomic) UITableViewCell *unitsCell;
@property (strong, nonatomic) UITableViewCell *notesCell;
@property (strong, nonatomic) UITableViewCell *addToShoppingListCell;
@property (strong, nonatomic) NSArray *rowCells;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
- (void)addToShoppingListValueChanged:(id)sender;
- (UITableViewCell *)createCellWithLabelText:(NSString *)labelText
	textFieldKeyboardType:(UIKeyboardType)keyboardType;
- (void)saveTextFieldValue:(UITextField *)textField;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)keyboardWillShowNotification:(NSNotification*)aNotification;
- (void)keyboardWillHideNotification:(NSNotification*)aNotification;
@end

@implementation MealIngredientEditView

@synthesize delegate;
@synthesize tableView;
@synthesize ingredientCell;
@synthesize quantityCell;
@synthesize unitsCell;
@synthesize notesCell;
@synthesize addToShoppingListCell;
@synthesize shoppingListItemCell;
@synthesize ingredient;
@synthesize rowCells;
@synthesize selectedIndexPath;

const NSInteger kTagMIETextField = 10001;
const NSInteger kTagMIEAddToSLAsk = 10002;

const NSInteger kSectionIngredientDetails = 0;
const NSInteger kSectionShoppingList = 1;

const NSInteger kRowIngredientName = 0;
const NSInteger kRowQuantity = 1;
const NSInteger kRowUnits = 2;
const NSInteger kRowNotes = 3;

const NSInteger kRowAddToShoppingList = 0;
const NSInteger kRowShoppingListItem = 1;

const NSInteger kSegmentNo = 0;
const NSInteger kSegmentYes = 1;
const NSInteger kSegmentAsk = 2;

#pragma mark - memory management
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		self.ingredientCell = [self createCellWithLabelText:@"Ingredient:"
			textFieldKeyboardType:UIKeyboardTypeDefault];
		self.quantityCell = [self createCellWithLabelText:@"Quantity:"
			textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
		self.unitsCell = [self createCellWithLabelText:@"Units:"
			textFieldKeyboardType:UIKeyboardTypeDefault];
		self.notesCell = [self createCellWithLabelText:@"Notes:"
			textFieldKeyboardType:UIKeyboardTypeDefault];

		// Fill out the addToShoppingListCell
		self.addToShoppingListCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
			reuseIdentifier:nil];
		self.addToShoppingListCell.selectionStyle = UITableViewCellSelectionStyleNone;

		UILabel *aLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 280, 25)];
		aLabel.adjustsFontSizeToFitWidth = YES;
		aLabel.font = [UIFont boldSystemFontOfSize:17];
		aLabel.text = @"Add to shopping list?";
		[self.addToShoppingListCell.contentView addSubview:aLabel];

		UISegmentedControl *addToSL = [[UISegmentedControl alloc]
			initWithItems:[NSArray arrayWithObjects:
			@"Never", @"Always", @"Ask", nil]];
		addToSL.tag = kTagMIEAddToSLAsk;
		[addToSL addTarget:self action:@selector(addToShoppingListValueChanged:)
			forControlEvents:UIControlEventValueChanged];
		addToSL.frame = CGRectMake(10, 40, 280, 30);
		[self.addToShoppingListCell.contentView addSubview:addToSL];

		// Fill out the shoppingListItemCell
		[[NSBundle mainBundle] loadNibNamed:@"IngredientShoppingListItemCell" owner:self options:nil];
		self.shoppingListItemCell.quantityLabel.hidden = NO;
		self.shoppingListItemCell.clearButton.hidden = NO;

		UITapGestureRecognizer *recognizer = nil;

		recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(accessoryTapped:)];
		[self.shoppingListItemCell.accessoryButton addGestureRecognizer:recognizer];

		recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(incrementTapped:)];
		[self.shoppingListItemCell.incrementButton addGestureRecognizer:recognizer];

		recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(decrementTapped:)];
		[self.shoppingListItemCell.decrementButton addGestureRecognizer:recognizer];

		recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(resetShoppingListProductTapped:)];
		[self.shoppingListItemCell.clearButton addGestureRecognizer:recognizer];

		recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(associateShoppingListProductTapped:)];
		[self.shoppingListItemCell.associateButton addGestureRecognizer:recognizer];

		self.rowCells = [NSArray arrayWithObjects:
			[NSArray arrayWithObjects:self.ingredientCell, self.quantityCell, self.unitsCell,
				self.notesCell, nil],
			[NSArray arrayWithObjects:self.addToShoppingListCell, self.shoppingListItemCell, nil],
			nil];

		changedIngredient = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

	self.title = @"Edit Ingredient";
}

- (void)viewWillDisappear:(BOOL)animated
{
	if (editingTextField != nil) {
		[self saveTextFieldValue:editingTextField];
		editingTextField = nil;
		self.selectedIndexPath = nil;
	}

	// notify delegate
	if (changedIngredient) {
		NSError *error;
		if (![self.ingredient.managedObjectContext save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}

		if (delegate
				&& [delegate conformsToProtocol:@protocol(MealIngredientEditViewDelegate)]) {
			[delegate mealIngredientEditView:self didUpdateIngredient:self.ingredient];
		}
/*
		// Sync the change out to the cloud
		[BCObjectManager syncMeals:SendOnly];
*/
	}
	changedIngredient = NO;

/*
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:UIKeyboardWillHideNotification object:nil];
*/

    [super viewWillDisappear:animated];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - local methods
- (void)addToShoppingListValueChanged:(id)sender
{
	UISegmentedControl *addToSL = sender;
	self.ingredient.addToShoppingList = [NSNumber numberWithInteger:[addToSL selectedSegmentIndex]];
	NSError *error;
	if (![self.ingredient.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	[self.tableView reloadData];
	changedIngredient = YES;
}

- (UITableViewCell *)createCellWithLabelText:(NSString *)labelText
textFieldKeyboardType:(UIKeyboardType)keyboardType
{
	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
		reuseIdentifier:nil];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;

	UILabel *aLabel = [[UILabel alloc] 
		initWithFrame:CGRectMake(10, 10, 100, 25)];
	aLabel.adjustsFontSizeToFitWidth = YES;
	aLabel.textAlignment = NSTextAlignmentRight;
	aLabel.font = [UIFont boldSystemFontOfSize:14];
	aLabel.text = labelText;
	aLabel.backgroundColor = [UIColor clearColor];
	[cell.contentView addSubview:aLabel];

	UITextField *aTextField = [[UITextField alloc]
		initWithFrame:CGRectMake(115, 12, 175, 25)];
	aTextField.enabled = NO;
	aTextField.tag = kTagMIETextField;
	[aTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[aTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
	aTextField.keyboardType = keyboardType;
	[cell.contentView addSubview:aTextField];

	return cell;
}

- (void)saveTextFieldValue:(UITextField *)textField
{
	BOOL changedValue = NO;
	switch ([self.selectedIndexPath section]) {
		case kSectionIngredientDetails:
			switch ([self.selectedIndexPath row]) {
				case kRowIngredientName:
					if (![self.ingredient.name isEqualToString:textField.text]) {
						changedValue = YES;
						self.ingredient.name = textField.text;
						if (!self.ingredient.productId || ![self.ingredient.productId integerValue]) {
							// This isn't a 'real' product, so update the shopping list text to match the name
							self.ingredient.shoppingListText = self.ingredient.name;
							// Also clear the category for the shopping list item, as the category we
							// have may not be accurate any longer
							self.ingredient.shoppingListCategory = nil;
						}
					}
					break;
				case kRowQuantity:
					if (![self.ingredient.measure isEqualToString:textField.text]) {
						changedValue = YES;
						self.ingredient.measure = textField.text;
					}
					break;
				case kRowUnits:
					if (![self.ingredient.measureUnits isEqualToString:textField.text]) {
						changedValue = YES;
						self.ingredient.measureUnits = textField.text;
					}
					break;
				case kRowNotes:
					if (![self.ingredient.notes isEqualToString:textField.text]) {
						changedValue = YES;
						self.ingredient.notes = textField.text;
					}
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}

	if (changedValue) {
		// Update the fullText with the changes made
		self.ingredient.fullText = [self.ingredient generateFullText];

		// Clear the relationship to the original 'recipe' since they've changed it
		self.ingredient.recipe.externalId = nil;
		changedIngredient = YES;
	}

	textField.enabled = NO;
}

- (void)accessoryTapped:(UITapGestureRecognizer *)recognizer
{
	// Show the details of this shopping list item (product)
	ProductDetailViewController *productView = [[ProductDetailViewController alloc]
		initWithNibName:nil bundle:nil];
	ProductDetailItem *item = [[ProductDetailItem alloc] init];
	item.name = self.ingredient.shoppingListText;
	item.productId = self.ingredient.productId;
	productView.productDetailItem = item;
	productView.managedObjectContext = self.ingredient.managedObjectContext;

	[self.navigationController pushViewController:productView animated:YES];

}

- (void)incrementTapped:(UITapGestureRecognizer *)recognizer
{
	self.ingredient.shoppingListQuantity = [NSNumber numberWithInteger:
		[self.ingredient.shoppingListQuantity doubleValue] + 1];

	[self configureCell:self.addToShoppingListCell
		atIndexPath:[NSIndexPath indexPathForRow:kRowShoppingListItem
		inSection:kSectionShoppingList]];

	changedIngredient = YES;
}

- (void)decrementTapped:(UITapGestureRecognizer *)recognizer
{
	if ([self.ingredient.shoppingListQuantity doubleValue] > 1) {
		self.ingredient.shoppingListQuantity = [NSNumber numberWithInteger:
			[self.ingredient.shoppingListQuantity doubleValue] - 1];

		[self configureCell:self.addToShoppingListCell
			atIndexPath:[NSIndexPath indexPathForRow:kRowShoppingListItem
			inSection:kSectionShoppingList]];

		changedIngredient = YES;
	}
}

- (void)resetShoppingListProductTapped:(UITapGestureRecognizer *)recognizer
{
	// If this is not a 'real' product, ignore this tap
	if (!self.ingredient.productId || ![self.ingredient.productId integerValue]) {
		return;
	}

	self.ingredient.productId = [NSNumber numberWithInteger:0];
	self.ingredient.shoppingListText = self.ingredient.name;
	// Also clear the category for the shopping list item, as the category we have may not be
	// accurate any longer
	self.ingredient.shoppingListCategory = nil;

	[self configureCell:self.addToShoppingListCell
		atIndexPath:[NSIndexPath indexPathForRow:kRowShoppingListItem
		inSection:kSectionShoppingList]];

	changedIngredient = YES;
}

- (void)associateShoppingListProductTapped:(UITapGestureRecognizer *)recognizer
{
	// TODO use the new search view
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rows;

	switch (section) {
		case kSectionIngredientDetails:
			rows = 4;
			break;
		case kSectionShoppingList:
			if ([self.ingredient.addToShoppingList integerValue] != kSegmentNo) {
				rows = 2;
			}
			else {
				rows = 1;
			}
			break;
		default:
			rows = 0;
			break;
	}

	return rows;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView
cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [self.rowCells BC_nestedObjectAtIndexPath:indexPath];

	[self configureCell:cell atIndexPath:indexPath];

    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	switch ([indexPath section]) {
		case kSectionIngredientDetails:
		{
			id <GenericValueDisplay, NSObject> rowValue = nil;
			NSString *placeholder = nil;
			switch ([indexPath row]) {
				case kRowIngredientName:
					rowValue = self.ingredient.name;
					placeholder = @"e.g. onion";
					break;
				case kRowQuantity:
					rowValue = self.ingredient.measure;
					placeholder = @"e.g. 1 1/2";
					break;
				case kRowUnits:
					rowValue = self.ingredient.measureUnits;
					placeholder = @"e.g. cups";
					break;
				case kRowNotes:
					rowValue = self.ingredient.notes;
					placeholder = @"e.g. chopped";
					break;
				default:
					break;
			}
			UITextField *aTextField = (UITextField *)[cell viewWithTag:kTagMIETextField];
			if (rowValue) {
				aTextField.text = [rowValue genericValueDisplay];
			}
			aTextField.placeholder = placeholder;
			break;
		}
		case kSectionShoppingList:
			if ([indexPath row] == kRowAddToShoppingList) {
				UISegmentedControl *addToSL = (UISegmentedControl *)[cell viewWithTag:kTagMIEAddToSLAsk];
				if (self.ingredient.addToShoppingList) {
					[addToSL setSelectedSegmentIndex:[self.ingredient.addToShoppingList integerValue]];
				}
				else {
					[addToSL setSelectedSegmentIndex:kSegmentNo];
				}
			}
			else {
				if (!self.ingredient.shoppingListText || ![self.ingredient.shoppingListText length]) {
					self.ingredient.shoppingListText = self.ingredient.name;
				}
				self.shoppingListItemCell.textLabel.text = self.ingredient.shoppingListText;

				if (!self.ingredient.shoppingListQuantity
						|| ![self.ingredient.shoppingListQuantity doubleValue]) {
					self.shoppingListItemCell.quantityLabel.text = @"1";
				}
				else {
					self.shoppingListItemCell.quantityLabel.text = [self.ingredient.shoppingListQuantity
						stringValue];
				}
				if ([self.ingredient.productId integerValue]) {
					// Actual product, present the 'Clear' button
					self.shoppingListItemCell.clearButton.hidden = NO;
					self.shoppingListItemCell.associateButton.hidden = YES;
				}
				else {
					// Free text product, present the 'Associate Product' button
					self.shoppingListItemCell.clearButton.hidden = YES;
					self.shoppingListItemCell.associateButton.hidden = NO;
				}
			}
			break;
		default:
			break;
	}
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)aTableView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 0;
	switch ([indexPath section]) {
		case kSectionIngredientDetails:
			height = 44;
			break;
		case kSectionShoppingList:
			if ([indexPath row] == kRowAddToShoppingList) {
				height = 80;
			}
			else {
				height = 103;
			}
			break;
		default:
			break;
	}

	return height;
}

- (NSIndexPath *)tableView:(UITableView *)tableView 
willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	return indexPath;
}

- (void)tableView:(UITableView *)theTableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingTextField) {
		editingTextField.enabled = NO;
		editingTextField = nil;
	}

	switch ([indexPath section]) {
		case kSectionIngredientDetails:
		{
			self.selectedIndexPath = indexPath;
			UITableViewCell *cell = [rowCells BC_nestedObjectAtIndexPath:indexPath];
			UITextField *aTextField = (UITextField *)[cell viewWithTag:kTagMIETextField];
			editingTextField = aTextField;
			editingTextField.enabled = YES;
			editingTextField.delegate = self;
			[editingTextField becomeFirstResponder];
			[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
			break;
		}
		default:
			break;
	}

}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[editingTextField resignFirstResponder];

	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[self saveTextFieldValue:textField];

	editingTextField = nil;
	self.selectedIndexPath = nil;
}

#pragma mark - ProductSearchViewDelegate
- (void)productSearchView:(id)productSearchView
didSelectProduct:(NSDictionary *)product
{
	NSString *productName = [product valueForKey:kItemName];
	self.ingredient.shoppingListText = productName;
	self.ingredient.shoppingListCategory = [CategoryMO categoryById:[[product valueForKey:kProductCategoryId]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle]
		withManagedObjectContext:self.ingredient.managedObjectContext];

	NSNumber *productId = [[product valueForKey:kProductId]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
	self.ingredient.productId = productId;

	[self configureCell:self.addToShoppingListCell
		atIndexPath:[NSIndexPath indexPathForRow:kRowShoppingListItem
		inSection:kSectionShoppingList]];

	changedIngredient = YES;
}

#pragma mark - Notifications for keyboard hide/show
- (void)keyboardWillShowNotification:(NSNotification*)aNotification
{
	//
	// Remove any previous view offset.
	//
	[self keyboardWillHideNotification:nil];
	
	//
	// Only animate if the text field is part of the hierarchy that we manage.
	//
	UIView *parentView = [editingTextField superview];
	while (parentView != nil && ![parentView isEqual:self.view])
	{
		parentView = [parentView superview];
	}
	if (parentView == nil)
	{
		//
		// Not our hierarchy... ignore.
		//
		return;
	}
	
	CGRect keyboardRect = [self.view.superview
		convertRect:[[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue]
		fromView:nil];
	
	CGRect viewFrame = self.view.frame;

	textFieldAnimatedDistance = 0;
	if (keyboardRect.origin.y < viewFrame.origin.y + viewFrame.size.height)
	{
		textFieldAnimatedDistance = (viewFrame.origin.y + viewFrame.size.height) - (keyboardRect.origin.y - viewFrame.origin.y);
		viewFrame.size.height = keyboardRect.origin.y - viewFrame.origin.y;

		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.33];
		[self.view setFrame:viewFrame];
		[UIView commitAnimations];
	}
}

- (void)keyboardWillHideNotification:(NSNotification*)aNotification
{
	if (textFieldAnimatedDistance == 0)
	{
		return;
	}
	
	CGRect viewFrame = self.view.frame;
	viewFrame.size.height += textFieldAnimatedDistance;
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.33];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
	
	textFieldAnimatedDistance = 0;
}   

@end
