//
//  BCTableViewCell
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/27/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import "BCTableViewCell.h"

@implementation BCTableViewCell

- (UIActivityIndicatorView *)spinner
{
	if (!_spinner) {
		_spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		_spinner.hidesWhenStopped = YES;
		_spinner.center = self.imageView.center;
		[self.contentView addSubview:_spinner];
	}
	return _spinner;
}

- (StarRatingControl *)ratingControl
{
	if (!self.ratingControl) {
		self.ratingControl = [[StarRatingControl alloc] initWithFrame:CGRectMake(245, 5, 73, 16)];
		self.ratingControl.userInteractionEnabled = NO;
		self.ratingControl.emptyStar = [UIImage imageNamed:@"RatingStar-Empty.png"];
		self.ratingControl.fullStar = [UIImage imageNamed:@"RatingStar-Gold.png"];
		[self.contentView addSubview:self.ratingControl];
	}

	return self.ratingControl;
}

@end
