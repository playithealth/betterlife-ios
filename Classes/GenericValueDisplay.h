//
//  GenericValueDisplay.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/23/2010
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GenericValueDisplay
- (NSString *)genericValueDisplay;
@end

@interface NSString (GenericValueDisplay) <GenericValueDisplay>
- (NSString *)genericValueDisplay;
@end

@interface NSNumber (GenericValueDisplay) <GenericValueDisplay>
- (NSString *)genericValueDisplay;
@end

@interface NSDate (GenericValueDisplay) <GenericValueDisplay>
- (NSString *)genericValueDisplay;
@end

@interface NSNull (GenericValueDisplay) <GenericValueDisplay>
- (NSString *)genericValueDisplay;
@end
