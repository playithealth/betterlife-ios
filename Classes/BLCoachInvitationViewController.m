//
//  BLCoachInvitationViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 7/25/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLCoachInvitationViewController.h"

#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCObjectManager.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BLOnboardWelcomeViewController.h"
#import "CoachInviteMO.h"
#import "GTMHTTPFetcherAdditions.h"

@interface BLCoachInvitationViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *coachImageView;
@property (weak, nonatomic) IBOutlet UILabel *coachInviteLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *sentOnLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BLCoachInvitationViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:(NSCoder *)aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

	// If we're running via onboarding, change the title to match onboarding, and show the 'Next' button
	if (self.viewStack) {
		self.title = [NSString stringWithFormat:@"Welcome: %ld of %ld",
			(long)[[self.navigationController viewControllers] count] - 1, (long)[self.viewStack count] - 1];
	}
	else {
		self.title = @"Coach Invitation";
		self.navigationItem.rightBarButtonItem = nil;
	}

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateView:) name:[CoachInviteMO entityName] object:nil];

	// Quasi-hack to cause the table view to not show 'blank' rows at the bottom
	self.tableView.tableFooterView = [[UIView alloc] init];

	[self updateView:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - local
- (void)updateView:(NSNotification *)notification
{
	if (self.invitation) {
		[self.activityIndicator startAnimating];
		NSString *size = kImageSizeLarge;
		// Leverage info in the invitation to populate these UI Elements
		self.coachInviteLabel.text = [NSString stringWithFormat:@"You've been invited to be coached by %@ of %@", self.invitation.advisorName,
			self.invitation.rootEntityName];
		self.sentOnLabel.text = [NSString stringWithFormat:@"Sent on %@",
			[NSDateFormatter localizedStringFromDate:[NSDate dateWithTimeIntervalSince1970:[self.invitation.createdOn doubleValue]]
				dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle]];
		UIImage *image = [[BCImageCache sharedCache] imageWithCoachId:self.invitation.advisorId size:size];
		if (image) {
			[self.activityIndicator stopAnimating];
			if (image != (id)[NSNull null]) {
				self.coachImageView.image = image;
			}
			else {
			}
		}
		else {
			GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:
				[BCUrlFactory imageURLForCoachId:self.invitation.advisorId imageSize:size]];

			BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
			[appDelegate setNetworkActivityIndicatorVisible:YES];

			__typeof__(self) __weak weakself = self;
			[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
				[appDelegate setNetworkActivityIndicatorVisible:NO];

				if (error != nil) {
					if (error.code == 404) {
						// If we got a 404, then leave a null placeholder, which tells us that there is no image
						[[BCImageCache sharedCache] setImage:[NSNull null] forCoachId:weakself.invitation.advisorId size:size];
					}
					else {
						[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
					}
				}
				else {
					UIImage *image = [[UIImage alloc] initWithData:retrievedData];
					if (image) {
						[[BCImageCache sharedCache] setImage:image forCoachId:weakself.invitation.advisorId size:size];

						weakself.coachImageView.image = image;
					}
					else {
						[[BCImageCache sharedCache] setImage:[NSNull null] forCoachId:weakself.invitation.advisorId size:size];
					}
				}

				[self.activityIndicator stopAnimating];
			}];
		}
	}
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions
- (IBAction)acceptInvitePressed:(UIButton *)sender
{
	if (self.invitation) {
		self.invitation.status = kStatusPut;
	}

	[self dismissView];
}

- (IBAction)declineInvitePressed:(UIButton *)sender
{
	if (self.invitation) {
		self.invitation.status = kStatusDelete;
	}

	[self dismissView];
}

- (IBAction)nextViewController:(id)sender {
	if (self.delegate) {
		[self.delegate nextView];
	}
}

- (void)dismissView
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:[CoachInviteMO entityName] object:nil];

	[self.managedObjectContext BL_save];
	[BCObjectManager syncCheck:SendAndReceive];

	// Check the nav stack, and if this view is the only thing on it, then send the user to the initial view controller
	if ([self.navigationController.viewControllers count] == 1) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate gotoInitialViewController];
	}
	else {
		// Pop back to the prior view
		[self.navigationController popViewControllerAnimated:YES];
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Only 'active' when run from onboarding
	return (self.viewStack ? 1 : 0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Only 'active' when run from onboarding
	return (self.viewStack ? 2 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellId = @"AcceptDecline";

	UITableViewCell *cell = cell = [tableView dequeueReusableCellWithIdentifier:cellId];
	
	[cell setAccessoryType:UITableViewCellAccessoryNone];
	if (indexPath.row == 0) {
		cell.textLabel.text = @"Accept";
		if ([self.invitation.status isEqualToString:kStatusPut]) {
			[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
		}
	}
	else {
		cell.textLabel.text = @"Decline";
		if ([self.invitation.status isEqualToString:kStatusDelete]) {
			[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
		}
	}

	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0) {
		self.invitation.status = kStatusPut;
	}
	else {
		self.invitation.status = kStatusDelete;
	}

	[self.tableView reloadData];
}

@end
