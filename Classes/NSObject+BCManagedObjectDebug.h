//
//  NSObject+BCManagedObjectDebug.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 12/14/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#if DEBUG
@interface NSObject(BCManagedObjectDebug)
+ (NSMutableArray *)BCProperties;
@end

@interface NSManagedObject(BCManagedObjectDebug)
- (NSString *)description;
@end
#endif
