//
//  OAuthConsumer.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/22/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "OAuthConsumer.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"

#define kFilename   @"consumer"
#define kDataKey    @"Data"

@interface OAuthConsumer ()
@property (nonatomic, strong) KeychainItemWrapper *oauthConsumerKeychainItem;
@end

@implementation OAuthConsumer

- (id)init {
	return [self initWithCoder:nil];
}

- (id)initWithCoder:(NSCoder *)decoder {
	self = [super init];
	if (self) {
		self.oauthConsumerKeychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"OAuthConsumer" accessGroup:nil];
		if (decoder) {
			self.oauthConsumerKey = [decoder decodeObjectForKey:kOAuthConsumerKey];
			self.oauthConsumerSecret = [decoder decodeObjectForKey:kOAuthConsumerSecret];
			self.oauthConsumerHost = [decoder decodeObjectForKey:kOAuthConsumerHost];
		}
	}
	return self;
}

- (BOOL)consumerIsValid {
	return (([self.oauthConsumerHost isEqualToString:[BCUrlFactory apiRootURLString]])
		&& ([self.oauthConsumerKey length] > 0)
		&& ([self.oauthConsumerSecret length] > 0));
}

+ (OAuthConsumer *)currentOAuthConsumer {
	static OAuthConsumer *currentOAuthConsumer;

	@synchronized(self) {
		if (!currentOAuthConsumer) {
			currentOAuthConsumer = [[OAuthConsumer alloc] init];

			// Check to see if we got values from keychain, as if not, we may need to load them from the  old files
			if (!currentOAuthConsumer.oauthConsumerHost || [currentOAuthConsumer.oauthConsumerHost isEqualToString:@""]) {
				OAuthConsumer *oldConsumer = [OAuthConsumer loadOAuthConsumer];
				if (oldConsumer) {
					currentOAuthConsumer.oauthConsumerHost = oldConsumer.oauthConsumerHost;
					currentOAuthConsumer.oauthConsumerKey = oldConsumer.oauthConsumerKey;
					currentOAuthConsumer.oauthConsumerSecret = oldConsumer.oauthConsumerSecret;

					// Remove the old mechanism of the consumer file
					[OAuthConsumer deleteConsumerArchive];
				}
			}
		}
		return currentOAuthConsumer;
	}
}

+ (void)refreshOAuthConsumerWithSuccess:(void(^)(void))success failure:(void(^)(NSError *))failure {
	[OAuthConsumer clearOAuthConsumer];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher oauthConsumerFetcher];
	[fetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		if (error != nil) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
			if (failure) {
				failure(error);
			}
		} 
		else {
			NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			DDLogInfo(@"OAuth Consumer Fetch succeeded, results:%@", [resultsDict description]);

			if ([resultsDict objectForKey:kOAuthConsumerKey] != nil) {
				OAuthConsumer *thisConsumer = [OAuthConsumer currentOAuthConsumer];
				thisConsumer.oauthConsumerKey = [resultsDict objectForKey:kOAuthConsumerKey];
				thisConsumer.oauthConsumerSecret = [resultsDict objectForKey:kOAuthConsumerSecret];
				thisConsumer.oauthConsumerHost = [BCUrlFactory apiRootURLString];
			}

			if (success) {
				success();
			}
		}
	}];
}

+ (OAuthConsumer *)loadOAuthConsumer {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataFilePath = [documentsDirectory stringByAppendingPathComponent:kFilename];
	NSData *data = [[NSMutableData alloc] initWithContentsOfFile:dataFilePath];
	NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
	OAuthConsumer *outOAuthConsumer = [unarchiver decodeObjectForKey:kDataKey];
	[unarchiver finishDecoding];
	return outOAuthConsumer;
}

// Deprecated concept, this is still here to eliminate the old consumer file
+ (BOOL)deleteConsumerArchive {
	// delete the consumer archive
	NSError *error = nil;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
	NSString *archivePath = [documentsPath stringByAppendingPathComponent:@"consumer"];

	if ([fileManager fileExistsAtPath:archivePath]) {
		if (![fileManager removeItemAtPath:archivePath error:&error]) {
			NSLog(@"Unresolved error trying to remove deprecated consumer store %@, %@", error, [error userInfo]);
			return NO;
		}
	}

	return YES;
}

+ (void)clearOAuthConsumer
{
	OAuthConsumer *inConsumer = [OAuthConsumer currentOAuthConsumer];
	inConsumer.oauthConsumerKey = @"";
	inConsumer.oauthConsumerSecret = @"";
	inConsumer.oauthConsumerHost = @"";
}

#pragma mark - local
- (NSString*)oauthConsumerHost
{
	return [self.oauthConsumerKeychainItem objectForKey:(__bridge id)kSecAttrDescription];
}

- (void)setOauthConsumerHost:(NSString *)consumerHost
{
	[self.oauthConsumerKeychainItem setObject:consumerHost forKey:(__bridge id)kSecAttrDescription];
}

- (NSString*)oauthConsumerKey
{
	return [self.oauthConsumerKeychainItem objectForKey:(__bridge id)kSecAttrGeneric];
}

- (void)setOauthConsumerKey:(NSString *)consumerKey
{
	[self.oauthConsumerKeychainItem setObject:consumerKey forKey:(__bridge id)kSecAttrGeneric];
}

- (NSString*)oauthConsumerSecret
{
	return [self.oauthConsumerKeychainItem objectForKey:(__bridge id)kSecValueData];
}

- (void)setOauthConsumerSecret:(NSString *)consumerSecret
{
	[self.oauthConsumerKeychainItem setObject:consumerSecret forKey:(__bridge id)kSecValueData];
}

#pragma mark NSCoding
- (void)encodeWithCoder:(NSCoder *)encoder {
	[encoder encodeObject:self.oauthConsumerKey forKey:kOAuthConsumerKey];
	[encoder encodeObject:self.oauthConsumerSecret forKey:kOAuthConsumerSecret];
	[encoder encodeObject:self.oauthConsumerHost forKey:kOAuthConsumerHost];
}

#pragma mark NSCopying
- (id)copyWithZone:(NSZone *)zone {
	OAuthConsumer *consumerCopy = [[OAuthConsumer allocWithZone:zone] init];
	consumerCopy.oauthConsumerKey = self.oauthConsumerKey; 
	consumerCopy.oauthConsumerSecret = self.oauthConsumerSecret; 
	consumerCopy.oauthConsumerHost = self.oauthConsumerHost; 
	return consumerCopy;
}
@end
