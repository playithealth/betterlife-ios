//
//  UIViewAdditions.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/19/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "UIViewAdditions.h"

#import "QuartzCore/QuartzCore.h"

@implementation UIView (UIViewAdditions)
- (void)addBoundsOvershootAnimation
{
	CAKeyframeAnimation *boundsOvershootAnimation = nil;

	boundsOvershootAnimation = [CAKeyframeAnimation 
		animationWithKeyPath:@"transform"];

	CATransform3D startingScale = CATransform3DMakeScale(1, 1, 1);
	CATransform3D overshootScale = CATransform3DMakeScale(1.1, 1.1, 1);
	CATransform3D undershootScale = CATransform3DMakeScale(0.9, 0.9, 1);
	CATransform3D endingScale = CATransform3DIdentity;

	NSArray *boundsValues = [NSArray arrayWithObjects:
		[NSValue valueWithCATransform3D:startingScale],
		[NSValue valueWithCATransform3D:overshootScale],
		[NSValue valueWithCATransform3D:undershootScale],
		[NSValue valueWithCATransform3D:endingScale], nil];
	[boundsOvershootAnimation setValues:boundsValues];

	NSArray *times = [NSArray arrayWithObjects:
		[NSNumber numberWithFloat:0.0f],
		[NSNumber numberWithFloat:0.5f],
		[NSNumber numberWithFloat:0.9f],
		[NSNumber numberWithFloat:1.0f], nil];
	[boundsOvershootAnimation setKeyTimes:times];

	NSArray *timingFunctions = [NSArray arrayWithObjects:
		[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
		[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
		[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
		nil];
	[boundsOvershootAnimation setTimingFunctions:timingFunctions];
	boundsOvershootAnimation.fillMode = kCAFillModeForwards;
	boundsOvershootAnimation.removedOnCompletion = NO;

	[self.layer addAnimation:boundsOvershootAnimation forKey:@"scale"];
}
@end
