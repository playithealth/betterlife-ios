//
//  BCTextFieldCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/10/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCTextFieldCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *bcImageView;
@property (strong, nonatomic) IBOutlet UILabel *bcTextLabel;
@property (strong, nonatomic) IBOutlet UITextField *bcTextField;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end
