//
//  MealDetailViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/18/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "RecipeDetailViewController.h"

#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCProgressHUD.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"
#import "GenericValueDisplay.h"
#import "MealDetailCell.h"
#import "MealIngredient.h"
#import "MealPlannerAskViewController.h"
#import "MealPlannerShoppingListItem.h"
#import "MealShoppingListItemsViewController.h"
#import "NumberValue.h"
#import "RecipeTagMO.h"
#import "RecipeTagsViewController.h"
#import "ShoppingListItem.h"
#import "StarRatingControl.h"
#import "UIColor+Additions.h"
#import "UIImage+Additions.h"
#import "User.h"

static const NSInteger kTagTextView = 10001;

static const NSInteger kAddToSLNever = 0;
static const NSInteger kAddToSLAlways = 1;
static const NSInteger kAddToSLAsk = 2;

static const NSInteger kCellContentPadding = 10;
static const NSInteger kServesFontSize = 14;
static const NSInteger kServesLabelWidth = 165;

static const NSInteger kDefaultRowHeight = 44;
static const NSInteger kDetailsMinHeight = 130;
static const NSInteger kTagsMinHeight = kDefaultRowHeight;

@interface RecipeDetailViewController () <MealPlannerAskViewDelegate, RecipeTagsViewDelegate>

@property (strong, nonatomic) NSString *instructionsText;
@property (strong, nonatomic) NSString *ingredientsText;
@property (strong, nonatomic) NSString *detailsText;
@property (strong, nonatomic) NSString *tagsString;
@property (assign, nonatomic) BOOL mealChanged;
@property (assign, nonatomic) CGFloat ingredientsRowHeight;
@property (assign, nonatomic) CGFloat instructionsRowHeight;
@property (assign, nonatomic) CGFloat detailsRowHeight;

@end

@implementation RecipeDetailViewController

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.mealChanged = NO;

		self.ingredientsRowHeight = kDefaultRowHeight;
		self.instructionsRowHeight = kDefaultRowHeight;
		self.detailsRowHeight = kDetailsMinHeight;
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// If we have an moc, then this is a recipe we've added, so allow editing
	if (self.recipe.managedObjectContext) {
		if ([self.recipe.isOwner boolValue]) {
			UIBarButtonItem *editButton = [[UIBarButtonItem alloc]
				initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editMeal:)];
			self.navigationItem.rightBarButtonItem = editButton;
		}
		else {
			self.navigationItem.rightBarButtonItem = nil;
		}
		[self configureViewWithRecipe:self.recipe];
	}
	else {
		// This is a temporary MO being used to display the recipe, allow them to add it as a recipe
		// Load up the details of this recipe
		[self fetchRecipeDetails];
	}
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowTags"]) {
		RecipeTagsViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;
		detailView.recipe = self.recipe;
	}
}

#pragma mark - Local methods
- (void)fetchRecipeDetails
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory recipeDetailsURLForRecipeId:self.recipeId]];
	[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error != nil) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			[self configureViewWithRecipe:resultsDict];

			UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addToMeals:)];
			self.navigationItem.rightBarButtonItem = addButton;

			[self.tableView reloadData];
		}

		[appDelegate setNetworkActivityIndicatorVisible:NO];
	}];
}

- (void)loadRecipeImage
{
	MealDetailCell *mdCell = (MealDetailCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:Details]];
	if (self.recipe.imageId && self.recipe.imageIdValue > 0) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory imageURLForImage:self.recipe.imageId imageSize:@"medium"]];

		[imageFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {            
			if (error != nil) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:imageFetcher response:nil];
			}
			else {
				UIImage *recipeImage = [[UIImage alloc] initWithData:retrievedData];
				[[BCImageCache sharedCache] setImage:recipeImage forImageId:self.recipe.imageId size:@"medium"];
				mdCell.mealImageView.image = recipeImage;
			}
			[appDelegate setNetworkActivityIndicatorVisible:NO];
		}];
	}
	else if (self.recipe.imageUrl) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate setNetworkActivityIndicatorVisible:YES];

		NSURL *url = [NSURL URLWithString:self.recipe.imageUrl];
		GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher fetcherWithURL:url httpMethod:kGet body:nil];

		[imageFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
			if (error != nil) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:imageFetcher response:nil];
			} 
			else {
				UIImage *recipeImage = [[UIImage alloc] initWithData:retrievedData];
				[[BCImageCache sharedCache] setImage:recipeImage forImageURL:self.recipe.imageUrl size:@"medium"];
				mdCell.mealImageView.image = recipeImage;
			}
			[appDelegate setNetworkActivityIndicatorVisible:NO];
		}];
	}
	else {
		mdCell.mealImageView.image = [UIImage imageNamed:kStockImageRecipe];
	}
}

- (void)editMeal:(id)sender
{
	MealEditViewController *editView = [self.storyboard instantiateViewControllerWithIdentifier:@"MealEdit"];
	editView.delegate = self;
	editView.recipe = self.recipe;

	[self.navigationController pushViewController:editView animated:YES];

	self.navigationItem.title = @"Done";
}

- (void)addToMeals:(id)sender
{
	RecipeMO *newMeal = [RecipeMO insertInManagedObjectContext:self.managedObjectContext];
	newMeal.accountId = [[User currentUser] accountId];
	newMeal.status = kStatusPost;
	[newMeal setWithRecipeMO:self.recipe];
	self.recipe = newMeal;
	NSError *error;
	if (![self.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	// Switch out the plus button for an edit button
	UIBarButtonItem *editButton = [[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editMeal:)];
	self.navigationItem.rightBarButtonItem = editButton;

	// Reload the table so that the shopping list items row has a chance to appear
	[self.tableView reloadData];

	[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"Added %@ to meals", newMeal.name]
		onView:self.tableView];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	switch ([indexPath section]) {
		case Details:
		{
			MealDetailCell *mdCell = (MealDetailCell *)cell;
			mdCell.mealNameLabel.text = self.recipe.name;
			mdCell.starRatingControl.rating = [self.recipe.rating integerValue];
			mdCell.servesLabel.text = self.detailsText;

			CGSize size = [mdCell sizeThatFits:mdCell.frame.size];
			self.detailsRowHeight = size.height;

			[mdCell sizeToFit];
			[mdCell layoutIfNeeded];

			UIImage *recipeImage = nil;
			if (self.recipe.imageId && self.recipe.imageIdValue > 0) {
				recipeImage = [[BCImageCache sharedCache] imageWithImageId:self.recipe.imageId];
			}
			else if (self.recipe.imageUrl) {
				recipeImage = [[BCImageCache sharedCache] imageWithImageURL:self.recipe.imageUrl];
			}

			if (recipeImage) {
				mdCell.mealImageView.image = recipeImage;
			}
			else {
				mdCell.mealImageView.image = [UIImage imageNamed:kStockImageRecipe];

				[self loadRecipeImage];
			}

			break;
		}
		case Tags:
		{
			cell.textLabel.text = @"Tags";
			cell.detailTextLabel.text = self.tagsString;

			break;
		}
		case Ingredients:
		{
			UITextView *textView = (UITextView *)[cell viewWithTag:kTagTextView];
			textView.text = self.ingredientsText;
			textView.frame = CGRectMake(2, 1, 296, self.ingredientsRowHeight - kCellContentPadding);
			textView.backgroundColor = [UIColor BC_groupedTableViewCellBackgroundColor];
			
			break;
		}
		case Instructions:
		{
			UITextView *textView = (UITextView *)[cell viewWithTag:kTagTextView];
			textView.text = self.instructionsText;
			textView.frame = CGRectMake(2, 1, 296, self.instructionsRowHeight - kCellContentPadding);
			textView.backgroundColor = [UIColor BC_groupedTableViewCellBackgroundColor];

			break;
		}
		case AddToShoppingList:
		case SetupShoppingListItems:
		default:
		break;
	}
}

- (void)processIngredientsAfterAskView
{
	NSInteger ingredientCount = 0;
	// Iterate through the ingredients and add the ingredients marked for add
	for (MealIngredient *ingredient in self.recipe.ingredients) {
		switch ([ingredient.addToShoppingList integerValue]) {
			case kAddToSLNever:
			default:
				// marked NEVER, skip it
				break;
			case kAddToSLAsk:
				if (!ingredient.answer || [ingredient.answer boolValue] == NO) {
					// marked ask, but not selected, skip it
					continue;
				}
				// FALL THRU
			case kAddToSLAlways:
			{
				// add or increment this item on the shopping list 
				[ShoppingListItem addOrIncrementItemNamed:ingredient.shoppingListText
					productId:ingredient.productId category:ingredient.shoppingListCategory
					barcode:nil quantity:[ingredient.shoppingListQuantity doubleValue] 
					inMOC:self.recipe.managedObjectContext];

				ingredientCount++;

				break;
			}
		}
	}

	if (ingredientCount > 0) {
		NSString *alertText = [NSString stringWithFormat:
			@"Added %ld ingredients to the shopping list", (long)ingredientCount];
		[BCProgressHUD notificationWithText:alertText onView:self.navigationController.view];
	}
}

- (void)configureViewWithRecipe:(id)inRecipe
{
	// build a string representing the list of ingredients sorted by sort order
	NSInteger sortOrder = 0;
	id ingredients = [inRecipe valueForKey:@"ingredients"];
	if ([ingredients isKindOfClass:[NSArray class]]) {
		for (NSDictionary *ingredient in ingredients) {
			MealIngredient *tempMealIngredient = [[MealIngredient alloc] initWithEntity:
				[MealIngredient entityInManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:nil];
			if ([self.recipeId isKindOfClass:[NSNumber class]]) {
				[tempMealIngredient setWithRecipeDetailDictionary:ingredient];
			}
			else {
				[tempMealIngredient setWithExternalRecipeDetailDictionary:ingredient];
			}
			tempMealIngredient.sortOrder = @(sortOrder++);
			[self.recipe addIngredientsObject:tempMealIngredient];
		}
	}
	NSArray *sortedIngredients = [self.recipe.ingredients sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES]]];
	self.ingredientsText = [sortedIngredients componentsJoinedByString:@"\n"];

	// determine the height of the ingredientsText
	if (self.ingredientsText) {
		// establish the size needed for the cell - subtract 16 from the width of the frame to
		// allow for the padding that the textview has between its edges and where the text begins
		CGSize size = [self.ingredientsText sizeWithFont:[UIFont systemFontOfSize:kServesFontSize]
									   constrainedToSize:CGSizeMake(280, CGFLOAT_MAX)];
		self.ingredientsRowHeight = size.height + kCellContentPadding * 2;
	}
	else {
		self.ingredientsRowHeight = 0;
	}

	// build a string representing the list of instructions
	id instructions = [inRecipe valueForKey:@"instructions"];
	if ([instructions isKindOfClass:[NSString class]]) {
		self.recipe.instructions = instructions;
	}
	else {
		self.recipe.instructions = [instructions componentsJoinedByString:@"\n"];
	}
	self.instructionsText = self.recipe.instructions;

	// determine the height of the ingredientsText
	if (self.instructionsText) {
		// Subtract 16 from the width of the frame to allow for the padding that the
		// textview has between its edges and where the text begins
		CGSize size = [self.instructionsText sizeWithFont:[UIFont systemFontOfSize:kServesFontSize]
										constrainedToSize:CGSizeMake(280, CGFLOAT_MAX)];
		self.instructionsRowHeight = size.height + kCellContentPadding * 2;
	}
	else {
		self.instructionsRowHeight = 0;
	}

	// implode the list of tags into a single string
	if ([inRecipe isKindOfClass:[RecipeMO class]]) {
		RecipeMO *recipe = (RecipeMO *)inRecipe;
		NSSet *tags = recipe.tags;
		if ([tags count] > 0) {
			NSSet *tagNames = [tags valueForKeyPath:RecipeTagMOAttributes.name];
			self.tagsString = [[[tagNames allObjects] sortedArrayUsingSelector:@selector(compare:)] componentsJoinedByString:@","];
		}
	}
	else {
		id tags = [inRecipe valueForKey:@"tags"];
		if ([(NSArray*)tags count]) {
			self.tagsString = [tags componentsJoinedByString:@","];
		}
	}

	// build the string that represents the details
	NSMutableArray *detailsArray = [[NSMutableArray alloc] init];
	if (self.recipe.serves) {
		[detailsArray addObject:[NSString stringWithFormat:@"Serves: %@", self.recipe.serves]];
	}
	if (self.recipe.cookTime) {
		NSString *cookTimeUnit = nil;
		if (self.recipe.cookTimeUnit) {
			cookTimeUnit = self.recipe.cookTimeUnit;
		}
		else if ([self.recipe.cookTime integerValue] > 1) {
			cookTimeUnit = @"minutes";
		}
		else {
			cookTimeUnit = @"minute";
		}
		[detailsArray addObject:[NSString stringWithFormat:@"Cook time: %@ %@", self.recipe.cookTime, cookTimeUnit]];
	}
	if (self.recipe.calories) {
		[detailsArray addObject:[NSString stringWithFormat:@"Calories: %@", self.recipe.calories]];
	}
	if (self.recipe.caloriesFromFat) {
		[detailsArray addObject:[NSString stringWithFormat:@"Calories From fat: %@%%",
			self.recipe.caloriesFromFat]];
	}
	if (self.recipe.fat) {
		[detailsArray addObject:[NSString stringWithFormat:@"Fat: %@g", self.recipe.fat]];
	}
	if (self.recipe.satfat) {
		[detailsArray addObject:[NSString stringWithFormat:@"Saturated Fat: %@g", self.recipe.satfat]];
	}
	if (self.recipe.cholesterol) {
		[detailsArray addObject:[NSString stringWithFormat:@"Cholesterol: %@mg",
			self.recipe.cholesterol]];
	}
	if (self.recipe.sodium) {
		[detailsArray addObject:[NSString stringWithFormat:@"Sodium: %@mg", self.recipe.sodium]];
	}
	if (self.recipe.carbs) {
		[detailsArray addObject:[NSString stringWithFormat:@"Carbohydrates: %@g", self.recipe.carbs]];
	}
	if (self.recipe.protein) {
		[detailsArray addObject:[NSString stringWithFormat:@"Protein: %@g", self.recipe.protein]];
	}
	self.detailsText = [detailsArray componentsJoinedByString:@"\n"];

}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	NSInteger rows = 0;
	switch (section) {
		case Details:
		case Tags:
			rows = 1;
			break;
		case AddToShoppingList:
			// If there are no ingredients, no reason to show the add to shoppinglist section
			// Also, if this is not yet on their recipes list, don't show this (as determined by the temporary mo, or no moc)
			if ([self.recipe.ingredients count] && self.recipe.managedObjectContext) {
				// If there are no ingredients set to either always or ask, then no reason to show the add to sl section
				for (MealIngredient *ingredient in self.recipe.ingredients) {
					if ([ingredient.addToShoppingList integerValue] > 0) {
						rows = 1;
						break;
					}
				}
			}
			break;
		case SetupShoppingListItems:
			if ([self.recipe.ingredients count]) {
				rows = 1;
			}
			break;
		case Ingredients:
			if ([self.recipe.ingredients count]) {
				rows = 1;
			}
			else {
				rows = 0;
			}
			break;
		case Instructions:
			if ([self.recipe.instructions length]) {
				rows = 1;
			}
			else {
				rows = 0;
			}
			break;
		default:
			break;
	}

	return rows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{
    NSString *theTitle = nil;
	switch (section) {
		case Details:
			break;
		case Ingredients:
			theTitle = @"Ingredients";
			break;
		case Instructions:
			theTitle = @"Instructions";
			break;
		default:
			break;
	}
	
    return theTitle;
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *mealDetailCellId = @"MealDetail";
	static NSString *addToShoppingListCellId = @"AddToShoppingList";
	static NSString *setupShoppingListItemsCellId = @"SetupShoppingListItems";
	static NSString *tagsCellId = @"Tags";
	static NSString *textViewCellId = @"TextView";

	UITableViewCell *cell = nil;

	switch ([indexPath section]) {
		case Details:
		{
			cell = [self.tableView dequeueReusableCellWithIdentifier:mealDetailCellId];

			MealDetailCell *mdCell = (MealDetailCell *)cell;
			mdCell.mealImageView.image = [UIImage imageNamed:kStockImageRecipe];

			if (!mdCell.starRatingControl) {
				UIView *contentView = mdCell.contentView;
				UIView *mealImageView = mdCell.mealImageView;
				UIView *mealNameLabel = mdCell.mealNameLabel;
				UIView *servesLabel = mdCell.servesLabel;

				StarRatingControl *starRatingControl = [[StarRatingControl alloc] initWithFrame:CGRectMake(128, 52, 75, 15)];
				starRatingControl.userInteractionEnabled = NO;
				starRatingControl.emptyStar = [UIImage imageNamed:@"RatingStar-Empty.png"];
				starRatingControl.fullStar = [UIImage imageNamed:@"RatingStar-Gold.png"];
				starRatingControl.hidden = NO;
				starRatingControl.translatesAutoresizingMaskIntoConstraints = NO;

				mdCell.starRatingControl = starRatingControl;
				[contentView addSubview:starRatingControl];

				NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(mealNameLabel, mealImageView, servesLabel, starRatingControl);

				[contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[mealImageView]-[starRatingControl(75)]" options:0 metrics:nil views:viewsDictionary]];
				[contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[mealNameLabel]-[starRatingControl(15)]-[servesLabel]" options:0 metrics:nil views:viewsDictionary]];

				[contentView layoutIfNeeded];
			}

			break;
		}
		case Tags:
			cell = [self.tableView dequeueReusableCellWithIdentifier:tagsCellId];
			break;
		case AddToShoppingList:
			cell = [self.tableView dequeueReusableCellWithIdentifier:addToShoppingListCellId];
			break;
		case SetupShoppingListItems:
			cell = [self.tableView dequeueReusableCellWithIdentifier:setupShoppingListItemsCellId];
			break;
		case Ingredients:
		case Instructions:
			cell = [self.tableView dequeueReusableCellWithIdentifier:textViewCellId];
			break;
		default:
			break;
	}
	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 0.0f;
	switch ([indexPath section]) {
		case Details:
		{
			CGSize constraintSize = CGSizeMake(kServesLabelWidth, CGFLOAT_MAX);
			CGSize detailSize = [self.detailsText sizeWithFont:[UIFont systemFontOfSize:kServesFontSize] constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];

			rowHeight = MAX(detailSize.height + 70.0 + 12.0, kDetailsMinHeight);
			break;
		}
		case Tags:
		{
			NSString *cellText = @"Tags";
			NSString *cellDetailText = nil;
			if ([self.recipe.tags count]) {
				cellDetailText = [self.recipe tagsString];
			}
			// The width subtracted from the tableView frame depends on:
			// 40.0 for detail accessory
			// Width of icon image
			// Editing width
			// I don't think you can count on the cell being properly laid out here, so you've
			// got to hard code it based on the state of the table.
			CGSize constraintSize = CGSizeMake(self.tableView.frame.size.width - 40.0 - 50.0, CGFLOAT_MAX);
			CGSize labelSize = [cellText sizeWithFont:[UIFont boldSystemFontOfSize:18] constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
			CGSize detailSize = [cellDetailText sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];

			rowHeight = MAX(labelSize.height + detailSize.height + 12.0, kTagsMinHeight);
			break;
		}
		case Ingredients:
			rowHeight = self.ingredientsRowHeight;
			break;
		case Instructions:
			rowHeight = self.instructionsRowHeight;
			break;
		case AddToShoppingList:
			// If there are no ingredients, nothing to show
			// Also, if this is not yet on their recipes list, don't show this (as determined by the temporary mo, or no moc)
			if ([self.recipe.ingredients count] && self.recipe.managedObjectContext) {
				// If there are no ingredients set to either always or ask, then no reason to show the add to sl section
				for (MealIngredient *ingredient in self.recipe.ingredients) {
					if ([ingredient.addToShoppingList integerValue] > 0) {
						rowHeight = kDefaultRowHeight;
						break;
					}
				}
			}
			break;
		case SetupShoppingListItems:
			if ([self.recipe.ingredients count]) {
				rowHeight = kDefaultRowHeight;
			}
			break;
		default:
			rowHeight = kDefaultRowHeight;
			break;
	}
	return rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat height = 0.0f;
	switch (section) {
		default:
			height = 0.0f;
			break;
		case Ingredients:
		case Instructions:
			height = 30.0f;
			break;
	}
	return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == AddToShoppingList) {
		MealPlannerAskViewController *askView = [self.storyboard instantiateViewControllerWithIdentifier:@"MealPlannerAsk"];
		askView.delegate = self;
		askView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;

		// Iterate through the ingredients and set up the lists of ask and yes items
		for (MealIngredient *ingredient in self.recipe.ingredients) {
			switch ([ingredient.addToShoppingList integerValue]) {
				case kAddToSLAlways:
					// add to the list of items that will be added
					[askView.yesItems addObject:ingredient];
					break;
				case kAddToSLAsk:
					// add to the list of items to ask the user about
					[askView.askItems addObject:ingredient];
					break;
				case kAddToSLNever:
				default:
					// marked NEVER, skip it
					break;
			}
		}

		if ([askView.askItems count]) {
			[self presentViewController:askView animated:YES completion:nil];
		}
		else {
			[self processIngredientsAfterAskView];
		}
	}
	else if (indexPath.section == SetupShoppingListItems) {
		MealShoppingListItemsViewController *editView = [self.storyboard instantiateViewControllerWithIdentifier:@"SetupShoppingListItems"];
		editView.recipe = self.recipe;

		[self.navigationController pushViewController:editView animated:YES];

		self.navigationItem.title = @"Done";
	}
}

#pragma mark - MealEditViewDelegate
- (void)mealEditView:(MealEditViewController *)mealEditView
didUpdateMeal:(RecipeMO *)aMeal
{
	self.recipe.status = kStatusPut;

	// Just set the flag to changed, rather than saving the MOC.
	// This is because the child view may fire the delegate here when it is pushing another child,
	// rather than when popping back to this view, and in that situation we don't yet
	// want or need to save the MOC
	self.mealChanged = YES;
}

#pragma mark - MealPlannerAskViewDelegate
- (void)mealPlannerAskView:(MealPlannerAskViewController *)mealPlannerAskView didFinish:(BOOL)done
{
	if (done) {
		[self processIngredientsAfterAskView];
	}
}

#pragma mark - RecipeTagsViewController
- (void)recipeTagsView:(RecipeTagsViewController *)recipeTagsView changedTags:(BOOL)changedTags
{
	__typeof__(self) __weak weakself = self;
	[self dismissViewControllerAnimated:YES completion:^{
		if (changedTags) {
			NSSet *tags = self.recipe.tags;
			if ([tags count] > 0) {
				NSSet *tagNames = [tags valueForKeyPath:RecipeTagMOAttributes.name];
				self.tagsString = [[[tagNames allObjects] sortedArrayUsingSelector:@selector(compare:)] componentsJoinedByString:@","];
			}
			[weakself.tableView reloadSections:[NSIndexSet indexSetWithIndex:Tags] withRowAnimation:UITableViewRowAnimationAutomatic];
		}
	}];
}

@end
