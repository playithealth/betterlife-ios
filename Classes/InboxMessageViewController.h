//
//  InboxMessageViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "InboxMessage.h"

@interface InboxMessageViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *senderLabel;
@property (strong, nonatomic) IBOutlet UILabel *subjectLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIWebView *messageWebView;
@property (strong, nonatomic) IBOutlet UIToolbar *actionBar;
@property (strong, nonatomic) InboxMessage *message;

- (IBAction)deleteTapped:(id)sender;

@end
