//
//  ShoppingListViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/9/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "ShoppingListViewController.h"
#import <objc/runtime.h>

#import "BCAppDelegate.h"
#import "BCObjectManager.h"
#import "BCProgressHUD.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BLProductItem.h"
#import "CategoryMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "NumberValue.h"
#import "PantryViewController.h"
#import "PurchaseHxItem.h"
#import "RegisterAuthCodeViewController.h"
#import "ScannedItem.h"
#import "ShoppingListItem.h"
#import "Store.h"
#import "StoreCategory.h"
#import "TaskQueueItems.h"
#import "UIBarButtonItemAdditions.h"
#import "UIColor+Additions.h"
#import "UIImage+Additions.h"
#import "UIViewAdditions.h"
#import "User.h"

static const NSInteger kSectionHeaderHeight = 25;

static const NSInteger kTagAlertCheckout = 1;
static const NSInteger kTagAlertItemNotFound = 2;

@interface ShoppingListViewController ()

@property (weak, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) CustomBadge *cartQuantityBadge;
@property (strong, nonatomic) CustomBadge *shoppingListQuantityBadge;
@property (strong, nonatomic) NSIndexPath *expandedIndexPath;
@property (assign, nonatomic) ShoppingListSortMode sortMode;
@property (assign, nonatomic) BOOL showCart;
@property (weak, nonatomic) IBOutlet UILabel *currentStoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentStoreAddressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *emptyImageView;
@property (weak, nonatomic) IBOutlet UIImageView *footerImageView;
@property (weak, nonatomic) IBOutlet UIButton *checkoutButton;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewTopConstraint;
@property (strong, nonatomic) UISegmentedControl *headerSegmentedControl;

- (IBAction)scanButtonTapped:(id)sender;
- (IBAction)modeButtonTapped:(id)sender;
- (IBAction)quantityButtonTapped:(id)sender;
- (IBAction)checkboxButtonTapped:(id)sender;
- (IBAction)checkoutButtonTapped:(id)sender;
- (IBAction)incrementButtonTapped:(id)sender;
- (IBAction)decrementButtonTapped:(id)sender;
- (IBAction)snoozeButtonTapped:(id)sender;
- (IBAction)hideButtonTapped:(id)sender;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)reloadShoppingListCache;
- (void)addOrIncrementItemNamed:(NSString *)name productId:(NSNumber *)productId
	categoryId:(NSNumber *)categoryId barcode:(NSString *)barcode
	showHUDAddedTo:(UIView *)viewForHUD moveToCart:(BOOL)moveToCart;
- (void)updateBadgesAnimated:(BOOL)animate;
- (void)toggleRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)expandRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)collapseRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)getProductByBarcode:(NSString *)barcode andType:(NSString *)type;
@end

@implementation ShoppingListViewController

static char barcodeKey;

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:(NSCoder *)aDecoder];
	if (self) {
		self.showCart = NO;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];

	self.navigationController.toolbarHidden = YES;

	self.headerSegmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Shopping List", @"Cart"]];
	self.headerSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
	[self.headerSegmentedControl addTarget:self action:@selector(dataTypeChanged:) forControlEvents:UIControlEventValueChanged];
	[self.headerSegmentedControl setSelectedSegmentIndex:0];
	self.navigationItem.titleView = self.headerSegmentedControl;
	
	// Right now, being checked into a store is giving priority to the ShoppingListSortByStoreCategory
	// mode of sorting, this may need to be changed if we want the users to be able to alpha
	// sort while checked into a store, etc.
	Store *store = [Store currentStore];
	if ([store hasOrderedCategories]) {
		self.sortMode = ShoppingListSortByStoreCategory;
	}
	else {
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		self.sortMode = [defaults integerForKey:kShoppingListSortSetting];
	}

	self.emptyImageView.image = [[UIImage imageNamed:@"shoppinglist-empty"] 
		resizableImageWithCapInsets:UIEdgeInsetsMake(200, 0, 100, 0)];

	[self refresh];
}

- (void)viewWillAppear:(BOOL)animated
{
	Store *currentStore = [Store currentStore];
	[self updateStoreInfoWithStore:currentStore];

	// Try to handle the situation where the store is already set, or when the store
	// gets unset from outside this view.  If the sort mode has changed, need to ensure
	// the data is proper for that mode prior to continuing
	// Right now, being checked into a store is giving priority to the ShoppingListSortByStoreCategory
	// mode of sorting, this may need to be changed if we want the users to be able to alpha
	// sort while checked into a store, etc.
	ShoppingListSortMode newSortMode;
	if ([currentStore hasOrderedCategories]) {
		newSortMode = ShoppingListSortByStoreCategory;
	}
	else {
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		newSortMode = [defaults integerForKey:kShoppingListSortSetting];
	}

	if ((newSortMode != self.sortMode)) {
		self.sortMode = newSortMode;
	}

	// Want to receive notifications regarding syncCheck finishing
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(syncShoppingListDidFinish:)
		name:@"syncCheck" object:nil];

	// Want to receive notifications regarding changes to StoreCategories
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(didChangeStoreCategories:)
		name:UserStoresDidChangeStoreCategories
		object:nil];

	[self reloadShoppingListCache];

	[self updateBadgesAnimated:NO];

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// Remove the notification observer for changes to ShoppingList
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:@"syncCheck" object:nil];

	// Remove the notification observer for changes to StoreCategories
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:UserStoresDidChangeStoreCategories
		object:nil];

	[BCObjectManager syncCheck:SendOnly];

	[super viewWillDisappear:animated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowItemDetail"]) {
		ShoppingListItemDetailViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;
		detailView.managedObjectContext = self.managedObjectContext;

		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		ShoppingListItem *itemToEdit = [self.fetchedResultsController objectAtIndexPath:indexPath];
		detailView.shoppingListItem = itemToEdit;
	}
	else if ([segue.identifier isEqualToString:@"ShowSearch"]) {
		ShoppingListSearchViewController *searchView = segue.destinationViewController;
		searchView.managedObjectContext = self.managedObjectContext;
		searchView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"StoreCheckin"]) {
		StoreSearchViewController *storeSearchView = segue.destinationViewController;
		storeSearchView.managedObjectContext = self.managedObjectContext;
		storeSearchView.delegate = self;
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// Return YES for supported orientations
	return(interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - BCTableViewController overrides
- (void)refresh
{
	[BCObjectManager syncCheck:SendAndReceive];
}

#pragma mark - Notifications
- (void)syncShoppingListDidFinish:(NSNotification *)notification
{
	[self reloadShoppingListCache];

	[self updateBadgesAnimated:YES];
}

- (void)didChangeStoreCategories:(NSNotification *)notification
{
	// The notification must be invoked on the main thread
	if (![NSThread isMainThread]) {
		NSAssert(NO, @"Notification was not invoked on the main thread");
		return;
	}

	if (![[Store currentStore] isStoreSet]) {
		// Nothing to do here if there is no store checked into
		return;
	}

	NSNumber *storeId = [[notification userInfo] valueForKey:UserStoresDidChangeStoreId];

	// If the current store is the same as the one that just got changed,
	// need to update our view
	if ([storeId isEqualToNumber:[[Store currentStore] storeId]]) {
		[self reloadShoppingListCache];
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rowCount = 0;

	id<NSFetchedResultsSectionInfo> sectionInfo =
		[[self.fetchedResultsController sections] objectAtIndex:section];
	rowCount = [sectionInfo numberOfObjects];

	return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//QuietLog(@"%s: %d, %d", __FUNCTION__, [indexPath section], [indexPath row]);
	UITableViewCell *cell = nil;

	static NSString *normalCellId = @"ShoppingListItemCellId";
	static NSString *expandedCellId = @"ShoppingListExpandedItemCellId";
	if (self.expandedIndexPath != nil && [self.expandedIndexPath compare:indexPath] == NSOrderedSame) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:expandedCellId];
	}
	else {
		cell = [self.tableView dequeueReusableCellWithIdentifier:normalCellId];
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
	forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		// Delete the managed object for the given index path
		ShoppingListItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
		if ([item.isRecommendation boolValue]) {
			// Recommended items are snoozed when you swipe / delete, set to PUT for this
			item.status = kStatusPut;
		}
		else {
			item.status = kStatusDelete;
		}

		// Save the context.
		[self.managedObjectContext BL_save];

		dispatch_async(dispatch_get_main_queue(), ^{
			[self updateBadgesAnimated:YES];
		});
	}
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
	if (self.sortMode == ShoppingListSortByName) {
		return [self.fetchedResultsController sectionIndexTitles];
		/*
		 * TODO - I want to use a full alphabetic index, but since the actual results
		 * don't have that many sections, I have to figure out how to make it work correctly
		 * i.e. when the user clicks M the function below will try to take them to section 12,
		 * which could be Z or not exist or whatever...
		 * NSArray *sectionIndices = [NSArray arrayWithObjects:
		 *  @"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",
		 *  @"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",
		 *  nil];
		 * return sectionIndices;
		 */
	}
	else {
		return nil;
	}
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
	return index;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView
	heightForHeaderInSection:(NSInteger)section
{
	NSUInteger sectionCount = [[self.fetchedResultsController sections] count];
	if (self.sortMode == ShoppingListSortByName || self.showCart) {
		return 0.0f;
	}
	else if (section < sectionCount) {
		return kSectionHeaderHeight;
	}
	else {
		return 0.0f;
	}
}

- (UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section
{
	NSString *sectionTitle = @"";

	if (!self.showCart) {
		if ((self.sortMode == ShoppingListSortByCategory) || (self.sortMode == ShoppingListSortByStoreCategory)) {
			id<NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
			if ([[sectionInfo objects] count]) {
				ShoppingListItem *item = (ShoppingListItem *)[[sectionInfo objects] objectAtIndex:0];
				sectionTitle = item.category.name;
			}
		}
	}

	if (![sectionTitle length]) {
		return nil;
	}

	return [BCTableViewController customViewForHeaderWithTitle:sectionTitle];
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 50.0f;
	return rowHeight;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.expandedIndexPath) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.expandedIndexPath) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}
}

#pragma mark - Fetched results controller
- (NSFetchedResultsController *)fetchedResultsController
{
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}

	NSString *sectionKey = nil;
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

	[fetchRequest setEntity:[ShoppingListItem entityInManagedObjectContext:self.managedObjectContext]];
	[fetchRequest setFetchBatchSize:20];

	// filter for the user, status, cart, and purchased
	User *thisUser = [User currentUser];

	if (self.showCart) {
		// the cart should show items that are in the cart and not purchased or deleted
		fetchRequest.predicate = [NSPredicate predicateWithFormat:
			@"(accountId = %@) AND (status <> %@) AND (inCart = YES) AND (purchased = NO)",
			thisUser.accountId, kStatusDelete];

		// items in the cart should be sorted by when they were put in the cart
		NSSortDescriptor *sortByDate = [NSSortDescriptor
			sortDescriptorWithKey:@"inCartTime" ascending:NO];
		fetchRequest.sortDescriptors = @[sortByDate];
	}
	else {
		// the shoppinglist should show items that are not in the cart and not purchased
		// or deleted
		fetchRequest.predicate = [NSPredicate predicateWithFormat:
			@"(accountId = %@) AND (status <> %@) AND (inCart = NO) AND (purchased = NO) AND NOT(isRecommendation = 1 AND status = %@)",
			thisUser.accountId, kStatusDelete, kStatusPut];

		// sortMode will determine the sortdescriptors and sections
		if (self.sortMode == ShoppingListSortByName) {
			sectionKey = @"nameFirstCharacter";
			NSSortDescriptor *sortByName = [NSSortDescriptor
				sortDescriptorWithKey:@"name" ascending:YES
				selector:@selector(caseInsensitiveCompare:)];
			fetchRequest.sortDescriptors = @[sortByName];
		}
		else if ((self.sortMode == ShoppingListSortByStoreCategory) && ([[Store currentStore] hasOrderedCategories])) {
			// sort by item category then name
			sectionKey = @"storeCategory.sortOrder";
			//sectionKey = @"categorySortOrder";
			NSSortDescriptor *sortByCategory = [NSSortDescriptor
				sortDescriptorWithKey:@"storeCategory.sortOrder" ascending:YES];
			//NSSortDescriptor *sortByCategory = [NSSortDescriptor
			//	sortDescriptorWithKey:@"categorySortOrder" ascending:YES];
			NSSortDescriptor *sortByName = [NSSortDescriptor
				sortDescriptorWithKey:@"name" ascending:YES
				selector:@selector(caseInsensitiveCompare:)];
			fetchRequest.sortDescriptors = @[sortByCategory, sortByName];
		}
		else { // CategoryMode is the default
			// sort by item category then name
			sectionKey = @"category.name";
			NSSortDescriptor *sortByCategory = [NSSortDescriptor
				sortDescriptorWithKey:@"category.name" ascending:YES];
			NSSortDescriptor *sortByName = [NSSortDescriptor
				sortDescriptorWithKey:@"name" ascending:YES
				selector:@selector(caseInsensitiveCompare:)];
			fetchRequest.sortDescriptors = @[sortByCategory, sortByName];
		}
	}

	// create and init the fetched results controller
	NSFetchedResultsController *aFetchedResultsController =
		[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
		managedObjectContext:self.managedObjectContext sectionNameKeyPath:sectionKey
		cacheName:nil];
	aFetchedResultsController.delegate = self;

	self.fetchedResultsController = aFetchedResultsController;

	NSError *error = nil;
	if (![_fetchedResultsController performFetch:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}


	return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
	//QuietLog(@"%s", __FUNCTION__);
	[self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
	didChangeSection:(id<NSFetchedResultsSectionInfo> )sectionInfo
	atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
	//QuietLog(@"%s", __FUNCTION__);
	switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                withRowAnimation:UITableViewRowAnimationLeft];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                withRowAnimation:UITableViewRowAnimationRight];
            break;
        case NSFetchedResultsChangeUpdate:
        case NSFetchedResultsChangeMove:
            break;
	}
}

- (void)controller:(NSFetchedResultsController *)controller
	didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath
	forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
	//QuietLog(@"%s", __FUNCTION__);
	UITableView *thisTableView = self.tableView;

	switch (type) {
	case NSFetchedResultsChangeInsert:
		[thisTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
			withRowAnimation:UITableViewRowAnimationLeft];
		//[thisTableView scrollToRowAtIndexPath:newIndexPath
		//	atScrollPosition:UITableViewScrollPositionNone animated:YES];
		break;

	case NSFetchedResultsChangeDelete:
		[thisTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
			withRowAnimation:UITableViewRowAnimationRight];
		break;

	case NSFetchedResultsChangeUpdate:
		// This change was put in place in hopes of solving an issue described here:
		// https://developer.apple.com/library/iOS/releasenotes/iPhone/NSFetchedResultsChangeMoveReportedAsNSFetchedResultsChangeUpdate/index.html
		// by implementing a fix described here: http://oleb.net/blog/2013/02/nsfetchedresultscontroller-documentation-bug/
		//[self configureCell:[thisTableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
		[thisTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

		//[thisTableView scrollToRowAtIndexPath:indexPath
		//	atScrollPosition:UITableViewScrollPositionNone animated:YES];
		break;

	case NSFetchedResultsChangeMove:
		[thisTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
			withRowAnimation:UITableViewRowAnimationLeft];
		[thisTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
			withRowAnimation:UITableViewRowAnimationRight];
		//[thisTableView scrollToRowAtIndexPath:newIndexPath
		//	atScrollPosition:UITableViewScrollPositionNone animated:YES];
		break;
	}
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	[self.tableView endUpdates];
}

#pragma mark - BCSearchViewDelegate
- (void)searchView:(id)searchView didFinish:(BOOL)finished withSelection:(id)selection
{
	if (selection) {
		ShoppingListItem *newItem = selection;

		NSString *infoText = nil;
		if ([newItem.status isEqualToString:kStatusPost]) {
			infoText = [NSString stringWithFormat:@"%@\nadded to your shopping list", newItem.name];
		}
		else {
			infoText = [NSString stringWithFormat:@"%@\nquantity now %@", newItem.name, newItem.quantity];
		}

		[BCProgressHUD notificationWithText:infoText onView:self.navigationController.view];
	}

	// TODO Investigate why this is calling saveNestedContexts
	[self.managedObjectContext MR_saveNestedContexts];

	if (finished) {
		[self reloadShoppingListCache];
		[self.navigationController popViewControllerAnimated:YES];
	}
}

#pragma mark - BCStoreSelectionDelegate
- (void)view:(id)viewController didSelectStore:(id)inStore
{
	// TODO Fetch the store categories from the cloud
	// If this is a user store, these will get stored in core data and the MOC will
	// have the updated values.  If this isn't a user store, the values will be returned.
	// If the cloud is unavailable, fall back to existing data for user stores,
	// or to alpha category mode for non-user stores

	//QuietLog(@"%s: %@", __FUNCTION__, [inStore description]);

	[self.navigationController popToViewController:self animated:YES];

	if ([inStore isKindOfClass:[UserStoreMO class]]) {
		[Store setCurrentStoreWithUserStore:inStore inMOC:self.managedObjectContext];
	}
	else {
		[Store setCurrentStoreWithDictionary:inStore inMOC:self.managedObjectContext];
	}

	Store *currentStore = [Store currentStore];
	[self updateStoreInfoWithStore:currentStore];

	if (![currentStore hasOrderedCategories]) {
		self.sortMode = ShoppingListSortByCategory;
	}
	else {
		self.sortMode = ShoppingListSortByStoreCategory;
	}

	[BCProgressHUD notificationWithText: [NSString stringWithFormat:@"Current store:\r%@", currentStore.name]
		onView:self.navigationController.view];

	[self reloadShoppingListCache];
}

#pragma mark - ShoppingListItemDetailViewDelegate
- (void)detailView:(ShoppingListItemDetailViewController *)shoppingListItemView	didUpdateItem:(ShoppingListItem *)item
{
	[self.navigationController popViewControllerAnimated:YES];

	[item markForSync];

	// Save the context.
	[self.managedObjectContext BL_save];
}

#pragma mark - ZBarReaderDelegate methods
- (void)imagePickerController:(UIImagePickerController *)reader didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];

	ZBarSymbol *symbol = nil;
	for (symbol in results) {
		break;
	}

	NSString *barcode = symbol.data;
	NSString *type = symbol.typeName;

	[self getProductByBarcode:barcode andType:type];

	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - local methods
- (void)reloadShoppingListCache
{
	// delete the cache
	_fetchedResultsController.delegate = nil;
	_fetchedResultsController = nil;

	// this will recreate the cache
	[self.tableView reloadData];
}

- (void)addOrIncrementItemNamed:(NSString *)name productId:(NSNumber *)productId
	categoryId:(NSNumber *)categoryId barcode:(NSString *)barcode
	showHUDAddedTo:(UIView *)viewForHUD moveToCart:(BOOL)moveToCart
{
	NSString *infoText = @"";

	ShoppingListItem *updateItem = [ShoppingListItem addOrIncrementItemNamed:name productId:productId
		category:[CategoryMO categoryById:categoryId withManagedObjectContext:self.managedObjectContext]
		barcode:barcode quantity:1 inMOC:self.managedObjectContext];

	if ([updateItem.quantity integerValue] > 1) {
		infoText = [NSString stringWithFormat:@"%@\nquantity now %@", name, updateItem.quantity];
	}
	else if (moveToCart) {
		infoText = [NSString stringWithFormat:@"%@\nadded to your cart", name];
	}
	else {
		infoText = [NSString stringWithFormat:@"%@\nadded to your shopping list", name];
	}

	if (viewForHUD) {
		[BCProgressHUD notificationWithText:infoText onView:viewForHUD];
	}

	if (moveToCart) {
		updateItem.inCart = [NSNumber numberWithBool:YES];
		updateItem.inCartTime = [NSDate date];
		[self updateBadgesAnimated:YES];
	}

	[self.managedObjectContext BL_save];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	ShoppingListItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];

	ShoppingListItemCell *itemCell = (ShoppingListItemCell *)cell;
	itemCell.nameLabel.text = item.name;

	// if this item has warnings or lifestyle counts show those indicators
	itemCell.warningImageView.hidden = YES;
	itemCell.lifestyleImageView.hidden = YES;
	if ([item.allergyCount integerValue] > 0 || [item.ingredientCount integerValue] > 0) {
		itemCell.warningImageView.hidden = NO;
	}
	if ([item.lifestyleCount integerValue] > 0) {
		itemCell.lifestyleImageView.hidden = NO;
	}

	// if this item is a recommendation, show the wand instead of a quantity
	// and show the snooze/hide as well as increment/decrement
	if (self.expandedIndexPath != nil
			&& [self.expandedIndexPath compare:indexPath] == NSOrderedSame) {
		if ([item.isRecommendation boolValue]) {
			itemCell.snoozeButton.hidden = NO;
			[itemCell.hideButton setImage:[UIImage imageNamed:@"lightbulb-red-slash"] forState:UIControlStateNormal];
		}
		else {
			itemCell.snoozeButton.hidden = YES;
			[itemCell.hideButton setImage:[UIImage imageNamed:@"btn-delete"] forState:UIControlStateNormal];
		}
	}

	if ([item.isRecommendation boolValue]) {
		itemCell.checkboxImageView.image = [UIImage imageNamed:@"lightbulb-white"];
	}
	else if ([item.inCart boolValue]) {
		itemCell.checkboxImageView.image = [UIImage imageNamed:@"checkbox-check-2012"];
	}
	else {
		itemCell.checkboxImageView.image = [UIImage imageNamed:@"checkbox-empty-2012"];
	}
	[itemCell.quantityButton setTitle:[item.quantity stringValue] forState:UIControlStateNormal];
}

- (void)updateBadgesAnimated:(BOOL)animate
{
	NSUInteger shopCount = [ShoppingListItem MR_countOfEntitiesWithPredicate:
		[NSPredicate predicateWithFormat:
			@"(accountId = %@) AND (status <> %@) AND (inCart = NO) AND (purchased = NO) AND NOT(isRecommendation = 1 AND status = %@)", 
			[[User currentUser] accountId], kStatusDelete, kStatusPut] inContext:self.managedObjectContext];
	[self.headerSegmentedControl setTitle:[NSString stringWithFormat:@"Shopping List - %lu", (unsigned long)shopCount] forSegmentAtIndex:0];

	NSUInteger cartCount = [ShoppingListItem MR_countOfEntitiesWithPredicate:
		[NSPredicate predicateWithFormat:@"(accountId = %@) AND (status <> %@) AND (inCart = YES) AND (purchased = NO)", 
			[[User currentUser] accountId], kStatusDelete] inContext:self.managedObjectContext];
	[self.headerSegmentedControl setTitle:[NSString stringWithFormat:@"Cart - %lu", (unsigned long)cartCount] forSegmentAtIndex:1];
	
	// show or hide the empty images
	if (self.showCart) {
		self.emptyImageView.hidden = (cartCount > 0);
	}
	else {
		self.emptyImageView.hidden = (shopCount > 0);
	}
	self.tableView.hidden = !self.emptyImageView.hidden;
	
	// color the checkout button
	if (cartCount) {
		[self.checkoutButton setTitleColor:[UIColor BC_mediumLightGreenColor] forState:UIControlStateNormal];
	}
	else {
		[self.checkoutButton setTitleColor:[UIColor BC_lightGrayColor] forState:UIControlStateNormal];
	}
}

- (void)getProductByBarcode:(NSString *)barcode andType:(NSString *)type
{
	// check for the item in the shoppinglist first
	id <BLProductItem> matchingItem = [ShoppingListItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	// then in pantry
	if (!matchingItem) {
		matchingItem = [PantryItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	}
	// then in purchase history
	if (!matchingItem) {
		matchingItem = [PurchaseHxItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	}

	// if the item is found in one of the three caches, exit here after sending to the delegate
	if (matchingItem) {
		[self addOrIncrementItemNamed:[matchingItem name]
			productId:[matchingItem productId] categoryId:[[matchingItem category] cloudId]
			barcode:barcode showHUDAddedTo:self.navigationController.view moveToCart:[[Store currentStore] isStoreSet]];
		return;
	}

	// if the item wasn't found in any local caches, do the search for the barcode
	NSURL *url = [BCUrlFactory productSearchURLForBarcode:barcode andType:type];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url];
	[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];
		if (error != nil) {
			NSInteger status = [error code];
			DDLogError(@"failed %ld", (long)status);
		}
		else {
			NSArray *results = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			if ([results count] > 0) {
				NSDictionary *product = [results objectAtIndex:0];
				NSNumber *productId = nil;
				NSString *productName = [product valueForKey:kName];
				if ((NSNull *)[product objectForKey:kId] == [NSNull null]) {
					productId = @0;
				}
				else {
					productId = [[product valueForKey:kId] numberValueDecimal];
				}
				NSNumber *categoryId = [[product valueForKey:kProductCategoryId] numberValueDecimal];

				[self addOrIncrementItemNamed:productName
					productId:productId categoryId:categoryId
					barcode:barcode showHUDAddedTo:self.navigationController.view moveToCart:[[Store currentStore] isStoreSet]];

				[self updateBadgesAnimated:YES];
			}
			else {
				ScannedItem *cachedItem = [ScannedItem cachedItemByBarcode:barcode inMOC:self.managedObjectContext];
				if (cachedItem) {
					[self addOrIncrementItemNamed:cachedItem.name productId:@0 categoryId:@1
						barcode:cachedItem.barcode showHUDAddedTo:self.navigationController.view moveToCart:[[Store currentStore] isStoreSet]];
				}
				else {
					UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Product not found."
						message:[NSString stringWithFormat:@"No matching product found with barcode %@, add this?", barcode]
						delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
					alert.tag = kTagAlertItemNotFound;
					alert.alertViewStyle = UIAlertViewStylePlainTextInput;

					// set the barcode into some storage associated with the alert view
					objc_setAssociatedObject(alert, &barcodeKey, barcode, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

					[alert show];
				}
			}
		}

	}];
}

- (void)toggleRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.expandedIndexPath != nil) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}
	else {
		[self expandRowAtIndexPath:indexPath];
	}
}

- (void)expandRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.expandedIndexPath != nil) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}

	self.expandedIndexPath = indexPath;

	[self.tableView beginUpdates];
	[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
		withRowAnimation:UITableViewRowAnimationNone];
	[self.tableView endUpdates];

	// force the scroll of this item so that you can see the editing area
	[self.tableView scrollToRowAtIndexPath:self.expandedIndexPath
		atScrollPosition:UITableViewScrollPositionNone animated:YES];
}

- (void)collapseRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSIndexPath *temp = indexPath;
	self.expandedIndexPath = nil;

	[self.tableView beginUpdates];
	[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:temp]
		withRowAnimation:UITableViewRowAnimationNone];
	[self.tableView endUpdates];
}

- (void)updateStoreInfoWithStore:(Store *)store
{
	self.footerImageView.image = [UIImage imageNamed:[store isStoreSet]  ? @"empty-footer-black" : @"shoppinglist-footer"];
	self.currentStoreLabel.text = store.name;
	self.currentStoreLabel.hidden = (store.name == nil);
	self.currentStoreAddressLabel.text = store.address;
	self.currentStoreAddressLabel.hidden = (store.address == nil);
}

#pragma mark - button responders
// this is the responder for the segmented control at the top: ShoppingList / Cart
- (IBAction)dataTypeChanged:(id)sender
{
	switch ([sender selectedSegmentIndex]) {
		default:
		case 0:
			[self shoppingListButtonTapped];
			break;
		case 1:
			[self cartButtonTapped];
			break;
	}

}

- (void)shoppingListButtonTapped
{
	self.showCart = NO;
	self.emptyImageView.image = [[UIImage imageNamed:@"shoppinglist-empty"] 
		resizableImageWithCapInsets:UIEdgeInsetsMake(200, 0, 100, 0)];

	if (self.expandedIndexPath != nil) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}

	// 'show' the search view
	self.searchViewTopConstraint.constant = 0;
	self.searchView.hidden = NO;

	[UIView animateWithDuration:0.3f animations:^{
		[self.view layoutIfNeeded];
	}];

	[self reloadShoppingListCache];

	[self updateBadgesAnimated:NO];
}

- (void)cartButtonTapped
{
	self.showCart = YES;
	self.emptyImageView.image = [[UIImage imageNamed:@"cart-empty"] 
		resizableImageWithCapInsets:UIEdgeInsetsMake(200, 0, 100, 0)];

	if (self.expandedIndexPath != nil) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}

	// 'hide' the search view under the nav bar
	self.searchViewTopConstraint.constant = -44;
	[self.tableView setNeedsLayout];
	[self.emptyImageView setNeedsLayout];

	[UIView animateWithDuration:0.3f animations:^{
		[self.view layoutIfNeeded];
	} 
	completion:^(BOOL finished){
		self.searchView.hidden = YES;
	}];

	[self reloadShoppingListCache];

	[self updateBadgesAnimated:NO];
}

- (IBAction)scanButtonTapped:(id)sender
{
	ZBarReaderViewController *reader = [ZBarReaderViewController new];
	reader.readerDelegate = self;
	[reader.scanner setSymbology:ZBAR_NONE config:ZBAR_CFG_ENABLE to:0];
	[reader.scanner setSymbology:ZBAR_EAN8 config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_EAN13 config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_UPCA config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_UPCE config:ZBAR_CFG_ENABLE to:1];
	[self presentViewController:reader animated:YES completion:nil];
}

- (IBAction)modeButtonTapped:(id)sender
{
	switch (self.sortMode) {
		case ShoppingListSortByCategory:
		case ShoppingListSortByStoreCategory:
			self.sortMode = ShoppingListSortByName;
			break;
		default:
		case ShoppingListSortByName:
			self.sortMode = ShoppingListSortByCategory;
			break;
	}

	// save the sort mode as a default
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setInteger:self.sortMode forKey:kShoppingListSortSetting];

	// If we're sorting by category and we're checked into a store that has ordered categories,
	// then switch the sort mode to sort by them.  Do this after preserving the settings,
	// as we don't want to preserve this type of setting, as it is really just a modifier of the
	// category mode
	if ((self.sortMode == ShoppingListSortByCategory) && [[Store currentStore] hasOrderedCategories]) {
		self.sortMode = ShoppingListSortByStoreCategory;
	}

	// rebuild the fetchedResultsController
	[self reloadShoppingListCache];
}

- (IBAction)checkoutButtonTapped:(id)sender
{
	if ([[Store currentStore] buyerCompassClient]) {
		RegisterAuthCodeViewController *registerCodeView =
			[[RegisterAuthCodeViewController alloc] initWithNibName:nil bundle:nil];
		//registerCodeView.delegate = self;
		registerCodeView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
		[self presentViewController:registerCodeView animated:YES completion:nil];

		// clear the cart badge
		[self.cartQuantityBadge removeFromSuperview];
		self.cartQuantityBadge = nil;
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Purchase Items"
			message:@"Continuing will mark all in-cart items as purchased "
			"and add them to your shopping history.  Continue?"
			delegate:self
			cancelButtonTitle:@"Cancel"
			otherButtonTitles:@"Yes", nil];
		alert.tag = kTagAlertCheckout;
		[alert show];
	}
}

- (IBAction)checkboxButtonTapped:(id)sender
{
	CGRect rect = [self.tableView convertRect:[sender frame] fromView:[sender superview]];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:CGPointMake(rect.origin.x, rect.origin.y)];

	if (self.expandedIndexPath != nil) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}
	else {
		ShoppingListItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
		ShoppingListItemCell *itemCell = (ShoppingListItemCell *)[self.tableView cellForRowAtIndexPath:indexPath];

		BOOL wasInCart = [item.inCart boolValue];
		if (wasInCart) {
			// animate the check disappearing and reappearing empty
			itemCell.checkboxImageView.alpha = 1;
			[UIView animateWithDuration:0.3 animations:^{
				itemCell.checkboxImageView.alpha = 0;
				itemCell.checkboxImageView.image = [UIImage imageNamed:@"checkbox-empty-2012"];
				itemCell.checkboxImageView.alpha = 1;
			}
			completion:^(BOOL finished) {
			   item.inCart = @NO;
			   item.inCartTime = nil;
			   [item.managedObjectContext BL_save];
			   [self updateBadgesAnimated:YES];
		   }];
		}
		else
		{
			// animate the check disappearing and reappearing checked
			itemCell.checkboxImageView.alpha = 1;
			[UIView animateWithDuration:0.3 animations:^{
				itemCell.checkboxImageView.alpha = 0;
				itemCell.checkboxImageView.image = [UIImage imageNamed:@"checkbox-check-2012"];
				itemCell.checkboxImageView.alpha = 1;
			}
			completion:^(BOOL finished) {
			   item.inCart = @YES;
			   item.inCartTime = [NSDate date];
			   [item.managedObjectContext BL_save];
			   [self updateBadgesAnimated:YES];
		   }];
		}
	}
}

- (IBAction)quantityButtonTapped:(id)sender
{
	CGRect rect = [self.tableView convertRect:[sender frame] fromView:[sender superview]];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:CGPointMake(rect.origin.x, rect.origin.y)];

	[self toggleRowAtIndexPath:indexPath];
}

- (IBAction)incrementButtonTapped:(id)sender
{
	if (self.expandedIndexPath != nil) {
		ShoppingListItem *updateItem = [self.fetchedResultsController
										objectAtIndexPath:self.expandedIndexPath];

		// if this item is a suggestion, transform it into a "real" item
		// and make it's quantity 1
		// otherwise increment it
		if ([updateItem.isRecommendation boolValue]) {
			updateItem.isRecommendation = [NSNumber numberWithBool:NO];
			updateItem.quantity = [NSNumber numberWithInteger:1];
		}
		else {
			updateItem.quantity = [NSNumber numberWithInteger:
				[updateItem.quantity integerValue] + 1];
		}

		[updateItem markForSync];

		[self configureCell:[self.tableView cellForRowAtIndexPath:self.expandedIndexPath]
			atIndexPath:self.expandedIndexPath];

		// Save the context.
		[updateItem.managedObjectContext BL_save];
	}
}

- (IBAction)decrementButtonTapped:(id)sender
{
	if (self.expandedIndexPath != nil) {
		ShoppingListItem *updateItem = [self.fetchedResultsController
										objectAtIndexPath:self.expandedIndexPath];

		// if this item is a suggestion, transform it into a "real" item
		// and make it's quantity 1
		// otherwise check to see if it is greater than 1
		// decrement it if it is
		if ([updateItem.isRecommendation boolValue]) {
			updateItem.isRecommendation = [NSNumber numberWithBool:NO];
			updateItem.quantity = [NSNumber numberWithInteger:1];
		}
		// if this has a remainder, just chop that off
		else if ([updateItem.quantity doubleValue] - [updateItem.quantity integerValue] > 0) {
			updateItem.quantity = [NSNumber numberWithInteger:
				[updateItem.quantity integerValue]];
		}
		// otherwise step down one as long as it's above 1
		else if ([updateItem.quantity integerValue] > 1) {
			updateItem.quantity = [NSNumber numberWithInteger:
				[updateItem.quantity integerValue] - 1];
		}
		else {
			return;
		}

		[updateItem markForSync];

		[self configureCell:[self.tableView cellForRowAtIndexPath:self.expandedIndexPath]
			atIndexPath:self.expandedIndexPath];

		// Save the context.
		[updateItem.managedObjectContext BL_save];
	}
}

- (IBAction)snoozeButtonTapped:(id)sender
{
	if (self.expandedIndexPath != nil) {
		ShoppingListItem *updateItem = [self.fetchedResultsController objectAtIndexPath:self.expandedIndexPath];

		// NOTE this is for suggestions only, and PUT is the appropriate status 
		// for suggestions that are to be snoozed
		updateItem.status = kStatusPut;

		[self collapseRowAtIndexPath:self.expandedIndexPath];

		// Save the context.
		[updateItem.managedObjectContext BL_save];

		[self updateBadgesAnimated:YES];
	}
}

- (IBAction)hideButtonTapped:(id)sender
{
	if (self.expandedIndexPath != nil) {
		ShoppingListItem *updateItem = [self.fetchedResultsController objectAtIndexPath:self.expandedIndexPath];

		// NOTE this is for normal items as well as suggestions, and DELETE is the appropriate status 
		// for suggestions that are to be hidden
		updateItem.status = kStatusDelete;

		[self collapseRowAtIndexPath:self.expandedIndexPath];

		// Save the context.
		[updateItem.managedObjectContext BL_save];

		[self updateBadgesAnimated:YES];
	}
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == kTagAlertCheckout) {
		// user has chosen to mark the items in the cart purchased
		if (buttonIndex == 1) {
			BCProgressHUD *hud = [BCProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
			hud.labelText = @"Processing";

			[BCObjectManager markItemsInCartPurchasedInMOC:self.managedObjectContext];

			[Store clearCurrentStore];
			[self updateStoreInfoWithStore:nil];

			// remove the badge from the cart
			[self.cartQuantityBadge removeFromSuperview];
			self.cartQuantityBadge = nil;

			if (self.sortMode == ShoppingListSortByStoreCategory) {
				// Revert to sorting by their saved sort setting
				NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				self.sortMode = [defaults integerForKey:kShoppingListSortSetting];

				// Reload the view to have this sort change take effect
				[self reloadShoppingListCache];

				[self updateBadgesAnimated:YES];
			}

			[BCProgressHUD hideHUD:YES];
		}
	}
	else if (alertView.tag == kTagAlertItemNotFound) {
		if (buttonIndex != alertView.cancelButtonIndex) {
			NSString *name = [[alertView textFieldAtIndex:0] text];

			// retrieve the barcode from the associated storage
			NSString *barcode = (NSString *)objc_getAssociatedObject(alertView, &barcodeKey);

			[self addOrIncrementItemNamed:name productId:@0 categoryId:@1
				barcode:barcode showHUDAddedTo:self.navigationController.view moveToCart:[[Store currentStore] isStoreSet]];

			[self updateBadgesAnimated:YES];
		}
	}
	else {
		NSAssert(NO, @"Unknown alert");
	}
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
	if (alertView.alertViewStyle == UIAlertViewStyleDefault) {
		return YES;
	}
	else {
		NSString *inputText = [[alertView textFieldAtIndex:0] text];

		if ([inputText length] > 0) {
			return YES;
		}
		else {
			return NO;
		}
	}
}

@end
