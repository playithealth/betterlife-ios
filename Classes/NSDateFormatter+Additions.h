//
//  NSDateFormatter+Additions.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 6/24/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Additions)
+ (NSString *)relativeStringFromDate:(NSDate *)date;
@end
