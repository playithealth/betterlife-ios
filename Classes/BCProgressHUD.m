//
//  BCProgressHUD.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 12/22/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "BCProgressHUD.h"

static const CGFloat kLabelFontSize = 16.0f;
static const CGFloat kDetailLabelFontSize = 12.0f;

@implementation BCProgressHUD

+ (BCProgressHUD *)currentHUD
{
	static dispatch_once_t once;
	static BCProgressHUD *sharedInstance = nil;

	dispatch_once(&once, ^{
		sharedInstance = [[BCProgressHUD alloc] init];
	});

	return sharedInstance;
}

// Convenience method, also standardizes the duration and gives a single place to alter it
+ (UIView *)notificationWithText:(NSString *)text onView:(UIView *)view
{
	return [BCProgressHUD BC_showHUDWithText:text onView:view animated:YES duration:1.3f];
}

+ (UIView *)BC_showHUDWithText:(NSString *)text onView:(UIView *)view animated:(BOOL)animated duration:(NSTimeInterval)duration
{
	// create a transparent view to fill the screen
	UIView *backgroundView = [[UIView alloc] init];
	[backgroundView setTranslatesAutoresizingMaskIntoConstraints:NO];
	NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(backgroundView);
	[view addSubview:backgroundView];
	[view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundView]|" options:0 metrics:nil views:viewsDictionary]];
	[view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundView]|" options:0 metrics:nil views:viewsDictionary]];

	// create a black view to hold the label
	UIView *frameView = [[UIView alloc] init];
	frameView.backgroundColor = [UIColor blackColor];
	[frameView setTranslatesAutoresizingMaskIntoConstraints:NO];
	frameView.alpha = 0.0;

	CALayer *layer = frameView.layer;
	[layer setMasksToBounds:YES];
	[layer setCornerRadius:8.0];

	[backgroundView addSubview:frameView];

	[backgroundView addConstraint:[NSLayoutConstraint 
			   constraintWithItem:frameView 
						attribute:NSLayoutAttributeCenterX
						relatedBy:NSLayoutRelationEqual
						   toItem:backgroundView
						attribute:NSLayoutAttributeCenterX
					   multiplier:1.0 
						 constant:0.0]];
	[backgroundView addConstraint:[NSLayoutConstraint 
			   constraintWithItem:frameView 
						attribute:NSLayoutAttributeBottom
						relatedBy:NSLayoutRelationEqual
						   toItem:backgroundView
						attribute:NSLayoutAttributeBottom
					   multiplier:1.0 
						 constant:-64.0]];

	// create the label
	UILabel *textLabel = [[UILabel alloc] init];
	textLabel.numberOfLines = 0;
	textLabel.lineBreakMode = NSLineBreakByWordWrapping;
	textLabel.preferredMaxLayoutWidth = 250;
	textLabel.textAlignment = NSTextAlignmentCenter;
	[textLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
	[textLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
	[textLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
	textLabel.text = text;
	textLabel.textColor = [UIColor whiteColor];
	textLabel.backgroundColor = [UIColor blackColor];
	textLabel.alpha = 0.0;

	[frameView addSubview:textLabel];

	viewsDictionary = NSDictionaryOfVariableBindings(textLabel);

	[frameView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textLabel]-|" options:0 metrics:nil views:viewsDictionary]];
	[frameView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[textLabel]-|" options:0 metrics:nil views:viewsDictionary]];
	
	if (animated) {
		[UIView animateWithDuration:0.3 animations:^{
			frameView.alpha = 1.0;
			textLabel.alpha = 1.0;
		}];
	}
	else {
		frameView.alpha = 1.0;
		textLabel.alpha = 1.0;
	}

	if (duration > 0) {
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), 
		^{
			if (animated) {
				[UIView animateWithDuration:0.3 animations:^{
					frameView.alpha = 0.0;
					textLabel.alpha = 0.0;
				}];
			}
			[backgroundView removeFromSuperview];
		});
	}

	return backgroundView;
}

+ (BCProgressHUD *)showHUDWithText:(NSString *)text onView:(UIView *)view 
	animated:(BOOL)animated duration:(float)duration
{
	BCProgressHUD *hud = [BCProgressHUD showHUDWithText:text onView:view animated:animated];

	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), 
			dispatch_get_main_queue(), ^{
		[hud hide:animated];
	});

	return hud;
}

+ (BCProgressHUD *)showHUDWithText:(NSString *)text onView:(UIView *)view 
	animated:(BOOL)animated 
{
	BCProgressHUD *hud = [BCProgressHUD currentHUD];
	[hud hide:NO];
	[hud setFrame:view.frame];
	
	CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(250, 250)];
	
	UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, size.height)];
	textLabel.textAlignment = NSTextAlignmentCenter;
	textLabel.lineBreakMode = NSLineBreakByWordWrapping;
	textLabel.numberOfLines = 0;
	textLabel.text = text;
	textLabel.textColor = [UIColor whiteColor];
	textLabel.backgroundColor = [UIColor clearColor];
	textLabel.font = [UIFont systemFontOfSize:17];
	hud.customView = textLabel;
	[hud setMode:MBProgressHUDModeCustomView];

	[view addSubview:hud];
	
	// since this mode of the HUD is intended to show a notification, no grace time is necessary
	[hud setGraceTime:0.0f];

	[hud show:animated];
	
	return hud;
}

+ (BCProgressHUD *)showHUDAddedTo:(UIView *)view graceTime:(float)inGraceTime animated:(BOOL)animated
{
	BCProgressHUD *hud = [BCProgressHUD currentHUD];
	[hud hide:NO];
	[hud setFrame:view.frame];
	[hud setMode:MBProgressHUDModeIndeterminate];
	
	[view addSubview:hud];

	// since this is showing an indeterminate "spinner", no need to show it unless 
	// it's going to be on the screen long enough for the user to notice
	if (inGraceTime) {
		[hud setGraceTime:inGraceTime];
		hud.taskInProgress = YES;
	}

	[hud show:animated];

	return hud;
}

+ (BCProgressHUD *)showHUDAddedTo:(UIView *)view animated:(BOOL)animated
{
	return [BCProgressHUD showHUDAddedTo:view graceTime:0.5f animated:animated];
}

+ (void)hideHUD:(BOOL)animated
{
	BCProgressHUD *hud = [BCProgressHUD currentHUD];
	[hud hide:animated];
}

- (void)resetData
{
	// Set default values for properties
	self.animationType = MBProgressHUDAnimationFade;
	self.mode = MBProgressHUDModeIndeterminate;
	self.labelText = nil;
	self.detailsLabelText = nil;
	self.opacity = 0.8f;
	self.labelFont = [UIFont boldSystemFontOfSize:kLabelFontSize];
	self.detailsLabelFont = [UIFont boldSystemFontOfSize:kDetailLabelFontSize];
	self.xOffset = 0.0f;
	self.yOffset = 0.0f;
	self.dimBackground = NO;
	self.margin = 20.0f;
	self.graceTime = 0.0f;
	self.minShowTime = 0.0f;
	self.removeFromSuperViewOnHide = NO;
	self.minSize = CGSizeZero;
	self.square = NO;
	self.customView = nil;

	self.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;

	// Transparent background
	self.opaque = NO;
	self.backgroundColor = [UIColor clearColor];

	// Make invisible for now
	self.alpha = 0.0f;

	self.taskInProgress = NO;
}

- (void)hide:(BOOL)animated
{
	[super hide:animated];
	[self resetData];
	[self removeFromSuperview];
}

@end
