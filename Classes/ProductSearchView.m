//
//  ProductSearchView.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/22/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "ProductSearchView.h"

#import "BCProgressHUD.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"
#import "GTMHTTPFetcherLogging.h"
#import "NSDateFormatter+Additions.h"
#import "NumberValue.h"
#import "OAuthConsumer.h"
#import "OAuthUtilities.h"
#import "PantryItem.h"
#import "PantryItemViewController.h"
#import "ProductDetailViewController.h"
#import "ProductSearchRecord.h"
#import "ProductSearchView.h"
#import "PurchaseHxItem.h"
#import "QuartzCore/QuartzCore.h"
#import "ScannedItem.h"
#import "ShoppingListItem.h"
#import "StarRatingControl.h"
#import "User.h"
#import "UserHealthChoice.h"

// Make sure these are in the proper order that they display in, left -> right
enum tab {
	searchTab, historyTab, pantryTab
};

// representation of what is currently displaying in the drill down search
typedef enum {
	noLayer, categoriesLayer, brandsLayer, productsLayer
} ProductSearchLayer;

@interface ProductSearchView ()
<NSFetchedResultsControllerDelegate, UIScrollViewDelegate,
	UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
	enum tab _activeTab;
	ProductSearchLayer _activeLayer;
}

@property (strong, nonatomic) NSArray *userHealthFilters;
@property (assign, nonatomic) enum tab activeTab;
@property (strong, nonatomic) NSMutableArray *allHxResults;
@property (strong, nonatomic) NSMutableArray *hxResults;
@property (strong, nonatomic) NSMutableArray *allPantry;
@property (strong, nonatomic) NSMutableArray *pantry;
@property (strong, nonatomic) NSArray *categoryResults;
@property (strong, nonatomic) NSArray *brandResults;
@property (strong, nonatomic) NSArray *productResults;
@property (strong, nonatomic) NSArray *tabCoordinates;
@property (strong, nonatomic) NSMutableDictionary *imageFetchersInProgress;
@property (assign, nonatomic) ProductSearchLayer activeLayer;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)refreshSearchForActiveTab:(NSString *)searchTerm forceSearch:(BOOL)force;
- (void)resetSearch;
- (void)loadPurchaseHx:(NSString *)searchTerm;
- (void)loadPantry:(NSString *)searchTerm;
- (void)loadHealthProfile;
- (void)sendSearchRequest:(NSString *)searchTerm;
- (void)sendSearchRequest:(NSString *)searchTerm categoryId:(NSNumber *)categoryId categoryCount:(NSNumber *)count;
- (void)sendSearchRequest:(NSString *)searchTerm categoryId:(NSNumber *)categoryId brandId:(NSNumber *)entityId;
- (void)startImageDownload:(ProductSearchRecord *)record forIndexPath:(NSIndexPath *)indexPath;
- (void)showScopeBar:(BOOL)show;
- (void)cancelAllImageFetchers;
@end

@implementation ProductSearchView
@synthesize activeLayer = _activeLayer;
@synthesize activeTab = _activeTab;
@synthesize activeTabImageView;
@synthesize brandResults;
@synthesize brandScopeButton;
@synthesize brandScopeLabel;
@synthesize categoryResults;
@synthesize categoryScopeButton;
@synthesize categoryScopeLabel;
@synthesize customCell;
@synthesize delegate;
@synthesize allHxResults;
@synthesize hxResults;
@synthesize imageFetchersInProgress;
@synthesize inputCategory;
@synthesize inputText;
@synthesize singleSelect;
@synthesize allPantry;
@synthesize pantry;
@synthesize productResults;
@synthesize scopeBarView;
@synthesize searchScopeButton;
@synthesize searchTextField;
@synthesize tabCoordinates;
@synthesize tableView;
@synthesize userHealthFilters;

#pragma mark - memory management
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
		self.imageFetchersInProgress = [NSMutableDictionary dictionary];
		self.singleSelect = NO;
	}
	return self;
}


- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];

	// terminate all pending download connections
	[self cancelAllImageFetchers];
}

#pragma mark - mark View lifecycle
- (void)viewDidLoad
{
	self.searchTextField.delegate = self;
	self.title = @"Add Item";

	[self.view addSubview:self.scopeBarView];

	[self showScopeBar:NO];
	[self setActiveLayer:noLayer];

	// Make sure these are properly ordered with respect to enum tab
	self.tabCoordinates = [NSArray arrayWithObjects:
		[NSValue valueWithCGRect:CGRectMake(-6.0f, 47.0f, 117.0f, 34.0f)],
		[NSValue valueWithCGRect:CGRectMake(101.0f, 47.0f, 117.0f, 34.0f)],
		[NSValue valueWithCGRect:CGRectMake(209.0f, 47.0f, 117.0f, 34.0f)],
		nil];

	// See if inputText was specified as a starting search term
	if (self.inputText) {
		self.searchTextField.text = self.inputText;
		// If we also got a category, then call the proper search for that
		if (self.inputCategory) {
			self.categoryScopeLabel.text = [self.inputCategory name];
			[self sendSearchRequest:self.inputText categoryId:[self.inputCategory cloudId]
				categoryCount:nil];
		}
		else {
			[self sendSearchRequest:self.inputText];
		}
	}

	[self loadPurchaseHx:@""];

	[self setActiveTab:searchTab];

	if ([self.searchTextField.text length] == 0) {
		[self.searchTextField becomeFirstResponder];
	}

	[super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
	// Load up the user's health profile info (or refresh it in case it got changed)
	[self loadHealthProfile];

	[super viewDidAppear:animated];
}

- (void)viewDidUnload
{
	self.tableView = nil;
	self.searchTextField = nil;
	self.activeTabImageView = nil;
	[self setScopeBarView:nil];

	[self setBrandScopeButton:nil];
	[self setCategoryScopeButton:nil];
	[self setCategoryScopeLabel:nil];
	[self setBrandScopeLabel:nil];
	[self setSearchScopeButton:nil];

	[super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// terminate all pending download connections
	[self cancelAllImageFetchers];

	[super viewWillDisappear:animated];
}

#pragma mark - local methods
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	switch ([self activeTab]) {
		case searchTab:
			{
				if (self.activeLayer == categoriesLayer) {
					NSDictionary *category = [self.categoryResults objectAtIndex:[indexPath row]];
					cell.textLabel.text = [category valueForKey:@"category"];
					cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ items",
						[category valueForKey:@"category_count"]];
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				}
				else if (self.activeLayer == brandsLayer) {
					NSDictionary *brand = [self.brandResults objectAtIndex:[indexPath row]];
					cell.textLabel.text = [brand valueForKey:@"entity_name"];
					cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ items",
						[brand valueForKey:@"brand_count"]];
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				}
				else {                     
					// productLayer
					ProductSearchRecord *record = [self.productResults objectAtIndex:[indexPath row]];
					ProductSearchResultsCell *psrCell = (ProductSearchResultsCell *)cell;
					psrCell.nameLabel.text = record.name;
					psrCell.sizeLabel.text = record.size;

					if ([record.allergyCount intValue] > 0) {
						psrCell.alertImageView.image = [UIImage imageNamed:@"alert"];
					}
					else {
						psrCell.alertImageView.image = nil;
					}

					if ([record.lifestyleCount intValue] > 0) {
						psrCell.lifestyleImageView.image = [UIImage imageNamed:@"leaf"];
					}
					else {
						psrCell.lifestyleImageView.image = nil;
					}

					if (record.rating != nil) {
						psrCell.ratingControl.rating = [record.rating integerValue];
						psrCell.ratingControl.hidden = NO;
					}
					else {
						psrCell.ratingControl.rating = -1;
						psrCell.ratingControl.hidden = YES;
					}

					if (!record.image) {
						if (self.tableView.dragging == NO
								&& self.tableView.decelerating == NO) {
							[self startImageDownload:record forIndexPath:indexPath];
						}
						// if a download is deferred or in progress, return a placeholder image
						psrCell.productImageView.image = [UIImage imageNamed:@"placeholder.png"];
					}
					else {
						psrCell.productImageView.image = record.image;
					}
				}
			}
			break;

		case historyTab:
			{
				PurchaseHxItem *purchase = [self.hxResults objectAtIndex:[indexPath row]];
				cell.textLabel.text = purchase.name;
				cell.detailTextLabel.text = [NSString stringWithFormat:@"Purchased %@", [NSDateFormatter relativeStringFromDate:[NSDate dateWithTimeIntervalSince1970:purchase.updatedOn doubleValue]]];
			}
			break;

		case pantryTab:
			{
				PantryItem *pantryItem = [self.pantry objectAtIndex:[indexPath row]];
				if (pantryItem) {
					cell.textLabel.text = pantryItem.name;
					cell.detailTextLabel.text = [NSString stringWithFormat:@"Currently have %@", pantryItem.quantity];
				}
			}
			break;

		default:
			// Should never get here unless someone goes rogue and adds a new tab
			break;
	}
}

- (void)refreshSearchForActiveTab:(NSString *)searchTerm forceSearch:(BOOL)force
{
	[self.searchTextField resignFirstResponder];

	switch ([self activeTab]) {
		case searchTab:
			{
				if (force) {
					[self resetSearch];
					if ([searchTerm length] <= 0) {
						[self.tableView reloadData];
						return;
					}
					[self sendSearchRequest:searchTerm];
				}
			}
			break;

		case historyTab:
			[self loadPurchaseHx:searchTerm];
			break;

		case pantryTab:
			[self loadPantry:searchTerm];
			break;

		default:
			// Should never get here unless someone goes rogue and adds a new tab
			break;
	}

	[self.tableView reloadData];
}

- (void)startImageDownload:(ProductSearchRecord *)record forIndexPath:(NSIndexPath *)indexPath
{
	GTMHTTPFetcher *imageFetcher = [self.imageFetchersInProgress objectForKey:indexPath];
	if (imageFetcher == nil) {
		// check to see if we are on an iPhone 4, use the medium image
		NSString *imageSize = @"small";
		if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]
				&& [[UIScreen mainScreen] scale] == 2) {
			imageSize = @"medium";
		}

		imageFetcher = [GTMHTTPFetcher signedFetcherWithURL:
			[BCUrlFactory imageURLForImage:record.imageId imageSize:imageSize]];
		[imageFetcher setProperty:record forKey:@"record"];
		[imageFetcher setProperty:indexPath forKey:@"indexPath"];
		[self.imageFetchersInProgress setObject:imageFetcher forKey:indexPath];

		[imageFetcher beginFetchWithDelegate:self
			didFinishSelector:@selector(imageFetcher:finishedWithData:error:)];
	}
}

- (void)loadImagesForOnscreenRows
{
	// check to make sure the search tab is active
	if (([self activeTab] == searchTab) && (self.activeLayer == productsLayer)
			&& ([self.productResults count] > 0)) {
		NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
		for (NSIndexPath *indexPath in visiblePaths) {
			ProductSearchRecord *record = [self.productResults objectAtIndex:[indexPath row]];

			if (!record.image) {             // avoid the image download if the row already has an image
				[self startImageDownload:record forIndexPath:indexPath];
			}
		}
	}
}

- (void)addItemsNotInShoppingList:(NSArray *)fromArray toArray:(NSMutableArray *)toArray
{
	// copy the items into the toArray that aren't in the filter array
	for (id item in fromArray) {
		NSArray *itemsOnShoppingList = [ShoppingListItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:
			@"productId = %@ and name = %@ and accountId = %@ and status <> %@",
			[item productId], [item name], [item accountId], kStatusDelete]];

		if (![itemsOnShoppingList count]) {
			[toArray addObject:item];
		}
	}
}

- (void)loadPurchaseHx:(NSString *)searchTerm
{
	if (!self.allHxResults) {
		NSEntityDescription *entity =
			[NSEntityDescription entityForName:@"PurchaseHxItem"
			inManagedObjectContext:self.managedObjectContext];

		// filter for the user and searchTerm
		User *userData = [User currentUser];
		NSMutableString *predicateString = [[NSMutableString alloc] initWithFormat:
			@"(accountId = %@)", userData.accountId];
		NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:predicateString];

		NSSortDescriptor *sortByItem = [NSSortDescriptor sortDescriptorWithKey:kName
			ascending:YES selector:@selector(caseInsensitiveCompare:)];
		NSArray *sortDescriptors = [NSArray arrayWithObject:sortByItem];

		NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
		fetchRequest.entity = entity;
		fetchRequest.predicate = searchPredicate;
		fetchRequest.sortDescriptors = sortDescriptors;

		NSArray *array = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];

		self.allHxResults = [[NSMutableArray alloc] init];

		[self addItemsNotInShoppingList:array toArray:self.allHxResults];
	}

	if ([searchTerm length] > 0) {
		if ([searchTerm length] >= 3) {
			self.hxResults = [[self.allHxResults filteredArrayUsingPredicate:
				[NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@", kName, searchTerm]]
				mutableCopy];
		}
		else {
			self.hxResults = [[self.allHxResults filteredArrayUsingPredicate:
				[NSPredicate predicateWithFormat:@"%K BEGINSWITH[c] %@", kName, searchTerm]]
				mutableCopy];
		}
	}
	else {
		self.hxResults = self.allHxResults;
	}
}

- (void)loadPantry:(NSString *)searchTerm
{
	if (!self.allPantry) {
		NSEntityDescription *entity =
			[NSEntityDescription entityForName:@"PantryItem"
			inManagedObjectContext:self.managedObjectContext];

		// filter for the user and searchTerm
		User *userData = [User currentUser];
		NSMutableString *predicateString = [[NSMutableString alloc] initWithFormat:
			@"(accountId = %@) AND (status <> '%@')", userData.accountId, kStatusDelete];
		NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:predicateString];

		NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:kName
			ascending:YES selector:@selector(caseInsensitiveCompare:)];
		NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];

		NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
		fetchRequest.entity = entity;
		fetchRequest.predicate = searchPredicate;
		fetchRequest.sortDescriptors = sortDescriptors;

		NSArray *array = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];

		self.allPantry = [[NSMutableArray alloc] init];

		[self addItemsNotInShoppingList:array toArray:self.allPantry];
	}

	if ([searchTerm length] > 0) {
		if ([searchTerm length] >= 3) {
			self.pantry = [[self.allPantry filteredArrayUsingPredicate:
				[NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@", kName, searchTerm]]
				mutableCopy];
		}
		else {
			self.pantry = [[self.allPantry filteredArrayUsingPredicate:
				[NSPredicate predicateWithFormat:@"%K BEGINSWITH[c] %@", kName, searchTerm]]
				mutableCopy];
		}
	}
	else {
		self.pantry = self.allPantry;
	}
}

- (void)resetSearch
{
	// TODO if the search is extended to the other tabs, their arrays should be reset here too
	self.categoryResults = nil;
	self.brandResults = nil;
	self.productResults = nil;
	[self setActiveLayer:noLayer];
}

- (IBAction)addFreeTextItem:(id)sender
{
	if ([self.searchTextField.text length] == 0) {
		return;
	}

	[self.searchTextField resignFirstResponder];

	NSDictionary *newItem = [NSDictionary dictionaryWithObjectsAndKeys:
		self.searchTextField.text, kItemName,
		[NSNumber numberWithInteger:0], kProductId,
		[NSNumber numberWithInteger:0], kProductCategoryId,
		nil];

	if (self.delegate != nil
			&& [self.delegate conformsToProtocol:@protocol(ProductSearchViewDelegate)]) {
		[self.delegate productSearchView:self didSelectProduct:newItem];
	}

	if (self.singleSelect) {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (IBAction)searchTabSelected:(id)sender
{
	[self setActiveTab:searchTab];
}

- (IBAction)historyTabSelected:(id)sender
{
	[self setActiveTab:historyTab];
}

- (IBAction)pantryTabSelected:(id)sender
{
	[self setActiveTab:pantryTab];
}

- (IBAction)searchScopeTapped:(id)sender
{
	self.brandResults = nil;
	self.productResults = nil;
	[self setActiveLayer:categoriesLayer];
}

- (IBAction)categoryScopeTapped:(id)sender
{
	self.productResults = nil;
	[self setActiveLayer:brandsLayer];
}

- (IBAction)brandScopeTapped:(id)sender
{
}

- (IBAction)searchButtonTapped:(id)sender
{
	NSString *searchTerm = self.searchTextField.text;
	[self.searchTextField resignFirstResponder];
	[self refreshSearchForActiveTab:searchTerm forceSearch:YES];
}

- (IBAction)freeTextButtonTapped:(id)sender
{
	[self addFreeTextItem:sender];
}

- (void)setActiveTab:(enum tab)tab
{
	if (_activeTab != tab) {
		_activeTab = tab;
		// change the view contents
		[UIView animateWithDuration:0.2f animations:^{
				[self.activeTabImageView setFrame:[[self.tabCoordinates objectAtIndex:tab] CGRectValue]];
			}];

		// refresh the contents of the tab
		[self refreshSearchForActiveTab:self.searchTextField.text forceSearch:NO];

		// if there is no search term, put the cursor in the text field
		if ([self.searchTextField.text length] == 0) {
			[self.searchTextField becomeFirstResponder];
		}

		[self.tableView reloadData];

		[self showScopeBar:(_activeTab == searchTab) && ([categoryResults count])];
	}
}

- (void)showScopeBar:(BOOL)show
{
	[UIView beginAnimations:@"animatedScopeBar" context:nil];
	[UIView setAnimationDuration:0.2];
	float scopeY = 436.0f;
	float tableHeight = 335.0f;
	if (show) {
		scopeY = 381.0f;
		tableHeight = 300.0f;
	}
	[self.tableView setFrame:CGRectMake(0.0f, 81.0f, 320.0f, tableHeight)];
	[self.scopeBarView setFrame:CGRectMake(0.0f, scopeY, 320.0f, 35.0f)];
	[UIView commitAnimations];
}

- (void)setActiveLayer:(ProductSearchLayer)newLayer
{
	if (_activeLayer != newLayer) {
		switch (_activeLayer) {
			case categoriesLayer:
				[self.searchScopeButton
					setImage:[UIImage imageNamed:@"search-bar-icon-inactive.png"]
					forState:UIControlStateNormal];
				[self.searchScopeButton
					setImage:[UIImage imageNamed:@"search-bar-icon-inactive.png"]
					forState:UIControlStateHighlighted];

			case brandsLayer:
				[self.categoryScopeButton
					setImage:[UIImage imageNamed:@"search-bar-button-inactive.png"]
					forState:UIControlStateNormal];
				[self.categoryScopeButton
					setImage:[UIImage imageNamed:@"search-bar-button-inactive.png"]
					forState:UIControlStateHighlighted];
				break;

			case productsLayer:
				[self.brandScopeButton
					setImage:[UIImage imageNamed:@"search-bar-end-inactive.png"]
					forState:UIControlStateNormal];
				[self.brandScopeButton
					setImage:[UIImage imageNamed:@"search-bar-end-inactive.png"]
					forState:UIControlStateHighlighted];

				[self cancelAllImageFetchers];

				break;

			default:
				break;
		}
		_activeLayer = newLayer;
		switch (newLayer) {
			case categoriesLayer:
				[self.searchScopeButton
					setImage:[UIImage imageNamed:@"search-bar-icon-active.png"]
					forState:UIControlStateNormal];
				[self.searchScopeButton
					setImage:[UIImage imageNamed:@"search-bar-icon-active.png"]
					forState:UIControlStateHighlighted];
				if (!self.categoryResults) {
					[self sendSearchRequest:self.searchTextField.text];
				}
				break;

			case brandsLayer:
				[self.categoryScopeButton
					setImage:[UIImage imageNamed:@"search-bar-button-active.png"]
					forState:UIControlStateNormal];
				[self.categoryScopeButton
					setImage:[UIImage imageNamed:@"search-bar-button-active.png"]
					forState:UIControlStateHighlighted];
				break;

			case productsLayer:
				// if we clicked thru a brand, we need to highlight the brand button
				if ([self.brandResults count]) {
					[self.brandScopeButton
						setImage:[UIImage imageNamed:@"search-bar-end-active.png"]
						forState:UIControlStateNormal];
					[self.brandScopeButton
						setImage:[UIImage imageNamed:@"search-bar-end-active.png"]
						forState:UIControlStateHighlighted];
				}
				else {                 // otherwise, highlight the category button
					[self.categoryScopeButton
						setImage:[UIImage imageNamed:@"search-bar-button-active.png"]
						forState:UIControlStateNormal];
					[self.categoryScopeButton
						setImage:[UIImage imageNamed:@"search-bar-button-active.png"]
						forState:UIControlStateHighlighted];
				}
				break;

			default:
				break;
		}

		// This looks complex, but it shows the category button if there
		// are brands, or products. BUT the brand button only shows if there
		// are both brands and products
		self.categoryScopeButton.hidden = !([self.brandResults count]
				|| [self.productResults count]);
		self.categoryScopeLabel.hidden = !([self.brandResults count]
				|| [self.productResults count]);
		self.brandScopeButton.hidden = !([self.brandResults count]
				&& [self.productResults count]);
		self.brandScopeLabel.hidden = !([self.brandResults count]
				&& [self.productResults count]);
	}

	[self.tableView reloadData];

	// scroll the list to the top
	if ([self.tableView numberOfSections]
			&& [self.tableView numberOfRowsInSection:0]) {
		// TODO this would be better if it actually saved the indexpath to each section, but
		// that seemed like a lot of work to fix a bug.
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
			atScrollPosition:UITableViewScrollPositionTop animated:NO];
	}
}

- (void)cancelAllImageFetchers
{
	NSArray *allDownloads = [self.imageFetchersInProgress allValues];
	[allDownloads makeObjectsPerformSelector:@selector(stopFetching)];
	self.imageFetchersInProgress = nil;
}

- (void)loadHealthProfile
{
	NSSet *results = [self.managedObjectContext fetchObjectsForEntityName:[UserHealthChoice entityName]
		withPredicate:@"loginId = %@ and value = %@", 
		[[User currentUser] loginId], [NSNumber numberWithInteger:1]];

	NSMutableArray *filters = [[NSMutableArray alloc] init];
	for (UserHealthChoice *choice in results) {
		[filters addObject:choice.choice.filter];
	}
	self.userHealthFilters = filters;
}

- (void)accessoryTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	id detailItem = nil;
	switch ([self activeTab]) {
		case searchTab:
			{
				// shouldn't be any way to get here unless it's the product layer
				if (self.activeLayer == productsLayer) {
					ProductDetailViewController *detailView = 
                        [[ProductDetailViewController alloc] initWithNibName:nil bundle:nil];
					detailView.productDetailItem = [self.productResults objectAtIndex:[indexPath row]];
					detailView.managedObjectContext = self.managedObjectContext;
					[self.navigationController pushViewController:detailView animated:YES];
				}
			}
			break;
		case historyTab:
			{
                ProductDetailViewController *detailView = 
                    [[ProductDetailViewController alloc] initWithNibName:nil bundle:nil];
				detailView.productDetailItem = [self.hxResults objectAtIndex:[indexPath row]];
				detailView.managedObjectContext = self.managedObjectContext;
				[self.navigationController pushViewController:detailView animated:YES];
			}
			break;
		case pantryTab:
			{
				ProductDetailViewController *detailView = 
                    [[ProductDetailViewController alloc] initWithNibName:nil bundle:nil];
				detailView.productDetailItem = [self.pantry objectAtIndex:[indexPath row]];
				detailView.managedObjectContext = self.managedObjectContext;
				[self.navigationController pushViewController:detailView animated:YES];
			}
			break;

		default:
			// Should never get here unless someone goes rogue and adds a new tab
			break;
	}

	if (detailItem) {
	}
}

- (void)cellTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	id selectedItem = nil;

	switch ([self activeTab]) {
		case searchTab:
			{
				NSString *searchTerm = self.searchTextField.text;
				if (self.activeLayer == categoriesLayer) {
					NSDictionary *category = [self.categoryResults objectAtIndex:[indexPath row]];
					NSNumber *count = [category valueForKey:@"category_count"];
					self.categoryScopeLabel.text = [category valueForKey:@"category"];
					[self sendSearchRequest:searchTerm
						categoryId:[category valueForKey:kProductCategoryId]
						categoryCount:count];
				}
				else if (self.activeLayer == brandsLayer) {
					NSDictionary *brand = [self.brandResults objectAtIndex:[indexPath row]];
					self.brandScopeLabel.text = [brand valueForKey:@"entity_name"];
					[self sendSearchRequest:searchTerm
						categoryId:[brand valueForKey:kProductCategoryId]
						brandId:[brand valueForKey:kEntityId]];
				}
				else if (self.activeLayer == productsLayer) {
					selectedItem = [self.productResults objectAtIndex:[indexPath row]];
					// it was determined that clearing the search results
					// was disruptive and unintuitive... IOS-170
					//[self resetSearch];
					//[self showScopeBar:NO];
				}
			}
			break;

		case historyTab:
			{
				selectedItem = [self.hxResults objectAtIndex:[indexPath row]];

				[self.hxResults removeObjectAtIndex:[indexPath row]];
				[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
					withRowAnimation:UITableViewRowAnimationLeft];
			}
			break;

		case pantryTab:
			{
				selectedItem = [self.pantry objectAtIndex:[indexPath row]];

				[self.pantry removeObjectAtIndex:[indexPath row]];
				[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
					withRowAnimation:UITableViewRowAnimationLeft];
			}
			break;

		default:
			// Should never get here unless someone goes rogue and adds a new tab
			break;
	}

	if (selectedItem) {
		NSMutableDictionary *newItem = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
			[selectedItem name], kItemName,
			[selectedItem productId], kProductId,
			[selectedItem barcode], kBarcode,
			[selectedItem imageId], kImageId,
			[selectedItem calories], kCalories,
			[selectedItem protein], kProtein,
			[selectedItem carbohydrates], kCarbohydrates,
			[selectedItem fat], kFat,
			[selectedItem saturatedFat], kSaturatedFat,
			[selectedItem sodium], kSodium,
			[selectedItem cholesterol], kCholesterol,
			[selectedItem caloriesFromFat], kPercentageFat,
			nil];

		if ([selectedItem isKindOfClass:[ProductSearchRecord class]]) {
			[newItem setValue:[selectedItem productCategoryId] forKey:kProductCategoryId];
		}
		else {
			[newItem setValue:[selectedItem category].cloudId forKey:kProductCategoryId];
		}

		if (self.delegate != nil
				&& [self.delegate conformsToProtocol:@protocol(ProductSearchViewDelegate)]) {
			[self.delegate productSearchView:self didSelectProduct:newItem];
		}

		if (self.singleSelect) {
			[self.navigationController popViewControllerAnimated:YES];
		}
	}
}

#pragma mark - web request responders
- (void)categorySearchFetcher:(GTMHTTPFetcher *)inFetcher
   finishedWithData:(NSData *)retrievedData
   error:(NSError *)error
{
	if (error != nil) {
		[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
			withFetcher:inFetcher response:nil];
	}
	else {
		// fetch succeeded
		NSString *response = [[NSString alloc] initWithData:retrievedData
			encoding:NSUTF8StringEncoding];

		self.categoryResults = [response JSONValue];
	}

	// Hide the HUD
	[BCProgressHUD hideHUD:NO];

	[self showScopeBar:YES];
	[self setActiveLayer:categoriesLayer];
}

- (void)brandSearchFetcher:(GTMHTTPFetcher *)inFetcher
   finishedWithData:(NSData *)retrievedData
   error:(NSError *)error
{
	if (error != nil) {
		[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
			withFetcher:inFetcher response:nil];
	}
	else {
		// fetch succeeded
		NSString *response = [[NSString alloc] initWithData:retrievedData
			encoding:NSUTF8StringEncoding];

		self.brandResults = [response JSONValue];
	}

	// Hide the HUD
	[BCProgressHUD hideHUD:NO];

	[self showScopeBar:YES];
	[self setActiveLayer:brandsLayer];
}

- (void)productSearchFetcher:(GTMHTTPFetcher *)inFetcher
   finishedWithData:(NSData *)retrievedData
   error:(NSError *)error
{
	if (error != nil) {
		[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
			withFetcher:inFetcher response:nil];
	}
	else {
		// fetch succeeded
		NSString *response = [[NSString alloc] initWithData:retrievedData
			encoding:NSUTF8StringEncoding];

		NSArray *responseArray = [response JSONValue];

		NSMutableArray *tempArray = [[NSMutableArray alloc] init];
		for (NSDictionary *itemData in responseArray) {
			[tempArray addObject:[ProductSearchRecord recordWithSearchDictionary:itemData
				withManagedObjectContext:self.managedObjectContext]];
		}
		self.productResults = tempArray;
	}

	// Hide the HUD
	[BCProgressHUD hideHUD:NO];

	[self showScopeBar:YES];
	[self setActiveLayer:productsLayer];
}

- (void)imageFetcher:(GTMHTTPFetcher *)inFetcher
   finishedWithData:(NSData *)retrievedData
   error:(NSError *)error
{
	NSIndexPath *indexPath = [inFetcher propertyForKey:@"indexPath"];

	if (error != nil) {
		[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
			withFetcher:inFetcher response:nil];
	}
	else {
		// fetch succeeded
		ProductSearchRecord *record =  [inFetcher propertyForKey:@"record"];

		UIImage *productImage = [[UIImage alloc] initWithData:retrievedData];
		if (productImage.size.width != 60 && productImage.size.height != 60) {
			CGSize itemSize = CGSizeMake(60, 60);
			UIGraphicsBeginImageContext(itemSize);
			CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
			[productImage drawInRect:imageRect];
			record.image = UIGraphicsGetImageFromCurrentImageContext();
			UIGraphicsEndImageContext();
		}
		else {
			record.image = productImage;
		}

		// Display the newly loaded image
		ProductSearchResultsCell *psrCell =
			(ProductSearchResultsCell *)[self.tableView cellForRowAtIndexPath:indexPath];
		psrCell.productImageView.image = record.image;
	}

	// remove the image fetcher from the dictionary
	[self.imageFetchersInProgress removeObjectForKey:indexPath];
}

#pragma mark - web requests
- (void)sendSearchRequest:(NSString *)searchTerm
{
//	BCProgressHUD *hud = [BCProgressHUD
//		showHUDAddedTo:self.navigationController.view animated:YES];
//	hud.labelText = @"Searching";
//
//	NSURL *productSearchURL = [BCUrlFactory productCategoriesForSearchTerm:searchTerm];
//	//QuietLog(@"search url %@", productSearchURL);
//
//	GTMHTTPFetcher *categorySearchFetcher =
//		[GTMHTTPFetcher signedFetcherWithURL:productSearchURL];
//	[categorySearchFetcher setProperty:searchTerm forKey:@"searchTerm"];
//	[categorySearchFetcher beginFetchWithDelegate:self
//		didFinishSelector:@selector(categorySearchFetcher:finishedWithData:error:)];
}

- (void)sendSearchRequest:(NSString *)searchTerm
   categoryId:(NSNumber *)categoryId
   categoryCount:(NSNumber *)count
{
//	BCProgressHUD *hud = [BCProgressHUD
//		showHUDAddedTo:self.navigationController.view animated:YES];
//	hud.labelText = @"Searching";
//
//	NSURL *productSearchURL = nil;
//	if (!count || ([count integerValue] > 25)) {
//		productSearchURL = [BCUrlFactory productBrandsForSearchTerm:searchTerm
//			inCategory:categoryId];
//
//		GTMHTTPFetcher *brandSearchFetcher =
//			[GTMHTTPFetcher signedFetcherWithURL:productSearchURL];
//		[brandSearchFetcher setProperty:searchTerm forKey:@"searchTerm"];
//		[brandSearchFetcher beginFetchWithDelegate:self
//			didFinishSelector:@selector(brandSearchFetcher:finishedWithData:error:)];
//	}
//	else {
//		productSearchURL = [BCUrlFactory productSearchForSearchTerm:searchTerm
//			inCategory:categoryId filters:self.userHealthFilters];
//
//		GTMHTTPFetcher *productSearchFetcher =
//			[GTMHTTPFetcher signedFetcherWithURL:productSearchURL];
//		[productSearchFetcher setProperty:searchTerm forKey:@"searchTerm"];
//		[productSearchFetcher beginFetchWithDelegate:self
//			didFinishSelector:@selector(productSearchFetcher:finishedWithData:error:)];
//	}
}

- (void)sendSearchRequest:(NSString *)searchTerm
   categoryId:(NSNumber *)categoryId
   brandId:(NSNumber *)entityId
{
//	BCProgressHUD *hud = [BCProgressHUD
//		showHUDAddedTo:self.navigationController.view animated:YES];
//	hud.labelText = @"Searching";
//
//	NSURL *productSearchURL = [BCUrlFactory productSearchForSearchTerm:searchTerm
//		inCategory:categoryId inBrand:entityId filters:self.userHealthFilters];
//	//QuietLog(@"search url %@", productSearchURL);
//
//	GTMHTTPFetcher *productSearchFetcher =
//		[GTMHTTPFetcher signedFetcherWithURL:productSearchURL];
//	[productSearchFetcher setProperty:searchTerm forKey:@"searchTerm"];
//	[productSearchFetcher beginFetchWithDelegate:self
//		didFinishSelector:@selector(productSearchFetcher:finishedWithData:error:)];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger count = 0;
	switch ([self activeTab]) {
		case searchTab:
			{
				if (self.activeLayer == categoriesLayer) {
					count = [self.categoryResults count];
				}
				else if (self.activeLayer == brandsLayer) {
					count = [self.brandResults count];
				}
				else if (self.activeLayer == productsLayer) {
					count = [self.productResults count];
				}
			}
			break;

		case historyTab:
			count = [self.hxResults count];
			break;

		case pantryTab:
			count = [self.pantry count];
			break;

		default:
			// Should never get here unless someone goes rogue and adds a new tab
			break;
	}

	return count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView
cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;

	switch ([self activeTab]) {
		case searchTab:
			if (self.activeLayer == categoriesLayer || self.activeLayer == brandsLayer) {
				static NSString *categoryResultsCellId = @"CategoryResultsCellId";

				cell = [self.tableView dequeueReusableCellWithIdentifier:categoryResultsCellId];
				if (cell == nil) {
					cell = [[UITableViewCell alloc]
						initWithStyle:UITableViewCellStyleSubtitle
						reuseIdentifier:categoryResultsCellId];
				}
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			}
			else {    
				// productLayer uses a nib
				static NSString *productResultsCellId = @"ProductResultsCellId";

				cell = [self.tableView dequeueReusableCellWithIdentifier:productResultsCellId];
				if (cell == nil) {
					[[NSBundle mainBundle] loadNibNamed:@"ProductSearchViewCells"
						owner:self options:nil];
					ProductSearchResultsCell *psrCell = customCell;
					self.customCell = nil;

					StarRatingControl *ratingControl = [[StarRatingControl alloc]
						initWithFrame:CGRectMake(245, 10, 100, 25)];
					ratingControl.userInteractionEnabled = NO;
					ratingControl.emptyStar = [UIImage imageNamed:@"star-03.png"];
					ratingControl.fullStar = [UIImage imageNamed:@"star-01.png"];

					psrCell.ratingControl = ratingControl;
					[psrCell.contentView addSubview:ratingControl];


					cell = psrCell;

					UIImageView *anAccessoryView = [[UIImageView alloc] initWithImage:
						[UIImage imageNamed:@"accessory-orange.png"]];
					anAccessoryView.userInteractionEnabled = YES;

					UITapGestureRecognizer *recognizer = nil;
					recognizer = [[UITapGestureRecognizer alloc]
						initWithTarget:self action:@selector(accessoryTapped:)];
					[anAccessoryView addGestureRecognizer:recognizer];

					cell.accessoryView = anAccessoryView;
				}
			}
			break;

		case historyTab:
			{
				static NSString *historyCellId = @"HistoryCellId";

				cell = [self.tableView dequeueReusableCellWithIdentifier:historyCellId];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
						reuseIdentifier:historyCellId];

					UIImageView *anAccessoryView = [[UIImageView alloc] initWithImage:
						[UIImage imageNamed:@"accessory-orange.png"]];
					anAccessoryView.userInteractionEnabled = YES;

					UITapGestureRecognizer *recognizer = nil;
					recognizer = [[UITapGestureRecognizer alloc]
						initWithTarget:self action:@selector(accessoryTapped:)];
					[anAccessoryView addGestureRecognizer:recognizer];

					cell.accessoryView = anAccessoryView;

					recognizer = [[UITapGestureRecognizer alloc]
						initWithTarget:self action:@selector(cellTapped:)];
					[cell.contentView addGestureRecognizer:recognizer];
				}
			}
			break;

		case pantryTab:
			{
				static NSString *pantryCellId = @"PantryCellId";

				cell = [self.tableView dequeueReusableCellWithIdentifier:pantryCellId];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
						reuseIdentifier:pantryCellId];

					UIImageView *anAccessoryView = [[UIImageView alloc] initWithImage:
						[UIImage imageNamed:@"accessory-orange.png"]];
					anAccessoryView.userInteractionEnabled = YES;

					UITapGestureRecognizer *recognizer = nil;
					recognizer = [[UITapGestureRecognizer alloc]
						initWithTarget:self action:@selector(accessoryTapped:)];
					[anAccessoryView addGestureRecognizer:recognizer];

					cell.accessoryView = anAccessoryView;
				}
			}
			break;

		default:
			// Should never get here unless someone goes rogue and adds a new tab
			break;
	}

	if (cell) {
		[self configureCell:cell atIndexPath:indexPath];

		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(cellTapped:)];
		[cell.contentView addGestureRecognizer:recognizer];
	}

	return cell;
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)aTableView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44.0f;
	if (self.activeTab == searchTab && self.activeLayer == productsLayer) {
		rowHeight = 66.0f;
	}
	return rowHeight;
}

- (NSIndexPath *)tableView:(UITableView *)aTableView
   willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.searchTextField resignFirstResponder];
	return indexPath;
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	NSString *searchTerm = [textField text];
	[textField resignFirstResponder];
	[self refreshSearchForActiveTab:searchTerm forceSearch:YES];

	return YES;
}

- (IBAction)searchTextDidChange:(id)sender
{
	NSString *searchTerm = self.searchTextField.text;
	switch ([self activeTab]) {
		case searchTab:
			break;

		case historyTab:
			[self loadPurchaseHx:searchTerm];
			[self.tableView reloadData];
			break;

		case pantryTab:
			if ([searchTerm length] > 0) {
				if ([searchTerm length] >= 3) {
					self.pantry = [[self.allPantry filteredArrayUsingPredicate:
						[NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@", kName, searchTerm]]
						mutableCopy];
				}
				else {
					self.pantry = [[self.allPantry filteredArrayUsingPredicate:
						[NSPredicate predicateWithFormat:@"%K BEGINSWITH[c] %@", kName, searchTerm]]
						mutableCopy];
				}
			}
			else {
				self.pantry = self.allPantry;
			}
			[self.tableView reloadData];
			break;

		default:
			// Should never get here unless someone goes rogue and adds a new tab
			break;
	}
}


#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

@end
