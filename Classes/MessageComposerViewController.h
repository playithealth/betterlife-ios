//
//  MessageComposerViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/20/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "InboxMessage.h"

@interface MessageComposerViewController : UIViewController

@property (strong, nonatomic) InboxMessage *replyMessage;

@end
