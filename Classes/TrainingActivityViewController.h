//
//  TrainingActivityViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/22/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCViewController.h"

#import "TrainingActivityMO.h"

@protocol TrainingActivityViewDelegate;

@interface TrainingActivityViewController : BCViewController <UITableViewDataSource, UITableViewDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) TrainingActivityMO *trainingActivity;
@property (assign, nonatomic) id<TrainingActivityViewDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (id)initWithLog:(TrainingActivityMO *)log;

@end

@protocol TrainingActivityViewDelegate <NSObject>
- (void)trainingActivityView:(TrainingActivityViewController*)ActivityView didUpdateLog:(TrainingActivityMO *)log;
@end
