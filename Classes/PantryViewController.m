//
//  PantryViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/24/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "PantryViewController.h"
#import <objc/runtime.h>

#import "BCProgressHUD.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BLProductItem.h"
#import "CategoryMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "NSDateFormatter+Additions.h"
#import "NumberValue.h"
#import "PantryItem.h"
#import "PantrySearchViewController.h"
#import "ProductSearchRecord.h"
#import "PurchaseHxItem.h"
#import "ScannedItem.h"
#import "ShoppingListItem.h"
#import "Store.h"
#import "StoreCategory.h"
#import "UIBarButtonItemAdditions.h"
#import "UIViewAdditions.h"
#import "User.h"

static const NSInteger kSectionHeaderHeight = 28;

static const NSInteger kTagAlertItemNotFound = 1;

@interface PantryViewController () <BCSearchViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) PantrySortMode sortMode;

@property (strong, nonatomic) NSIndexPath *expandedIndexPath;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)reloadPantryCache;
- (IBAction)searchButtonTapped:(id)sender;
- (IBAction)scanButtonTapped:(id)sender;
- (IBAction)modeButtonTapped:(id)sender;
- (void)showAddScannedItemAlertView:(NSString *)barcode;
- (void)addOrIncrementItemNamed:(NSString *)name productId:(NSNumber *)productId
   categoryId:(NSNumber *)categoryId barcode:(NSString *)barcode showHUD:(BOOL)showHUD;
- (void)expandRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)collapseRowAtIndexPath:(NSIndexPath *)indexPath;
@end

@implementation PantryViewController

static char barcodeKey;

#pragma mark - Init
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// restore the last sort mode
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		self.sortMode = [defaults integerForKey:kPantrySortSetting];
	}
	return self;
}

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];

	self.navigationController.toolbarHidden = YES;

	[self refresh];
}

- (void)viewWillAppear:(BOOL)animated
{
	if (self.expandedIndexPath != nil) {
		[self configureCell:[self.tableView cellForRowAtIndexPath:self.expandedIndexPath]
			atIndexPath:self.expandedIndexPath];
	}

	// Want to receive notifications regarding syncCheck finishing
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(syncPantryDidFinish:)
		name:@"syncCheck" object:nil];

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// Remove the notification observer for syncCheck
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:@"syncCheck" object:nil];

	[super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowPantryItem"]) {
		PantryItemViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;
		detailView.managedObjectContext = self.managedObjectContext;

		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		PantryItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
		detailView.pantryItem = item;
	}
	else if ([segue.identifier isEqualToString:@"ShowSearch"]) {
		PantrySearchViewController *searchView = segue.destinationViewController;
		searchView.managedObjectContext = self.managedObjectContext;
		searchView.delegate = self;
	}
}

#pragma mark - BCTableViewController overrides
- (void)refresh
{
	[BCObjectManager syncCheck:SendAndReceive];
}

#pragma mark - web responders
- (void)productByBarcodeFetcher:(GTMHTTPFetcher *)inFetcher
   finishedWithData:(NSData *)retrievedData
   error:(NSError *)error
{
	// Hide the HUD
	[BCProgressHUD hideHUD:NO];

	if (error != nil) {
		[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
			withFetcher:inFetcher response:nil];

		// TODO show a different alert with an option to add the item
		// or try the barcode search again?
	}
	else {
		// fetch succeeded
		NSString *barcode =  [inFetcher propertyForKey:@"barcode"];

		NSString *response = [[NSString alloc] initWithData:retrievedData
							  encoding:NSUTF8StringEncoding];
		NSArray *results = [response JSONValue];

		if ([results count] > 0) {
			NSDictionary *product = [results objectAtIndex:0];
			NSNumber *productId = nil;
			NSString *productName = [product valueForKey:kName];
			if ((NSNull *)[product objectForKey:kId] == [NSNull null]) {
				productId = [NSNumber numberWithInt:0];
			}
			else {
				productId = [[product valueForKey:kId]
					numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
			}
			NSNumber *categoryId = [[product valueForKey:kProductCategoryId]
				numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];

			[self addOrIncrementItemNamed:productName
			 productId:productId categoryId:categoryId
			 barcode:barcode showHUD:YES];
		}
		else {
			ScannedItem *cachedItem = [ScannedItem cachedItemByBarcode:barcode inMOC:self.managedObjectContext];
			if (cachedItem) {
				[self addOrIncrementItemNamed:cachedItem.name
					productId:[NSNumber numberWithInteger:0]
					categoryId:[NSNumber numberWithInteger:1]
					barcode:cachedItem.barcode showHUD:YES];
			}
			else {
				[self showAddScannedItemAlertView:barcode];
			}
		}
	}
}

#pragma mark - local methods
- (void)reloadPantryCache
{
	// delete the cache and force a refresh
	self.fetchedResultsController = nil;
	[self.tableView reloadData];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	PantryItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];

	PantryViewCell *pantryCell = (PantryViewCell *)cell;
	pantryCell.nameLabel.text = item.name;
	[pantryCell.quantityButton setTitle:[item.quantity stringValue] forState:UIControlStateNormal];

	if (self.expandedIndexPath != nil
			&& [self.expandedIndexPath compare:indexPath] == NSOrderedSame) {
		ShoppingListItem *itemOnShoppingList = [ShoppingListItem MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:
			@"productId = %@ and name = %@ and accountId = %@ and status <> %@",
			item.productId, item.name, item.accountId, kStatusDelete] inContext:self.managedObjectContext];

		if (itemOnShoppingList) {
			NSString *badgeText = nil;
			if ([itemOnShoppingList.quantity doubleValue] > 0) {
				badgeText = [itemOnShoppingList.quantity stringValue];
			}
			else {
				badgeText = @"?";
			}

			if (!pantryCell.shopQuantityBadge) {
				pantryCell.shopQuantityBadge = [CustomBadge customBadgeWithString:badgeText];
				pantryCell.shopQuantityBadge.userInteractionEnabled = NO;
				CGPoint shopCenter = pantryCell.shopButton.center;
				[pantryCell.shopQuantityBadge setFrame:CGRectMake(shopCenter.x, 0, pantryCell.shopQuantityBadge.frame.size.width,
						pantryCell.shopQuantityBadge.frame.size.height)];
				[pantryCell addSubview:pantryCell.shopQuantityBadge];
			}
			else {
				[pantryCell.shopQuantityBadge autoBadgeSizeWithString:badgeText];
			}
		}
		else {
			[pantryCell.shopQuantityBadge removeFromSuperview];
			pantryCell.shopQuantityBadge = nil;
		}
	}

	//itemCell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
}

- (void)changeQuantity:(NSInteger)change forPantryItem:(PantryItem *)item
{
	item.quantity = [NSNumber numberWithDouble:[item.quantity doubleValue] + change];
	// sanity check
	if ([item.quantity doubleValue] < 0) {
		item.quantity = [NSNumber numberWithInteger:0];
	}
	[item markForSync];

	// update timestamp
	NSTimeInterval timestamp = [[NSDate date] timeIntervalSince1970];
	item.updatedOn = [NSNumber numberWithInteger:(int)timestamp];

	// Save the context.
	NSError *error = nil;
	if (![[self.fetchedResultsController managedObjectContext] save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	[self reloadPantryCache];
}

- (IBAction)searchButtonTapped:(id)sender
{
	// TODO hook in the new search view
}

- (IBAction)scanButtonTapped:(id)sender
{
	ZBarReaderViewController *reader = [ZBarReaderViewController new];
	reader.readerDelegate = self;
	[reader.scanner setSymbology:ZBAR_NONE config:ZBAR_CFG_ENABLE to:0];
	[reader.scanner setSymbology:ZBAR_EAN8 config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_EAN13 config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_UPCA config:ZBAR_CFG_ENABLE to:1];
	[reader.scanner setSymbology:ZBAR_UPCE config:ZBAR_CFG_ENABLE to:1];
	[self presentViewController:reader animated:YES completion:nil];
}

- (IBAction)modeButtonTapped:(id)sender
{
	NSString *infoText = nil;

	switch (self.sortMode) {
		case PantrySortByCategory:
			infoText = @"Sort by name";
			self.sortMode = PantrySortByName;
			break;
		case PantrySortByName:
			infoText = @"Sort by purchase date";
			self.sortMode = PantrySortByPurchaseDate;
			break;
		case PantrySortByPurchaseDate:
		default:
			infoText = @"Sort by category";
			self.sortMode = PantrySortByCategory;
			break;
	}
	
	[BCProgressHUD notificationWithText:infoText onView:self.navigationController.view];

	// save the sort mode as a default
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setInteger:self.sortMode forKey:kPantrySortSetting];

	[self reloadPantryCache];
}

- (void)addOrIncrementItemOnShoppingList:(PantryItem *)pantryItem
{
	// Create a new ShoppingListItem
	[ShoppingListItem addOrIncrementItemNamed:pantryItem.name productId:pantryItem.productId
		category:pantryItem.category barcode:pantryItem.barcode quantity:1
		inMOC:self.managedObjectContext];

	// Save the context.
	NSError *error = nil;
	if (![self.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	[self configureCell:[self.tableView cellForRowAtIndexPath:self.expandedIndexPath]
		atIndexPath:self.expandedIndexPath];
}

- (void)toggleRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.expandedIndexPath != nil) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}
	else {
		[self expandRowAtIndexPath:indexPath];
	}
}

- (void)expandRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.expandedIndexPath != nil) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}

	self.expandedIndexPath = indexPath;

	[self.tableView beginUpdates];
	[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
		withRowAnimation:UITableViewRowAnimationAutomatic];
	[self.tableView endUpdates];

	// force the scroll of this item so that you can see the editing area
	[self.tableView scrollToRowAtIndexPath:self.expandedIndexPath
		atScrollPosition:UITableViewScrollPositionNone animated:YES];
}

- (void)collapseRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSIndexPath *temp = indexPath;
	self.expandedIndexPath = nil;

	[self.tableView beginUpdates];
	[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:temp]
		withRowAnimation:UITableViewRowAnimationAutomatic];
	[self.tableView endUpdates];
}

- (IBAction)quantityButtonTapped:(id)sender
{
	CGRect rect = [self.tableView convertRect:[sender frame] fromView:[sender superview]];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:CGPointMake(rect.origin.x, rect.origin.y)];

	[self toggleRowAtIndexPath:indexPath];
}

- (IBAction)incrementButtonTapped:(id)sender
{
	if (self.expandedIndexPath != nil) {
		PantryItem *updateItem = [self.fetchedResultsController objectAtIndexPath:self.expandedIndexPath];

		// this steps up to the next whole value
		updateItem.quantity = [NSNumber numberWithInteger:[updateItem.quantity integerValue] + 1];
		[updateItem markForSync];

		[self configureCell:[self.tableView cellForRowAtIndexPath:self.expandedIndexPath] atIndexPath:self.expandedIndexPath];

		// Save the context.
		NSError *error = nil;
		if (![[self.fetchedResultsController managedObjectContext] save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}
	}
}

- (IBAction)decrementButtonTapped:(id)sender
{
	if (self.expandedIndexPath != nil) {
		PantryItem *updateItem = [self.fetchedResultsController objectAtIndexPath:self.expandedIndexPath];

		// if this has a remainder, just chop that off
		if ([updateItem.quantity doubleValue] - [updateItem.quantity integerValue] > 0) {
			updateItem.quantity = [NSNumber numberWithInteger:[updateItem.quantity integerValue]];
		}
		// otherwise step down one
		else if ([updateItem.quantity compare:[NSNumber numberWithInteger:0]] == NSOrderedDescending) {
			updateItem.quantity = [NSNumber numberWithInteger:[updateItem.quantity integerValue] - 1];
		}
		[updateItem markForSync];

		[self configureCell:[self.tableView cellForRowAtIndexPath:self.expandedIndexPath] atIndexPath:self.expandedIndexPath];

		// Save the context.
		NSError *error = nil;
		if (![[self.fetchedResultsController managedObjectContext] save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}
	}
}

- (IBAction)addToSLButtonTapped:(id)sender
{
	if (self.expandedIndexPath != nil) {
		PantryItem *pantryItem = [self.fetchedResultsController objectAtIndexPath:self.expandedIndexPath];
		[self addOrIncrementItemOnShoppingList:pantryItem];
	}
}

- (IBAction)deleteButtonTapped:(id)sender
{
	if (self.expandedIndexPath != nil) {
		PantryItem *pantryItem = [self.fetchedResultsController objectAtIndexPath:self.expandedIndexPath];
		pantryItem.status = kStatusDelete;

		[self collapseRowAtIndexPath:self.expandedIndexPath];

		// Save the context.
		NSError *error = nil;
		if (![pantryItem.managedObjectContext save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}
	}
}

- (void)expandedCellTapped:(UITapGestureRecognizer *)recognizer
{
	if (self.expandedIndexPath != nil) {
		[self collapseRowAtIndexPath:self.expandedIndexPath];
	}
}

- (void)addOrIncrementItemNamed:(NSString *)name productId:(NSNumber *)productId
categoryId:(NSNumber *)categoryId barcode:(NSString *)barcode showHUD:(BOOL)showHUD
{
	NSString *infoText = @"";

	// check to see if item already exists by name/productId
	PantryItem *item = [PantryItem addOrIncrementItemNamed:name productId:productId categoryId:categoryId barcode:barcode inMOC:self.managedObjectContext];

	if (item.status == kStatusPost) {
		infoText = [NSString stringWithFormat:@"%@\nadded to your pantry", name];
	}
	else {
		infoText = [NSString stringWithFormat:@"%@\nquantity now %@", name,
				 item.quantity];
	}

	if (showHUD) {
		[BCProgressHUD notificationWithText:infoText onView:self.navigationController.view];
	}

	// Save the context.
	NSError *error = nil;
	if (![item.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}
}

- (void)syncPantryDidFinish:(NSNotification *)notification
{
	[self reloadPantryCache];
}

- (void)getProductByBarcode:(NSString *)barcode andType:(NSString *)type
{
	BCProgressHUD *hud = [BCProgressHUD
		showHUDAddedTo:self.navigationController.view animated:YES];
	hud.labelText = @"Matching UPC";

	// check for the item in the pantry first
	id <BLProductItem> matchingItem = [PantryItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	// then in shoppingList
	if (!matchingItem) {
		matchingItem = [ShoppingListItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	}
	// then in purchase history
	if (!matchingItem) {
		matchingItem = [PurchaseHxItem itemByBarcode:barcode inMOC:self.managedObjectContext];
	}

	// if the item is found in one of the three caches, exit here after sending to the delegate
	if (matchingItem) {
		[BCProgressHUD hideHUD:NO];
		[self addOrIncrementItemNamed:[matchingItem name]
			productId:[matchingItem productId] categoryId:[[matchingItem category] cloudId]
			barcode:barcode showHUD:YES];
		return;
	}
	else {
		DDLogInfo(@"item not found in any cache");
	}

	// if the item wasn't found in any local caches, do the search for the barcode
	NSURL *url = [BCUrlFactory productSearchURLForBarcode:barcode andType:type];

	GTMHTTPFetcher *productByBarcodeFetcher =
		[GTMHTTPFetcher signedFetcherWithURL:url];
	[productByBarcodeFetcher setProperty:barcode forKey:@"barcode"];
	[productByBarcodeFetcher beginFetchWithDelegate:self
		didFinishSelector:@selector(productByBarcodeFetcher:finishedWithData:error:)];
}

- (void)showAddScannedItemAlertView:(NSString *)barcode
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Product not found."
		message:[NSString stringWithFormat:
		@"No matching product found with barcode %@, add this?",
		barcode]
			delegate:self
			cancelButtonTitle:@"No"
			otherButtonTitles:@"Yes", nil];
	alert.tag = kTagAlertItemNotFound;
	alert.alertViewStyle = UIAlertViewStylePlainTextInput;
	[alert textFieldAtIndex:0].placeholder = @"Product name";
	// set the barcode into some storage associated with the alert view
	objc_setAssociatedObject(alert, &barcodeKey, barcode, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

	[alert show];
	// TODO release this in the delegate method
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	NSArray *sections = [self.fetchedResultsController sections];
	NSUInteger count = 0;
	if ([sections count]) {
		id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
		count = [sectionInfo numberOfObjects];
	}
	return count;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;

	static NSString *normalCellId = @"NormalCell";
	static NSString *expandedCellId = @"ExpandedCell";

	if (self.expandedIndexPath != nil && [self.expandedIndexPath compare:indexPath] == NSOrderedSame) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:expandedCellId];

		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(expandedCellTapped:)];
		[cell.contentView addGestureRecognizer:recognizer];
	}
	else {
		cell = [self.tableView dequeueReusableCellWithIdentifier:normalCellId];
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		// mark the pantry item deleted
		PantryItem *item = [self.fetchedResultsController objectAtIndexPath:indexPath];
		item.status = kStatusDelete;

		// Save the context.
		NSError *error = nil;
		if (![[item managedObjectContext] save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}
	}
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (self.sortMode == PantrySortByCategory) {
		return kSectionHeaderHeight;
	}
	return 0.0;
}

- (UIView *)tableView:(UITableView *)theTableView
viewForHeaderInSection:(NSInteger)section
{
	NSString *sectionTitle = @"";

	if (self.sortMode == PantrySortByCategory) {
		id <NSFetchedResultsSectionInfo>sectionInfo = [[self.fetchedResultsController sections]
			objectAtIndex:section];
		if ([[sectionInfo objects] count]) {
			PantryItem *item = (PantryItem *)[[sectionInfo objects] objectAtIndex:0];
			sectionTitle = item.category.name;
		}
	}
	
	if (![sectionTitle length]) {
		return nil;
	}

	return [BCTableViewController customViewForHeaderWithTitle:sectionTitle];
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == kTagAlertItemNotFound) {
		if (buttonIndex != alertView.cancelButtonIndex) {
			NSString *itemName = [alertView textFieldAtIndex:0].text;
			// retrieve the barcode from the associated storage, then clear it
			NSString *barcode = (NSString *)objc_getAssociatedObject(alertView, &barcodeKey);
			objc_setAssociatedObject(alertView, &barcodeKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

			[ScannedItem cacheScannedBarcode:barcode withName:itemName inMOC:self.managedObjectContext];

			[self addOrIncrementItemNamed:itemName
				productId:[NSNumber numberWithInteger:0]
				categoryId:[NSNumber numberWithInteger:1]
				barcode:barcode showHUD:YES];
		}
	}
	else {
		NSAssert(NO, @"Unknown alert");
	}
}

#pragma mark - BCSearchViewDelegate
- (void)searchView:(id)searchView didFinish:(BOOL)finished withSelection:(id)selection
{
	if (selection) {
		if ([selection isKindOfClass:[PurchaseHxItem class]]) {
			PurchaseHxItem *item = (PurchaseHxItem *)selection;
			[self addOrIncrementItemNamed:item.name
				productId:item.productId categoryId:item.category.cloudId
				barcode:item.barcode showHUD:YES];
		}
		else if ([selection isKindOfClass:[ProductSearchRecord class]]) {
			ProductSearchRecord *item = (ProductSearchRecord *)selection;
			[self addOrIncrementItemNamed:item.name
				productId:item.productId categoryId:item.category.cloudId
				barcode:item.barcode showHUD:YES];
		}
		else {
			[self addOrIncrementItemNamed:[selection valueForKey:kItemName]
				productId:[[selection valueForKey:kProductId] numberValueDecimal] 
				categoryId:[[selection valueForKey:kProductCategoryId] numberValueDecimal]
				barcode:[selection valueForKey:kBarcode] 
				showHUD:YES];
		}
	}

	if (finished) {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

#pragma mark - FetchedResultsController delegate
- (NSFetchedResultsController *)fetchedResultsController
{
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}

	// Create the fetch request for the entity.
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSPredicate *pantryPredicate = [NSPredicate predicateWithFormat:
		@"(accountId = %@) AND (status <> %@)",
		[[User currentUser] accountId], kStatusDelete];

	NSArray *sortDescriptors = nil;
	NSString *sectionKey = nil;
	if (self.sortMode == PantrySortByName) {
		NSSortDescriptor *sortByName = [NSSortDescriptor
			sortDescriptorWithKey:@"name" ascending:YES
			selector:@selector(caseInsensitiveCompare:)];
		sortDescriptors = [NSArray arrayWithObject:sortByName];
	}
	else if (self.sortMode == PantrySortByPurchaseDate) {
		NSSortDescriptor *sortByPurchaseDate = [NSSortDescriptor
			sortDescriptorWithKey:@"purchasedOn" ascending:NO];
		sortDescriptors = [NSArray arrayWithObject:sortByPurchaseDate];
	}
	else {     // PantrySortByCategory is the default
		// sort by item category then name
		sectionKey = @"category.name";
		NSSortDescriptor *sortByCategory = [NSSortDescriptor
			sortDescriptorWithKey:@"category.name" ascending:YES];
		NSSortDescriptor *sortByName = [NSSortDescriptor
			sortDescriptorWithKey:@"name" ascending:YES
			selector:@selector(caseInsensitiveCompare:)];
		sortDescriptors = [NSArray arrayWithObjects:
			sortByCategory, sortByName, nil];
	}

	[fetchRequest setEntity:[PantryItem entityInManagedObjectContext:self.managedObjectContext]];
	[fetchRequest setFetchBatchSize:20];
	[fetchRequest setPredicate:pantryPredicate];
	[fetchRequest setSortDescriptors:sortDescriptors];

	// create and init the fetched results controller
	NSFetchedResultsController *aFetchedResultsController =
		[[NSFetchedResultsController alloc]
		initWithFetchRequest:fetchRequest
		managedObjectContext:self.managedObjectContext
		sectionNameKeyPath:sectionKey
		cacheName:@"Pantry"];
	aFetchedResultsController.delegate = self;
	self.fetchedResultsController = aFetchedResultsController;


	NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
	[self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
	switch (type) {
		case NSFetchedResultsChangeInsert:
			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
				withRowAnimation:UITableViewRowAnimationLeft];
			break;

		case NSFetchedResultsChangeDelete:
			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
				withRowAnimation:UITableViewRowAnimationRight];
			break;
        case NSFetchedResultsChangeUpdate:
        case NSFetchedResultsChangeMove:
            break;
	}
}

- (void)controller:(NSFetchedResultsController *)controller
didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath
forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
	UITableView *thisTableView = self.tableView;

	switch (type) {
		case NSFetchedResultsChangeInsert:
			[thisTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
				withRowAnimation:UITableViewRowAnimationLeft];
			break;

		case NSFetchedResultsChangeDelete:
			[thisTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
				withRowAnimation:UITableViewRowAnimationRight];
			break;

		case NSFetchedResultsChangeUpdate:
			[self configureCell:[thisTableView cellForRowAtIndexPath:indexPath]
				atIndexPath:indexPath];
			break;

		case NSFetchedResultsChangeMove:
			[thisTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
				withRowAnimation:UITableViewRowAnimationLeft];
			[thisTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
				withRowAnimation:UITableViewRowAnimationRight];
			break;
	}
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	[self.tableView endUpdates];
}

#pragma mark - PantryItemViewController delegate
- (void)pantryItemView:(PantryItemViewController *)pantryItemView didUpdateItem:(PantryItem *)item
{
	[item markForSync];

	// Save the context.
	NSError *error = nil;
	if (![self.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}
}

#pragma mark - ZBarReaderDelegate methods
- (void)imagePickerController:(UIImagePickerController *)reader didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	id <NSFastEnumeration> results =
		[info objectForKey:ZBarReaderControllerResults];

	ZBarSymbol *symbol = nil;
	for (symbol in results) {
		break;
	}

	NSString *barcode = symbol.data;
	NSString *type = symbol.typeName;
	
	[self getProductByBarcode:barcode andType:type];

	// TODO save image
	//resultImage.image = [info objectForKey: UIImagePickerControllerOriginalImage];

	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
