//
//  HealthProfileViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/15/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "HealthProfileViewController.h"

#import "BCObjectManager.h"
#import "HealthChoiceMO.h"
#import "NumberValue.h"
#import "UserHealthChoice.h"
#import "UIImage+Additions.h"
#import "User.h"

// Make sure these are in the proper order that they display in, left -> right
typedef NS_ENUM(NSInteger, HealthProfileTab) { allergiesTab, lifestyleTab, ingredientsTab };

static const NSUInteger kTagTextField = 1001;

@interface HealthProfileViewController ()
<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    HealthProfileTab _activeTab;
	CGFloat textFieldAnimatedDistance;
	UITextField *editingTextField;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) NSMutableArray *healthChoices;
@property (assign, nonatomic) HealthProfileTab activeTab;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)reloadHealthChoiceCache;
- (void)syncHealthProfileDidFinish:(NSNotification *)notification;
- (void)saveTextFieldValue:(UITextField *)textField;
- (void)keyboardWillHideNotification:(NSNotification*)aNotification;
- (void)keyboardWillShowNotification:(NSNotification*)aNotification;
- (IBAction)segmentedControlValueChanged:(id)sender;
@end


@implementation HealthProfileViewController

#pragma mark - lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self refresh];
	[self reloadHealthChoiceCache];

	// remove the edit button initially since you can't edit except on the ingredients tab
	self.navigationItem.rightBarButtonItem = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
	self.navigationController.toolbarHidden = YES;
	self.navigationItem.title = @"Health Profile";

	// Want to receive notifications regarding syncCheck finishing
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(syncHealthProfileDidFinish:)
		name:@"syncCheck" object:nil];

	// Register for notification when the keyboard will be shown
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardWillShowNotification:)
		name:UIKeyboardWillShowNotification
		object:nil];

	// Register for notification when the keyboard will be hidden
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardWillHideNotification:)
		name:UIKeyboardWillHideNotification
		object:nil];

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	if (editingTextField != nil) {
		[self saveTextFieldValue:editingTextField];
		editingTextField = nil;
	}

	// remove all observers
	[[NSNotificationCenter defaultCenter] removeObserver:self];

	[BCObjectManager syncCheck:SendOnly];

    [super viewWillDisappear:animated];
}

#pragma mark - BCTableView overrides
- (void)refresh
{
	[BCObjectManager syncCheck:SendAndReceive];
}

#pragma mark - Fetched results array
- (NSMutableArray *)healthChoices
{
    if (_healthChoices != nil) {
        return _healthChoices;
    }

	// NOTE this loads a list of the 'meta' choices for the type in the selected tab
	// copies that array into the healthChoices property
	// the selection is done inside configureCell
	switch (self.activeTab) {
		case allergiesTab:
		case lifestyleTab:
		{
			_healthChoices = [[HealthChoiceMO MR_findByAttribute:HealthChoiceMOAttributes.choiceType
				withValue:[NSNumber numberWithInteger:self.activeTab] andOrderBy:HealthChoiceMOAttributes.sortOrder ascending:YES
				inContext:self.managedObjectContext] mutableCopy];
			break;
		}
		case ingredientsTab:
		{
			_healthChoices = [[UserHealthChoice MR_findAllSortedBy:HealthChoiceMOAttributes.name ascending:YES
				withPredicate:[NSPredicate predicateWithFormat:@"loginId = %@ AND choice.choiceType = %@ AND status <> %@",
					[[User currentUser] loginId], [NSNumber numberWithInteger:self.activeTab], kStatusDelete]
				inContext:self.managedObjectContext] mutableCopy];
			break;
		}
	}

    return _healthChoices;
}

#pragma mark - local methods
- (IBAction)addIngredient:(id)sender
{
	// get the name of the warning
	// get a reference to the ingredient warning health choice
	HealthChoiceMO *ingredientWarning = [HealthChoiceMO MR_findFirstByAttribute:HealthChoiceMOAttributes.choiceType withValue:@2
		inContext:self.managedObjectContext];

	UserHealthChoice *newUserChoice = [UserHealthChoice insertInManagedObjectContext:self.managedObjectContext];
	newUserChoice.name = @"";
	newUserChoice.value = @0;
	newUserChoice.loginId = [[User currentUser] loginId];
	newUserChoice.status = kStatusPut;
	newUserChoice.choice = ingredientWarning;

	[self.healthChoices insertObject:newUserChoice atIndex:0];

	[self.tableView reloadData];

	UITableViewCell *cell = [self.tableView 
		cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

	editingTextField = (UITextField *)[cell viewWithTag:kTagTextField];
	editingTextField.enabled = YES;
	editingTextField.delegate = self;
	[editingTextField becomeFirstResponder];
	[self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES];
}

- (void)setActiveTab:(HealthProfileTab)tab
{
	if (_activeTab != tab) {
		_activeTab = tab;

		[editingTextField resignFirstResponder];

		if (tab == ingredientsTab) {
			UIBarButtonItem *addButton = [[UIBarButtonItem alloc] 
				initWithBarButtonSystemItem:UIBarButtonSystemItemAdd 
				target:self action:@selector(addIngredient:)];
			self.navigationItem.rightBarButtonItem = addButton;
		}
		else {
			self.navigationItem.rightBarButtonItem = nil;
		}

		self.segmentedControl.selectedSegmentIndex = tab;

		[self reloadHealthChoiceCache];
		[self.tableView reloadData];
	}
	//QuietLog(@"%s tab:%d", __FUNCTION__, self.activeTab);
}

- (IBAction)segmentedControlValueChanged:(id)sender
{
	[self setActiveTab:[sender selectedSegmentIndex]];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	id choice = [self.healthChoices objectAtIndex:indexPath.row];

	UserHealthChoice *userChoice = nil;
	if (self.activeTab == allergiesTab || self.activeTab == lifestyleTab) {
		NSSet *userChoices = [[choice userHealthChoices] filteredSetUsingPredicate:
			[NSPredicate predicateWithFormat:@"loginId = %@", 
			[[User currentUser] loginId]]];

		userChoice = [userChoices anyObject];
		cell.textLabel.text = [choice name];
		cell.detailTextLabel.text = [choice detailText];
	}
	else {
		userChoice = choice;
		cell.textLabel.text = nil;

		UITextField *textField = (UITextField *)[cell viewWithTag:kTagTextField];
		textField.text = [choice name];
	}

	UIImage *onImage = nil;
	UIImage *offImage = nil;
	if (self.activeTab == allergiesTab) {
		onImage = [UIImage imageNamed:@"alert"];
		offImage = [onImage BC_imageTemplateWithColor:[UIColor lightGrayColor]];
	}
	// special case for the checkbox at the top of the lifestyle tab
	else if (self.activeTab == lifestyleTab && indexPath.row == 0) {
		onImage = [UIImage imageNamed:@"checkbox-check"];
		offImage = [UIImage imageNamed:@"checkbox-gray"];
	}
	else if (self.activeTab == lifestyleTab) {
		onImage = [UIImage imageNamed:@"leaf"];
		offImage = [onImage BC_imageTemplateWithColor:[UIColor lightGrayColor]];
	}

	if (!userChoice || !userChoice.value) {
		cell.imageView.image = offImage;
	}
	else {
		cell.imageView.image = onImage;
	}
}

- (void)saveTextFieldValue:(UITextField *)textField
{
	// Get the cell in which the textfield is embedded
	id textFieldSuper = textField;
	while (textFieldSuper && ![textFieldSuper isKindOfClass:[UITableViewCell class]]) {
		textFieldSuper = [textFieldSuper superview];
	}
	NSIndexPath *indexPathForCell = [self.tableView indexPathForCell:textFieldSuper];
	id choice = [self.healthChoices objectAtIndex:[indexPathForCell row]];
	[choice setName:textField.text];
	[choice setStatus:kStatusPut];

	NSError *error = nil;
	if (![[choice managedObjectContext] save:&error]) {
		DDLogError(@"(%@)[%ld] %@", error.domain, (long)error.code, [error localizedDescription]);
	}

	textField.enabled = NO;
}

- (void)syncHealthProfileDidFinish:(NSNotification *)notification
{
	[self reloadHealthChoiceCache];
}

- (void)reloadHealthChoiceCache
{
	// delete the cache
	self.healthChoices = nil;

	// this will recreate the cache
	[self.tableView reloadData];
}    

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.healthChoices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	static NSString *ingredientsCell = @"IngredientCell";
	static NSString *requireAllCell = @"RequireAllCell";
	static NSString *allergyCell = @"AllergyCell";
	static NSString *lifestyleCell = @"LifestyleCell";

	UITableViewCell *cell = nil;
	if (self.activeTab == ingredientsTab) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:ingredientsCell];
	}
	else if (self.activeTab == lifestyleTab) {
		if (indexPath.row == 0) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:requireAllCell];
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:lifestyleCell];
		}
	}
	else {
		cell = [self.tableView dequeueReusableCellWithIdentifier:allergyCell];
	}

	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)theTableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.activeTab == allergiesTab || (self.activeTab == lifestyleTab)) {
		id choice = [self.healthChoices objectAtIndex:indexPath.row];

		NSSet *userChoices = [[choice userHealthChoices] filteredSetUsingPredicate:
			[NSPredicate predicateWithFormat:@"loginId = %@", 
			[[User currentUser] loginId]]];

		UserHealthChoice *userChoice = [userChoices anyObject];

		if (userChoice) {
			// only two states again
			// nil => 0
			// 0 => nil
			if (!userChoice.value) {
				userChoice.value = @0;
			}
			else {
				userChoice.value = nil;
			}
			userChoice.status = kStatusPut; // these are always PUT
		}
		else {
			UserHealthChoice *newUserChoice = [UserHealthChoice insertInManagedObjectContext:self.managedObjectContext];
			newUserChoice.value = @0;
			newUserChoice.loginId = [[User currentUser] loginId];
			newUserChoice.status = kStatusPut; // these are always PUT
			newUserChoice.choice = choice;
		}

		NSError *error = nil;
		if (![self.managedObjectContext save:&error]) {
			DDLogError(@"(%@)[%ld] %@", error.domain, (long)error.code, [error localizedDescription]);
		}
	}
	else if (self.activeTab == ingredientsTab) {
		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
		editingTextField = (UITextField *)[cell viewWithTag:kTagTextField];
		editingTextField.enabled = YES;
		editingTextField.delegate = self;
		[editingTextField becomeFirstResponder];
		[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
	}

	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath]
		atIndexPath:indexPath];

	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		// only allow deleting of the ingredients filters
		if (self.activeTab == ingredientsTab) {
			// mark the ingredient DELETE
			UserHealthChoice *userChoice = (UserHealthChoice *)[self.healthChoices 
				objectAtIndex:indexPath.row];

			userChoice.status = kStatusDelete;

			// Save the context.
			NSError *error = nil;
			if (![[userChoice managedObjectContext] save:&error]) {
				DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
			}

			[self reloadHealthChoiceCache];
			[self.tableView reloadData];
		}
	}
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[editingTextField resignFirstResponder];

	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[self saveTextFieldValue:textField];

	editingTextField = nil;
}

#pragma mark - Notifications for keyboard hide/show
- (void)keyboardWillShowNotification:(NSNotification*)aNotification 
{
	//
	// Remove any previous view offset.
	//
	[self keyboardWillHideNotification:nil];
	
	//
	// Only animate if the text field is part of the hierarchy that we manage.
	//
	UIView *parentView = [editingTextField superview];
	while (parentView != nil && ![parentView isEqual:self.view])
	{
		parentView = [parentView superview];
	}
	if (parentView == nil)
	{
		//
		// Not our hierarchy... ignore.
		//
		return;
	}
	
	CGRect keyboardRect = [self.view.superview
		convertRect:[[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue]
		fromView:nil];
	
	CGRect viewFrame = self.view.frame;

	textFieldAnimatedDistance = 0;
	if (keyboardRect.origin.y < viewFrame.origin.y + viewFrame.size.height) {
		textFieldAnimatedDistance = (viewFrame.origin.y + viewFrame.size.height) - (keyboardRect.origin.y - viewFrame.origin.y);
		viewFrame.size.height = keyboardRect.origin.y - viewFrame.origin.y;

		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.33];
		[self.view setFrame:viewFrame];
		[UIView commitAnimations];
	}
}

- (void)keyboardWillHideNotification:(NSNotification*)aNotification 
{
	if (textFieldAnimatedDistance == 0) {
		return;
	}
	
	CGRect viewFrame = self.view.frame;
	viewFrame.size.height += textFieldAnimatedDistance;
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.33];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
	
	textFieldAnimatedDistance = 0;
}   

- (void)viewDidUnload {
	[self setSegmentedControl:nil];
	[super viewDidUnload];
}
@end
