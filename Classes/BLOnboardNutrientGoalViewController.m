//
//  BLOnboardNutrientGoalViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 3/29/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLOnboardNutrientGoalViewController.h"

#import "BLHeaderFooterMultiline.h"
#import "NSString+BCAdditions.h"
#import "NutritionMO.h"
#import "UIColor+Additions.h"
#import "UnitMO.h"
#import "UserProfileMO.h"

#pragma mark - Custom Cells
@interface MinMaxCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;
@end
@implementation MinMaxCell
@end

@interface UseBMRCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UISwitch *useBMRSwitch;
@end
@implementation UseBMRCell
@end

#pragma mark
@interface BLOnboardNutrientGoalViewController ()
@property (strong, nonatomic) NSMutableAttributedString *nutrientFooterAttributedString;
@property (strong, nonatomic) NSString *minValueString;
@property (strong, nonatomic) NSString *maxValueString;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) UITextField *activeField;
@end

@implementation BLOnboardNutrientGoalViewController

enum { RowUseBMR = 0, RowMin, RowMax };

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

	[self.tableView registerClass:[BLHeaderFooterMultiline class] forHeaderFooterViewReuseIdentifier:@"Multiline"];

	self.title = @"Goals";

	// Color the 'BMR' in blue to designate a link
	NSString *footerString = @"Values can be left blank.";
	if ([self.nutrientName isEqualToString:NutritionMOAttributes.calories]) {
		footerString = [footerString stringByAppendingString: @" If max is left blank, your BMR data will be used "
			"to automatically calculate a max value."];
	}

	self.nutrientFooterAttributedString = [[NSMutableAttributedString alloc] initWithString:footerString];
	[self.nutrientFooterAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14]
		range:NSMakeRange(0, [self.nutrientFooterAttributedString length])];
	[self.nutrientFooterAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor BC_colorWithHex:0x157efb]
		range:[footerString rangeOfString:@"BMR"]];

	self.minValueString = [[self.userProfileMO.minNutrition valueForKey:self.nutrientName] description];
	self.maxValueString = [[self.userProfileMO.maxNutrition valueForKey:self.nutrientName] description];

	// Don't show empty rows at the bottom
	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;
	if (indexPath.row == RowUseBMR) {
		UseBMRCell *useBMRCell = [tableView dequeueReusableCellWithIdentifier:@"UseBMR" forIndexPath:indexPath];
		cell = useBMRCell;
		if (self.calorieCalculatorGoal) {
			useBMRCell.descriptionLabel.text = [NSString stringWithFormat:@"Use Calories (%@) from Calculator", self.calorieCalculatorGoal];
		}
		else {
			useBMRCell.descriptionLabel.text = @"Use Calories from Calculator";
		}

		useBMRCell.useBMRSwitch.on = [self.userProfileMO.useBMR boolValue];
	}
	else {
		MinMaxCell *mmCell = [tableView dequeueReusableCellWithIdentifier:@"MinMax" forIndexPath:indexPath];
		cell = mmCell;
		switch ([indexPath row]) {
			case RowMin:
				mmCell.nameLabel.text = @"Min";
				mmCell.valueTextField.text = self.minValueString;
				break;
			case RowMax:
				mmCell.nameLabel.text = @"Max";
				mmCell.valueTextField.text = self.maxValueString;
				break;
		}

		mmCell.valueTextField.tag = [indexPath row];

		// This is less efficient than having unitMO as a property, but safer due to sync
		UnitMO *unitMO = [UnitMO MR_findFirstByAttribute:UnitMOAttributes.abbrev withValue:self.nutrientUnits
			inContext:self.managedObjectContext];

		if (unitMO.plural) {
			mmCell.valueTextField.placeholder = unitMO.plural;
		}
		else if ([self.nutrientUnits isEqualToString:@"%"]) {
			// Percent is not in the UnitMO, so make a special case for it
			mmCell.valueTextField.placeholder = @"percent";
		}
	}

    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
	CGFloat height = UITableViewAutomaticDimension;

	if ((indexPath.row == RowUseBMR) && ![self.nutrientName isEqualToString:NutritionMOAttributes.calories]) {
		// Don't show the use bmr row unless this is calories we're editing
		height = 0;
	}
	else if ((indexPath.row > RowUseBMR) && [self.nutrientName isEqualToString:NutritionMOAttributes.calories]
			&& [self.userProfileMO.useBMR boolValue]) {
		height = 0;
	}

	return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 22;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	CGFloat height = 22;
	if ([self.nutrientName isEqualToString:NutritionMOAttributes.calories]) {
		height = 0;
	}

	return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
	if ([self.nutrientUnits length]) {
		headerView.normalLabel.text = [NSString stringWithFormat:@"%@ GOALS (%@)", [self.nutrientDisplayName uppercaseString],
			self.nutrientUnits];
	}
	else {
		headerView.normalLabel.text = [NSString stringWithFormat:@"%@ GOALS", [self.nutrientDisplayName uppercaseString]];
	}

	return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *footerView = nil;

	if (![self.nutrientName isEqualToString:NutritionMOAttributes.calories]) {
		footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
		footerView.multilineLabel.text = @"Values can be left blank.";
	}

	return footerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	MinMaxCell *cell = (MinMaxCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[cell.valueTextField becomeFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
	self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	self.activeField = nil;
	[self serializeTextField:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return NO;
}

#pragma mark - local
- (void)serializeTextField:(UITextField *)textField
{
	switch (textField.tag) {
		case RowMin:
			self.minValueString = textField.text;
			break;
		case RowMax:
			self.maxValueString = textField.text;
			break;
	}
}

#pragma mark - responders

- (IBAction)usingBMRChanged:(UISwitch *)sender {
	self.userProfileMO.useBMR = @(sender.on);
	[self.tableView reloadRowsAtIndexPaths:@[
			[NSIndexPath indexPathForRow:RowMin inSection:0],
			[NSIndexPath indexPathForRow:RowMax inSection:0]]
		withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (IBAction)cancelTapped:(id)sender {
	[self.delegate view:self didUpdateObject:nil];
}

- (IBAction)saveTapped:(id)sender {
	// Check to see if we were in the midst of editing a text field, so that if the save event is fired prior to
	// the end editing event firing, we can still preserve the value they had entered
	if (self.activeField) {
		[self serializeTextField:self.activeField];
	}

	NSNumber *minValue = [self.minValueString BC_nutrientNumber];
	NSNumber *maxValue = [self.maxValueString BC_nutrientNumber];

	// If we're editing calories, and the useBMR is checked, then do not save out min/max calories
	if ([self.nutrientName isEqualToString:NutritionMOAttributes.calories] && [self.userProfileMO.useBMR boolValue]) {
		minValue = maxValue = nil;
	}

	if (maxValue && ([minValue doubleValue] > [maxValue doubleValue])) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Value"
			message:@"Min must not be greater than Max" delegate:self cancelButtonTitle:@"Try again" otherButtonTitles:nil];
		[alert show];
		return;
	}

	// If there is a min value, ensure we have a .minNutrition
	if (!self.userProfileMO.minNutrition && [self.minValueString length]) {
		self.userProfileMO.minNutrition = [NutritionMO insertInManagedObjectContext:self.userProfileMO.managedObjectContext];
	}

	// If there is a max value, ensure we have a .maxNutrition
	if (!self.userProfileMO.maxNutrition && [self.maxValueString length]) {
		self.userProfileMO.maxNutrition = [NutritionMO insertInManagedObjectContext:self.userProfileMO.managedObjectContext];
	}

	[self.userProfileMO.minNutrition setValue:minValue forKey:self.nutrientName];
	[self.userProfileMO.maxNutrition setValue:maxValue forKey:self.nutrientName];

	[self.delegate view:self didUpdateObject:self.userProfileMO];
}

@end
