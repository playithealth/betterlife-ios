//
//  BLLoggingWorkoutActionView.m
//  BuyerCompass
//
//  Created by Greg Goodrich on 2/11/15.
//  Copyright (c) 2015 BettrLife Corporation. All rights reserved.
//

#import "BLLoggingWorkoutActionView.h"

#import "UIColor+Additions.h"

@interface BLAnimatingTextLayer ()
@property (strong, nonatomic) NSNumberFormatter *numberFormatter;
@end

@implementation BLAnimatingTextLayer

@dynamic durationInSeconds;

- (id)init
{
    self = [super init];
    if (self) {
		_numberFormatter = [[NSNumberFormatter alloc] init];
		[_numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[_numberFormatter setMinimumFractionDigits:1];
		[_numberFormatter setMaximumFractionDigits:1];
    }
    return self;
}

+ (BOOL)needsDisplayForKey:(NSString *)key
{
    if ([key isEqualToString:@"durationInSeconds"]) {
        return YES;
    }
    return [super needsDisplayForKey:key];
}

/*
- (id<CAAction>)actionForKey:(NSString *)key
{
	// If we're acting on the duration, and it isn't nil (if it is, we're initializing, and don't want an animation)
    if ([key isEqualToString:@"durationInSeconds"]) {
    //if ([key isEqualToString:@"durationInSeconds"] && self.durationInSeconds ){
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:key];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
		animation.duration  = [self.durationInSeconds doubleValue];
        animation.fromValue = [[self presentationLayer] durationInSeconds];
        return animation;
    }
    return [super actionForKey:key];
}
*/

- (void)display
{
	// If there is no presentation layer duration, then use the regular property, as we may be initializing, and for some
	// reason, the initialization doesn't send the value in the presentation layer. I must be missing something
	double durationInSeconds = ([self.presentationLayer durationInSeconds] ? [[self.presentationLayer durationInSeconds] doubleValue]
		: [[self durationInSeconds] doubleValue]);
	NSUInteger hours = durationInSeconds / 3600;
	NSUInteger minutes = (int)(durationInSeconds / 60) % 60;
	NSUInteger seconds = (int)durationInSeconds % 60;
	if (hours) {
		self.string = [NSString stringWithFormat:@"%lu:%02lu:%02lu", (unsigned long)hours, (unsigned long)minutes, (unsigned long)seconds];
	}
	else if (minutes) {
		self.string = [NSString stringWithFormat:@"%lu:%02lu", (unsigned long)minutes, (unsigned long)seconds];
	}
	else {
		NSString *secondsString = [self.numberFormatter stringFromNumber:@(durationInSeconds)];
		self.string = secondsString;
	}
	if (self.maximumFontSize) {
		self.fontSize = self.maximumFontSize;
		UIFont *myFont = [UIFont fontWithName:(__bridge NSString *)CTFontCopyPostScriptName(self.font) size:self.fontSize];
		// options:NSStringDrawingUsesFontLeading
		CGRect rect = [self.string boundingRectWithSize:self.bounds.size options:0
			attributes:@{ NSFontAttributeName : myFont } context:nil];
		while ((rect.size.width > self.bounds.size.width) && (self.fontSize > 0)) {
			self.fontSize -= 1.0;
			myFont = [UIFont systemFontOfSize:self.fontSize];
			rect = [self.string boundingRectWithSize:self.bounds.size options:0
				attributes:@{ NSFontAttributeName : myFont } context:nil];
		}
		// Don't alter the horizontal position, assume it was set properly
		[self centerVertically];
	}
	[super display];
}

- (void)centerVertically {
	UIFont *myFont = [UIFont fontWithName:(__bridge NSString *)CTFontCopyPostScriptName(self.font) size:self.fontSize];
	//self.position = CGPointMake(self.superlayer.frame.size.width / 2.0, (self.superlayer.frame.size.height / 2.0)
	self.position = CGPointMake(self.position.x, (self.superlayer.frame.size.height / 2.0)
		+ (self.bounds.size.height / 2.0) - myFont.ascender + (myFont.capHeight / 2.0));
}

@end

@interface BLLoggingWorkoutActionView ()
@property (strong, nonatomic) CAShapeLayer *progressLayer;
@property (strong, nonatomic) CAShapeLayer *checkLayer;
@property (strong, nonatomic) CALayer *checkImageLayer;
@property (strong, nonatomic) BLAnimatingTextLayer *progressTextLayer;
@property (strong, nonatomic) UILabel *progressLabel;
@property (strong, nonatomic) UIBezierPath *progressPath;
@property (assign, nonatomic) BOOL started;
@property (strong, nonatomic) NSNumber *durationInSeconds;
@end

@implementation BLLoggingWorkoutActionView

- (void)initialize
{
    _progressColor = [UIColor BL_activityOrange];
	_showCircle = YES;

	_progressLayer = [[CAShapeLayer alloc] init];
	[self.layer addSublayer:_progressLayer];

	_progressTextLayer = [[BLAnimatingTextLayer alloc] init];
	[_progressLayer addSublayer:_progressTextLayer];

	// Layer and sublayer for showing the circle with a checkmark image in it
	_checkLayer = [[CAShapeLayer alloc] init];
	[self.layer addSublayer:_checkLayer];
	_checkImageLayer = [[CALayer alloc] init];
	[_checkLayer addSublayer:_checkImageLayer];

	_progressLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self addSubview:_progressLabel];

	//_selectedImageView = [[UIImageView alloc] init];

	[self setup];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		[self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)aRect
{
    self = [super initWithFrame:aRect];
    if (self) {
		[self initialize];
    }
    return self;
}

- (void)prepareForInterfaceBuilder
{
//	[self setup];
}

- (void)setup
{
	//self.layer.contents = (id)[[UIImage imageNamed:@"TimedActivityProgressionChevrons"] CGImage];
	// Make sure the progress layer is square by using the smaller dimension from the bounds of 'self'
	double progressLayerSize = MIN(self.bounds.size.width, self.bounds.size.height);
	self.progressLayer.frame = CGRectMake(0, 0, progressLayerSize, progressLayerSize);
	// Center the progress layer within the view, remember the view may not be exactly the same size as this layer!
	self.progressLayer.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
	self.progressLayer.strokeColor = [self.progressColor CGColor];
	self.progressLayer.lineWidth = 1.0f;
    self.progressLayer.fillColor = nil;

	if (!self.progressPath) {
		CGPoint position = CGPointMake(CGRectGetMidX(self.progressLayer.bounds), CGRectGetMidY(self.progressLayer.bounds));
		self.progressPath = [UIBezierPath bezierPathWithArcCenter:position radius:self.progressLayer.bounds.size.width / 2
			startAngle:[self degreesToRadians:270] endAngle:[self degreesToRadians:269.999] clockwise:YES];
	}
	self.progressLayer.path = self.progressPath.CGPath;

	self.progressTextLayer.frame = CGRectInset(self.progressLayer.bounds, 2, 14);
	self.progressTextLayer.foregroundColor = [self.progressColor CGColor];
	self.progressTextLayer.contentsScale = [[UIScreen mainScreen] scale];
	self.progressTextLayer.maximumFontSize = 16.0;
	self.progressTextLayer.alignmentMode = kCAAlignmentCenter;
	self.progressTextLayer.hidden = YES;

	self.checkLayer.frame = self.progressLayer.frame;
	self.checkLayer.position = CGPointMake(CGRectGetMidX(self.checkLayer.bounds), CGRectGetMidY(self.checkLayer.bounds));
	self.checkLayer.contentsGravity = kCAGravityResizeAspectFill;
	self.checkLayer.hidden = YES;
	self.checkLayer.path = [UIBezierPath bezierPathWithOvalInRect:self.checkLayer.bounds].CGPath;
    self.checkLayer.fillColor = [UIColor BL_activityGreen].CGColor;

	//self.checkImageLayer.position = self.progressLayer.position;
	self.checkImageLayer.frame = self.checkLayer.frame;
	self.checkImageLayer.position = self.checkLayer.position;
	self.checkImageLayer.contents = (id)[[UIImage imageNamed:@"Checkmark-Activities"] CGImage];
	self.checkImageLayer.contentsGravity = kCAGravityCenter;
	self.checkImageLayer.contentsScale = [[UIScreen mainScreen] scale];

	/*
	self.selectedImageView.frame = self.progressLayer.frame;
	[self addSubview:self.selectedImageView];
	self.selectedImageView.hidden = YES;
	// TODO Move this out, make it settable from outside, or default here?
	self.selectedImageView.image = [UIImage imageNamed:@"Checkmark-Activities"];
	self.selectedImageView.contentMode = UIViewContentModeCenter;
	self.selectedImageView.layer.cornerRadius = self.bounds.size.height / 2.0f;
	self.selectedImageView.layer.backgroundColor = [UIColor BL_activityGreen].CGColor;
	*/
}

- (void)layoutSubviews {
    [super layoutSubviews];

	self.progressLayer.path = (self.showCircle ? self.progressPath.CGPath : nil);

	[self.progressLabel setFrame:CGRectInset(self.bounds, 2, 10)];
	self.progressLabel.textColor = self.progressColor;
	self.progressLabel.textAlignment = NSTextAlignmentCenter;
	self.progressLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)setProgressColor:(UIColor *)newColor {
	_progressColor = newColor;
	self.progressLayer.strokeColor = newColor.CGColor;
	// Do I need this?
	[self.progressLayer setNeedsDisplay];
}

- (void)setLabelText:(NSString *)text {
	self.progressLabel.text = text;
}

- (void)setShowCircle:(BOOL)show {
	_showCircle = show;
	[self.progressLayer setNeedsLayout];
	//[self.progressLayer setNeedsDisplay];
}

- (CGFloat)degreesToRadians:(CGFloat)degrees {
    return ((degrees / 180.0f) * M_PI);
}

- (void)startProgressWithDuration:(NSNumber *)seconds {
	if (!self.started) {
		self.started = YES;
		self.progressLabel.hidden = YES;
		self.progressLayer.lineWidth = 4.0f;
		self.progressLayer.strokeStart = 0.0;
		self.progressLayer.strokeEnd = 1.0;
		self.progressLayer.path = self.progressPath.CGPath; // Ensure we have a path in the progress layer
		self.progressTextLayer.hidden = NO;

        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"durationInSeconds"];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
		animation.duration = [seconds doubleValue];
        animation.fromValue = seconds;
        animation.toValue = @(0);
		self.progressTextLayer.durationInSeconds  = @(0);
		[self.progressTextLayer addAnimation:animation forKey:nil];

		CABasicAnimation *animateStrokeEnd = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
		animateStrokeEnd.duration = [seconds doubleValue];
		animateStrokeEnd.fromValue = [NSNumber numberWithFloat:0.0f];
		animateStrokeEnd.toValue = [NSNumber numberWithFloat:1.0f];
		animateStrokeEnd.delegate = self;
		[self.progressLayer addAnimation:animateStrokeEnd forKey:nil];
	}
	else {
		if (self.progressLayer.speed == 0.0) {
			[self resumeLayer:self.progressLayer];
		}
		else {
			[self pauseLayer:self.progressLayer];
		}
	}
}

- (void)pauseLayer:(CALayer*)layer {
	CFTimeInterval pausedTime = [layer convertTime:CACurrentMediaTime() fromLayer:nil];
	layer.speed = 0.0;
	layer.timeOffset = pausedTime;
}
 
- (void)resumeLayer:(CALayer*)layer {
	CFTimeInterval pausedTime = [layer timeOffset];
	layer.speed = 1.0;
	layer.timeOffset = 0.0;
	layer.beginTime = 0.0;
	CFTimeInterval timeSincePause = [layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
	layer.beginTime = timeSincePause;
}

- (void)setCompleted:(BOOL)completed
{
	_completed = completed;
	// Use the completed flag to determine which layers are visible
	self.progressLayer.hidden = completed;
	self.progressLabel.hidden = completed;
	self.checkLayer.hidden = !completed;

	if (completed) {
		CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
		animation.duration = 0.3;
		animation.fromValue = (__bridge id)[UIBezierPath bezierPathWithOvalInRect:CGRectMake(self.checkLayer.bounds.size.height / 2,
			self.checkLayer.bounds.size.width / 2, 0, 0)].CGPath;
		animation.toValue = (__bridge id)[UIBezierPath bezierPathWithOvalInRect:self.checkLayer.bounds].CGPath;
		[self.checkLayer addAnimation:animation forKey:nil];
	}
}

#pragma mark - CAAnimation Delegate
- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
	self.completed = YES;
	if (self.delegate && [self.delegate respondsToSelector:@selector(actionComplete:)]) {
		[self.delegate actionComplete:self];
	}
}
@end
