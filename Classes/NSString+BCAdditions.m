//
//  NSString+BCAdditions.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/5/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "NSString+BCAdditions.h"
#import "NumberValue.h"

#import <CommonCrypto/CommonDigest.h>

#import "Base64Transcoder.h"

@implementation NSString(BCAdditions)

- (NSString *)BC_escapeSpecialCharacters {
	return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, 
			(__bridge CFStringRef)self, NULL, 
			CFSTR("!*'();:@&=+$,/?#[]<>\n\t\" "), kCFStringEncodingUTF8);
}

- (NSString *)BC_escapeURIReservedCharacters {
	return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, 
			(__bridge CFStringRef)self, NULL, 
			CFSTR("!*'();:@&=+$,/?#[]"), kCFStringEncodingUTF8);
}

- (NSData *)BC_decodeBase64EncodedJPEG
{
	NSString *encodingPrefix = @"data:image/jpeg;base64";
	NSData *imageData = nil;
	if ([self hasPrefix:encodingPrefix]) {
		NSString *encodedImage = [self substringFromIndex:[encodingPrefix length]];

		// prepare a Byte array for the input data
		Byte inputData[[encodedImage lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];

		// get the pointer of the data
		[[encodedImage dataUsingEncoding:NSUTF8StringEncoding] getBytes:inputData];
		size_t inputDataSize = (size_t)[encodedImage length];

		// calculate the decoded data size
		size_t outputDataSize = EstimateBas64DecodedDataSize(inputDataSize);

		// prepare a Byte array for the decoded data
		Byte outputData[outputDataSize];

		// decode the data
		Base64DecodeData(inputData, inputDataSize, outputData, &outputDataSize);

		// create the NSData object from the decoded data
		imageData = [[NSData alloc] initWithBytes:outputData length:outputDataSize];
	}

	return imageData;
}

- (NSString *)BC_passwordHash
{
	const char *s = [self cStringUsingEncoding:NSASCIIStringEncoding];
	NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
	uint8_t digest[CC_SHA1_DIGEST_LENGTH] = { 0 };

	CC_SHA1(keyData.bytes, (CC_LONG)keyData.length, digest);
	NSData *out = [NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH];
	NSString *hash = [out description];
	hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
	hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
	hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];

	return hash;
}

+ (NSString *)BC_stringWithNutrientNumber:(NSNumber *)number withPrecision:(NSUInteger)precision
{
    static NSNumberFormatter *_numberFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _numberFormatter = [[NSNumberFormatter alloc] init];
        [_numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[_numberFormatter setMinimumFractionDigits:0];
    });

	[_numberFormatter setMaximumFractionDigits:precision];

	NSNumber *absNumber = [NSNumber numberWithFloat:fabs([number floatValue])];
	NSString *sign = @"";
	if ([number integerValue] < 0) {
		sign = @"<";
	}
    return [NSString stringWithFormat:@"%@%@", sign, [_numberFormatter stringFromNumber:absNumber]];
}

+ (NSString *)BC_stringWithNutrientNumber:(NSNumber *)number
{
	return [NSString BC_stringWithNutrientNumber:number withPrecision:1];
}

- (NSNumber *)BC_nutrientNumber
{
	NSNumber *nutrientNumber = nil;
	if ([self length] && [self characterAtIndex:0] == '<') {
		nutrientNumber = [[NSString stringWithFormat:@"-%@", [self substringFromIndex:1]] numberValueDecimal];
	}
	else {
		nutrientNumber = [self numberValueDecimal];
	}

	return nutrientNumber;
}

- (NSNumber *)BL_numberWithPrecision:(NSUInteger)precision
{
    static NSNumberFormatter *_numberFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _numberFormatter = [[NSNumberFormatter alloc] init];
        [_numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[_numberFormatter setMinimumFractionDigits:0];
    });

	[_numberFormatter setMaximumFractionDigits:precision];

	return [_numberFormatter numberFromString:self];
}

@end

