//
//  MealEditViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/30/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCMoreLessFooter.h"
#import "RecipeMO.h"
#import "MealIngredientEditView.h"
#import "StarRatingControl.h"

@protocol MealEditViewDelegate;

@interface MealEditViewController : UITableViewController <BCMoreLessFooterDelegate, MealIngredientEditViewDelegate, StarRatingControlDelegate, UITextViewDelegate>

@property (nonatomic, strong) RecipeMO *recipe;
@property (nonatomic, unsafe_unretained) id<MealEditViewDelegate> delegate;

@end

@protocol MealEditViewDelegate <NSObject>
@required
- (void)mealEditView:(MealEditViewController *)mealEditView didUpdateMeal:(RecipeMO *)recipe;
@end
