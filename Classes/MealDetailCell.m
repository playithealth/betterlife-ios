//
//  MealDetailCell.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 8/23/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "MealDetailCell.h"

#import "StarRatingControl.h"

@implementation MealDetailCell
- (CGSize)sizeThatFits:(CGSize)size
{
	NSInteger lh = self.mealImageView.frame.size.height + 10; // Padding is for the top
	NSInteger rh = self.servesLabel.frame.origin.y + self.servesLabel.frame.size.height
		- self.mealNameLabel.frame.origin.y;

	CGSize newSize = size;
	if (lh > rh) {
		newSize.height = lh;
	}
	else {
		newSize.height = rh;
	}

	// Add some padding to the height to keep the control(s) from the bottom edge of the cell
	newSize.height += 8;

	return newSize;
}

- (void)layoutSubviews
{
	CGRect rect = self.mealNameLabel.frame;
	CGSize size = [self.mealNameLabel.text sizeWithFont:[UIFont systemFontOfSize:17]
		constrainedToSize:CGSizeMake(self.mealNameLabel.frame.size.width, 1000)];
	
	rect.size.height = size.height;
	self.mealNameLabel.frame = rect;

	rect = self.starRatingControl.frame;
	rect.origin.y = self.mealNameLabel.frame.origin.y + self.mealNameLabel.frame.size.height + 5;
	self.starRatingControl.frame = rect;

	rect = self.servesLabel.frame;
	size = [self.servesLabel.text sizeWithFont:self.servesLabel.font
		constrainedToSize:CGSizeMake(self.servesLabel.frame.size.width, 1000)];
	rect.origin.y = self.starRatingControl.frame.origin.y + self.starRatingControl.frame.size.height + 5;
	rect.size.height = size.height;
	self.servesLabel.frame = rect;

	// Now adjust the background view, wow, took me a long time to figure out that we need to do this!
	rect = self.backgroundView.frame;
	size = [self sizeThatFits:rect.size];
	rect.size.height = size.height;
	self.backgroundView.frame = rect;
}

@end
