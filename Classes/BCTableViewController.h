//
//  BCTableViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/15/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

@interface BCTableViewController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)refresh:(id)sender;
- (void)reloadBCTableViewData;

+ (UIView *)customViewForHeaderWithTitle:(NSString *)title;

@end
