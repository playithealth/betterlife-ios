//
//  ProductSearchRecord.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/27/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "ProductSearchRecord.h"

#import "CategoryMO.h"
#import "NumberValue.h"

@implementation ProductSearchRecord

- (id)initWithSearchDictionary:(NSDictionary *)searchDict 
withManagedObjectContext:(NSManagedObjectContext *)moc
{
	if ((self = [super init])) {
		[self setWithSearchDictionary:searchDict withManagedObjectContext:moc];
	}
	return (self);
}

+ (id)recordWithSearchDictionary:(NSDictionary *)searchDict 
withManagedObjectContext:(NSManagedObjectContext *)moc
{
	ProductSearchRecord *newRecord = [[ProductSearchRecord alloc]
		initWithSearchDictionary:searchDict withManagedObjectContext:moc];
	return newRecord;
}

- (void)setWithSearchDictionary:(NSDictionary *)searchDict 
withManagedObjectContext:(NSManagedObjectContext *)inMOC
{
	self.productId = [[searchDict valueForKey:kId] numberValueDecimal];
	self.name = [searchDict valueForKey:kName];
	self.genericName = [searchDict valueForKey:kGenericName];
	self.size = [searchDict valueForKey:@"size"];
	self.imageId = [[searchDict valueForKey:@"image_id"] numberValueDecimal];
	if ((NSNull *)[searchDict objectForKey:kBarcode] != [NSNull null]) {
		self.barcode = [searchDict valueForKey:kBarcode];
	}
	if ((NSNull *)[searchDict objectForKey:kRating] != [NSNull null]) {
		self.rating = [[searchDict valueForKey:kRating] numberValueDecimal];
	}
	if ((NSNull *)[searchDict objectForKey:kRatingCount] != [NSNull null]) {
		self.ratingCount = [[searchDict valueForKey:kRatingCount] numberValueDecimal];
	}
	self.productCategoryId = [[searchDict valueForKey:kProductCategoryId] numberValueDecimal];
	self.category = [CategoryMO categoryById:
		[[searchDict valueForKey:kProductCategoryId] numberValueDecimal]
		withManagedObjectContext:inMOC];
	if ((NSNull *)[searchDict objectForKey:kHealthFilterAllergyCount] != [NSNull null]) {
		self.allergyCount = [[searchDict valueForKey:kHealthFilterAllergyCount] numberValueDecimal];
	}
	if ((NSNull *)[searchDict objectForKey:kHealthFilterLifestyleCount] != [NSNull null]) {
		self.lifestyleCount = [[searchDict valueForKey:kHealthFilterLifestyleCount]
			numberValueDecimal];
	}

	self.calories = [[searchDict valueForKey:kNutritionCalories] numberValueDecimal];
	self.caloriesFromFat = [[searchDict valueForKey:kNutritionCaloriesFromFat] numberValueDecimal];
	self.totalFat = [[searchDict valueForKey:kNutritionTotalFat] numberValueDecimal];
	self.transFat = [[searchDict valueForKey:kNutritionTransFat] numberValueDecimal];
	self.saturatedFat = [[searchDict valueForKey:kNutritionSaturatedFat] numberValueDecimal];
	self.saturatedFatCalories = [[searchDict valueForKey:kNutritionSaturatedFatCalories] numberValueDecimal];
	self.polyunsaturatedFat = [[searchDict valueForKey:kNutritionPolyunsaturatedFat] numberValueDecimal];
	self.monounsaturatedFat = [[searchDict valueForKey:kNutritionMonounsaturatedFat] numberValueDecimal];
	self.cholesterol = [[searchDict valueForKey:kNutritionCholesterol] numberValueDecimal];
	self.sodium = [[searchDict valueForKey:kNutritionSodium] numberValueDecimal];
	self.potassium = [[searchDict valueForKey:kNutritionPotassium] numberValueDecimal];
	self.totalCarbohydrates = [[searchDict valueForKey:kNutritionTotalCarbohydrates] numberValueDecimal];
	self.otherCarbohydrates = [[searchDict valueForKey:kNutritionOtherCarbohydrates] numberValueDecimal];
	self.dietaryFiber = [[searchDict valueForKey:kNutritionDietaryFiber] numberValueDecimal];
	self.solubleFiber = [[searchDict valueForKey:kNutritionSolubleFiber] numberValueDecimal];
	self.insolubleFiber = [[searchDict valueForKey:kNutritionInsolubleFiber] numberValueDecimal];
	self.sugars = [[searchDict valueForKey:kNutritionSugars] numberValueDecimal];
	self.sugarsAlcohol = [[searchDict valueForKey:kNutritionSugarsAlcohol] numberValueDecimal];
	self.protein = [[searchDict valueForKey:kNutritionProtein] numberValueDecimal];
	self.vitaminAPercent = [[searchDict valueForKey:kNutritionVitaminAPercent] numberValueDecimal];
	self.vitaminCPercent = [[searchDict valueForKey:kNutritionVitaminCPercent] numberValueDecimal];
	self.calciumPercent = [[searchDict valueForKey:kNutritionCalciumPercent] numberValueDecimal];
	self.ironPercent = [[searchDict valueForKey:kNutritionIronPercent] numberValueDecimal];
}

- (NSDictionary *)dictionary
{
	NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
	[dictionary setObject:self.name forKey:kItemName];
	[dictionary setObject:self.productId forKey:kProductId];
	[dictionary setObject:self.category.cloudId forKey:kProductCategoryId];

	if (self.barcode) {
		[dictionary setObject:self.barcode forKey:kBarcode];
	}
	if (self.imageId) {
		[dictionary setObject:self.imageId forKey:kImageId];
	}

	if (self.calories) {
		[dictionary setObject:self.calories forKey:kNutritionCalories];
	}
	if (self.caloriesFromFat) {
		[dictionary setObject:self.caloriesFromFat forKey:kNutritionCaloriesFromFat];
	}
	if (self.totalFat) {
		[dictionary setObject:self.totalFat forKey:kNutritionTotalFat];
	}
	if (self.transFat) {
		[dictionary setObject:self.transFat forKey:kNutritionTransFat];
	}
	if (self.saturatedFat) {
		[dictionary setObject:self.saturatedFat forKey:kNutritionSaturatedFat];
	}
	if (self.saturatedFatCalories) {
		[dictionary setObject:self.saturatedFatCalories forKey:kNutritionSaturatedFatCalories];
	}
	if (self.polyunsaturatedFat) {
		[dictionary setObject:self.polyunsaturatedFat forKey:kNutritionPolyunsaturatedFat];
	}
	if (self.monounsaturatedFat) {
		[dictionary setObject:self.monounsaturatedFat forKey:kNutritionMonounsaturatedFat];
	}
	if (self.cholesterol) {
		[dictionary setObject:self.cholesterol forKey:kNutritionCholesterol];
	}
	if (self.sodium) {
		[dictionary setObject:self.sodium forKey:kNutritionSodium];
	}
	if (self.potassium) {
		[dictionary setObject:self.potassium forKey:kNutritionPotassium];
	}
	if (self.totalCarbohydrates) {
		[dictionary setObject:self.totalCarbohydrates forKey:kNutritionTotalCarbohydrates];
	}
	if (self.otherCarbohydrates) {
		[dictionary setObject:self.otherCarbohydrates forKey:kNutritionOtherCarbohydrates];
	}
	if (self.dietaryFiber) {
		[dictionary setObject:self.dietaryFiber forKey:kNutritionDietaryFiber];
	}
	if (self.solubleFiber) {
		[dictionary setObject:self.solubleFiber forKey:kNutritionSolubleFiber];
	}
	if (self.insolubleFiber) {
		[dictionary setObject:self.insolubleFiber forKey:kNutritionInsolubleFiber];
	}
	if (self.sugars) {
		[dictionary setObject:self.sugars forKey:kNutritionSugars];
	}
	if (self.sugarsAlcohol) {
		[dictionary setObject:self.sugarsAlcohol forKey:kNutritionSugarsAlcohol];
	}
	if (self.protein) {
		[dictionary setObject:self.protein forKey:kNutritionProtein];
	}
	if (self.vitaminAPercent) {
		[dictionary setObject:self.vitaminAPercent forKey:kNutritionVitaminAPercent];
	}
	if (self.vitaminCPercent) {
		[dictionary setObject:self.vitaminCPercent forKey:kNutritionVitaminCPercent];
	}
	if (self.calciumPercent) {
		[dictionary setObject:self.calciumPercent forKey:kNutritionCalciumPercent];
	}
	if (self.ironPercent) {
		[dictionary setObject:self.ironPercent forKey:kNutritionIronPercent];
	}

	return dictionary;
}

- (NSString *)description
{
	return [[self dictionary] description];
}

@end
