//
//  BLAlwaysShowViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 11/9/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLAlwaysShowViewController : UIViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end
