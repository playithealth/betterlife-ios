//
//  Store.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/27/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "Store.h"

#import "BCAppDelegate.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"
#import "NumberValue.h"
#import "ShoppingListItem.h"
#import "StoreCategory.h"
#import "User.h"


@implementation Store

+ (Store *)currentStore
{
	static dispatch_once_t once;
	static Store *sharedInstance = nil;

	dispatch_once(&once, ^{
		sharedInstance = [[Store alloc] init];
	});

	return sharedInstance;
}

+ (void)clearCurrentStore
{
	Store *store = [self currentStore];
	store.name = nil;
	store.address = nil;
	store.storeId = nil;
	store.userStore = nil;
	store.storeCategories = nil;
	store.storeCategoriesDict = nil;
}

+ (void)setCurrentStoreWithDictionary:(NSDictionary *)inStore inMOC:(NSManagedObjectContext *)moc
{
	Store *store = [self currentStore];
	store.name = [inStore valueForKey:kStore];
	store.address = [inStore valueForKey:kAddress];
	store.storeId = [[inStore valueForKey:kCloudId] numberValueDecimal];
	store.buyerCompassClient = [[[inStore valueForKey:kBuyerCompassClient] numberValueDecimal] boolValue];
	store.userStore = nil;
	store.storeCategories = nil;
	store.storeCategoriesDict = nil;
	[store storeCheckIn:moc];
}

+ (void)setCurrentStoreWithUserStore:(UserStoreMO *)inStore inMOC:(NSManagedObjectContext *)moc
{
	Store *store = [self currentStore];
	store.name = inStore.name;
	store.address = inStore.address;
	store.storeId = inStore.storeEntityId;
	store.buyerCompassClient = [inStore.buyerCompassClient boolValue];
	store.userStore = inStore;
	if (![[inStore categories] count]) {
		// This user store doesn't seem to have any store categories, add them
		[inStore populateStoreCategories];
	}
	store.storeCategories = [[[inStore categories] allObjects] mutableCopy];
	store.storeCategoriesDict = nil;
	[store storeCheckIn:moc];
}

- (id)copyWithZone:(NSZone *)zone
{
	Store *newStore = [[Store allocWithZone:zone] init];
	newStore.userStore = self.userStore;
	newStore.storeCategories = self.storeCategories;
	newStore.storeCategoriesDict = self.storeCategoriesDict;
	newStore.name = self.name;
	newStore.address = self.address;
	newStore.storeId = self.storeId;

	return newStore;
}


- (BOOL)isStoreSet
{
	return (([self.name length] > 0) && (self.storeId > 0));
}

- (BOOL)isUserStore
{
	return (self.userStore ? YES : NO);
}

- (BOOL)hasOrderedCategories
{
	if (![self isStoreSet]) {
		return NO;
	}

	return ([[self storeCategories] count] ? YES : NO);
}

- (void)storeCheckIn:(NSManagedObjectContext *)moc
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];

	__block id responseObject = nil;

	// Check-in with the cloud
	if (appDelegate.connected) {
		// pass flag to cloud as to whether we want categories returned from the check-in call
		// if this is a user store, we don't need sort data, as we have it
		NSDictionary *postData = @{kEntityId : self.storeId, @"sortdata" : (self.userStore ? @0 : @1)};
		NSData *body = [NSJSONSerialization dataWithJSONObject:postData options:0 error:0];

		GTMHTTPFetcher *checkinFetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory checkInUrl] httpMethod:kPost postData:body];
		[checkinFetcher beginFetchWithCompletionHandler:
			^(NSData *retrievedData, NSError * error) {
				if (error != nil) {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:checkinFetcher response:nil];
				}
				else {
					responseObject = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

					// Only returns the success token if no sort data is requested, so only
					// check this if a dictionary came back
					if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]
							&& ([responseObject objectForKey:kSuccess] == nil)) {
						[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:checkinFetcher response:nil];
					}
				}
				[self updateShoppingListItems:responseObject inMOC:moc];
			}];
	}

}

- (void)updateShoppingListItems:(NSArray *)cloudCategories inMOC:(NSManagedObjectContext *)inMOC
{
	// If we have no category ordering data, nothing to do here
	if (!self.storeCategories && !cloudCategories) {
		return;
	}

	BOOL useCloudData = NO;
	if (!self.storeCategories) {
		useCloudData = YES;
	}

	if (useCloudData) {
		self.storeCategories = [[StoreCategory MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"(store = NULL)"]
			inContext:inMOC] mutableCopy];
	}

	// Build a dictionary that will be used to quickly access any one category's sort order
	self.storeCategoriesDict = [[NSMutableDictionary alloc] init];
	for (StoreCategory *storeCategory in self.storeCategories) {
		[self.storeCategoriesDict setObject:storeCategory forKey:storeCategory.category.cloudId];
	}

	if (useCloudData && [cloudCategories isKindOfClass:[NSArray class]]) {
		// Ensure all the rows exist in the storeCategory entity for the 'null' store,
		// and update their sort orders
		for (NSDictionary *cloudDict in cloudCategories) {
			NSNumber *cloudStoreCatId = [[cloudDict valueForKey:@"category_id"] numberValueDecimal];
			StoreCategory *storeCategory = [self.storeCategoriesDict objectForKey:cloudStoreCatId];
			if (!storeCategory) {
				// Need to add a storecategory entity for this category for the 'null' store
				storeCategory = [StoreCategory insertInManagedObjectContext:inMOC];
				storeCategory.category = [CategoryMO categoryById:cloudStoreCatId withManagedObjectContext:inMOC];
				// Add this to the storeCategories property
				[self.storeCategories addObject:storeCategory];
				// And add it to the dictionary
				[self.storeCategoriesDict setObject:storeCategory forKey:cloudStoreCatId];
			}
			// Update the sort order to reflect what the cloud passed back
			storeCategory.sortOrder = [[cloudDict valueForKey:@"sort_order"] numberValueDecimal];
		}
		[inMOC save:nil];
	}

	NSArray *slItems = [ShoppingListItem MR_findAllSortedBy:@"category.name" ascending:YES 
		withPredicate:[NSPredicate predicateWithFormat:@"(accountId = %@) AND (status <> %@) AND (purchased = NO)",
			[[User currentUser] accountId], kStatusDelete]
		inContext:inMOC];

	for (ShoppingListItem *slItem in slItems) {
		StoreCategory *storeCategory = [self.storeCategoriesDict objectForKey:slItem.category.cloudId];
		if (storeCategory) {
			slItem.storeCategory = (id)[inMOC existingObjectWithID:[storeCategory objectID] error:nil];
		}
	}

	[inMOC BL_save];
}

// Given a category, return the storeCategory of that category based upon the currently set store
- (StoreCategory *)storeCategoryForCategory:(CategoryMO *)category
{
	return [self.storeCategoriesDict objectForKey:category.cloudId];
}

- (NSString *)displayString
{
	NSString *title = nil;

	if (!self.name) {
		title = nil;
	}
	else if (!self.address) {
		title = self.name;
	}
	else {
		title = [NSString stringWithFormat:@"%@, %@", self.name, self.address];
	}

	return title;
}

@end
