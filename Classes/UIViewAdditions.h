//
//  UIViewAdditions.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/19/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIView (UIViewAdditions)
- (void)addBoundsOvershootAnimation;
@end
