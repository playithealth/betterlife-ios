//
//  BCUserStoresViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/25/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import "BCUserStoresViewController.h"

#import "StoreDetailViewController.h"
#import "User.h"
#import "UserStoreMO.h"

@interface BCUserStoresViewController () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation BCUserStoresViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowDetail"]) {
		StoreDetailViewController *detailView = segue.destinationViewController;
		detailView.managedObjectContext = self.managedObjectContext;

		detailView.storeData = [self.fetchedResultsController 
			objectAtIndexPath:[self.tableView indexPathForCell:sender]];
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section 
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StoreCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UserStoreMO *store = [self.fetchedResultsController objectAtIndexPath:indexPath];

	cell.textLabel.text = store.name;
	cell.detailTextLabel.text = store.address;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{ 
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo name];
}
 
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)table
{
    return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index 
{
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

	User *thisUser = [User currentUser];

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	fetchRequest.entity = [UserStoreMO entityInManagedObjectContext:self.managedObjectContext];
    fetchRequest.fetchBatchSize = 20;
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"accountId = %@", thisUser.accountId];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:UserStoreMOAttributes.name ascending:YES]];
    
    // create and init the fetched results controller
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
		managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;

    _fetchedResultsController = aFetchedResultsController;

	NSError *error = nil;
	if (![_fetchedResultsController performFetch:&error]) {
	    DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}    

@end
