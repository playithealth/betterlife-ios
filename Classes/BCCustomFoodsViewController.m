//
//  BCCustomFoodsViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 12/16/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCCustomFoodsViewController.h"

#import "BCEditCustomFoodViewController.h"
#import "CustomFoodMO.h"
#import "User.h"

@interface BCCustomFoodsViewController ()
@property (strong, nonatomic) NSArray *customFoods;
@end

@implementation BCCustomFoodsViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// custom loading
	NSArray *unordered = [CustomFoodMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"%K = %@",
		CustomFoodMOAttributes.accountId, [[User currentUser] accountId]] inContext:self.managedObjectContext];
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:CustomFoodMOAttributes.name ascending:YES
		selector:@selector(caseInsensitiveCompare:)];        
	self.customFoods = [unordered sortedArrayUsingDescriptors:@[sortByName]];

	self.navigationController.hidesBottomBarWhenPushed = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"EditCustomFood"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		
		BCEditCustomFoodViewController *viewController = segue.destinationViewController;
		viewController.delegate = self;

		NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
		viewController.managedObjectContext = scratchContext;
		viewController.customFood = (CustomFoodMO *)[scratchContext objectWithID:[self.customFoods[indexPath.row] objectID]];
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.customFoods.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
	CustomFoodMO *customFood = self.customFoods[indexPath.row];
	cell.textLabel.text = customFood.name;
    
    return cell;
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	[self dismissViewControllerAnimated:YES completion:^{
		if (object) {
			CustomFoodMO *food = object;
			NSError *error = nil;
			if (![food.managedObjectContext save:&error]) {
				DDLogError(@"some error %@", error);
			}

			if (![self.managedObjectContext save:&error]) {
				DDLogError(@"some error %@", error);
			}

			[self.tableView reloadData];
		}
	}];
}

@end
