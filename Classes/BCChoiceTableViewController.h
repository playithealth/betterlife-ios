//
//  BCChoiceTableViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on July 30, 2012.
//  Copyright 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BCChoiceTableViewDelegate;

@interface BCChoiceTableViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) NSString *headerTitle;
@property (strong, readonly, nonatomic) NSMutableArray *sections;
@property (weak, nonatomic) id<BCChoiceTableViewDelegate> delegate;
@property (assign, nonatomic) NSInteger tag;

- (void)addSection:(NSDictionary *)section;
@end

@protocol BCChoiceTableViewDelegate <NSObject>
@optional
- (void)BCChoiceView:(BCChoiceTableViewController *)choiceView didFinish:(BOOL)finish;
- (void)BCChoiceView:(BCChoiceTableViewController *)choiceView didSelectRowAtIndexPath:(NSIndexPath *)selectedIndexPath;
@end
