//
//  BCActivitySearchDataModel.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/18/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCActivitySearchDataModel.h"

#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "ExerciseRoutineMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "NSDictionary+NumberValue.h"
#import "TrainingActivityMO.h"
#import "User.h"

@interface BCActivitySearchDataModel ()

@property (strong, nonatomic) NSMutableArray *exerciseRoutines;
@property (strong, nonatomic) NSMutableArray *trainingActivities;
@property (strong, nonatomic) NSMutableArray *trainingActivitiesGroupedByActivity;
@property (strong, nonatomic) NSMutableArray *trainingActivitiesGroupedByDate;
@property (strong, nonatomic) NSMutableArray *filteredActivities;
@property (strong, nonatomic) NSString *lastSearchTerm;
@property (assign, nonatomic) NSUInteger offset;
@end


@implementation BCActivitySearchDataModel

#pragma mark - lifecycle
- (id)initWithManagedObjectContext:(NSManagedObjectContext *)moc withLogDate:(NSDate *)logDate
{
	if ((self = [super init])) {
		_managedObjectContext = moc;
		_logDate = logDate;
		_exerciseRoutines = [[ExerciseRoutineMO MR_findAllInContext:_managedObjectContext] mutableCopy];
		_trainingActivities = [[TrainingActivityMO MR_findAllWithPredicate:
			[NSPredicate predicateWithFormat:@"activity.length > 0 AND source = NULL AND %K = %@ AND %K <> %@", 
			TrainingActivityMOAttributes.loginId, [[User currentUser] loginId],
			TrainingActivityMOAttributes.status, kStatusDelete] inContext:_managedObjectContext] mutableCopy];

	}
	return self;
}

- (void)save
{
	NSError *error = nil;
	if (![self.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}
}

- (NSArray *)allExerciseRoutines
{
	return _exerciseRoutines;
}

- (NSArray *)allTrainingActivities
{
	return _trainingActivities;
}

- (void)activitySmartSearch:(NSString *)searchTerm
completionBlock:(void (^)(NSUInteger resultCount, BOOL more, NSError *error))completionBlock
{
	if ([searchTerm isEqualToString:self.lastSearchTerm]) {
        // Duplicate search, this should be a noop but we still need to call the completion block so that it may
        // clean up whatever it may have
        completionBlock(0, NO, [NSError errorWithDomain:@"Duplicate Search" code:1 userInfo:nil]);
		return;
	}
	self.lastSearchTerm = searchTerm;

	self.filteredActivities = nil;
	[self activitySmartSearch:searchTerm offset:0 limit:10 completionBlock:completionBlock];
}

- (void)activitySmartSearchMoreWithCompletionBlock:(void (^)(NSUInteger resultCount, BOOL more, NSError *error))completionBlock
{
	[self activitySmartSearch:self.lastSearchTerm offset:self.offset limit:10 completionBlock:completionBlock];
}

- (void)activitySmartSearch:(NSString *)searchTerm offset:(NSUInteger)offset limit:(NSUInteger)limit
completionBlock:(void (^)(NSUInteger resultCount, BOOL more, NSError *error))completionBlock
{
	NSURL *searchURL = [BCUrlFactory activitySmartSearchURLForSearchTerm:searchTerm offset:offset rows:limit];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
	[fetcher beginFetchWithCompletionHandler:
		^(NSData *retrievedData, NSError *error) {
			NSUInteger resultCount = 0;
			BOOL showMore = NO;
			if (error != nil) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
			}
			else {
				NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:kNilOptions error:nil];
				NSDictionary *sectionsDict = resultDict[@"sections"][0];
				showMore = [[sectionsDict BC_numberForKey:@"showmore"] boolValue];
				NSArray *activities = [sectionsDict objectForKey:@"items"];
				resultCount = [activities count];

				if (self.filteredActivities) {
					[self.filteredActivities addObjectsFromArray:activities];
				}
				else {
					self.filteredActivities = [activities mutableCopy];
				}
				self.moreResults = showMore;
				self.offset = [self.filteredActivities count];
			}
			completionBlock(resultCount, showMore, error);
		}];
}

- (id)insertActivityBasedOnExerciseRoutine:(ExerciseRoutineMO *)routine
{
	TrainingActivityMO *newActivity = [TrainingActivityMO insertInManagedObjectContext:self.managedObjectContext];
	newActivity.loginId = [[User currentUser] loginId];
	newActivity.date = self.logDate;
	newActivity.activity = routine.activity;
	newActivity.factor = routine.factor;
	newActivity.exerciseRoutine = routine;

	newActivity.status = kStatusPost;

	NSError *error = nil;
	if (![self.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	[_trainingActivities addObject:newActivity];

	return newActivity;
}

- (id)insertActivityBasedOnTrainingActivity:(TrainingActivityMO *)activity
{
	TrainingActivityMO *newActivity = [TrainingActivityMO insertInManagedObjectContext:self.managedObjectContext];
	newActivity.loginId = [[User currentUser] loginId];
	newActivity.date = self.logDate;
	newActivity.calories = activity.calories;
	newActivity.activity = activity.activity;
	newActivity.distance = activity.distance;
	newActivity.factor = activity.factor;
	newActivity.time = activity.time;
	newActivity.weight = activity.weight;

	newActivity.status = kStatusPost;

	NSError *error = nil;
	if (![self.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	[_trainingActivities addObject:newActivity];

	return newActivity;
}

- (NSArray *)filterTrainingActivities:(NSString *)searchText
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.activity contains[c] %@", searchText];
	return [_trainingActivities filteredArrayUsingPredicate:predicate];	
}

- (NSArray *)trainingActivitiesGroupedByActivity
{
	if (_trainingActivitiesGroupedByActivity) {
		return _trainingActivitiesGroupedByActivity;
	}

	if ([self.trainingActivities count]) {
		NSMutableArray* activitiesWithCount = [[NSMutableArray alloc] init];
		for (TrainingActivityMO *loggedActivity in self.trainingActivities) {
			NSUInteger matchingLogIndex = [activitiesWithCount 
				indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					NSString *activityName = [obj valueForKeyPath:@"activity.activity"];
					return [activityName isEqualToString:loggedActivity.activity];
				}];

			if (matchingLogIndex == NSNotFound) {
				NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

				[dict setValue:@1 forKey:@"count"];
				[dict setValue:loggedActivity forKey:@"activity"];

				[activitiesWithCount addObject:dict];
			}
			else {
				NSMutableDictionary *dict = [activitiesWithCount objectAtIndex:matchingLogIndex];
				NSNumber *count = [dict objectForKey:@"count"];
				[dict setValue:@([count integerValue] + 1)  forKey:@"count"];
			}
		}

		NSSortDescriptor *sortByCount = [NSSortDescriptor sortDescriptorWithKey:@"count" ascending:NO];
		NSSortDescriptor *sortByActivity = [NSSortDescriptor sortDescriptorWithKey:@"activity.activity" ascending:YES];
		_trainingActivitiesGroupedByActivity = [activitiesWithCount sortedArrayUsingDescriptors:@[sortByCount, sortByActivity]];
	}
	else {
		_trainingActivitiesGroupedByActivity = [NSArray array];
	}

	return _trainingActivitiesGroupedByActivity;
}

- (NSArray *)trainingActivitiesGroupedByDate
{
	if (_trainingActivitiesGroupedByDate) {
		return _trainingActivitiesGroupedByDate;
	}

	if ([self.trainingActivities count]) {
		NSMutableArray *temp = [NSMutableArray array];

		NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:TrainingActivityMOAttributes.date ascending:NO];
		NSArray *trainingActivitiesSortedByDate = [self.trainingActivities sortedArrayUsingDescriptors:@[sortByDate]];

		NSDate *lastDate = nil;
		NSInteger activityCount = 0;
		for (TrainingActivityMO *activity in trainingActivitiesSortedByDate) {
			if ([activity.date isEqualToDate:lastDate]) {
				// grab the last item in the array
				NSMutableDictionary *activityDict = [temp lastObject];

				// add this activity to the embedded array
				NSMutableArray *activities = activityDict[@"activities"];
				[activities addObject:activity];

				// remake the name string
				NSSet *activityNames = [activities valueForKeyPath:TrainingActivityMOAttributes.activity];
				activityDict[TrainingActivityMOAttributes.activity] = [[activityNames allObjects] componentsJoinedByString:@", "];
			}
			else if (activityCount >= 30) {
				break;
			}
			else {
				lastDate = activity.date;
				activityCount++;

				// create a new record
				NSMutableDictionary *activityDict = [NSMutableDictionary dictionary];

				// give it a name
				activityDict[TrainingActivityMOAttributes.activity] = activity.activity;

				// set the date string
				activityDict[TrainingActivityMOAttributes.date] = [self stringFromDate:activity.date];

				// create the embedded array
				NSMutableArray *activities = [NSMutableArray array];
				[activities addObject:activity];
				activityDict[@"activities"] = activities;

				// add this to the outer array
				[temp addObject:activityDict];
			}
		}
		_trainingActivitiesGroupedByDate = temp;
	}
	else {
		_trainingActivitiesGroupedByDate = [NSArray array];
	}

	return _trainingActivitiesGroupedByDate;
}

#pragma mark - local
- (NSString *)stringFromDate:(NSDate *)date
{
	static NSDateFormatter *_dateFormatter = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
			_dateFormatter = [[NSDateFormatter alloc] init];
			[_dateFormatter setTimeStyle:NSDateFormatterNoStyle];
			[_dateFormatter setDateStyle:NSDateFormatterShortStyle];
		});

	NSString *string = [_dateFormatter stringFromDate:date];
	return string;
}

@end
