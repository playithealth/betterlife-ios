//
//  BCStrengthLogDetailViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/27/2013
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCStrengthLogDetailViewController.h"

#import "NSDate+Additions.h"
#import "NumberValue.h"
#import "TrackingMO.h"
#import "TrainingStrengthMO.h"
#import "TrainingStrengthSetMO.h"
#import "User.h"

@interface BCStrengthLogDetailViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIPickerView *resistancePicker;

@property (weak, nonatomic) UITextField *activeField;
@property (assign, nonatomic) BOOL resistancePickerShown;

- (void)keyboardWillShowNotification:(NSNotification*)aNotification;
- (void)keyboardWillHideNotification:(NSNotification*)aNotification;

@end

@implementation BCStrengthLogDetailViewController

static const CGFloat kSectionHeaderHeight = 20.0;

static const NSInteger kSectionHeader = 0;
static const NSInteger kSectionActivity = 1;
static const NSInteger kSectionSets = 2;
static const NSInteger kNumberOfSections = 3;

static const CGFloat kStandardRowHeight = 44.0;
static const CGFloat kSetsRowHeight = 88.0;
static const CGFloat kPickerRowHeight = 217.0;
static const CGFloat kHeaderRowHeight = 100.0;

static const NSInteger kRowName = 0;
static const NSInteger kRowResistance = 1;
static const NSInteger kRowResistancePicker = 2;
static const NSInteger kNumberOfRows = 3;

static const NSInteger kTagValueLabel = 1001;
static const NSInteger kTagTextField = 1002;
//static const NSInteger kTagDurationPicker = 1003;
static const NSInteger kTagSecondTextField = 1012;

#pragma mark - View lifecycle
- (void)viewWillAppear:(BOOL)animated
{
	// Register for notification when the keyboard will be shown
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardWillShowNotification:)
		name:UIKeyboardWillShowNotification
		object:nil];

	// Register for notification when the keyboard will be hidden
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardWillHideNotification:)
		name:UIKeyboardWillHideNotification
		object:nil];

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// remove all observers
	[[NSNotificationCenter defaultCenter] removeObserver:self];

    [super viewWillDisappear:animated];
}

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
	return UIBarPositionTopAttached;
}

#pragma mark - local
- (TrainingStrengthMO *)strengthLog
{
	if (_strengthLog) {
		return _strengthLog;
	}

	_strengthLog = [TrainingStrengthMO insertInManagedObjectContext:self.managedObjectContext];
	_strengthLog.loginId = [[User currentUser] loginId];
	_strengthLog.date = self.logDate;
	_strengthLog.resistance = @"isotonic";

	TrainingStrengthSetMO *newSet = [TrainingStrengthSetMO insertInManagedObjectContext:self.managedObjectContext];
	newSet.trainingStrength = _strengthLog;
	newSet.sortOrder = @1;

	return _strengthLog;
}

- (void)showResistancePicker
{
	if (self.activeField) {
		[self.activeField resignFirstResponder];
	}

	self.tableView.scrollEnabled = NO;

	self.resistancePickerShown = YES;
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowResistancePicker inSection:kSectionActivity]]
		withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)hideResistancePicker
{
	self.tableView.scrollEnabled = YES;

	self.resistancePickerShown = NO;
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:kRowResistancePicker inSection:kSectionActivity]]
		withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	NSInteger numberOfRows = 3;
	return numberOfRows;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *title = nil;

	if (row == 0) {
		title = @"Weights/Machine (Isotonic)";
	}
	else if (row == 1) {
		title = @"Bodyweight (Calisthenic)";
	}
	else if (row == 2) {
		title = @"Isometric";
	}

	return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	if (row == 0) {
		self.strengthLog.resistance = @"isotonic"; //"@"Weights/Machine (Isotonic)";
	}
	else if (row == 1) {
		self.strengthLog.resistance = @"calisthenic";//@"Bodyweight (Calisthenic)";
	}
	else if (row == 2) {
		self.strengthLog.resistance = @"isometric";
	}

	NSIndexPath *resistanceIndexPath = [NSIndexPath indexPathForRow:kRowResistance inSection:kSectionActivity];
	[self configureCell:[self.tableView cellForRowAtIndexPath:resistanceIndexPath] atIndexPath:resistanceIndexPath];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return kNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	switch (section) {
		case kSectionHeader:
			numberOfRows = 1;
			break;
		case kSectionActivity:
			numberOfRows = kNumberOfRows;
			break;
		case kSectionSets:
			numberOfRows = self.strengthLog.sets.count + 1;
			break;
		default:
			break;
	}

	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *headerCellId = @"HeaderCell";
	static NSString *nameCellId = @"NameCell";
	static NSString *resistanceCellId = @"ResistanceCell";
	static NSString *resistancePickerCellId = @"ResistancePickerCell";
	static NSString *setCellId = @"SetCell";
	static NSString *addSetCellId = @"AddSetCell";

	UITableViewCell *cell = nil;

	switch (indexPath.section) {
		case kSectionHeader:
		{
			cell = [self.tableView dequeueReusableCellWithIdentifier:headerCellId];
			break;
		}
		default:
		case kSectionActivity:
		{
			if (indexPath.row == kRowName) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:nameCellId];
			}
			else if (indexPath.row == kRowResistancePicker) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:resistancePickerCellId];
			}
			else if (indexPath.row == kRowResistance) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:resistanceCellId];
			}
			break;
		}
		case kSectionSets:
		{
			if (indexPath.row < self.strengthLog.sets.count) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:setCellId];
			}
			else {
				cell = [self.tableView dequeueReusableCellWithIdentifier:addSetCellId];
			}
			break;
		}
	}

	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionActivity) {
		switch (indexPath.row) {
			case kRowName: {
				UITextField *textField = (UITextField *)[cell viewWithTag:kTagTextField];
				textField.text = self.strengthLog.name;
				break;
			}
			case kRowResistance: {
				UILabel *valueLabel = (UILabel *)[cell viewWithTag:kTagValueLabel];
				valueLabel.text = self.strengthLog.resistance;
				if ([self.strengthLog.resistance isEqualToString:@"isotonic"]) {
					valueLabel.text = @"Weights/Machine (Isotonic)";
				}
				else if ([self.strengthLog.resistance isEqualToString:@"calisthenic"]) {
					valueLabel.text = @"Bodyweight (Calisthenic)";
				}
				else if ([self.strengthLog.resistance isEqualToString:@"isometric"]) {
					valueLabel.text = @"Isometric";
				}
				break;
			}
		}
	}
	else {
		if (indexPath.row < self.strengthLog.sets.count) {
			TrainingStrengthSetMO *set = self.strengthLog.sortedSets[indexPath.row];
			UITextField *weightTextField = (UITextField *)[cell viewWithTag:kTagTextField];
			weightTextField.text = [NSString stringWithFormat:@"%@", (set.weight ?: @"")];
			UITextField *repsTextField = (UITextField *)[cell viewWithTag:kTagSecondTextField];
			repsTextField.text = [NSString stringWithFormat:@"%@", (set.reps ?: @"")];
		}
	}
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat headerHeight = 0.0f;
	if (section == kSectionActivity || section == kSectionSets) {
		headerHeight = kSectionHeaderHeight;
	}
	return headerHeight;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString *headerText = nil;
	if (section == kSectionActivity) {
		headerText = @"ACTIVITY";
	}
	else if (section == kSectionSets) {
		headerText = @"SETS";
	}
	return headerText;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44.0f;
	switch (indexPath.section) {
		case kSectionHeader:
		{
			rowHeight = kHeaderRowHeight;
			break;
		}
		case kSectionSets:
		{
			if (indexPath.row == self.strengthLog.sets.count) {
				rowHeight = kStandardRowHeight;
			}
			else {
				rowHeight = kSetsRowHeight;
			}
			break;
		}
		default:
		case kSectionActivity:
		{
			if (indexPath.row == kRowResistancePicker) {
				rowHeight = self.resistancePickerShown ? kPickerRowHeight : 0.0f;
			}
			else {
				rowHeight = kStandardRowHeight;
			}
			break;
		}
	}
	return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionActivity) {
		switch (indexPath.row) {
			case kRowName: {
				if (self.resistancePickerShown) {
					[self hideResistancePicker];
				}
				break;
			}
			case kRowResistance: {
				if (self.resistancePickerShown) {
					[self hideResistancePicker];
				}
				else {
					[self showResistancePicker];
				}
				break;
			}
		}
	}
	else if ((indexPath.section == kSectionSets) && (indexPath.row == self.strengthLog.sets.count)) {
		TrainingStrengthSetMO *newSet = [TrainingStrengthSetMO insertInManagedObjectContext:self.managedObjectContext];
		newSet.trainingStrength = self.strengthLog;
		newSet.sortOrder = @([[[[self.strengthLog sortedSets] lastObject] sortOrder] integerValue] + 1);

		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionSets] withRowAnimation:UITableViewRowAnimationAutomatic];
	}
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	CGPoint textFieldPosition = [textField convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath* indexPath = [self.tableView indexPathForRowAtPoint:textFieldPosition];

	[self saveValueFromTextField:textField atIndexPath:indexPath];

	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	if (self.resistancePickerShown) {
		[self hideResistancePicker];
	}
	self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	self.activeField = nil;
	CGPoint textFieldPosition = [textField convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath* indexPath = [self.tableView indexPathForRowAtPoint:textFieldPosition];

	[self saveValueFromTextField:textField atIndexPath:indexPath];
}

- (void)finishEditing
{
	if (self.activeField) {
		[self textFieldDidEndEditing:self.activeField];
	}
}

- (void)saveValueFromTextField:(UITextField *)textField atIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionSets) {
		TrainingStrengthSetMO *setToEdit = self.strengthLog.sortedSets[indexPath.row];

		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
		UITextField *weightTextField = (UITextField *)[cell viewWithTag:kTagTextField];

		UITextField *repsTextField = (UITextField *)[cell viewWithTag:kTagSecondTextField];

		if (textField == weightTextField) {
			setToEdit.weight = [textField.text numberValueDecimal];
		}
		else if (textField == repsTextField) {
			setToEdit.reps = [textField.text numberValueDecimal];
		}

		[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
	}
	else {
		if (indexPath.row == kRowName) {
			self.strengthLog.name = textField.text;
			[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
		}
	}
}

#pragma mark - Notifications for keyboard hide/show
- (void)keyboardWillShowNotification:(NSNotification*)aNotification
{
	NSDictionary *keyboardAnimationDetail = [aNotification userInfo];

	CGRect keyboardEndFrameWindow = [keyboardAnimationDetail[UIKeyboardFrameEndUserInfoKey] CGRectValue];

	double keyboardTransitionDuration  = [keyboardAnimationDetail[UIKeyboardAnimationDurationUserInfoKey] doubleValue];

	CGRect keyboardEndFrameView = [self.view convertRect:keyboardEndFrameWindow fromView:nil];

	[UIView animateWithDuration:keyboardTransitionDuration 
		animations:^{
			self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, 0, self.view.frame.size.height - keyboardEndFrameView.origin.y, 0);
			self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(self.tableView.scrollIndicatorInsets.top, 0, self.view.frame.size.height - keyboardEndFrameView.origin.y, 0);
			[self.view layoutIfNeeded];
		}];
}

- (void)keyboardWillHideNotification:(NSNotification*)aNotification
{
	NSDictionary *keyboardAnimationDetail = [aNotification userInfo];

	double keyboardTransitionDuration  = [keyboardAnimationDetail[UIKeyboardAnimationDurationUserInfoKey] doubleValue];

	[UIView animateWithDuration:keyboardTransitionDuration 
		animations:^{
			self.tableView.contentInset = UIEdgeInsetsZero;
			self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
			[self.view layoutIfNeeded];
		}];
}   

@end
