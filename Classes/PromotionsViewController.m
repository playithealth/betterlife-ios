//
//  PromotionsViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "PromotionsViewController.h"

#import "PromotionDetailViewController.h"
#import "ShoppingListItem.h"
#import "UIViewAdditions.h"
#import "User.h"

@interface PromotionsViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation PromotionsViewController
@synthesize list;
@synthesize tableView;
@synthesize activeTabImageView;

#pragma mark - lifespan
- (void)viewDidLoad
{
	self.navigationItem.title = @"Save";

	NSArray *array = [[NSArray alloc] initWithObjects:@"50\% off stuff", 
		@"Buy One Get One Free", @"Buy Mayonnaise Get Onions Free", 
		@"Buy 2 bags of Lays Potato Chips - Pay Full Price", nil];
	self.list = array;
	[super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidUnload
{
	self.list = nil;
	childController = nil;
	[self setActiveTabImageView:nil];
	[super viewDidUnload];
}

#pragma mark - Table Data Source Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [list count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellId = @"PromotionsCellId";
    
    UITableViewCell *cell = [self.tableView 
		dequeueReusableCellWithIdentifier:CellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
			reuseIdentifier:CellId];
    }
    
	[self configureCell:cell atIndexPath:indexPath];

    return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	NSUInteger row = [indexPath row];
	NSString *rowString = [list objectAtIndex:row];
	cell.textLabel.text = rowString;
	cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)theTableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	// do something here
}
- (void)tableView:(UITableView *)theTableView
accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	if (childController == nil) {
		childController = [[PromotionDetailViewController alloc]
			initWithNibName:nil bundle:nil];
	}

	childController.title = @"Disclosure Button Pressed";
	NSUInteger row = [indexPath row];

	NSString *selectedItem = [list objectAtIndex:row];
	NSString *detailMessage = [[NSString alloc]
		initWithFormat:@"You selected %@", selectedItem];
	childController.message = detailMessage;
	childController.title = selectedItem;
	[self.navigationController pushViewController:childController animated:YES];
}

#pragma mark - custom methods
- (IBAction)scanButtonClicked:(id)sender
{
	DDLogVerbose(@"scan clicked");
}
- (IBAction)recentClicked:(id)sender
{
	DDLogVerbose(@"recent clicked");
	[UIView beginAnimations:@"animatedActiveTab" context:nil];
	[UIView setAnimationDuration:0.4];
	[activeTabImageView setFrame:CGRectMake(-10.0f, 47.0f, 117.0f, 34.0f)];
	[UIView commitAnimations];
}
- (IBAction)nearbyClicked:(id)sender
{
	DDLogVerbose(@"nearby clicked");
	[UIView beginAnimations:@"animatedActiveTab" context:nil];
	[UIView setAnimationDuration:0.4];
	[activeTabImageView setFrame:CGRectMake(102.0f, 47.0f, 117.0f, 34.0f)];
	[UIView commitAnimations];
}
- (IBAction)recommendedClicked:(id)sender
{
	DDLogVerbose(@"recommended clicked");
	[UIView beginAnimations:@"animatedActiveTab" context:nil];
	[UIView setAnimationDuration:0.4];
	[activeTabImageView setFrame:CGRectMake(214.0f, 47.0f, 117.0f, 34.0f)];
	[UIView commitAnimations];
}

@end
