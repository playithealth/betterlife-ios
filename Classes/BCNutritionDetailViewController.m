//
//  BCNutritionDetailViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 5/21/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCNutritionDetailViewController.h"

#import "BCDashboardViewController.h"
#import "BCDrawBlockView.h"
#import "BCUtilities.h"
#import "BLAlertController.h"
#import "FoodLogMO.h"
#import "NSCalendar+Additions.h"
#import "NSDate+Additions.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "TrainingActivityMO.h"
#import "UIColor+Additions.h"
#import "User.h"
#import "UserProfileMO.h"

@interface BCNutritionDetailViewController ()
@property (weak, nonatomic) IBOutlet BCDrawBlockView *graphView;
@property (weak, nonatomic) IBOutlet UIImageView *noEntriesImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *scopeControl;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSArray *nutrientLogsByDay;
@property (strong, nonatomic) NSArray *nutrientLogsByMonth;
@property (strong, nonatomic) NSArray *caloriesExpendedByPeriod;
@property (strong, nonatomic) NSCalendar *calendar;
@property (assign, nonatomic) NSUInteger customScopeValue;

- (IBAction)scopeChanged:(id)sender;
@end

@implementation BCNutritionDetailViewController

enum { ScopeWeek = 0, ScopeMonth, ScopeYear, ScopeCustom, ScopeConfigure };

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:(NSCoder *)aDecoder];
	if (self) {
		self.scopeIndex = ScopeWeek;

		self.dateFormatter = [[NSDateFormatter alloc] init];
		[self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];

		self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];

	self.customScopeValue = [[User currentUser].userProfile.customNutritionTimeframe integerValue];
	[self.scopeControl setTitle:[NSString stringWithFormat:@"%lu days", (long)self.customScopeValue] forSegmentAtIndex:ScopeCustom];
	[self.scopeControl setSelectedSegmentIndex:self.scopeIndex];

	if (![self.nutrientUnits length]) {
		self.navigationItem.title = self.nutrientDisplayName;
	}
	else {
		self.navigationItem.title = [NSString stringWithFormat:@"%@ (%@)", self.nutrientDisplayName, self.nutrientUnits];
	}

	[self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload 
{
	[self setGraphView:nil];
	[self setTableView:nil];
	[self setScopeControl:nil];
	[self setNoEntriesImageView:nil];

	[super viewDidUnload];
}

#pragma mark - local
- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
	NSDictionary *day = [self.nutrientLogsByDay objectAtIndex:indexPath.row];

	UILabel *valueLabel = (UILabel *)[cell.contentView viewWithTag:1];
	UILabel *unitsLabel = (UILabel *)[cell.contentView viewWithTag:2];
	UILabel *dateLabel = (UILabel *)[cell.contentView viewWithTag:3];
	dateLabel.text = [self.dateFormatter stringFromDate:[day objectForKey:@"date"]];

	valueLabel.text = [NSString stringWithFormat:@"%ld", (long)([[day objectForKey:@"value"] doubleValue] + 0.5)];
	unitsLabel.text = self.nutrientUnits;

	// resize and position the value and units
	CGSize valueTextSize = [valueLabel.text sizeWithFont:valueLabel.font];
	CGSize unitsTextSize = [unitsLabel.text sizeWithFont:unitsLabel.font];
	CGRect unitsFrame = unitsLabel.frame;
	valueLabel.frame = CGRectMake(valueLabel.frame.origin.x, valueLabel.frame.origin.y, valueTextSize.width, valueLabel.frame.size.height);
	unitsLabel.frame = CGRectMake(valueLabel.frame.origin.x + valueTextSize.width + 2, unitsFrame.origin.y,
		unitsTextSize.width, unitsFrame.size.height);
}

- (IBAction)scopeChanged:(id)sender
{
	NSUInteger scopeIndex = [sender selectedSegmentIndex];
	if (scopeIndex == ScopeConfigure) {
		BLAlertController* alert = [BLAlertController alertControllerWithTitle:@"Custom range" message:@"Please enter a range, in days"
			preferredStyle:UIAlertControllerStyleAlert];

		[alert addAction:[BLAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
			handler:^(UIAlertAction * action) {
				// Re-select the prior scope index, since they canceled
				[self.scopeControl setSelectedSegmentIndex:self.scopeIndex];
			}]];

		[alert addAction:[BLAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
			handler:^(UIAlertAction * action) {
				NSNumber *value = [[[alert.textFields objectAtIndex:0] text] numberValueDecimal];
				self.customScopeValue = MIN([value integerValue], 365);
				UserProfileMO *userProfile = [User currentUser].userProfile;
				userProfile.customNutritionTimeframe = @(self.customScopeValue);
				userProfile.status = kStatusPut;
				[userProfile.managedObjectContext BL_save];
				// Post a notification that we've updated the user profile
				[[NSNotificationCenter defaultCenter] postNotificationName:[UserProfileMO entityName]
					object:self userInfo:nil];
				self.scopeIndex = ScopeCustom;
				[self.scopeControl setTitle:[NSString stringWithFormat:@"%lu days", (long)self.customScopeValue]
					forSegmentAtIndex:ScopeCustom];
				[self reloadLogs];

				[self.scopeControl setSelectedSegmentIndex:self.scopeIndex];
			}]];

		[alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
			textField.text = [NSString stringWithFormat:@"%lu", (long)self.customScopeValue];
			textField.placeholder = @"days";
			textField.keyboardType = UIKeyboardTypeNumberPad;
		}];

		[alert presentInViewController:self animated:YES completion:nil];
	}
	else {
		self.scopeIndex = scopeIndex;
		[self reloadLogs];
	}
}

- (void)reloadLogs
{
	self.nutrientLogsByDay = nil;
	self.nutrientLogsByMonth = nil;
	self.caloriesExpendedByPeriod = nil;

	[self.tableView reloadData];

	[self configureView];
}

- (void)configureView
{
	if (self.scopeIndex != ScopeYear) {
		self.noEntriesImageView.hidden = [self.nutrientLogsByDay count];
	}
	else {
		self.noEntriesImageView.hidden = [self.nutrientLogsByMonth count];
	}

	[self buildGraph];
}

- (void)buildGraph
{
	NSDate *leftDate = [NSDate date];
	NSDate *middleDate = [NSDate date];
	NSInteger maxPointsToShow = 1;
	switch (self.scopeIndex) {
		case ScopeWeek:
			leftDate = [self.calendar BC_dateByAddingDays:-6 toDate:leftDate];
			middleDate = [self.calendar BC_dateByAddingDays:3 toDate:leftDate];
			maxPointsToShow = 7;
			break;
		case ScopeMonth:
			leftDate = [self.calendar BC_dateByAddingDays:-29 toDate:leftDate];
			middleDate = [self.calendar BC_dateByAddingDays:15 toDate:leftDate];
			maxPointsToShow = 30;
			break;
		case ScopeYear:
			leftDate = [self.calendar BC_dateByAddingYears:-1 toDate:leftDate];
			middleDate = [self.calendar BC_dateByAddingDays:180 toDate:leftDate];
			maxPointsToShow = 12;
			break;
		case ScopeCustom:
			leftDate = [self.calendar BC_dateByAddingDays:-1 * (self.customScopeValue - 1) toDate:leftDate];
			middleDate = [self.calendar BC_dateByAddingDays:(NSUInteger)(self.customScopeValue / 2) toDate:leftDate];
			maxPointsToShow = self.customScopeValue;
			break;
	}

	NSArray *logs = nil;
	if (self.scopeIndex != ScopeYear) {
		logs = self.nutrientLogsByDay;
	}
	else {
		logs = self.nutrientLogsByMonth;
	}

	//
	// Begin - copied from BCDashboardViewController
	//

	// Figure out the min, max, and range
	// Start out by setting the min and max values to the goal min and max
	NSNumber *goalMin = [self.goalInfo.minNutrition valueForKey:self.nutrientName];
	NSNumber *goalMax = [self.goalInfo.maxNutrition valueForKey:self.nutrientName];

	// Set min and max based upon values logged
	double minValue = [[logs valueForKeyPath:@"@min.value.doubleValue"] doubleValue];
	double maxValue = [[logs valueForKeyPath:@"@max.value.doubleValue"] doubleValue];

	// Special handling for calories, as there are other factors in play, such as BMR, and calorie burn, etc
	if ([self.nutrientName isEqualToString:NutritionMOAttributes.calories]) {
		// Adjust the min and max for calorie burn too
		minValue = MIN(minValue, [[self.caloriesExpendedByPeriod valueForKeyPath:@"@min.doubleValue"] doubleValue]);
		// Calorie burn is tacked onto bmr, so adjust the max to reflect that
		double maxBurn = [self.goalInfo.bmr doubleValue]
			+ [[self.caloriesExpendedByPeriod valueForKeyPath:@"@max.doubleValue"] doubleValue];
		maxValue = MAX(maxValue, maxBurn);

		// Alter goals if the bmrGoal is set, as it can override a normal calorie goal range
		if (self.goalInfo.usingBMR) {
			// Adjust the goal min/max based upon weight gain/loss
			if (self.goalInfo.isWeightLoss) {
				goalMin = nil;
				goalMax = self.goalInfo.bmrGoal;
			}
			else {
				goalMin = self.goalInfo.bmrGoal;
				goalMax = nil;
			}
		}
	}

	// Adjust minValue and maxValue to accommodate for goals
	minValue = (goalMin ? MIN(minValue, [goalMin doubleValue]) : minValue);
	minValue = (goalMax ? MIN(minValue, [goalMax doubleValue]) : minValue);
	maxValue = (goalMin ? MAX(maxValue, [goalMin doubleValue]) : maxValue);
	maxValue = (goalMax ? MAX(maxValue, [goalMax doubleValue]) : maxValue);

	if (maxValue == 0) {
		maxValue = 100;
	}
	else if (maxValue > 1000) {
		maxValue = ceil(maxValue / 100) * 100;
	}
	else if (maxValue > 10) {
		maxValue = ceil(maxValue / 10) * 10;
	}

	double range = maxValue - minValue;

	NSInteger circleRadius = (maxPointsToShow > 12 ? 2 : 4);
	double circleLineWidth = 1.5;
	NSInteger graphWidth = 320; // Difference from Dashboard
	double graphLeftPad = 40;
	NSInteger graphHeight = 190; // Difference from Dashboard
	NSInteger topOffset = graphHeight / 7; // Difference from Dashboard
	NSInteger maxGraphHeight = graphHeight * 5 / 7;
	NSInteger xIndex = 0;
	double xLineStart = 35;

	// Total width / gaps between points
	double xStep = (graphWidth - graphLeftPad - ((2.0 * circleRadius) + (2.0 * circleLineWidth))) / (maxPointsToShow - 1);

	NSMutableArray *dataSets = [NSMutableArray array];
	NSMutableArray *dataPoints = [NSMutableArray array];
	NSMutableArray *goalPoints = [NSMutableArray array];
	for (NSDictionary *logDict in logs) { // Difference from Dashboard
		[dataPoints addObject:[NSValue valueWithCGPoint:CGPointMake(
			graphWidth - (xIndex++ * xStep + (circleRadius + circleLineWidth)),
			maxGraphHeight * (maxValue - [[logDict objectForKey:@"value"] doubleValue]) / range + topOffset)]];
	}
	[dataSets addObject:dataPoints];

	double maxGoalY = 0.0f;
	double minGoalY = 0.0f;

	if ([self.nutrientName isEqualToString:NutritionMOAttributes.calories]) {
		xIndex = 0;
		NSMutableArray *dataPoints2 = [NSMutableArray array];
		UserProfileMO *userProfile = [User currentUser].userProfile;
		for (NSNumber *value in self.caloriesExpendedByPeriod) {
			double xValue = graphWidth - (xIndex++ * xStep + (circleRadius + circleLineWidth));
			[dataPoints2 addObject:[NSValue valueWithCGPoint:CGPointMake(xValue,
				maxGraphHeight * (maxValue - ([value doubleValue] + [self.goalInfo.bmr doubleValue]))
				/ range + topOffset)]];

			// Add in the point for the goal line, if we're using BMR, as this line will not necessarily be straight, due to activities,
			// and will track parallel to the burn line, with the gap between determined by the nutrition effort
			// Also factor in weight gain/loss by adding/subtracting nutrition effort!
			if (self.goalInfo.usingBMR) {
				[goalPoints addObject:[NSValue valueWithCGPoint:CGPointMake(xValue,
					maxGraphHeight * (maxValue - ([value doubleValue] + [self.goalInfo.bmr doubleValue]
					+ ((self.goalInfo.isWeightLoss ? -1 : 1) * [userProfile.nutritionEffort doubleValue]))) / range + topOffset)]];
			}
		}
		[dataSets addObject:dataPoints2];
	}
	if (goalMin) {
		double goalMinValue = [goalMin doubleValue];
		minGoalY = (maxGraphHeight * (maxValue - goalMinValue) / range) + topOffset;
	}

	if (goalMax) {
		double goalMaxValue = [goalMax doubleValue];
		maxGoalY = (maxGraphHeight * (maxValue - goalMaxValue) / range) + topOffset;
	}
	//
	// End - copied from BCDashboardViewController
	//

	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[numberFormatter setMinimumFractionDigits:0];
	if (range > 10) {
		[numberFormatter setMaximumFractionDigits:0];
	}
	else {
		[numberFormatter setMaximumFractionDigits:1];
	}

	self.graphView.drawBlock = ^(UIView* v, CGContextRef context) {
		NSArray *colors = @[
			[UIColor BL_graphGreen],
			[UIColor BL_graphBlue],
		];

		/*
		 * draw background gradient
		 */
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGFloat gradientColors[] = { 
			246/255.0, 249/255.0, 253/255.0, 1.0, 
			232/255.0, 240/255.0, 251/255.0, 1.0
		};
		CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, gradientColors, NULL, 2);
		CGColorSpaceRelease(colorSpace);
		colorSpace = NULL;
		CGPoint startPoint = CGPointMake(CGRectGetMidX(self.graphView.frame), CGRectGetMinY(self.graphView.frame));
		CGPoint endPoint = CGPointMake(CGRectGetMidX(self.graphView.frame), CGRectGetMaxY(self.graphView.frame));
		CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
		CGGradientRelease(gradient);
		gradient = NULL;

		/* 
		 * Draw the line at the bottom
		 */
		CGContextSetLineWidth(context, 1.0);
		CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
		CGContextBeginPath(context);
		CGContextMoveToPoint(context, CGRectGetMinX(self.graphView.frame), CGRectGetMaxY(self.graphView.frame));
		CGContextAddLineToPoint(context, CGRectGetMaxX(self.graphView.frame), CGRectGetMaxY(self.graphView.frame));
		CGContextDrawPath(context, kCGPathStroke);


		/*
		 * draw horizontal lines
		 */
		CGContextSetLineWidth(context, 1.0);
		CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
		for (NSInteger lineIndex = 1; lineIndex < 7; lineIndex++) {
			CGContextBeginPath(context);
			CGPoint firstPoint = CGPointMake(xLineStart, graphHeight / 7 * lineIndex);
			CGPoint lastPoint = CGPointMake(graphWidth, graphHeight / 7 * lineIndex);
			CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
			CGContextAddLineToPoint(context, lastPoint.x, lastPoint.y);
			CGContextDrawPath(context, kCGPathStroke);
		}

		/* 
		 * draw y axis labels
		 */
		CGContextSelectFont(context, "Helvetica Neue Bold", 10, kCGEncodingMacRoman);
		CGContextSetTextDrawingMode(context, kCGTextFill);
		CGContextSetFillColorWithColor(context, [[UIColor darkGrayColor] CGColor]);
		CGContextSetTextMatrix(context, CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0));
		UIFont *labelFont = [UIFont boldSystemFontOfSize:10];

		NSNumber *theNumber = nil;
		NSString *theText = nil;
		CGSize labelSize;
		for (NSInteger textIndex = 0; textIndex < 6; textIndex++) {
			if (minValue == maxValue) {
				theNumber = @(maxValue * 0.95f + (maxValue * 0.02f * textIndex));
			}
			else {
				theNumber = @(maxValue + (minValue - maxValue) / 5 * textIndex);
			}
			theText = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:theNumber]];
			CGSize labelSize = [theText sizeWithFont:labelFont];
			CGContextShowTextAtPoint(context, 6, (graphHeight / 7 * (textIndex + 1)) + (labelSize.height / 3),
				[theText cStringUsingEncoding:NSUTF8StringEncoding], [theText length]);
		}

		/*
		 * draw x axis labels
		 */
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[dateFormatter setDateFormat:@"EEE, MM/dd/yyyy"];
		theText = [dateFormatter stringFromDate:leftDate];
		labelSize = [theText sizeWithFont:labelFont];
		CGContextShowTextAtPoint(context, 40, graphHeight - labelSize.height, [theText cStringUsingEncoding:NSUTF8StringEncoding],
			[theText length]);
		theText = [dateFormatter stringFromDate:middleDate];
		labelSize = [theText sizeWithFont:labelFont];
		CGContextShowTextAtPoint(context, graphWidth / 2 - labelSize.width / 2 + 20, graphHeight - labelSize.height,
			[theText cStringUsingEncoding:NSUTF8StringEncoding], [theText length]);
		theText = [dateFormatter stringFromDate:[NSDate date]];
		labelSize = [theText sizeWithFont:labelFont];
		CGContextShowTextAtPoint(context, graphWidth - (labelSize.width + 4), graphHeight - labelSize.height,
			[theText cStringUsingEncoding:NSUTF8StringEncoding], [theText length]);

		/*
		 * shade-fill the goal
		 */
		/*
		double shadeTop = (goalMax ? maxGoalY : topOffset);
		double shadeBottom = (goalMin ? minGoalY : graphHeight - (graphHeight / 7));
		if (goalMin || goalMax) {
			// Shade between the goal min and max
			CGContextSetFillColorWithColor(context, [[UIColor BL_goalRangeColor] CGColor]);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, xLineStart, shadeTop);
			CGContextAddLineToPoint(context, graphWidth, shadeTop);
			CGContextAddLineToPoint(context, graphWidth, shadeBottom);
			CGContextAddLineToPoint(context, xLineStart, shadeBottom);
			CGContextClosePath(context);
			CGContextDrawPath(context, kCGPathFill);
		}
		*/

		// Draw the goal line
		CGFloat dashLengths[] = {10, 10};
		CGContextSetLineDash(context, 0, dashLengths, 2);
		CGContextSetLineWidth(context, 2.0);
		CGContextSetStrokeColorWithColor(context, [[UIColor BL_graphGoal] CGColor]);
		CGContextBeginPath(context);
		if ([goalPoints count]) {
			CGPoint firstPoint = [[goalPoints objectAtIndex:0] CGPointValue];
			CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
			for (int pointIndex = 1; pointIndex < [goalPoints count]; pointIndex++) {
				CGPoint destinationPoint = [[goalPoints objectAtIndex:pointIndex] CGPointValue];
				CGPoint previousPoint = [[goalPoints objectAtIndex:pointIndex - 1] CGPointValue];
				if (previousPoint.y == destinationPoint.y) {
					CGContextAddLineToPoint(context, destinationPoint.x, destinationPoint.y);
				}
				else {
					CGPoint controlPoint1 = CGPointMake(previousPoint.x - xStep * 0.50, previousPoint.y);
					CGPoint controlPoint2 = CGPointMake(destinationPoint.x + xStep * 0.50, destinationPoint.y);
					CGContextAddCurveToPoint(context, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y,
						destinationPoint.x, destinationPoint.y);
				}
			}
			CGContextDrawPath(context, kCGPathStroke);
		}
		else if (goalMin || goalMax) {
			if (goalMin) {
				CGContextBeginPath(context);
				CGContextMoveToPoint(context, xLineStart, minGoalY);
				CGContextAddLineToPoint(context, graphWidth, minGoalY);
				CGContextDrawPath(context, kCGPathStroke);
			}
			if (goalMax) {
				CGContextBeginPath(context);
				CGContextMoveToPoint(context, xLineStart, maxGoalY);
				CGContextAddLineToPoint(context, graphWidth, maxGoalY);
				CGContextDrawPath(context, kCGPathStroke);
			}
		}
		CGContextSetLineDash(context, 0, nil, 0);

		for (NSInteger setIndex = 0; setIndex < [dataSets count]; setIndex++) {
			NSArray *dataPoints = [dataSets objectAtIndex:setIndex];

			CGPoint firstPoint = [[dataPoints objectAtIndex:0] CGPointValue];

			/*
			 * Draw the data line
			 */
			CGContextSetLineWidth(context, 2.0);
			CGContextSetStrokeColorWithColor(context, [[colors objectAtIndex:setIndex] CGColor]);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
			for (int pointIndex = 1; pointIndex < [dataPoints count]; pointIndex++) {
				CGPoint destinationPoint = [[dataPoints objectAtIndex:pointIndex] CGPointValue];
				CGPoint previousPoint = [[dataPoints objectAtIndex:pointIndex - 1] CGPointValue];
				if (previousPoint.y == destinationPoint.y) {
					CGContextAddLineToPoint(context, destinationPoint.x, destinationPoint.y);
				}
				else {
					CGPoint controlPoint1 = CGPointMake(previousPoint.x - xStep * 0.50, previousPoint.y);
					CGPoint controlPoint2 = CGPointMake(destinationPoint.x + xStep * 0.50, destinationPoint.y);
					CGContextAddCurveToPoint(context, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y,
						destinationPoint.x, destinationPoint.y);
				}
			}
			CGContextDrawPath(context, kCGPathStroke);

			// Don't draw the circles if they would be too close together
			if ([dataPoints count] <= 30) {
				/*
				 * add the circles at the data points
				 */
				CGRect rect;
				CGContextBeginPath(context);
				CGContextSetLineWidth(context, circleLineWidth);
				CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
				for (int pointIndex = 0; pointIndex < [dataPoints count]; pointIndex++) {
					CGPoint point = [[dataPoints objectAtIndex:pointIndex] CGPointValue];
					rect = CGRectMake(point.x - circleRadius, point.y - circleRadius, 2 * circleRadius, 2 * circleRadius);
					CGContextAddEllipseInRect(context, rect);
				}
				CGContextDrawPath(context, kCGPathFillStroke);

				/*
				 * the last circle should be filled
				 */
				rect = CGRectMake(firstPoint.x - circleRadius, firstPoint.y - circleRadius, 2 * circleRadius, 2 * circleRadius);
				CGContextSetFillColorWithColor(context, [[colors objectAtIndex:setIndex] CGColor]);
				CGContextBeginPath(context);
				CGContextAddEllipseInRect(context, rect);
				CGContextDrawPath(context, kCGPathFillStroke);
			}
		}
	};
	[self.graphView setNeedsDisplay];
}

#pragma mark - fetch
- (NSArray *)nutrientLogsByDay
{
	if (_nutrientLogsByDay != nil) {
		return _nutrientLogsByDay;
	}

	User *thisUser = [User currentUser];

	NSMutableArray *tempArray = [NSMutableArray array];

	// sum the nutrition for each day
	NSInteger numberOfDays = 0;
	switch (self.scopeIndex) {
		case ScopeWeek:
			numberOfDays = 7;
			break;
		case ScopeMonth:
			numberOfDays = 30;
			break;
		case ScopeCustom:
			numberOfDays = self.customScopeValue;
			break;
	}

	for (NSInteger dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
		NSDate *date = [self.calendar BC_dateByAddingDays:-1 * dayIndex toDate:[NSDate date]];
		NSArray *foodLogs = [FoodLogMO MR_findAllWithPredicate:
			[NSPredicate predicateWithFormat:@"date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
			[self.calendar BC_startOfDate:date], [self.calendar BC_endOfDate:date], kStatusDelete, thisUser.loginId]
			inContext:self.managedObjectContext];
		double sum = 0.0f;
		for (FoodLogMO *foodLog in foodLogs) {
			sum += [[foodLog.nutrition valueForKey:self.nutrientName] doubleValue] * [foodLog.nutritionFactor doubleValue]
				* [foodLog.servings doubleValue];
		}
		[tempArray addObject:@{@"date":date,@"value":@(sum)}];
	}

	self.nutrientLogsByDay = tempArray;

	return _nutrientLogsByDay;
}

- (NSArray *)nutrientLogsByMonth
{
	if (_nutrientLogsByMonth != nil) {
		return _nutrientLogsByMonth;
	}

	NSDate *today = [NSDate date];

	NSMutableArray *tempArray = [NSMutableArray array];
	for (NSInteger index = 0; index < 12; index++) {
		[tempArray addObject:@{@"date":today,@"value":@0}];
	}

	NSInteger month = 0;
	float monthSum = 0.0f;
	NSInteger daysWithEntries = 0;

	// loop thru each food log entry
	// NOTE entries are only loaded once at this time
	for (FoodLogMO *foodLog in self.foodLogEntries) {
		// is this in the same month?
		NSInteger monthDiff = [[self.calendar components:NSMonthCalendarUnit fromDate:foodLog.date toDate:today options:0] month];
		if (month != monthDiff) {
			// avoid dividing by zero
			if (daysWithEntries > 0) {
				[tempArray replaceObjectAtIndex:month withObject:@{@"date":today,@"value":[NSNumber numberWithFloat:monthSum / daysWithEntries]}];
			}
			// reset the counters
			month = monthDiff;
			monthSum = 0.0f;
			daysWithEntries = 0;
		}

		daysWithEntries++;

		// TODO FIXME: How is this being factored in when it is being calculated AFTER the monthSum is added to the array above????????
		// At the very least, it looks like the last iteration will be missing a value
		//
		// add this to the month
		monthSum += [[foodLog.nutrition valueForKey:self.nutrientName] doubleValue]
			* [foodLog.nutritionFactor doubleValue] * [foodLog.servings doubleValue];
	}

	self.nutrientLogsByMonth = tempArray;

	return _nutrientLogsByMonth;
}

- (NSArray *)foodLogEntries
{
	if (_foodLogEntries != nil) {
		return _foodLogEntries;
	}

	User *thisUser = [User currentUser];

	// query all the entries for the last year
	NSDate *startDate = [self.calendar BC_dateByAddingDays:-1 toDate:[NSDate date]];
	self.foodLogEntries = [FoodLogMO MR_findAllSortedBy:FoodLogMOAttributes.date ascending:NO 
		withPredicate:[NSPredicate predicateWithFormat:@"date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
			startDate, [NSDate date], kStatusDelete, thisUser.loginId] 
		inContext:self.managedObjectContext];

	return _foodLogEntries;
}

- (NSArray *)caloriesExpendedByPeriod
{
	if (_caloriesExpendedByPeriod != nil) {
		return _caloriesExpendedByPeriod;
	}

	User *thisUser = [User currentUser];

	NSMutableArray *tempArray = [NSMutableArray array];

	if (self.scopeIndex != ScopeYear) {
		NSInteger numberOfDays = 0;
		switch (self.scopeIndex) {
			case ScopeWeek:
				numberOfDays = 7;
				break;
			case ScopeMonth:
				numberOfDays = 30;
				break;
			case ScopeCustom:
				numberOfDays = self.customScopeValue;
				break;
		}
		for (NSInteger dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
			NSDate *date = [self.calendar BC_dateByAddingDays:-1 * dayIndex toDate:[NSDate date]];
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
				[self.calendar BC_startOfDate:date], [self.calendar BC_endOfDate:date], kStatusDelete, thisUser.loginId];

			NSNumber *activityCalories = [TrainingActivityMO MR_aggregateOperation:@"sum:" onAttribute:TrainingActivityMOAttributes.calories
				withPredicate:predicate inContext:self.managedObjectContext];

			[tempArray addObject:activityCalories];
		}
	}
	else {
		for (NSInteger monthOffset = 0; monthOffset > -12; monthOffset--) {
			NSDate *date = [self.calendar BC_dateByAddingMonths:monthOffset toDate:[NSDate date]];
			NSDate *firstOfMonth = [self.calendar BC_firstOfMonthForDate:date];
			NSDate *lastOfMonth = [self.calendar BC_lastOfMonthForDate:date];
			NSInteger daysInMonth = [self.calendar BC_daysFromDate:firstOfMonth toDate:lastOfMonth];
			NSInteger daysWithEntries = 0;
			float monthSum = 0.0f;
			for (NSInteger dayIndex = 0; dayIndex < daysInMonth; dayIndex++) {
				NSDate *date = [self.calendar BC_dateByAddingDays:dayIndex toDate:firstOfMonth];
				NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date BETWEEN {%@, %@} AND status <> %@ and loginId = %@",
					[self.calendar BC_startOfDate:date], [self.calendar BC_endOfDate:date], kStatusDelete, thisUser.loginId];

				NSNumber *activityCalories = [TrainingActivityMO MR_aggregateOperation:@"sum:" onAttribute:TrainingActivityMOAttributes.calories
					withPredicate:predicate inContext:self.managedObjectContext];
				NSNumber *cardioCalories = [TrainingActivityMO MR_aggregateOperation:@"sum:" onAttribute:TrainingActivityMOAttributes.calories
					withPredicate:predicate inContext:self.managedObjectContext];

				NSNumber *daySum = @([activityCalories integerValue] + [cardioCalories integerValue]);

				if ([daySum floatValue] > 0) {
					daysWithEntries++;
					monthSum += [daySum floatValue];
				}
			}
			if (monthSum > 0) {
				[tempArray addObject:[NSNumber numberWithFloat:monthSum / daysWithEntries]];
			}
			else {
				[tempArray addObject:@0];
			}
		}
	}

	self.caloriesExpendedByPeriod = tempArray;

	return _caloriesExpendedByPeriod;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	numberOfRows = [self.nutrientLogsByDay count];
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *normalCellId = @"NormalCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:normalCellId];
    
    [self configureCell:cell inTableView:tableView atIndexPath:indexPath];
    
    return cell;
}

@end
