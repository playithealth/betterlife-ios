//
//  NSDate+Additions.h
//  BettrLife
//
//  Created by Sef Tarbell on 8/23/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const struct BCDateWeekDays {
	NSInteger sunday;
	NSInteger monday;
	NSInteger tuesday;
	NSInteger wednesday;
	NSInteger thursday;
	NSInteger friday;
	NSInteger saturday;
} BCDateWeekDays;

@interface NSDate (Additions)

- (BOOL)BC_isInSameMonthAs:(NSDate *)monthDate;
- (BOOL)BC_isInSameWeekAs:(NSDate *)monthDate;
- (BOOL)BC_isSameDayAs:(NSDate *)anotherDate;

- (NSString *)BC_monthString;
- (NSString *)BC_monthYearString;
- (NSString *)BC_dayString;
- (NSString *)BC_yearString;

@end
