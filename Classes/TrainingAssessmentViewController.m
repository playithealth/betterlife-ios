//
//  TrainingAssessmentViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/22/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "TrainingAssessmentViewController.h"

#import "BCPopupView.h"
#import "BCSegmentedControlCell.h"
#import "BCTextFieldCell.h"
#import "GenericValueDisplay.h"
#import "NSArray+NestedArrays.h"
#import "NumberValue.h"
#import "UIColor+Additions.h"
#import "UserProfileMO.h"

//static const NSInteger kTagGenderControl = 10003;
static const NSInteger kTagActivityLevelPopupView = 10004;
static const NSInteger kTagHeightPopupView = 10005;

static const NSInteger kScrollInset = 300;

static const NSInteger kSegmentMale = 0;
static const NSInteger kSegmentFemale = 1;

static const NSInteger kValueMale = 2;
static const NSInteger kValueFemale = 1;

@interface TrainingAssessmentViewController () <BCPopupViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) UIToolbar *keyboardToolbar;
@property (strong, nonatomic) NSArray *sectionNames;
@property (strong, nonatomic) NSArray *rowLabels;
@property (strong, nonatomic) NSArray *rowKeys;
@property (assign, nonatomic) BOOL changedLog;
@property (strong, nonatomic) UITextField *editingTextField;
@property (strong, nonatomic) NSIndexPath *editingIndexPath;
@property (strong, nonatomic) NSMutableArray *cellsBySection;

- (void)keyboardDidShowNotification:(NSNotification*)aNotification;
- (void)keyboardWillHideNotification:(NSNotification*)aNotification;
- (void)configureCell:(id)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)saveTextFieldValue:(UITextField *)textField;
- (void)genderChanged:(id)sender;
@end

@implementation TrainingAssessmentViewController
@synthesize tableView;
@synthesize keyboardToolbar;
@synthesize trainingAssessment;
@synthesize delegate;
@synthesize sectionNames;
@synthesize rowLabels;
@synthesize rowKeys;
@synthesize changedLog;
@synthesize editingTextField;
@synthesize editingIndexPath;
@synthesize cellsBySection;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.sectionNames = [NSArray arrayWithObjects:
			@"Measurements",
			@"Activity",
			nil];

		self.rowLabels = [NSArray arrayWithObjects:
			[NSArray arrayWithObjects:
				@"Sex:",
				@"Age:",
				@"Height:",
				@"Weight:",
				@"Body Fat %:",
				@"Neck:",
				@"Chest:",
				@"Waist:",
				@"Stomach:",
				@"Hips:",
				@"Left Bicep:",
				@"Right Bicep:",
				@"Left Thigh:",
				@"Right Thigh:",
				nil],
			[NSArray arrayWithObjects:
				@"Activity Level:",
				@"Crunches:",
				@"Push-ups:",
				@"Pull-ups:",
				@"Squats:",
				@"Mile run:",
				nil],
			nil];

		self.rowKeys = [NSArray arrayWithObjects:
			[NSArray arrayWithObjects:
				TrainingAssessmentMOAttributes.sex,
				TrainingAssessmentMOAttributes.age,
				TrainingAssessmentMOAttributes.height,
				TrainingAssessmentMOAttributes.weight,
				TrainingAssessmentMOAttributes.bodyFatPercent,
				TrainingAssessmentMOAttributes.neck,
				TrainingAssessmentMOAttributes.chest,
				TrainingAssessmentMOAttributes.stomach,
				TrainingAssessmentMOAttributes.waist,
				TrainingAssessmentMOAttributes.hips,
				TrainingAssessmentMOAttributes.leftBicep,
				TrainingAssessmentMOAttributes.rightBicep,
				TrainingAssessmentMOAttributes.leftThigh,
				TrainingAssessmentMOAttributes.rightThigh,
				nil],
			[NSArray arrayWithObjects:
				TrainingAssessmentMOAttributes.activityLevel,
				TrainingAssessmentMOAttributes.crunches,
				TrainingAssessmentMOAttributes.pushups,
				TrainingAssessmentMOAttributes.pullups,
				TrainingAssessmentMOAttributes.squats,
				TrainingAssessmentMOAttributes.mileRun,
				nil],
			nil];
		
		self.changedLog = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.tableView.backgroundColor = [UIColor colorWithPatternImage:
		[UIImage imageNamed:@"background.png"]];

	if (!keyboardToolbar) {
		keyboardToolbar = [[UIToolbar alloc] initWithFrame:
			CGRectMake(0, 0, self.tableView.bounds.size.width, 35)];
		keyboardToolbar.barStyle = UIBarStyleBlack;
		keyboardToolbar.translucent = YES;

		UIBarButtonItem *previousButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Previous"
			style:UIBarButtonItemStyleBordered
			target:self action:@selector(previousEditableField:)];

		UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Next"
			style:UIBarButtonItemStyleBordered
			target:self action:@selector(nextEditableField:)];

		UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
			initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
			target:nil action:nil];

		UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Done" style:UIBarButtonItemStyleDone
			target:self action:@selector(resignKeyboard:)];

		NSArray *buttons = [NSArray arrayWithObjects:
			previousButton, nextButton, flexibleSpace, doneButton, nil];

		keyboardToolbar.items = buttons;
	}

	// create the cells based off of what is in the sectionNames and rowLabels array
	NSUInteger section = 0;
	NSUInteger row = 0;
	self.cellsBySection = [NSMutableArray arrayWithCapacity:[self.sectionNames count]];
	for (section = 0; section < [self.sectionNames count]; section++) {
		NSArray *labels = [self.rowLabels objectAtIndex:section];
		NSMutableArray *cellsForThisSection = [NSMutableArray arrayWithCapacity:[labels count]];
		for (row = 0; row < [labels count]; row++) {
			// TODO when the number of sets becomes dynamic, maybe we should have a reuseIdentifier
			// the first row is an exception, we need a segmented control here
			if (section == 0 && row == 0) {
				BCSegmentedControlCell *genderCell = [[BCSegmentedControlCell alloc] 
					initWithItems:[NSArray arrayWithObjects:@"Male", @"Female", nil]
					reuseIdentifier:nil];
				[genderCell.segmentedControl addTarget:self action:@selector(genderChanged:)
					forControlEvents:UIControlEventValueChanged];
				genderCell.bcTextLabel.text = [labels objectAtIndex:row];
				[cellsForThisSection addObject:genderCell];
			}
			else {
				BCTextFieldCell *aCell = [[BCTextFieldCell alloc] initWithReuseIdentifier:nil];

				aCell.bcTextField.inputAccessoryView = keyboardToolbar;
				aCell.bcTextField.delegate = self;
				aCell.bcTextField.enabled = NO;
				[aCell.bcTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
				[aCell.bcTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
				[aCell.bcTextField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];

				aCell.bcTextLabel.text = [labels objectAtIndex:row];
				[cellsForThisSection addObject:aCell];
			}
		}
		[self.cellsBySection addObject:cellsForThisSection];
	}
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

	self.title = @"Assessment Training";

	// Register for notification when the keyboard will be shown
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardDidShowNotification:)
		name:UIKeyboardDidShowNotification
		object:nil];

	// Register for notification when the keyboard will be hidden
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardWillHideNotification:)
		name:UIKeyboardWillHideNotification
		object:nil];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillDisappear:(BOOL)animated
{
	if (self.editingTextField != nil) {
		[self saveTextFieldValue:self.editingTextField];
		self.editingTextField = nil;
	}
	
	// notify delegate
	if (self.changedLog) {
		if (delegate && [delegate conformsToProtocol:@protocol(TrainingAssessmentViewDelegate)]) {
			[delegate trainingAssessmentView:self didUpdateLog:self.trainingAssessment];
		}
	}

	self.changedLog = NO;

	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:UIKeyboardWillHideNotification object:nil];

    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.cellsBySection count];
}

- (NSInteger)tableView:(UITableView *)tableView 
numberOfRowsInSection:(NSInteger)section
{
    return [self.cellsBySection BC_countOfNestedArrayAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = [self.cellsBySection BC_nestedObjectAtIndexPath:indexPath];
    
	// configure cell
	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (void)configureCell:(id)cell atIndexPath:(NSIndexPath *)indexPath
{
	if ([cell isKindOfClass:[BCTextFieldCell class]]) {
		BCTextFieldCell *aTextFieldCell = cell;

		// special case for the activity level
		if (indexPath.section == 1 && indexPath.row == 0) {
			float activityLevel = [self.trainingAssessment.activityLevel floatValue];
			if (activityLevel == kActivityLevelValueSedentary) {
				aTextFieldCell.bcTextField.text = @"Sedentary";
			}
			else if (activityLevel == kActivityLevelValueLight) {
				aTextFieldCell.bcTextField.text = @"Light";
			}
			else if (activityLevel == kActivityLevelValueModerate) {
				aTextFieldCell.bcTextField.text = @"Moderate";
			}
			else if (activityLevel == kActivityLevelValueActive) {
				aTextFieldCell.bcTextField.text = @"Active";
			}
			else if (activityLevel == kActivityLevelValueVeryActive) {
				aTextFieldCell.bcTextField.text = @"Very Active";
			}
		}
		else if (indexPath.section == 0 && indexPath.row == 2) {
			NSInteger totalInches = [self.trainingAssessment.height integerValue];
			NSInteger feet = totalInches / 12;
			NSInteger inches = totalInches % 12;
			aTextFieldCell.bcTextField.text = [NSString stringWithFormat:@"%ld' %ld\"", (long)feet, (long)inches];
		}
		else {
			id <GenericValueDisplay, NSObject> rowValue = [self.trainingAssessment 
				valueForKey:[self.rowKeys BC_nestedObjectAtIndexPath:indexPath]];
			aTextFieldCell.bcTextField.text = [rowValue genericValueDisplay];
		}
	}
	else if ([cell isKindOfClass:[BCSegmentedControlCell class]]) {
		BCSegmentedControlCell *aSegmentedCell = cell;

		// safety check - only the first row is this way
		if (indexPath.section == 0 && indexPath.row == 0) {
			// select based on the string value
			[aSegmentedCell.segmentedControl setSelectedSegmentIndex:-1];
			if ([self.trainingAssessment.sex integerValue] == kValueMale) {
				[aSegmentedCell.segmentedControl setSelectedSegmentIndex:kSegmentMale];
			}
			else if ([self.trainingAssessment.sex integerValue] == kValueFemale) {
				[aSegmentedCell.segmentedControl setSelectedSegmentIndex:kSegmentFemale];
			}
		}
	}
}

#pragma mark - UITableViewDelegate
- (NSString *)tableView:(UITableView *)tableView 
titleForHeaderInSection:(NSInteger)section 
{
    NSString *title = [self.sectionNames objectAtIndex:section];
    if ([title isKindOfClass:[NSNull class]]) {
        return nil;
	}
	
    return title;
}

- (void)tableView:(UITableView *)tableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.editingIndexPath = indexPath;

	// is this the activity level row
	if (indexPath.section == 1 && indexPath.row == 0) {
		[self resignKeyboard:nil];

		self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, kScrollInset, 0.0);

		[self.tableView scrollToRowAtIndexPath:indexPath
			atScrollPosition:UITableViewScrollPositionMiddle animated:YES];

		BCPopupView *aPopupView = [[BCPopupView alloc] 
			initWithTableViewStyle:UITableViewStylePlain 
			caretPosition:CGPointMake(160, 120)
			title:@"Choose an activity level:"
			delegate:self 
			rowTitles:[NSArray arrayWithObjects:
				[NSDictionary dictionaryWithObjectsAndKeys:
					@"Sedentary", @"labelText", 
					@"little or no exercise", @"detailLabelText",
					nil],
				[NSDictionary dictionaryWithObjectsAndKeys:
					@"Light", @"labelText", 
					@"light exercise or sports 1-3 days/week", @"detailLabelText",
					nil],
				[NSDictionary dictionaryWithObjectsAndKeys:
					@"Moderate", @"labelText", 
					@"moderate exercise or sports 3-5 days/week", @"detailLabelText",
					nil],
				[NSDictionary dictionaryWithObjectsAndKeys:
					@"Active", @"labelText", 
					@"hard exercise or sports 6-7 days/week", @"detailLabelText",
					nil],
				[NSDictionary dictionaryWithObjectsAndKeys:
					@"Very Active", @"labelText", 
					@"very hard exercise or sports and physical job or 2x training", @"detailLabelText",
					nil],
				nil]];
		aPopupView.tag = kTagActivityLevelPopupView;

		[self.navigationController.view addSubview:aPopupView];
	}
	// is this the height row
	else if (indexPath.section == 0 && indexPath.row == 2) {
		[self resignKeyboard:nil];

		self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, kScrollInset, 0.0);

		[self.tableView scrollToRowAtIndexPath:indexPath
			atScrollPosition:UITableViewScrollPositionMiddle animated:YES];

		UIPickerView *aPickerView = [[UIPickerView alloc] init];
		aPickerView.dataSource = self;
		aPickerView.delegate = self;
		aPickerView.showsSelectionIndicator = YES;

		NSInteger totalInches = [self.trainingAssessment.height integerValue];
		NSInteger feet = totalInches / 12;
		NSInteger inches = totalInches % 12;
		[aPickerView selectRow:feet - 4 inComponent:0 animated:NO];
		[aPickerView selectRow:inches - 1 inComponent:1 animated:NO];

		BCPopupView *aPopupView = [[BCPopupView alloc] 
			initWithView:aPickerView
			caretPosition:CGPointMake(160, 120)
			title:@"Choose a height:"
			delegate:self];
		aPopupView.tag = kTagHeightPopupView;

		UIButton *heightDoneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		heightDoneButton.frame = CGRectMake(0, 0, 80, 37);
		[heightDoneButton setTitle:@"Done" forState:UIControlStateNormal];
		[aPopupView addSubview:heightDoneButton];
		heightDoneButton.center = CGPointMake(160, 420);

		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:aPopupView action:@selector(handleDoneButton:)];
		[heightDoneButton addGestureRecognizer:recognizer];

		[self.navigationController.view addSubview:aPopupView];
	}
	else {
		id cell = [self.cellsBySection BC_nestedObjectAtIndexPath:indexPath];
		if ([cell isKindOfClass:[BCTextFieldCell class]]) {
			[cell bcTextField].enabled = YES;
			[[cell bcTextField] becomeFirstResponder];
			self.editingTextField = [cell bcTextField];
		}
	}

	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[self saveTextFieldValue:textField];

	self.editingTextField = nil;
}

#pragma mark - Notifications for keyboard hide/show
- (void)keyboardDidShowNotification:(NSNotification*)aNotification
{
	NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
 
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;

	[self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)keyboardWillHideNotification:(NSNotification*)aNotification 
{
	self.tableView.contentInset = UIEdgeInsetsZero;
	self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}   

#pragma mark - Local Methods
- (void)saveTextFieldValue:(UITextField *)textField
{
	// Get the cell in which the textfield is embedded
	id textFieldSuper = textField;
	while (textFieldSuper && ![textFieldSuper isKindOfClass:[UITableViewCell class]]) {
		textFieldSuper = [textFieldSuper superview];
	}
	NSIndexPath *indexPath = [self.tableView indexPathForCell:textFieldSuper];
	NSString *rowKey = [self.rowKeys BC_nestedObjectAtIndexPath:indexPath];
	[self.trainingAssessment setValue:[textField.text numberValueDecimal] forKey:rowKey];
	textField.enabled = NO;
	self.changedLog = YES;
}

- (IBAction)previousEditableField:(id)sender
{
	NSIndexPath *oldIndexPath = self.editingIndexPath;

	// roll over to the last item 
	NSInteger newSection = oldIndexPath.section;
	NSInteger newRow = oldIndexPath.row - 1;
	if (newRow < 0) {
		newSection = oldIndexPath.section - 1;
		if (newSection < 0) {
			newSection = [self.tableView numberOfSections] - 1;
		}
		newRow = [self.tableView numberOfRowsInSection:newSection] - 1;
	}

	// is this the activity level row
	if (newSection == 1 && newRow == 0) {
		// skip to the last row of the previous section
		newSection = 0;
		newRow = [self.tableView numberOfRowsInSection:newSection] - 1;
	}
	// is this the height row
	else if (newSection == 0 && newRow == 2) {
		// skip to the last row of the previous section
		newRow = 1;
	}

	self.editingIndexPath = [NSIndexPath indexPathForRow:newRow inSection:newSection];

	self.editingTextField.enabled = NO;

    id cell = [self.cellsBySection BC_nestedObjectAtIndexPath:self.editingIndexPath];
	if ([cell isKindOfClass:[BCTextFieldCell class]]) {
		[cell bcTextField].enabled = YES;
		[[cell bcTextField] becomeFirstResponder];
		self.editingTextField = [cell bcTextField];
	}

	[self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (IBAction)nextEditableField:(id)sender
{
	NSIndexPath *oldIndexPath = self.editingIndexPath;

	// roll back to the first row
	NSInteger newSection = oldIndexPath.section;
	NSInteger newRow = oldIndexPath.row + 1;
	if (newRow >= [self.tableView numberOfRowsInSection:newSection]) {
		newSection = oldIndexPath.section + 1;
		if (newSection >= [self.tableView numberOfSections]) {
			newSection = 0;
		}
		newRow = 0;
	}

	// is this the activity level row
	if (newSection == 1 && newRow == 0) {
		// skip to the next row
		newRow = 1;
	}
	// is this the height row
	else if (newSection == 0 && newRow == 2) {
		// skip to the last row of the previous section
		newRow = 3;
	}

	self.editingIndexPath = [NSIndexPath indexPathForRow:newRow inSection:newSection];

	self.editingTextField.enabled = NO;

    id cell = [self.cellsBySection BC_nestedObjectAtIndexPath:self.editingIndexPath];
	if ([cell isKindOfClass:[BCTextFieldCell class]]) {
		[cell bcTextField].enabled = YES;
		[[cell bcTextField] becomeFirstResponder];
		self.editingTextField = [cell bcTextField];
	}

	[self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)resignKeyboard:(id)sender
{
	self.editingTextField.enabled = NO;
	[self.editingTextField resignFirstResponder];
	self.editingTextField = nil;
}

- (void)genderChanged:(id)sender
{
	UISegmentedControl *genderControl = sender;
	if (genderControl.selectedSegmentIndex == kSegmentMale) {
		self.trainingAssessment.sex = [NSNumber numberWithInteger:kValueMale];
	}
	else if (genderControl.selectedSegmentIndex == kSegmentFemale) {
		self.trainingAssessment.sex = [NSNumber numberWithInteger:kValueFemale];
	}
	self.changedLog = YES;
}

- (IBAction)handleDoneButton:(id)sender
{
	
}
#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	if (component == 0) {
		return 4;
	}
	else {
		return 12;
	}
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	if (component == 0) {
		return [NSString stringWithFormat:@"%ld feet", (long)row + 4];
	}
	else {
		return [NSString stringWithFormat:@"%ld inches", (long)row + 1];
	}
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	//QuietLog(@"row %d component %d", row, component);
}

#pragma mark - BCPopupViewDelegate
- (void)popupView:(BCPopupView *)inPopup
didDismissWithSelectedIndexPath:(NSIndexPath *)indexPath
{
	self.tableView.contentInset = UIEdgeInsetsZero;

	// bail if the user cancelled the popup
	if (!indexPath) {
		return;
	}

	NSIndexPath *cellIndexPath = nil;
	if (inPopup.tag == kTagActivityLevelPopupView) {
		float activityLevel = [self.trainingAssessment.activityLevel floatValue];
		float newActivityLevel = 0.0f;
		switch ([indexPath row]) {
			default:
			case ActivityLevelSedentary:
				newActivityLevel = kActivityLevelValueSedentary;
				break;
			case ActivityLevelLight:
				newActivityLevel = kActivityLevelValueLight;
				break;
			case ActivityLevelModerate:
				newActivityLevel = kActivityLevelValueModerate;
				break;
			case ActivityLevelActive:
				newActivityLevel = kActivityLevelValueActive;
				break;
			case ActivityLevelVeryActive:
				newActivityLevel = kActivityLevelValueVeryActive;
				break;
		}

		if (activityLevel != newActivityLevel) {
			self.trainingAssessment.activityLevel = [NSNumber numberWithFloat:newActivityLevel];
			self.changedLog = YES;
		}

		cellIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
	}

	[self configureCell:[self.tableView cellForRowAtIndexPath:cellIndexPath]
		atIndexPath:cellIndexPath];
}

- (void)popupView:(BCPopupView *)inPopup didDismissWithInfo:(NSDictionary *)userInfo
{
	UIPickerView *pickerView = (UIPickerView *)inPopup.customView;
	NSInteger feet = [pickerView selectedRowInComponent:0] + 4;
	NSInteger inches = [pickerView selectedRowInComponent:1] + 1;
	NSInteger totalInches = feet * 12 + inches;
	self.trainingAssessment.height = [NSNumber numberWithInteger:totalInches];

	[self configureCell:[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]]
		atIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
}

@end
