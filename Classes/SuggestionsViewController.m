//
//  SuggestionsViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 12/9/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "SuggestionsViewController.h"

#import "BCColorButton.h"
#import "BCObjectManager.h"
#import "Recommendation.h"
#import "ShoppingListItem.h"
#import "UIColor+Additions.h"
#import "User.h"

@interface SuggestionsViewController () 
<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *emptyImageView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (assign, nonatomic) BOOL needsSync;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)syncSuggestionsDidFinish:(NSNotification *)notification;

@end

@implementation SuggestionsViewController
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize needsSync;

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

	self.navigationController.toolbarHidden = YES;

	[self refresh];
	
	self.tableView.rowHeight = 66;

	self.navigationItem.rightBarButtonItem = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
	self.navigationItem.title = @"Suggestions";

	// Want to receive notifications regarding syncCheck finishing
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(syncSuggestionsDidFinish:)
		name:@"syncCheck" object:nil];
	
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// Remove the notification observer for syncCheck
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:@"syncCheck" object:nil];

	if (self.needsSync) {
		[BCObjectManager syncCheck:SendOnly];
		self.needsSync = NO;
	}

	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
	[self setEmptyImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - BCTableView overrides
- (void)refresh
{
	[BCObjectManager syncCheck:SendAndReceive];
}

#pragma mark - Local methods
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	Recommendation *suggestion = [self.fetchedResultsController objectAtIndexPath:indexPath];

	cell.textLabel.text = suggestion.name;
	cell.detailTextLabel.text = [NSString stringWithFormat:@"Purchased an average of every %@ days", suggestion.days];
	
	if ([suggestion.hidden boolValue]) {
		cell.imageView.image = [UIImage imageNamed:@"lightbulb-white"];
	}
	else {
		cell.imageView.image = [UIImage imageNamed:@"lightbulb-green"];
	}
}

- (void)syncSuggestionsDidFinish:(NSNotification *)notification
{
	[self refreshFetchedResultsController];
}

- (void)refreshFetchedResultsController
{
	// delete the cache
	self.fetchedResultsController = nil;

	// this will recreate the cache
	[self.tableView reloadData];

	self.emptyImageView.hidden = ([self.fetchedResultsController.fetchedObjects count] > 0);
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"SuggestionCell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];

    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	Recommendation *rec = [self.fetchedResultsController objectAtIndexPath:indexPath];
	if ([rec.hidden boolValue]) {
		rec.hidden = [NSNumber numberWithBool:NO];
		rec.status = kStatusPost;
	}
	else {
		rec.hidden = [NSNumber numberWithBool:YES];
		rec.status = kStatusDelete;
	}
	self.needsSync = YES;

	UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
	[self configureCell:cell atIndexPath:indexPath];

	NSError *error = nil;
	if (![rec.managedObjectContext save:&error]) {
		DDLogError(@"(%@)[%ld] %@", error.domain, (long)error.code, [error localizedDescription]);
	}
}

#pragma mark - NSFetchedResultsController delegate
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[Recommendation entityInManagedObjectContext:self.managedObjectContext]];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
	// filter for the user
	User *thisUser = [User currentUser];
	NSPredicate *accountIdPredicate = [NSPredicate predicateWithFormat:@"(accountId = %@)", thisUser.accountId];
	fetchRequest.predicate = accountIdPredicate;
	
    // sort by item name
	NSSortDescriptor *sortByName = [NSSortDescriptor
		sortDescriptorWithKey:@"name" ascending:YES
		selector:@selector(caseInsensitiveCompare:)];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    // create and init the fetched results controller
    NSFetchedResultsController *aFetchedResultsController = 
		[[NSFetchedResultsController alloc] 
		initWithFetchRequest:fetchRequest 
		managedObjectContext:self.managedObjectContext 
		sectionNameKeyPath:nil 
		cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    

	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    
    return _fetchedResultsController;
}    

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
	//QuietLog(@"%s", __FUNCTION__);
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller 
didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
	//QuietLog(@"%s", __FUNCTION__);
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
				withRowAnimation:UITableViewRowAnimationLeft];
			break;

		case NSFetchedResultsChangeDelete:
			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
				withRowAnimation:UITableViewRowAnimationRight];
			break;
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
	}
}

- (void)controller:(NSFetchedResultsController *)controller
didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath
forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
	//QuietLog(@"%s", __FUNCTION__);
	UITableView *thisTableView = self.tableView;

	switch(type) {
		case NSFetchedResultsChangeInsert:
			[thisTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
				withRowAnimation:UITableViewRowAnimationLeft];
			break;

		case NSFetchedResultsChangeDelete:
			[thisTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
				withRowAnimation:UITableViewRowAnimationRight];
			break;

		case NSFetchedResultsChangeUpdate:
			[self configureCell:[thisTableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
			break;

		case NSFetchedResultsChangeMove:
			[thisTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
				withRowAnimation:UITableViewRowAnimationLeft];
			[thisTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
				withRowAnimation:UITableViewRowAnimationRight];
			break;
	}
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	//QuietLog(@"%s", __FUNCTION__);
    [self.tableView endUpdates];
}

@end
