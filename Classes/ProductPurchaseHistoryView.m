//
//  ProductPurchaseHistoryView.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/9/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "ProductPurchaseHistoryView.h"

#import "BCProgressHUD.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"
#import "GTMHTTPFetcherLogging.h"
#import "NSDateFormatter+Additions.h"
#import "PurchaseHxItem.h"
#import "QuartzCore/QuartzCore.h"

static const NSInteger kSectionHeaderHeight = 28;

@interface ProductPurchaseHistoryView ()
- (void)purchaseHxFetcher:(GTMHTTPFetcher *)inFetcher 
finishedWithData:(NSData *)retrievedData 
error:(NSError *)error;
@end

@implementation ProductPurchaseHistoryView

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

	self.title = @"Purchase History";

	NSURL *url = nil;
	if ([self.purchasedItem.productId integerValue] == 0) {
		url = [BCUrlFactory purchaseHistoryURLForProductNamed:self.purchasedItem.name
			limit:[NSNumber numberWithInteger:20]];
	}
	else {
		url = [BCUrlFactory purchaseHistoryURLForProductId:self.purchasedItem.productId 
			limit:[NSNumber numberWithInteger:20]];
	}

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url];
	[fetcher beginFetchWithDelegate:self 
		didFinishSelector:@selector(purchaseHxFetcher:finishedWithData:error:)];

	BCProgressHUD *hud = [BCProgressHUD showHUDAddedTo:self.tableView animated:YES];
	hud.labelText = @"fetching...";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Local methods
- (void)purchaseHxFetcher:(GTMHTTPFetcher *)inFetcher 
finishedWithData:(NSData *)retrievedData 
error:(NSError *)error 
{
	if (error != nil) {
		[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:inFetcher response:nil];
	} 
	else {
		// fetch succeeded
		NSString *response = [[NSString alloc] initWithData:retrievedData
			encoding:NSUTF8StringEncoding];
		id responseData = [response JSONValue];

		if ([responseData isKindOfClass:[NSArray class]]) {
			self.purchaseHxItems = responseData;
		}
	}
	[self.tableView reloadData];
	[BCProgressHUD hideHUD:NO];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.purchaseHxItems count];
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
   
    UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
			reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
	NSDictionary *item = [self.purchaseHxItems objectAtIndex:[indexPath row]];
	if ((NSNull *)[item objectForKey:kStore] != [NSNull null]) {
		cell.textLabel.text = [item objectForKey:kStore];
		cell.detailTextLabel.text = [NSDateFormatter relativeStringFromDate:[NSDate dateWithTimeIntervalSince1970:[[item objectForKey:kPurchaseHxPurchasedOn] doubleValue]]];
	}
	else {
		cell.textLabel.text = [NSDateFormatter relativeStringFromDate:[NSDate dateWithTimeIntervalSince1970:[[item objectForKey:kUpdatedOn] doubleValue]]];
	}
    
    return cell;
}

#pragma mark - UITableViewDelegate 
- (CGFloat)tableView:(UITableView *)tableView 
heightForHeaderInSection:(NSInteger)section
{
	return kSectionHeaderHeight;
}

- (UIView *)tableView:(UITableView *)theTableView 
viewForHeaderInSection:(NSInteger)section
{
	return [BCTableViewController customViewForHeaderWithTitle:self.purchasedItem.name];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

@end
