//
//  NSData+Base64.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 8/12/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Base64)

+ (NSString *)base64EncodeString:(NSString *)strData;
+ (NSString *)base64EncodeData:(NSData *)objData;
+ (NSData *)base64DecodeString:(NSString *)strBase64;

@end
