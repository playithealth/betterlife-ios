//
//  PantrySearchViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/30/2012.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCSearchViewController.h"

@interface PantrySearchViewController : BCSearchViewController

@end
