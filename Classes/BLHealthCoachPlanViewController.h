//
//  BLHealthCoachPlanViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 12/5/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AdvisorMealPlanMO;

@interface BLHealthCoachPlanViewController : UITableViewController
@property (strong, nonatomic) AdvisorMealPlanMO *plan;
@end
