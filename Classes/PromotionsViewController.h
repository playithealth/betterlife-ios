//
//  PromotionsViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PromotionDetailViewController;

@interface PromotionsViewController : UIViewController {
@private
	PromotionDetailViewController *childController;
	UIImageView *activeTabImageView;
}

@property (nonatomic, strong) NSArray *list;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIImageView *activeTabImageView;

- (IBAction)scanButtonClicked:(id)sender;
- (IBAction)recentClicked:(id)sender;
- (IBAction)nearbyClicked:(id)sender;
- (IBAction)recommendedClicked:(id)sender;

@end
