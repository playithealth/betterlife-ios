//
//  BLFoodItem.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 3/6/14.
//  Copyright (c) 2014 BettrLife Corporation All rights reserved.
//
@class NutritionMO;
@class RecipeMO;
@class CustomFoodMO;
@class AdvisorMealPlanTagMO;

@protocol BLFoodItem <NSObject>
- (void)markForSync;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic, readonly) NSNumber *itemId;
@property (strong, nonatomic) NSString *itemType;
@property (strong, nonatomic) NSNumber *imageId;
@property (strong, nonatomic) NSNumber *servings;
@property (strong, nonatomic) NSString *servingSize;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSNumber *logTime;
@property (strong, nonatomic) NutritionMO *nutrition;
@property (strong, nonatomic) RecipeMO *recipe;
- (NSString *)servingsUnit;

@optional
@property (strong, nonatomic) NSNumber *nutritionFactor;
@property (strong, nonatomic) CustomFoodMO *customFood;
@property (strong, nonatomic) AdvisorMealPlanTagMO *tag;
- (void)setServingsUnit:(NSString *)servingsUnit;
@end
