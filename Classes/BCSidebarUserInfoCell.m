//
//  BCSidebarUserInfoCell.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/16/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCSidebarUserInfoCell.h"

#import "BLSidebarCalendarButton.h"
#import "NSCalendar+Additions.h"
#import "UIColor+Additions.h"

@implementation BLSeparatorView
// Needed to subclass UIView to make a separator view because autolayout was trumping my 0.5 frame height. This mechanism plays nice
// with autolayout :)
- (CGSize)intrinsicContentSize {
	CGFloat	separatorHeight = 1.0 / [[UIScreen mainScreen] scale];
	return CGSizeMake(UIViewNoIntrinsicMetric, separatorHeight);
}
@end

@interface BCSidebarUserInfoCell ()
@property (weak, nonatomic) IBOutlet UIView *currentWeekIndicatorView;
@property (strong, nonatomic) NSLayoutConstraint *currentWeekIndicatorViewVerticalConstraint;
@property (weak, nonatomic) IBOutlet UIView *currentWeekDotView;
@property (strong, nonatomic) CAGradientLayer *gradientLayer;
@property (strong, nonatomic) NSLayoutConstraint *poweredByHeightConstraint;
@property (assign, nonatomic) double poweredByHeight;
@property (strong, nonatomic) NSCalendar *calendar;
@property (strong, nonatomic) NSDate *dateOfFirstButton; ///< The date of the first button of the calendar view
@property (strong, nonatomic) NSArray *sortedCalendarDays;
@end

@implementation BCSidebarUserInfoCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _calendar = [NSCalendar currentCalendar];
		_gradientLayer = [CAGradientLayer layer];

		// iOS 8 handles adding this layer to the cell's layer, but iOS 7 wants it in the contentView's layer
		// It won't show up on iOS 7 if it is in the cell's layer
		[self.contentView.layer insertSublayer:_gradientLayer atIndex:0];

		_dateOfFirstButton = [_calendar BC_firstOfWeekForDate:[_calendar BC_startOfDate:[NSDate date]]];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];

	self.gradientLayer.frame = self.bounds;
	self.currentWeekDotView.layer.cornerRadius = self.currentWeekDotView.bounds.size.height / 2.0;
}

- (void)useGradientWithTopColor:(UIColor *)topColor bottomColor:(UIColor *)bottomColor
{
	// If we don't have a gradient layer, add it
	if (!self.gradientLayer) {
		// Add the gradient
		self.gradientLayer = [CAGradientLayer layer];
		// iOS 8 handles adding this layer to the cell's layer, but iOS 7 wants it in the contentView's layer
		// It won't show up on iOS 7 if it is in the cell's layer
		[self.contentView.layer insertSublayer:self.gradientLayer atIndex:0];
	}

	// Unsure exactly why, but these colors seem to go from bottom-up
	self.gradientLayer.colors = @[(id)[bottomColor CGColor], (id)[topColor CGColor]];
	self.gradientLayer.locations = @[ @(0.0), @(1.0) ];
	self.gradientLayer.hidden = NO;
}

- (void)hideGradient
{
	self.gradientLayer.hidden = YES;
}

- (void)showPoweredBy:(BOOL)show
{
	if (!self.poweredByHeightConstraint) {
		// See if there is a constraint on the poweredByView for the height, if so, grab its pointer
		NSArray *constraints = [self.poweredByView constraints];
		for (NSLayoutConstraint *constraint in constraints) {
			if ([constraint.firstItem isEqual:self.poweredByView] && (constraint.firstAttribute == NSLayoutAttributeHeight)) {
				self.poweredByHeightConstraint = constraint;
				self.poweredByHeight = constraint.constant;
				break;
			}
		}
	}

	self.poweredByHeightConstraint.constant = (show ? self.poweredByHeight : 0.0f);
	self.poweredByView.hidden = !show;
}

- (void)showMyZone:(BOOL)show
{
	self.myzoneView.hidden = !show;
}

- (void)setCalendarShown:(BOOL)shown
{
	if (_calendarShown == shown) {
		// Nothing to do since the view is already set this way
		return;
	}
	_calendarShown = shown;

	// Toggle the views
	self.myzoneWeekView.hidden = _calendarShown;
	self.myzoneCalendarView.hidden = !_calendarShown;

	if (shown) {
		// Initialize the calendar back to the current month whenever it is shown
		self.showingMonth = [NSDate date];
	}
	else {
		// Ensure that the week view has the correct starting date so the buttons work properly
		self.dateOfFirstButton = [self.calendar BC_firstOfWeekForDate:[self.calendar BC_startOfDate:[NSDate date]]];
	}
}

- (void)setShowingMonth:(NSDate *)dateInMonth
{
	if (!self.sortedCalendarDays) {
		// Sort the calendarDays and populate sortedCalendarDays
		self.sortedCalendarDays = [self.calendarDays sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"tag"
			ascending:YES]]];
	}

	NSDate *currentMonthStart = [self.calendar BC_firstOfMonthForDate:[NSDate date]];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"MMMM YYYY"];
	self.monthLabel.text = [formatter stringFromDate:dateInMonth];

	// Find the first day of the month, and figure out its weekday so we know where to start
	NSDate *firstOfMonth = [self.calendar BC_firstOfMonthForDate:dateInMonth];
	self.dateOfFirstButton = [self.calendar BC_startOfDate:[self.calendar BC_firstOfWeekForDate:firstOfMonth]];
    if ([self.delegate respondsToSelector:@selector(didChangeCalendarStartDate:)]) {
		[self.delegate didChangeCalendarStartDate:self.dateOfFirstButton];
	}

	NSDate *lastOfMonth = [self.calendar BC_lastOfMonthForDate:dateInMonth];
	NSDateComponents *components = [self.calendar components:(NSWeekdayCalendarUnit | NSMonthCalendarUnit) fromDate:firstOfMonth];
	NSUInteger startingWeekDay = components.weekday;
	components = [self.calendar components:(NSDayCalendarUnit) fromDate:lastOfMonth];
	NSUInteger daysInMonth = components.day;

	// Fill out the days for the current month
	for (NSUInteger day = 1; day <= daysInMonth; day++) {
		NSUInteger buttonIndex = (day - 1) + (startingWeekDay - 1);
		BLSidebarCalendarButton *button = [self.sortedCalendarDays objectAtIndex:buttonIndex];
		[button setTitle:[NSString stringWithFormat:@"%lu", (long)day] forState:UIControlStateNormal];
		[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[button setHasData:[self.delegate hasDataAtIndex:buttonIndex]];
	}

	// Now fill out the visible days for the prior month
	NSDate *lastOfLastMonth = [self.calendar BC_lastOfMonthForDate:[self.calendar BC_dateByAddingMonths:-1 toDate:dateInMonth]];
	components = [self.calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:lastOfLastMonth];
	NSUInteger endIndex = startingWeekDay - 2; // Subtract one for the zero offset, and one more to go back one day from start
	NSUInteger daysLastMonth = components.day;
	for (NSInteger dayOffset = endIndex; dayOffset >= 0; dayOffset--) {
		NSUInteger buttonIndex = endIndex - dayOffset;
		BLSidebarCalendarButton *button = [self.sortedCalendarDays objectAtIndex:buttonIndex];
		[button setTitle:[NSString stringWithFormat:@"%lu", (long)daysLastMonth - dayOffset] forState:UIControlStateNormal];
		[button setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4] forState:UIControlStateNormal];
		[button setHasData:[self.delegate hasDataAtIndex:buttonIndex]];
	}

	// Now fill out the visible days for the next month
	NSUInteger totalDays = 6 * 7;
	NSUInteger remainingDays = totalDays - (daysInMonth + startingWeekDay - 1);
	for (NSUInteger day = 1; day <= remainingDays; day++) {
		NSUInteger buttonIndex = totalDays - remainingDays + day - 1;
		BLSidebarCalendarButton *button = [self.sortedCalendarDays objectAtIndex:buttonIndex];
		[button setTitle:[NSString stringWithFormat:@"%lu", (long)day] forState:UIControlStateNormal];
		[button setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4] forState:UIControlStateNormal];
		[button setHasData:[self.delegate hasDataAtIndex:buttonIndex]];
	}

	// Determine if the month we're showing is the current month, if so, then we will highlight the current week
	if ([firstOfMonth isEqualToDate:currentMonthStart]) {
		self.currentWeekIndicatorView.hidden = NO;
		components = [self.calendar components:(NSDayCalendarUnit) fromDate:[NSDate date]];
		NSUInteger currentDayOfMonth = components.day;
		// Determine the button for the current day, and then align the shader view vertically with it
		// Need to offset the week day by 1, and need to offset the whole thing by another 1 due to zero based index
		BLSidebarCalendarButton *button = [self.sortedCalendarDays objectAtIndex:startingWeekDay - 1 + currentDayOfMonth - 1];
		if (!self.currentWeekIndicatorViewVerticalConstraint) {
			// Find the constraint
			NSArray *constraints = [self.myzoneCalendarView constraints];
			for (NSLayoutConstraint *constraint in constraints) {
				if ([constraint.secondItem isEqual:self.currentWeekIndicatorView]
						&& (constraint.secondAttribute == NSLayoutAttributeCenterY)) {
					self.currentWeekIndicatorViewVerticalConstraint = constraint;
					break;
				}
			}
		}

		NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeCenterY
			relatedBy:NSLayoutRelationEqual toItem:self.currentWeekIndicatorView attribute:NSLayoutAttributeCenterY
			multiplier:(CGFloat)1.0 constant:(CGFloat)0.0];
		[self.myzoneCalendarView removeConstraint:self.currentWeekIndicatorViewVerticalConstraint];
		self.currentWeekIndicatorViewVerticalConstraint = newConstraint;
		[self.myzoneCalendarView addConstraint:newConstraint];
	}
	else {
		self.currentWeekIndicatorView.hidden = YES;
	}

    [self setNeedsLayout];
    [self layoutIfNeeded];

	_showingMonth = dateInMonth;
}

- (NSDate *)dateForButton:(UIButton *)dayButton {
	return [self.calendar BC_dateByAddingDays:dayButton.tag toDate:self.dateOfFirstButton];
}

- (UIButton *)buttonForDate:(NSDate *)targetDate {
	return [self.sortedCalendarDays objectAtIndex:[self.calendar BC_daysFromDate:self.dateOfFirstButton toDate:
		[self.calendar BC_startOfDate:targetDate]]];
}

- (IBAction)previousMonthTapped:(UIButton *)sender {
	NSDate *previousMonth = [self.calendar BC_dateByAddingMonths:-1 toDate:self.showingMonth];
	[self setShowingMonth:previousMonth];
}

- (IBAction)nextMonthTapped:(UIButton *)sender {
	NSDate *nextMonth = [self.calendar BC_dateByAddingMonths:1 toDate:self.showingMonth];
	[self setShowingMonth:nextMonth];
}

- (IBAction)myzoneWeekdayButtonTapped:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didSelectDate:)]) {
        [self.delegate didSelectDate:[self dateForButton:sender]];
    }
}

- (IBAction)myzoneDayButtonTapped:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didSelectDate:)]) {
        [self.delegate didSelectDate:[self dateForButton:sender]];
    }
}

@end
