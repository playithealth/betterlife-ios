//
//  NSCalendar+Additions.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 6/5/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "NSCalendar+Additions.h"

@implementation NSCalendar (Additions)

- (NSDate *)BC_startOfDate:(NSDate *)date
{
	NSDateComponents *components = [self components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
	
	// NOTE I think this is technically unnecessary
	components.hour = 0;
	components.minute = 0;
	components.second = 0;

	return [self dateFromComponents:components];
}

- (NSDate *)BC_endOfDate:(NSDate *)date
{
	NSDateComponents *components = [self components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
	
	components.hour = 23;
	components.minute = 59;
	components.second = 59;

	return [self dateFromComponents:components];
}

- (NSInteger)BC_daysFromDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
	// Need to normalize the dates to ensure the math works
	NSDate *normalizedStartDate = [self BC_startOfDate:startDate];
	NSDate *normalizedEndDate = [self BC_startOfDate:endDate];
	NSInteger startDay = [self ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:normalizedStartDate];
	NSInteger endDay = [self ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:normalizedEndDate];
	return endDay - startDay;
}

- (NSInteger)BC_monthsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
	startDate = [self BC_lastOfMonthForDate:startDate];
	endDate = [self BC_lastOfMonthForDate:endDate];
	NSDateComponents *difference = [self components:NSMonthCalendarUnit fromDate:startDate toDate:endDate options:0];
	return difference.month;
}

- (NSInteger)BC_yearsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
	NSDateComponents *difference = [self components:NSYearCalendarUnit fromDate:startDate toDate:endDate options:0];
	return difference.year;
}

- (NSDate *)BC_firstOfYearForDate:(NSDate *)baseDate
{
	NSDateComponents *components = [self components:(NSYearCalendarUnit) fromDate:baseDate];
	[components setDay:1];
	return [self dateFromComponents:components];
}

- (NSDate *)BC_firstOfYearForToday
{
	return [self BC_firstOfYearForDate:[NSDate date]];
}

- (NSDate *)BC_lastOfYearForDate:(NSDate *)baseDate
{
	NSDateComponents *components = [self components:(NSYearCalendarUnit) fromDate:baseDate];
	[components setDay:0];
	[components setHour:23];
	[components setMinute:59];
	[components setSecond:59];
	[components setYear:components.year + 1];
	return [self dateFromComponents:components];
}

- (NSDate *)BC_lastOfYearForToday
{
	return [self BC_lastOfYearForDate:[NSDate date]];
}

- (NSDate *)BC_firstOfMonthForDate:(NSDate *)baseDate
{
	NSDateComponents *components = [self components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:baseDate];
	[components setDay:1];
	return [self dateFromComponents:components];
}

- (NSDate *)BC_firstOfMonthForToday
{
	return [self BC_firstOfMonthForDate:[NSDate date]];
}

- (NSDate *)BC_lastOfMonthForDate:(NSDate *)baseDate
{
	NSDateComponents *components = [self components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:baseDate];
	[components setDay:0];
	[components setMonth:components.month + 1];
	return [self dateFromComponents:components];
}

- (NSDate *)BC_lastOfMonthForToday
{
	return [self BC_lastOfMonthForDate:[NSDate date]];
}

- (NSDate *)BC_firstOfWeekForDate:(NSDate *)baseDate
{
	NSDateComponents *components = [self components:(NSWeekdayCalendarUnit) fromDate:baseDate];
	return [self BC_dateByAddingDays:BCWeekdaySunday - components.weekday toDate:baseDate];
}

- (NSDate *)BC_firstOfWeekForToday
{
	return [self BC_firstOfWeekForDate:[NSDate date]];
}

- (NSDate *)BC_lastOfWeekForDate:(NSDate *)baseDate
{
	NSDateComponents *components = [self components:(NSWeekdayCalendarUnit) fromDate:baseDate];
	return [self BC_dateByAddingDays:BCWeekdaySaturday - components.weekday toDate:baseDate];
}

- (NSDate *)BC_lastOfWeekForToday
{
	return [self BC_lastOfWeekForDate:[NSDate date]];
}

- (NSDate *)BC_dateByAddingDays:(NSInteger)days toDate:(NSDate *)baseDate
{
	NSDateComponents *components = [[NSDateComponents alloc] init];
	components.day = days;
	return [self dateByAddingComponents:components toDate:baseDate options:0];
}

- (NSDate *)BC_dateByAddingMonths:(NSInteger)months toDate:(NSDate *)baseDate
{
	NSDateComponents *components = [[NSDateComponents alloc] init];
	components.month = months;
	return [self dateByAddingComponents:components toDate:baseDate options:0];
}

- (NSDate *)BC_dateByAddingYears:(NSInteger)years toDate:(NSDate *)baseDate
{
	NSDateComponents *components = [[NSDateComponents alloc] init];
	components.year = years;
	return [self dateByAddingComponents:components toDate:baseDate options:0];
}

@end
