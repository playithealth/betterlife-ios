//
//  PantrySearchViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/30/2012.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "PantrySearchViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCProgressHUD.h"
#import "BCTableViewCell.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "CategoryMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "PantryItem.h"
#import "PantryItemViewController.h"
#import "ProductSearchRecord.h"
#import "PurchaseHxItem.h"
#import "ProductSearchFilterViewController.h"
#import "UIColor+Additions.h"
#import "User.h"

static const NSInteger kSectionAddCustomItem = 0;
static const NSInteger kSectionPurchaseHxResults = 1;
static const NSInteger kSectionProductResults = 2;

static const NSInteger kRowsPerSection = 3;
static const NSInteger kSearchPageSize = 10;

@interface PantrySearchViewController () <ProductSearchFilterViewDelegate, PantryItemViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIImageView *footerImageView;
@property (weak, nonatomic) IBOutlet UILabel *filtersLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerViewBottomConstraint;

@property (strong, nonatomic) NSMutableArray *productResults;
@property (strong, nonatomic) NSArray *searchBrands;
@property (strong, nonatomic) NSArray *searchCategories;
@property (strong, nonatomic) NSArray *purchaseHxResults;
@property (assign, nonatomic) BOOL morePurchaseHx;
@property (assign, nonatomic) BOOL moreProducts;
@property (assign, nonatomic) NSInteger purchaseHxPage;
@property (assign, nonatomic) NSInteger productsPage;
@property (strong, nonatomic) NSArray *searchFilters;
@property (strong, nonatomic) NSMutableDictionary *imageFetchersInProgress;
@property (strong, nonatomic) NSString *searchTerm;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)cancelAllImageFetchers;
- (void)addFreeTextItem;

@end

@implementation PantrySearchViewController

#pragma mark - view lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.imageFetchersInProgress = [NSMutableDictionary dictionary];
	}
	return self;
}

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
    
	// terminate all pending download connections
	[self cancelAllImageFetchers];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.tableView.frame = CGRectMake(0, 44, 320, 416);
	self.footerView.frame = CGRectMake(0, 460, 320, 44);

    [self.searchTextField becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
	[super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    // terminate all pending download connections
	[self cancelAllImageFetchers];

	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [self setSearchTextField:nil];
    [self setCancelButton:nil];
    [self setTableView:nil];
	[self setFooterImageView:nil];
	[self setFiltersLabel:nil];

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowFilters"]) {
		ProductSearchFilterViewController *filterView = segue.destinationViewController;
		filterView.managedObjectContext = self.managedObjectContext;
		filterView.delegate = self;
		filterView.searchBrands = self.searchBrands;
		filterView.searchCategories = self.searchCategories;
	}
}

#pragma mark - local methods
- (void)sendFirstSearchRequest
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];
	
	NSURL *searchURL = [BCUrlFactory searchUrlForSearchTerm:self.searchTerm searchSections:@[@"products"] showRows:kRowsPerSection showOnlyFood:NO];
    
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
	
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:nil];
		}
		else {
			// fetch succeeded
			NSDictionary *searchResultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
            
			NSDictionary *productsDict = [searchResultsDict objectForKey:@"products"];
			NSArray *productsArray = [productsDict objectForKey:@"results"];
			self.moreProducts = [[productsDict objectForKey:@"more"] boolValue];

			NSMutableArray *psrArray = [NSMutableArray array];
			for (NSDictionary *product in productsArray) {
				[psrArray addObject:[ProductSearchRecord recordWithSearchDictionary:product withManagedObjectContext:self.managedObjectContext]];
			}

			self.productResults = psrArray;
			[self.tableView reloadData];

			[appDelegate setNetworkActivityIndicatorVisible:NO];
		}

		[self.searchTextField resignFirstResponder];
	}];
}

- (void)sendLoadMoreRequest
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.productResults count] inSection:kSectionProductResults];
	BCTableViewCell *cell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[cell.spinner startAnimating];
	cell.bcTextLabel.text = @"Loading...";
	
	if (self.productsPage == 0) {
		[self resetSearch];
		self.productResults = [NSMutableArray array];
	}
    
	NSMutableDictionary *filterDict = [NSMutableDictionary dictionary];
	if (self.searchFilters) {
		NSMutableArray *categoryFilters = [self.searchFilters objectAtIndex:0];
		if ([categoryFilters count]) {
			[filterDict setObject:categoryFilters forKey:@"categories"];
		}
		
		NSMutableArray *brandFilters = [self.searchFilters objectAtIndex:1];
		if ([brandFilters count]) {
			[filterDict setObject:brandFilters forKey:@"brands"];
		}
		
		NSMutableArray *lifestyleFilters = [self.searchFilters objectAtIndex:2];
		if ([lifestyleFilters count]) {
			[filterDict setObject:lifestyleFilters forKey:@"lifestyles"];
		}
		
		NSMutableArray *ratingsFilters = [self.searchFilters objectAtIndex:3];
		if ([ratingsFilters count]) {
			[filterDict setObject:ratingsFilters forKey:@"ratings"];
		}
	}


	NSURL *searchURL = [BCUrlFactory productSearchURLForSearchTerm:self.searchTerm showRows:kSearchPageSize 
		rowOffset:self.productsPage++ * kSearchPageSize filters:filterDict showOnlyFood:NO];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
	
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:nil];
		}
		else {
			// fetch succeeded
			NSDictionary *productsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			NSArray *productsArray = [productsDict objectForKey:kProductResults];
			for (NSDictionary *product in productsArray) {
				[self.productResults addObject:[ProductSearchRecord recordWithSearchDictionary:product withManagedObjectContext:self.managedObjectContext]];
			}

			if ([productsDict objectForKey:kProductBrands]) {
				self.searchBrands = [productsDict objectForKey:kProductBrands];
			}
			if ([productsDict objectForKey:kProductCategories]) {
				self.searchCategories = [productsDict objectForKey:kProductCategories];			
			}

			[self.tableView reloadData];

			[appDelegate setNetworkActivityIndicatorVisible:NO];
			[cell.spinner stopAnimating];
			cell.bcTextLabel.text = @"Load more results...";

			if (self.footerView.hidden) {
				self.footerView.hidden = NO;

				// remove the constraint tying the table view to the main view and 
				// tie it to the top of the footer
				[self.view removeConstraint:self.tableViewBottomConstraint];
				self.tableViewBottomConstraint = 
					[NSLayoutConstraint constraintWithItem:self.tableView
												 attribute:NSLayoutAttributeBottom
												 relatedBy:NSLayoutRelationEqual
													toItem:self.footerView
												 attribute:NSLayoutAttributeTop
												multiplier:1.0
												  constant:0];
				[self.view addConstraint:self.tableViewBottomConstraint];

				// 'show' the footer view at the bottom of the view
				self.footerViewBottomConstraint.constant = 0;

				[UIView animateWithDuration:0.3f animations:^{
					[self.view layoutIfNeeded];

					self.footerImageView.image = [UIImage imageNamed:@"product-search-footer"];
					self.filtersLabel.text = @"";
				}];
			}
		}
		
		[self.searchTextField resignFirstResponder];
	}];
}

- (void)resetSearch
{
	self.purchaseHxResults = nil;
	self.purchaseHxPage = 0;
	self.productResults = nil;
	self.productsPage = 0;
}

- (void)loadPurchaseHx:(NSInteger)rows
{    
    User *thisUser = [User currentUser];
    NSSortDescriptor *sortByItem = [NSSortDescriptor sortDescriptorWithKey:PurchaseHxItemAttributes.name
        ascending:YES selector:@selector(caseInsensitiveCompare:)];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [PurchaseHxItem entityInManagedObjectContext:self.managedObjectContext];

    NSPredicate *predicate = nil;
    if ([self.searchTerm length] >= 3) {
        predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@ AND accountId = %@", 
            PurchaseHxItemAttributes.name, self.searchTerm, thisUser.accountId];
    }
    else {
        predicate = [NSPredicate predicateWithFormat:@"%K BEGINSWITH[c] %@ AND accountId = %@", 
            PurchaseHxItemAttributes.name, self.searchTerm, thisUser.accountId];
    }

    // filter for the user and searchTerm
    fetchRequest.predicate = predicate;
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortByItem];
	
	NSUInteger count = [self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
	self.morePurchaseHx = (count > rows);
	
	fetchRequest.fetchLimit = rows;
    self.purchaseHxResults = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
}

- (void)addFreeTextItem
{
	NSDictionary *freeTextItem = @{ kItemName : self.searchTextField.text, kProductId : @0, kProductCategoryId : @1 };
    
	if (self.delegate != nil
	        && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:freeTextItem];
	}
}

- (void)cancelAllImageFetchers
{
	NSArray *allDownloads = [self.imageFetchersInProgress allValues];
	[allDownloads makeObjectsPerformSelector:@selector(stopFetching)];
	self.imageFetchersInProgress = nil;
}

- (void)downloadImageWithId:(NSNumber *)imageId forIndexPath:(NSIndexPath *)indexPath
{
	GTMHTTPFetcher *imageFetcher = [self.imageFetchersInProgress objectForKey:indexPath];
	if (imageFetcher == nil) {
		imageFetcher = [GTMHTTPFetcher signedFetcherWithURL:
            [BCUrlFactory imageURLForImage:imageId imageSize:@"medium"]];
		[self.imageFetchersInProgress setObject:imageFetcher forKey:indexPath];
        
        [imageFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {            
            if (error != nil) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
					withFetcher:imageFetcher response:nil];
            }
            else {
                UIImage *productImage = [[UIImage alloc] initWithData:retrievedData];
                if (productImage.size.width != 60 && productImage.size.height != 60) {
                    CGSize itemSize = CGSizeMake(60, 60);
                    UIGraphicsBeginImageContext(itemSize);
                    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
                    [productImage drawInRect:imageRect];
                    productImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                }

				[[BCImageCache sharedCache] setImage:productImage forImageId:imageId];
                
                // Display the newly loaded image
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                cell.imageView.image = productImage;
            }
            
            // remove the image fetcher from the dictionary
            [self.imageFetchersInProgress removeObjectForKey:indexPath];
        }];
	}
}

- (void)loadImagesForOnscreenRows
{
	// check to make sure the search tab is active
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
	for (NSIndexPath *indexPath in visiblePaths) {
		if (indexPath.section == kSectionPurchaseHxResults) {
			if (indexPath.row < [self.purchaseHxResults count]) {
				PurchaseHxItem *purchaseHxItem = [self.purchaseHxResults objectAtIndex:indexPath.row];

				UIImage *productImage = [[BCImageCache sharedCache] imageWithImageId:purchaseHxItem.imageId];

				// avoid the image download if the row already has an image
				if (purchaseHxItem.imageId && !productImage) {
					[self downloadImageWithId:purchaseHxItem.imageId forIndexPath:indexPath];
				}
			}
		}
		else if (indexPath.section == kSectionProductResults) {
			if (indexPath.row < [self.productResults count]) {
				ProductSearchRecord *record = [self.productResults objectAtIndex:[indexPath row]];

				UIImage *productImage = [[BCImageCache sharedCache] imageWithImageId:record.imageId];

				// avoid the image download if the row already has an image
				if (record.imageId && !productImage) {
					[self downloadImageWithId:record.imageId forIndexPath:indexPath];
				}
			}
		}
	}
}

#pragma mark - button responders
- (void)addFreeTextButtonTapped:(UITapGestureRecognizer *)recognizer
{
	if ([self.searchTextField.text length] == 0) {
		return;
	}
    
	[self.searchTextField resignFirstResponder];
    
    [self addFreeTextItem];
}

- (void)quickAddPurchaseHxButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	PurchaseHxItem *purchaseHxItem = [self.purchaseHxResults objectAtIndex:indexPath.row];

	if (self.delegate != nil
			&& [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:purchaseHxItem];
	}
}

- (void)purchaseHxCellTapped:(UITapGestureRecognizer *)recognizer
{
}

- (void)quickAddProductButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    ProductSearchRecord *searchRecord = [self.productResults objectAtIndex:indexPath.row];

	if (self.delegate != nil
		&& [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:searchRecord];
	}
}

- (void)productCellTapped:(UITapGestureRecognizer *)recognizer
{
}

#pragma mark - UITableDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numberOfSections = 3;
        
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    
    if (section == kSectionAddCustomItem) {
        if ([self.searchTextField.text length] > 0) {
            numberOfRows = 1;
        }
    }
    else if (section == kSectionPurchaseHxResults) {
        numberOfRows = [self.purchaseHxResults count];
		if (self.morePurchaseHx) {
			numberOfRows++;
		}
    }
    else if (section == kSectionProductResults) {
        numberOfRows = [self.productResults count];
		if (self.moreProducts) {
			numberOfRows++;
		}
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *addCustomCellId = @"AddCustomCell";
    static NSString *purchaseHxCellId = @"PurchaseHxCell";
    static NSString *productCellId = @"ProductCell";
    static NSString *showMoreCellId = @"ShowMoreCell";

    UITableViewCell *cell = nil;
    if (indexPath.section == kSectionAddCustomItem) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:addCustomCellId];

		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addFreeTextButtonTapped:)];
		[cell.imageView addGestureRecognizer:recognizer];
    }
	else if (indexPath.section == kSectionPurchaseHxResults) {
		if (indexPath.row < [self.purchaseHxResults count]) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:purchaseHxCellId];

			UIImageView *addImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"green-plus"]];
			addImageView.userInteractionEnabled = YES;
			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddPurchaseHxButtonTapped:)];
			[addImageView addGestureRecognizer:recognizer];
			cell.accessoryView = addImageView;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
    }
	else if (indexPath.section == kSectionProductResults) {
		if (indexPath.row < [self.productResults count]) {
			BCTableViewCell *bcCell = [self.tableView dequeueReusableCellWithIdentifier:productCellId];

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(quickAddProductButtonTapped:)];
			[bcCell.bcAccessoryButton addGestureRecognizer:recognizer];

			cell = bcCell;
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:showMoreCellId];
		}
	}

	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionAddCustomItem) {
		NSString *addString = [NSString stringWithFormat:@"Add “%@” to pantry", self.searchTerm];
		if ([cell.textLabel respondsToSelector:@selector(setAttributedText:)]) {
			NSMutableAttributedString *addAttrString = [[NSMutableAttributedString alloc] initWithString:addString 
				attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_mediumDarkGrayColor] }];
			[addAttrString setAttributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName : [UIColor BC_darkGrayColor] }
				range:NSMakeRange(4, [self.searchTerm length] + 2)];
			[cell.textLabel setAttributedText:addAttrString];
		}
		else {
			cell.textLabel.text = addString;
		}
    }
    else if (indexPath.section == kSectionPurchaseHxResults) {
		if (indexPath.row < [self.purchaseHxResults count]) {
			PurchaseHxItem *purchaseHxItem = [self.purchaseHxResults objectAtIndex:indexPath.row];

			cell.textLabel.text = purchaseHxItem.name;

			UIImage *productImage = [[BCImageCache sharedCache] imageWithImageId:purchaseHxItem.imageId];

			if (productImage) {
				cell.imageView.image = productImage;
			}
			else {
				cell.imageView.image = [UIImage imageNamed:@"placeholder"];
				if (!self.tableView.dragging && !self.tableView.decelerating && purchaseHxItem.imageId) {
					[self downloadImageWithId:purchaseHxItem.imageId forIndexPath:indexPath];
				}
			}
		}
    }
    else if (indexPath.section == kSectionProductResults) {
		if (indexPath.row < [self.productResults count]) {
			ProductSearchRecord *record = [self.productResults objectAtIndex:indexPath.row];

			BCTableViewCell *bcCell = (BCTableViewCell *)cell;
			bcCell.bcTextLabel.text = record.name;
			bcCell.bcAlertImageView.hidden = [record.allergyCount integerValue] == 0;
			bcCell.bcLeafImageView.hidden = [record.lifestyleCount integerValue] == 0;

			UIImage *productImage = [[BCImageCache sharedCache] imageWithImageId:record.imageId];
			if (productImage) {
				bcCell.bcImageView.image = productImage;
			}
			else {
				bcCell.bcImageView.image = [UIImage imageNamed:@"placeholder"];
			}
		}
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 0.0f;
    switch (section) {
        case kSectionPurchaseHxResults:
            if ([self.purchaseHxResults count] > 0) {
                headerHeight = 22;
            }
            break;
        case kSectionProductResults:
            if ([self.productResults count] > 0) {
                headerHeight = 22;
            }
            break;
        case kSectionAddCustomItem:
        default:
            break;
    }
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = nil;
    switch (section) {
        case kSectionPurchaseHxResults:
            sectionTitle = @"Previously purchased";
            break;
        case kSectionProductResults:
            sectionTitle = @"Search BettrLife";
            break;
        case kSectionAddCustomItem:
        default:
            break;
    }
    
	return [BCTableViewController customViewForHeaderWithTitle:sectionTitle];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44.0f;
    switch (indexPath.section) {
        case kSectionPurchaseHxResults:
			if (indexPath.row == [self.purchaseHxResults count] && self.morePurchaseHx) {
				rowHeight = 44.0f;
			}
			else {
				rowHeight = 66.0f;
			}
			break;
        case kSectionProductResults:
			if (indexPath.row == [self.productResults count] && self.moreProducts) {
				rowHeight = 44.0f;
			}
			else {
				rowHeight = 66.0f;
			}
			break;
		default:
        case kSectionAddCustomItem:
			rowHeight = 44.0f;
			break;
	}	
	return rowHeight;
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [theTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case kSectionAddCustomItem:
            // add a custom item with this text
			[self.searchTextField resignFirstResponder];
            [self addFreeTextItem];
            break;
        case kSectionPurchaseHxResults:
		{
			if (indexPath.row < [self.purchaseHxResults count]) {
				// TODO
			}
			else {
				[self loadPurchaseHx:self.purchaseHxPage * kSearchPageSize];
			}

			break;
		}
        case kSectionProductResults:
		{
			if (indexPath.row < [self.productResults count]) {
				// TODO
			}
			else {
				[self sendLoadMoreRequest];
			}

			break;
		}
        default:
            break;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	self.searchTerm = self.searchTextField.text;
	[self.searchTextField resignFirstResponder];
    
	if ([self.searchTextField.text length] > 0) {
        // clear search results and send a new search request
		[self resetSearch];

		[self loadPurchaseHx:kRowsPerSection];
        [self sendFirstSearchRequest];

		// remove the constraint tying the table view to the footer view and 
		// tie it to the bottom of the view instead
		[self.view removeConstraint:self.tableViewBottomConstraint];
		self.tableViewBottomConstraint = 
			[NSLayoutConstraint constraintWithItem:self.tableView
										 attribute:NSLayoutAttributeBottom
										 relatedBy:NSLayoutRelationEqual
											toItem:self.view
										 attribute:NSLayoutAttributeBottom
										multiplier:1.0
										  constant:0];
		[self.view addConstraint:self.tableViewBottomConstraint];

		// 'hide' the footer view under the bottom of the view
		self.footerViewBottomConstraint.constant = -44;

		[UIView animateWithDuration:0.3f animations:^{
			[self.view layoutIfNeeded];
		}];

		self.footerImageView.image = [UIImage imageNamed:@"product-search-footer"];
		self.filtersLabel.text = @"";
		self.footerView.hidden = YES;
	}

	return YES;
}

- (IBAction)searchTextFieldEditingChanged:(id)sender
{
	self.searchTerm = self.searchTextField.text;

    [self loadPurchaseHx:kRowsPerSection];
    [self.tableView reloadData];
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - ProductSearchFilterViewDelegate
- (void)productSearchFilterView:(ProductSearchFilterViewController *)filterView didFinish:(BOOL)done
{
	self.searchFilters = filterView.filters;
	
	// the filters have changes, so we should start over
	[self resetSearch];

	// slight hack
	self.moreProducts = YES;
	[self.tableView reloadData];

	[self sendLoadMoreRequest];
}

#pragma mark - PantryItemViewController delegate
- (void)pantryItemView:(PantryItemViewController *)pantryItemView didUpdateItem:(PantryItem *)item
{
	[self.navigationController popViewControllerAnimated:YES];

	// shouldn't do much here, just let the shopping list view know about the item
	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCSearchViewDelegate)]) {
		[self.delegate searchView:self didFinish:NO withSelection:item];
	}
}

@end
