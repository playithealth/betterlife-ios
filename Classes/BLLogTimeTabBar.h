//
//  BLLogTimeTabBar.h
//  BettrLife
//
//  Created by Greg Goodrich on 4/29/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLLogTimeTabBar : UIControl
typedef NS_ENUM(NSUInteger, LTTBActiveTab) { ATTotal = 0, ATBreakfast, ATMorningSnack, ATLunch, ATAfternoonSnack, ATDinner, ATEveningSnack };

@property (assign, nonatomic) LTTBActiveTab selectedTab;

@end
