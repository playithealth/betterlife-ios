//
//  RefineProductSearchViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/13/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductSearchFilterViewDelegate;

@interface ProductSearchFilterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) id<ProductSearchFilterViewDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) NSArray *filters;
@property (strong, nonatomic) NSArray *searchBrands;
@property (strong, nonatomic) NSArray *searchCategories;
@property (strong, nonatomic) NSArray *healthyChoices;

- (IBAction)doneTapped:(id)sender;

@end

@protocol ProductSearchFilterViewDelegate <NSObject>
- (void)productSearchFilterView:(id)filterView didFinish:(BOOL)done;
@end
