//
//  StoreMapLocation.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 5/23/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreLocation/CoreLocation.h"
#import "MapKit/MapKit.h"

@interface StoreMapLocation : NSObject <MKAnnotation> {
    CLLocationCoordinate2D _coordinate;
}

- (StoreMapLocation *)initWithCoordinate:(CLLocationCoordinate2D)initCoordinate title:(NSString *)initTitle;
- (CLLocation *)location;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, assign) NSInteger arrayIndex;

@end
