//
//  ShoppingListItemViewItemCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/1/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StarRatingControl;

@interface ShoppingListItemViewItemCell : UITableViewCell 
{
	UIImageView *imageView;
	UILabel *textLabel;
}
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel *textLabel;
@property (nonatomic, strong) StarRatingControl *ratingControl;

@end
