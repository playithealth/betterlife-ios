//
//  RegisterAuthCodeViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/5/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "RegisterAuthCodeViewController.h"

#import "SingleUseToken.h"
#import "Store.h"
#import "User.h"

@implementation RegisterAuthCodeViewController
@synthesize authCodeLabel;
@synthesize showThisCodeLabel;
@synthesize getCodeButton;
@synthesize thisWillMarkLabel;
@synthesize cancelButton;
@synthesize purchaseButton;

#pragma mark - lifecycle
- (void)viewDidLoad
{
	authCodeLabel.text = @"";
    [super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated
{
	[self.navigationController setNavigationBarHidden:YES animated:animated];

	// Want to receive notifications regarding syncCheck finishing
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(syncSingleUseTokensDidFinish:)
		name:@"syncCheck" object:nil];

	[super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated
{
	[self getSingleUseToken:nil];

	[super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
	// Remove the notification observer for changes to ShoppingList
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:[SingleUseToken entityName] object:nil];

	[self.navigationController setNavigationBarHidden:NO animated:animated];

	[super viewWillDisappear:animated];
}
- (void)viewDidUnload
{
    [self setAuthCodeLabel:nil];
    [self setShowThisCodeLabel:nil];
    [self setGetCodeButton:nil];
    [self setThisWillMarkLabel:nil];
    [self setCancelButton:nil];
    [self setPurchaseButton:nil];
    [super viewDidUnload];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation
{
	return (orientation != UIInterfaceOrientationPortraitUpsideDown);
}
- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)orientation 
duration:(NSTimeInterval)duration
{
	if (orientation == UIInterfaceOrientationPortrait 
			|| orientation == UIInterfaceOrientationPortraitUpsideDown) {
		showThisCodeLabel.frame = CGRectMake(42, 170, 236, 22);
		authCodeLabel.frame = CGRectMake(20, 200, 280, 60);
		getCodeButton.frame = CGRectMake(92, 282, 137, 37);
		thisWillMarkLabel.frame = CGRectMake(20, 370, 280, 22);
		cancelButton.frame = CGRectMake(20, 403, 74, 37);
		purchaseButton.frame = CGRectMake(208, 403, 92, 37);
	}
	else {
		showThisCodeLabel.frame = CGRectMake(112, 30, 236, 22);
		authCodeLabel.frame = CGRectMake(30, 70, 420, 60);
		getCodeButton.frame = CGRectMake(172, 160, 137, 37);
		thisWillMarkLabel.frame = CGRectMake(30, 210, 420, 22);
		cancelButton.frame = CGRectMake(20, 243, 74, 37);
		purchaseButton.frame = CGRectMake(348, 243, 92, 37);
	}
}

#pragma mark - local methods
- (void)syncSingleUseTokensDidFinish:(NSNotification *)notification
{
	//QuietLog(@"%s", __FUNCTION__);
	[self getSingleUseToken:nil];
}

#pragma mark - Button responders
- (IBAction)purchaseClicked:(id)sender
{
	__typeof__(self) __weak weakself = self;
	[self dismissViewControllerAnimated:YES completion:^{
		[BCObjectManager markItemsInCartPurchasedInMOC:weakself.managedObjectContext];

		// Clear the current store
		[Store clearCurrentStore];
	}];
}
- (IBAction)cancelClicked:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)getSingleUseToken:(id)sender
{
	NSEntityDescription *singleUseEntity = [NSEntityDescription 
		entityForName:@"SingleUseToken" inManagedObjectContext:self.managedObjectContext];
	
	// filter for the user
	User *thisUser = [User currentUser];
	NSPredicate *unusedPredicate = 
		[NSPredicate predicateWithFormat:@"(accountId = %@) AND (timeViewed = 0)", 
			thisUser.accountId];
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	fetchRequest.entity = singleUseEntity;
	fetchRequest.predicate = unusedPredicate;

	NSArray *array = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];

	if ([array count] > 0) {
		SingleUseToken *unusedToken = [array objectAtIndex:0];
		authCodeLabel.text = unusedToken.token;
		unusedToken.timeViewed = [NSNumber numberWithInteger:
			[[NSDate date] timeIntervalSince1970]];

		NSError *error = nil;
		if (![self.managedObjectContext save:&error]) {
			DDLogError(@"(%@)[%ld] %@", error.domain, (long)error.code,
				[error localizedDescription]);
			// TODO present error
			return;
		}
	}
	else {
		authCodeLabel.text = @"";
		[BCObjectManager syncCheck:SendAndReceive];
	}
}

@end
