//
//  BLOnboardFinalViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 3/31/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BCViewController.h"
#import "BLOnboardViewDelegate.h"

@interface BLOnboardFinalViewController : BCViewController <BLOnboardViewDelegate>
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *viewStack;
@property (weak, nonatomic) id <BLOnboardDelegate> delegate;
@end
