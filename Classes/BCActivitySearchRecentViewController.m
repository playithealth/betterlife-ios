//
//  BCActivitySearchRecentViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/18/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCActivitySearchRecentViewController.h"

#import "BCActivitySearchDataModel.h"
#import "BCProgressHUD.h"
#import "TrainingActivityMO.h"

@implementation BCActivitySearchRecentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataModel.trainingActivitiesGroupedByDate.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"NormalCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];

	UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTapped:)];
	[cell.contentView addGestureRecognizer:recognizer];

	NSDictionary *activitiesDict = self.dataModel.trainingActivitiesGroupedByDate[indexPath.row];
	NSArray *activities = activitiesDict[@"activities"];
    
    cell.textLabel.text = activitiesDict[TrainingActivityMOAttributes.activity];
	cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%lu activities)", activitiesDict[TrainingActivityMOAttributes.date], (unsigned long)[activities count]];

    return cell;
}

#pragma mark - responders
- (void)contentViewTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	// get the activity list at this indexPath
	NSDictionary *activitiesDict = self.dataModel.trainingActivitiesGroupedByDate[indexPath.row];

	// get the embedded array
	NSArray *activities = activitiesDict[@"activities"];

	for (TrainingActivityMO *original in activities) {
		[self.dataModel insertActivityBasedOnTrainingActivity:original];
	}

	[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%lu activities added to your log", (unsigned long)[activities count]]
		onView:self.view.superview];
}

@end
