//
//  AboutViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/11/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "AboutViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "UIColor+Additions.h"

@interface AboutViewController ()

@property (weak, nonatomic) IBOutlet UILabel *appVersionLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end


@implementation AboutViewController
#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

	self.navigationController.toolbarHidden = YES;

	self.webView.layer.borderWidth = 1.0f;
	self.webView.layer.cornerRadius = 6.0f;
	
	NSString *marketingVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
	NSArray *tokens = [marketingVersion componentsSeparatedByString:@"."];
	self.appVersionLabel.text = [NSString stringWithFormat:@"Version %@.%@ (Build %@)",
		[tokens objectAtIndex:0], [tokens objectAtIndex:1], [tokens objectAtIndex:2]];

	[self loadDocument:@"privacy-policy.html" inView:self.webView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - local methods
-(void)loadDocument:(NSString*)documentName inView:(UIWebView*)inWebView
{
    NSString *path = [[NSBundle mainBundle] pathForResource:documentName ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [inWebView loadRequest:request];
}

@end
