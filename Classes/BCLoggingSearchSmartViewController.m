//
//  BCLoggingSearchSmartViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/9/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCLoggingSearchSmartViewController.h"

#import "BCAppDelegate.h"
#import "BCDetailViewDelegate.h"
#import "BCImageCache.h"
#import "BCProgressHUD.h"
#import "BCSearchViewDelegate.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "FoodLogDetailViewController.h"
#import "FoodLogMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "MealPredictionMO.h"
#import "NSCalendar+Additions.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "RecipeMO.h"
#import "UIColor+Additions.h"
#import "UIImage+Additions.h"
#import "UITableView+DownloadImage.h"
#import "User.h"


@interface BCLoggingSearchSmartViewController ()
@property (strong, nonatomic) NSMutableArray *loggedFoods;
@property (strong, nonatomic) NSMutableArray *mealPredictions;
@property (strong, nonatomic) NSCalendar *calendar;
@end

@implementation BCLoggingSearchSmartViewController

static const NSInteger kSectionEntree = 0;
static const NSInteger kSectionPredictions = 1;
static const NSInteger kSectionEmptyMessage = 2;
static const NSInteger kNumberOfSections = 3;

#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

	self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

	[[NSNotificationCenter defaultCenter] addObserverForName:[FoodLogMO entityName] object:nil queue:nil
		usingBlock:^(NSNotification *notification) {
			_loggedFoods = nil;
		}];

	[[NSNotificationCenter defaultCenter] addObserverForName:[MealPredictionMO entityName] object:nil queue:nil
		usingBlock:^(NSNotification *notification) {
			_mealPredictions = nil;
		}];
}

- (void)viewDidAppear:(BOOL)animated
{
    if ([self.mealPredictions count]) {
		[self loadImagesForOnscreenRows];
	}

	[super viewDidAppear:animated];
}

- (void)viewWillDisappearAnimated:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowSmartDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		MealPredictionMO *prediction = self.mealPredictions[indexPath.row];

		NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
		FoodLogMO *scratchLog = [FoodLogMO insertFoodLogWithPrediction:prediction inContext:scratchContext];
		scratchLog.date = self.logDate;
		scratchLog.logTime = self.logTime;

		FoodLogDetailViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;
		detailView.managedObjectContext = scratchContext;
		detailView.item = scratchLog;
	}
}

#pragma mark - local
- (NSArray *)loggedFoods
{
	if (_loggedFoods) {
		return _loggedFoods;
	}

	_loggedFoods = [[FoodLogMO MR_findAllSortedBy:FoodLogMOAttributes.creationDate ascending:YES 
		withPredicate:[NSPredicate predicateWithFormat:@"%K == %@ AND %K <> %@ AND %K == %@ AND %K BETWEEN {%@, %@}", 
			FoodLogMOAttributes.loginId, [[User currentUser] loginId],
			FoodLogMOAttributes.status, kStatusDelete,
			FoodLogMOAttributes.logTime, _logTime, 
			FoodLogMOAttributes.date, [self.calendar BC_startOfDate:_logDate], [self.calendar BC_endOfDate:_logDate]]
		inContext:self.managedObjectContext] mutableCopy];

	return _loggedFoods;
}

- (NSArray *)mealPredictions
{
	if (_mealPredictions) {
		return _mealPredictions;
	}

	if ([self.loggedFoods count]) {
		// get the entree (the first item in the log sorted by creation date)
		// TODO The logging view is getting suggested sides for ALL logged items, and this spot is not doing that, should it?
		FoodLogMO *entree = self.loggedFoods[0];

		// find the predictions for that entree
		MealPredictionMO *matchingPrediction = [MealPredictionMO MR_findFirstWithPredicate:
			[NSPredicate predicateWithFormat:@"entree == nil AND %K == %@ AND %K == %@ AND %K == %@ AND %K == %@",
				MealPredictionMOAttributes.logTime, self.logTime, 
				MealPredictionMOAttributes.item_type, entree.itemType, 
				MealPredictionMOAttributes.item_id, entree.itemId,
				MealPredictionMOAttributes.login_id, [[User currentUser] loginId]]
			inContext:self.managedObjectContext];
		if (matchingPrediction) {
			NSArray *sidePredictions = [matchingPrediction.sides sortedArrayUsingDescriptors:@[
				[NSSortDescriptor sortDescriptorWithKey:MealPredictionMOAttributes.usageCount ascending:NO],
				[NSSortDescriptor sortDescriptorWithKey:MealPredictionMOAttributes.name ascending:YES]
				]];
			NSMutableArray *predictionsToShow = [NSMutableArray array];
			for (MealPredictionMO *side in sidePredictions) {
				BOOL (^TestPredictionToFoodLog)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					FoodLogMO *log = obj;
					if ([side.item_type isEqualToString:log.itemType] && [side.item_id isEqualToNumber:log.itemId]) {
						*stop = YES;
						return YES;
					}
					return NO;
				};

				BOOL (^TestPredictionToPrediction)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					MealPredictionMO *prediction = obj;
					if ([side.item_type isEqualToString:prediction.item_type]
							&& [side.item_id isEqualToNumber:prediction.item_id]) {
						*stop = YES;
						return YES;
					}
					return NO;
				};

				// make sure that the item isn't a dupe
				NSUInteger foodLogIndex = [self.loggedFoods indexOfObjectPassingTest:TestPredictionToFoodLog];
				NSUInteger predictionsIndex = [predictionsToShow indexOfObjectPassingTest:TestPredictionToPrediction];
				if (foodLogIndex == NSNotFound && predictionsIndex == NSNotFound) {
					[predictionsToShow addObject:side];
				}
			}
			_mealPredictions = predictionsToShow;
		}
	}
	else {
		NSArray *entreePredictions = [MealPredictionMO MR_findAllSortedBy:MealPredictionMOAttributes.usageCount
			ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"entree == nil AND %K == %@ AND %K == %@",
				MealPredictionMOAttributes.logTime, self.logTime, MealPredictionMOAttributes.login_id, [[User currentUser] loginId]]
			inContext:self.managedObjectContext];

		NSMutableArray *predictionsToShow = [NSMutableArray array];
		for (MealPredictionMO *entree in entreePredictions) {
			BOOL (^TestPredictionToPrediction)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
				MealPredictionMO *prediction = obj;
				if ([entree.item_type isEqualToString:prediction.item_type] 
						&& [entree.item_id isEqualToNumber:prediction.item_id]) {
					*stop = YES;
					return YES;
				}
				return NO;
			};

			// make sure that the item isn't a dupe
			NSUInteger predictionsIndex = [predictionsToShow indexOfObjectPassingTest:TestPredictionToPrediction];
			if (predictionsIndex == NSNotFound) {
				[predictionsToShow addObject:entree];
			}
		}
		_mealPredictions = predictionsToShow;
	}

	return _mealPredictions;
}

- (void)clearSmartData
{
	_loggedFoods = nil;
	_mealPredictions = nil;
}

- (NSString *)servingsString:(NSNumber *)number 
{
    static NSNumberFormatter *_numberFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _numberFormatter = [[NSNumberFormatter alloc] init];
        [_numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[_numberFormatter setMinimumFractionDigits:0];
		[_numberFormatter setMaximumFractionDigits:2];
    });

    return [_numberFormatter stringFromNumber:number];
}

- (void)reloadDataCache
{
	// Ensure that the image trackers don't try to update indexPaths until after we refresh
	[self.tableView clearTrackerIndexPaths];

	[self clearSmartData];

	[self.tableView reloadData];

	[self loadImagesForOnscreenRows];
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];

	for (NSIndexPath *indexPath in visiblePaths) {
		if (indexPath.section == kSectionPredictions) {
			MealPredictionMO *mealPrediction = self.mealPredictions[indexPath.row];
			if (mealPrediction.image_idValue > 0) {
				[self.tableView downloadImageWithImageId:mealPrediction.image_id forIndexPath:indexPath withSize:kImageSizeMedium];
			}
		}
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return kNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
    if (section == kSectionEntree) {
		numberOfRows = ([self.loggedFoods count] ? 1 : 0);
	}
	else if (section == kSectionPredictions) {
		numberOfRows = [self.mealPredictions count];
	}
	else {
		if (!self.mealPredictions.count) {
			numberOfRows = 1;
		}
	}
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *basicCellID = @"BasicCell";
    static NSString *entreeCellID = @"EntreeCell";
    static NSString *emptyCellID = @"EmptyCell";
    static NSString *firstTimeCellID = @"FirstTimeCell";

    UITableViewCell *cell = nil;
    if (indexPath.section == kSectionEntree) {
		cell = [tableView dequeueReusableCellWithIdentifier:entreeCellID forIndexPath:indexPath];
	}
	else if (indexPath.section == kSectionPredictions) {
		cell = [tableView dequeueReusableCellWithIdentifier:basicCellID forIndexPath:indexPath];
	}
	else { 
		if ([self.loggedFoods count]) {
			cell = [tableView dequeueReusableCellWithIdentifier:emptyCellID forIndexPath:indexPath];
		}
		else {
			cell = [tableView dequeueReusableCellWithIdentifier:firstTimeCellID forIndexPath:indexPath];
		}
	}
    
	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionEntree) {
		NSSet *foodNames = [self.loggedFoods valueForKeyPath:FoodLogMOAttributes.name];
		cell.textLabel.text = [NSString stringWithFormat:@"Based on %@", [[[foodNames allObjects] sortedArrayUsingSelector:@selector(compare:)] componentsJoinedByString:@", "]];
	}
	else if (indexPath.section == kSectionPredictions) {
		MealPredictionMO *mealPrediction = self.mealPredictions[indexPath.row];

		// the primary label just shows the name of the prediction
		cell.textLabel.text = mealPrediction.name;

		// the sub label should show something like "2 servings, 250 calories"
		cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ serving%@, %ld calories", [self servingsString:mealPrediction.servings],
					(mealPrediction.servingsValue > 1) ? @"s" : @"", (long)(mealPrediction.nutrition.caloriesValue * mealPrediction.servingsValue)];
		
		cell.imageView.image = [[BCImageCache sharedCache] imageWithImageId:mealPrediction.image_id
			withItemType:mealPrediction.item_type withSize:kImageSizeMedium];
	}
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 44.0f;
	if (indexPath.section == kSectionEmptyMessage) {
		if ([self.loggedFoods count]) {
			height = self.tableView.bounds.size.height - self.tableView.contentInset.top - self.tableView.contentInset.bottom - 44;
		}
		else {
			height = self.tableView.bounds.size.height - self.tableView.contentInset.top - self.tableView.contentInset.bottom;
		}
	}
	return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Just in case, only act on cells in the predictions section. The others shouldn't be selectable, but this will
	// ensure that we don't do something bad
	if (indexPath.section != kSectionPredictions) {
		return;
	}

	MealPredictionMO *prediction = self.mealPredictions[indexPath.row];

	FoodLogMO *foodLog = [FoodLogMO insertFoodLogWithPrediction:prediction inContext:self.managedObjectContext];
	foodLog.date = self.logDate;
	foodLog.logTime = self.logTime;
	// Save it
	[self.managedObjectContext BL_save];

	NSString *labelText = [NSString stringWithFormat:@"%@ added to your log", foodLog.name];

	[BCProgressHUD notificationWithText:labelText onView:self.view.superview];

	// Deselect this cell now that we're done, use an animation to make this not abrupt
	[[tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];

	[self reloadDataCache];
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	DDLogInfo(@"logging search smart - view:%@ didUpdateObject:%@", viewController, object);

	[self dismissViewControllerAnimated:YES
		completion:^{
			if (object) {
				// pass this back to the logging view
				[self.delegate view:viewController didUpdateObject:object];
			}
		}];
}

#pragma mark - BLLoggingFoodSearchDelegate
- (void)didUpdateLoggedFoods
{
	[self reloadDataCache];
}

- (void)didUpdateMealPredictions
{
	[self reloadDataCache];
}

@end
