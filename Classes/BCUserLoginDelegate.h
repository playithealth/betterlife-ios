//
//  BCUserLoginDelegate.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 12/4/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#ifndef BuyerCompass_BCUserLoginDelegate_h
#define BuyerCompass_BCUserLoginDelegate_h

#import <Foundation/Foundation.h>

@protocol BCUserLoginDelegate <NSObject>
@required
- (void)userDidLogin:(id)sender;
@end

#endif
