//
//  AccountRegistrationView.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/17/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCUserLoginDelegate.h"
#import "PrivacyPolicyViewController.h"

@interface AccountRegistrationView : UIViewController <PrivacyPolicyViewControllerDelegate>

@property (weak, nonatomic) id<BCUserLoginDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
