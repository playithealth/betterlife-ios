//
//  RefineProductSearchViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/13/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "ProductSearchFilterViewController.h"

#import "BCChoiceTableViewController.h"
#import "NumberValue.h"
#import "NSArray+NestedArrays.h"
#import "StarRatingControl.h"
#import "UIColor+Additions.h"

@interface ProductSearchFilterViewController ()<BCChoiceTableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *filterTypes;
@property (strong, nonatomic) NSArray *ratings;
@end

@implementation ProductSearchFilterViewController

static const NSInteger kSectionCategories = 0;
static const NSInteger kSectionBrands = 1;
static const NSInteger kSectionRatings = 2;

static const NSInteger kTagRatingControl = 10001;

#pragma mark - view life cycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.filterTypes = @[@"Categories", @"Brands", @"Ratings"];
		self.filters = @[ [NSMutableArray array], [NSMutableArray array], [NSMutableArray array]];

		self.ratings = @[
			@ { @"labelText" : @"*", @"rating" : @1 },
			@ { @"labelText" : @"**", @"rating" : @2 },
			@ { @"labelText" : @"***", @"rating" : @3 },
			@ { @"labelText" : @"****", @"rating" : @4 },
			@ { @"labelText" : @"*****", @"rating" : @5 }];
	}
	return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return(interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - local methods
- (IBAction)doneTapped:(id)sender
{
	__typeof__(self) __weak weakself = self;
	[self dismissViewControllerAnimated:YES completion:^{
		[weakself.delegate productSearchFilterView:self didFinish:YES];
	}];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0) {
		// show the filter type
		// I could have made these headers, but the first row in each section was easier
		cell.textLabel.text = [self.filterTypes objectAtIndex:indexPath.section];
	}
	else {
		StarRatingControl *ratingControl = (StarRatingControl *)[cell.contentView viewWithTag:kTagRatingControl];
		// if there is an existing rating control, remove it.
		if (ratingControl) {
			[ratingControl removeFromSuperview];
		}

		// because I hacked the headers into the sections, need to adjust the row of the indexpath by 1
		NSIndexPath *filterIndexPath = [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section];
		NSDictionary *filterDict = [self.filters BC_nestedObjectAtIndexPath:filterIndexPath];
		if (indexPath.section == kSectionRatings) {
			NSNumber *rating = [filterDict objectForKey:@"rating"];
			ratingControl = [[StarRatingControl alloc] initWithFrame:CGRectMake(85, 6, 160, 32)];
			ratingControl.userInteractionEnabled = NO;
			ratingControl.emptyStar = [UIImage imageNamed:@"star-large-03.png"];
			ratingControl.fullStar = [UIImage imageNamed:@"star-large-01.png"];
			ratingControl.rating = [rating integerValue];
			ratingControl.tag = kTagRatingControl;
			[cell.contentView addSubview:ratingControl];

			cell.textLabel.text = nil;
		}
		else {
			cell.textLabel.text = [filterDict objectForKey:@"labelText"];
		}
	}
}

- (void)removeFilterTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
	NSIndexPath *filterIndexPath = [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section];

	[self.filters BC_removeObjectAtIndexPath:filterIndexPath];

	[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
	 withRowAnimation:UITableViewRowAnimationFade];
}

- (void)setSearchCategories:(NSArray *)inCategories
{
	// These come with category, category_id and cnt
	NSMutableArray *newSearchCategories = [NSMutableArray array];

	for (NSDictionary *inCat in inCategories) {
		NSMutableDictionary *copyCat = [inCat mutableCopy];
		NSNumber *categoryId = [[inCat valueForKey:@"category_id"] numberValueDecimal];
		[copyCat setValue:categoryId forKey:@"category_id"];
		NSString *labelText = [NSString stringWithFormat:@"%@ (%@)",
							   [inCat valueForKey:@"category"], [inCat valueForKey:@"cnt"]];
		[copyCat setValue:labelText forKey:@"labelText"];

		[newSearchCategories addObject:copyCat];
	}

	_searchCategories = newSearchCategories;
}

- (void)setSearchBrands:(NSArray *)inBrands
{
	// These come with brand, brand_id and cnt
	NSMutableArray *newSearchBrands = [NSMutableArray array];

	for (NSDictionary *inBrand in inBrands) {
		if ([inBrand objectForKey:@"brand"] != [NSNull null]) {
			NSMutableDictionary *copyBrand = [inBrand mutableCopy];
			NSNumber *brandId = [[inBrand valueForKey:@"brand_id"] numberValueDecimal];
			[copyBrand setValue:brandId forKey:@"brand_id"];
			NSString *labelText = [NSString stringWithFormat:@"%@ (%@)",
								   [inBrand valueForKey:@"brand"], [inBrand valueForKey:@"cnt"]];
			[copyBrand setValue:labelText forKey:@"labelText"];

			[newSearchBrands addObject:copyBrand];
		}
	}

	_searchBrands = newSearchBrands;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return [self.filterTypes count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = [self.filters BC_countOfNestedArrayAtIndex:section] + 1;
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *filterTypeCellId = @"FilterTypeCell";
	static NSString *filterCellId = @"FilterCell";

	UITableViewCell *cell = nil;
	if (indexPath.row == 0) {
		cell = [tableView dequeueReusableCellWithIdentifier:filterTypeCellId];
	}
	else {
		cell = [tableView dequeueReusableCellWithIdentifier:filterCellId];
		//cell.indentationLevel = 1;

		cell.contentView.backgroundColor = [UIColor BC_lightGrayColor];
		cell.textLabel.backgroundColor = [UIColor BC_lightGrayColor];
		cell.textLabel.textColor = [UIColor BC_mediumDarkGrayColor];

		UIImageView *closeButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn-gray-x"]];
		closeButton.userInteractionEnabled = YES;
		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeFilterTapped:)];
		[closeButton addGestureRecognizer:recognizer];
		cell.accessoryView = closeButton;
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)theTableView
	didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	switch (indexPath.section) {
		case kSectionCategories:
		{
			BCChoiceTableViewController *choiceView = [self.storyboard instantiateViewControllerWithIdentifier:@"ChoiceTableView"];
			[choiceView addSection:@{ @"title" : [NSNull null], @"rows" : self.searchCategories }];
			choiceView.headerTitle = @"Choose Categories";
			choiceView.delegate = self;
			choiceView.tag = kSectionCategories;
			[self presentViewController:choiceView animated:YES completion:nil];

			break;
		}
		case kSectionBrands:
		{
			BCChoiceTableViewController *choiceView = [self.storyboard instantiateViewControllerWithIdentifier:@"ChoiceTableView"];
			[choiceView addSection:@{ @"title" : [NSNull null], @"rows" : self.searchBrands }];
			choiceView.headerTitle = @"Choose Brands";
			choiceView.delegate = self;
			choiceView.tag = kSectionBrands;
			[self presentViewController:choiceView animated:YES completion:nil];

			break;
		}
		case kSectionRatings:
		{
			BCChoiceTableViewController *choiceView = [self.storyboard instantiateViewControllerWithIdentifier:@"ChoiceTableView"];
			[choiceView addSection:@{ @"title" : [NSNull null], @"rows" : self.ratings }];
			choiceView.headerTitle = @"Choose Ratings";
			choiceView.delegate = self;
			choiceView.tag = kSectionRatings;
			[self presentViewController:choiceView animated:YES completion:nil];

			break;
		}
		default:
			break;
	}
}


#pragma mark - BCChoiceTableViewDelegate
- (void)BCChoiceView:(BCChoiceTableViewController *)choiceView didFinish:(BOOL)finish
{
	NSMutableArray *filters = [self.filters objectAtIndex:choiceView.tag];
	NSUInteger index = 0;
	for (NSDictionary *section in choiceView.sections) {
		for (NSDictionary *rowData in [section objectForKey : @"rows"]) {
			if ([[rowData valueForKey:@"selected"] boolValue]) {
				switch (choiceView.tag) {
					case kSectionCategories :
					{
						NSDictionary *filterDict = [self.searchCategories objectAtIndex:index];
						[filters addObject:filterDict];
						break;
					}
					case kSectionBrands:
					{
						NSDictionary *filterDict = [self.searchBrands objectAtIndex:index];
						[filters addObject:filterDict];
						break;
					}
					case kSectionRatings:
					{
						NSDictionary *filterDict = [self.ratings objectAtIndex:index];
						[filters addObject:filterDict];
						break;
					}
					default:
						break;
				}
			}
			index++;
		}
	}

	__typeof__(self) __weak weakself = self;
	[self dismissViewControllerAnimated:YES completion:^{
		[weakself.tableView reloadData];
	}];
}

@end
