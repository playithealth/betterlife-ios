//
//  BLOnboardNutritionGoalsViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 3/28/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLOnboardNutritionGoalsViewController.h"

#import "BCDetailViewDelegate.h"
#import "BCUtilities.h"
#import "BLHeaderFooterMultiline.h"
#import "BLOnboardLifestyleViewController.h"
#import "BLOnboardNutrientGoalViewController.h"
#import "BLOnboardWelcomeViewController.h"
#import "NSCalendar+Additions.h"
#import "NutrientMO.h"
#import "NutritionMO.h"
#import "TrackingMO.h"
#import "UserProfileMO.h"

@interface BLOnboardNutritionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nutrientLabel;
@property (weak, nonatomic) IBOutlet UILabel *goalLabel;
@end
@implementation BLOnboardNutritionCell
@end

@interface BLOnboardUseRatiosCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UISwitch *useRatiosSwitch;
@end
@implementation BLOnboardUseRatiosCell
@end

@interface BLOnboardRatioPickerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@end
@implementation BLOnboardRatioPickerCell
@end

typedef NS_ENUM(NSUInteger, ActivePicker) { APNone, APCarbRatio, APFatRatio, APProteinRatio };

@interface BLOnboardNutritionGoalsViewController () <BCDetailViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) UserProfileMO *userProfileMO;
@property (strong, nonatomic) NSArray *nutrients; ///< All the nutrients from the Nutrients entity for dynamic cell creation
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSNumber *calorieCalculatorGoal;
@property (assign, nonatomic) BOOL isWeightLoss;
@property (assign, nonatomic) ActivePicker activePicker;
@end

@implementation BLOnboardNutritionGoalsViewController

enum {
	RowCalories = 0,
	RowUseRatios,
	RowCarbRatio,
	RowCarbRatioPicker,
	RowProteinRatio,
	RowProteinRatioPicker,
	RowFatRatio,
	RowFatRatioPicker,
	RowRemainingNutrients
};

#pragma mark - life cycle
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

	[self.tableView registerClass:[BLHeaderFooterMultiline class] forHeaderFooterViewReuseIdentifier:@"Multiline"];

	if (self.viewStack) {
		self.title = [NSString stringWithFormat:@"Welcome: %ld of %ld",
			(long)[[self.navigationController viewControllers] count] - 1, (long)[self.viewStack count] - 1];
	}
	else {
		self.title = @"Nutrition Goals";
		self.navigationItem.rightBarButtonItem = nil;
	}

	// Load up the current user profile
	self.userProfileMO = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

	// No point in attempting to perform BMR if we don't at least have the following things
	NSNumber *currentWeight = [TrackingMO getCurrentWeightInMOC:self.managedObjectContext];
	if (self.userProfileMO.height && self.userProfileMO.gender && self.userProfileMO.birthday && currentWeight) {

		NSInteger nutritionEffort = 0;
		if ([currentWeight doubleValue] < [self.userProfileMO.goalWeight doubleValue]) {
			// Weight gain
			nutritionEffort = [self.userProfileMO.nutritionEffort integerValue];
			self.isWeightLoss = NO;
		}
		else {
			// Weight loss (don't support 'maintain', so this is interpreted as loss)
			nutritionEffort = -1 * [self.userProfileMO.nutritionEffort integerValue];
			self.isWeightLoss = YES;
		}

		NSCalendar *calendar = [NSCalendar currentCalendar];
		NSNumber *bmr = [BCUtilities calculateBmrForGender:[self.userProfileMO.gender integerValue]
			ageInYears:[calendar BC_yearsFromDate:self.userProfileMO.birthday toDate:[NSDate date]]
			weightInPounds:[currentWeight doubleValue]
			heightInInches:[self.userProfileMO.height doubleValue] usingEquation:kBMREquationMifflinStJeor];

		self.calorieCalculatorGoal = @((int)([bmr doubleValue] * [self.userProfileMO.activityLevel doubleValue]
			+ nutritionEffort + 0.5));
	}

	// This is important, as nutrient sync uses a delete strategy, so these can just go away if they re-sync
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadNutrients:) name:[NutrientMO entityName] object:nil];
	// Load up the nutrients
	self.nutrients = [NutrientMO MR_findAllSortedBy:NutrientMOAttributes.sortOrder ascending:YES
		inContext:self.managedObjectContext];
}

- (void)viewWillAppear:(BOOL)animated
{
	if (!self.isBeingPresented && self.selectedIndexPath) {
		// Coming back from editing a nutrient's min/max, update it

		[self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
	}

	[super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.nutrients count] + RowRemainingNutrients - 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;

	switch (indexPath.row) {
		case RowUseRatios: {
			BLOnboardUseRatiosCell *rCell = [tableView dequeueReusableCellWithIdentifier:@"UseRatios" forIndexPath:indexPath];
			cell = rCell;
			rCell.useRatiosSwitch.on = [self.userProfileMO.useAutoRatios boolValue];
			break;
		}

		case RowCarbRatio: {
			BLOnboardNutritionCell *nCell = [tableView dequeueReusableCellWithIdentifier:@"Ratio" forIndexPath:indexPath];
			cell = nCell;
			nCell.nutrientLabel.text = @"Carb Ratio";
			nCell.goalLabel.text = self.userProfileMO.totalCarbohydrateRatio ? [NSString stringWithFormat:@"%@%%",
				self.userProfileMO.totalCarbohydrateRatio] : @"";
			break;
        }
		case RowProteinRatio: {
			BLOnboardNutritionCell *nCell = [tableView dequeueReusableCellWithIdentifier:@"Ratio" forIndexPath:indexPath];
			cell = nCell;
			nCell.nutrientLabel.text = @"Protein Ratio";
			nCell.goalLabel.text = self.userProfileMO.proteinRatio ? [NSString stringWithFormat:@"%@%%", self.userProfileMO.proteinRatio]
				: @"";
			break;
        }
        case RowFatRatio: {
			BLOnboardNutritionCell *nCell = [tableView dequeueReusableCellWithIdentifier:@"Ratio" forIndexPath:indexPath];
			cell = nCell;
			nCell.nutrientLabel.text = @"Fat Ratio";
			nCell.goalLabel.text = self.userProfileMO.totalFatRatio ? [NSString stringWithFormat:@"%@%%", self.userProfileMO.totalFatRatio]
				: @"";
			break;
        }
		case RowCarbRatioPicker:
		case RowProteinRatioPicker:
		case RowFatRatioPicker: {
			BLOnboardRatioPickerCell *pickerCell = [tableView dequeueReusableCellWithIdentifier:@"RatioPicker" forIndexPath:indexPath];
			cell = pickerCell;
			pickerCell.pickerView.tag = indexPath.row;
			pickerCell.pickerView.delegate = self;
			pickerCell.pickerView.dataSource = self;
			pickerCell.pickerView.showsSelectionIndicator = YES;
			NSNumber *value = nil;
			// I know this seems odd, but it allows me to put common code above and below this switch, and then nest specific
			// code within the switch, so, less lines of code overall, slightly better maintainability
			switch (indexPath.row) {
				case RowCarbRatioPicker:
					value = self.userProfileMO.totalCarbohydrateRatio;
					break;
				case RowProteinRatioPicker:
					value = self.userProfileMO.proteinRatio;
					break;
				case RowFatRatioPicker:
					value = self.userProfileMO.totalFatRatio;
					break;
			}
			[pickerCell.pickerView selectRow:(value ? [value integerValue] + 1 : 0) inComponent:0 animated:NO];
			break;
		}

		case RowCalories:
		case RowRemainingNutrients:
		default: {
			// Regular nutrient cells
			BLOnboardNutritionCell *nCell = [tableView dequeueReusableCellWithIdentifier:@"Nutrient" forIndexPath:indexPath];
			cell = nCell;

			// Strange math on the indexPath.row to get the proper index for the nutrients array for all but calories
			NutrientMO *nutrient = [self.nutrients objectAtIndex:(indexPath.row ? indexPath.row - RowRemainingNutrients + 1 : 0)];
			nCell.nutrientLabel.text = nutrient.displayName;
			NSNumber *minValue = [self.userProfileMO.minNutrition valueForKey:nutrient.name];
			NSNumber *maxValue = [self.userProfileMO.maxNutrition valueForKey:nutrient.name];
			if ([nutrient.name isEqualToString:NutritionMOAttributes.calories] && [self.userProfileMO.useBMR boolValue]
					&& (self.calorieCalculatorGoal)) {
				nCell.goalLabel.text = [NSString stringWithFormat:@"[%@%@%@]", (self.isWeightLoss ? @"<" : @">"),
					self.calorieCalculatorGoal, (nutrient.units ?: @"")];
			}
			else if (minValue && maxValue) {
				nCell.goalLabel.text = [NSString stringWithFormat:@"%@-%@%@", minValue, maxValue, (nutrient.units ?: @"")];
			}
			else if (minValue) {
				nCell.goalLabel.text = [NSString stringWithFormat:@">%@%@", minValue, (nutrient.units ?: @"")];
			}
			else if (maxValue) {
				nCell.goalLabel.text = [NSString stringWithFormat:@"<%@%@", maxValue, (nutrient.units ?: @"")];
			}
			else {
				// No values
				nCell.goalLabel.text = @"";
			}
			break;
		}
	}
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	switch (indexPath.row) {
		case RowUseRatios:
			break;

		case RowCarbRatio:
		case RowProteinRatio:
        case RowFatRatio: {
			// Picker selected
			NSMutableArray *reloadPaths = [[NSMutableArray alloc] init];
			NSIndexPath *pickerIndexPath = [NSIndexPath indexPathForRow:([indexPath row] + 1) inSection:[indexPath section]];
			NSIndexPath *currentPath = nil;
			switch (self.activePicker) {
				case APNone:
					break;
				case APCarbRatio:
					currentPath = [NSIndexPath indexPathForRow:RowCarbRatioPicker inSection:0];
					break;
				case APFatRatio:
					currentPath = [NSIndexPath indexPathForRow:RowFatRatioPicker inSection:0];
					break;
				case APProteinRatio:
					currentPath = [NSIndexPath indexPathForRow:RowProteinRatioPicker inSection:0];
					break;
			}

			BOOL isOnlyCollapsing = NO;
			// If there is an active picker, we will be collapsing it, so add its index path to our reload
			if (currentPath) {
				[reloadPaths addObject:currentPath];
				// If the click was on the same row that activates this picker, then the user is simply collapsing the current picker,
				// and not opening a different one, so no point in doing more
				isOnlyCollapsing = ([pickerIndexPath compare:currentPath] == NSOrderedSame ? YES : NO);
			}

			BOOL reload = YES;
			self.activePicker = APNone;
			if (!isOnlyCollapsing) {
				// Now find the picker that we're expanding, and mark it active
				switch ([indexPath row]) {
					case RowCarbRatio:
						self.activePicker = APCarbRatio;
						break;
					case RowFatRatio:
						self.activePicker = APFatRatio;
						break;
					case RowProteinRatio:
						self.activePicker = APProteinRatio;
						break;
					default:
						reload = NO;
						break;
				}
				if (reload) {
					[reloadPaths addObject:pickerIndexPath];
				}
			}

			if (reload) {
				[tableView reloadRowsAtIndexPaths:reloadPaths withRowAnimation:UITableViewRowAnimationAutomatic];
			}
			break;
		}

		case RowCarbRatioPicker:
		case RowProteinRatioPicker:
		case RowFatRatioPicker: {
		}

		case RowCalories:
		case RowRemainingNutrients:
		default: {
			BLOnboardNutrientGoalViewController *nutrientGoalView = [self.storyboard
				instantiateViewControllerWithIdentifier:@"Onboard Nutrient Goal"];
			NutrientMO *nutrientMO = [self.nutrients objectAtIndex:(indexPath.row ? indexPath.row - RowRemainingNutrients + 1 : 0)];
			nutrientGoalView.managedObjectContext = self.managedObjectContext;
			nutrientGoalView.nutrientName = nutrientMO.name;
			nutrientGoalView.nutrientDisplayName = nutrientMO.displayName;
			nutrientGoalView.nutrientUnits = nutrientMO.units;
			nutrientGoalView.userProfileMO = self.userProfileMO;
			if ([nutrientMO.name isEqualToString:NutritionMOAttributes.calories]) {
				nutrientGoalView.calorieCalculatorGoal = self.calorieCalculatorGoal;
			}
			nutrientGoalView.delegate = self;

			self.selectedIndexPath = indexPath;

			[self presentViewController:nutrientGoalView animated:YES completion:nil];
			break;
		}
	}
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = UITableViewAutomaticDimension;

	switch (indexPath.row) {
		case RowCarbRatio:
		case RowProteinRatio:
		case RowFatRatio:
			// Hide these rows if we're not using auto ratios
			if (![self.userProfileMO.useAutoRatios boolValue]) {
				height = 0;
			}
			break;
		case RowCarbRatioPicker:
			height = (self.activePicker == APCarbRatio ? 160 : 0);
			break;
		case RowProteinRatioPicker:
			height = (self.activePicker == APProteinRatio ? 160 : 0);
			break;
		case RowFatRatioPicker:
			height = (self.activePicker == APFatRatio ? 160 : 0);
			break;

		default:
			break;
	}

	return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 84;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];

	headerView.multilineLabel.text = @"All of this information is optional, but the more you enter, "
		"the more we can do to help you manage your health.";
	headerView.normalLabel.text = @"DAILY NUTRITION GOALS";

	return headerView;
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	if (object && [object isKindOfClass:[UserProfileMO class]]) {
		[object setStatus:kStatusPut];
		[[object managedObjectContext] BL_save];
	}

	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - responders
- (IBAction)nextViewController:(id)sender {
	if (self.delegate) {
		[self.delegate nextView];
	}
}

- (void)reloadNutrients:(NSNotification *)notification
{
	self.nutrients = [NutrientMO MR_findAllSortedBy:NutrientMOAttributes.sortOrder ascending:YES
		inContext:self.managedObjectContext];

	[self.tableView reloadData];
}

- (IBAction)useRatiosChanged:(UISwitch *)sender {
    self.userProfileMO.useAutoRatios = @(sender.on);

	// If we're hiding the ratio rows, ensure that there isn't an active picker
	if (!sender.on) {
		self.activePicker = APNone;
	}

	NSArray *indexPaths = @[
		[NSIndexPath indexPathForRow:RowCarbRatio inSection:0],
		[NSIndexPath indexPathForRow:RowCarbRatioPicker inSection:0],
		[NSIndexPath indexPathForRow:RowProteinRatio inSection:0],
		[NSIndexPath indexPathForRow:RowProteinRatioPicker inSection:0],
		[NSIndexPath indexPathForRow:RowFatRatio inSection:0],
		[NSIndexPath indexPathForRow:RowFatRatioPicker inSection:0],
	];

    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	// 'none', 0 - 100
	return 102;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	// Zeroth row is 'none', all other rows represent one greater than their value
	NSString *title = @"none";
	if (row) {
		title = [NSString stringWithFormat:@"%ld%%", (long)row - 1];
	}

	return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	NSNumber *newValue = (row ? @(row - 1) : nil);
	NSArray *reloadIndexPaths = nil;
	switch (pickerView.tag) {
		case RowCarbRatioPicker:
			self.userProfileMO.totalCarbohydrateRatio = newValue;
			reloadIndexPaths = @[[NSIndexPath indexPathForRow:RowCarbRatio inSection:0]];
			break;
		case RowProteinRatioPicker:
			self.userProfileMO.proteinRatio = newValue;
			reloadIndexPaths = @[[NSIndexPath indexPathForRow:RowProteinRatio inSection:0]];
			break;
		case RowFatRatioPicker:
			self.userProfileMO.totalFatRatio = newValue;
			reloadIndexPaths = @[[NSIndexPath indexPathForRow:RowFatRatio inSection:0]];
			break;
	}

	[self.tableView reloadRowsAtIndexPaths:reloadIndexPaths withRowAnimation:UITableViewRowAnimationNone];
}

@end
