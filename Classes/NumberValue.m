//
//  NumberValue.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/12/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "NumberValue.h"



@implementation NSNumber (numberValue)
- (NSNumber *)numberValueWithNumberStyle:(NSNumberFormatterStyle)style
{
	return self;
}
- (NSNumber *)numberValueDecimal
{
	return self;
}
@end

@implementation NSString (numberValue)
- (NSNumber *)numberValueWithNumberStyle:(NSNumberFormatterStyle)style
{
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	formatter.numberStyle = style;
	NSNumber *number = [formatter numberFromString:self];
	return number;
}
- (NSNumber *)numberValueDecimal
{
	return [self numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
}
@end

@implementation NSNull (numberValue)
- (NSNumber *)numberValueWithNumberStyle:(NSNumberFormatterStyle)style
{
	return nil;
}
- (NSNumber *)numberValueDecimal
{
	return nil;
}
@end

