//
//  InboxMessageCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/7/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface InboxMessageCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *statusImage;
@property (nonatomic, weak) IBOutlet UILabel *fromLabel;
@property (nonatomic, weak) IBOutlet UILabel *subjectLabel;
@property (nonatomic, weak) IBOutlet UILabel *summaryLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;

@end
