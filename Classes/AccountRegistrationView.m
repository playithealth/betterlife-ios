//
//  AccountRegistrationView.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/17/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "AccountRegistrationView.h"

#import "BCColorButton.h"
#import "BCTextFieldCell.h"
#import "BCURLFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"
#import "MBProgressHUD.h"
#import "NSArray+NestedArrays.h"
#import "NSString+BCAdditions.h"
#import "NumberValue.h"
#import "OAuthConsumer.h"
#import "PrivacyPolicyViewController.h"
#import "UIColor+Additions.h"
#import "User.h"
#import "UserProfileMO.h"

@interface AccountRegistrationView () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *privacyButton;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmTextField;
@property (strong, nonatomic) UIToolbar *keyboardToolbar;
@property (assign, nonatomic) BOOL privacyPolicyAccepted;

- (IBAction)createAccountTapped:(id)sender;
- (void)resignKeyboard:(id)sender;

@end

@implementation AccountRegistrationView

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
	if (!self.keyboardToolbar) {
		self.keyboardToolbar = [[UIToolbar alloc] initWithFrame:
			CGRectMake(0, 0, self.view.bounds.size.width, 35)];
		self.keyboardToolbar.barStyle = UIBarStyleBlack;
		self.keyboardToolbar.translucent = YES;

		UIBarButtonItem *previousButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Previous"
			style:UIBarButtonItemStyleBordered
			target:self action:@selector(previous:)];

		UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Next"
			style:UIBarButtonItemStyleBordered
			target:self action:@selector(next:)];

		UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
			initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
			target:nil action:nil];

		UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Done" style:UIBarButtonItemStyleDone
			target:self action:@selector(resignKeyboard:)];

		NSArray *buttons = [NSArray arrayWithObjects:
			previousButton, nextButton, flexibleSpace, doneButton, nil];

		self.keyboardToolbar.items = buttons;
	}
	self.nameTextField.inputAccessoryView = self.keyboardToolbar;
	self.emailTextField.inputAccessoryView = self.keyboardToolbar;
	self.zipCodeTextField.inputAccessoryView = self.keyboardToolbar;
	self.passwordTextField.inputAccessoryView = self.keyboardToolbar;
	self.confirmTextField.inputAccessoryView = self.keyboardToolbar;

	self.scrollView.contentSize = self.contentView.bounds.size;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowPrivacyPolicy"]) {
		PrivacyPolicyViewController *policyView = segue.destinationViewController;
		policyView.delegate = self;

		self.navigationItem.title = @"Cancel";
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Local methods
- (IBAction)createAccountTapped:(id)sender
{
	[self resignKeyboard:nil];

	// check to see if all the fields are filled out
	// and if the password and confirm match, etc
	NSString *name = self.nameTextField.text;
	NSString *email = self.emailTextField.text;
	NSString *zipCode = self.zipCodeTextField.text;
	NSString *password = self.passwordTextField.text;
	NSString *confirm = self.confirmTextField.text;

	NSMutableArray *errors = [[NSMutableArray alloc] init];
	if (!self.privacyPolicyAccepted) {
		[errors addObject:@"Privacy policy must be accepted"];
	}
	if ([name length] < 2) {
		[errors addObject:@"Name is too short"];
	}
	else if ([name length] > 255) {
		[errors addObject:@"Name is too long"];
	}

	if (![email length]) {
		[errors addObject:@"email is required"];
	}
	else {
		NSError *error = NULL;
		NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
									@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$"
			options:NSRegularExpressionCaseInsensitive error:&error];

		NSUInteger matches = [regex numberOfMatchesInString:email options:0 range:NSMakeRange(0, [email length])];

		if (!matches) {
			[errors addObject:@"email is invalid"];
		}
	}

	if (![password length]) {
		[errors addObject:@"password is required"];
	}
	else if ([password length] < 6 || [password length] > 40) {
		[errors addObject:@"password must be between 6 and 40 characters"];
	}
	else if (![password isEqualToString:confirm]) {
		[errors addObject:@"passwords must match"];
	}

	if ([errors count]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errors"
			message:[errors componentsJoinedByString:@"\n"]
			delegate:self
			cancelButtonTitle:@"Try again"
			otherButtonTitles:nil];
		[alert show];
	}
	else {
		OAuthConsumer *thisConsumer = [OAuthConsumer currentOAuthConsumer];
		if (![thisConsumer consumerIsValid]) {
			// TODO pop up a warning
			return;
		}

		NSDictionary *accountInfo = @{ 
			kName : name,
			kEmail : email,
			kZip : zipCode,
			kPassword : [password BC_passwordHash],
			kOAuthConsumerKey : [thisConsumer oauthConsumerKey] };

		NSData *json = [NSJSONSerialization dataWithJSONObject:accountInfo options:kNilOptions error:nil];

		MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
		hud.labelText = @"Validating account information";

		GTMHTTPFetcher* fetcher = [GTMHTTPFetcher fetcherWithURL:[BCUrlFactory accountURL] httpMethod:kPost postData:json];
		[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
			NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			[MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];

			if (error != nil) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:nil];
				if ([resultsDict isKindOfClass:[NSDictionary class]]
						&& ([resultsDict objectForKey:kErrors] != nil)) {
					NSArray *errors = [resultsDict objectForKey:kErrors];
					DDLogError(@"Account creation errors: %@", errors);
					UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errors"
						message:[errors componentsJoinedByString:@"\n"]
						delegate:self
						cancelButtonTitle:@"Try again"
						otherButtonTitles:nil];
					[alert show];
				}
			} 
			else {
				// fetch succeeded
				if ([resultsDict isKindOfClass:[NSDictionary class]]
						&& ([resultsDict objectForKey:kLoginUserId] != nil)) {
					DDLogInfo(@"Account created, results:%@", [resultsDict description]);

					// grab the account, login, and OAuth info out of the response
					User *thisUser = [User currentUser];
					thisUser.loginId = [[resultsDict objectForKey:kLoginUserId] numberValueDecimal];
					thisUser.email = email;
					thisUser.oauthAccessToken = [resultsDict objectForKey:kOAuthAccessToken];
					thisUser.oauthSecret = [resultsDict objectForKey:kOAuthSecret];

					// Create/fetch the new user profile for this user, and populate the permissions, etc
					UserProfileMO *userProfileMO = thisUser.userProfile;
					if (!userProfileMO) {
						userProfileMO = [UserProfileMO createUserProfileInMOC:self.managedObjectContext];
					}
					[userProfileMO setWithUpdateDictionary:resultsDict];
					userProfileMO.name = name;
					userProfileMO.zipCode = zipCode;
					userProfileMO.email = email;
					[userProfileMO updatePermissions:[resultsDict objectForKey:kPermission]];
					[self.managedObjectContext BL_save];

					if (self.delegate != nil
							&& [self.delegate conformsToProtocol:@protocol(BCUserLoginDelegate)]) {
						[self.delegate userDidLogin:self];
					}
				}
				else {
					DDLogError(@"Account creation failed, %@", [resultsDict description]);
				}
			}
		}];
	}
}

- (IBAction)previous:(id)sender
{
	if ([self.nameTextField isFirstResponder]) {
		[self.confirmTextField becomeFirstResponder];
	}
	else if ([self.emailTextField isFirstResponder]) {
		[self.nameTextField becomeFirstResponder];
	}
	else if ([self.zipCodeTextField isFirstResponder]) {
		[self.emailTextField becomeFirstResponder];
	}
	else if ([self.passwordTextField isFirstResponder]) {
		[self.zipCodeTextField becomeFirstResponder];
	}
	else if ([self.confirmTextField isFirstResponder]) {
		[self.passwordTextField becomeFirstResponder];
	}
}

- (IBAction)next:(id)sender
{
	if ([self.nameTextField isFirstResponder]) {
		[self.emailTextField becomeFirstResponder];
	}
	else if ([self.emailTextField isFirstResponder]) {
		[self.zipCodeTextField becomeFirstResponder];
	}
	else if ([self.zipCodeTextField isFirstResponder]) {
		[self.passwordTextField becomeFirstResponder];
	}
	else if ([self.passwordTextField isFirstResponder]) {
		[self.confirmTextField becomeFirstResponder];
	}
	else if ([self.confirmTextField isFirstResponder]) {
		[self.nameTextField becomeFirstResponder];
	}
}

- (void)resignKeyboard:(id)sender
{
	[self.emailTextField becomeFirstResponder];
	[self.emailTextField resignFirstResponder];
}

#pragma mark - PrivacyPolicyViewControllerDelegate
- (void)privacyPolicyViewDidAccept:(PrivacyPolicyViewController *)controller
{
	self.privacyPolicyAccepted = YES;
	self.privacyButton.enabled = NO;

	[self.navigationController popViewControllerAnimated:YES];
}

@end
