//
//  ProductPurchaseHistoryView.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/9/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCViewController.h"

@protocol PurchasedItem;

@interface ProductPurchaseHistoryView : BCViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong) id<PurchasedItem> purchasedItem;
@property (strong) NSArray *purchaseHxItems;
@end

@protocol PurchasedItem <NSObject>
- (NSString *)name;
- (NSNumber *)productId;
@end

