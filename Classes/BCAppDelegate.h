//
//  BCAppDelegate.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCSidebarViewController.h"
#import "BCUserLoginDelegate.h"
#import "Reachability.h"

@interface BCAppDelegate : NSObject <BCUserLoginDelegate, UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) IBOutlet UINavigationController *navigationController;
@property (strong, nonatomic) IBOutlet BCSidebarViewController *sidebarView;
@property (strong, nonatomic) Facebook *facebook;
@property (strong, nonatomic) DDFileLogger *fileLogger;
@property (strong, nonatomic) NSManagedObjectContext *adManagedObjectContext; ///< Named differently for easy searches, refactor to private

//=== Reachability
@property (strong, nonatomic) Reachability *hostReach;
@property (strong, nonatomic) Reachability *internetReach;
@property (strong, nonatomic) Reachability *wifiReach;
@property (assign, nonatomic) BOOL connected;
//===

- (void)showSidebar:(BOOL)show disableClosing:(BOOL)disableClosing;

- (BOOL)resetCoreData;

- (void)setNetworkActivityIndicatorVisible:(BOOL)visible;

- (UIViewController *)gotoInitialViewController;
- (UIViewController *)gotoViewWithStoryboardId:(NSString *)storyboardId inStoryboardNamed:(NSString *)storyboardName;
- (UIViewController *)gotoViewWithStoryboardId:(NSString *)storyboardId inStoryboardNamed:(NSString *)storyboardName
withProperties:(NSDictionary *)propertiesDict;
- (UIViewController *)gotoInitialViewControllerInStoryboardNamed:(NSString *)storyboardName;
- (UIViewController *)gotoInitialViewControllerInStoryboardNamed:(NSString *)storyboardName withProperties:(NSDictionary *)propertiesDict;

- (void)authenticationFailure;
- (void)gotoLogin;

@end

