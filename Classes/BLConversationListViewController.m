//
//  BLConversationListViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 5/20/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLConversationListViewController.h"

#import "BCImageCache.h"
#import "BCObjectManager.h"
#import "BLConversationViewController.h"
#import "ConversationMO.h"
#import "MessageDeliveryMO.h"
#import "MessageMO.h"
#import "UITableView+DownloadImage.h"
#import "User.h"

#pragma mark - Custom Cells
@interface ConversationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *senderImageView;
@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *unreadMessagesImageView;
@end
@implementation ConversationCell
@end

#pragma mark - Conversation List View
@interface BLConversationListViewController () <BLConversationDelegate>
@property (strong, nonatomic) NSArray *conversations;
@property (strong, nonatomic) NSArray *newestMessages;
@end

@implementation BLConversationListViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
	[self reloadData];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:[MessageMO entityName] object:nil];

	[BCObjectManager syncCheck:SendAndReceive];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];

	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - local
- (void)loadConversations
{
	NSFetchRequest *request = [ConversationMO MR_createFetchRequestInContext:self.managedObjectContext];
	request.predicate = [NSPredicate predicateWithFormat:@"%K = %@",
		ConversationMOAttributes.loginId, [[User currentUser] loginId]];

	NSArray *conversations = [self.managedObjectContext executeFetchRequest:request error:nil];

	// Now filter out any conversations that have no messages to the logged in user (only sent from the logged in user), as we have
	// decided that this is the way we wish it to work, specifically so that a coach that sends a message to a group with 1000 users
	// won't have 1000 conversations that have no response from the client
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY messageDeliveries.loginId = %@", [[User currentUser] loginId]];
	conversations = [conversations filteredArrayUsingPredicate:predicate];

	// Now order these conversations by their newest messages
	// NOTE!!!: This comparator block is intentionally sorting DESC!!!
	self.conversations = [conversations sortedArrayUsingComparator: ^(ConversationMO *obj1, ConversationMO *obj2) {
	 
	 	NSUInteger obj1CreatedOn = [[((MessageDeliveryMO*)[obj1.newestMessage firstObject]).message createdOn] integerValue];
		NSUInteger obj2CreatedOn = [[((MessageDeliveryMO*)[obj2.newestMessage firstObject]).message createdOn] integerValue];
		if (obj1CreatedOn < obj2CreatedOn) {
			return (NSComparisonResult)NSOrderedDescending;
		}
	 
		if (obj1CreatedOn > obj2CreatedOn) {
			return (NSComparisonResult)NSOrderedAscending;
		}
		return (NSComparisonResult)NSOrderedSame;
	}];

	// Pre-load all the attributed strings for two reasons, one, they're slow to generate, and two, they seem to process the runloop,
	// causing issues like the download image category methods to fire while the table is being generated, causing range errors
	NSMutableArray *newestMessages = [NSMutableArray array];
	NSMutableAttributedString *attributedString;
	for (ConversationMO *conversationMO in self.conversations) {
		MessageDeliveryMO *newestMessage = [conversationMO.newestMessage firstObject];
		if (newestMessage) {
			if ([NSMutableAttributedString instancesRespondToSelector:@selector(initWithData:options:documentAttributes:error:)]) {
				attributedString = [[NSMutableAttributedString alloc]
					initWithData:[newestMessage.message.body dataUsingEncoding:NSUTF8StringEncoding]
					options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
						NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
					documentAttributes:nil error:nil];
			}
			else {
				// Punt for now on iOS6 when trying to parse out the html and just display it
				attributedString = [[NSMutableAttributedString alloc] initWithString:newestMessage.message.body];
			}
		}
		else {
			attributedString = [[NSMutableAttributedString alloc] initWithString:@""];
		}

		[attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15]
			range:NSMakeRange(0, [attributedString length])];
		[newestMessages addObject:attributedString];
	}
	self.newestMessages = newestMessages;
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];

	for (NSIndexPath *indexPath in visiblePaths) {
		ConversationMO *conversationMO = [self.conversations objectAtIndex:indexPath.row];
		if ([conversationMO.conversationUserImageId integerValue]) {
			[self.tableView downloadImageWithImageId:conversationMO.conversationUserImageId forIndexPath:indexPath withSize:kImageSizeMedium];
		}
	}
}

- (void)reloadData
{
	[self loadConversations];
	[self loadImagesForOnscreenRows];
}

- (void)refreshView:(NSNotification *)notification
{
	[self reloadData];
	[self.tableView reloadData];
}

#pragma mark - Conversation Delegate
- (void)updatedConversation:(ConversationMO *)conversation
{
	[self refreshView:nil];
	/*
	NSUInteger index = [self.conversations indexOfObject:conversation];
	if (index != NSNotFound) {
		[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
	}
	*/
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.conversations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConversationCell *cell = (ConversationCell *)[tableView dequeueReusableCellWithIdentifier:@"Conversation" forIndexPath:indexPath];
    
    // Configure the cell...
	ConversationMO *conversationMO = [self.conversations objectAtIndex:indexPath.row];
	NSMutableAttributedString *attributedString = [self.newestMessages objectAtIndex:indexPath.row];

	cell.senderLabel.text = conversationMO.conversationUserName;
	NSString *userType = ([conversationMO.conversationUserGender integerValue] == kGenderFemale ? kItemTypeUserFemale : kItemTypeUserMale);
	cell.senderImageView.image = [[BCImageCache sharedCache] imageWithImageId:conversationMO.conversationUserImageId
		withItemType:userType withSize:kImageSizeMedium];
	cell.unreadMessagesImageView.hidden = [conversationMO.isRead boolValue];
	cell.messageLabel.text = [attributedString string]; // Just show plain text since this is a preview

    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
	[self loadImagesForOnscreenRows];
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ConversationView"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		BLConversationViewController *viewController = [segue destinationViewController];
		viewController.delegate = self;
		viewController.conversation = [self.conversations objectAtIndex:indexPath.row];
	}
}

@end
