//
//  BLLoggingWorkoutActivityCell.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 2/11/15.
//  Copyright (c) 2015 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BLLoggingWorkoutActionView.h"

@protocol BLWorkoutActivityCellDelegate <NSObject>
@optional
- (void)actionViewTapped:(BLLoggingWorkoutActionView *)actionView inCell:(UITableViewCell *)cell;
@end

@interface BLLoggingWorkoutActivityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet BLLoggingWorkoutActionView *actionView;
@property (weak, nonatomic) id delegate;
@end
