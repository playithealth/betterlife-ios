//
//  PlanningViewController.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 8/15/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "PlanningViewController.h"

#import "ActivityPlanMO.h"
#import "AdvisorMealPlanMO.h"
#import "AdvisorMealPlanEntryMO.h"
#import "AdvisorMealPlanPhaseMO.h"
#import "AdvisorMealPlanTimelineMO.h"
#import "BCAppDelegate.h"
#import "BCCalendarView.h"
#import "BCDetailViewDelegate.h"
#import "BCDrawBlockView.h"
#import "BCHealthCoachPlanNutritionViewController.h"
#import "BCImageCache.h"
#import "BCMealPlanInCell.h"
#import "BCObjectManager.h"
#import "BCPermissionsDataModel.h"
#import "BCProgressHUD.h"
#import "BCTableViewCell.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BLHealthCoachPlanViewController.h"
#import "FoodLogDetailViewController.h"
#import "GTMHTTPFetcherAdditions.h"
#import "RecipeMO.h"
#import "MealIngredient.h"
#import "MealPlannerDay.h"
#import "MealPlannerShoppingListItem.h"
#import "NSArray+NestedArrays.h"
#import "NSCalendar+Additions.h"
#import "NSDate+Additions.h"
#import "NumberValue.h"
#import "NutritionMO.h"
#import "PlanningSearchViewController.h"
#import "PlanningTagSearchViewController.h"
#import "ShoppingListItem.h"
#import "UIColor+Additions.h"
#import "UITableView+DownloadImage.h"
#import "User.h"

@interface PlanningItem : NSObject

typedef NS_ENUM(NSInteger, PlannerStatus) {
    PlannerStatusError = -1,
    PlannerStatusUnplanned = 0,
    PlannerStatusComplete = 1
};

typedef NS_ENUM(NSInteger, PlanningItemType) {
	PlanningItemTypeMealPlan,
	PlanningItemTypeAdvisorMealPlanEntry,
	PlanningItemTypeAdvisorMealPlanTag,
	PlanningItemTypeAdvisorMealPlanTagFilled,
	PlanningItemTypeActivity
};

@property (assign, nonatomic) PlanningItemType type;
@property (strong, nonatomic) id data;

+ (id)itemWithData:(id)data andType:(PlanningItemType)type;

@end

@interface PlanningViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PlanningItem
- (id)initWithData:(id)data andType:(PlanningItemType)type
{
    self = [super init];
    if (self) {
		self.data = data;
		self.type = type;
    }
    return self;
}
+ (id)itemWithData:(id)data andType:(PlanningItemType)type
{
	PlanningItem *item = [[PlanningItem alloc] initWithData:data andType:type];
    return item;
}
@end

@interface PlanningViewController() <BCDetailViewDelegate, BCPopupViewDelegate, BCSearchViewDelegate>

@property (weak, nonatomic) IBOutlet BCDrawBlockView *planHeaderView;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UILabel *planNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *planPhaseLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayCaloriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxCaloriesLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerNutritionButton;

@property (strong, nonatomic) UISegmentedControl *headerSegmentControl;

@property (strong, nonatomic) NSCalendar *calendar;
@property (strong, nonatomic) NSDateFormatter *longDateFormatter;
@property (strong, nonatomic) NSDateFormatter *shortDateFormatter;
@property (strong, nonatomic) NSDateFormatter *weekFormatter;

@property (strong, nonatomic) NSDate *originDate;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;

@property (assign, nonatomic) NSNumber *lastAddedMealTime;

@property (strong, nonatomic) AdvisorMealPlanMO *displayedMealPlan;
@property (strong, nonatomic) AdvisorMealPlanPhaseMO *displayedPhase;
@property (strong, nonatomic) NSMutableDictionary *imageFetchersInProgress;
@property (strong, nonatomic) NSMutableArray *itemsBySection;
@property (strong, nonatomic) NSArray *sectionTitles;
@property (assign, nonatomic) BOOL needsSync;
@property (assign, nonatomic) BOOL weekMode;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewTopConstraint;

enum weekDays { Today = 0, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday };
enum mealTimes { Breakfast = 0, MorningSnack, Lunch, AfternoonSnack, Dinner, EveningSnack };
enum displayModes { Day = 0, Week = 1 };
@end


@implementation PlanningViewController

static const NSInteger kSectionHeaderHeight = 30;
static const NSInteger kWeekdays = 7;
static const NSInteger kNumberOfSections = 7;
static const NSInteger kTagCalendarPopup = 12342;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		self.imageFetchersInProgress = [NSMutableDictionary dictionary];

		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		self.weekMode = [defaults boolForKey:@"MealPlannerWeekMode"];

		self.longDateFormatter = [[NSDateFormatter alloc] init];
		[self.longDateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[self.longDateFormatter setDateStyle:NSDateFormatterFullStyle];
		
		self.shortDateFormatter = [[NSDateFormatter alloc] init];
		[self.shortDateFormatter setTimeStyle:NSDateFormatterNoStyle];
		[self.shortDateFormatter setDateStyle:NSDateFormatterMediumStyle];
		
		self.weekFormatter = [[NSDateFormatter alloc] init];
		[self.weekFormatter setDateFormat:@"MMM d"];
		
		self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];

	self.navigationController.toolbarHidden = YES;

	self.headerSegmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Previous", @"Date", @"Next"]];
	[self.headerSegmentControl setImage:[UIImage imageNamed:@"previous"] forSegmentAtIndex:0];
	[self.headerSegmentControl setWidth:30 forSegmentAtIndex:0];
	[self.headerSegmentControl setImage:[UIImage imageNamed:@"next"] forSegmentAtIndex:2];
	[self.headerSegmentControl setWidth:30 forSegmentAtIndex:2];
	[self.headerSegmentControl setWidth:110 forSegmentAtIndex:1];
	self.headerSegmentControl.momentary = YES;
	self.headerSegmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
	[self.headerSegmentControl addTarget:self action:@selector(headerSegmentControlValueChanged:) forControlEvents:UIControlEventValueChanged];
	self.navigationItem.titleView = self.headerSegmentControl;

	if (![BCPermissionsDataModel userHasPermission:kPermissionShop]) {
		self.navigationItem.rightBarButtonItem = nil;
	}

	// origin date is the date that the calendar is based on and
	// what it will switch to when put in daily mode
	self.originDate = [NSDate date];
	self.lastAddedMealTime = @4;

	// load the initial data before doing a refresh
	// this already gets triggered on set origin date above
	// [self reloadAdvisorMealPlan];

	// sync
	[self refresh];
	self.needsSync = NO;

	// Want to receive notifications regarding plans changing
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncCheckFinished:)
		name:[AdvisorMealPlanTimelineMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncCheckFinished:)
		name:[AdvisorMealPlanMO entityName] object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
	[[NSNotificationCenter defaultCenter] addObserver:self 
		selector:@selector(syncCheckFinished:) 
		name:@"syncCheck" object:nil];

    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// Remove the notification observer for syncCheck
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:@"syncCheck" object:nil];

	if (self.needsSync) {
		[BCObjectManager syncCheck:SendOnly];
		self.needsSync = NO;
	}

	[self.tableView cancelAllImageTrackers];

	[super viewWillDisappear:animated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	NSManagedObjectContext *moc = self.managedObjectContext;
	if ([segue.identifier isEqualToString:@"ShowActivityDetail"]) {
		#pragma message "TODO Hook in a new activity detail view here"
		/*
		BCActivityPlanViewController *detailView = segue.destinationViewController;

		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		detailView.plan = [(PlanningItem *)[self.itemsBySection BC_nestedObjectAtIndexPath:indexPath] data];
		detailView.delegate = self;
		*/
	}
	else if ([segue.identifier isEqualToString:@"ShowTagSearch"]) {
		PlanningTagSearchViewController *searchView = segue.destinationViewController;

		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		searchView.tag = [(PlanningItem *)[self.itemsBySection BC_nestedObjectAtIndexPath:indexPath] data];
		NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
		moc = scratchContext;
	}
	else if ([segue.identifier isEqualToString:@"ShowSearch"]) {
		PlanningSearchViewController *searchView = segue.destinationViewController;
		searchView.date = [self getNewItemDate];
		searchView.logTime = self.lastAddedMealTime;
		searchView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"ShowAnySearch"]) {
		PlanningSearchViewController *searchView = segue.destinationViewController;
		searchView.date = [self getNewItemDate];
		searchView.logTime = self.lastAddedMealTime;
		searchView.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"ShowPlanDetail"]) {
		BLHealthCoachPlanViewController *detailView = segue.destinationViewController;
		detailView.plan = self.displayedMealPlan;
	}
	else if ([segue.identifier isEqualToString:@"ShowPlanNutrition"]) {
		BCHealthCoachPlanNutritionViewController *view = segue.destinationViewController;
		// TODO
		// view.itemNutrition = 
		view.minNutrition = self.displayedPhase.minNutrition;
		view.maxNutrition = self.displayedPhase.maxNutrition;
	}
	else if ([segue.identifier isEqualToString:@"ShowMealNutrition"]) {
	}

	if ([segue.destinationViewController respondsToSelector:@selector(setManagedObjectContext:)]) {
		[segue.destinationViewController setManagedObjectContext:moc];
	}
}

// Unwind segue for the Health Coach Plan Nutrition View
- (IBAction)planNutritionViewDone:(UIStoryboardSegue *)segue
{
}

// Unwind segue for the Meal Plan Tag Search View
- (IBAction)planningTagSearchViewDone:(UIStoryboardSegue *)segue
{
	PlanningTagSearchViewController *viewController = segue.sourceViewController;

	if (viewController.selection && [viewController.selection isKindOfClass:[MealPlannerDay class]]) {
		MealPlannerDay *newMP = viewController.selection;
		newMP.date = [self getNewItemDate];

		[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your plan", newMP.name]
			onView:self.navigationController.view];

		// If we were using a scratch context, save it out first
        NSManagedObjectContext *moc = newMP.managedObjectContext;
        while (moc) {
            [moc BL_save];
            moc = [moc parentContext];
        }

		[self reloadAdvisorMealPlan];
		[self reloadMealCache];
		[self loadImagesForOnscreenRows];

		[BCObjectManager syncCheck:SendOnly];
	}
}

- (void)syncCheckFinished:(NSNotification *)notification
{
	[self reloadAdvisorMealPlan];
	[self reloadMealCache];
	[self loadImagesForOnscreenRows];
}

#pragma mark - BCTableView overrides
- (void)refresh
{
	[BCObjectManager syncCheck:SendAndReceive];
}

#pragma mark - local methods
- (void)setOriginDate:(NSDate *)date
{
	_originDate = date;

	if (self.weekMode) {
		[self setWeekRangeFromDate:_originDate];
	}
	else {
		[self setDayRangeFromDate:_originDate];
	}
}

- (NSDate *)getNewItemDate
{
	NSDate *newItemDate = self.startDate;

	// use Sunday or the date we are on unless the week is this week
	if (self.weekMode) {
		if ([[NSDate date] BC_isInSameWeekAs:self.startDate]) {
			// then use today
			newItemDate = [NSDate date];
		}
	}
	return newItemDate;
}

- (void)setWeekRangeFromDate:(NSDate *)inDate
{
	self.weekMode = YES;

	self.startDate = [self.calendar BC_startOfDate:[self.calendar BC_firstOfWeekForDate:inDate]];
	self.endDate = [self.calendar BC_endOfDate:[self.calendar BC_lastOfWeekForDate:inDate]];

	[self dateChanged];
}

- (void)setDayRangeFromDate:(NSDate *)inDate
{
	self.weekMode = NO;
	
	self.startDate = [self.calendar BC_startOfDate:inDate];
	self.endDate = [self.calendar BC_endOfDate:inDate];

	[self dateChanged];
}

- (void)dateChanged
{
	if (self.weekMode) {
		[self.headerSegmentControl setTitle:[NSString stringWithFormat:@"%@ - %@", [self.weekFormatter stringFromDate:self.startDate], [self.weekFormatter stringFromDate:self.endDate]] forSegmentAtIndex:1];

		NSMutableArray *dateStrings = [[NSMutableArray alloc] initWithCapacity:kWeekdays];
		for (NSInteger section = 0; section < kWeekdays; section++) {
			[dateStrings addObject:[self.longDateFormatter stringFromDate:[self.calendar BC_dateByAddingDays:section toDate:self.startDate]]];
		}
		self.sectionTitles = dateStrings;
	}
	else {
		[self.headerSegmentControl setTitle:[self.shortDateFormatter stringFromDate:self.startDate] forSegmentAtIndex:1];
		
		NSMutableArray *sections = [NSMutableArray array];
		if ([BCPermissionsDataModel userHasPermission:kPermissionMealplan]) {
			[sections addObjectsFromArray:@[@"Breakfast", @"Morning Snack", @"Lunch", @"Afternoon Snack", @"Dinner", @"Evening Snack"]];
		}
		if ([BCPermissionsDataModel userHasPermission:kPermissionActivityplan]) {
			[sections addObject:@"Activities"];
		}
		self.sectionTitles = sections;
	}

	[self reloadAdvisorMealPlan];
	[self reloadMealCache];
	[self loadImagesForOnscreenRows];
}

- (void)reloadMealCache
{
	// Ensure that the image trackers don't try to update indexPaths until after we refresh
	[self.tableView clearTrackerIndexPaths];

	// delete the cache
	self.itemsBySection = nil;

	// this will recreate the cache
	[self.tableView reloadData];
}

- (void)updateMealPlanHeader
{
	if (self.weekMode) {
		self.planNameLabel.text = @"You have an active meal plan!";
		[self.planNameLabel sizeToFit];
		self.planPhaseLabel.text = @"Switch to day view.";
		[self.planPhaseLabel sizeToFit];

		self.dayCaloriesLabel.text = nil;
		self.maxCaloriesLabel.text = nil;
	}
	else {
		self.planNameLabel.text = self.displayedMealPlan.planName;
		[self.planNameLabel sizeToFit];
		self.planPhaseLabel.text = [NSString stringWithFormat:@"You are in phase %@ of your meal plan", self.displayedPhase.phaseNumber];
		[self.planPhaseLabel sizeToFit];

		// make a caloric sum
		NSInteger calorieSum = 0;
		for (NSArray *section in self.itemsBySection) {
			for (PlanningItem *planningItem in section) {
				if ((planningItem.type == PlanningItemTypeMealPlan) || (planningItem.type == PlanningItemTypeAdvisorMealPlanTagFilled)) {
					MealPlannerDay *mpd = planningItem.data;
					if (mpd.recipe) {
						if (mpd.recipe.nutrition) {
							calorieSum += [mpd.recipe.nutrition.calories integerValue];
						}
						else {
							calorieSum += [mpd.recipe.calories integerValue];
						}
					}
					else {
						calorieSum += [mpd.nutrition.calories integerValue];
					}
				}
			}
		}

		if (calorieSum > 0) {
			self.dayCaloriesLabel.text = [NSString stringWithFormat:@"%ld", (long)calorieSum];
		}
		else {
			self.dayCaloriesLabel.text = nil;
		}
		self.maxCaloriesLabel.text = [self.displayedPhase.maxNutrition.calories stringValue];
	}
}

- (void)reloadAdvisorMealPlan
{
	self.displayedMealPlan = [AdvisorMealPlanMO getMealPlanForDate:self.originDate inContext:self.managedObjectContext];

	if (self.displayedMealPlan
			&& [BCPermissionsDataModel userHasPermission:kPermissionMealplan]) {
		PlannerStatus overallStatus = PlannerStatusComplete;
		for (NSInteger section = 0; section < kNumberOfSections; section++) {
			PlannerStatus sectionStatus = [self statusOfItemsInSection:section];
			if (sectionStatus != PlannerStatusComplete) {
				overallStatus = sectionStatus;
				break;
			}
		}
		
		UIColor *statusBackgroundColor = nil;
		if (overallStatus == PlannerStatusUnplanned) {
			statusBackgroundColor = [UIColor BC_blueColor];
			[self.headerNutritionButton setImage:[UIImage imageNamed:@"nutrition-unplanned-meal"] forState:UIControlStateNormal];
		}
		else if (overallStatus == PlannerStatusComplete) {
			statusBackgroundColor = [UIColor BC_mediumLightGreenColor];
			[self.headerNutritionButton setImage:[UIImage imageNamed:@"nutrition-complete"] forState:UIControlStateNormal];
		}
		else if (overallStatus == PlannerStatusError) {
			statusBackgroundColor = [UIColor BC_goldColor];
			[self.headerNutritionButton setImage:[UIImage imageNamed:@"error-white"] forState:UIControlStateNormal];
		}

		self.planHeaderView.drawBlock = ^(UIView* view, CGContextRef context) {
			/*
			* draw background color
			*/
			CGContextSetFillColorWithColor(context, [statusBackgroundColor CGColor]);
			CGContextFillRect(context, view.frame);

			/* 
			* Draw the line at the top
			*/
			CGContextSetLineWidth(context, 1.0);
			CGContextSetStrokeColorWithColor(context, [[UIColor BC_mediumDarkGrayColor] CGColor]);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, CGRectGetMinX(view.frame), CGRectGetMinY(view.frame) + 0.5);
			CGContextAddLineToPoint(context, CGRectGetMaxX(view.frame), CGRectGetMinY(view.frame) + 0.5);
			CGContextDrawPath(context, kCGPathStroke);

			/* 
			* Draw the line at the bottom
			*/
			CGContextSetStrokeColorWithColor(context, [[UIColor BC_mediumDarkGrayColor] CGColor]);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, CGRectGetMinX(view.frame), CGRectGetMaxY(view.frame) - 0.5);
			CGContextAddLineToPoint(context, CGRectGetMaxX(view.frame), CGRectGetMaxY(view.frame) - 0.5);
			CGContextDrawPath(context, kCGPathStroke);
		};

		AdvisorMealPlanPhaseMO *phase = [self.displayedMealPlan getPhaseForDate:self.originDate];
		self.displayedPhase = phase;

		self.planHeaderView.hidden = NO;
		[self.planHeaderView setNeedsDisplay];
		
		[self.view removeConstraint:self.searchViewTopConstraint];

		NSDictionary *viewsDictionary = @{ @"planHeaderView" : self.planHeaderView, @"searchView" : self.searchView };
		NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[planHeaderView][searchView]" options:0 metrics:nil views:viewsDictionary];
		self.searchViewTopConstraint = constraints[0];

		[self.view addConstraint:self.searchViewTopConstraint];
	}
	else {
		self.planHeaderView.hidden = YES;
		self.displayedPhase = nil;

		[self.view removeConstraint:self.searchViewTopConstraint];

		NSDictionary *viewsDictionary = @{ @"topGuide" : self.topLayoutGuide, @"searchView" : self.searchView };
		NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide][searchView]" options:0 metrics:nil views:viewsDictionary];
		self.searchViewTopConstraint = constraints[0];

		[self.view addConstraint:self.searchViewTopConstraint];
	}
	
	[self.view setNeedsUpdateConstraints];
	[self.view layoutIfNeeded];

	[self updateMealPlanHeader];
}

- (IBAction)addToListTapped:(id)sender 
{
	NSMutableArray *askItems = [[NSMutableArray alloc] init];
	NSMutableArray *yesItems = [[NSMutableArray alloc] init];

	for (NSArray *section in self.itemsBySection) {
		for (PlanningItem *planningItem in section) {
			if (planningItem.type == PlanningItemTypeMealPlan) {
				MealPlannerDay *mpd = planningItem.data;
				// Don't do anything if this day is in the past
				if ([self.calendar BC_daysFromDate:mpd.date toDate:[NSDate date]] < 0) {
					// Don't do anything if this day is in the past
					continue;
				}

				// Iterate through the ingredients and add the ingredients marked for add
				for (MealIngredient *ingredient in mpd.recipe.ingredients) {
					if ([ingredient.addToShoppingList integerValue] == 0) {
						// TODO May want to process this anyhow, in case of an edit?  Maybe we should
						// check to see if this used to be set to add, and items were added to the SL
						// and we should now remove them?
						continue;
					}

					// See if this ingredient has already been added
					BOOL (^SamePlannerDay)(id, BOOL *) = ^BOOL(id obj, BOOL *stop) {
						MealPlannerShoppingListItem *mpsli = obj;
						return ([mpsli.mealPlannerDay isEqual:mpd]);
					};

					NSSet *matches = [ingredient.mealPlannerShoppingListItems
						objectsPassingTest:SamePlannerDay];
					MealPlannerShoppingListItem *item = [matches anyObject];
					// If the quantity doesn't match, adjust it
					// Start off with the quantity specified in the ingredient
					NSInteger quantity = [ingredient.shoppingListQuantity doubleValue];
					if (item) {
						// Decrement the quantity by any value already placed into the shopping list
						quantity -= [item.quantity doubleValue];
					}

					if (!quantity) {
						// This item has already been added to the SL with the proper quantity,
						// so continue
						continue;
					}

					if ([ingredient.addToShoppingList integerValue] == 2) {
						[askItems addObject:ingredient];
					}
					else {
						[yesItems addObject:ingredient];
					}
				}
			}
		}
	}

	if ([askItems count]) {
		MealPlannerAskViewController *askView = [self.storyboard instantiateViewControllerWithIdentifier:@"MealPlannerAsk"];
		askView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
		askView.delegate = self;
		askView.askItems = askItems;
		askView.yesItems = yesItems;

		[self presentViewController:askView animated:YES completion:nil];
	}
	else if ([yesItems count]) {
		[self processIngredientsAfterAskView];
	}
	else {
		[BCProgressHUD notificationWithText:@"No items added to the shopping list" onView:self.navigationController.view];
	}
}

- (IBAction)headerSegmentControlValueChanged:(id)sender
{
	if ([sender selectedSegmentIndex] == 0) {
		[self previousButtonTapped:sender];
	}
	else if ([sender selectedSegmentIndex] == 1) {
		[self todayButtonTapped:sender];
	}
	else if ([sender selectedSegmentIndex] == 2) {
		[self nextButtonTapped:sender];
	}
}

- (IBAction)previousButtonTapped:(id)sender
{
	if (self.weekMode) {
		self.originDate = [self.calendar BC_dateByAddingDays:-7 toDate:self.originDate];
	}
	else {
		self.originDate = [self.calendar BC_dateByAddingDays:-1 toDate:self.originDate];
	}
}

- (IBAction)nextButtonTapped:(id)sender
{
	if (self.weekMode) {
		self.originDate = [self.calendar BC_dateByAddingDays:7 toDate:self.originDate];
	}
	else {
		self.originDate = [self.calendar BC_dateByAddingDays:1 toDate:self.originDate];
	}
}

- (IBAction)todayButtonTapped:(id)sender 
{
	BCCalendarView *aCalendarView = [[BCCalendarView alloc] initWithBaseDate:[NSDate date] delegate:self];
	aCalendarView.tag = kTagCalendarPopup;

	if (self.weekMode) {
		aCalendarView.selectedWeek = self.startDate;
	}
	else {
		aCalendarView.selectedDay = self.originDate;
	}

	[self.navigationController.view addSubview:aCalendarView];	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)processIngredientsAfterAskView
{
	NSInteger mealCount = 0;
	NSInteger ingredientCount = 0;
	for (NSArray *section in self.itemsBySection) {
		for (PlanningItem *planningItem in section) {
			if (planningItem.type == PlanningItemTypeMealPlan) {
				MealPlannerDay *mpd = planningItem.data;
				// Don't do anything if this day is in the past
				if ([self.calendar BC_daysFromDate:mpd.date toDate:[NSDate date]] < 0) {
					continue;
				}

				mealCount++;
				// Iterate through the ingredients and add the ingredients marked for add
				for (MealIngredient *ingredient in mpd.recipe.ingredients) {
					if ([ingredient.addToShoppingList integerValue] == 0) {
						// This item is marked NO, skip it

						// TODO May want to process this anyhow, in case of an edit?  Maybe we should
						// check to see if this used to be set to add, and items were added to the SL
						// and we should now remove them?
						continue;
					}
					else if ([ingredient.addToShoppingList integerValue] == 2
							&& (!ingredient.answer || [ingredient.answer boolValue] == NO)) {
						// this item is marked ask and they didn't answer YES
						// so skip it
						continue;
					}

					// See if this ingredient has already been added
					BOOL (^SamePlannerDay)(id, BOOL *) = ^BOOL(id obj, BOOL *stop) {
						MealPlannerShoppingListItem *mpsli = obj;
						return ([mpsli.mealPlannerDay isEqual:mpd]);
					};

					NSSet *matches = [ingredient.mealPlannerShoppingListItems
						objectsPassingTest:SamePlannerDay];
					MealPlannerShoppingListItem *item = [matches anyObject];
					// If the quantity doesn't match, adjust it
					// Start off with the quantity specified in the ingredient
					NSInteger quantity = [ingredient.shoppingListQuantity doubleValue];
					if (item) {
						// Decrement the quantity by any value already placed into the shopping list
						quantity -= [item.quantity doubleValue];
					}

					if (!quantity) {
						// This item has already been added to the SL with the proper quantity,
						// so continue
						continue;
					}

					ingredientCount++;
					if (!item) {
						// Add this to the MealPlannerShoppingListItem entity
						MealPlannerShoppingListItem *newItem = [MealPlannerShoppingListItem
							insertInManagedObjectContext:self.managedObjectContext];
						newItem.ingredient = ingredient;
						newItem.mealPlannerDay = mpd;
						[mpd markForSync];
						item = newItem;
					}

					// Set the quantity to whatever the shoppingListQuantity is, we don't want to
					// inadvertently set it to the difference (which is what our local quantity var has)
					item.quantity = ingredient.shoppingListQuantity;

					if (quantity > 0) {
						// Increment (or add)
						ShoppingListItem *slItem = [ShoppingListItem
							addOrIncrementItemNamed:ingredient.shoppingListText
										  productId:ingredient.productId category:ingredient.shoppingListCategory
											barcode:nil quantity:quantity inMOC:self.managedObjectContext];
						item.shoppingListItem = slItem;
					}
					else {
						// We already had a greater quantity, so decrement, user must have
						// lowered the shoppinglistquantity in the ingredient
						if ([item.shoppingListItem.quantity doubleValue] > labs(quantity)) {
							item.shoppingListItem.quantity = @([item.shoppingListItem.quantity doubleValue] - labs(quantity));
							[item.shoppingListItem markForSync];
						}
						else {
							// Quantity going to or below zero, delete
							if ([item.shoppingListItem.status isEqualToString:kStatusPost]) {
								// We're posting, which implies the cloud doesn't know about this object,
								// just delete it instead
								[self.managedObjectContext deleteObject:item.shoppingListItem];
							}
							else {
								item.shoppingListItem.status = kStatusDelete;
							}
						}
					}
				}
			}
		}
	}

	if ([self.managedObjectContext BL_save] && mealCount) {
		// Only show a message if we attempted to add ingredients for at least one meal
		[BCProgressHUD notificationWithText:
			[NSString stringWithFormat:@"Added %ld ingredients for %ld meals to the shopping list",
				(long)ingredientCount, (long)mealCount]
			onView:self.navigationController.view];
	}

	[BCObjectManager syncCheck:SendOnly];
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
	for (NSIndexPath *indexPath in visiblePaths) {
		PlanningItem *planningItem = [self.itemsBySection BC_nestedObjectAtIndexPath:indexPath];
		// if this is a meal, use the meal's image id
		if ((planningItem.type == PlanningItemTypeMealPlan) || (planningItem.type == PlanningItemTypeAdvisorMealPlanTagFilled)) {
			MealPlannerDay *mpd = planningItem.data;

			if (mpd.imageIdValue > 0) {
				[self.tableView downloadImageWithImageId:mpd.imageId forIndexPath:indexPath withSize:kImageSizeMedium];
			}
		}
	}
}

- (IBAction)scopeValueChanged:(id)sender
{
	if ([sender selectedSegmentIndex] == Week) {
		[self setWeekRangeFromDate:self.startDate];
	}
	else {
		[self setDayRangeFromDate:self.startDate];
	}
}

- (NSInteger)statusOfItemsInSection:(NSInteger)section
{
	PlannerStatus sectionStatus = PlannerStatusComplete;
	for (PlanningItem *planningItem in self.itemsBySection[section]) {
		if (planningItem.type != PlanningItemTypeAdvisorMealPlanTagFilled) {
			sectionStatus = PlannerStatusUnplanned;
			break;
		}
	}
	return sectionStatus;
}

- (IBAction)nutritionButtonTapped:(UIButton *)sender
{
	NSMutableDictionary *mealNutrition = [NSMutableDictionary dictionary];

	NSInteger section = sender.tag;
	for (PlanningItem *planningItem in self.itemsBySection[section]) {
		if ((planningItem.type == PlanningItemTypeAdvisorMealPlanTagFilled) || (planningItem.type == PlanningItemTypeMealPlan)) {
			MealPlannerDay *mpd = planningItem.data;
			for (NSString *key in [NutritionMO allKeys]) {
				NSNumber *mealValue = [mealNutrition valueForKey:key];
				NSNumber *mpdValue = [mpd.nutrition valueForKey:key];
				if (mpdValue) {
					if (mealValue) {
						[mealNutrition setValue:@([mpdValue doubleValue] + [mealValue doubleValue]) forKey:key];
					}
					else {
						[mealNutrition setValue:mpdValue forKey:key];
					}
				}
			}
		}
	}

	BCHealthCoachPlanNutritionViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"PlanNutritionView"];
	view.itemNutrition = mealNutrition;
	view.minNutrition = self.displayedPhase.minNutrition;
	view.maxNutrition = self.displayedPhase.maxNutrition;
	[self presentViewController:view animated:YES completion:nil];
}

- (void)fetchRecipeDetailsForRecipeId:(id)recipeId
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	// show a HUD
	BCProgressHUD *hud = [BCProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
	hud.labelText = @"Fetching recipe details";

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory recipeDetailsURLForRecipeId:recipeId]];
	[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		[hud hide:YES];
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
			// fetch succeeded
			NSDictionary *webRecipe = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			RecipeMO *recipe = [RecipeMO insertInManagedObjectContext:self.managedObjectContext];
			recipe.accountId = [[User currentUser] accountId];
			[recipe setWithUpdateDictionary:webRecipe];

			[self.managedObjectContext BL_save];

			[self searchView:self didFinish:NO withSelection:recipe];
		}
	}];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return kNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.itemsBySection BC_countOfNestedArrayAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *mealCellId = @"MealCell";
	static NSString *planTagCellId = @"PlanTagCell";
	static NSString *planAnyCellId = @"PlanAnyCell";
	static NSString *planInCellId = @"PlanInCell";
	static NSString *activityCellId = @"ActivityCell";

	PlanningItem *planningItem = [self.itemsBySection BC_nestedObjectAtIndexPath:indexPath];

	UITableViewCell *cell = nil;
	switch (planningItem.type) {
		case PlanningItemTypeMealPlan:
			cell = [self.tableView dequeueReusableCellWithIdentifier:mealCellId];
			break;
		case PlanningItemTypeAdvisorMealPlanEntry:
			cell = [self.tableView dequeueReusableCellWithIdentifier:planAnyCellId];
			break;
		case PlanningItemTypeAdvisorMealPlanTag:
			cell = [self.tableView dequeueReusableCellWithIdentifier:planTagCellId];
			break;
		case PlanningItemTypeAdvisorMealPlanTagFilled:
			cell = [self.tableView dequeueReusableCellWithIdentifier:planInCellId];
			break;
		case PlanningItemTypeActivity:
			cell = [self.tableView dequeueReusableCellWithIdentifier:activityCellId];
			break;
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	PlanningItem *planningItem = [self.itemsBySection BC_nestedObjectAtIndexPath:indexPath];

	BCTableViewCell *bcCell = (BCTableViewCell *)cell;

	if (planningItem.type == PlanningItemTypeMealPlan) {
		MealPlannerDay *mpd = planningItem.data;
		bcCell.bcTextLabel.text = mpd.name;
		bcCell.bcImageView.image = [[BCImageCache sharedCache] imageWithImageId:mpd.imageId withItemType:mpd.itemType
			withSize:kImageSizeMedium];
	}
	else if (planningItem.type == PlanningItemTypeAdvisorMealPlanEntry) {
		bcCell.bcTextLabel.text = [NSString
			stringWithFormat:@"You may eat whatever you like for %@ as long as it fits in your overall meal plan.",
			self.sectionTitles[indexPath.section]];
	}
	else if (planningItem.type == PlanningItemTypeAdvisorMealPlanTag) {
		AdvisorMealPlanTagMO *tag = planningItem.data;
		bcCell.bcTextLabel.text = tag.name;
		bcCell.bcDetailTextLabel.text = [NSString stringWithFormat:@"Select an item for %@",
			[[self.sectionTitles objectAtIndex:indexPath.section] lowercaseString]];
	}
	else if (planningItem.type == PlanningItemTypeAdvisorMealPlanTagFilled) {
		MealPlannerDay *mpd = planningItem.data;
		BCMealPlanInCell *inCell = (BCMealPlanInCell *)bcCell;
		inCell.bcTextLabel.text = mpd.name;
		inCell.bcImageView.image = [[BCImageCache sharedCache] imageWithImageId:mpd.imageId withItemType:mpd.itemType
			withSize:kImageSizeMedium];
		inCell.bcDetailTextLabel.text = mpd.tag.name;
		inCell.servingsLabel.text = mpd.servingSize;
		inCell.servingsUnitsLabel.text = mpd.servingSizeUnitAbbreviation;
		inCell.caloriesLabel.text = [NSString stringWithFormat:@"%d", (int)([mpd.nutrition.calories doubleValue] + 0.5)];
	}
	else if (planningItem.type == PlanningItemTypeActivity) {
		ActivityPlanMO *plan = planningItem.data;
		bcCell.bcTextLabel.text = plan.activityName;
	}
	else {
		bcCell.bcTextLabel.text = nil;
	}
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
		PlanningItem *planItem = [self.itemsBySection BC_nestedObjectAtIndexPath:indexPath];
        
		// If this day is today or in the future,
		// remove the items from the shopping list
		if ((planItem.type == PlanningItemTypeMealPlan) || (planItem.type == PlanningItemTypeAdvisorMealPlanTagFilled)) {
			MealPlannerDay *mpd = planItem.data;
			AdvisorMealPlanTagMO *tag = nil;
			if ((planItem.type == PlanningItemTypeAdvisorMealPlanTagFilled) && mpd.tag) {
				tag = mpd.tag;
			}
			if ([self.calendar BC_daysFromDate:mpd.date toDate:[NSDate date]] < 0) {
				NSInteger ingredientsCount = 0;
				for (MealPlannerShoppingListItem *item in mpd.shoppingListItems) {
					if (item.shoppingListItem && ![item.shoppingListItem.purchased boolValue]
							&& ![item.shoppingListItem.status isEqualToString:kStatusDelete]) {

						if ([item.shoppingListItem.quantity doubleValue] > [item.quantity doubleValue]) {
							item.shoppingListItem.quantity = @([item.shoppingListItem.quantity doubleValue] - [item.quantity doubleValue]);
							[item.shoppingListItem markForSync];
						}
						else {
							// Quantity going to or below zero, delete
							if ([item.shoppingListItem.status isEqualToString:kStatusPost]) {
								// We're posting, which implies the cloud doesn't know about this object,
								// just delete it instead
								[item.managedObjectContext deleteObject:item.shoppingListItem];
							}
							else {
								item.shoppingListItem.status = kStatusDelete;
							}
						}
						ingredientsCount++;
					}
				}

				if (ingredientsCount) {
					[BCProgressHUD notificationWithText:
							[NSString stringWithFormat:@"Removed %ld ingredients from the shopping list", (long)ingredientsCount]
						onView:self.navigationController.view];
				}
			}

			if ([mpd.status isEqualToString:kStatusPost]) {
				// actually delete the item if it is set to POST and/or has no cloud id yet
				[mpd.managedObjectContext deleteObject:mpd];
			}
			else {
				[mpd setStatus:kStatusDelete];
			}

			[mpd.managedObjectContext BL_save];
			self.needsSync = YES;

			// If this was a filled tag, bring back the unfilled tag
			if (tag) {
				// Replace the MealPlannerDay with an AdvisorMealPlanTagMO and reload that section
				PlanningItem *newItem = [PlanningItem itemWithData:tag andType:PlanningItemTypeAdvisorMealPlanTag];
				[self.itemsBySection BC_replaceObjectAtIndexPath:indexPath withObject:newItem];
				// Need to reload the section, so that the section status gets updated
				[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:[indexPath section]]
					withRowAnimation:UITableViewRowAnimationAutomatic];
			}
			else {
				// Just remove the item, as it wasn't based upon a tag
				[self.itemsBySection BC_removeObjectAtIndexPath:indexPath];
				[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
			}
		}
		else if (planItem.type == PlanningItemTypeAdvisorMealPlanEntry) {
			// this can't be deleted
		}
		else if (planItem.type == PlanningItemTypeAdvisorMealPlanTag) {
			// this can't be deleted
		}
		else if (planItem.type == PlanningItemTypeActivity) {
			ActivityPlanMO *plan = planItem.data;
			if ([plan.status isEqualToString:kStatusPost]) {
				[plan.managedObjectContext deleteObject:plan];
			}
			else {
				[plan setStatus:kStatusDelete];
			}

			[plan.managedObjectContext BL_save];
			self.needsSync = YES;
			[self.itemsBySection BC_removeObjectAtIndexPath:indexPath];
			[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
		}
	}   
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	PlanningItem *planItem = [self.itemsBySection BC_nestedObjectAtIndexPath:indexPath];

	if (planItem.type == PlanningItemTypeAdvisorMealPlanTag) {
		return NO;
	}
	else if (planItem.type == PlanningItemTypeAdvisorMealPlanEntry) {
		return NO;
	}

	return YES;
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 0.0f;
	// hide headers for sections that have no items
	if ([self.itemsBySection BC_countOfNestedArrayAtIndex:section] > 0) {
		headerHeight = kSectionHeaderHeight;
	}
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	NSString *sectionTitle = self.sectionTitles[section];

	UIView *headerView = nil;

	if (self.displayedMealPlan) {
		UIColor *statusBackgroundColor = [UIColor BC_blueColor];
		UIImage *statusImage = [UIImage imageNamed:@"nutrition-unplanned-meal"];

		PlannerStatus sectionStatus = [self statusOfItemsInSection:section];

		if (sectionStatus == PlannerStatusUnplanned) {
			statusBackgroundColor = [UIColor BC_blueColor];
			statusImage = [UIImage imageNamed:@"nutrition-unplanned-meal"];
		}
		else if (sectionStatus == PlannerStatusComplete) {
			statusBackgroundColor = [UIColor BC_mediumLightGreenColor];
			statusImage = [UIImage imageNamed:@"nutrition-complete"];
		}
		else if (sectionStatus == PlannerStatusError) {
			statusBackgroundColor = [UIColor BC_goldColor];
			statusImage = [UIImage imageNamed:@"error-white"];
		}

		CGRect headerFrame = CGRectMake(0, 0, tableView.frame.size.width, kSectionHeaderHeight);
		BCDrawBlockView *drawBlockView = [[BCDrawBlockView alloc] initWithFrame:headerFrame];
		drawBlockView.drawBlock = ^(UIView* view, CGContextRef context) {
			/*
			 * draw the background
			 */
			CGContextSetFillColorWithColor(context, [[UIColor BC_lightGrayColor] CGColor]);
			CGContextFillRect(context, view.bounds);

			/*
			 * draw the lines at the top and bottom
			 */
			CGContextSetLineWidth(context, 1.0);
			CGContextSetStrokeColorWithColor(context, [[UIColor BC_mediumDarkGrayColor] CGColor]);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, CGRectGetMinX(view.bounds), CGRectGetMinY(view.bounds));
			CGContextAddLineToPoint(context, CGRectGetMaxX(view.bounds), CGRectGetMinY(view.bounds));
			CGContextDrawPath(context, kCGPathStroke);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, CGRectGetMinX(view.bounds), CGRectGetMaxY(view.bounds));
			CGContextAddLineToPoint(context, CGRectGetMaxX(view.bounds), CGRectGetMaxY(view.bounds));
			CGContextDrawPath(context, kCGPathStroke);

			/* 
			 * draw the text
			 */
			CGContextSelectFont(context, "Helvetica Neue Bold", 12, kCGEncodingMacRoman);
			CGContextSetTextDrawingMode(context, kCGTextFill);
			CGContextSetFillColorWithColor(context, [[UIColor BC_mediumDarkGrayColor] CGColor]);
			CGContextSetTextMatrix(context, CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0));
			CGContextShowTextAtPoint(context, 6, 20, [sectionTitle cStringUsingEncoding:NSUTF8StringEncoding], [sectionTitle length]);

			/*
			 * draw a blue block
			 */
			CGContextSetFillColorWithColor(context, [statusBackgroundColor CGColor]);
			CGContextFillRect(context, CGRectMake(CGRectGetMaxX(view.bounds) - 40, CGRectGetMinY(view.bounds) + 1, CGRectGetMaxX(view.bounds), CGRectGetMaxY(view.bounds) - 1));

			/*
			 * draw the icon
			 */
			CGRect imageRect = CGRectMake(CGRectGetMaxX(view.bounds) - 30, -5, statusImage.size.width, statusImage.size.height);       
			CGContextTranslateCTM(context, 0, statusImage.size.height);
			CGContextScaleCTM(context, 1.0, -1.0);
			CGContextDrawImage(context, imageRect, statusImage.CGImage);
		};

		UIButton *nutritionButton = [UIButton buttonWithType:UIButtonTypeCustom];
		nutritionButton.frame = CGRectMake(headerFrame.origin.x + headerFrame.size.width - 44, headerFrame.origin.y, 44, headerFrame.size.height);
		nutritionButton.tag = section; // Use this to easily recognize which button was tapped
		[nutritionButton addTarget:self action:@selector(nutritionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
		[drawBlockView addSubview:nutritionButton];

		[drawBlockView setNeedsDisplay];
		headerView = drawBlockView;
	}
	else {
		headerView = [BCTableViewController customViewForHeaderWithTitle:sectionTitle];
	}

	return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	PlanningItem *planItem = [self.itemsBySection BC_nestedObjectAtIndexPath:indexPath];
	if ((planItem.type == PlanningItemTypeMealPlan) || (planItem.type == PlanningItemTypeAdvisorMealPlanTagFilled)) {
		NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
		MealPlannerDay *newMP = (id)[scratchContext objectWithID:[planItem.data objectID]];
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Logging" bundle:nil];
		FoodLogDetailViewController *detailView = [storyboard instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
		detailView.delegate = self;
		detailView.managedObjectContext = scratchContext;
		detailView.item = newMP;
		detailView.detailViewMode = ItemDetailViewModePlanning;

		[self.navigationController presentViewController:detailView animated:YES completion:nil];
	}
}

#pragma mark - Fetched results array
- (NSMutableArray *)itemsBySection
{
    if (_itemsBySection != nil) {
        return _itemsBySection;
    }

	/*
	 * Initialize our storage (multi-dimensional array, top level is each week day or meal time)
	 */
	_itemsBySection = [[NSMutableArray alloc] init];
	for (int idx = 0; idx < 7; idx++) {
		[_itemsBySection addObject:[[NSMutableArray alloc] init]];
	}

	/*
	 * Add the meal plans (and the advisor meal plans)
	 */
	if ([BCPermissionsDataModel userHasPermission:kPermissionMealplan]) {
		NSMutableArray *mealPlans = [[MealPlannerDay MR_findAllSortedBy:@"date,sortOrder" ascending:YES 
			withPredicate:[NSPredicate predicateWithFormat:@"date BETWEEN {%@, %@} AND status <> %@ AND accountId = %@", 
				self.startDate, self.endDate, kStatusDelete, [[User currentUser] accountId]]
			inContext:self.managedObjectContext] mutableCopy];

		if (self.weekMode) {
			for (MealPlannerDay *mealPlan in mealPlans) {
				// As we iterate through the meal planner days ordered by date asc,
				// add it to the array based on the index of the weekday
				NSDateComponents *components = [self.calendar components:(NSWeekdayCalendarUnit) fromDate:mealPlan.date];
				// weekday has to be adjusted by 1 to relate to the index
				[_itemsBySection[components.weekday - 1] addObject:[PlanningItem itemWithData:mealPlan andType:PlanningItemTypeMealPlan]];
			}
		}
		else {
			/*
			* The advisor meal plan events should go in first
			*/
			if (self.displayedMealPlan) {
				AdvisorMealPlanPhaseMO *phase = [self.displayedMealPlan getPhaseForDate:self.originDate];
				for (AdvisorMealPlanEntryMO *entry in phase.entries) {
					// If there are tags, add each tag as an item
					if ([entry.tags count] > 0) {
						NSArray *sortedTags = [entry.tags sortedArrayUsingDescriptors:
							@[[NSSortDescriptor sortDescriptorWithKey:AdvisorMealPlanTagMOAttributes.cloudId ascending:YES]]];
						for (AdvisorMealPlanTagMO *tag in sortedTags) {
							// Search for a planned item for this tag
							MealPlannerDay *mpd = [[tag.mealPlannerDays filteredSetUsingPredicate:[NSPredicate
								predicateWithFormat:@"logTime = %@ AND date BETWEEN {%@, %@} AND status <> %@ AND accountId = %@", 
									entry.logTime, self.startDate, self.endDate, kStatusDelete, [[User currentUser] accountId]]]
								anyObject];
							if (mpd) {
								// Filled tag
								[_itemsBySection[mpd.logTime.integerValue] addObject:[PlanningItem itemWithData:mpd
									andType:PlanningItemTypeAdvisorMealPlanTagFilled]];
								[mealPlans removeObject:mpd];
							}
							else {
								// Unfilled tag
								[_itemsBySection[entry.logTime.integerValue] addObject:[PlanningItem itemWithData:tag
									andType:PlanningItemTypeAdvisorMealPlanTag]];
							}
						}
					}
					// otherwise, just add the entry itself
					else if ([entry.unplannedMealOption isEqualToString:@"any"]) {
						[_itemsBySection[entry.logTime.integerValue] addObject:[PlanningItem itemWithData:entry
							andType:PlanningItemTypeAdvisorMealPlanEntry]];
					}
				}
			}

			/*
			* Then any meal plan items that aren't accounted for
			*/
			for (MealPlannerDay *mpd in mealPlans) {
				// if this meal is still in the mealPlans array, then it wasn't dealt with above, so add it
				[_itemsBySection[mpd.logTime.integerValue] addObject:[PlanningItem itemWithData:mpd andType:PlanningItemTypeMealPlan]];
			}
		}
	}

	/*
	 * Add activity plans into the array
	 */
	if ([BCPermissionsDataModel userHasPermission:kPermissionActivityplan]) {
		NSArray *activityPlans = [ActivityPlanMO activityPlansForLoginId:[User loginId] forDate:self.startDate
			inContext:self.managedObjectContext];

		// No more week mode support here, due to the query above only supporting one day
		for (ActivityPlanMO *activityPlan in activityPlans) {
			[[_itemsBySection lastObject] addObject:[PlanningItem itemWithData:activityPlan andType:PlanningItemTypeActivity]];
		}
	}

    return _itemsBySection;
}    

#pragma mark - BCPopupViewDelegate
- (void)popupView:(BCPopupView *)inPopup didDismissWithInfo:(NSDictionary *)info
{
	if (info) {
		if (inPopup.tag == kTagCalendarPopup) {
			NSDate *selectedDate = [info objectForKey:@"selectedDate"];
			self.originDate = selectedDate;

			[self.headerSegmentControl setTitle:[self.shortDateFormatter stringFromDate:self.originDate] forSegmentAtIndex:1];
		}
	}
}

#pragma mark - BCSearchViewDelegate
- (void)searchView:(id)searchView didFinish:(BOOL)finished withSelection:(id)selection
{
	if (selection) {
		if ([searchView isKindOfClass:[PlanningTagSearchViewController class]]) {
			MealPlannerDay *newMP = selection;
			newMP.date = [self getNewItemDate];

			[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your plan", newMP.name]
				onView:self.navigationController.view];

			// If we were using a scratch context, save it out first
			if (newMP.managedObjectContext != self.managedObjectContext) {
				[newMP.managedObjectContext BL_save];
			}

			[self.managedObjectContext BL_save];
		}
		else if ([selection isKindOfClass:[RecipeMO class]]) {
			RecipeMO *recipe = selection;
			MealPlannerDay *newMP = [MealPlannerDay insertMealPlannerDayWithRecipeMO:recipe forDate:[self getNewItemDate]
				logTime:self.lastAddedMealTime inContext:self.managedObjectContext];
			[newMP markForSync];

			[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your plan", newMP.recipe.name]
				onView:self.navigationController.view];

			[self.managedObjectContext BL_save];
		}
		else if ([selection isKindOfClass:[MealPlannerDay class]]) {
			MealPlannerDay *newMP = selection;

			newMP.date = [self getNewItemDate];
			self.lastAddedMealTime = newMP.logTime;
			[newMP markForSync];

			NSString *name = nil;
			if (newMP.recipe) {
				name = newMP.recipe.name;
			}
			else {
				name = newMP.name;
			}

			[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your plan", name]
				onView:self.navigationController.view];

			[newMP.managedObjectContext BL_save];
			[self.managedObjectContext BL_save];
		}
		else if ([selection isKindOfClass:[ActivityPlanMO class]]) {
			ActivityPlanMO *newAP = selection;

			newAP.targetDate = [self getNewItemDate];
			[newAP markForSync];

			[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your plan", newAP.activityName]
				onView:self.navigationController.view];

			[newAP.managedObjectContext BL_save];
			[self.managedObjectContext BL_save];
		}
		else if ([selection isKindOfClass:[NSDictionary class]]) {
			NSDictionary *selectDict = selection;
 			id tag = selectDict[@"tag"];

			// TODO Deprecated??? Should now be returning a MealPlannerDay instead
			if (tag && (NSNull *)tag != [NSNull null]) {
				// the selection is a choice from the PlanningTagSearchViewController
				// and represents a "tag" for an "event" in the health coach meal plan
				MealPlannerDay *newMP = [MealPlannerDay insertMealPlannerDayWithTag:tag forDate:[self getNewItemDate]
					withAdvisorMealPlanTagDictionary:selectDict inContext:self.managedObjectContext];

				[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your plan", newMP.name]
					onView:self.navigationController.view];

				[self.managedObjectContext BL_save];
			}
			else {
				id recipeId;
				if (selectDict[kRecipeId]) {
					recipeId = selectDict[kRecipeId];
				}
				else if (selectDict[kId]){
					recipeId = selectDict[kId];
				}

				if ([recipeId integerValue] > 0) {
					MealPlannerDay *newMP = [MealPlannerDay insertMealPlannerDayWithRecipeDictionary:selectDict
						forDate:[self getNewItemDate] logTime:self.lastAddedMealTime inContext:self.managedObjectContext];

					[newMP markForSync];

					[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your plan", newMP.recipe.name]
						onView:self.navigationController.view];

					[self.managedObjectContext BL_save];

					self.needsSync = YES;
				}
				else if (recipeId) {
					[self fetchRecipeDetailsForRecipeId:recipeId];
				}
			}
		}

	}

	if (finished) {
		[self.navigationController popViewControllerAnimated:YES];
	}

	if (selection) {
		[self reloadAdvisorMealPlan];
		[self reloadMealCache];
		[self loadImagesForOnscreenRows];
	}
}

#pragma mark - MealPlannerAskViewDelegate
- (void)mealPlannerAskView:(MealPlannerAskViewController *)mealPlannerAskView didFinish:(BOOL)done
{
	__typeof__(self) __weak weakself = self;
	[self dismissViewControllerAnimated:YES completion:^{
		if (done) {
			[weakself processIngredientsAfterAskView];
		}
	}];
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}

	//[super scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	if ([viewController isKindOfClass:[FoodLogDetailViewController class]]) {
		if ([object isKindOfClass:[MealPlannerDay class]]) {
			MealPlannerDay *newMP = object;
			[newMP markForSync];

			// If we were using a scratch context, save it out first
			if (newMP.managedObjectContext != self.managedObjectContext) {
				[newMP.managedObjectContext BL_save];
			}

			[self.managedObjectContext BL_save];

			// This is kind of a big hammer, as it reloads the entire view. However, depending on what they changed in the detail view,
			// that could affect several things, and thus it is much easier and more reliable to do this for now
			[self dateChanged];
		}
		[self.navigationController dismissViewControllerAnimated:YES completion:nil];
	}
	else {
		if ([object isKindOfClass:[ActivityPlanMO class]]) {
			[object markForSync];

			[[object managedObjectContext] BL_save];

			[BCObjectManager syncCheck:SendOnly];

			[self dateChanged];
		}

		[self.navigationController popViewControllerAnimated:YES];
	}
}

@end
