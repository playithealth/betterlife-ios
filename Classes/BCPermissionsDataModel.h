//
//  BCPermissionsDataModel.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 1/2/14.
//  Copyright (c) 2014 BuyerCompass, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCPermissionsDataModel : NSObject

+ (BOOL)userHasPermission:(NSString *)name;

@end
