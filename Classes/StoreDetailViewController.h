//
//  StoreDetailViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "AverageRatingCell.h"
#import "BCMoreLessFooter.h"
#import "BCViewController.h"
#import "LatestRatingCell.h"
#import "StarRatingControl.h"
#import "StoreMapLocation.h"
#import "StoreSelectionDelegate.h"

enum StoreDetailSection
{
	Map,
	Details,
	Categories,
	Amenities,
	UserRatings,
	AverageRatings,
	LatestRatings
};

@interface StoreDetailViewController : BCViewController
	<BCMoreLessFooterDelegate, MKMapViewDelegate, StarRatingControlDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) id storeData;
@property (strong, nonatomic) NSNumber *storeId;
@property (strong, nonatomic) StoreMapLocation *storeMapLocation;
@property (strong, nonatomic) MKMapView *storeMapView;
@property (assign, nonatomic) BOOL showUserLocation;
@property (weak, nonatomic) IBOutlet LatestRatingCell *latestRatingCell;
@property (weak, nonatomic) IBOutlet AverageRatingCell *averageRatingCell;
@property (weak, nonatomic) IBOutlet AverageRatingCell *userRatingCell;
@property (weak, nonatomic) id<StoreSelectionDelegate> storeSelectionDelegate;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
// - (IBAction)ratingWasUpdated:(id)sender;
// - (IBAction)save;

@end
