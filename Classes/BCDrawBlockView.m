//
//  BCDrawBlockView.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/25/13.
//  Copyright (c) 2013 BettrLife. All rights reserved.
//

#import "BCDrawBlockView.h"

@implementation BCDrawBlockView

#pragma mark - drawing
- (void)drawRect:(CGRect)rect
{
   [super drawRect:rect];
    
	CGContextRef context = UIGraphicsGetCurrentContext();
	
    if (self.drawBlock) {
        self.drawBlock(self, context);
	}
}

@end

