//
//  BCSearchViewDelegate.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/11/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BCSearchViewDelegate <NSObject>
@required
- (void)searchView:(id)searchView didFinish:(BOOL)finished withSelection:(id)selection;
@optional
- (void)searchView:(id)searchView didSelectItem:(id)selection;
@end
