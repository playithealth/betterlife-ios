//
//  PrivacyPolicyViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/21/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "PrivacyPolicyViewController.h"

@interface PrivacyPolicyViewController () <UINavigationBarDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation PrivacyPolicyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

	self.automaticallyAdjustsScrollViewInsets = NO;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"privacy-policy.html"
		ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];

	UIBarButtonItem *acceptButton = [[UIBarButtonItem alloc] initWithTitle:@"Accept"
		style:UIBarButtonItemStyleDone target:self action:@selector(accept:)];
	self.navigationItem.rightBarButtonItem = acceptButton;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
	return UIBarPositionTopAttached;
}

#pragma mark - local
- (IBAction)accept:(id)sender
{
	if (self.delegate && [self.delegate conformsToProtocol:
			@protocol(PrivacyPolicyViewControllerDelegate)]) {
		[self.delegate privacyPolicyViewDidAccept:self];
	}
}

@end
