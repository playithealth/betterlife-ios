//
//  BCSidebarViewController.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 12/1/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "BCSidebarViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "AdvisorMealPlanMO.h"
#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCPermissionsDataModel.h"
#import "BCSidebarNavigationCell.h"
#import "BCSidebarUserInfoCell.h"
#import "BLHeaderFooterWithSeparators.h"
#import "BLMyZoneDataModel.h"
#import "BLSidebarCalendarButton.h"
#import "CoachInviteMO.h"
#import "CommunityMO.h"
#import "ConversationMO.h"
#import "FoodLogMO.h"
#import "InboxMessage.h"
#import "NSArray+NestedArrays.h"
#import "NSCalendar+Additions.h"
#import "NSDate+Additions.h"
#import "NSDictionary+NumberValue.h"
#import "NumberValue.h"
#import "ShoppingListItem.h"
#import "SyncUpdateMO.h"
#import "ThreadMO.h"
#import "TrackingMO.h"
#import "TrainingActivityMO.h"
#import "UIColor+Additions.h"
#import "UIImage+Additions.h"
#import "UITableView+DownloadImage.h"
#import "User.h"
#import "UserImageMO.h"
#import "UserPermissionMO.h"
#import "UserProfileMO.h"

static const NSInteger kSectionUserInfo = 0;
static const NSInteger kSectionLife = 1;
static const NSInteger kSectionCommunity = 2;
static const NSInteger kSectionOther = 3;
///static const NSInteger kSectionMain = 1;
///static const NSInteger kSectionSecondary = 2;

static const NSInteger kRowUserInfo = 0;

static const NSInteger kRowProgress = 0;

@interface BCSidebarViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) BCAppDelegate *appDelegate;
@property (strong, nonatomic) BCPermissionsDataModel *permissionsModel;
@property (strong, nonatomic) NSArray *sidebarItems;
@property (strong, nonatomic) NSIndexPath *currentViewIndexPath;
@property (strong, nonatomic) NSArray *weightTracks;
@property (strong, nonatomic) NSCalendar *calendar;
@property (strong, nonatomic) UIImage *profileImage;
@property (strong, nonatomic) NSLayoutConstraint *tableViewRightMarginConstraint;
@property (assign, nonatomic) NSInteger tableViewNormalRightMargin;
@property (strong, nonatomic) BLMyZoneDataModel *myzoneDataModel;
@property (assign, nonatomic) BOOL isMyZoneCalendarShown;
@property (strong, nonatomic) BCSidebarUserInfoCell *userInfoCell;

- (void)closeTapped:(UITapGestureRecognizer *)recognizer;
- (void)profilePictureTapped:(UITapGestureRecognizer *)recognizer;

@end // Interface

@implementation BCSidebarViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		// Custom initialization
		self.appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];

		self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

		self.permissionsModel = [[BCPermissionsDataModel alloc] init];
	}
	return self;
}

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self.tableView registerClass:[BLHeaderFooterWithSeparators class] forHeaderFooterViewReuseIdentifier:@"SeparatorHeader"];

	self.view.backgroundColor = [UIColor clearColor];

	// add a shadow to the right side
/*
	CAGradientLayer *gradientLayer = [CAGradientLayer layer];
	gradientLayer.frame = self.view.frame;
	gradientLayer.startPoint = CGPointMake(0.85, 0.5);
	gradientLayer.endPoint = CGPointMake(0.0, 0.5);
	gradientLayer.colors = @[
		(id)[[UIColor blackColor] CGColor],
		(id)[[UIColor clearColor] CGColor],
		(id)[[UIColor clearColor] CGColor]];
	gradientLayer.locations = @[@0.0f, @0.02f, @1.0f];
	[self.tableView.layer addSublayer:gradientLayer];
*/

	self.myzoneDataModel = [[BLMyZoneDataModel alloc] initWithMOC:self.managedObjectContext];
	[self.myzoneDataModel reloadData];

	// Load the profile image
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];
	if (!self.profileImage && userProfile.imageId) {
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kRowUserInfo inSection:kSectionUserInfo];
		[self.tableView downloadImageWithImageId:userProfile.imageId forIndexPath:indexPath withSize:kImageSizeLarge];
	}
	[self startNotificationObservers];
}

- (void)viewDidUnload
{
	[self stopNotificationObservers];

	[super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];

	[super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - local methods
- (NSDictionary *)currentViewInfo
{
	NSDictionary *viewInfo = nil;
	if (self.currentViewIndexPath) {
		viewInfo = [self.sidebarItems BC_nestedObjectAtIndexPath:self.currentViewIndexPath];
	}

	return viewInfo;
}

- (void)refreshSidebarItems:(NSNotification* )note
{
	self.sidebarItems = nil;
}

- (void)updateViewConstraints {
	// Find the constraint that keeps the tableview from being the width of the view
	if (!self.tableViewRightMarginConstraint) {
		// See if there is a constraint on the table view for the 'right margin' to the superview, if so, grab its pointer
		NSArray *constraints = [self.view constraints];
		for (NSLayoutConstraint *constraint in constraints) {
			if ([constraint.firstItem isEqual:self.view] && [constraint.secondItem isEqual:self.tableView]
					&& (constraint.secondAttribute == NSLayoutAttributeTrailing)) {
				
				self.tableViewRightMarginConstraint = constraint;
				self.tableViewNormalRightMargin = constraint.constant;
				break;
			}
		}
	}

	if (self.disableClosing) {
		self.tableViewRightMarginConstraint.constant = 0;
	}
	else {
		self.tableViewRightMarginConstraint.constant = self.tableViewNormalRightMargin;
	}

	[super updateViewConstraints];
}

- (void)setDisableClosing:(BOOL)isDisabled
{
	if (isDisabled != _disableClosing) {
		_disableClosing = isDisabled;
		[self.view setNeedsUpdateConstraints];
	}
}

- (NSArray *)sidebarItems
{
	if (_sidebarItems) {
		return _sidebarItems;
	}

	//
	// Badge callbacks
	//

	BadgeCallback hasNewMealPlans = ^BOOL (NSManagedObjectContext *context, User *user, id callbackContextObj) { 
		// count the number of meals plans that haven't been seen
		NSUInteger count = [AdvisorMealPlanMO MR_countOfEntitiesWithPredicate:
			[NSPredicate predicateWithFormat:@"%K = %@ AND %K <> %@ AND %K = YES", 
			AdvisorPlanMOAttributes.loginId, user.loginId,
			AdvisorPlanMOAttributes.status, kStatusDelete,
			AdvisorPlanMOAttributes.isNew]
				inContext:context];
		return (count ? YES : NO);
	};

	BadgeCallback unreadConversations = ^BOOL (NSManagedObjectContext *context, User *user, id callbackContextObj) { 
		NSFetchRequest *request = [ConversationMO MR_createFetchRequestInContext:context];
		request.predicate = [NSPredicate predicateWithFormat:@"%K = %@",
			ConversationMOAttributes.loginId, [[User currentUser] loginId]];

		NSArray *conversations = [context executeFetchRequest:request error:nil];

		// Now filter out any conversations that have no messages to the logged in user (only sent from the logged in user), as we have
		// decided that this is the way we wish it to work, specifically so that a coach that sends a message to a group with 1000 users
		// won't have 1000 conversations that have no response from the client
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY messageDeliveries.loginId = %@", [[User currentUser] loginId]];
		conversations = [conversations filteredArrayUsingPredicate:predicate];
		predicate = [NSPredicate predicateWithFormat:@"%K = NO", ConversationMOAttributes.isRead];
		conversations = [conversations filteredArrayUsingPredicate:predicate];
		return ([conversations count] ? YES : NO);
	};

	BadgeCallback unreadCommunityInvite = ^BOOL (NSManagedObjectContext *context, User *user, id callbackContextObj) { 
		if ([callbackContextObj isMemberOfClass:[CommunityMO class]]) {
			return [[callbackContextObj communityStatus] isEqualToString:kCommunityStatusNone];
		}
		return NO;
	};

	BadgeCallback unreadCommunityThread = ^BOOL (NSManagedObjectContext *context, User *user, id callbackContextObj) { 
		if ([callbackContextObj isMemberOfClass:[ThreadMO class]]) {
			ThreadMO *threadMO = callbackContextObj;
			// NOTE: Not sure this should be using updatedOn, maybe should be using createdOn, as the message itself doesn't
			// really have an updatedOn, that is currently just there so the owner can delete the message
			NSNumber *newestMessage = ([threadMO.messages valueForKeyPath:@"@max.updatedOn"] ?: @0);
			//DDLogDebug(@"\nDOT for %@ based upon %@ > %@ = %d\n", threadMO.community.name, newestMessage, threadMO.readOn,
			//	(newestMessage && ([newestMessage integerValue] > [threadMO.readOn integerValue])));
			return (newestMessage && ([newestMessage integerValue] > [threadMO.readOn integerValue]));
		}
		return NO;
	};

	BadgeCallback unreadCommunityThreads = ^BOOL (NSManagedObjectContext *context, User *user, id callbackContextObj) { 
		BOOL unread = NO;
		if ([callbackContextObj isMemberOfClass:[CommunityMO class]]) {
			NSNumber *syncedThru = [SyncUpdateMO lastUpdateForTable:kSyncThreadMessages forUser:[User loginId] inContext:context];
			CommunityMO *communityMO = callbackContextObj;
			if (syncedThru && [syncedThru integerValue]) {
				NSPredicate *predicate = [NSPredicate predicateWithFormat:@"readOn < %@", syncedThru];
				NSSet *unreadThreads = [communityMO.threads filteredSetUsingPredicate:predicate];

				if ([unreadThreads count]) {
					unread = YES;
				}
			}
		}
		return unread;
	};

	BadgeCountCallback coachInviteCount = ^NSUInteger (NSManagedObjectContext *context, User *user) { 
		// count the number of coach invites available
		NSArray *invitations = [CoachInviteMO MR_findByAttribute:CoachInviteMOAttributes.loginId withValue:[[User currentUser] loginId]
			inContext:context];
		return [invitations count];
	};

	// 
	// Navigation Array
	//
	NSMutableArray *navArray = [NSMutableArray array];

	NSMutableArray *topViews = [NSMutableArray array];
	[topViews addObject:@{ kSBDisplayName : @"Account Info" }];

	// Check for invitations before showing coach invitations in sidebar
	NSArray *invitations = [CoachInviteMO MR_findByAttribute:CoachInviteMOAttributes.loginId withValue:[[User currentUser] loginId]
		inContext:self.managedObjectContext];
	if ([invitations count]) {
		// If there is only one invitation, go directly to it when selecting this, otherwise, go to the list
		if ([invitations count] == 1) {
			[topViews addObject:@{
				kSBDisplayName : @"Coach Invitations",
				kSBImage : @"sidebar-healthcoach",
				kSBStoryboardName : @"Main",
				kSBStoryboardID : @"Coach Invite",
				kSBBadgeCountCallback : coachInviteCount,
				kSBPropertiesDictionary : @{ @"invitation" : [invitations firstObject] },
			}];
		}
		else {
			[topViews addObject:@{
				kSBDisplayName : @"Coach Invitations",
				kSBImage : @"sidebar-healthcoach",
				kSBStoryboardName : @"Main",
				kSBStoryboardID : @"Coach Invite List",
				kSBBadgeCountCallback : coachInviteCount,
			}];
		}
	}
	[navArray addObject:topViews];

	NSMutableArray *lifeViews = [NSMutableArray array];

	if ([BCPermissionsDataModel userHasPermission:kPermissionTracking] || [BCPermissionsDataModel userHasPermission:kPermissionFoodlog]) {
		[lifeViews addObject:@{ kSBDisplayName : @"Progress", kSBImage : @"sidebar-dashboard", kSBStoryboardName : @"Main", kSBStoryboardID : @"Dashboard"}];
	}

	// show Planning if the user has mealplan OR activityplan
	if ([BCPermissionsDataModel userHasPermission:kPermissionMealplan] || [BCPermissionsDataModel userHasPermission:kPermissionActivityplan]) {
		[lifeViews addObject:@{ kSBDisplayName : @"Planning", kSBImage : @"sidebar-planning", kSBStoryboardName : @"Planning"}];
	}
	if ([BCPermissionsDataModel userHasPermission:kPermissionShop]) {
		[lifeViews addObject:@{ kSBDisplayName : @"Shopping",
			kSBImage : @"sidebar-shoppinglist",
			kSBStoryboardName : @"ShoppingList"}];
	}
	// show Logging if the user has foodlog OR activitylog
	if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog] || [BCPermissionsDataModel userHasPermission:kPermissionActivitylog]) {
		[lifeViews addObject:@{ kSBDisplayName : @"Logging",
			kSBImage : @"sidebar-logging",
			kSBStoryboardName : @"Logging"}];
	}
	if ([BCPermissionsDataModel userHasPermission:kPermissionPantry]) {
		[lifeViews addObject:@{ kSBDisplayName : @"Pantry",
			kSBImage : @"sidebar-pantry",
			kSBStoryboardName : @"Pantry"}];
	}

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	BOOL showPlans = NO;
	if ([BCPermissionsDataModel userHasPermission:kPermissionMealplan] || ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog])) {
		// Check for the existence of meal plans to determine whether to show this
		NSInteger mealPlansAvailable = [AdvisorMealPlanMO MR_countOfEntitiesWithPredicate:
			[NSPredicate predicateWithFormat:@"%K = %@ AND %K <> %@", 
				AdvisorPlanMOAttributes.loginId, [[User currentUser] loginId], AdvisorPlanMOAttributes.status, kStatusDelete]
			inContext:appDelegate.adManagedObjectContext];

		if (mealPlansAvailable) {
			showPlans = YES;
		}
	}
	// No need to even check this if showPlans is already set to YES
	if (!showPlans && ([BCPermissionsDataModel userHasPermission:kPermissionActivityplan]
			|| [BCPermissionsDataModel userHasPermission:kPermissionActivitylog])) {

		// TODO Check for the existence of activity plans to determine whether to show this
		/*
		NSInteger activityPlansAvailable = [AdvisorMealPlanMO MR_countOfEntitiesWithPredicate:
			[NSPredicate predicateWithFormat:@"%K = %@ AND %K <> %@", 
				AdvisorMealPlanMOAttributes.loginId, [[User currentUser] loginId], AdvisorMealPlanMOAttributes.status, kStatusDelete]
			inContext:appDelegate.adManagedObjectContext];

		if (activityPlansAvailable) {
			showPlans = YES;
		}
		*/
	}

	if (showPlans) {
		[lifeViews addObject:@{ kSBDisplayName : @"Health Plans",
			kSBImage : @"sidebar-healthcoach",
			kSBStoryboardName : @"Planning",
			kSBStoryboardID : @"HealthCoachPlans",
			kSBBadgeCallback : hasNewMealPlans}];
	}

	[navArray addObject:lifeViews];

	NSMutableArray *communityViews = [NSMutableArray array];
	// NOTE always include messages
	[communityViews addObject:@{
			kSBDisplayName : @"Messages",
			kSBImage : @"sidebar-inbox",
			kSBStoryboardName : @"Messages",
			kSBBadgeCallback : unreadConversations
		}];

	// Add any Communities to the sidebar at this location (into the community views section)
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];
	// Sorting communities by acceptance (accepted ones at the top), then by name
	NSSortDescriptor *byAcceptance = [NSSortDescriptor sortDescriptorWithKey:CommunityMOAttributes.communityStatus ascending:YES
		comparator:^(id obj1, id obj2) {
			// Sort the accepted communities to the top, all others to the bottom, like statuses are equal, of course
			if ([obj1 isEqualToString:obj2]) {
				return (NSComparisonResult)NSOrderedSame;
			}
			else if ([obj1 isEqualToString:kCommunityStatusAccepted]) {
				return (NSComparisonResult)NSOrderedAscending;
			}
			else if ([obj2 isEqualToString:kCommunityStatusAccepted]) {
				return (NSComparisonResult)NSOrderedDescending;
			}
			return (NSComparisonResult)NSOrderedSame;
		}];
	NSSortDescriptor *byName = [NSSortDescriptor sortDescriptorWithKey:CommunityMOAttributes.name ascending:YES];

	for (CommunityMO *community in [userProfile.communities sortedArrayUsingDescriptors:@[byAcceptance, byName]]) {
		NSInteger threadCount = [community.threads count];

		// If the community status is not accepted, then take them to the acceptance view
		if (![community.communityStatus isEqualToString:kCommunityStatusAccepted]) {
			[communityViews addObject:@{
				kSBDisplayName : community.name,
				kSBImage : @"sidebar-community",
				kSBStoryboardName : @"Messages",
				kSBStoryboardID : @"CommunityInvite",
				kSBPropertiesDictionary : @{ @"community" : community },
				kSBBadgeCallback : unreadCommunityInvite,
				kSBCallbackContextObj : community,
			}];
		}
		// If there is more than one thread, then go to the threads view, otherwise, go straight to the messages
		else if (threadCount == 1) {
			[communityViews addObject:@{
				kSBDisplayName : community.name,
				kSBImage : @"sidebar-community",
				kSBStoryboardName : @"Messages",
				kSBStoryboardID : @"ThreadMessage",
				kSBPropertiesDictionary : @{ @"thread" : [community.threads anyObject] },
				kSBBadgeCallback : unreadCommunityThread,
				kSBCallbackContextObj : [community.threads anyObject],
			}];
		}
		else if (threadCount > 1) {
			[communityViews addObject:@{
				kSBDisplayName : community.name,
				kSBImage : @"sidebar-community",
				kSBStoryboardName : @"Messages",
				kSBStoryboardID : @"ThreadList",
				kSBPropertiesDictionary : @{ @"community" : community },
				kSBBadgeCallback : unreadCommunityThreads,
				kSBCallbackContextObj : community,
			}];
		}
	}

	// 'Community' views
	[navArray addObject:communityViews];

	// 'Other' views
	[navArray addObject:@[
		@{ kSBDisplayName : @"Account",
			kSBImage : @"sidebar-account-settings",
			kSBStoryboardName : @"AccountSettings"},
		@{ kSBDisplayName : @"Onboard",
			kSBImage : @"sidebar-about",
			kSBStoryboardName : @"Onboarding",
			kSBStoryboardID : @"Onboard"},
		@{ kSBDisplayName : @"About",
			kSBImage : @"sidebar-about",
			kSBStoryboardName : @"Main",
			kSBStoryboardID : @"About"}
	]];

	_sidebarItems = navArray;

	return _sidebarItems;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	UserProfileMO *userProfile = [User currentUser].userProfile;
	NSDictionary *dict = [self.sidebarItems BC_nestedObjectAtIndexPath:indexPath];
	if ((indexPath.section == kSectionUserInfo) && (indexPath.row == kRowUserInfo)) {
		BCSidebarUserInfoCell *uiCell = (BCSidebarUserInfoCell *)cell;
		uiCell.nameLabel.text = [[User currentUser] name];

		// the profile image

		// First see if we have established a profile image, this helps to deal with the situation where the user changed it
		// in this view, prior to it being round-tripped back from the cloud with an imageId
		UIImage *profileImage = self.profileImage;
		if (!profileImage) {
			profileImage = [[BCImageCache sharedCache] profileImageWithImageId:userProfile.imageId
				withGender:userProfile.gender withSize:kImageSizeLarge];
		}

		uiCell.userImageView.image = profileImage;

		if ([userProfile.hasMyZone boolValue]) {
			BCSidebarUserInfoCell *userCell = (BCSidebarUserInfoCell *)cell;

			NSArray *dailyLogs = self.myzoneDataModel.weekdaysWithData;
			userCell.sundayButton.hasData = [[dailyLogs objectAtIndex:0] boolValue];
			userCell.mondayButton.hasData = [[dailyLogs objectAtIndex:1] boolValue];
			userCell.tuesdayButton.hasData = [[dailyLogs objectAtIndex:2] boolValue];
			userCell.wednesdayButton.hasData = [[dailyLogs objectAtIndex:3] boolValue];
			userCell.thursdayButton.hasData = [[dailyLogs objectAtIndex:4] boolValue];
			userCell.fridayButton.hasData = [[dailyLogs objectAtIndex:5] boolValue];
			userCell.saturdayButton.hasData = [[dailyLogs objectAtIndex:6] boolValue];

			NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
			[formatter setMinimum:@0];
			[formatter setPositiveFormat:@"#,##0"];
			[formatter setNilSymbol:@"0"];

			[formatter setUsesGroupingSeparator:([self.myzoneDataModel.weekMEPs integerValue] >= 10000)];
			userCell.mepsWeekLabel.text = [formatter stringForObjectValue:self.myzoneDataModel.weekMEPs];

			[formatter setUsesGroupingSeparator:([self.myzoneDataModel.monthMEPs integerValue] >= 10000)];
			userCell.mepsMonthLabel.text = [formatter stringForObjectValue:self.myzoneDataModel.monthMEPs];

			[formatter setUsesGroupingSeparator:([self.myzoneDataModel.yearMEPs integerValue] >= 10000)];
			userCell.mepsYearLabel.text = [formatter stringForObjectValue:self.myzoneDataModel.yearMEPs];

			[formatter setUsesGroupingSeparator:([self.myzoneDataModel.weekCals integerValue] >= 10000)];
			userCell.calsWeekLabel.text = [formatter stringForObjectValue:self.myzoneDataModel.weekCals];

			[formatter setUsesGroupingSeparator:([self.myzoneDataModel.monthCals integerValue] >= 10000)];
			userCell.calsMonthLabel.text = [formatter stringForObjectValue:self.myzoneDataModel.monthCals];

			[formatter setUsesGroupingSeparator:([self.myzoneDataModel.yearCals integerValue] >= 10000)];
			userCell.calsYearLabel.text = [formatter stringForObjectValue:self.myzoneDataModel.yearCals];
		}
	}
	else if ((indexPath.section == kSectionLife) && (indexPath.row == kRowProgress)) {
		BLSidebarProgressCell *progressCell = (BLSidebarProgressCell *)cell;
		// handle the showing and updating of the weight arrow and label
		if ([BCPermissionsDataModel userHasPermission:kPermissionTracking]) {
			progressCell.arrowImageView.hidden = NO;
			progressCell.valueLabel.hidden = NO;

			// get the last couple of weights
			NSNumber* currentWeight = nil;
			NSNumber* previousWeight = nil;
			if ([self.weightTracks count] > 0) {
				TrackingMO *lastWeightTrack = [self.weightTracks objectAtIndex:0];
                NSDictionary *currentTrackDict = lastWeightTrack.valueDictionary;
				currentWeight = [[currentTrackDict valueForKey:kTrackingValue1] numberValueDecimal];
			}
			if ([self.weightTracks count] > 1) {
				TrackingMO *previousWeightTrack = [self.weightTracks objectAtIndex:1];
                NSDictionary *previousTrackDict = previousWeightTrack.valueDictionary;
				previousWeight = [[previousTrackDict valueForKey:kTrackingValue1] numberValueDecimal];
			}

			if (currentWeight) {
				NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
				[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
				[numberFormatter setMinimumFractionDigits:1];
				[numberFormatter setMaximumFractionDigits:1];
				progressCell.valueLabel.text = [NSString stringWithFormat:@"%@ lbs.", [numberFormatter stringFromNumber:currentWeight]];
			}
			else {
				progressCell.valueLabel.text = @"";
			}

			// check direction of goal
			NSString *arrowColor = nil;
			NSString *arrowDirection = nil;
			if (currentWeight && previousWeight) {
				NSComparisonResult compareWeight = [previousWeight compare:currentWeight];

				// if the user's weight is higher than last week then make the arrow point up
				if (compareWeight == NSOrderedAscending) {
					arrowDirection = @"up";
				}
				else {
					arrowDirection = @"down";
				}

				// the user is moving in the direction of their goal, the arrow should be green
				if (compareWeight != NSOrderedSame
						&& ([userProfile.goalWeight integerValue] > 0)) {
					NSComparisonResult compareGoal = [currentWeight compare:userProfile.goalWeight];
					if (compareWeight == compareGoal) {
						arrowColor = @"green";
					}
					// otherwise, red arrow and text
					else {
						arrowColor = @"red";
					}
				}
			}
			if (arrowColor && arrowDirection) {
				progressCell.arrowImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"arrow-%@-%@", arrowColor, arrowDirection]];
			}
			else {
				progressCell.arrowImageView.image = nil;
			}
		}
		else {
			progressCell.arrowImageView.image = nil;
			progressCell.arrowImageView.hidden = YES;
			progressCell.valueLabel.text = nil;
			progressCell.valueLabel.hidden = YES;
		}
	}
	else {
		BCSidebarNavigationCell *navCell = (BCSidebarNavigationCell *)cell;

		navCell.textLabel.text = [dict objectForKey:kSBDisplayName];
		navCell.imageView.image = [UIImage imageNamed:[dict objectForKey:kSBImage]];
		navCell.accessoryView = nil;

		// clear the old badge from the view and storage
		if (navCell.customBadge) {
			[navCell.customBadge removeFromSuperview];
			navCell.customBadge = nil;
		}

		if ([dict objectForKey:kSBBadgeCallback]) {
			User *thisUser = [User currentUser];
			BadgeCallback shouldShowBadge = [dict objectForKey:kSBBadgeCallback];
			id contextObject = [dict objectForKey:kSBCallbackContextObj];
			if (shouldShowBadge(self.managedObjectContext, thisUser, contextObject)) {
				UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MessagesUnreadDot"]];
				navCell.accessoryView = imageView;
			}
		}
		else if ([dict objectForKey:kSBBadgeCountCallback]) {
			User *thisUser = [User currentUser];
			BadgeCountCallback getBadgeCount = [dict objectForKey:kSBBadgeCountCallback];
			NSUInteger count = getBadgeCount(self.managedObjectContext, thisUser);
			if (count > 0) {
				/*
				UILabel *badge = [[UILabel alloc] init];
				badge.text = [NSString stringWithFormat:@"%lu", (long)count];
				badge.layer.cornerRadius = 8.0f;
				badge.backgroundColor = [UIColor whiteColor];
				badge.textColor = [UIColor blackColor];
				navCell.accessoryView = badge;
				*/
				CustomBadge *customBadge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%lu", (unsigned long)count]];
				customBadge.badgeTextColor = [UIColor BC_darkGrayColor];
				customBadge.badgeInsetColor = [UIColor BC_goldColor];
				customBadge.badgeFrame = NO;
				customBadge.badgeScaleFactor = 1.3f;
				customBadge.badgeShining = NO;
				customBadge.badgeCornerRoundness = 0.1;

				navCell.customBadge = customBadge;
				[navCell.contentView addSubview:customBadge];
				[customBadge setCenter:CGPointMake(self.tableView.frame.size.width - (customBadge.frame.size.width / 2) - 10, 22)];
			}
		}
	}
}

- (void)updateBadges
{
	[self.tableView reloadData];

	// Re-select the selected row
	if (self.currentViewIndexPath) {
		[self.tableView selectRowAtIndexPath:self.currentViewIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
	}
}

- (void)startNotificationObservers
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSidebarItems:)
		 name:[UserPermissionMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSidebarItems:)
		 name:[AdvisorMealPlanMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSidebarItems:)
		name:[CoachInviteMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSidebarItems:)
		name:[CommunityMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSidebarItems:)
		name:[ThreadMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userProfileUpdated:)
		name:[UserProfileMO entityName] object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(trackingUpdated:)
		name:[TrackingMO entityName] object:nil];
	// NOTE: This should probably not be using the very generic userProfileUpdated: selector, but don't have an appropriate one
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userProfileUpdated:)
		name:[TrainingActivityMO entityName] object:nil];
}

- (void)stopNotificationObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)userLoggedOut
{
	// Ensure that any items related to a specific user are updated when the user is changed (logout/login)
	[self stopNotificationObservers];
	self.profileImage = nil;
	[self refreshSidebarItems:nil];
}

- (void)userLoggedIn
{
	// Ensure that any items related to a specific user are updated when the user is changed (logout/login)
	self.profileImage = nil;
	// Load the profile image
	UserProfileMO *userProfile = [User currentUser].userProfile;
	if (userProfile.imageId) {
		[self.tableView downloadImageWithImageId:userProfile.imageId forIndexPath:nil withSize:kImageSizeLarge];
	}
	[self.myzoneDataModel reloadData];
	[self refreshSidebarItems:nil];

	[self startNotificationObservers];
}

- (IBAction)profilePictureTapped:(UITapGestureRecognizer *)recognizer
{
	UIActionSheet *actionSheet = [[UIActionSheet alloc]
		initWithTitle:@"Choose a source" 
		delegate:self
		cancelButtonTitle:@"Cancel" 
		destructiveButtonTitle:nil
		otherButtonTitles:@"Take a new picture", @"Choose from library", nil];
	[actionSheet showInView:self.view];
}

- (IBAction)closeTapped:(UITapGestureRecognizer *)recognizer
{
	if (!self.disableClosing) {
		[self.appDelegate showSidebar:NO disableClosing:NO];
	}
}

- (IBAction)closeSwiped:(UISwipeGestureRecognizer *)sender
{
	if (!self.disableClosing) {
		[self.appDelegate showSidebar:NO disableClosing:NO];
	}
}

- (IBAction)myzoneCalendarButtonTapped:(UIButton *)sender {
	[self.tableView beginUpdates];
	self.isMyZoneCalendarShown = !self.isMyZoneCalendarShown;
	[self.userInfoCell setCalendarShown:self.isMyZoneCalendarShown];
	[self.tableView endUpdates];
}

- (NSArray *)weightTracks
{
    if (_weightTracks != nil) {
        return _weightTracks;
    }

	User *thisUser = [User currentUser];
	NSFetchRequest *request = [TrackingMO MR_requestAllSortedBy:TrackingMOAttributes.date ascending:NO 
		withPredicate:[NSPredicate predicateWithFormat:@"loginId = %@ and status <> %@ and trackingType = %d",
		thisUser.loginId, kStatusDelete, TrackingTypes.weight] inContext:self.managedObjectContext];
	request.fetchLimit = 4;

	self.weightTracks = [self.managedObjectContext executeFetchRequest:request error:nil];

	return _weightTracks;
}

- (NSInteger)getAverageDailyCaloriesFor:(NSInteger)numberOfDays startingAt:(NSInteger)startingOffset
{
	User *thisUser = [User currentUser];
	
	NSDate *today = [self.calendar BC_startOfDate:[NSDate date]];
	
	NSInteger average = 0;
	NSInteger sum = 0;
	NSInteger days = 0;

	for (NSInteger offset = startingOffset; offset > startingOffset - numberOfDays; offset--) {
		NSDate *offsetDate = [self.calendar BC_dateByAddingDays:offset toDate:today];

		NSArray *foodLogs = [FoodLogMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"loginId = %@ AND status <> %@ AND date BETWEEN {%@, %@}",
				thisUser.loginId, kStatusDelete, [self.calendar BC_startOfDate:offsetDate], [self.calendar BC_endOfDate:offsetDate]]
			inContext:self.managedObjectContext];
		NSInteger dayCalories = 0;
		for (FoodLogMO *foodLog in foodLogs) {
			dayCalories += foodLog.nutrition.caloriesValue;
		}

		if (dayCalories > 0) {
			days++;
			sum += dayCalories;
		}
	}

	if (sum > 0 && days > 0) {
		average = sum / days;
	}

	return average;
}

#pragma mark - BLMyZoneCalendarDelegate
- (BOOL)hasDataAtIndex:(NSUInteger)index {
	return [[self.myzoneDataModel.daysWithData objectAtIndex:index] boolValue];
}

- (void)didSelectDate:(NSDate *)selectedDate {
	[self.appDelegate gotoInitialViewControllerInStoryboardNamed:@"Logging" withProperties:@{ @"initialDate" : selectedDate }];
	[self.appDelegate showSidebar:NO disableClosing:NO];
}

- (void)didChangeCalendarStartDate:(NSDate *)newStartDate {
	[self.myzoneDataModel loadDailyLogsForCalendarWithStartDate:newStartDate];
}

#pragma mark - Image Handling
- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType 
{ 
	NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType]; 
	if ([UIImagePickerController isSourceTypeAvailable:sourceType] 
			&& [mediaTypes count] > 0) { 
		NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType]; 
		UIImagePickerController *picker = [[UIImagePickerController alloc] init]; 
		picker.mediaTypes = mediaTypes;
		picker.delegate = self;
		picker.allowsEditing = YES;
		picker.sourceType = sourceType;
		[self presentViewController:picker animated:YES completion:nil];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] 
			initWithTitle:@"Error accessing media"
			message:@"Device doesn't support that media source."
			delegate:self 
			cancelButtonTitle:@"OK"
			otherButtonTitles:nil];
		[alert show];
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [self.sidebarItems count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.sidebarItems BC_countOfNestedArrayAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *userInfoCellId = @"UserInfoCell";
	static NSString *progressCellId = @"ProgressCell";
	static NSString *mainItemCellId = @"MainItemCell";
	static NSString *secondaryItemCellId = @"SecondaryItemCell";
	static NSString *coachInviteCellId = @"CoachInviteCell";
	User *thisUser = [User currentUser];

	UITableViewCell *cell = nil;
	if (indexPath.section == kSectionUserInfo) {
		if (indexPath.row == kRowUserInfo) {
			if (!cell) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:userInfoCellId];
				self.userInfoCell = (BCSidebarUserInfoCell *)cell;
			}
			// Right now, we only support the kosama skin, so specifically check for it
			if (thisUser.userProfile.skin && [thisUser.userProfile.skin isEqualToString:kUserProfileSkinKosama]) {
				[self.userInfoCell showPoweredBy:YES];
				[self.userInfoCell useGradientWithTopColor:[UIColor BL_topKosamaBlue] bottomColor:[UIColor BL_bottomKosamaBlue]];
            }
			else {
				[self.userInfoCell showPoweredBy:NO];
				[self.userInfoCell hideGradient];
				cell.contentView.backgroundColor = [UIColor BC_darkGrayColor];
			}
			if ([thisUser.userProfile.hasMyZone boolValue]) {
				[self.userInfoCell showMyZone:YES];
				[self.userInfoCell setCalendarShown:self.isMyZoneCalendarShown];
			}
			else {
				[self.userInfoCell showMyZone:NO];
			}
		}
		else {
			// Coach Invites
			cell = [self.tableView dequeueReusableCellWithIdentifier:coachInviteCellId];
		}
	}
	else {
		if (indexPath.section == kSectionOther) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:secondaryItemCellId];
		}
		else if ((indexPath.section == kSectionLife) && (indexPath.row == kRowProgress)) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:progressCellId];
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:mainItemCellId];
		}
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44.0f; // Standard row height for this view

	if ((indexPath.section == kSectionUserInfo) && (indexPath.row == kRowUserInfo)) {
		// The following values are designed to match the height constraints of their respective subviews in IB
		CGFloat poweredByHeight = 37.0f;
		CGFloat userInfoHeight = 82.0f;
		CGFloat myzoneInfoHeight = 52.0f;
		CGFloat myzoneWeekHeight = 44.0f;
		CGFloat myzoneCalendarHeight = 271.0f;
		rowHeight = userInfoHeight; // Start off with the height for just the user info
		User *thisUser = [User currentUser];
		// Right now, we only support the kosama skin, so specifically check for it
		if (thisUser.userProfile.skin && [thisUser.userProfile.skin isEqualToString:kUserProfileSkinKosama]) {
			rowHeight += poweredByHeight; // Add in the powered by height
		}
		if ([thisUser.userProfile.hasMyZone boolValue]) {
			rowHeight += myzoneInfoHeight; // Add in the height of the myzone info

			// Add in for whichever view is currently visible, either the week view or the calendar view
			rowHeight += (self.isMyZoneCalendarShown ? myzoneCalendarHeight : myzoneWeekHeight);
		}
	}	

	return rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//	CGFloat headerHeight = UITableViewAutomaticDimension; // NOTE: Unfortunately, this causes the framework to NOT call viewForHeaderInSection!
	CGFloat headerHeight = 30.0;
	if (section == kSectionUserInfo) {
		headerHeight = 0.0f;
	}
	return headerHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	BLHeaderFooterWithSeparators *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"SeparatorHeader"];

    // Background color - This alters the color slightly, making it not match the user info cell
    //view.tintColor = [UIColor BC_darkGrayColor];
    // Another way to set the background color
    // Note: does not preserve gradient effect of original header
    headerView.contentView.backgroundColor = [UIColor BC_darkGrayColor];

    // Text Color
    [headerView.customLabel setTextColor:[UIColor BC_lightGrayColor]];
	[headerView setSeparatorColor:[UIColor blackColor]];

	switch (section) {
		case kSectionUserInfo:
			break;
		case kSectionLife:
			headerView.customLabel.text = @"LIFE";
			break;
		case kSectionCommunity:
			headerView.customLabel.text = @"COMMUNITY";
			break;
		case kSectionOther:
			headerView.customLabel.text = @"OTHER";
			break;
	}

	return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSDictionary *navigationItem = [self.sidebarItems BC_nestedObjectAtIndexPath:indexPath];
	//[self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];

	NSString *storyboardId = [navigationItem objectForKey:kSBStoryboardID];
	NSString *storyboardName = [navigationItem objectForKey:kSBStoryboardName];
	NSDictionary *propertiesDict = [navigationItem objectForKey:kSBPropertiesDictionary];
	if (storyboardId && storyboardName) {
		[self.appDelegate gotoViewWithStoryboardId:storyboardId inStoryboardNamed:storyboardName withProperties:propertiesDict];
		[self.appDelegate showSidebar:NO disableClosing:NO];
	}
	else if (storyboardName) {
		[self.appDelegate gotoInitialViewControllerInStoryboardNamed:storyboardName withProperties:propertiesDict];
		[self.appDelegate showSidebar:NO disableClosing:NO];
	}
}

#pragma mark - UIResponder
// This will allow the app delegate to set the sidebar to the first responder to ensure
// that if the view that is visible has the keyboard open, it will be closed when showing
// to the sidebar
- (BOOL)canBecomeFirstResponder
{
	return YES; 
}

#pragma mark UIImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info 
{
	NSManagedObjectContext *context = self.managedObjectContext;
	User *thisUser = [User currentUser];

	UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];

	// NOTE: We will just stuff the image in locally for now, as in, into a property rather than putting it into the cache,
	// since we will want an image id for the cache
	// We also need a way to get in sync with the cloud with the image changes, as if the user doesn't re-login and/or change their user profile
	// info, we will not have their image id, as this is a new column. Maybe I will have to bite the bullet and do a data migration :(
	self.profileImage = chosenImage;
	
	// create a 200x200 size image and save it to be uploaded
	UIImage *bigImage = [chosenImage BC_imageWithTargetSize:CGSizeMake(200, 200)];
	UserImageMO *customImage = [UserImageMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K = %d AND %K = %@ AND %K = %@",
			UserImageMOAttributes.imageType, kUserImageTypeUserProfile, UserImageMOAttributes.accountId, thisUser.accountId, UserImageMOAttributes.loginId, thisUser.loginId]
		inContext:context];
	if (!customImage) {
		customImage = [UserImageMO insertInManagedObjectContext:context];
		customImage.accountId = thisUser.accountId;
		customImage.loginId = thisUser.loginId;
		customImage.imageType = @(kUserImageTypeUserProfile);
	}
	[customImage setUserImageWithUIImage:bigImage];
	[customImage markForSync];
	[context BL_save];

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kRowUserInfo inSection:kSectionUserInfo];
	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];

	[picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker 
{ 
	[picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex != [actionSheet cancelButtonIndex]) {
		switch (buttonIndex) {
			case 0:
				[self getMediaFromSource:UIImagePickerControllerSourceTypeCamera];
				break;
			case 1:	
			default:
				[self getMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
				break;
		}		
	}
}

#pragma mark - notification handlers
- (void)userProfileUpdated:(NSNotification* )note
{
	self.permissionsModel = [[BCPermissionsDataModel alloc] init];
	[self.myzoneDataModel reloadData];
	[self refreshSidebarItems:nil];

	// Reload the profile image
	UserProfileMO *userProfile = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];
	if (!self.profileImage && userProfile.imageId) {
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kRowUserInfo inSection:kSectionUserInfo];
		[self.tableView downloadImageWithImageId:userProfile.imageId forIndexPath:indexPath withSize:kImageSizeLarge];
	}

	[self.tableView reloadData];
}

- (void)trackingUpdated:(NSNotification* )note
{
	self.weightTracks = nil;

	if ([[User currentUser].userProfile.hasMyZone boolValue]) {
		// Pretend the user profile got updated, which reloads everything
		[self userProfileUpdated:nil];
	}
	else {
		// Just reload the cell that displays the weight
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kRowProgress inSection:kSectionLife];
		[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
	}
}

@end
