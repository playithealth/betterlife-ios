//
//  BCLoggingJustCaloriesViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/25/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCLoggingJustCaloriesViewController.h"
#import "BCDetailViewDelegate.h"
#import "FoodLogMO.h"
#import "NumberValue.h"

@interface BCLoggingJustCaloriesViewController ()
@end

@implementation BCLoggingJustCaloriesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	if (self.foodLog) {
		self.caloriesField.text = [self.foodLog.nutrition.calories stringValue];
	}

	[self.caloriesField becomeFirstResponder];
}

#pragma mark - responders
- (IBAction)cancelButtonTapped:(id)sender
{
	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCDetailViewDelegate)]) {
		[self.delegate view:self didUpdateObject:nil];
	}

}

- (IBAction)saveButtonTapped:(id)sender
{
	NSNumber *calories = [self.caloriesField.text numberValueDecimal];

	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(BCDetailViewDelegate)]) {
		[self.delegate view:self didUpdateObject:calories];
	}

}

@end
