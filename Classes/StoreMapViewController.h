//
//  StoreMapViewController.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 5/28/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "BCViewController.h"

@class StoreMapViewController;
@protocol StoreSelectionDelegate;

@interface StoreMapViewController : BCViewController <MKMapViewDelegate> {
    MKMapView *mapView;
	NSArray *stores;
	CLLocation *startingLocation;
}
@property (unsafe_unretained, nonatomic) id<StoreSelectionDelegate> delegate;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSArray *stores;
@property (strong, nonatomic) CLLocation *startingLocation;

@end
