//
//  LatestRatingCell.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/6/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "LatestRatingCell.h"

#import "StarRatingControl.h"

@implementation LatestRatingCell
@synthesize categoryLabel;
@synthesize commentLabel;
@synthesize userDateLabel;
@synthesize ratingControl;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    categoryLabel = nil;
    commentLabel = nil;
    userDateLabel = nil;
    ratingControl = nil;

}

@end
