//
//  LoggingViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/4/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCCalendarView.h"
#import "BCDetailViewDelegate.h"
#import "BCViewController.h"
#import "FoodLogViewCustomCell.h"
#import "ProductSearchView.h"
#import "RestaurantSearchView.h"
#import "ZBarReaderViewController.h"


@interface LoggingViewController : BCViewController <BCDetailViewDelegate,
	BCPopupViewDelegate, NSFetchedResultsControllerDelegate, 
	UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate, ZBarReaderDelegate>

- (void)setInitialDate:(NSDate *)toDate;
@end
