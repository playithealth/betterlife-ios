//
//  BCMoreLessFooter.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 1/18/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCMoreLessFooter.h"

@interface BCMoreLessFooter ()
@property (assign, nonatomic) BOOL expanded;
@property (strong, nonatomic) NSString *expandText;
@property (strong, nonatomic) NSString *collapseText;
@property (strong, nonatomic) UILabel *textLabel;
@property (strong, nonatomic) UIImageView *leftArrowDown;
@property (strong, nonatomic) UIImageView *leftArrowUp;
@property (strong, nonatomic) UIImageView *rightArrowDown;
@property (strong, nonatomic) UIImageView *rightArrowUp;
@property (unsafe_unretained, nonatomic) id<BCMoreLessFooterDelegate>delegate;

- (void)moreLessTapped:(UITapGestureRecognizer *)recognizer;
@end

@implementation BCMoreLessFooter
@synthesize expanded;
@synthesize expandText;
@synthesize collapseText;
@synthesize textLabel;
@synthesize leftArrowDown;
@synthesize leftArrowUp;
@synthesize rightArrowDown;
@synthesize rightArrowUp;
@synthesize delegate;

//
// Designated initializer
//
- (id)initWithExpandText:(NSString *)inExpandText collapseText:(NSString *)inCollapseText
delegate:(id<BCMoreLessFooterDelegate>)inDelegate;
{
    self = [super initWithImage:[UIImage imageNamed:@"more-less-background"]];
    if (self) {
        // Initialization code
		self.expandText = inExpandText;
		self.collapseText = inCollapseText;
		self.delegate = inDelegate;
		self.expanded = NO;
		self.contentMode = UIViewContentModeTop;
		self.userInteractionEnabled = YES;

		UIImageView *arrow;
		arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow-down"]];
		arrow.frame = CGRectMake(55, 5, arrow.frame.size.height, arrow.frame.size.width);
		[self addSubview:arrow];
		self.leftArrowDown = arrow;

		arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow-down"]];
		arrow.frame = CGRectMake(245, 5, arrow.frame.size.height, arrow.frame.size.width);
		[self addSubview:arrow];
		self.rightArrowDown = arrow;

		arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow-up"]];
		arrow.frame = CGRectMake(55, 5, arrow.frame.size.height, arrow.frame.size.width);
		arrow.hidden = YES;
		[self addSubview:arrow];
		self.leftArrowUp = arrow;

		arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow-up"]];
		arrow.frame = CGRectMake(245, 5, arrow.frame.size.height, arrow.frame.size.width);
		arrow.hidden = YES;
		[self addSubview:arrow];
		self.rightArrowUp = arrow;

		UILabel *moreLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, 80, 20)];
		moreLabel.backgroundColor = [UIColor colorWithWhite:0.97 alpha:1.0];
		moreLabel.text = self.expandText;
		moreLabel.textAlignment = NSTextAlignmentCenter;
		[self addSubview:moreLabel];
		moreLabel.center = CGPointMake(160, 15);
		self.textLabel = moreLabel;

		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(moreLessTapped:)];
		[self addGestureRecognizer:recognizer];
    }
    return self;
}

// Use this initializer if you're okay with the default text labels and just
// want to specify a delegate
- (id)initWithDelegate:(id<BCMoreLessFooterDelegate>)inDelegate;
{
    return [self initWithExpandText:@"More" collapseText:@"Less" delegate:inDelegate];
}


- (BOOL)isExpanded
{
	return self.expanded;
}

- (BOOL)isCollapsed
{
	return !self.expanded;
}

- (void)moreLessTapped:(UITapGestureRecognizer *)recognizer
{
	// First, toggle the expanded flag, so we know which state we're moving to
	self.expanded = !self.expanded;

	self.leftArrowDown.hidden = self.expanded;
	self.rightArrowDown.hidden = self.expanded;
	self.leftArrowUp.hidden = !self.expanded;
	self.rightArrowUp.hidden = !self.expanded;

	if (self.expanded) {
		// Show more, and change the label to allow for showing less again
		textLabel.text = self.collapseText;
	}
	else {
		// Show less, and change the label to allow for showing more again
		textLabel.text = self.expandText;
	}

	// Notify the delegate
	if (delegate && [delegate conformsToProtocol:@protocol(BCMoreLessFooterDelegate)]) {
		[delegate moreLessFooterTapped:self];
	}
}

@end
