//
//  BCLoggingSearchSearchViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/9/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCLoggingSearchSearchViewController.h"

#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCProgressHUD.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "CustomFoodMO.h"
#import "ExerciseRoutineMO.h"
#import "FoodLogDetailViewController.h"
#import "FoodLogMO.h"
#import "GTMHTTPFetcherAdditions.h"
#import "HTTPQueue.h"
#import "NSDictionary+NumberValue.h"
#import "NutritionMO.h"
#import "RecipeMO.h"
#import "TrainingActivityMO.h"
#import "UIImage+Additions.h"
#import "UITableView+DownloadImage.h"
#import "User.h"

@interface BCLoggingSearchSearchViewController () <BCDetailViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSArray *sectionTitles;
@property (assign, nonatomic) BOOL backFromDetailView;
@property (strong, nonatomic) NSMutableArray *unifiedSearchResults;
@property (assign, nonatomic) BOOL moreUnifiedSearchResults;
@property (strong, nonatomic) NSString *lastSearchTerm;
@end


@implementation BCLoggingSearchSearchViewController

#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

	self.backFromDetailView = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
	if (!self.backFromDetailView) {
		[self.searchBar becomeFirstResponder];
	}
	// Whether we came back from the detail view or not, turn off our flag now, as we've used it
	self.backFromDetailView = NO;

	[super viewDidAppear:animated];
}

- (void)viewWillDisappearAnimated:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowSearchDetail"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

		FoodLogDetailViewController *detailView = segue.destinationViewController;
		detailView.delegate = self;

		NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
		NSDictionary *searchDict = self.unifiedSearchResults[indexPath.row];
		FoodLogMO *scratchLog = [self insertFoodLogWithDictionary:searchDict inContext:scratchContext];
		detailView.managedObjectContext = scratchLog.managedObjectContext;
		detailView.item = scratchLog;
	}
}

- (IBAction)detailViewDone:(UIStoryboardSegue *)segue
{
	self.backFromDetailView = YES;
	NSLog(@"sdetail");
}

#pragma mark - gesture recognizers
- (void)contentViewTapped:(UITapGestureRecognizer *)recognizer
{
	[self.searchBar resignFirstResponder];

	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	if (indexPath.row < self.unifiedSearchResults.count) {
		NSDictionary *searchItem = self.unifiedSearchResults[indexPath.row];
		FoodLogMO *newFoodLog = [self insertFoodLogWithDictionary:searchItem];

		// Save this
		[newFoodLog.managedObjectContext BL_save];

		NSString *labelText = [NSString stringWithFormat:@"%@ added to your log", newFoodLog.name];
		[BCProgressHUD notificationWithText:labelText onView:self.view.superview];
	}
	else {
		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
		UILabel *moreLabel = (UILabel *)[cell viewWithTag:10002];
		moreLabel.text = @"Loading...";
		UIActivityIndicatorView *spinner = (UIActivityIndicatorView *)[cell viewWithTag:10001];
		[spinner startAnimating];

		__typeof__(self) __weak weakSelf = self;

		NSIndexPath *moreIndexPath = indexPath;
		NSUInteger existingRowsCount = moreIndexPath.row;

		[self unifiedSmartSearchMoreWithCompletionBlock:^(NSUInteger resultCount, BOOL more, NSError *error) {
			if (!error) {
				dispatch_async(dispatch_get_main_queue(), ^{
					moreLabel.text = @"Show more results...";
					[spinner stopAnimating];

					NSMutableArray *indexPathsToInsert = [NSMutableArray array];
					for (NSInteger rowIndex = 0; rowIndex < resultCount; rowIndex++) {
						[indexPathsToInsert addObject:[NSIndexPath indexPathForRow:existingRowsCount + rowIndex inSection:0]];
					}

					[weakSelf.tableView beginUpdates];
					[weakSelf.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationTop];
					if (!more) {
						[weakSelf.tableView deleteRowsAtIndexPaths:@[moreIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
					}
					[weakSelf.tableView endUpdates];

					[weakSelf loadImagesForOnscreenRows];
				});
			}
		}];
	}
}

#pragma mark - local
- (void)unifiedSmartSearch:(NSString *)searchTerm completionBlock:(void (^)(NSUInteger resultCount, BOOL more, NSError *error))completionBlock
{
	if ([searchTerm isEqualToString:self.lastSearchTerm]) {
        // Duplicate search, this should be a noop but we still need to call the completion block so that it may
        // clean up whatever it may have
        completionBlock(0, NO, [NSError errorWithDomain:@"Duplicate Search" code:1 userInfo:nil]);
		return;
	}
	self.lastSearchTerm = searchTerm;

	NSURL *searchURL = [BCUrlFactory unifiedSmartSearchURLForSearchTerm:searchTerm offset:0 rows:10];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
	[fetcher beginFetchWithCompletionHandler:
		^(NSData *retrievedData, NSError *error) {
			NSUInteger resultCount = 0;
			BOOL showMore = NO;
			if (error != nil) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
			}
			else {
				NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:kNilOptions error:nil];
				NSDictionary *unifiedSection = resultDict[@"sections"][0];
				showMore = [[unifiedSection BC_numberForKey:@"showmore"] boolValue];
				NSArray *results = [unifiedSection objectForKey:@"items"];
				resultCount = [results count];

				_unifiedSearchResults = [results mutableCopy];
				self.moreUnifiedSearchResults = showMore;
			}
			completionBlock(resultCount, showMore, error);
		}];
}

- (void)unifiedSmartSearchMoreWithCompletionBlock:(void (^)(NSUInteger resultCount, BOOL more, NSError *error))completionBlock
{
	NSInteger offset = self.unifiedSearchResults.count;

	NSURL *searchURL = [BCUrlFactory unifiedSmartSearchURLForSearchTerm:self.lastSearchTerm offset:offset rows:10];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
	[fetcher beginFetchWithCompletionHandler:
		^(NSData *retrievedData, NSError *error) {
			NSUInteger resultCount = 0;
			BOOL showMore = NO;
			if (error != nil) {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
			}
			else {
				NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:kNilOptions error:nil];
				NSDictionary *unifiedSection = resultDict[@"sections"][0];
				showMore = [[unifiedSection BC_numberForKey:@"showmore"] boolValue];
				NSArray *results = [unifiedSection objectForKey:@"items"];
				resultCount = [results count];

				[_unifiedSearchResults addObjectsFromArray:results];
				self.moreUnifiedSearchResults = showMore;
			}
			completionBlock(resultCount, showMore, error);
		}];
}

- (id)insertFoodLogWithDictionary:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)moc
{
	NSString *itemType = [dictionary BC_stringOrNilForKey:kUnifiedSearchItemType];

	FoodLogMO* newFoodLog = nil;
	if ([itemType isEqualToString:kUnifiedSearchItemTypeProduct]) {
		newFoodLog = [self insertFoodLogWithProductDictionary:dictionary inContext:moc];
	}
	else if ([itemType isEqualToString:kUnifiedSearchItemTypeRecipe]) {
		newFoodLog = [self insertFoodLogWithRecipeDictionary:dictionary inContext:moc];
	}
	else if ([itemType isEqualToString:kUnifiedSearchItemTypeMenuItem]) {
		newFoodLog = [self insertFoodLogWithMenuDictionary:dictionary inContext:moc];
	}

	return newFoodLog;
}

- (id)insertFoodLogWithDictionary:(NSDictionary *)dictionary
{
	return [self insertFoodLogWithDictionary:dictionary inContext:self.managedObjectContext];
}

- (id)insertFoodLogCommonWithDictionary:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)moc
{
	FoodLogMO *newFoodLog = [FoodLogMO insertInManagedObjectContext:moc];

	newFoodLog.status = kStatusPost;
	newFoodLog.loginId = [[User currentUser] loginId];
	newFoodLog.date = self.logDate;
	newFoodLog.logTime = self.logTime;
	newFoodLog.servings = @1;
	newFoodLog.nutritionFactor = @1;
	newFoodLog.itemType = [dictionary BC_stringOrNilForKey:kUnifiedSearchItemType];
	newFoodLog.imageId = [dictionary BC_numberOrNilForKey:kUnifiedSearchImageID];

	return newFoodLog;
}

- (id)insertFoodLogWithRecipeDictionary:(NSDictionary *)recipe inContext:(NSManagedObjectContext *)moc
{
	FoodLogMO *newFoodLog = [self insertFoodLogCommonWithDictionary:recipe inContext:moc];

	NSNumber *recipeId = [recipe BC_numberOrNilForKey:kUnifiedSearchItemID];
	RecipeMO *localRecipe = [RecipeMO recipeById:recipeId withManagedObjectContext:moc];

	if (!localRecipe) {
		localRecipe = [RecipeMO insertInManagedObjectContext:moc];
		[localRecipe setWithUnifiedSearchDictionary:recipe];
		// Set the accountId last, as the accountId in the dictionary may
		// be zero if this recipe isn't yet subscribed to
		localRecipe.accountId = [[User currentUser] accountId];
	}

	if (!localRecipe.nutrition) {
		localRecipe.nutrition = [NutritionMO insertInManagedObjectContext:moc];
		[localRecipe.nutrition setWithUpdateDictionary:recipe];
	}

	newFoodLog.recipe = localRecipe;
	newFoodLog.name = localRecipe.name;
	newFoodLog.nutrition = [NutritionMO insertInManagedObjectContext:moc];
	[newFoodLog.nutrition copyFromNutritionMO:localRecipe.nutrition];

	return newFoodLog;
}

- (id)insertFoodLogWithProductDictionary:(NSDictionary *)product inContext:(NSManagedObjectContext *)moc
{
	FoodLogMO *newFoodLog = [self insertFoodLogCommonWithDictionary:product inContext:moc];

    newFoodLog.servingSize = [NSString stringWithFormat:@"%@ %@", [product valueForKey:kUnifiedSearchServingSizeText],
		[product valueForKey:kUnifiedSearchServingSizeUnit]];
	newFoodLog.name = [product objectForKey:kName];
	newFoodLog.productId = [product BC_numberForKey:kUnifiedSearchItemID];
	newFoodLog.customFood = [CustomFoodMO MR_findFirstByAttribute:CustomFoodMOAttributes.cloudId
		withValue:newFoodLog.productId inContext:moc];

	[newFoodLog setNutritionWithDictionary:product];

	return newFoodLog;
}

- (id)insertFoodLogWithMenuDictionary:(NSDictionary *)menu inContext:(NSManagedObjectContext *)moc
{
	FoodLogMO *newFoodLog = [self insertFoodLogCommonWithDictionary:menu inContext:moc];

	newFoodLog.name = [menu objectForKey:kName];
	newFoodLog.menuId = [menu BC_numberForKey:kUnifiedSearchItemID];

	[newFoodLog setNutritionWithDictionary:menu];

	return newFoodLog;
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];

	for (NSIndexPath *indexPath in visiblePaths) {
		if (indexPath.row < self.unifiedSearchResults.count) {
			NSDictionary *searchItem = self.unifiedSearchResults[indexPath.row];
			NSNumber *imageId = [searchItem BC_numberOrNilForKey:kUnifiedSearchImageID];
			if ([imageId integerValue] > 0) {
				[self.tableView downloadImageWithImageId:imageId forIndexPath:indexPath withSize:kImageSizeMedium];
			}
		}
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	numberOfRows = self.unifiedSearchResults.count;
	if (self.moreUnifiedSearchResults) {
		numberOfRows += 1;
	}
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *subtitleCellID = @"SubtitleCell";
    static NSString *showMoreCellID = @"ShowMoreCell";

    UITableViewCell *cell = nil;
	if (indexPath.row < self.unifiedSearchResults.count) {
    	cell = [tableView dequeueReusableCellWithIdentifier:subtitleCellID forIndexPath:indexPath];
	}
	else {
    	cell = [tableView dequeueReusableCellWithIdentifier:showMoreCellID forIndexPath:indexPath];
	}
    
	UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTapped:)];
	[cell.contentView addGestureRecognizer:recognizer];	
    
	[self configureCell:cell atIndexPath:indexPath];

    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row < self.unifiedSearchResults.count) {
		NSDictionary *searchItem = self.unifiedSearchResults[indexPath.row];
		NSString *itemType = [searchItem BC_stringOrNilForKey:kFoodLogItemType];
		NSNumber *imageId = [searchItem BC_numberOrNilForKey:kUnifiedSearchImageID];
		cell.textLabel.text = [searchItem objectForKey:kName];

		if ([itemType isEqualToString:kUnifiedSearchItemTypeRecipe]) {
			NSNumber *calories = [searchItem BC_numberOrNilForKey:kNutritionCalories];
			if (calories) {
				cell.detailTextLabel.text = [NSString stringWithFormat:@"1 serving, %ld calories", (long)[calories integerValue]];
			}
			else {
				cell.detailTextLabel.text = @"1 serving";
			}
		}
		else if ([itemType isEqualToString:kUnifiedSearchItemTypeProduct]) {
			NSMutableArray *details = [NSMutableArray array];

			NSString *servingSizeText = [searchItem BC_stringOrNilForKey:kUnifiedSearchServingSizeText];
			NSString *servingSizeUnit = [searchItem BC_stringOrNilForKey:kUnifiedSearchServingSizeUnit];
			if (servingSizeText && servingSizeUnit) {
				[details addObject:[NSString stringWithFormat:@"%@ %@", servingSizeText, servingSizeUnit]];
			}

			NSNumber *calories = [searchItem BC_numberOrNilForKey:kNutritionCalories];
			if (calories) {
				[details addObject:[NSString stringWithFormat:@"%ld calories", (long)[calories integerValue]]];
			}

			cell.detailTextLabel.text = [details componentsJoinedByString:@", "];
		}
		else if ([itemType isEqualToString:kUnifiedSearchItemTypeMenuItem]) {
			NSNumber *calories = [searchItem BC_numberOrNilForKey:kNutritionCalories];
			if (calories) {
				cell.detailTextLabel.text = [NSString stringWithFormat:@"1 serving, %ld calories", (long)[calories integerValue]];
			}
			else {
				cell.detailTextLabel.text = @"1 serving";
			}
		}
		cell.imageView.image = [[BCImageCache sharedCache] imageWithImageId:imageId withItemType:itemType withSize:kImageSizeMedium];
	}
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	[searchBar resignFirstResponder];

	MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.labelText = @"searching...";

	[self unifiedSmartSearch:searchBar.text completionBlock:^(NSUInteger resultCount, BOOL more, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[MBProgressHUD hideHUDForView:self.view animated:YES];
			if (!error) {
				// Ensure that the image trackers don't try to update indexPaths until after we refresh
				[self.tableView clearTrackerIndexPaths];

				// NOTE: May get an error in domain "Duplicate Search" with a code of 1, which is not really
				// an error, but rather the search telling us that we don't need to do this
				[self.tableView reloadData];
				[self loadImagesForOnscreenRows];
			}
		});
	}];
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	DDLogInfo(@"%@ - view:%@ didUpdateObject:%@", THIS_FILE, viewController, object);

	self.backFromDetailView = YES;

	__typeof__(self) __weak weakself = self;
	[self dismissViewControllerAnimated:YES completion:^{
		if (object) {
			// pass this back to the logging view
			[weakself.delegate view:viewController didUpdateObject:object];
		}
	}];
}

@end
