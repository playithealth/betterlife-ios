//
//  NSNumber+BettrLife.m
//  BettrLife
//
//  Created by Greg Goodrich on 4/17/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "NSNumber+BettrLife.h"

@implementation NSNumber (BettrLife)

- (NSNumber *)BL_numberWithPrecision:(NSNumber *)precision
{
	double multiplier = pow(10, [precision doubleValue]);
	double number = round([self doubleValue] * multiplier) / multiplier;

	return ([NSNumber numberWithDouble:number]);
}

@end
