//
//  ProductDetailViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/26/10.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "ProductDetailViewController.h"

#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BCAppDelegate.h"
#import "CategoryMO.h"
#import "GenericValueDisplay.h"
#import "GTMHTTPFetcher.h"
#import "GTMHTTPFetcherAdditions.h"
#import "HTTPQueue.h"
#import "NSArray+NestedArrays.h"
#import "NumberValue.h"
#import "NutritionCell.h"
#import "ProductPurchaseHistoryView.h"
#import "Promotion.h"
#import "PromotionCell.h"
#import "Store.h"
#import "UIColor+Additions.h"
#import "UIBarButtonItemAdditions.h"

static const NSUInteger kTagName = 10001;
static const NSUInteger kTagCategory = 10002;
static const NSUInteger kTagBarcode = 10003;
static const NSUInteger kTagNameLabel = 11111;
static const NSUInteger kTagValueLabel = 11112;

static const NSInteger kSectionItem = 0;
static const NSInteger kSectionProductDetails = 1;
static const NSInteger kSectionPromotions = 2;
static const NSInteger kSectionPurchaseHx = 3;
static const NSInteger kSectionNutrition = 4;

@implementation ProductDetailItem
@synthesize name;
@synthesize barcode;
@synthesize productId;
@synthesize imageId;
@synthesize categoryId;
@end


@interface ProductDetailViewController () <UIWebViewDelegate>
{
	ProductDetailItem *_productDetailItem;
}

/// a set of parrallel arrays that hold the table data
@property (strong, nonatomic) NSArray *sectionNames;
@property (strong, nonatomic) NSArray *sectionHasData;
@property (strong, nonatomic) NSMutableArray *visibleSections;
@property (strong, nonatomic) NSArray *rowLabels;
@property (strong, nonatomic) NSArray *rowKeys;
@property (strong, nonatomic) NSArray *rowTags;
/// the promotions associated with this shopping list item
@property (strong, nonatomic) NSArray *itemPromotions;
/// the nutrition html returned from the api for the related product
@property (strong, nonatomic) NSString *itemNutrition;
/// flag that tells whether the item needs to be saved or not
/// maybe should be renamed hasChanges or something
@property (assign, nonatomic) BOOL changedItem;
/// the text field currently being edited
@property (strong, nonatomic) UITextField *editingTextField;
/// height of the nutrition row which is dynamic
@property (assign, nonatomic) NSInteger nutritionRowHeight;
/// the web view which displays the nutrition html
@property (strong, nonatomic) UIWebView *nutritionWebView;
/// the spinner shown on the nutrition row while loading
@property (strong, nonatomic) UIActivityIndicatorView *nutritionSpinner;
@property (strong, nonatomic) UIImage *productImage;

/// a helper function which configures the UI elements in the cell
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
/// a helper function which returns the section type based on the index path
- (NSUInteger)getSectionTypeWithIndexPath:(NSIndexPath *)indexPath;
/// a helper function which returns the section type based on the section index
- (NSUInteger)getSectionTypeWithSection:(NSInteger)indexPath;
/**
 * Rebuilds the sections for the UITableView based on the information returned
 * by the callback blocks defined in sectionHasData.
 */
- (void)checkForData;
- (void)refreshProductImage;
- (void)purchaseHxButtonTapped:(UITapGestureRecognizer *)recognizer;
@end

@implementation ProductDetailViewController
@synthesize tableView;
@synthesize sectionNames;
@synthesize sectionHasData;
@synthesize visibleSections;
@synthesize rowLabels;
@synthesize rowKeys;
@synthesize rowTags;
@synthesize itemPromotions;
@synthesize itemNutrition;
@synthesize changedItem;
@synthesize editingTextField;
@synthesize nutritionRowHeight;
@synthesize nutritionWebView;
@synthesize nutritionSpinner;
@synthesize productImage;

#pragma mark - lifespan
#pragma mark - memory management
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.nutritionRowHeight = 44;
	}
	return self;
}

- (void)viewDidLoad
{
	self.sectionNames = [NSArray arrayWithObjects:
		@"Item",
		@"Details",
		@"Promotions",
		[NSNull null],
		@"Nutrition",
		nil];

	self.sectionHasData = [NSArray arrayWithObjects:
		^NSUInteger (ProductDetailViewController *me)
			{ return 1; },
		^NSUInteger (ProductDetailViewController * me)
			{ return [me.rowLabels count]; },
		^NSUInteger (ProductDetailViewController *me)
			{ return [me.itemPromotions count]; },
		^NSUInteger (ProductDetailViewController *me)
			{ return 1; },
		^NSUInteger (ProductDetailViewController *me)
			{ return ([me.itemNutrition length] > 0) ? 1:0; },
		nil];

	self.visibleSections = [[NSMutableArray alloc] init];

	self.rowLabels = [NSArray arrayWithObjects:
		@"Category",
		@"Barcode",
		nil];

	self.rowKeys = [NSArray arrayWithObjects:
		[NSArray arrayWithObject:[NSNull null]],
		[NSArray arrayWithObjects:
			@"category",
			@"barcode",
			nil],
		[NSArray arrayWithObject:[NSNull null]],
		[NSArray arrayWithObject:[NSNull null]],
		[NSArray arrayWithObject:[NSNull null]],
		nil];

	self.rowTags = [NSArray arrayWithObjects:
		[NSArray arrayWithObject:
			[NSNumber numberWithInteger:kTagName]],
		[NSArray arrayWithObjects:
			[NSNumber numberWithInteger:kTagCategory],
			[NSNumber numberWithInteger:kTagBarcode],
			nil],
		[NSArray arrayWithObject:[NSNull null]],
		[NSArray arrayWithObject:[NSNull null]],
		[NSArray arrayWithObject:[NSNull null]],
		nil];

	// set up the web view that holds the nutrition data
	UIWebView *aWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 300, 375)];
	aWebView.hidden = YES;
	self.nutritionWebView = aWebView;

	// create the spinner that is used to show that it is loading nutrition data
	UIActivityIndicatorView *aSpinner = [[UIActivityIndicatorView alloc]
		initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	aSpinner.hidesWhenStopped = YES;
	self.nutritionSpinner = aSpinner;
	
	UIView *backgroundView = [[UIView alloc] init];
	backgroundView.backgroundColor = [UIColor BC_lightGrayColor];
	self.tableView.backgroundView = backgroundView;

	HTTPQueue *detailsQueue = [HTTPQueue queueNamed:@"ProductDetails" completionBlock:
		^(NSString *identifier, NSError *error) {
			//QuietLog(@"completion block for %@, error:%@", identifier, error);
			[self checkForData];
			[self.tableView reloadData];
		}];

	if (self.productDetailItem.imageId) {
		[self refreshProductImage];
	}

	if ([self.productDetailItem.productId integerValue] != 0) {
		BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
		if (appDelegate.connected) {

			__block ProductDetailViewController *me = self;

			// put something in the nutrition so that the section shows up
			// this gets overridden in the nutritionFetcher
			self.itemNutrition = @"loading";

			GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory nutritionForProductURL:self.productDetailItem.productId]];

			[detailsQueue addRequest:fetcher completionBlock:
				^void(NSData *retrievedData, NSError *error) {
					NSIndexPath *nutritionIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
					UITableViewCell *cell = [me.tableView cellForRowAtIndexPath:nutritionIndexPath];

					NSString *response = [[NSString alloc] initWithData:retrievedData
						encoding:NSUTF8StringEncoding];
					if (!error) {
						// fetch succeeded

						if ([response isKindOfClass:[NSString class]]) {
							me.itemNutrition = response;

							[me.nutritionSpinner stopAnimating];
							[me setNutritionSpinner:nil];
							cell.accessoryView = nil;

							cell.textLabel.text = nil;

							me.nutritionWebView.hidden = NO;
							me.nutritionWebView.delegate = me;
							me.nutritionRowHeight = 44;
							[me.nutritionWebView loadHTMLString:me.itemNutrition baseURL:nil];
						}
					}
					else {
						[me.nutritionSpinner stopAnimating];
						[me setNutritionSpinner:nil];
						cell.accessoryView = nil;

						cell.textLabel.text = @"Failed to load nutrition";

						[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:response];
					}
				}];

			fetcher = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory productsURLInfoForProductId:self.productDetailItem.productId]];

			[detailsQueue addRequest:fetcher completionBlock:
				^void(NSData *retrievedData, NSError *error) {
					NSString *response = [[NSString alloc] initWithData:retrievedData
						encoding:NSUTF8StringEncoding];
					if (!error) {
						id responseData = [response JSONValue];

						NSDictionary *productInfo = nil;
						if ([responseData isKindOfClass:[NSDictionary class]]) {
							productInfo = (NSDictionary *)responseData;
						}

						me.productDetailItem.barcode = [productInfo valueForKey:kBarcode];
						me.productDetailItem.categoryId = [productInfo valueForKey:kProductCategoryId];
						me.productDetailItem.imageId = [productInfo valueForKey:kImageId];

						[me.tableView reloadData];

						[me refreshProductImage];
					}
					else {
						[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:response];
					}
				}];
		}
		else {
			NSIndexPath *nutritionIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
			UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:nutritionIndexPath];
			cell.textLabel.text = @"Failed to load nutrition";
			[self.nutritionSpinner stopAnimating];
			[self setNutritionSpinner:nil];
			cell.accessoryView = nil;
		}
	}

	[detailsQueue runQueue:nil];

	[super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated
{
	self.title = @"Item Details";

	[self checkForData];
	[self.tableView reloadData];

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
	self.productDetailItem = nil;

	// Unsure if these are necessary or correct being here
	self.rowLabels = nil;
	self.rowKeys = nil;
	self.rowTags = nil;
	self.sectionNames = nil;
	self.sectionHasData = nil;
	self.visibleSections = nil;
	self.itemPromotions = nil;
	self.itemNutrition = nil;

    [self setTableView:nil];
	[super viewDidUnload];
}

- (void)dealloc
{
	[self setProductDetailItem:nil];


}

#pragma mark - Custom getter/setter
- (void)setProductDetailItem:(id)item
{
	if (item) {
		_productDetailItem = [[ProductDetailItem alloc] init];
		if ([item isKindOfClass:[NSDictionary class]]) {
			_productDetailItem.name = [item objectForKey:kName];
			_productDetailItem.barcode = [item objectForKey:kBarcode];
			_productDetailItem.productId = [item objectForKey:kProductId];
			_productDetailItem.imageId = [item objectForKey:kImageId];
			_productDetailItem.categoryId = [item objectForKey:kProductCategoryId];
		}
		else {
			_productDetailItem.name = [item name];
			_productDetailItem.barcode = [item barcode];
			_productDetailItem.productId = [item productId];
			_productDetailItem.imageId = [item imageId];
			//_productDetailItem.categoryId = [item categoryId];
		}
	}
}
- (ProductDetailItem *)productDetailItem
{
	return _productDetailItem;
}

#pragma mark - local methods
- (NSUInteger)getSectionTypeWithIndexPath:(NSIndexPath *)indexPath
{
	return ([[self.visibleSections objectAtIndex:[indexPath section]] integerValue]);
}

- (NSUInteger)getSectionTypeWithSection:(NSInteger)section
{
	return ([[self.visibleSections objectAtIndex:section] integerValue]);
}

- (void)checkForData
{
	[self.visibleSections removeAllObjects];
	for (int idx = 0; idx < [self.sectionNames count]; idx++) {
		BOOL (^hasData)(ProductDetailViewController *) = [self.sectionHasData objectAtIndex:idx];
		if (hasData(self)) {
			[self.visibleSections addObject:[NSNumber numberWithInt:idx]];
		}
	}
}

- (void)purchaseHxButtonTapped:(UITapGestureRecognizer *)recognizer
{
	ProductPurchaseHistoryView *view = [[ProductPurchaseHistoryView alloc]
		initWithNibName:nil bundle:nil];
	view.purchasedItem = (id<PurchasedItem>)self.productDetailItem;
	[self.navigationController pushViewController:view animated:YES];
}

- (void)refreshProductImage
{
	// check to see if we are on an iPhone 4, use the medium image
	NSString *imageSize = @"small";
	if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] 
			&& [[UIScreen mainScreen] scale] == 2){
		imageSize = @"medium";
	}

	NSURL *url = [BCUrlFactory imageURLForImage:self.productDetailItem.imageId
		imageSize:imageSize];
	GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher signedFetcherWithURL:url];
	[imageFetcher beginFetchWithCompletionHandler:
		^(NSData *retrievedData, NSError *error) {
			if (!error) {
				UIImage *tempImage = [[UIImage alloc] initWithData:retrievedData];
				self.productImage = tempImage;
				NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
				UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
				[self configureCell:cell atIndexPath:indexPath];
			}
			else {
				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:imageFetcher response:nil];
			}
		}];
}

#pragma mark - UITableView data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [self.visibleSections count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	id theTitle = [self.sectionNames objectAtIndex:[self getSectionTypeWithSection:section]];
	if ([theTitle isKindOfClass:[NSNull class]]) {
		return nil;
	}

	return theTitle;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSUInteger (^numberOfRows)(ProductDetailViewController *)
		= [self.sectionHasData objectAtIndex:[self getSectionTypeWithSection:section]];
	return numberOfRows(self);
}

- (UITableViewCell *)tableView:(UITableView *)theTableView
   cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;

	switch ([self getSectionTypeWithIndexPath:indexPath]) {
		case kSectionItem :
			{
				static NSString *itemCellId = @"ProductDetailItemCellId";
				cell = [theTableView dequeueReusableCellWithIdentifier:itemCellId];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] 
						initWithStyle:UITableViewCellStyleDefault
						reuseIdentifier:itemCellId];
					cell.textLabel.numberOfLines = 2;
					cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
				}
			}
			break;
		case kSectionProductDetails :
			{
				static NSString *detailCellId = @"ProductDetailDetailsCellId";

				cell = [theTableView dequeueReusableCellWithIdentifier:detailCellId];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
						reuseIdentifier:detailCellId];
					UILabel *label = [[UILabel alloc] 
						initWithFrame:CGRectMake(10, 10, 75, 25)];
					label.textAlignment = NSTextAlignmentRight;
					label.backgroundColor = [UIColor clearColor];
					label.tag = kTagNameLabel;
					label.font = [UIFont boldSystemFontOfSize:14];
					[cell.contentView addSubview:label];

					UILabel *valueLabel = [[UILabel alloc]
						initWithFrame:CGRectMake(90, 10, 200, 25)];
					valueLabel.backgroundColor = [UIColor clearColor];
					valueLabel.tag = kTagValueLabel;
					[cell.contentView addSubview:valueLabel];
				}
			}
			break;

		case kSectionPromotions:
			{
				static NSString *promotionCellId = @"ProductDetailPromotionCellId";

				cell = [theTableView dequeueReusableCellWithIdentifier:promotionCellId];
				if (cell == nil) {
					NSArray *topLevelObjects = [[NSBundle mainBundle]
						loadNibNamed:@"PromotionCell" owner:self options:nil];
					cell = [topLevelObjects objectAtIndex:0];

					// Create a new UIView to eliminate "border" drawing around each row/cell
					UIView *transparentView = [[UIView alloc] initWithFrame:CGRectZero];
					transparentView.backgroundColor = [UIColor clearColor];
					cell.backgroundView = transparentView;
				}
			}
			break;

		case kSectionPurchaseHx:
			{
				static NSString *purchaseHxCellId = @"ProductDetailPurchaseHxCellId";

				cell = [theTableView dequeueReusableCellWithIdentifier:purchaseHxCellId];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
						reuseIdentifier:purchaseHxCellId];
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

					// Create a new UIView to eliminate "border" drawing around each row/cell
					UIView *transparentView = [[UIView alloc] initWithFrame:CGRectZero];
					transparentView.backgroundColor = [UIColor clearColor];
					cell.backgroundView = transparentView;

					UIButton *purchaseHxButton = [[UIButton alloc]
						initWithFrame:CGRectMake(0, 0, 306, 51)];

					UIImageView *greenButtonView = [[UIImageView alloc]
						initWithImage:[UIImage imageNamed:@"btn-green.png"]];
					greenButtonView.center = CGPointMake(160, 25);
					[purchaseHxButton addSubview:greenButtonView];

					UIImageView *historyImageView = [[UIImageView alloc]
						initWithImage:[UIImage imageNamed:@"history-ico.png"]];
					historyImageView.frame = CGRectMake(16, 6, 35, 35);
					[purchaseHxButton addSubview:historyImageView];

					[cell addSubview:purchaseHxButton];

					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(50, 12, 200, 22)];
					label.text = @"View Purchase History";
					label.textColor = [UIColor whiteColor];
					label.backgroundColor = [UIColor clearColor];
					label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
					[cell addSubview:label];

					UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]
						initWithTarget:self action:@selector(purchaseHxButtonTapped:)];
					[purchaseHxButton addGestureRecognizer:recognizer];
				}
			}
			break;

		case kSectionNutrition:
			{
				static NSString *nutritionCellId = @"ProductDetailNutritionCellId";

				cell = [theTableView dequeueReusableCellWithIdentifier:nutritionCellId];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
						reuseIdentifier:nutritionCellId];

					cell.accessoryView = self.nutritionSpinner;
					[self.nutritionSpinner startAnimating];

					cell.textLabel.text = @"checking for nutrition info...";

					[cell.contentView addSubview:self.nutritionWebView];
				}
			}
			break;

		default:
			break;
	}

	if (cell) {
		[self configureCell:cell atIndexPath:indexPath];
	}

	return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	switch ([self getSectionTypeWithIndexPath:indexPath]) {
		case kSectionItem:
			{
				cell.textLabel.text = self.productDetailItem.name;
				if (self.productImage) {
					cell.imageView.image = self.productImage;
				}
				else {
					cell.imageView.image = [UIImage imageNamed:@"placeholder.png"];
				}
			}
			break;
		case kSectionProductDetails:
			{
				// Product details section
				UILabel *label = (UILabel *)[cell viewWithTag:kTagNameLabel];
				label.text = [self.rowLabels objectAtIndex:[indexPath row]];

				id <GenericValueDisplay, NSObject> rowValue = nil;
				NSString *rowKey = [self.rowKeys BC_nestedObjectAtIndexPath:indexPath];
				if ([rowKey isEqualToString:@"category"]) {
					rowValue = [CategoryMO categoryById:[self.productDetailItem categoryId] withManagedObjectContext:self.managedObjectContext];
				}
				else if ([rowKey isEqualToString:@"barcode"]) {
					rowValue = [self.productDetailItem barcode];
				}
				//id <GenericValueDisplay, NSObject> rowValue = [self.productDetailItem valueForKey:rowKey];

				UILabel *valueLabel = (UILabel *)[cell viewWithTag:kTagValueLabel];
				valueLabel.text = [rowValue genericValueDisplay];
			}
			break;

		case kSectionPromotions:
			{
				// Promotions section
				PromotionCell *promotionCell = (PromotionCell *)cell;
				Promotion *promotion = nil;
				promotion = [itemPromotions objectAtIndex:[indexPath row]];
				if (promotion) {
					promotionCell.discountLabel.text = promotion.discount;
					promotionCell.nameLabel.text = promotion.name;
					promotionCell.descriptionLabel.text = promotion.promoDescription;
					promotionCell.disclaimerLabel.text = promotion.disclaimer;
					NSString *imageName = [NSString stringWithFormat:@"promotion-cell-background%@.png",
							 ([promotion.accepted boolValue] ? @"-selected":@"")];
					promotionCell.backgroundImageView.image = [UIImage imageNamed:imageName];
				}
			}
			break;

		case kSectionNutrition :
			{
				// the nutrition html is loaded from the nutritionFetcher
			}
			break;

		case kSectionPurchaseHx:
			cell.textLabel.text = @"View Purchase Hx";
			break;

		default:
			break;
	}
}

#pragma mark - UITableView delegates
- (NSIndexPath *)tableView:(UITableView *)tableView
   willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	return nil;
}

- (void)tableView:(UITableView *)theTableView
   didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (CGFloat)tableView:(UITableView *)tableView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height;

	switch ([self getSectionTypeWithIndexPath:indexPath]) {
		case kSectionItem:
			height = 100;
			break;

		case kSectionPromotions:
			height = 155;
			break;

		case kSectionNutrition:
			height = self.nutritionRowHeight;
			break;

		case kSectionPurchaseHx:
		case kSectionProductDetails:
		default:
			height = 44;
			break;
	}

	return height;
}


#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	CGFloat webViewHeight = 0.0f;
	if (nutritionWebView.subviews.count > 0) {
		UIView *scrollerView = [nutritionWebView.subviews objectAtIndex:0];
		if (scrollerView.subviews.count > 0) {
			UIView *webDocView = scrollerView.subviews.lastObject;
			if ([webDocView isKindOfClass:[NSClassFromString (@"UIWebDocumentView")class]]) {
				webViewHeight = webDocView.frame.size.height;
				self.nutritionRowHeight = webViewHeight + 16;
				nutritionWebView.frame = CGRectMake(0, 8, 300, webViewHeight);
				nutritionWebView.delegate = nil;
				[self checkForData];
				[self.tableView reloadData];
			}
		}
	}
	//QuietLog(@"height %f", webViewHeight);
}

@end

