//
//  BCFitbitStepsDetailViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 7/29/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TrainingActivityMO;

@interface BCFitbitStepsDetailViewController : UITableViewController

@property (strong, nonatomic) TrainingActivityMO *activityLog;

@end

