//
//  BLOnboardWelcomeViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 3/27/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLOnboardWelcomeViewController.h"

#import "BCAppDelegate.h"
#import "BCPermissionsDataModel.h"
#import "BLOnboardViewDelegate.h"
#import "CoachInviteMO.h"
#import "User.h"

NSString * const kOBStoryboardId = @"storyboard";
NSString * const kOBViewProperties = @"properties";

@interface BLOnboardWelcomeViewController ()
@end

@implementation BLOnboardWelcomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Check permissions and build the view 'stack'
	self.viewStack = [BLOnboardWelcomeViewController buildViewStack];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - local
+ (NSArray *)buildViewStack
{
	NSMutableArray *stack = [[NSMutableArray alloc] initWithArray:@[@{ kOBStoryboardId : @"Onboard" }]];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSArray *allInvites = [CoachInviteMO MR_findByAttribute:CoachInviteMOAttributes.loginId withValue:[[User currentUser] loginId]
		inContext:appDelegate.adManagedObjectContext];
	
	for (CoachInviteMO *coachInvite in allInvites) {
		[stack addObject:@{ kOBStoryboardId : @"Onboard Invite", kOBViewProperties : @{ @"invitation" : coachInvite } }];
	}

	if ([BCPermissionsDataModel userHasPermission:kPermissionFoodlog]
			|| [BCPermissionsDataModel userHasPermission:kPermissionActivitylog]
			|| [BCPermissionsDataModel userHasPermission:kPermissionMealplan]
			|| [BCPermissionsDataModel userHasPermission:kPermissionActivityplan]) {

		[stack addObject:@{ kOBStoryboardId : @"Onboard Demographics" }];
		//[stack addObject:@{ kOBStoryboardId : @"Onboard Activity" }]; // This is now combined with the Demographics view
	}

	if ([BCPermissionsDataModel userHasPermission:kPermissionGoals]) {
		[stack addObject:@{ kOBStoryboardId : @"Onboard Nutrition" }];
	}

	if ([BCPermissionsDataModel userHasPermission:kPermissionHealthprefs]) {
		[stack addObject:@{ kOBStoryboardId : @"Onboard Lifestyle" }];
		[stack addObject:@{ kOBStoryboardId : @"Onboard Allergies" }];
	}

	[stack addObject:@{ kOBStoryboardId : @"Onboard Final" }];

	return stack;
}

+ (BOOL)canUseOnboarding
{
	NSArray *viewStack = [self buildViewStack];

	// If there are more views in the view stack than just the welcome and final, then they can use onboarding
	return ([viewStack count] > 2);
}

#pragma mark - responders
- (IBAction)nextViewController:(id)sender {
	[self nextView];
}

- (void)nextView
{
    NSDictionary *stackDict = [self.viewStack objectAtIndex:[[self.navigationController viewControllers] count]];
    NSString *storyboardId = [stackDict valueForKey:kOBStoryboardId];
	UIViewController <BLOnboardViewDelegate> *viewController = [self.storyboard instantiateViewControllerWithIdentifier:storyboardId];

	NSAssert1([[viewController class] conformsToProtocol:@protocol(BLOnboardViewDelegate)],
		@"%@ does not conform to the BLOnboardViewDelegate Protocol", [viewController class]);

	NSDictionary *propertiesDict = [stackDict objectForKey:kOBViewProperties];
	if (propertiesDict) {
		[viewController setValuesForKeysWithDictionary:propertiesDict];
	}

	[viewController setManagedObjectContext:self.managedObjectContext];
	[viewController setViewStack:self.viewStack];
	[viewController setDelegate:self];

	[self.navigationController pushViewController:viewController animated:YES];
}

@end
