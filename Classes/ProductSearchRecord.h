//
//  ProductSearchRecord.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/27/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CategoryMO;

@interface ProductSearchRecord : NSObject

@property (strong, nonatomic) NSNumber *productId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *genericName;
@property (strong, nonatomic) NSString *size;
@property (strong, nonatomic) NSNumber *imageId;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *barcode;
@property (strong, nonatomic) NSNumber *rating;
@property (strong, nonatomic) NSNumber *ratingCount;
@property (strong, nonatomic) NSNumber *productCategoryId;
@property (strong, nonatomic) CategoryMO *category;

@property (strong, nonatomic) NSNumber *promotionAvailable;
@property (strong, nonatomic) NSNumber *promotionCount;
@property (strong, nonatomic) NSNumber *allergyCount;
@property (strong, nonatomic) NSNumber *lifestyleCount;
@property (strong, nonatomic) NSNumber *ingredientCount;

@property (strong, nonatomic) NSNumber *calories;
@property (strong, nonatomic) NSNumber *caloriesFromFat;
@property (strong, nonatomic) NSNumber *totalFat;
@property (strong, nonatomic) NSNumber *transFat;
@property (strong, nonatomic) NSNumber *saturatedFat;
@property (strong, nonatomic) NSNumber *saturatedFatCalories;
@property (strong, nonatomic) NSNumber *polyunsaturatedFat;
@property (strong, nonatomic) NSNumber *monounsaturatedFat;
@property (strong, nonatomic) NSNumber *cholesterol;
@property (strong, nonatomic) NSNumber *sodium;
@property (strong, nonatomic) NSNumber *potassium;
@property (strong, nonatomic) NSNumber *totalCarbohydrates;
@property (strong, nonatomic) NSNumber *otherCarbohydrates;
@property (strong, nonatomic) NSNumber *dietaryFiber;
@property (strong, nonatomic) NSNumber *solubleFiber;
@property (strong, nonatomic) NSNumber *insolubleFiber;
@property (strong, nonatomic) NSNumber *sugars;
@property (strong, nonatomic) NSNumber *sugarsAlcohol;
@property (strong, nonatomic) NSNumber *protein;
@property (strong, nonatomic) NSNumber *vitaminAPercent;
@property (strong, nonatomic) NSNumber *vitaminCPercent;
@property (strong, nonatomic) NSNumber *calciumPercent;
@property (strong, nonatomic) NSNumber *ironPercent;

- (id)initWithSearchDictionary:(NSDictionary *)searchDict withManagedObjectContext:(NSManagedObjectContext *)moc;
+ (id)recordWithSearchDictionary:(NSDictionary *)searchDict withManagedObjectContext:(NSManagedObjectContext *)moc;
- (void)setWithSearchDictionary:(NSDictionary *)updateDict withManagedObjectContext:(NSManagedObjectContext *)moc;
- (NSDictionary *)dictionary;

@end
