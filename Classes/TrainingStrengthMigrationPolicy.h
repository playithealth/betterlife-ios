//
//  TrainingStrengthMigrationPolicy.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/27/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface TrainingStrengthMigrationPolicy : NSEntityMigrationPolicy
- (NSString *)nameFromFocus:(NSString *)focus equipment:(NSString *)equipment;
@end
