//
//  BCLoggingSearchSearchViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/9/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCDetailViewDelegate.h"

@class BCLoggingFoodSearchDataModel;

@interface BCLoggingSearchSearchViewController : UITableViewController <BCDetailViewDelegate>
@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;
@property (strong, nonatomic) BCLoggingFoodSearchDataModel *dataModel;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSDate *logDate;
@property (assign, nonatomic) NSNumber *logTime;
@end
