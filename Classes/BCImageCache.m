//
//  BCImageCache.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 1/3/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCImageCache.h"

#import "UIImage+Additions.h"

// 6 hours = 60 * 60 * 6
#define kAGE_OUT_IN_SECONDS 21600

@interface BCImageCacheEntry : NSObject
@property (strong, nonatomic) id image;
@property (assign, nonatomic) NSTimeInterval timestamp;

- (id)initWithImage:(id)image withAge:(NSTimeInterval)age;
@end

@implementation BCImageCacheEntry

- (id)initWithImage:(id)image withAge:(NSTimeInterval)age
{
	self = [super init];
    if (self) {
		_image = image;
		// Set the expiration time to 'never' if it comes in as zero, otherwise just add it to the current time
		_timestamp = (age ? age + [[NSDate date] timeIntervalSince1970] : DBL_MAX);
	}
	return self;
}
@end

@interface BCImageCache ()
@property (strong, nonatomic) NSDictionary *stockImages;
@end

@implementation BCImageCache

+ (BCImageCache *)sharedCache
{
	static dispatch_once_t once;
	static BCImageCache *sharedInstance = nil;

	dispatch_once(&once, ^{
		sharedInstance = [[BCImageCache alloc] init];
	});

	return sharedInstance;
}

- (id)init
{
	self = [super init];
    if (self) {
		// Set up the stock images, we'll store the names, as iOS already does its own caching on image resources
		// grayscale images are a bit of a hack, but they're always size medium, and we treat grayscale like a size
		_stockImages = @{
			kItemTypeRecipe: @{
				kImageSizeSmall: kStockImageRecipe,
				kImageSizeMedium: kStockImageRecipe,
				@"grayscale": kStockImageRecipeGrayscale,
				kImageSizeLarge: kStockImageRecipeLarge
			},
			kItemTypeProduct: @{
				kImageSizeSmall: kStockImageProduct,
				kImageSizeMedium: kStockImageProduct,
				@"grayscale": kStockImageProductGrayscale,
				kImageSizeLarge: kStockImageProductLarge
			},
			kItemTypeMenu: @{
				kImageSizeSmall: kStockImageMenuItem,
				kImageSizeMedium: kStockImageMenuItem,
				@"grayscale": kStockImageMenuItemGrayscale,
				kImageSizeLarge: kStockImageMenuItemLarge
			},
			kItemTypeCalories: @{
				kImageSizeSmall: kStockImageJustCalories,
				kImageSizeMedium: kStockImageJustCalories,
				@"grayscale": [NSNull null],
				kImageSizeLarge: kStockImageJustCaloriesLarge
			},
			kItemTypeUserFemale: @{
				kImageSizeSmall: kStockImageUserFemale,
				kImageSizeMedium: kStockImageUserFemale,
				@"grayscale": [NSNull null],
				kImageSizeLarge: kStockImageUserFemale
			},
			kItemTypeUserMale: @{
				kImageSizeSmall: kStockImageUserMale,
				kImageSizeMedium: kStockImageUserMale,
				@"grayscale": [NSNull null],
				kImageSizeLarge: kStockImageUserMale
			},
			kItemTypeProfileFemale: @{
				kImageSizeSmall: kStockImageProfileFemale,
				kImageSizeMedium: kStockImageProfileFemale,
				@"grayscale": [NSNull null],
				kImageSizeLarge: kStockImageProfileFemale
			},
			kItemTypeProfileMale: @{
				kImageSizeSmall: kStockImageProfileMale,
				kImageSizeMedium: kStockImageProfileMale,
				@"grayscale": [NSNull null],
				kImageSizeLarge: kStockImageProfileMale
			},
		};
	}

    return self;
}

#pragma mark - key generator methods
+ (NSString *)imageKeyForImageId:(NSNumber *)imageId withSize:(NSString *)size
{
	return [NSString stringWithFormat:@"i.%@.%@", imageId, size];
}

+ (NSString *)imageKeyForProductId:(NSNumber *)productId withSize:(NSString *)size
{
	return [NSString stringWithFormat:@"p.%@.%@", productId, size];
}

+ (NSString *)imageKeyForLoginId:(NSNumber *)loginId inCommunity:(NSNumber *)communityId withSize:(NSString *)size
{
	return [NSString stringWithFormat:@"l.%@.%@.%@", loginId, communityId, size];
}

+ (NSString *)imageKeyForCoachId:(NSNumber *)coachId withSize:(NSString *)size
{
	return [NSString stringWithFormat:@"c.%@.%@", coachId, size];
}

+ (NSString *)imageKeyForGrayscaleImageId:(NSNumber *)imageId withSize:(NSString *)size
{
	return [NSString stringWithFormat:@"gi.%@.%@", imageId, size];
}

+ (NSString *)imageKeyForGrayscaleProductId:(NSNumber *)productId withSize:(NSString *)size
{
	return [NSString stringWithFormat:@"gp.%@.%@", productId, size];
}

+ (NSString *)imageKeyForImageURL:(NSString *)imageURLString withSize:(NSString *)size
{
	return [NSString stringWithFormat:@"%@.%@", imageURLString, size];
}

#pragma mark - core methods
- (void)setImage:(id)image forKey:key
{
	BCImageCacheEntry *entry = [[BCImageCacheEntry alloc] initWithImage:image withAge:kAGE_OUT_IN_SECONDS];
	[self setObject:entry forKey:key];
	DDLogImageCache(@"setImage:%@ forKey:%@", image, key);
}

- (id)imageForKey:(NSString *)key
{
	id image = nil;

	// retrieve the entry by key
	BCImageCacheEntry *entry = [self objectForKey:key];

	if (entry) {
		DDLogImageCache(@"imageForKey:%@ - Found! %@", key, entry.image);
		// check to make sure it's within the last 6 hours
		NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
		if (now > entry.timestamp) {
			[self removeObjectForKey:key];
		}
		else {
			image = entry.image;
		}

		// make sure it's actually an image
		//if (![image isKindOfClass:[UIImage class]]) {
		//	image = nil;
		//}
	}
	else {
		DDLogImageCache(@"imageForKey:%@ - Not Found!", key);
	}

	return image;
}

/*
 * Stock image handling
 */
#pragma mark - stock image methods
- (id)stockImageWithItemType:(NSString *)itemType withSize:(NSString *)size
{
	NSString *imageName = [self.stockImages valueForKeyPath:[NSString stringWithFormat:@"%@.%@", itemType, size]];

	UIImage *image = nil;

	if (imageName) {
		image = [UIImage imageNamed:imageName];
	}

	return image;
}

/*
 * by image id
 */
#pragma mark - imageId based methods
- (id)imageWithImageId:(NSNumber *)imageId
{
	return [self imageWithImageId:imageId size:kImageSizeSmall];
}

- (id)imageWithImageId:(NSNumber *)imageId size:(NSString *)size
{
	return [self imageForKey:[BCImageCache imageKeyForImageId:imageId withSize:size]];
}

// This method will return a stock image if the cache doesn't have an image for this imageId, so DO NOT
// call it if you want to know whether to load an image!!! Use imageWithImageId:size: instead!
- (id)imageWithImageId:(NSNumber *)imageId withItemType:(NSString *)itemType withSize:(NSString *)size
{
	UIImage *image = nil;

	if (imageId && ([imageId integerValue] > 0)) {
		image = [self imageWithImageId:imageId size:size];
	}

	if (!image || ((id)image == [NSNull null])) {
		// Return the proper stock image
		image = [self stockImageWithItemType:itemType withSize:size];
	}

	return image;
}

- (id)userImageWithImageId:(NSNumber *)imageId withGender:(NSNumber *)gender withSize:(NSString *)size
{
	NSString *itemType = ([gender integerValue] == kGenderFemale ? kItemTypeUserFemale : kItemTypeUserMale);

	return [self imageWithImageId:imageId withItemType:itemType withSize:size];
}

- (id)userImageWithLoginId:(NSNumber *)loginId forCommunity:(NSNumber *)communityId withGender:(NSNumber *)gender withSize:(NSString *)size
{
	UIImage *image = nil;

	if (loginId && ([loginId integerValue] > 0)) {
		image = [self imageForKey:[BCImageCache imageKeyForLoginId:loginId inCommunity:communityId withSize:size]];
	}

	if (!image || ((id)image == [NSNull null])) {
		// Return the proper stock image
		NSString *itemType = ([gender integerValue] == kGenderFemale ? kItemTypeUserFemale : kItemTypeUserMale);
		image = [self stockImageWithItemType:itemType withSize:size];
	}

	return image;
}

- (void)setUserImage:(UIImage *)image withLoginId:(NSNumber *)loginId forCommunity:(NSNumber *)communityId withSize:(NSString *)size
{
	if (image && loginId && communityId && size) {
		[self setImage:image forKey:[BCImageCache imageKeyForLoginId:loginId inCommunity:communityId withSize:size]];
	}
}

// This method will return a stock image if the cache doesn't have an image for this imageId, so DO NOT
// call it if you want to know whether to load an image!!! Use imageWithImageId:size: instead!
- (id)profileImageWithImageId:(NSNumber *)imageId withGender:(NSNumber *)gender withSize:(NSString *)size
{
	NSString *itemType = ([gender integerValue] == kGenderFemale ? kItemTypeProfileFemale : kItemTypeProfileMale);

	return [self imageWithImageId:imageId withItemType:itemType withSize:size];
}

- (id)grayscaleImageWithImageId:(NSNumber *)imageId
{
	return [self imageForKey:[BCImageCache imageKeyForGrayscaleImageId:imageId withSize:kImageSizeSmall]];
}

- (id)grayscaleImageWithImageId:(NSNumber *)imageId withItemType:(NSString *)itemType
{
	UIImage *image = nil;

	if (imageId && ([imageId integerValue] > 0)) {
		// First, check for a cached grayscale image
		image = [self grayscaleImageWithImageId:imageId];

		if (!image) {
			// Check for a cached regular image, size medium, as all grayscales are medium
			image = [self imageWithImageId:imageId size:kImageSizeMedium];

			if (image) {
				if ((id)image != [NSNull null]) {
					// We found a regular image in the cache, convert to grayscale, set in cache, and return it
					image = [image BC_imageGrayscale];
				}

				// This may be setting in [NSNull null], depending on what the imagecache return, which is okay,
				// it would mean that there is no image for this, or that it isn't done being loaded
				[self setGrayscaleImage:image forImageId:imageId];
			}
		}
	}

	if (!image || ((id)image == [NSNull null])) {
		// Return the proper stock image
		image = [self stockImageWithItemType:itemType withSize:@"grayscale"];
	}

	return image;
}

- (void)setImage:(id)image forImageId:(NSNumber *)imageId
{
	[self setImage:image forImageId:imageId size:@"small"];
}

- (void)setImage:(id)image forImageId:(NSNumber *)imageId size:(NSString *)size
{
	if (image && imageId && size) {
		[self setImage:image forKey:[BCImageCache imageKeyForImageId:imageId withSize:size]];
	}
}

- (void)setGrayscaleImage:(id)image forImageId:(NSNumber *)imageId
{
	if (image && imageId) {
		[self setImage:image forKey:[BCImageCache imageKeyForGrayscaleImageId:imageId withSize:kImageSizeSmall]];
	}
}

- (void)removeImageWithImageId:(NSNumber *)imageId size:(NSString *)size
{
	DDLogImageCache(@"removeImageWithImageId:%@ size:%@", imageId, size);
	[self removeObjectForKey:[BCImageCache imageKeyForImageId:imageId withSize:size]];
}

/*
 * by product id
 */
#pragma mark - productId based methods
- (id)imageWithProductId:(NSNumber *)productId
{
	return [self imageWithProductId:productId size:@"small"];
}

- (void)setImage:(id)image forProductId:(NSNumber *)productId
{
	[self setImage:image forProductId:productId size:@"small"];
}

- (id)imageWithProductId:(NSNumber *)productId size:(NSString *)size
{
	id image = [self imageForKey:[BCImageCache imageKeyForProductId:productId withSize:size]];
	return (image && [image isKindOfClass:[UIImage class]] ? image : nil);
}

- (void)setImage:(id)image forProductId:(NSNumber *)productId size:(NSString *)size
{
	if (image && productId && size) {
		[self setImage:image forKey:[BCImageCache imageKeyForProductId:productId withSize:size]];
	}
}

- (id)grayscaleImageWithProductId:(NSNumber *)productId
{
	id image = [self imageForKey:[BCImageCache imageKeyForGrayscaleProductId:productId withSize:kImageSizeSmall]];
	return (image && [image isKindOfClass:[UIImage class]] ? image : nil);
}

- (void)setGrayscaleImage:(id)image forProductId:(NSNumber *)productId
{
	if (image && productId) {
		[self setImage:image forKey:[BCImageCache imageKeyForGrayscaleProductId:productId withSize:kImageSizeSmall]];
	}
}

/*
 * by coach_id (advisor_id)
 */
- (id)imageWithCoachId:(NSNumber *)coachId size:(NSString *)size;
{
	return [self imageForKey:[BCImageCache imageKeyForCoachId:coachId withSize:size]];
}

- (void)setImage:(id)image forCoachId:(NSNumber *)coachId size:(NSString *)size;
{
	if (image && coachId && size) {
		[self setImage:image forKey:[BCImageCache imageKeyForCoachId:coachId withSize:size]];
	}
}

/*
 * by image URL
 */
#pragma mark - URL based methods
- (id)imageWithImageURL:(NSString *)imageURL
{
	id image = [self imageForKey:imageURL];
	return (image && [image isKindOfClass:[UIImage class]] ? image : nil);
}

- (void)setImage:(id)image forImageURL:(NSString *)imageURL
{
	if (image && imageURL) {
		[self setImage:image forKey:imageURL];
	}
}

- (id)imageWithImageURL:(NSString *)imageURL size:(NSString *)size
{
	id image = [self imageForKey:[BCImageCache imageKeyForImageURL:imageURL withSize:size]];
	return (image && [image isKindOfClass:[UIImage class]] ? image : nil);
}

- (void)setImage:(id)image forImageURL:(NSString *)imageURL size:(NSString *)size
{
	if (image && imageURL && size) {
		[self setImage:image forKey:[BCImageCache imageKeyForImageURL:imageURL withSize:size]];
	}
}

@end
