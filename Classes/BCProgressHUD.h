//
//  BCProgressHUD.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 12/22/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "MBProgressHUD.h"

@interface BCProgressHUD : MBProgressHUD

+ (BCProgressHUD *)currentHUD;

+ (UIView *)notificationWithText:(NSString *)text onView:(UIView *)view; ///< Convenience method, should probably always use this
+ (UIView *)BC_showHUDWithText:(NSString *)text onView:(UIView *)view animated:(BOOL)animated duration:(NSTimeInterval)duration;

+ (BCProgressHUD *)showHUDAddedTo:(UIView *)view graceTime:(float)inGraceTime animated:(BOOL)animated;
+ (BCProgressHUD *)showHUDAddedTo:(UIView *)view animated:(BOOL)animated;
+ (BCProgressHUD *)showHUDWithText:(NSString *)text onView:(UIView *)view animated:(BOOL)animated;

+ (void)hideHUD:(BOOL)animated;

- (void)resetData;

- (void)hide:(BOOL)animated;

@end
