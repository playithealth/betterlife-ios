//
//  BCUserProfileViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "BCUserProfileViewController.h"

#import "BCSegmentedControlCell.h"
#import "BCObjectManager.h"
#import "BCTextFieldCell.h"
#import "GenericValueDisplay.h"
#import "NSArray+NestedArrays.h"
#import "NumberValue.h"
#import "OAuthConsumer.h"
#import "UIColor+Additions.m"
#import "User.h"

@interface AccountCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;
@end
@implementation AccountCell
@end

@interface BCUserProfileViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet AccountCell *nameCell;
@property (weak, nonatomic) IBOutlet AccountCell *emailCell;
@property (weak, nonatomic) IBOutlet AccountCell *zipCell;

@property (strong, nonatomic) UITextField *editingTextField;
@property (assign, nonatomic) BOOL needsSync;
@end

@implementation BCUserProfileViewController

enum { RowName, RowEmail, RowZipcode };

#pragma mark - View lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		self.needsSync = NO;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.userProfile = [User currentUser].userProfile;

	self.needsSync = NO;
	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

	[self updateView];

	[[NSNotificationCenter defaultCenter] addObserverForName:[UserProfileMO entityName] object:nil queue:nil
		usingBlock:^void(NSNotification *notification) {
			[self updateView];
			[self.tableView reloadData];
		}];

	[BCObjectManager syncCheck:SendAndReceive];
}

- (void)viewWillDisappear:(BOOL)animated
{
	if (self.editingTextField != nil) {
		[self saveTextFieldValue:self.editingTextField];
		self.editingTextField = nil;
	}

	if (self.needsSync) {
		self.userProfile.status = kStatusPut;
		[self.userProfile.managedObjectContext BL_save];
		[BCObjectManager syncCheck:SendOnly];
		self.needsSync = NO;
	}

    [super viewWillDisappear:animated];
}

#pragma mark - BCTableView overrides
- (void)updateView
{
	self.nameCell.valueTextField.text = self.userProfile.name;
	self.emailCell.valueTextField.text = self.userProfile.email;
	self.zipCell.valueTextField.text = self.userProfile.zipCode;
}

#pragma mark - local methods
- (void)saveTextFieldValue:(UITextField *)textField
{
	// The text field's tag is the same as its row number!!!
	switch (textField.tag) {
		case RowName:
			self.userProfile.name = textField.text;
			break;
		case RowEmail:
			self.userProfile.email = textField.text;
			break;
		case RowZipcode:
			self.userProfile.zipCode = textField.text;
			break;
	}
	
	self.needsSync = YES;
}

- (void)syncCheckDidFinish:(NSNotification *)notification
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:@"syncCheck" object:nil];

	// just reload the table
	[self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	switch (indexPath.row) {
		case RowName:
			[self.nameCell.valueTextField becomeFirstResponder];
			break;
		case RowEmail:
			[self.emailCell.valueTextField becomeFirstResponder];
			break;
		case RowZipcode:
			[self.zipCell.valueTextField becomeFirstResponder];
			break;
	}
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
	self.editingTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[self saveTextFieldValue:textField];

	self.editingTextField = nil;
}

@end

