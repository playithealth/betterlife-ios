//
//  MealShoppingListItemsViewController.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 5/22/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "MealShoppingListItemsViewController.h"

#import "CategoryMO.h"
#import "IngredientShoppingListItemCell.h"
#import "MealIngredient.h"
#import "NumberValue.h"
#import "ProductDetailViewController.h"
#import "ShoppingListSearchViewController.h"
#import "ShoppingListItem.h"

@interface MealShoppingListItemsViewController () <BCSearchViewDelegate>
{
	BOOL changedMeal;
}

@property (strong, nonatomic) NSArray *ingredients;
@property (strong, nonatomic) NSIndexPath *editingIndexPath;

@end

@implementation MealShoppingListItemsViewController

const NSInteger kMSLITagAddToSLAsk = 10002;

const NSInteger kMSLIRowIngredient = 0;
const NSInteger kMSLIRowAddToShoppingList = 1;
const NSInteger kMSLIRowShoppingListItem = 2;

const NSInteger kMSLISegmentNever = 0;

#pragma mark - view lifecycle
- (void)viewDidLoad
{
	//self.useSidebarButton = YES;

	self.ingredients = [self.recipe.ingredients sortedArrayUsingDescriptors:
		[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES]]];

    [super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewWillDisappear:(BOOL)animated
{
	if (changedMeal) {
		// TODO - maybe add a notification and/or delegate call to force a sync?
		self.recipe.status = kStatusPut;
	}
	changedMeal = NO;

    [super viewWillDisappear:animated];
}

#pragma mark - local methods
- (void)addToShoppingListValueChanged:(id)sender
{
	UISegmentedControl *addToSL = sender;

	// Get the cell in which the sender is embedded
	id currentCell = sender;
	while (currentCell && ![currentCell isKindOfClass:[UITableViewCell class]]) {
		currentCell = [currentCell superview];
	}

	// Figure out which ingredient was clicked on
	NSIndexPath *indexPath = [self.tableView indexPathForCell:currentCell];
	MealIngredient *ingredient = [self.ingredients objectAtIndex:[indexPath section]];

	ingredient.addToShoppingList = [NSNumber numberWithInteger:[addToSL selectedSegmentIndex]];
	NSError *error;
	if (![ingredient.managedObjectContext save:&error]) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	[self.tableView reloadData];

	changedMeal = YES;
}

- (void)accessoryTapped:(UITapGestureRecognizer *)recognizer
{
	// Get the cell in which the recognizer is embedded
	id currentCell = [recognizer view];
	while (currentCell && ![currentCell isKindOfClass:[UITableViewCell class]]) {
		currentCell = [currentCell superview];
	}
	NSIndexPath *indexPath = [self.tableView indexPathForCell:currentCell];
	MealIngredient *ingredient = [self.ingredients objectAtIndex:[indexPath section]];

	// Show the details of this shopping list item (product)
	ProductDetailViewController *productView = [[ProductDetailViewController alloc]
		initWithNibName:nil bundle:nil];
	ProductDetailItem *item = [[ProductDetailItem alloc] init];
	item.name = ingredient.shoppingListText;
	item.productId = ingredient.productId;
	productView.productDetailItem = item;
	productView.managedObjectContext = ingredient.managedObjectContext;

	[self.navigationController pushViewController:productView animated:YES];

}

- (void)incrementTapped:(UITapGestureRecognizer *)recognizer
{
	// Get the cell in which the recognizer is embedded
	id currentCell = [recognizer view];
	while (currentCell && ![currentCell isKindOfClass:[UITableViewCell class]]) {
		currentCell = [currentCell superview];
	}
	NSIndexPath *indexPath = [self.tableView indexPathForCell:currentCell];
	MealIngredient *ingredient = [self.ingredients objectAtIndex:[indexPath section]];

	ingredient.shoppingListQuantity = [NSNumber numberWithInteger:
		[ingredient.shoppingListQuantity doubleValue] + 1];

	[self configureCell:currentCell atIndexPath:indexPath];

	changedMeal = YES;
}

- (void)decrementTapped:(UITapGestureRecognizer *)recognizer
{
	// Get the cell in which the recognizer is embedded
	id currentCell = [recognizer view];
	while (currentCell && ![currentCell isKindOfClass:[UITableViewCell class]]) {
		currentCell = [currentCell superview];
	}
	NSIndexPath *indexPath = [self.tableView indexPathForCell:currentCell];
	MealIngredient *ingredient = [self.ingredients objectAtIndex:[indexPath section]];

	if ([ingredient.shoppingListQuantity doubleValue] > 1) {
		ingredient.shoppingListQuantity = [NSNumber numberWithInteger:
			[ingredient.shoppingListQuantity doubleValue] - 1];

		[self configureCell:currentCell atIndexPath:indexPath];

		changedMeal = YES;
	}
}

- (void)resetShoppingListProductTapped:(UITapGestureRecognizer *)recognizer
{
	// Get the cell in which the recognizer is embedded
	id currentCell = [recognizer view];
	while (currentCell && ![currentCell isKindOfClass:[UITableViewCell class]]) {
		currentCell = [currentCell superview];
	}
	NSIndexPath *indexPath = [self.tableView indexPathForCell:currentCell];
	MealIngredient *ingredient = [self.ingredients objectAtIndex:[indexPath section]];

	// If this is not a 'real' product, ignore this tap
	if (!ingredient.productId || ![ingredient.productId integerValue]) {
		return;
	}

	ingredient.productId = [NSNumber numberWithInteger:0];
	ingredient.shoppingListText = ingredient.name;
	// Also clear the category for the shopping list item, as the category we have may not be
	// accurate any longer
	ingredient.shoppingListCategory = nil;

	[self configureCell:currentCell atIndexPath:indexPath];

	changedMeal = YES;
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [self.ingredients count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rows = 2;

	MealIngredient *ingredient = [self.ingredients objectAtIndex:section];

	if ([ingredient.addToShoppingList integerValue] != kMSLISegmentNever) {
		rows = 3;
	}
	else {
		rows = 2;
	}

	return rows;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView
cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *ingredientCellId = @"IngredientCell";
	static NSString *addToShoppingListCellId = @"AddToShoppingListCell";
	static NSString *shoppingListItemCellId = @"ShoppingListItemCell";

	UITableViewCell *cell = nil;
	if (indexPath.row == kMSLIRowIngredient) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:ingredientCellId];
	}
	else if (indexPath.row == kMSLIRowAddToShoppingList) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:addToShoppingListCellId];

		UISegmentedControl *addToSL = (UISegmentedControl *)[cell viewWithTag:kMSLITagAddToSLAsk];
		[addToSL addTarget:self action:@selector(addToShoppingListValueChanged:) forControlEvents:UIControlEventValueChanged];
	}
	else if (indexPath.row == kMSLIRowShoppingListItem) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:shoppingListItemCellId];
		IngredientShoppingListItemCell *sliCell = (IngredientShoppingListItemCell*)cell;

		UITapGestureRecognizer *recognizer = nil;

		recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(incrementTapped:)];
		[sliCell.incrementButton addGestureRecognizer:recognizer];

		recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(decrementTapped:)];
		[sliCell.decrementButton addGestureRecognizer:recognizer];
	}
	else {
		DDLogError(@"WTF indexPath:%@", indexPath);
	}

	[self configureCell:cell atIndexPath:indexPath];

    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	MealIngredient *ingredient = [self.ingredients objectAtIndex:[indexPath section]];

	if ([indexPath row] == kMSLIRowIngredient) {
		cell.textLabel.text = ingredient.name;
	}
	else if ([indexPath row] == kMSLIRowAddToShoppingList) {
		UISegmentedControl *addToSL = (UISegmentedControl *)[cell viewWithTag:kMSLITagAddToSLAsk];
		if (ingredient.addToShoppingList) {
			[addToSL setSelectedSegmentIndex:[ingredient.addToShoppingList integerValue]];
		}
		else {
			[addToSL setSelectedSegmentIndex:kMSLISegmentNever];
		}
	}
	else {
		IngredientShoppingListItemCell *sliCell = (IngredientShoppingListItemCell*)cell;
		if (!ingredient.shoppingListText || ![ingredient.shoppingListText length]) {
			ingredient.shoppingListText = ingredient.name;
		}
		cell.textLabel.text = ingredient.shoppingListText;

		if (!ingredient.shoppingListQuantity || ![ingredient.shoppingListQuantity doubleValue]) {
			[sliCell.quantityButton setTitle:@"1" forState:UIControlStateNormal];
		}
		else {
			[sliCell.quantityButton setTitle:[ingredient.shoppingListQuantity stringValue] forState:UIControlStateNormal];
		}
	}
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)aTableView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 0;

	if ([indexPath row] == kMSLIRowAddToShoppingList) {
		height = 106;
	}
	else {
		height = 50;
	}

	return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == kMSLIRowShoppingListItem) {
		self.editingIndexPath = indexPath;

		ShoppingListSearchViewController *searchView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingListSearch"];
		searchView.managedObjectContext = self.recipe.managedObjectContext;
		searchView.delegate = self;
		[self.navigationController pushViewController:searchView animated:YES];
	}
}

#pragma mark - BCSearchViewDelegate
- (void)searchView:(id)searchView didFinish:(BOOL)finished withSelection:(id)selection
{
	if (selection) {
		ShoppingListItem *slItem = selection;

		MealIngredient *ingredient = [self.ingredients objectAtIndex:self.editingIndexPath.section];
		ingredient.shoppingListText = slItem.name;
		ingredient.shoppingListCategory = (CategoryMO*)[self.recipe.managedObjectContext objectWithID:[slItem.category objectID]];
		ingredient.productId = slItem.productId;

		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.editingIndexPath];
		[self configureCell:cell atIndexPath:self.editingIndexPath];

		changedMeal = YES;
	}

	self.editingIndexPath = nil;

	[self.navigationController popViewControllerAnimated:YES];
}

@end
