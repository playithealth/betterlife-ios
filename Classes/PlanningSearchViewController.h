//
//  PlanningSearchViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 2/19/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCSearchViewController.h"

@interface PlanningSearchViewController : BCSearchViewController

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSNumber *logTime;

@end
