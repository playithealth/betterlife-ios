//
//  BLConversationListViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 5/20/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCTableViewController.h"

@interface BLConversationListViewController : BCTableViewController

@end
