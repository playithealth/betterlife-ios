//
//  BCCalendarView.m
//  BettrLife
//
//  Created by Sef Tarbell on 8/23/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCCalendarView.h"

#import <QuartzCore/QuartzCore.h>

#import "NSCalendar+Additions.h"
#import "NSDate+Additions.h"
#import "NSArray+NestedArrays.h"
#import "UIColor+Additions.h"

@interface BCCalendarView ()

@property (strong, nonatomic) NSDate *baseDate;
@property (strong, nonatomic) NSCalendar *calendar;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSMutableArray *datesByWeek;
@property (strong, nonatomic) NSMutableArray *buttons;
@property (strong, nonatomic) UILabel *headerLabel;

@end

@implementation BCCalendarView

static const CGFloat kButtonHeight = 30.0f;
static const CGFloat kButtonWidth = 40.0f;
static const CGFloat kHeaderHeight = 52.0f;
static const CGFloat kLabelHeight = 24.0f;
static const CGFloat kLabelWidth = kButtonWidth;
static const CGFloat kFrameInset = 6.0f;
static const CGFloat kSpacing = 4.0f;

#pragma mark - Initialization
- (id)initWithBaseDate:(NSDate *)inBaseDate delegate:(id<BCPopupViewDelegate> )delegate
{
	self = [self initWithView:nil caretPosition:CGPointMake(160, 35) title:nil delegate:delegate];
	if (self) {
		self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

		_baseDate = inBaseDate;

		/*
		 * header background layer
		 */
		CAShapeLayer *headerBackgroundLayer = [CAShapeLayer layer];
		headerBackgroundLayer.frame = CGRectMake(self.innerFrame.origin.x, self.innerFrame.origin.y, self.innerFrame.size.width, kHeaderHeight);
		headerBackgroundLayer.backgroundColor = [UIColor BC_lightGreenColor].CGColor;
		// Create the top rounded corner shape layer
		CGRect topCornersMaskFrame = CGRectMake(0,0,headerBackgroundLayer.frame.size.width, headerBackgroundLayer.frame.size.height);
		UIBezierPath *topCornersMaskPath = [UIBezierPath bezierPathWithRoundedRect:topCornersMaskFrame byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(3, 3)];
		CAShapeLayer *topCornersMaskLayer = [CAShapeLayer layer];
		topCornersMaskLayer.frame = topCornersMaskFrame;
		topCornersMaskLayer.path = topCornersMaskPath.CGPath;
		headerBackgroundLayer.mask = topCornersMaskLayer;
		[self.layer addSublayer:headerBackgroundLayer];
		
		/*
		 * gradient layer
		 */
//		CAGradientLayer *gradientLayer = [CAGradientLayer layer];
//		gradientLayer.frame = CGRectMake(self.innerFrame.origin.x, self.innerFrame.origin.y, self.innerFrame.size.width, kHeaderHeight);
//		gradientLayer.colors = @[
//			(id)[UIColor BC_lightGreenColor].CGColor,
//			(id)[UIColor BC_darkGreenColor].CGColor];
//		[self.layer addSublayer:gradientLayer];
		
		/*
		 * header label
		 */
		CGFloat left = self.innerFrame.origin.x + kFrameInset;
		CGFloat top = self.innerFrame.origin.y;
		self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(left + kSpacing, top + kSpacing, self.innerFrame.size.width - kSpacing * 2, kLabelHeight)];
		self.headerLabel.textAlignment = NSTextAlignmentCenter;
		self.headerLabel.font = [UIFont systemFontOfSize:14];
		self.headerLabel.textColor = [UIColor whiteColor];
		self.headerLabel.backgroundColor = [UIColor clearColor];
		self.headerLabel.text = [self.baseDate BC_monthYearString];
		[self addSubview:self.headerLabel];

		/*
		 * add prev/next buttons
		 */
		UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
		previousButton.frame = CGRectMake(self.innerFrame.origin.x, self.innerFrame.origin.y, kButtonWidth, kButtonHeight);
		[previousButton setImage:[UIImage imageNamed:@"previous"] forState:UIControlStateNormal];
		UITapGestureRecognizer *aRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(previousButtonTapped:)];
		[previousButton addGestureRecognizer:aRecognizer];
		[self addSubview:previousButton];

		UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
		nextButton.frame = CGRectMake(self.innerFrame.origin.x + self.innerFrame.size.width - kButtonWidth, self.innerFrame.origin.y, kButtonWidth, kButtonHeight);
		[nextButton setImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];
		aRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nextButtonTapped:)];
		[nextButton addGestureRecognizer:aRecognizer];
		[self addSubview:nextButton];

		/* 
		 * add headers for the week days
		 */
		self.dateFormatter = [[NSDateFormatter alloc] init];
		top += kLabelHeight;
		for (NSInteger day = BCDateWeekDays.sunday; day <= BCDateWeekDays.saturday; day++) {
			UILabel *aLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top + kSpacing, kLabelWidth, kLabelHeight)];
			aLabel.backgroundColor = [UIColor clearColor];
			aLabel.font = [UIFont systemFontOfSize:18];
			aLabel.textColor = [UIColor whiteColor];
			aLabel.textAlignment = NSTextAlignmentCenter;
			aLabel.text = [[self.dateFormatter veryShortWeekdaySymbols] objectAtIndex:day - 1];
			[self addSubview:aLabel];
			left += kLabelWidth;
		}

		CGRect temp = self.innerFrame;
		self.innerFrame = CGRectMake(temp.origin.x, temp.origin.y, temp.size.width, 208);
		self.innerLayer.frame = self.innerFrame;
		
		temp = self.outerFrame;
		self.outerFrame = CGRectMake(temp.origin.x, temp.origin.y, temp.size.width, 210);
		self.outerLayer.frame = self.outerFrame;
		self.outerLayer.colors = @[(id)[[UIColor BC_mediumDarkGrayColor] CGColor],
			(id)[[UIColor BC_mediumDarkGrayColor] CGColor], (id)[[UIColor BC_mediumDarkGrayColor] CGColor]];
		self.outerLayer.locations = @[@0.0f, @0.5f, @1.0f];
		self.outerLayer.borderColor = [[UIColor BC_mediumDarkGrayColor] CGColor];

		// this should be the last thing called in here
		[self dateChanged];
	}

	return self;
}

#pragma mark - Local
- (NSDate *)firstSundayInGridForDate:(NSDate *)gridDate
{
	NSDate *firstOfMonth = [self.calendar BC_firstOfMonthForDate:gridDate];
	NSDateComponents *components = [self.calendar components:(NSWeekdayCalendarUnit) fromDate:firstOfMonth];
	return [self.calendar BC_dateByAddingDays:BCWeekdaySunday - components.weekday toDate:firstOfMonth];
}

- (NSDate *)lastSaturdayInGridForDate:(NSDate *)gridDate
{
	NSDate *lastOfMonth = [self.calendar BC_lastOfMonthForDate:gridDate];
	NSDateComponents *components = [self.calendar components:(NSWeekdayCalendarUnit) fromDate:lastOfMonth];
	return [self.calendar BC_dateByAddingDays:BCWeekdaySaturday - components.weekday toDate:lastOfMonth];
}

- (NSInteger)weeksInGridForDate:(NSDate *)gridDate
{
	NSDate *first = [self firstSundayInGridForDate:gridDate];
	NSDate *last = [self lastSaturdayInGridForDate:gridDate];
	
	NSInteger daysBetween = [self.calendar BC_daysFromDate:first toDate:last];
	
	return (daysBetween / 7) + 1;
}

- (void)dateChanged
{
	// remove all the buttons
	for (UIButton *button in self.buttons) {
		[button removeFromSuperview];
	}
	self.buttons = [[NSMutableArray alloc] init];

	NSDateComponents *baseDateComponents = [self.calendar components:(NSMonthCalendarUnit) fromDate:self.baseDate];
	
	NSDate *firstSunday = [self firstSundayInGridForDate:self.baseDate];
	NSInteger weeksInGrid = [self weeksInGridForDate:self.baseDate];
	
	CGFloat top = self.innerFrame.origin.y + kHeaderHeight;
	CGRect temp = self.innerFrame;
	self.innerFrame = CGRectMake(temp.origin.x, temp.origin.y, temp.size.width, kButtonHeight * weeksInGrid + 50);
	self.innerLayer.cornerRadius = 3;
	self.innerLayer.frame = self.innerFrame;
	
	temp = self.outerFrame;
	self.outerFrame = CGRectMake(temp.origin.x, temp.origin.y, temp.size.width, self.innerFrame.size.height + 1 * 2);
	self.outerLayer.frame = self.outerFrame;

	for (NSInteger weekIndex = 0; weekIndex < weeksInGrid; weekIndex++) {
		CGFloat left = self.innerFrame.origin.x + kFrameInset;

		for (NSInteger dayIndex = 0; dayIndex < 7; dayIndex++) {
			NSDate *thisDay = [self.calendar BC_dateByAddingDays:(weekIndex * 7) + dayIndex toDate:firstSunday];
			
			NSDateComponents *thisDayComponents = [self.calendar components:(NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:thisDay];
			NSString *title = [NSString stringWithFormat:@"%ld", (long)thisDayComponents.day];
			
			UIColor *titleColor = [UIColor blackColor];
			if ([thisDay BC_isSameDayAs:[NSDate date]]) {
				titleColor = [UIColor BC_blueColor];
			}
			else if (thisDayComponents.month == baseDateComponents.month) {
				titleColor = [UIColor BC_darkGrayColor];
			}
			else if (thisDayComponents.month < baseDateComponents.month) {
				titleColor = [UIColor BC_mediumLightGrayColor];
			}
			else if (thisDayComponents.month > baseDateComponents.month) {
				titleColor = [UIColor BC_lightGreenColor];
			}
			
			UIButton *aButton = [[UIButton alloc] initWithFrame:CGRectMake(left, top, kButtonWidth, kButtonHeight)];
			[aButton setTitle:title forState:UIControlStateNormal];
			[aButton setTitleColor:titleColor forState:UIControlStateNormal];
			[aButton setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
			[aButton setBackgroundImage:[UIImage imageNamed:@"calendar-button"] forState:UIControlStateNormal];
			[aButton setBackgroundImage:[UIImage imageNamed:@"calendar-button-pressed"] forState:UIControlStateHighlighted];

			UITapGestureRecognizer *dateButtonRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateButtonTapped:)];
			[aButton addGestureRecognizer:dateButtonRecognizer];
			
			[self.buttons addObject:aButton];
			[self addSubview:aButton];

			left += kButtonWidth;
		}

		top += kButtonHeight;
	}

	self.headerLabel.text = [self.baseDate BC_monthYearString];
}

- (void)dateButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self];
	
	CGFloat top = self.innerFrame.origin.y + kLabelHeight * 2 + kSpacing;
	CGFloat left = self.innerFrame.origin.x + kFrameInset;
	NSInteger weekday = (location.x - left) / kButtonWidth;
	NSInteger week = (location.y - top) / kButtonHeight;
	
	NSDate *selectedDate = [self.calendar BC_dateByAddingDays:(week * 7) + weekday toDate:[self firstSundayInGridForDate:self.baseDate]];
	
	if (self.delegate
			&& [self.delegate conformsToProtocol:@protocol(BCPopupViewDelegate)]) {
		[self.delegate popupView:self didDismissWithInfo:@{@"selectedDate":selectedDate}];
	}
	[self removeFromSuperview];
}

- (void)previousButtonTapped:(UITapGestureRecognizer *)recognizer
{
	self.baseDate = [self.calendar BC_dateByAddingMonths:-1 toDate:self.baseDate];
	[self dateChanged];
}

- (void)nextButtonTapped:(UITapGestureRecognizer *)recognizer
{
	self.baseDate = [self.calendar BC_dateByAddingMonths:1 toDate:self.baseDate];
	[self dateChanged];
}

- (void)setBaseDate:(NSDate *)baseDate
{
	_baseDate = baseDate;

	// this seems inefficient... 
	[self dateChanged];
}

- (void)setSelectedWeek:(NSDate *)selectedWeek
{
	_selectedDay = nil;
	_selectedWeek = selectedWeek;

	// this seems inefficient... 
	[self dateChanged];
}

- (void)setSelectedDay:(NSDate *)selectedDay
{
	_selectedDay = selectedDay;
	_selectedWeek = nil;

	// this seems inefficient... 
	[self dateChanged];
}

@end
