//
//  ShoppingListItemDetailCell.h
//  BettrLife
//
//  Created by Sef Tarbell on 7/25/2012
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StarRatingControl;

@interface ShoppingListItemDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) StarRatingControl *starRatingControl;
@property (weak, nonatomic) IBOutlet UIImageView *healthImageView;
@property (weak, nonatomic) IBOutlet UIImageView *alertImageView;

@end
