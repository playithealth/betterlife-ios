//
//  AboutViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/11/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCViewController.h"

@interface AboutViewController : BCViewController

@end
