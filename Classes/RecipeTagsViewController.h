//
//  RecipeTagsViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/7/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RecipeMO;

@protocol RecipeTagsViewDelegate;

@interface RecipeTagsViewController : UIViewController

@property (strong, nonatomic) RecipeMO *recipe;
@property (unsafe_unretained, nonatomic) id<RecipeTagsViewDelegate> delegate;

@end

@protocol RecipeTagsViewDelegate <NSObject>
@required
- (void)recipeTagsView:(RecipeTagsViewController *)recipeTagsView changedTags:(BOOL)done;
@end
