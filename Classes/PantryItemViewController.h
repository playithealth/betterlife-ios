//
//  PantryItemViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 5/20/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCCategorySelectViewController.h"
#import "PantryItem.h"
#import "ProductSearchView.h"

@class PantryItemViewController;

@protocol PantryItemViewDelegate <NSObject>
@required
- (void)pantryItemView:(PantryItemViewController *)pantryItemView didUpdateItem:(PantryItem *)item;
@end

@interface PantryItemViewController : UITableViewController <BCCategorySelectDelegate, ProductSearchViewDelegate, UITextFieldDelegate> 

@property (strong, nonatomic) PantryItem *pantryItem;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) id<PantryItemViewDelegate> delegate;

@end
