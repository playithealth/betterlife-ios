//
//  MealIngredientEditCell.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 9/15/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "MealIngredientEditCell.h"

@interface MealIngredientEditCell()
@end

@implementation MealIngredientEditCell
@synthesize quantity;
@synthesize name;
@synthesize units;

#pragma mark - life cycle
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - local methods

@end
