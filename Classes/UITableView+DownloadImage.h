//
//  UITableView+DownloadImage.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 12/13/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BC_DownloadImageBlock)(NSError *, UIImage *);

@interface UITableView (DownloadImage)

- (void)downloadImageWithLoginId:(NSNumber *)loginId inCommunity:(NSNumber *)communityId forIndexPath:(NSIndexPath *)indexPath
	withSize:(NSString *)size;
- (void)downloadImageWithImageId:(NSNumber *)imageId forIndexPath:(NSIndexPath *)indexPath withSize:(NSString *)size;
- (void)downloadImageWithCoachId:(NSNumber *)coachId forIndexPath:(NSIndexPath *)indexPath withSize:(NSString *)size;
- (void)downloadImageWithImageURL:(NSString *)imageURL forIndexPath:(NSIndexPath *)indexPath completionBlock:(BC_DownloadImageBlock)completionBlock;

- (void)cancelAllImageFetchers;
- (void)clearTrackerIndexPaths;
- (void)cancelAllImageTrackers;

@end
