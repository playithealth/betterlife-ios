//
//  BCStrengthSearchSmartViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/20/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCStrengthSearchSmartViewController.h"

#import "BCStrengthSearchDataModel.h"
#import "BCProgressHUD.h"
#import "TrainingStrengthMO.h"

@interface BCStrengthSearchSmartViewController ()

@end

@implementation BCStrengthSearchSmartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataModel.trainingStrengthsGroupedByCount.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"NormalCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];

	UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTapped:)];
	[cell.contentView addGestureRecognizer:recognizer];

	TrainingStrengthMO *trainingStrength = self.dataModel.trainingStrengthsGroupedByCount[indexPath.row][@"strength"];
    
    cell.textLabel.text = trainingStrength.name;
	cell.detailTextLabel.text = trainingStrength.setsDescription;
    
    return cell;
}

#pragma mark - responders
- (void)contentViewTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	TrainingStrengthMO *original = self.dataModel.trainingStrengthsGroupedByCount[indexPath.row][@"strength"];

	TrainingStrengthMO *newStrength = [self.dataModel insertStrengthBasedOnTrainingStrength:original];

	[BCProgressHUD notificationWithText:[NSString stringWithFormat:@"%@ added to your log", newStrength.name] 
		onView:self.view.superview];
}

@end
