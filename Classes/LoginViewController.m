//
//  LoginViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "LoginViewController.h"

#import "AccountRegistrationView.h"
#import "BCAppDelegate.h"
#import "BCTextFieldCell.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"
#import "GenericValueDisplay.h"
#import "MBProgressHUD.h"
#import "NSDictionary+NumberValue.h"
#import "NSString+BCAdditions.h"
#import "OAuthConsumer.h"
#import "UIColor+Additions.h"
#import "UIImage+Additions.h"
#import "User.h"
#import "UserPermissionMO.h"
#import "UserProfileMO.h"

static const NSUInteger kTagAlertResetDatabase = 0;
static const NSUInteger kTagAlertCannotSendMail = 1;

// indexes for debug action sheet
static const NSUInteger kSendDatabase = 0;
static const NSUInteger kResetDatabase = 1;
static const NSUInteger kResetAuthentication = 2;
static const NSUInteger kSendLogs = 3;

@interface LoginViewController () <UIActionSheetDelegate, UIAlertViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *debugMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (assign, nonatomic) BOOL consumerOAuthRefreshed; ///< This is a limiter so that we'll only refresh the oauth consumer once per try

- (IBAction)facebookButtonTapped:(id)sender;
- (IBAction)loginButtonTapped:(id)sender;
- (IBAction)viewDoubleTapped:(id)sender;

- (void)fbDidLogin;
- (void)didLoginWithUser:(User *)user;
- (void)promptForResetDatabase;
- (void)sendDatabase;
- (void)sendLogs;
- (void)reachabilityChanged:(NSNotification* )note;
- (IBAction)next:(id)sender;
- (IBAction)resignKeyboard:(id)sender;

@end

@implementation LoginViewController
{
	UIToolbar *keyboardToolbar;
}

#pragma mark - view lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];

	self.navigationController.toolbarHidden = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self 
		selector:@selector(reachabilityChanged:) 
		name:kReachabilityChangedNotification object:nil];
	
	if (!keyboardToolbar) {
		keyboardToolbar = [[UIToolbar alloc] init];
		[keyboardToolbar sizeToFit];
		keyboardToolbar.barStyle = UIBarStyleBlack;
		keyboardToolbar.translucent = YES;

		UIBarButtonItem *previousButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Previous"
			style:UIBarButtonItemStyleBordered
			target:self action:@selector(next:)];

		UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Next"
			style:UIBarButtonItemStyleBordered
			target:self action:@selector(next:)];

		UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
			initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
			target:nil action:nil];

		UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Done" style:UIBarButtonItemStyleDone
			target:self action:@selector(resignKeyboard:)];

		NSArray *buttons = [NSArray arrayWithObjects:
			previousButton, nextButton, flexibleSpace, doneButton, nil];

		keyboardToolbar.items = buttons;
	}

	self.emailTextField.inputAccessoryView = keyboardToolbar;	
	self.passwordTextField.inputAccessoryView = keyboardToolbar;
}

- (void)viewWillAppear:(BOOL)animated
{
	User* thisUser = [User currentUser];
	OAuthConsumer *thisConsumer = [OAuthConsumer currentOAuthConsumer];
	if (![thisConsumer consumerIsValid]) {
		// refresh the oauth consumer cache
		DDLogDebug(@"OAuthConsumer is not valid, refreshing consumer credentials");
		[OAuthConsumer refreshOAuthConsumerWithSuccess:nil
			failure:^(NSError *error) {
				[self showHUDWithLabelText:@"Failed to refresh OAuth consumer!" andCustomView:nil];
			}];
	}

	self.emailTextField.text = thisUser.email;
	self.passwordTextField.text = nil;

	// update the state of the login button to reflect the reachability state
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[self updateInterfaceWithReachability:appDelegate.hostReach];

	[self.navigationController setNavigationBarHidden:YES animated:NO];
	
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[self.navigationController setNavigationBarHidden:NO animated:NO];

	[[NSNotificationCenter defaultCenter] removeObserver:self];

	[super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"CreateAccount"]) {
		AccountRegistrationView *regView = segue.destinationViewController;
		regView.managedObjectContext = self.managedObjectContext;
		regView.delegate = self.delegate;
	}
}

#pragma mark - local methods
- (void)promptForResetDatabase
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset Database"
		message:@"This action cannot be undone"
		delegate:self
		cancelButtonTitle:@"Cancel"
		otherButtonTitles:@"OK", nil];
    alert.tag = kTagAlertResetDatabase;
	[alert show];
}

- (void)sendDatabase
{
	if ([MFMailComposeViewController canSendMail]) {
		User* userData = [User currentUser];
        
		MFMailComposeViewController* mailViewController = [[MFMailComposeViewController alloc] init];
		mailViewController.mailComposeDelegate = self;
		[mailViewController setSubject:@"BettrLife Database"];
		[mailViewController setToRecipients:[NSArray arrayWithObject:@"iphone.issues@bettrlife.com"]];
		[mailViewController setMessageBody:[NSString stringWithFormat:@"%@'s database.", userData.name] isHTML:NO]; 
        
		NSString *fileName = @"BettrLife.sqlite";
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
		NSString *dbPath = [documentsPath stringByAppendingPathComponent:fileName];

		if ([fileManager fileExistsAtPath:dbPath]) {
			NSData *dbFile = [fileManager contentsAtPath:dbPath];
			[mailViewController addAttachmentData:dbFile mimeType:@"sqlite-db" fileName:fileName];
		}
		[self presentViewController:mailViewController animated:YES completion:nil];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to send"
			message:@"Device is unable to send email in its current state."
			delegate:self
			cancelButtonTitle:@"Cancel"
			otherButtonTitles:@"OK", nil];
		alert.tag = kTagAlertCannotSendMail;
		[alert show];
	}
}

- (NSMutableArray *)errorLogData
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	DDFileLogger *fileLogger = appDelegate.fileLogger;

    NSUInteger maximumLogFilesToReturn = MIN(fileLogger.logFileManager.maximumNumberOfLogFiles, 10);
    NSMutableArray *errorLogFiles = [NSMutableArray arrayWithCapacity:maximumLogFilesToReturn];
    NSArray *sortedLogFileInfos = [fileLogger.logFileManager sortedLogFileInfos];
    for (int index = 0; index < MIN(sortedLogFileInfos.count, maximumLogFilesToReturn); index++) {
        DDLogFileInfo *logFileInfo = [sortedLogFileInfos objectAtIndex:index];
        NSData *fileData = [NSData dataWithContentsOfFile:logFileInfo.filePath];
        [errorLogFiles addObject:fileData];
    }
    return errorLogFiles;
}

- (void)sendLogs
{
	if ([MFMailComposeViewController canSendMail]) {
		User* userData = [User currentUser];
        
		MFMailComposeViewController* mailViewController =
        [[MFMailComposeViewController alloc] init];
		mailViewController.mailComposeDelegate = self;
		[mailViewController setSubject:@"BettrLife Log Files"];
		[mailViewController setToRecipients:[NSArray arrayWithObject:@"iphone.issues@bettrlife.com"]];
		[mailViewController setMessageBody:[NSString stringWithFormat:@"%@'s log files.", userData.name] isHTML:NO];

        NSMutableData *errorLogData = [NSMutableData data];
        for (NSData *errorLogFileData in [self errorLogData]) {
            [errorLogData appendData:errorLogFileData];
        }
        [mailViewController addAttachmentData:errorLogData mimeType:@"text/plain" fileName:@"errorLog.txt"];
        
		[self presentViewController:mailViewController animated:YES completion:nil];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to send"
                                                        message:@"Device is unable to send email in its current state."
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK", nil];
		[alert show];
	}
}

- (IBAction)facebookButtonTapped:(id)sender 
{
	NSArray* permissions = [NSArray arrayWithObjects:
		@"email", @"user_about_me", @"publish_stream", nil];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate.facebook authorize:permissions delegate:self];
}

- (IBAction)loginButtonTapped:(id)sender 
{
	[self.view endEditing:YES];

	[self loginWithEmail];
}

- (IBAction)viewDoubleTapped:(id)sender
{
	// NOTE: Removing this for now, as it is a security hole, and it will be reevaluated at a later time
	//[self showDebugActionSheet];
}

- (void)fbDidLogin
{
	[self loginWithFacebook];
}

- (IBAction)showDebugActionSheet
{
	[self.view endEditing:YES];
    
	// present an action sheet with some debug actions
	UIActionSheet *actionSheet = [[UIActionSheet alloc]
		initWithTitle:@"Debug Actions"
		delegate:self
		cancelButtonTitle:@"Cancel"
		destructiveButtonTitle:nil
		otherButtonTitles:
			@"Send database", 
			@"Reset database", 
			@"Reset authentication", 
            @"Send Logs",
			nil];
	[actionSheet showInView:self.view];
}

- (void)successfulLoginWithData:(NSData *)retrievedData
{
	NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

	// grab the loginId, accountId, email, zip, etc out of the response
	User *thisUser = [User currentUser];
	if ([resultsDict objectForKey:kLoginUserId] != nil) {
		thisUser.loginId = [resultsDict BC_numberOrNilForKey:kLoginUserId];
		thisUser.email = self.emailTextField.text;
		thisUser.oauthAccessToken = [resultsDict objectForKey:kLoginOAuthAccessToken];
		thisUser.oauthSecret = [resultsDict objectForKey:kLoginOAuthSecret];

		// Since we just logged in, make sure to clear the old saved nav stack location, as
		// it is no longer relevant
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		[defaults removeObjectForKey:kCurrentNavigationLocation];

		// save this information in the UserProfileMO row for this user
		UserProfileMO *currentUserProfile = thisUser.userProfile;
		if (!currentUserProfile) {
			currentUserProfile = [UserProfileMO createUserProfileInMOC:self.managedObjectContext];
		}
		if (currentUserProfile) {
			[currentUserProfile setWithUpdateDictionary:resultsDict];

			id permissions = resultsDict[@"permissions"];
			if ([permissions isKindOfClass:[NSDictionary class]]) {
				[currentUserProfile updatePermissions:permissions];
			}

			// Save the context.
			[self.managedObjectContext BL_save];

			[self didLoginWithUser:thisUser];
		}
		else {
			DDLogError(@"No user profile, unable to finish login process");
		}
	}
	else {
		// The login failed... let the user know.
		DDLogError(@"Invalid login\rrealm:%@\r", [BCUrlFactory loginURL]);

		[self showHUDWithLabelText:@"Login failed" andCustomView:nil];
	}
}

- (void)loginWithEmail
{
	if ([self.emailTextField.text length] == 0) {
		[self.emailTextField becomeFirstResponder];
		return;
	}
	else if ([self.passwordTextField.text length] == 0) {
		[self.passwordTextField becomeFirstResponder];
		return;
	}

	MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
	hud.labelText = @"Logging in";

	OAuthConsumer *thisConsumer = [OAuthConsumer currentOAuthConsumer];
	UIDevice *device = [UIDevice currentDevice];
	
	NSDictionary *loginDict = @{ 
		kEmail : self.emailTextField.text,
		kPassword : [self.passwordTextField.text BC_passwordHash],
		kOAuthConsumerKey : [thisConsumer oauthConsumerKey],
		kDeviceName : [device name] };

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	hud.labelText = @"Sending authentication";

	GTMHTTPFetcher *loginFetcher = [GTMHTTPFetcher signedFetcherWithLoginCredentials:loginDict];
	[loginFetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		if (error != nil) {
			[MBProgressHUD hideHUDForView:self.navigationController.view animated:NO];
			NSDictionary *responseHeaders = [loginFetcher responseHeaders];
			if (!self.consumerOAuthRefreshed && [responseHeaders objectForKey:@"Www-Authenticate"]) {
				self.consumerOAuthRefreshed = YES;
				DDLogDebug(@"Refreshing oauth consumer due to apparent issue with current oauth credentials");
				[OAuthConsumer refreshOAuthConsumerWithSuccess:^{
						[self loginWithEmail];
					}
					failure:^(NSError *error) {
						[self showHUDWithLabelText:@"Failed to refresh OAuth consumer!" andCustomView:nil];
                    }];
			}
			else {
				[self showHUDWithLabelText:@"Login failed!" andCustomView:nil];

				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:loginFetcher retrievedData:retrievedData];
				// Reset this flag, so that if the user tries to login again, it will again attempt to clear the oauth if
				// that is still determined to be a problem
				self.consumerOAuthRefreshed = NO;
			}
		} 
		else {
			[self successfulLoginWithData:retrievedData];
		}
	}];
}

- (void)loginWithFacebook
{
	DDLogInfo(@"Entering: %s", __FUNCTION__);
	OAuthConsumer *thisConsumer = [OAuthConsumer currentOAuthConsumer];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	DDLogInfo(@"%@", [appDelegate.facebook description]);

	UIDevice *device = [UIDevice currentDevice];

	NSDictionary *jsonDict = @{
		kFacebookAccessToken : appDelegate.facebook.accessToken,
		kOAuthConsumerKey : [thisConsumer oauthConsumerKey],
		kDeviceName : [device name]};

	[appDelegate setNetworkActivityIndicatorVisible:YES];

	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];

	GTMHTTPFetcher *loginFetcher = [GTMHTTPFetcher fetcherWithURL:[BCUrlFactory loginURL] httpMethod:kPost postData:jsonData];
	[loginFetcher beginFetchWithCompletionHandler:^void(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];

		if (error != nil) {
			NSDictionary *responseHeaders = [loginFetcher responseHeaders];
			if (!self.consumerOAuthRefreshed && [responseHeaders objectForKey:@"Www-Authenticate"]) {
				self.consumerOAuthRefreshed = YES;
				DDLogDebug(@"Refreshing oauth consumer due to apparent issue with current oauth credentials");
				[OAuthConsumer refreshOAuthConsumerWithSuccess:^{
						[self loginWithFacebook];
					}
					failure:^(NSError *error) {
						[self showHUDWithLabelText:@"Failed to refresh OAuth consumer!" andCustomView:nil];
                    }];
			}
			else {
				[self showHUDWithLabelText:@"Login failed!" andCustomView:nil];

				[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:loginFetcher retrievedData:retrievedData];
				// Reset this flag, so that if the user tries to login again, it will again attempt to clear the oauth if
				// that is still determined to be a problem
				self.consumerOAuthRefreshed = NO;
			}
		} 
		else {
			[self successfulLoginWithData:retrievedData];
		}
	}];
}

- (void)updateInterfaceWithReachability:(Reachability *)curReach
{
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
	//NSString *statusString;
	BOOL allowLogin = NO;
    switch ([curReach currentReachabilityStatus])
	{
		case NotReachable:
			{
				//statusString = @"Access Not Available";
				allowLogin = NO;
				break;
			}

		case ReachableViaWWAN:
			{
				//statusString = @"Reachable WWAN";
				allowLogin = YES;
				break;
			}
		case ReachableViaWiFi:
			{
				//statusString = @"Reachable WiFi";
				allowLogin = YES;
				break;
			}
		default:
			{
				//statusString = @"connection status unknown";
				allowLogin = NO;
				break;
			}
	}

	self.loginButton.enabled = allowLogin;
}

- (void)reachabilityChanged:(NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}

- (void)didLoginWithUser:(User *)user
{
	DDLogInfo(@"Valid login for user:%@ in realm:%@", user.name, [BCUrlFactory loginURL]);

	UIView *hudTargetView = self.navigationController.view;
	MBProgressHUD *hud = [MBProgressHUD HUDForView:hudTargetView];
	hud.mode = MBProgressHUDModeCustomView;
	hud.labelText = [NSString stringWithFormat:@"Welcome, %@", user.name];
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(),
		^{
			[MBProgressHUD hideHUDForView:hudTargetView animated:YES];
		});
	[self.delegate userDidLogin:self];
}

- (IBAction)next:(id)sender
{
	if ([self.emailTextField isFirstResponder]) {
		[self.passwordTextField becomeFirstResponder];
	}
	else if ([self.passwordTextField isFirstResponder]) {
		[self.emailTextField becomeFirstResponder];
	}
}

- (IBAction)resignKeyboard:(id)sender
{
	[self.view endEditing:YES];
}

- (void)showHUDWithLabelText:(NSString *)labelText andCustomView:(UIView *)customView
{
	MBProgressHUD *hud = [MBProgressHUD HUDForView:self.navigationController.view];
	if (!hud) {
		hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
	}

	if (customView) {
		hud.mode = MBProgressHUDModeCustomView;
		hud.customView = customView;
	}
	else {
		hud.mode = MBProgressHUDModeText;
	}

	hud.labelText = labelText;

	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3.0 * NSEC_PER_SEC), dispatch_get_main_queue(), 
		^{
			[MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
		});
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kTagAlertResetDatabase) {
        if (buttonIndex == 1) {
            BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];

			BOOL success = [appDelegate resetCoreData];

			MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
			hud.mode = MBProgressHUDModeText;

			if (success) {
				hud.labelText = @"Application data reset";
			}
			else {
				hud.labelText = @"Failed to reset application data";
			}
			dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3.0 * NSEC_PER_SEC), dispatch_get_main_queue(), 
				^{
					[MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
				});
		}
	}
}

#pragma mark - MFMailComposeViewControllerDelegate
-(void)mailComposeController:(MFMailComposeViewController*)controller
didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self dismissViewControllerAnimated:YES completion:^{
		if (result == MFMailComposeResultSent) {
			DDLogVerbose(@"mail sent");
		}
		else {
			DDLogWarn(@"error sending mail, %@", [error description]);
		}
	}];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == actionSheet.cancelButtonIndex) {
		return;
	}

	switch (buttonIndex) {
		case kSendDatabase:
			[self sendDatabase];
			break;
		case kSendLogs:
			[self sendLogs];
			break;
		case kResetDatabase:
			[self promptForResetDatabase];
			break;
		case kResetAuthentication:
			{
				// refresh the oauth consumer cache
				[OAuthConsumer refreshOAuthConsumerWithSuccess:nil
					failure:^(NSError *error) {
						[self showHUDWithLabelText:@"Failed to refresh OAuth consumer!" andCustomView:nil];
                    }];

				// delete the user archive
				//TODO BOOL success = [BCUtilities deleteLoginArchive];
				BOOL success = true;

				MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
				hud.mode = MBProgressHUDModeText;

				if (success) {
					hud.labelText = @"Authentication reset";
					self.emailTextField.text = nil;
					self.passwordTextField.text = nil;
				}
				else {
					hud.labelText = @"Failed to reset authentication";
				}
				dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3.0 * NSEC_PER_SEC), dispatch_get_main_queue(), 
					^{
						[MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
					});
			}
			break;
		default:
			break;
	}
}

@end
