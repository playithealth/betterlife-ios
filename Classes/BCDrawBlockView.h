//
//  BCDrawBlockView.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/25/13.
//  Copyright (c) 2013 BettrLife. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BC_DrawBlock)(UIView* v, CGContextRef context);

@interface BCDrawBlockView : UIView

@property (strong, nonatomic) BC_DrawBlock drawBlock;

@end
