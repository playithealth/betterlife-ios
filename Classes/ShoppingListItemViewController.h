//
//  ShoppingListItemViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShoppingListItem.h"

@protocol ShoppingListItemViewDelegate;

@interface ShoppingListItemViewController : UITableViewController

/// a pointer to the shopping list item which is displayed
@property (strong, nonatomic) ShoppingListItem *shoppingListItem;
/// a toolbar used as a keyboard accessory
@property (strong, nonatomic) IBOutlet UIToolbar *keyboardToolbar;
/// the delegate which gets notified when this item is altered
@property (weak, nonatomic) id <ShoppingListItemViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *healthImageView;
@property (weak, nonatomic) IBOutlet UIImageView *alertImageView;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UIWebView *nutritionWebView;

- (IBAction)incrementButtonTapped:(id)sender;
- (IBAction)decrementButtonTapped:(id)sender;

@end

@protocol ShoppingListItemViewDelegate <NSObject>
@required
- (void)shoppingListItemView:(ShoppingListItemViewController *)shoppingListItemView
   didUpdateItem:(ShoppingListItem *)item;
@end
