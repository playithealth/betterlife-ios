//
//  UIColor+Additions.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/1/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "UIColor+Additions.h"

#if TARGET_OS_IPHONE
	#import <UIKit/UIKit.h>
	@implementation UIColor (Additions)
#else
	#import <Cocoa/Cocoa.h>
	@implementation NSColor (Additions)
#endif


/*
 * some convenience functions to make creating color easier
 */
+ (COLOR_CLASS *)BC_colorWithRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue
{
	return [COLOR_CLASS BC_colorWithRed:red green:green blue:blue alpha:1.0];
}

+ (COLOR_CLASS *)BC_colorWithRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue alpha:(CGFloat)alpha
{
#if TARGET_OS_IPHONE
	return [COLOR_CLASS colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:alpha];
#else
	CGFloat	components[4];
	components[0] = red/255.0;
	components[1] = green/255.0;
	components[2] = blue/255.0;
	components[3] = alpha;
	return [COLOR_CLASS colorWithColorSpace:[NSColorSpace genericRGBColorSpace] components:components count:4];
#endif
}

+ (COLOR_CLASS *)BC_colorWithHex:(NSInteger)hex
{
	return [COLOR_CLASS BC_colorWithRed:((hex & 0xFF0000) >> 16) green:((hex & 0x00FF00) >> 8) blue:(hex & 0x0000FF) alpha:1.0];
}

/*
 * The following colors are taken from the hex values given in the document 'bettrlife_ui_mobile_guides_01v1.png'
 */
+ (COLOR_CLASS *)BC_darkGreenColor
{
    return [COLOR_CLASS BC_colorWithHex:0x365c00];
}

+ (COLOR_CLASS *)BC_mediumDarkGreenColor
{
    return [COLOR_CLASS BC_colorWithHex:0x447500];
}

+ (COLOR_CLASS *)BC_mediumLightGreenColor
{
    return [COLOR_CLASS BC_colorWithHex:0x699e00];
}

+ (COLOR_CLASS *)BC_lightGreenColor
{
    return [COLOR_CLASS BC_colorWithHex:0x8fc400];
}

+ (COLOR_CLASS *)BC_blueColor
{
    return [COLOR_CLASS BC_colorWithHex:0x0086b2];
}

+ (COLOR_CLASS *)BC_redColor
{
	return [COLOR_CLASS BC_colorWithHex:0xe22222];
}

+ (COLOR_CLASS *)BC_goldColor
{
    return [COLOR_CLASS BC_colorWithHex:0xffc61a];
}

+ (COLOR_CLASS *)BC_backgroundColor
{
    return [COLOR_CLASS BC_colorWithHex:0xf4f2ee];
}

+ (COLOR_CLASS *)BC_lightGrayColor
{
    return [COLOR_CLASS BC_colorWithHex:0xeaeae9];
}

+ (COLOR_CLASS *)BC_mediumLightGrayColor
{
    return [COLOR_CLASS BC_colorWithHex:0xcdccca];
}

+ (COLOR_CLASS *)BC_mediumDarkGrayColor
{
    return [COLOR_CLASS BC_colorWithHex:0x727375];
}

+ (COLOR_CLASS *)BC_darkGrayColor
{
    return [COLOR_CLASS BC_colorWithHex:0x303439];
}

/*
 * making the background easier in a UITableViewCell
 */
+ (COLOR_CLASS *)BC_groupedTableViewCellBackgroundColor
{
    return [COLOR_CLASS colorWithWhite:0.968f alpha:1.0f];
}

/*
 * standard table view header color
 */
+ (COLOR_CLASS *)BC_tableViewHeaderColor
{
    return [COLOR_CLASS BC_colorWithHex:0xe9e9e8];
}

/*
 * standard table view header color
 */
+ (COLOR_CLASS *)BC_tableViewHeaderTextColor
{
    return [COLOR_CLASS BC_colorWithHex:0x232323];
}

/*
 * the bluish color I was using for editable text (deprecated)
 */
+ (COLOR_CLASS *)BC_nonEditableTextColor
{
    return [COLOR_CLASS colorWithHue:0.6f saturation:0.44f brightness:0.57f alpha:1.0f];
}

+ (COLOR_CLASS *)BL_goalRangeColor
{
	return [COLOR_CLASS BC_colorWithRed:108 green:173 blue:55 alpha:0.3];
}

+ (COLOR_CLASS *)BL_mehTextColor
{
	return [COLOR_CLASS BC_colorWithRed:86 green:96 blue:59 alpha:1.0f];
}

+ (COLOR_CLASS *)BL_darkLineColor
{
    return [COLOR_CLASS BC_colorWithHex:0xB2B2B2];
}

+ (COLOR_CLASS *)BL_lightLineColor
{
    return [COLOR_CLASS BC_colorWithHex:0xC8C7CC];
}

+ (COLOR_CLASS *)BL_tabColor
{
    return [COLOR_CLASS BC_colorWithHex:0xEFEFF4];
}

+ (COLOR_CLASS *)BL_linkColor
{
    return [COLOR_CLASS BC_colorWithHex:0x157EFB];
}

+ (COLOR_CLASS *)BL_powderBlueColor
{
    return [COLOR_CLASS BC_colorWithHex:0x00AFEC];
}

+ (COLOR_CLASS *)BL_happySmileyColor
{
    return [COLOR_CLASS BC_colorWithHex:0x8FC400];
}

+ (COLOR_CLASS *)BL_mehSmileyColor
{
    return [COLOR_CLASS BC_colorWithHex:0xC8E47E];
}

+ (COLOR_CLASS *)BL_sadSmileyColor
{
    return [COLOR_CLASS BC_colorWithHex:0xFEBB2D];
}

// Graph colors
+ (COLOR_CLASS *)BL_graphBlue
{
    return [COLOR_CLASS BC_colorWithHex:0x0086b2];
}

+ (COLOR_CLASS *)BL_graphGreen
{
    return [COLOR_CLASS BC_colorWithHex:0x699e00];
}

+ (COLOR_CLASS *)BL_graphRed
{
    return [COLOR_CLASS BC_colorWithHex:0xdd4360];
}

+ (COLOR_CLASS *)BL_graphOrange
{
    return [COLOR_CLASS BC_colorWithHex:0xf47b2a];
}

+ (COLOR_CLASS *)BL_graphPurple
{
    return [COLOR_CLASS BC_colorWithHex:0x7b4ba0];
}

+ (COLOR_CLASS *)BL_graphGray
{
    return [COLOR_CLASS BC_colorWithHex:0x727272];
}

+ (COLOR_CLASS *)BL_graphGoal
{
    return [COLOR_CLASS BC_colorWithHex:0xffc61a];
}

// Kosama Theme colors

+ (COLOR_CLASS *)BL_topKosamaBlue {
	return [COLOR_CLASS BC_colorWithRed:34 green:96 blue:178 alpha:1.0];
}

+ (COLOR_CLASS *)BL_bottomKosamaBlue {
	return [COLOR_CLASS BC_colorWithRed:31 green:142 blue:200 alpha:1.0];
}

// Food Log colors
+ (COLOR_CLASS *)BL_foodlogPurple {
    return [COLOR_CLASS BC_colorWithHex:0xd26da6];
}

// Activity Plan colors
+ (COLOR_CLASS *)BL_activityOrange {
    return [COLOR_CLASS BC_colorWithHex:0xff8b3d];
}

+ (COLOR_CLASS *)BL_activityGreen {
    return [COLOR_CLASS BC_colorWithHex:0x1cd71c];
}

@end
