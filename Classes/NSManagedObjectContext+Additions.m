//
//  NSManagedObjectContext+Additions.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 2/22/14.
//  Copyright (c) 2014 BettrLife Corporation All rights reserved.
//

#import "NSManagedObjectContext+Additions.h"

@implementation NSManagedObjectContext (BettrLife)

- (BOOL)BL_save
{
	NSError *error = nil;
	BOOL ret = [self save:&error];
	if (!ret) {
		DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}

	return ret;
}

@end
