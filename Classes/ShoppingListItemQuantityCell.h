//
//  ShoppingListItemQuantityCell.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/6/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingListItemQuantityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
