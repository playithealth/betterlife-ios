//
//  StoreSearchViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "StoreSearchViewController.h"

#import "BCObjectManager.h"
#import "BCTableViewCell.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BCAppDelegate.h"
#import "GTMHTTPFetcherAdditions.h"
#import "BCProgressHUD.h"
#import "NumberValue.h"
#import "OAuthConsumer.h"
#import "OAuthUtilities.h"
#import "StarRatingControl.h"
#import "StoreDetailViewController.h"
#import "StoreMapViewController.h"
#import "UIColor+Additions.h"
#import "User.h"
#import "UserStoreMO.h"

static const NSInteger kSectionAddCustomStore = 0;
static const NSInteger kSectionPreviousStores = 1;
static const NSInteger kSectionNearbyStores = 2;

static const NSInteger kTagAlertZipCode = 2;

@interface StoreSearchViewController ()
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UITextField *searchTextField;
@property (nonatomic, strong) NSArray *nearbyStores;
@property (nonatomic, strong) NSArray *previousStores;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;

@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) NSString *searchZipCode;
@property (strong, nonatomic) NSTimer *locationServiceTimer;
@property (strong, nonatomic) NSMutableDictionary *userStoresByEntityId;

// The location manager can continue sending packets even after we've told it to
// stop.  I believe these are packets that were in process when we asked it to stop.
// Either way, this will cause us to call unnecessary, and potentially harmful duplicate
// calls to searchForNearbyStoresByName, as well as having the HUD fighting back and forth.  This
// flag should keep this from happening.
@property (assign, nonatomic) BOOL doneUpdatingLocation;

- (void)transitionSpinnerToNavBar;
- (void)removeSpinnerWithAnimation:(BOOL)animation;
- (void)startLocationServiceTimer;
- (void)stopLocationServiceTimer;
- (void)locationServiceTimerFired:(NSTimer *)timer;
@end

@implementation StoreSearchViewController

#pragma mark - view life cycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.doneUpdatingLocation = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];

	// Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
	// Hide the HUD in the main thread
	[BCProgressHUD hideHUD:YES];

	self.locationManager.delegate = nil;
	self.locationManager = nil;
	[self setSearchTextField:nil];
	[super viewDidUnload];
	self.doneUpdatingLocation = NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowMap"]) {
		StoreMapViewController *storeMapView = segue.destinationViewController;
		storeMapView.stores = self.nearbyStores;
		storeMapView.startingLocation = self.currentLocation;
		storeMapView.delegate = self.delegate;
	}
	else if ([segue.identifier isEqualToString:@"ShowPreviousStoreDetail"]) {
		StoreDetailViewController *detailView = segue.destinationViewController;
		detailView.managedObjectContext = self.managedObjectContext;
		detailView.storeSelectionDelegate = self;

		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		UserStoreMO *userStore = [self.previousStores objectAtIndex:indexPath.row];
		detailView.storeData = userStore;
	}
	else if ([segue.identifier isEqualToString:@"ShowNearbyStoreDetail"]) {
		StoreDetailViewController *detailView = segue.destinationViewController;
		detailView.managedObjectContext = self.managedObjectContext;
		detailView.storeSelectionDelegate = self;

		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		NSDictionary *nearbyStore = [self.nearbyStores objectAtIndex:indexPath.row];
		detailView.storeData = nearbyStore;
	}
}

#pragma mark - local methods

- (void)transitionSpinnerToNavBar
{
	[UIView animateWithDuration:0.5
		animations:^{
			self.spinner.center = CGPointMake (250, 42);
		}
		completion:^(BOOL finished) {
			// release the big spinner, swap in a small one
			// if this is nil, then the process has already finished
			// in the delegate, so there is no reason to create the
			// little spinner or remove the big one... 
			if (self.spinner) {
				[self.spinner removeFromSuperview];
				self.spinner = [[UIActivityIndicatorView alloc]
					initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
				[self.navigationController.view addSubview:self.spinner];
				self.spinner.center = CGPointMake(250, 42);
				[self.spinner startAnimating];
			}
		}];
}

- (void)removeSpinnerWithAnimation:(BOOL)animation
{
	if (animation) {
		[UIView animateWithDuration:0.3
			animations:^{
				self.spinner.alpha = 0.0;
			}
			completion:^(BOOL finished) {
				[self.spinner removeFromSuperview];
				[self setSpinner:nil];
			}];
	}
	else {
		[self.spinner removeFromSuperview];
		[self setSpinner:nil];
	}
}

- (void)startLocationServiceTimer
{
	NSTimeInterval seconds = 10;
	self.locationServiceTimer = [NSTimer scheduledTimerWithTimeInterval:seconds
		target:self
		selector:@selector(locationServiceTimerFired:)
		userInfo:nil
		repeats:NO];
}

- (void)stopLocationServiceTimer
{
	[self.locationServiceTimer invalidate];
	self.locationServiceTimer = nil;
}

- (void)locationServiceTimerFired:(NSTimer *)timer
{
	// Make sure that the location service timer is still there, as if not,
	// then some other process has attempted to stop it already, and this processing
	// is unnecessary and undesired
	if (self.locationServiceTimer) {
		[self stopLocationServiceTimer];

		[self.locationManager stopUpdatingLocation];
		self.doneUpdatingLocation = YES;
		[self removeSpinnerWithAnimation:YES];

		[self searchForNearbyStoresByName:@"" showMore:NO];
	}
}

- (BOOL)canBecomeFirstResponder
{
	return YES;
}

- (void)buildUserStoreMap
{
	NSArray *userStoresArray = [UserStoreMO MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"accountId = %@",
		[[User currentUser] accountId]] inContext:self.managedObjectContext];

	self.userStoresByEntityId = [NSMutableDictionary dictionary];
	for (UserStoreMO *favStore in userStoresArray) {
		[self.userStoresByEntityId setObject:favStore forKey:favStore.storeEntityId];
	}
}

- (void)checkinWithStore:(id)store
{
	if (self.delegate != nil && [self.delegate conformsToProtocol:@protocol(StoreSelectionDelegate)]) {
		UserStoreMO *userStore  = nil;
		if (![store isMemberOfClass:[UserStoreMO class]]) {
			NSNumber *entityId = [[store valueForKey:kCloudId] numberValueDecimal];
			userStore = [self.userStoresByEntityId objectForKey:entityId];
			if (userStore) {
				store = userStore;
			}
			else {
				UserStoreMO *newStore = [UserStoreMO insertInManagedObjectContext:self.managedObjectContext];

				[newStore setWithDictionary:store];
				newStore.status = kStatusPost;
				newStore.storeEntityId = newStore.cloudId;
				newStore.cloudId = @0;
				newStore.updatedOn = @0;

				NSError *error = nil;
				if (![newStore.managedObjectContext save:&error]) {
					DDLogError(@"Error saving new store for checkin %@, %@", error, [error userInfo]);
					DDLogError(@"store data:%@", store);
				}

				store = newStore;
			}
		}
		[self.delegate view:self didSelectStore:store];

		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)checkinButtonTapped:(UITapGestureRecognizer *)recognizer
{
	CGPoint location = [recognizer locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

	id store = nil;
	if (indexPath.section == kSectionPreviousStores) {
		store = [self.previousStores objectAtIndex:indexPath.row];
	}
	else if (indexPath.section == kSectionNearbyStores) {
		store = [self.nearbyStores objectAtIndex:indexPath.row];
	}

	[self checkinWithStore:store];
}

#pragma mark - web requests
- (void)searchForNearbyStoresByName:(NSString *)searchTerm showMore:(BOOL)more
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	if (!appDelegate.connected) {
		return;
	}
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	BCTableViewCell *bcCell = nil;

	NSUInteger offset = 0;
	if (more && ([self.nearbyStores count] > 0)) {
		offset = [self.nearbyStores count];

		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.nearbyStores count] inSection:kSectionNearbyStores];
		bcCell = (BCTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
		[bcCell.spinner startAnimating];
		bcCell.bcTextLabel.text = @"Loading...";
	}

	// make the url request for getting the stores
	NSURL *storeSearchURL = nil;
	if (self.currentLocation) {
		storeSearchURL = [BCUrlFactory searchStoresURLByLocation:self.currentLocation name:searchTerm showRows:10 rowOffset:offset];
	}
	else {
		User *thisUser = [User currentUser];
		if (!self.searchZipCode) {
			self.searchZipCode = thisUser.homeZip;
		}
		storeSearchURL = [BCUrlFactory searchStoresURLByZip:self.searchZipCode name:searchTerm showRows:10 rowOffset:offset];
	}

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:storeSearchURL];
	[fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];
		if (error != nil) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:nil];
		}
		else {
			// fetch succeeded
			NSString *response = [[NSString alloc] initWithData:retrievedData encoding:NSUTF8StringEncoding];

			if (more) {
				self.nearbyStores = [self.nearbyStores arrayByAddingObjectsFromArray:[response JSONValue]];
			}
			else {
				self.nearbyStores = [response JSONValue];
			}
			[self.tableView reloadData];
		}

		if (bcCell) {
			[bcCell.spinner stopAnimating];
			bcCell.bcTextLabel.text = @"Load more results...";
		}
	}];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[self buildUserStoreMap];
	[BCObjectManager syncCheck:SendAndReceive];

	// set up the location service
	self.locationManager = [[CLLocationManager alloc] init];
	self.locationManager.delegate = self;
	self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;

	// Prepopulate the list with their favorite stores
	self.previousStores = [self.userStoresByEntityId allValues];

	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
	self.navigationController.navigationBar.tintColor = [UIColor darkGrayColor];	

	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	// start the location ping
	[self.locationManager startUpdatingLocation];
	// Start the location service timer, just in case it takes too long for this to work
	[self startLocationServiceTimer];

	// No need to bring up the HUD if we're already done with location services
	if (!self.doneUpdatingLocation) {
		// NOTE hiding this is done in the dispatch below
		BCProgressHUD *hud = [BCProgressHUD showHUDWithText:@"\n\nupdating current location..."
			onView:self.navigationController.view
			animated:YES];

		// create the spinner
		self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:
			UIActivityIndicatorViewStyleWhiteLarge];
		[self.navigationController.view addSubview:self.spinner];
		self.spinner.center = CGPointMake(160, 225);
		[self.spinner startAnimating];

		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC), 
			dispatch_get_main_queue(), ^{
				[hud hide:YES];
				[self transitionSpinnerToNavBar];
			});
	}

	[super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[self removeSpinnerWithAnimation:NO];

	self.navigationController.navigationBar.tintColor = [UIColor BC_mediumLightGreenColor];	

	[super viewWillDisappear:animated];
}

#pragma mark - CLLocationManagerDelegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	static NSUInteger validPackets = 0;

	[self stopLocationServiceTimer];

	if (self.doneUpdatingLocation) {
		DDLogInfo(@"%s - Got location update after asking the service to stop, skipping", __FUNCTION__);
		return;
	}

	DDLogVerbose(@"%@", newLocation);

	self.navigationItem.title = @"Store Search";

	NSDate *eventDate = newLocation.timestamp;
	NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
	// capture new location
	self.currentLocation = newLocation;
	if (abs(howRecent) < 60.0) {
		if (++validPackets >= 2) {
			// Got a recent location update
			[manager stopUpdatingLocation];
			self.doneUpdatingLocation = YES;
			DDLogVerbose(@"stopping location updates");

			// Hide the spinner and the HUD if it is still there
			[BCProgressHUD hideHUD:YES];
			[self removeSpinnerWithAnimation:YES];

			self.nearbyStores = nil;
			[self searchForNearbyStoresByName:@"" showMore:NO];
			// Important note:  The following return keeps the location service timer
			// from being restarted
			return;
		}
	}
	else {
		validPackets = 0;
	}

	// Important note:  If we have concluded our efforts to get the current location,
	// do not restart the timer, see the 'return' and accompanying comment above
	[self startLocationServiceTimer];
}

- (void)locationManager:(CLLocationManager *)manager
	didFailWithError:(NSError *)error
{
	/*
	NSString *errorType = (error.code == kCLErrorDenied)
		? @"Access Denied" : @"Unknown Error";
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error getting location"
		message:errorType delegate:nil cancelButtonTitle:@"OK"
		otherButtonTitles:nil, nil];
	[alert show];
	[alert release];
	*/

	self.navigationItem.title = @"Store Search";

	// Hide the spinner and the HUD if it is still there
	[BCProgressHUD hideHUD:YES];
	[self removeSpinnerWithAnimation:YES];

	// turn off location ping
	[self.locationManager stopUpdatingLocation];

	// ask user if they want to get the list by Zip
	UIActionSheet *actionSheet = [[UIActionSheet alloc]
		initWithTitle:@"No location services"
		delegate:self
		cancelButtonTitle:@"Cancel search"
		destructiveButtonTitle:nil
		otherButtonTitles:@"Use zipcode", nil];
	[actionSheet showInView:self.view];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    
    if (section == kSectionAddCustomStore) {
        if ([self.searchTextField.text length] > 0) {
            numberOfRows = 1;
        }
    }
    else if (section == kSectionPreviousStores) {
        numberOfRows = [self.previousStores count];
    }
    else if (section == kSectionNearbyStores) {
		if ([self.nearbyStores count] > 0) {
			numberOfRows = [self.nearbyStores count] + 1;
		}
    }

    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *addCustomCellId = @"AddCustomCell";
    static NSString *previousStoreCellId = @"PreviousStoreCell";
    static NSString *nearbyStoreCellId = @"NearbyStoreCell";
    static NSString *loadMoreCellId = @"LoadMoreCell";

	UITableViewCell *cell = nil;
    if (indexPath.section == kSectionAddCustomStore) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:addCustomCellId];
    }
	else if (indexPath.section == kSectionPreviousStores) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:previousStoreCellId];

		UIImageView *checkinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkin-green"]];
		checkinImageView.userInteractionEnabled = YES;
		cell.accessoryView = checkinImageView;

		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkinButtonTapped:)];
		[checkinImageView addGestureRecognizer:recognizer];
	}
	else if (indexPath.section == kSectionNearbyStores) {
		if (indexPath.row < [self.nearbyStores count]) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:nearbyStoreCellId];

			UIImageView *checkinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkin-green"]];
			checkinImageView.userInteractionEnabled = YES;
			cell.accessoryView = checkinImageView;

			UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkinButtonTapped:)];
			[checkinImageView addGestureRecognizer:recognizer];
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:loadMoreCellId];
		}
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionAddCustomStore) {
        cell.textLabel.text = [NSString stringWithFormat:@"Add “%@” as a custom store", self.searchTextField.text];
    }
    else if (indexPath.section == kSectionPreviousStores) {
		UserStoreMO *userStore = [self.previousStores objectAtIndex:indexPath.row];
		cell.textLabel.text = userStore.name;
		cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@ - %@",
			userStore.city, userStore.state, userStore.address];
    }
    else if (indexPath.section == kSectionNearbyStores) {
		if (indexPath.row < [self.nearbyStores count]) {
			NSDictionary *nearbyStore = [self.nearbyStores objectAtIndex:indexPath.row];
			cell.textLabel.text = [nearbyStore valueForKey:kStore];
			cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@ - %@",
				[nearbyStore valueForKey:kCity], [nearbyStore valueForKey:kState], 
				[nearbyStore valueForKey:kAddress]];
		}
	}
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 0.0f;
    switch (section) {
        case kSectionPreviousStores:
            if ([self.previousStores count] > 0) {
                headerHeight = 24.0f;
            }
            break;
        case kSectionNearbyStores:
            if ([self.nearbyStores count] > 0) {
                headerHeight = 24.0f;
            }
            break;
        case kSectionAddCustomStore:
        default:
            break;
    }
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = nil;
    switch (section) {
        case kSectionPreviousStores:
            sectionTitle = @"Previous Stores";
            break;
        case kSectionNearbyStores:
            sectionTitle = @"Nearby Stores";
            break;
        case kSectionAddCustomStore:
        default:
            break;
    }
    
	return [BCTableViewController customViewForHeaderWithTitle:sectionTitle];
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [theTableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.section == kSectionAddCustomStore) {
		UserStoreMO *newStore = [UserStoreMO insertInManagedObjectContext:self.managedObjectContext];

		newStore.name = self.searchTextField.text;
		newStore.status = kStatusPost;
		newStore.accountId = [[User currentUser] accountId];
		double latitude = self.currentLocation.coordinate.latitude;
		double longitude = self.currentLocation.coordinate.longitude;
		newStore.latitude = [NSNumber numberWithDouble:latitude];
		newStore.longitude = [NSNumber numberWithDouble:longitude];

		[self.managedObjectContext save:nil];

		[self checkinWithStore:newStore];
	}
	else if (indexPath.section == kSectionNearbyStores) {
		if (indexPath.row == [self.nearbyStores count]) {
			// show more
			[self searchForNearbyStoresByName:self.searchTextField.text showMore:YES];
		}
	}
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.searchTextField resignFirstResponder];
	return indexPath;
}

#pragma mark - Text Field delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	NSString *searchTerm = self.searchTextField.text;
	[self.searchTextField resignFirstResponder];

	self.previousStores = nil;
	self.nearbyStores = nil;

	if ([searchTerm length] > 0) {
		self.previousStores = [UserStoreMO MR_findAllSortedBy:UserStoreMOAttributes.name ascending:YES
			withPredicate:[NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@ AND accountId = %@", 
            UserStoreMOAttributes.name, searchTerm, [[User currentUser] accountId]] inContext:self.managedObjectContext];

		[self searchForNearbyStoresByName:searchTerm showMore:NO];
    }

	[self.tableView reloadData];

    return YES;
}

- (IBAction)searchTextFieldEditingChanged:(id)sender
{
    [self.tableView reloadData];
}

#pragma mark - ActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet
	didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == [actionSheet cancelButtonIndex]) {
		//[self.navigationController popViewControllerAnimated:YES];
	}
	else {
		NSString *zipCode = nil;
		User *thisUser = [User currentUser];
		if (![thisUser.homeZip isEqualToString:@"00000"]) {
			zipCode = thisUser.homeZip;
		}
		else {
			zipCode = @"";
		}
		// popup a text box to enter zip

		UIAlertView *alert = [[UIAlertView alloc]
			initWithTitle:@"Search by zipcode"
			message:@"Use this zipcode for search:\n\n\n"
			delegate:self
			cancelButtonTitle:@"No"
			otherButtonTitles:@"Yes", nil];
		alert.tag = kTagAlertZipCode;
		alert.alertViewStyle = UIAlertViewStylePlainTextInput;
		[alert textFieldAtIndex:0].placeholder = @"Zipcode";
        [alert textFieldAtIndex:0].text = zipCode;

		[alert show];
	}
}

#pragma mark - StoreMapView delegate
- (void)storeMapView:(StoreMapViewController *)storeSearchView didSelectStore:(id)storeData
{
	[self checkinWithStore:storeData];
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == kTagAlertZipCode) {
		if (buttonIndex != alertView.cancelButtonIndex) {
			self.searchZipCode = [alertView textFieldAtIndex:0].text;
			self.nearbyStores = nil;
			[self searchForNearbyStoresByName:@"" showMore:NO];
		}
	}
}

#pragma mark - BCStoreSelectionDelegate
- (void)view:(id)viewController didSelectStore:(id)inStore
{
	// the user has tapped the check in button on the detail view
	// just pass this along
	[self checkinWithStore:inStore];
}

@end
