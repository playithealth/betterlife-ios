//
//  BCViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/21/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "BCViewController.h"

#import "BCAppDelegate.h"
#import "UIColor+Additions.h"

@interface BCViewController ()
- (IBAction)viewSidebar:(id)sender;
@end

@implementation BCViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	UIBarButtonItem *navButton = [[UIBarButtonItem alloc]
		initWithImage:[UIImage imageNamed:@"sidebar-button"]
				style:UIBarButtonItemStylePlain
			   target:self action:@selector(viewSidebar:)];

	// If this view is the bottom of the nav stack, don't display the back button
	BOOL leftItemsSupplementBackButton = NO;

	// Figure out if we're on a root view controller, using a local var for cleanliness
	BOOL isRootVC = [[self.navigationController.viewControllers objectAtIndex:0] isEqual:self];

	if (!isRootVC) {
		// We're not on a root view controller, so at best we want to share the left
		// side of the navigation controller (item) with the back button
		leftItemsSupplementBackButton = YES;
	}

	// We always want the sidebar on root view controllers, and we can also have it on other VCs
	// next to the back button
	if (isRootVC) {
		self.navigationItem.leftBarButtonItem = navButton;
	}

	self.navigationItem.leftItemsSupplementBackButton = leftItemsSupplementBackButton;

	[super viewDidLoad];
}

- (void) willNavigateAway
{
	// Added as a hack to help with navigation from one of our sections to another
	// since viewWillDisappear/viewWillUnload don't get called early enough.
}

#pragma mark - local methods
- (IBAction)viewSidebar:(id)sender
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];

	[appDelegate showSidebar:YES disableClosing:NO];
}

+ (UIView *)customNavigationHeaderViewForTitle:(NSString *)title
{
    UILabel *headerView = [[UILabel alloc] init];
    headerView.text = title;
    headerView.textAlignment = NSTextAlignmentCenter;

	// on iOS 7 and greater show green text on a white background
	// earlier versions show green nav bar with white text
	if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f ) {
		headerView.backgroundColor = [UIColor clearColor];
		headerView.textColor = [UIColor whiteColor];
		headerView.shadowColor = [UIColor BC_darkGreenColor];
		headerView.shadowOffset = CGSizeMake(0, -1);
	}
	else {
		headerView.backgroundColor = [UIColor whiteColor];
		headerView.textColor = [UIColor BC_mediumDarkGreenColor];
	}

    headerView.font = [UIFont systemFontOfSize:24];
    [headerView sizeToFit];
    
    return headerView;
}

@end
