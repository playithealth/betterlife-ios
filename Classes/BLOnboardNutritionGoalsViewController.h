//
//  BLOnboardNutritionGoalsViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 3/28/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BLOnboardViewDelegate.h"

@protocol BLOnboardDelegate;

@interface BLOnboardNutritionGoalsViewController : UITableViewController <BLOnboardViewDelegate>
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *viewStack;
@property (weak, nonatomic) id<BLOnboardDelegate> delegate;
@end
