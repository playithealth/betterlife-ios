//
//  StoreSelectionDelegate.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/29/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#ifndef BuyerCompass_StoreSelectionDelegate_h
#define BuyerCompass_StoreSelectionDelegate_h

#import <Foundation/Foundation.h>

@protocol StoreSelectionDelegate <NSObject>
@required
- (void)view:(id)viewController didSelectStore:(id)store;
@end

#endif
