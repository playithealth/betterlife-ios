//
//  BCSidebarNavigationCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/13/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CustomBadge.h"

@interface BCSidebarNavigationCell : UITableViewCell

@property (strong, nonatomic) CustomBadge *customBadge;

@end

@interface BLSidebarProgressCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (strong, nonatomic) IBOutlet UILabel *valueLabel;
@end
