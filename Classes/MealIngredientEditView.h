//
//  MealIngredientEditView.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 10/18/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCViewController.h"
#import "MealIngredientEditCell.h"
#import "MealIngredient.h"
#import "ProductSearchView.h"
#import "IngredientShoppingListItemCell.h"

@protocol MealIngredientEditViewDelegate;

@interface MealIngredientEditView : BCViewController <UITextFieldDelegate, ProductSearchViewDelegate>
{
	UITextField *editingTextField;
	BOOL changedIngredient;
	CGFloat textFieldAnimatedDistance;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet IngredientShoppingListItemCell *shoppingListItemCell;
@property (strong, nonatomic) MealIngredient *ingredient;
@property (unsafe_unretained, nonatomic) id<MealIngredientEditViewDelegate> delegate;
@end

@protocol MealIngredientEditViewDelegate <NSObject>
@required
- (void)mealIngredientEditView:(MealIngredientEditView *)mealIngredientEditView
didUpdateIngredient:(MealIngredient *)ingredient;
@end
