//
//  TrainingStrengthMigrationPolicy.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/27/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "TrainingStrengthMigrationPolicy.h"

@implementation TrainingStrengthMigrationPolicy
- (NSString *)nameFromFocus:(NSString *)focus equipment:(NSString *)equipment
{
	return [NSString stringWithFormat:@"%@ %@", focus, equipment];
}
@end
