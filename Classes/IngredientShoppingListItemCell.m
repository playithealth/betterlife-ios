//
//  IngredientShoppingListItemCell.m
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 10/28/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "IngredientShoppingListItemCell.h"

@implementation IngredientShoppingListItemCell

@synthesize textLabel;
@synthesize quantityButton;
@synthesize quantityLabel;
@synthesize incrementButton;
@synthesize decrementButton;
@synthesize accessoryButton;
@synthesize clearButton;
@synthesize associateButton;


@end
