//
//  BCColorButton.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/6/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCColorButton.h"

@implementation BCColorButton
@synthesize gradientLayer;

- (void)awakeFromNib;
{
    gradientLayer = [[CAGradientLayer alloc] init];
    [gradientLayer setBounds:[self bounds]];
    [gradientLayer setPosition:CGPointMake([self bounds].size.width/2, [self bounds].size.height/2)];
    
    [[self layer] insertSublayer:gradientLayer atIndex:0];
    
    [[self layer] setCornerRadius:8.0f];
    [[self layer] setMasksToBounds:YES];
    [[self layer] setBorderWidth:1.0f];
    
}

- (void)drawRect:(CGRect)rect;
{
    if (_highColor && _lowColor)
    {
        [gradientLayer setColors:[NSArray arrayWithObjects:(id)[_highColor CGColor], (id)[_lowColor CGColor], nil]];
    }
    [super drawRect:rect];
}

- (UIColor*)highColor
{
    return _highColor;
}

- (UIColor*)lowColor
{
    return _lowColor;
}

- (void)setHighColor:(UIColor*)color;
{
    _highColor = color;
    [[self layer] setNeedsDisplay];
}

- (void)setLowColor:(UIColor*)color;
{
    _lowColor = color;
    [[self layer] setNeedsDisplay];
}

@end
