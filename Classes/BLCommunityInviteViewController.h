//
//  BLCommunityInviteViewController.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 9/9/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CommunityMO;

@interface BLCommunityInviteViewController : UITableViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) CommunityMO *community;
@end
