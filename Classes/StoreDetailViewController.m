//
//  StoreDetailViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "StoreDetailViewController.h"

#import "BCObjectManager.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "BCCategoryOrderViewController.h"
#import "GTMHTTPFetcherAdditions.h"
#import "GenericValueDisplay.h"
#import "NSArray+NestedArrays.h"
#import "NSDateFormatter+Additions.h"
#import "NSString+BCAdditions.h"
#import "NumberValue.h"
#import "OAuthUtilities.h"
#import "UIColor+Additions.h"
#import "User.h"
#import "UserStoreMO.h"

@interface StoreDetailViewController ()
@property (strong, nonatomic) NSMutableArray *sectionNames;
@property (strong, nonatomic) NSMutableArray *rowLabels;
@property (strong, nonatomic) NSMutableArray *rowValues;
@property (strong, nonatomic) NSMutableArray *addedRatings;
@property (assign, nonatomic) BOOL storeChanged;
@property (strong, nonatomic) BCMoreLessFooter *amenitiesFooterView;
@property (assign, nonatomic) BOOL needsAmenitiesFooter;

- (void)didChangeStoreCategories:(NSNotification *)notification;
@end

@implementation StoreDetailViewController

#pragma mark - Memory Management
- (id)initWithCoder:(NSCoder *)aCoder
{
	self = [super initWithCoder:aCoder];
	if (self) {
		self.addedRatings = [[NSMutableArray alloc] init];
	}
	return self;
}

#pragma mark - lifesapn
- (void)viewDidLoad
{
	UIView *backgroundView = [[UIView alloc] init];

	backgroundView.backgroundColor = [UIColor BC_lightGrayColor];
	self.tableView.backgroundView = backgroundView;

	if (self.storeSelectionDelegate) {
		UIBarButtonItem *checkinButton = [[UIBarButtonItem alloc]
			initWithImage:[UIImage imageNamed:@"checkin-ico-sm"]
			style:UIBarButtonItemStyleBordered
			target:self action:@selector(checkinButtonTapped:)];
		self.navigationItem.rightBarButtonItem = checkinButton;
	}

	// Want to receive notifications regarding changes to StoreCategories
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(didChangeStoreCategories:)
		name:UserStoresDidChangeStoreCategories
		object:nil];

	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
	GTMHTTPFetcher *amenitiesFetcher = [GTMHTTPFetcher signedFetcherWithURL:
										[BCUrlFactory amenitiesURLForStoreId:self.storeId]];

	[amenitiesFetcher beginFetchWithDelegate:self
		didFinishSelector:@selector(amenitiesFetcher:finishedWithData:error:)];

	NSString *storeKeyName = kStore;
	if ([self.storeData isKindOfClass:[UserStoreMO class]]) {
		storeKeyName = @"name";
	}
	double latitude = [[[self.storeData valueForKey:@"latitude"]
						numberValueWithNumberStyle:NSNumberFormatterDecimalStyle] doubleValue];
	double longitude = [[[self.storeData valueForKey:@"longitude"]
		numberValueWithNumberStyle:NSNumberFormatterDecimalStyle] doubleValue] * -1;
	self.storeMapLocation = [[StoreMapLocation alloc]
		initWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude)
		title:[self.storeData valueForKey:storeKeyName]];
	self.storeMapLocation.subtitle = [self.storeData valueForKey:@"address"];

	self.sectionNames = [[NSMutableArray alloc] initWithObjects:
		[NSNull null],
		@"Store",
		@"Categories",
		nil];

	self.rowLabels = [[NSMutableArray alloc] initWithObjects:
		[NSArray arrayWithObject:@""],
		[NSArray arrayWithObjects:@"Name", @"Phone", @"Address", nil],
		[NSArray arrayWithObjects:@"Update order", @"Auto Sorting", nil],
		nil];

	NSArray *storeRowValues = nil;
	if ([self.storeData isKindOfClass:[UserStoreMO class]]) {
		storeRowValues = [NSArray arrayWithObjects:
			[self.storeData valueForKey:@"name"],
			[self.storeData valueForKey:@"phone"],
			[self.storeData valueForKey:@"address"],
			nil];
	}
	else {
		storeRowValues = [NSArray arrayWithObjects:
			[self.storeData valueForKey:kStore],
			[self.storeData valueForKey:kPhone],
			[self.storeData valueForKey:kAddress],
			nil];
	}

	self.rowValues = [[NSMutableArray alloc] initWithObjects:
		[NSNull null],
		storeRowValues,
		[NSNull null],
		nil];

	[self.tableView reloadData];

	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	self.storeMapView.showsUserLocation = NO;
	self.storeMapView.delegate = nil;

	if ([self.addedRatings count]) {
		GTMHTTPFetcher *ratingsSender = [GTMHTTPFetcher signedFetcherWithURL:[BCUrlFactory evaluateURL] httpMethod:kPost 
			postData:[NSJSONSerialization dataWithJSONObject:self.addedRatings options:kNilOptions error:nil]];
		[ratingsSender beginFetchWithCompletionHandler:
			^(NSData * retrievedData, NSError * error) {
				if (error != nil) {
					// status code or network error
					DDLogError (@"failed %@", error);
				}
				else {
					// succeeded - clear out the addedRatings so that it doesn't
					// send these a second time
					[self.addedRatings removeAllObjects];
				}
			}];
	}

	if (self.storeChanged) {
		// Sync up, outbound only
		[BCObjectManager syncCheck:SendOnly];
		self.storeChanged = NO;
	}

	[super viewWillDisappear:animated];
}
- (void)viewDidUnload
{
	self.storeData = nil;

	// Remove the notification observer for changes to StoreCategories
	[[NSNotificationCenter defaultCenter] removeObserver:self
		name:UserStoresDidChangeStoreCategories
		object:nil];

	[super viewDidUnload];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"OrderCategories"]) {
		BCCategoryOrderViewController *categoryOrderView = segue.destinationViewController;
		categoryOrderView.managedObjectContext = self.managedObjectContext;
		categoryOrderView.userStore = self.storeData;
	}
}

#pragma mark - custom properties
- (NSNumber *)storeId
{
	if (_storeId) {
		return _storeId;
	}

	if (!self.storeData) {
		return nil;
	}

	if ([self.storeData isKindOfClass:[UserStoreMO class]]) {
		_storeId = [self.storeData storeEntityId];
	}
	else {
		_storeId = [self.storeData valueForKey:kId];
	}

	return _storeId;
}

#pragma mark - local methods
- (void)didChangeStoreCategories:(NSNotification *)notification
{
	self.storeChanged = YES;
}

- (void)amenitiesFetcher:(GTMHTTPFetcher *)inFetcher
	finishedWithData:(NSData *)retrievedData
	error:(NSError *)error
{
	if (error != nil) {
		[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
			withFetcher:inFetcher response:nil];
	}
	else {
		// fetch succeeded
		NSString *response = [[NSString alloc] initWithData:retrievedData
			encoding:NSUTF8StringEncoding];
		NSArray *amenities = [response JSONValue];

		NSMutableString *amenitiesString = nil;
		for (NSDictionary *amenity in amenities) {
			if (!amenitiesString) {
				amenitiesString = [[amenity valueForKey:@"name"] mutableCopy];
			}
			else {
				[amenitiesString appendFormat:@", %@", [amenity valueForKey:@"name"]];
			}
		}

		if (!amenitiesString) {
			amenitiesString = [@"No amenities specified" mutableCopy];
		}

		// set the string into the table
		[self.sectionNames addObject:@"Amenities"];
		[self.rowLabels addObject:[NSArray arrayWithObject:@"Amenities"]];
		[self.rowValues addObject:[NSArray arrayWithObject:amenitiesString]];

		// Determine the necessary size for the amenities string
		UIFont *font = [UIFont boldSystemFontOfSize:17];
		CGSize requiredSize = [amenitiesString sizeWithFont:font];
		if (requiredSize.width >= 280) {
			self.needsAmenitiesFooter = YES;
		}

		[self.tableView reloadData];
	}

	// Chain the next request
	GTMHTTPFetcher *userRatingsFetcher = [GTMHTTPFetcher signedFetcherWithURL:
		[BCUrlFactory userEvaluationsURLForStoreId:self.storeId]];
	[userRatingsFetcher beginFetchWithDelegate:self
		didFinishSelector:@selector(userRatingsFetcher:finishedWithData:error:)];
}
- (void)userRatingsFetcher:(GTMHTTPFetcher *)inFetcher
	finishedWithData:(NSData *)retrievedData
	error:(NSError *)error
{
	if (error != nil) {
		[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
			withFetcher:inFetcher response:nil];
	}
	else {
		// fetch succeeded
		NSString *response = [[NSString alloc] initWithData:retrievedData
			encoding:NSUTF8StringEncoding];
		NSArray *userRatings = [response JSONValue];

		[self.sectionNames addObject:@"Your ratings"];
		[self.rowLabels addObject:[NSNull null]];
		[self.rowValues addObject:userRatings];

		[self.tableView reloadData];
	}

	// Chain the next request
	GTMHTTPFetcher *latestRatingsFetcher = [GTMHTTPFetcher signedFetcherWithURL:
											[BCUrlFactory latestEvaluationsURLForStoreId:self.storeId]];
	[latestRatingsFetcher beginFetchWithDelegate:self
		didFinishSelector:@selector(latestRatingsFetcher:finishedWithData:error:)];
}
- (void)latestRatingsFetcher:(GTMHTTPFetcher *)inFetcher
	finishedWithData:(NSData *)retrievedData
	error:(NSError *)error
{
	if (error != nil) {
		[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__
			withFetcher:inFetcher response:nil];
	}
	else {
		// fetch succeeded
		NSString *response = [[NSString alloc] initWithData:retrievedData
			encoding:NSUTF8StringEncoding];
		NSDictionary *latestRatings = [response JSONValue];

		[self.sectionNames addObject:@"Average Ratings"];
		[self.rowLabels addObject:[NSNull null]];
		[self.rowValues addObject:
			[NSArray arrayWithArray:[latestRatings valueForKey:@"categories"]]];
		[self.sectionNames addObject:@"Latest Ratings"];
		[self.rowLabels addObject:[NSNull null]];
		[self.rowValues addObject:
			[NSArray arrayWithArray:[latestRatings valueForKey:@"latest"]]];

		[self.tableView reloadData];
	}
}

- (void)autoSortValueChanged:(id)sender
{
	UISwitch *autoSort = sender;

	if ([self.storeData isKindOfClass:[UserStoreMO class]]) {
		UserStoreMO *userStore = self.storeData;
		userStore.autoSort = [NSNumber numberWithBool:[autoSort isOn]];

		[userStore.managedObjectContext save:nil];
		self.storeChanged = YES;
	}
}

- (void)checkinButtonTapped:(id)sender
{
	if (self.storeSelectionDelegate != nil
		&& [self.storeSelectionDelegate conformsToProtocol:@protocol(StoreSelectionDelegate)]) {
		if (![self.storeData isMemberOfClass:[UserStoreMO class]]) {
			UserStoreMO *userStore = [UserStoreMO storeByEntityId:self.storeId inMOC:self.managedObjectContext];
			if (userStore) {
				self.storeData = userStore;
			}
		}
		[self.storeSelectionDelegate view:self didSelectStore:self.storeData];
	}

	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource delegate methods
- (UIView *)tableView:(UITableView *)theTableView
	viewForFooterInSection:(NSInteger)section
{
	UIView *sectionFooterView = nil;

	if (section == Amenities) {
		if (!self.amenitiesFooterView && self.needsAmenitiesFooter) {
			self.amenitiesFooterView = [[BCMoreLessFooter alloc] initWithDelegate:self];
		}

		sectionFooterView = self.amenitiesFooterView;
	}

	return sectionFooterView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [self.sectionNames count];
}

- (NSString *)tableView:(UITableView *)tableView
	titleForHeaderInSection:(NSInteger)section
{
	id theTitle = [self.sectionNames objectAtIndex:section];

	// show no title for categories section if this is not a user store
	if ([theTitle isKindOfClass:[NSNull class]]
		|| (section == Categories
			&& ![self.storeData isKindOfClass:[UserStoreMO class]])) {
		return nil;
	}
	// show no title for the rating sections if there aren't any ratings
	else if (section == AverageRatings || section == LatestRatings
			|| section == UserRatings) {
		NSUInteger rowCount = [self.rowValues BC_countOfNestedArrayAtIndex:section];
		if (rowCount == 0) {
			return nil;
		}
	}

	return theTitle;
}
- (NSInteger)tableView:(UITableView *)tableView
	numberOfRowsInSection:(NSInteger)section
{
	NSUInteger rowCount = 0;

	switch (section) {
		case Categories:
			if (![self.storeData isKindOfClass:[UserStoreMO class]]) {
				rowCount = 0;
			}
			else {
				rowCount = [self.rowLabels BC_countOfNestedArrayAtIndex:section];
			}
			break;
		case UserRatings:
		case AverageRatings:
		case LatestRatings:
			rowCount = [self.rowValues BC_countOfNestedArrayAtIndex:section];
			break;
		case Map:
		case Details:
		case Amenities:
		default:
			rowCount = [self.rowLabels BC_countOfNestedArrayAtIndex:section];
			break;
	}
	return rowCount;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
	cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *labelValueCellId = @"LabelValueCellId";
	static NSString *singleLineAccessoryCellId = @"SingleLineAccessoryCellId";
	static NSString *autoSortCellId = @"AutoSortCellId";
	static NSString *mapCellId = @"MapCellId";
	static NSString *latestRatingCellId = @"LatestRatingCellId";
	static NSString *averageRatingCellId = @"AverageRatingCellId";
	static NSString *userRatingCellId = @"UserRatingCellId";
	static NSString *amenitiesCellId = @"AmenitiesCellId";

	UITableViewCell *cell = nil;

	switch ([indexPath section]) {
		case Map:
			cell = [self.tableView dequeueReusableCellWithIdentifier:mapCellId];
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
						reuseIdentifier:mapCellId];
				cell.selectionStyle = UITableViewCellSelectionStyleNone;

				if (!self.storeMapView) {
					self.storeMapView = [[MKMapView alloc]
						initWithFrame:CGRectMake(0, 0, 300, 200)];
					self.storeMapView.tag = 8004;
					self.storeMapView.delegate = self;
					self.storeMapView.mapType = MKMapTypeStandard;
					[self.storeMapView setRegion:
						MKCoordinateRegionMakeWithDistance([self.storeMapLocation coordinate], 1500, 1000)];
					[self.storeMapView addAnnotation:[self storeMapLocation]];
				}
				[cell.contentView addSubview:self.storeMapView];

				if (self.showUserLocation) {
					// Not viewing user stores, show the user's current location, if available
					[self.storeMapView setShowsUserLocation:YES];

					if ([[self.storeMapView userLocation] location]) {
						[self.storeMapView setCenterCoordinate:[[[self.storeMapView userLocation]
							location] coordinate]];
						// TODO Set the region
					}
				}

				// Create a new UIView to eliminate "border" drawing around each row/cell
				UIView *transparentView = [[UIView alloc] initWithFrame:CGRectZero];
				transparentView.backgroundColor = [UIColor clearColor];
				cell.backgroundView = transparentView;
			}
			break;
		case Categories:
			// Row zero allows manually adjusting categories
			if ([indexPath row] == 0) {
				cell = [self.tableView dequeueReusableCellWithIdentifier:singleLineAccessoryCellId];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
							reuseIdentifier:singleLineAccessoryCellId];
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				}
			}
			// Row one allows toggling the auto sort feature
			else {
				cell = [self.tableView dequeueReusableCellWithIdentifier:autoSortCellId];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
							reuseIdentifier:autoSortCellId];
					UISwitch *autoSort = [[UISwitch alloc]
						initWithFrame:CGRectMake(190, 8, 94, 27)];
					[autoSort setOn:YES animated:NO];
					[autoSort addTarget:self action:@selector(autoSortValueChanged:)
						forControlEvents:UIControlEventValueChanged];
					cell.accessoryView = autoSort;
				}
			}
			break;
		case UserRatings:
		{
			AverageRatingCell *urCell = (AverageRatingCell *)[self.tableView
				dequeueReusableCellWithIdentifier:userRatingCellId];
			if (urCell == nil) {
				[[NSBundle mainBundle] loadNibNamed:@"StoreDetailCustomCells" owner:self options:nil];
				urCell = self.userRatingCell;
				self.userRatingCell = nil;

				StarRatingControl *ratingControl = [[StarRatingControl alloc]
													initWithFrame:CGRectMake(140, 6, 160, 32)
													animateStars:YES
													emptyStarImage:[UIImage imageNamed:@"RatingStar-Empty.png"]
													fullStarImage:[UIImage imageNamed:@"RatingStar-Gold.png"]
													delegate:self];
				ratingControl.rating = 0;
				ratingControl.tag = [indexPath row];
				[urCell.contentView addSubview:ratingControl];
				urCell.ratingControl = ratingControl;
			}
			cell = urCell;
			break;
		}
		case AverageRatings:
		{
			AverageRatingCell *arCell = (AverageRatingCell *)[self.tableView
				dequeueReusableCellWithIdentifier:averageRatingCellId];
			if (arCell == nil) {
				[[NSBundle mainBundle] loadNibNamed:@"StoreDetailCustomCells" owner:self options:nil];
				arCell = self.averageRatingCell;
				self.averageRatingCell = nil;

				StarRatingControl *ratingControl = [[StarRatingControl alloc]
													initWithFrame:CGRectMake(220, 6, 100, 25)];
				ratingControl.animate = NO;
				ratingControl.userInteractionEnabled = NO;
				ratingControl.emptyStar = [UIImage imageNamed:@"RatingStar-Empty.png"];
				ratingControl.fullStar = [UIImage imageNamed:@"RatingStar-Green.png"];
				[arCell.contentView addSubview:ratingControl];
				arCell.ratingControl = ratingControl;
			}
			cell = arCell;
			break;
		}
		case LatestRatings:
		{
			LatestRatingCell *lrCell = (LatestRatingCell *)[self.tableView
															dequeueReusableCellWithIdentifier:latestRatingCellId];
			if (lrCell == nil) {
				[[NSBundle mainBundle] loadNibNamed:@"StoreDetailCustomCells" owner:self options:nil];
				lrCell = self.latestRatingCell;
				self.latestRatingCell = nil;

				StarRatingControl *ratingControl = [[StarRatingControl alloc]
													initWithFrame:CGRectMake(220, 6, 100, 25)];
				ratingControl.animate = NO;
				ratingControl.userInteractionEnabled = NO;
				ratingControl.emptyStar = [UIImage imageNamed:@"RatingStar-Empty.png"];
				ratingControl.fullStar = [UIImage imageNamed:@"RatingStar-Gold.png"];
				[lrCell.contentView addSubview:ratingControl];
				lrCell.ratingControl = ratingControl;
			}
			cell = lrCell;
			break;
		}
		case Amenities:
			cell = [self.tableView dequeueReusableCellWithIdentifier:amenitiesCellId];
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
						reuseIdentifier:amenitiesCellId];
				cell.selectionStyle = UITableViewCellSelectionStyleNone;
			}
			if ([self.amenitiesFooterView isExpanded]) {
				cell.textLabel.numberOfLines = 0;
			}
			else {
				cell.textLabel.numberOfLines = 2;
			}
			break;
		case Details:
		default:
			cell = [self.tableView dequeueReusableCellWithIdentifier:labelValueCellId];
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
						reuseIdentifier:labelValueCellId];
				cell.selectionStyle = UITableViewCellSelectionStyleNone;
			}
			break;
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	UIColor *fontColor = nil;
	NSUInteger section = [indexPath section];

	switch (section) {
		case Map:
		default:
			break;
		case Categories:
		{
			if (([indexPath row] == 1)
				&& [self.storeData isKindOfClass:[UserStoreMO class]]) {
				// This is the auto sort row, set the control based upon the user store value
				UserStoreMO *userStore = self.storeData;
				UISwitch *autoSort = (UISwitch *)[cell accessoryView];
				[autoSort setOn:[userStore.autoSort boolValue] animated:NO];
			}
			cell.textLabel.text = [self.rowLabels BC_nestedObjectAtIndexPath:indexPath];
			id <GenericValueDisplay, NSObject> rowValue =
				[self.rowValues BC_nestedObjectAtIndexPath:indexPath];
			cell.detailTextLabel.text = [rowValue genericValueDisplay];
			break;
		}
		case Details:
		{
			if ([indexPath row] == 1) {
				fontColor = [UIColor blueColor];
			}
			cell.textLabel.text = [self.rowLabels BC_nestedObjectAtIndexPath:indexPath];
			id <GenericValueDisplay, NSObject> rowValue =
				[self.rowValues BC_nestedObjectAtIndexPath:indexPath];
			cell.detailTextLabel.text = [rowValue genericValueDisplay];
			if (fontColor) {
				cell.detailTextLabel.textColor = fontColor;
			}
			break;
		}
		case Amenities:
		{
			cell.textLabel.text = [self.rowLabels BC_nestedObjectAtIndexPath:indexPath];
			id <GenericValueDisplay, NSObject> rowValue =
				[self.rowValues BC_nestedObjectAtIndexPath:indexPath];
			cell.textLabel.text = [rowValue genericValueDisplay];
			if (fontColor) {
				cell.textLabel.textColor = fontColor;
			}
			break;
		}
		case UserRatings:
		{
			AverageRatingCell *arCell = (AverageRatingCell *)cell;
			arCell.textLabel.text = @"";
			NSDictionary *ratingDict = (NSDictionary *)
				[self.rowValues BC_nestedObjectAtIndexPath:indexPath];
			NSNumber *rating = [[ratingDict objectForKey:kRating]
								numberValueWithNumberStyle:NSNumberFormatterDecimalStyle];
			if ([rating compare:[NSNumber numberWithInteger:0]] == NSOrderedDescending) {
				[arCell.ratingControl setRating:[rating integerValue]];
			}
			else {
				[arCell.ratingControl setRating:0];
			}
			if ((NSNull *)[ratingDict objectForKey:kEvaluationCategory] != [NSNull null]) {
				arCell.categoryLabel.text = [ratingDict valueForKey:kEvaluationCategory];
			}
			if ((NSNull *)[ratingDict objectForKey:kEvaluationComment] != [NSNull null]) {
				arCell.ratingCountLabel.text = [ratingDict valueForKey:kEvaluationComment];
			}
			else {
				arCell.ratingCountLabel.text = @"Click here to comment";
			}
			break;
		}
		case AverageRatings:
		{
			AverageRatingCell *arCell = (AverageRatingCell *)cell;
			arCell.textLabel.text = @"";
			NSDictionary *ratingDict = (NSDictionary *)
				[self.rowValues BC_nestedObjectAtIndexPath:indexPath];
			if ((NSNull *)[ratingDict objectForKey:kRating] != [NSNull null]) {
				[arCell.ratingControl setRating:[[ratingDict valueForKey:kRating] integerValue]];
			}
			if ((NSNull *)[ratingDict objectForKey:kEvaluationCategory] != [NSNull null]) {
				arCell.categoryLabel.text = [ratingDict valueForKey:kEvaluationCategory];
			}
			if ((NSNull *)[ratingDict objectForKey:kRatingCount] != [NSNull null]) {
				arCell.ratingCountLabel.text =
					[NSString stringWithFormat:@"%@ users",
					[ratingDict valueForKey:kRatingCount]];
			}
			break;
		}
		case LatestRatings:
		{
			LatestRatingCell *lrCell = (LatestRatingCell *)cell;
			lrCell.textLabel.text = @"";
			NSDictionary *ratingDict = (NSDictionary *)
				[self.rowValues BC_nestedObjectAtIndexPath:indexPath];
			if ((NSNull *)[ratingDict objectForKey:kRating] != [NSNull null]) {
				[lrCell.ratingControl setRating:[[ratingDict valueForKey:kRating] integerValue]];
			}
			if ((NSNull *)[ratingDict objectForKey:kEvaluationComment] != [NSNull null]) {
				lrCell.commentLabel.text = [ratingDict valueForKey:kEvaluationComment];
			}
			if ((NSNull *)[ratingDict objectForKey:kEvaluationCategory] != [NSNull null]) {
				lrCell.categoryLabel.text = [ratingDict valueForKey:kEvaluationCategory];
			}
			if ((NSNull *)[ratingDict objectForKey:kEvaluationUserName] != [NSNull null]) {
				lrCell.userDateLabel.text = [NSString stringWithFormat:@"reviewed by %@, %@",
					[ratingDict valueForKey:kEvaluationUserName],
					[NSDateFormatter relativeStringFromDate:[NSDate dateWithTimeIntervalSince1970:[[ratingDict objectForKey:kCreatedOn] doubleValue]]]];
			}
			break;
		}
	}
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView
	heightForFooterInSection:(NSInteger)section
{
	if ((section == Amenities) && [self needsAmenitiesFooter]) {
		return 40.0;
	}
	else {
		return 0.0;
	}
}

- (CGFloat)tableView:(UITableView *)tableView
	heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 0.0f;

	switch ([indexPath section]) {
		case Map:
			height = 200.0f;
			break;
		case LatestRatings:
			height = 65.0f;
			break;
		case UserRatings:
		case AverageRatings:
		case Categories:
			height = 44.0f;
			break;
		case Amenities:
			if ([self.amenitiesFooterView isExpanded]) {
				id <GenericValueDisplay, NSObject> rowValue =
					[self.rowValues BC_nestedObjectAtIndexPath:indexPath];
				NSString *amenitiesText = [rowValue genericValueDisplay];
				UIFont *font = [UIFont boldSystemFontOfSize:17];
				CGSize requiredSize = [amenitiesText sizeWithFont:font
					constrainedToSize:CGSizeMake(280, 10000)];
				height = requiredSize.height + 10;
			}
			else {
				height = 55.0f;
			}
			break;
		case Details:
		default:
			height = 55.0f;
			break;
	}

	return height;
}
- (NSIndexPath *)tableView:(UITableView *)tableView
	willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSIndexPath *selPath = nil;

	switch ([indexPath section]) {
		case Categories:
			if (([indexPath row] == 0) && [self.storeData isKindOfClass:[UserStoreMO class]]) {
				selPath = indexPath;
			}
			break;
		case Details:
			// Sorta hackey, but need to be able to click the phone number
			// TODO check the text field to ensure it isn't empty
			if ([indexPath row] == 1) {
				selPath = indexPath;
			}
		case Map:
		case Amenities:
		case UserRatings:
		case AverageRatings:
		case LatestRatings:
		default:
			break;
	}

	return selPath;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	switch ([indexPath section]) {
		case Categories:
			// Only capture row zero, as row one is the autosort switch
			if ([indexPath row] == 0) {
				[self performSegueWithIdentifier:@"OrderCategories" sender:[tableView cellForRowAtIndexPath:indexPath]];
			}
			break;
		case Details:
			if ([indexPath row] == 1) {
				UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
				NSString *urlString = [NSString stringWithFormat:@"tel://%@",
					[cell.detailTextLabel.text BC_escapeSpecialCharacters]];
				[[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
			}
			break;
		case Map:
		case Amenities:
		case UserRatings:
		case AverageRatings:
		case LatestRatings:
		default:
			break;
	}
}

#pragma mark - StarRatingControlDelegate
- (void)starRatingControl:(StarRatingControl *)control didUpdateRating:(NSUInteger)rating fromRating:(NSUInteger)fromRating
{
	// label.text = [NSString stringWithFormat:@"rating: %d", control.rating];
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:control.tag
		inSection:UserRatings];
	NSDictionary *ratingDict = (NSDictionary *)
		[self.rowValues BC_nestedObjectAtIndexPath:indexPath];

	DDLogInfo(@"%s %@", __FUNCTION__, ratingDict);

	// TODO block search for index of item matching so we don't duplicate
	NSMutableDictionary *newRatingDict = [[NSMutableDictionary alloc] init];
	[newRatingDict setValue:[NSNumber numberWithInteger:4] forKey:@"eval_item_type_id"];
	[newRatingDict setValue:self.storeId forKey:@"eval_item_id"];
	[newRatingDict setValue:[ratingDict valueForKey:kEvaluationCategory]
		forKey:kEvaluationCategory];
	[newRatingDict setValue:[NSNumber numberWithInteger:rating]
		forKey:kRating];
	// TODO give the user a way to type in the comment
	// [newRatingDict setValue: forKey:@"eval_comment"];
	[self.addedRatings addObject:newRatingDict];
}

#pragma mark - MKMapView delegate
/*
   // NOTE we will need this when we want to customize the pins, for instance
   // to show a BuyerCompass logo on the BuyerCompass client locations
   - (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
   {
    MKPinAnnotationView *annView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"store"];
        if (!annView) {
                annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"store"];
        }
    annView.pinColor = MKPinAnnotationColorGreen;
    annView.animatesDrop = TRUE;
    annView.canShowCallout = YES;

    return annView;
   }
 */
- (void)mapView:(MKMapView *)mapView
	didUpdateUserLocation:(MKUserLocation *)userLocation
{
	if (self.showUserLocation && [[mapView userLocation] location]) {

		double distCalc = [[userLocation location] distanceFromLocation:[self.storeMapLocation location]];

		// Not viewing user stores, show the user's current location, if available
		[mapView setRegion:MKCoordinateRegionMakeWithDistance([[[mapView userLocation]
																location] coordinate], 1.5 * distCalc, distCalc)];
	}
}

#pragma mark - BCMoreLessFooter
- (void)moreLessFooterTapped:(BCMoreLessFooter *)footer
{
	[self.tableView reloadData];
	[self.tableView scrollToRowAtIndexPath:
		[NSIndexPath indexPathForRow:0 inSection:Amenities]
			atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

@end
