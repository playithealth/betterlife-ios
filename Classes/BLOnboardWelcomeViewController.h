//
//  BLOnboardWelcomeViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 3/27/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString * const kOBStoryboardId;
extern NSString * const kOBViewProperties;

@protocol BLOnboardDelegate <NSObject>
- (void)nextView;
@end

@interface BLOnboardWelcomeViewController : UIViewController <BLOnboardDelegate>
+ (BOOL)canUseOnboarding;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *viewStack;
@end
