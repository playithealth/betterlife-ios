//
//  UIViewController+BLSidebarView.m
//  BuyerCompass
//
//  Created by Greg Goodrich on 9/9/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "UIViewController+BLSidebarView.h"

#import "BCAppDelegate.h"

@implementation UIViewController (BLSidebarView)

- (void)BL_implementSidebar
{
	// Figure out if we're on a root view controller, using a local var for cleanliness
	BOOL isRootVC = [[self.navigationController.viewControllers objectAtIndex:0] isEqual:self];

	// If we're the root view controller, show the sidebar button instead of the back button
	if (isRootVC) {
		self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
			initWithImage:[UIImage imageNamed:@"sidebar-button"]
			style:UIBarButtonItemStylePlain
			target:self action:@selector(viewSidebar:)];
	}
}

- (void)viewSidebar:(id)sender
{
	BCAppDelegate *appDelegate =
		(BCAppDelegate *)[[UIApplication sharedApplication] delegate];

	[appDelegate showSidebar:YES disableClosing:NO];
}

@end
