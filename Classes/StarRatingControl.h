//
//  StarRatingControl.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/23/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StarRatingControlDelegate;

@interface StarRatingControl : UIControl 

@property (strong) id<StarRatingControlDelegate> delegate;
@property (strong) UIImage *emptyStar;
@property (strong) UIImage *fullStar;
@property (assign) NSUInteger rating;
@property (assign) BOOL animate;
@property (assign) int numberOfStars;
@property (assign) UIEdgeInsets edgeInsets;

- (id)initWithFrame:(CGRect)inFrame
	animateStars:(BOOL)inAnimate
	emptyStarImage:(UIImage *)inEmptyStar
	fullStarImage:(UIImage *)inFullStar
	delegate:(id<StarRatingControlDelegate>)delegate;
	
- (void)updateStars;

@end

@protocol StarRatingControlDelegate <NSObject>
- (void)starRatingControl:(StarRatingControl *)control didUpdateRating:(NSUInteger)rating fromRating:(NSUInteger)oldRating;
@end
