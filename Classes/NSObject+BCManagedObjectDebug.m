//
//  NSObject+BCManagedObjectDebug.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 12/14/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "NSObject+BCManagedObjectDebug.h"

#if DEBUG
#import <objc/runtime.h>
#import <Foundation/Foundation.h>

@implementation NSObject (BCManagedObjectDebug)
+ (NSMutableArray *)BCProperties
{
    NSMutableArray *properties = nil;
    
    if ([self superclass] != [NSManagedObject class])
        properties = [[self superclass] BCProperties];
    else
        properties = [NSMutableArray array];
    
    
    unsigned int propCount;
    objc_property_t *propList = class_copyPropertyList([self class], &propCount);
    int i;
    
    for (i=0; i < propCount; i++)
    {
        objc_property_t oneProp = propList[i];
        NSString *propName = [NSString stringWithUTF8String:property_getName(oneProp)];
        if (![properties containsObject:propName])
            [properties addObject:propName];
    }
    return properties;
}
@end

@implementation NSManagedObject(BCManagedObjectDebug)
- (NSString *)description
{
    NSArray *properties = [[self class] BCProperties];
    NSMutableString *ret = [NSMutableString stringWithFormat:@"%@:", [self class]];
    NSDictionary *myAttributes = [[self entity] attributesByName];
    
    for (NSString *oneProperty in properties)
    {
        NSAttributeDescription *oneAttribute = [myAttributes valueForKey:oneProperty];
        if (oneAttribute != nil) // If not, it's a relationship or fetched property
        {
            id value = [self valueForKey:oneProperty];
            [ret appendFormat:@"\n\t%@ = %@", oneProperty, value];
        }
		
    }
    return ret;
}

@end
#endif
