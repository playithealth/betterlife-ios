//
//  TrainingAssessmentViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/22/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "BCViewController.h"

#import "TrainingAssessmentMO.h"

@protocol TrainingAssessmentViewDelegate;

@interface TrainingAssessmentViewController : BCViewController

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) TrainingAssessmentMO *trainingAssessment;
@property (assign, nonatomic) id<TrainingAssessmentViewDelegate> delegate;

@end

@protocol TrainingAssessmentViewDelegate <NSObject>
- (void)trainingAssessmentView:(TrainingAssessmentViewController*)strengthView didUpdateLog:(TrainingAssessmentMO *)log;
@end
