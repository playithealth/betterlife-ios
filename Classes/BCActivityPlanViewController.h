//
//  BCActivityPlanViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 2/7/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ActivityPlanMO;
@protocol BCDetailViewDelegate;

@interface BCActivityPlanViewController : UITableViewController

@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext; ///< This is just retaining the MOC, in case it is a scratch context
@property (strong, nonatomic) ActivityPlanMO *plan;

@end
