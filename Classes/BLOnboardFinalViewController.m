//
//  BLOnboardFinalViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 3/31/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLOnboardFinalViewController.h"

#import "BCAppDelegate.h"
#import "BCObjectManager.h"
#import "UserProfileMO.h"

@interface BLOnboardFinalViewController ()

@end

@implementation BLOnboardFinalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	NSInteger viewCount = [[self.navigationController viewControllers] count] - 1;
	self.title = [NSString stringWithFormat:@"Welcome: %ld of %ld", (long)viewCount, (long)viewCount];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - responders
- (IBAction)doneSelected:(id)sender {
	// Mark the onboarding as complete
	UserProfileMO *userProfileMO = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];
	userProfileMO.onboardingComplete = @YES;
	userProfileMO.status = kStatusPut;

	[self.managedObjectContext BL_save];

	[BCObjectManager syncCheck:SendOnly];

	// Now send the user to the initial view controller via the app delegate
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate gotoInitialViewController];
}
@end
