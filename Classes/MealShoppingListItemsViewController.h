//
//  MealShoppingListItemsViewController.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 5/22/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCViewController.h"
#import "RecipeMO.h"
#import "ProductSearchView.h"

@interface MealShoppingListItemsViewController : UITableViewController

@property (strong, nonatomic) RecipeMO *recipe;

@end
