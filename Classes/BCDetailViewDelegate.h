//
//  BCDetailViewDelegate.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 2/7/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#ifndef BuyerCompass_BCDetailViewDelegate_h
#define BuyerCompass_BCDetailViewDelegate_h

#import <Foundation/Foundation.h>

@protocol BCDetailViewDelegate <NSObject>
@required
- (void)view:(id)viewController didUpdateObject:(id)object;
@end

#endif