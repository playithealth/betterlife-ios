//
//  BCActivitySearchRecentViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 11/18/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BCActivitySearchDataModel;

@interface BCActivitySearchRecentViewController : UITableViewController
@property (strong, nonatomic) BCActivitySearchDataModel *dataModel;
@end
