//
//  BCAccountSettingsViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/12/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCAccountSettingsViewController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
