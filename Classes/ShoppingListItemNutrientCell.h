//
//  NutrientCell.h
//  BettrLife
//
//  Created by Sef Tarbell on 09/06/2012.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingListItemNutrientCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;

@end
