//
//  BLHeaderFooterMultiline.h
//  BettrLife
//
//  Created by Greg Goodrich on 3/22/14.
//  Copyright (c) 2014 BettrLife Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLHeaderFooterMultiline : UITableViewHeaderFooterView
@property (strong, nonatomic) UILabel *multilineLabel;
@property (strong, nonatomic) UILabel *normalLabel;
@property (strong, nonatomic) UIImageView *headerImageView;
@end
