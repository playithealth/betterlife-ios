//
//  BLLoggingWorkoutActivityCell.m
//  BuyerCompass
//
//  Created by Greg Goodrich on 2/11/15.
//  Copyright (c) 2015 BettrLife Corporation. All rights reserved.
//

#import "BLLoggingWorkoutActivityCell.h"

@implementation BLLoggingWorkoutActivityCell

- (void)awakeFromNib {
    // Initialization code
	[self.actionView addTarget:self action:@selector(actionViewTapped:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)actionViewTapped:(id)control
{
	// Notifiy the delegate that we got a tap
	if (self.delegate && [self.delegate respondsToSelector:@selector(actionViewTapped:inCell:)]) {
		[self.delegate actionViewTapped:control inCell:self];
	}
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
