//
//  BLNutritionStatusViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 4/16/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLNutritionStatusViewController.h"

#import "BLLoggingDataModel.h"
#import "BLLogTimeTabBar.h"
#import "NSNumber+BettrLife.h"
#import "NSString+BCAdditions.h"
#import "NutrientMO.h"
#import "NutritionMO.h"
#import "UIColor+Additions.h"

#pragma mark - Custom Cells
@interface NutrientGoalCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nutrientLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UIView *progressLine;
@property (weak, nonatomic) IBOutlet UIView *goalLine;
@property (weak, nonatomic) IBOutlet UIImageView *minArrow;
@property (weak, nonatomic) IBOutlet UIImageView *maxArrow;

@property (assign, nonatomic) double currentValue;
@property (strong, nonatomic) NSNumber *minGoalValue;
@property (strong, nonatomic) NSNumber *maxGoalValue;

@property (strong, nonatomic) NSLayoutConstraint *progressLineWidthConstraint;
@property (strong, nonatomic) NSLayoutConstraint *goalLineWidthConstraint;
@property (strong, nonatomic) NSLayoutConstraint *goalLineRightMarginConstraint;
@end
@implementation NutrientGoalCell
- (id)initWithCoder:(NSCoder *)decoder
{
	self = [super initWithCoder:decoder];
    if (self) {
        // Custom initialization
	}
	return self;
}

- (void)prepareForReuse
{
}

// Note: The goal and progress bars should have IB constraints on their widths as well as a right/left superview constraint respectively
// This method will adjust the width constraints, as well as the right margin constraint for the goal, based upon values set into the cell
- (void)updateConstraints {
	if (!self.progressLineWidthConstraint) {
		// See if there is a constraint on the progressLine for the width, if so, grab its pointer
		NSArray *constraints = [self.progressLine constraints];
		for (NSLayoutConstraint *constraint in constraints) {
			if ([constraint.firstItem isEqual:self.progressLine] && (constraint.firstAttribute == NSLayoutAttributeWidth)) {
				self.progressLineWidthConstraint = constraint;
				break;
			}
		}
	}
	if (!self.goalLineWidthConstraint) {
		// See if there is a constraint on the goalLine for the 'left margin' to the superview, if so, grab its pointer
		NSArray *constraints = [self.goalLine constraints];
		for (NSLayoutConstraint *constraint in constraints) {
			if ([constraint.firstItem isEqual:self.goalLine] && (constraint.firstAttribute == NSLayoutAttributeWidth)) {
				
				self.goalLineWidthConstraint = constraint;
				break;
			}
		}
	}
	if (!self.goalLineRightMarginConstraint) {
		// See if there is a constraint on the goalLine for the 'right margin' to the superview, if so, grab its pointer
		NSArray *constraints = [self.contentView constraints];
		for (NSLayoutConstraint *constraint in constraints) {
			if ([constraint.firstItem isEqual:self.contentView] && [constraint.secondItem isEqual:self.goalLine]
					&& (constraint.secondAttribute == NSLayoutAttributeTrailing)) {
				
				self.goalLineRightMarginConstraint = constraint;
				break;
			}
		}
	}

	// Update the constraints
	double margin = 15; // The margin between the cell contents and the edge of the cell's superview (the table)
	double pxMin = margin;
	double pxMax = self.bounds.size.width; // No margin on the right hand side at this time
	double rangeMin = 0.0; // For now, the left edge of the bar is always zero

	// The right edge of the bar is the greater of goal max (or goal min, if no goal max is set) or current
	double rangeMax = MAX(self.currentValue, (self.maxGoalValue ? [self.maxGoalValue doubleValue] : [self.minGoalValue doubleValue]));
	if (rangeMax == [self.maxGoalValue doubleValue]) {
		pxMax = self.bounds.size.width - margin + self.maxArrow.frame.size.width;
	}
	else if (rangeMax == [self.minGoalValue doubleValue]) {
		pxMax = self.bounds.size.width - margin;
	}

	// Ensure we have a ratio, if not, just default it to 1 for now, otherwise we get NaN errors from dividing by zero
	double ratio = ((rangeMax - rangeMin) / (pxMax - pxMin) ?: 1);

	double progressValue = round(self.currentValue / ratio);

	self.progressLineWidthConstraint.constant = progressValue;

	self.goalLine.hidden = NO;
	self.minArrow.image = [UIImage imageNamed:
		(self.currentValue >= [self.minGoalValue doubleValue] ? @"NutritionGraphArrowMinWhite" : @"NutritionGraphArrowMinGreen")];
	self.maxArrow.image = [UIImage imageNamed:
		(self.currentValue >= [self.maxGoalValue doubleValue] ? @"NutritionGraphArrowMaxWhite" : @"NutritionGraphArrowMaxGreen")];

	if (self.minGoalValue) {
		self.minArrow.hidden = NO;
		double goalLineWidth;
		if (self.maxGoalValue) {
			goalLineWidth = round(([self.maxGoalValue doubleValue] - [self.minGoalValue doubleValue]) / ratio);
		}
		else if (self.currentValue > [self.minGoalValue doubleValue]) {
			goalLineWidth = round((self.currentValue - [self.minGoalValue doubleValue]) / ratio);
			// Set the right margin constraint to the right edge
			self.goalLineRightMarginConstraint.constant = 0;
		}
		else {
			goalLineWidth = self.minArrow.frame.size.width + margin;
			// Set the right margin constraint to the right edge, since there is no max goal
			self.goalLineRightMarginConstraint.constant = 0;
			self.goalLine.hidden = NO;
		}
		self.goalLineWidthConstraint.constant = goalLineWidth;
	}
	else {
		// Hide the minArrow
		self.minArrow.hidden = YES;
	}
	if (self.maxGoalValue) {
		self.maxArrow.hidden = NO;
		double maxGoalValue = round([self.maxGoalValue doubleValue] / ratio) + pxMin;
		double maxGoalRightMargin = self.bounds.size.width - maxGoalValue;
		self.goalLineRightMarginConstraint.constant = maxGoalRightMargin;
		if (!self.minGoalValue) {
			self.goalLineWidthConstraint.constant = maxGoalValue - pxMin;
		}
	}
	else {
		// Hide the maxArrow
		self.maxArrow.hidden = YES;
	}

	[super updateConstraints];
}

@end

#pragma mark - implementation
@interface BLNutritionStatusViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet BLLogTimeTabBar *logTimeTabBar;
@property (weak, nonatomic) IBOutlet UIView *smileyView;
@property (weak, nonatomic) IBOutlet UILabel *smileyTitle;
@property (weak, nonatomic) IBOutlet UILabel *subText;
@property (weak, nonatomic) IBOutlet UIImageView *smileyImage;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@property (assign, nonatomic) LTTBActiveTab activeTab;
@end

@implementation BLNutritionStatusViewController

#pragma mark - life cycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
		self.activeTab = ATTotal;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	BLSmiley currentCompliance = [self.dataModel currentCompliance];
	self.doneButton.layer.borderWidth = 1.0f;
	self.doneButton.layer.borderColor = [[UIColor whiteColor] CGColor];
	self.doneButton.titleLabel.textColor = [UIColor whiteColor];
	self.doneButton.layer.cornerRadius = 8.0f;

	switch (currentCompliance) {
		case SmileyHappy:
			self.smileyImage.image = [UIImage imageNamed:@"SmileyHappyLarge"];
			self.smileyView.backgroundColor = [UIColor BL_happySmileyColor];
			break;
		case SmileyMeh:
			self.smileyImage.image = [UIImage imageNamed:@"SmileyMehLarge"];
			self.smileyView.backgroundColor = [UIColor BL_mehSmileyColor];
			self.smileyTitle.textColor = [UIColor BL_mehTextColor];
			self.doneButton.titleLabel.textColor = [UIColor BL_mehTextColor];
			self.doneButton.layer.borderColor = [[UIColor BL_mehTextColor] CGColor];
			self.subText.textColor = [UIColor BL_mehTextColor];
			break;
		case SmileySad:
			self.smileyImage.image = [UIImage imageNamed:@"SmileySadLarge"];
			self.smileyView.backgroundColor = [UIColor BL_sadSmileyColor];
			break;
	}

	if (self.dataModel.dayOffset >= 0) {
		// Default the smiley title
		self.smileyTitle.text = @"Remember to eat healthy!";

		if (self.dataModel.latestLoggedTime == LogTimeNone) {
			// Nothing logged today
			self.subText.Text = @"As you log meals your nutrition will be tracked below.";
		}
		else if (self.dataModel.goalInfo.goalSource == GoalsDefaults) {
			// No goals set up
			self.subText.Text = @"Setting nutrition goals will allow you see your progress through the day.";
		}
		else {
			NSString *planPhrase = @"";
			if (self.dataModel.goalInfo.goalSource == GoalsMealPlan) {
				planPhrase = @"meal plan ";
			}
			switch (currentCompliance) {
				case SmileyHappy:
					self.smileyTitle.text = @"You’re doing great!";
					self.subText.Text = [NSString stringWithFormat:@"You are on track to meet your %@nutrition goals today.", planPhrase];
					break;
				case SmileyMeh:
					self.smileyTitle.text = @"You're doing so-so.";
					self.subText.Text = [NSString stringWithFormat:@"You are not completely on track to meet your %@nutrition goals today.",
						planPhrase];
					break;
				case SmileySad:
					self.smileyTitle.text = @"Having some problems?";
					self.subText.Text = [NSString stringWithFormat:@"You are not on track to meet your %@nutrition goals today.", planPhrase];
					break;
			}
		}
	}
	else {
		if (self.dataModel.latestLoggedTime == LogTimeNone) {
			// Nothing logged today
			self.smileyTitle.text = @"You ate nothing?";
			self.subText.Text = @"You can always go back and log meals for prior days.";
		}
		else if (self.dataModel.goalInfo.goalSource == GoalsDefaults) {
			// No goals set up
			self.smileyTitle.text = @"Did you eat healthy?";
			self.subText.Text = @"Setting nutrition goals will allow you see your progress through the day.";
		}
		else {
			NSString *planPhrase = @"goals";
			if (self.dataModel.goalInfo.goalSource == GoalsMealPlan) {
				planPhrase = @"meal plan";
			}
			switch (currentCompliance) {
				case SmileyHappy:
					self.smileyTitle.text = @"You did great!";
					self.subText.Text = [NSString stringWithFormat:@"Your nutrients are all in the green zone based on your %@.", planPhrase];
					break;
				case SmileyMeh:
					self.smileyTitle.text = @"You did so-so.";
					self.subText.Text = [NSString stringWithFormat:@"Only some of your nutrients are in the green zone based on your %@.",
						planPhrase];
					break;
				case SmileySad:
					self.smileyTitle.text = @"Had some problems?";
					self.subText.Text = [NSString stringWithFormat:@"Many of your nutrients are not in the green zone based on your %@.",
						planPhrase];
					break;
			}
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.dataModel.nutrients count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;

	NutrientMO *nutrient = [self.dataModel.nutrients objectAtIndex:indexPath.row];

	// Adjust based upon which logtime is selected
	if (self.activeTab == ATTotal) {
		NSNumber *currentValue = [self.dataModel.totalNutrition valueForKey:nutrient.name];
		NSString *currentValueString = [NSString stringWithFormat:@"%.*f", [nutrient.precision intValue], [currentValue doubleValue]];

		if ([self.dataModel.goalInfo.goalNutrients containsObject:nutrient.name]) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:@"Nutrient Goal"];
			NutrientGoalCell *ngCell = (NutrientGoalCell *)cell;
			ngCell.nutrientLabel.text = nutrient.displayName;
			NSNumber *minValue = [self.dataModel.goalInfo.minNutrition valueForKey:nutrient.name];
			NSNumber *maxValue = [self.dataModel.goalInfo.maxNutrition valueForKey:nutrient.name];
			
			//if ([nutrient.name isEqualToString:NutritionMOAttributes.calories] && self.dataModel.goalInfo.bmrGoal) {
			if ([nutrient.name isEqualToString:NutritionMOAttributes.calories] && self.dataModel.goalInfo.usingBMR) {
				if (self.dataModel.goalInfo.isWeightLoss) {
					maxValue = @([maxValue doubleValue] + self.dataModel.goalInfo.activityCalories);
					minValue = nil;
				}
				else {
					minValue = @([minValue doubleValue] + self.dataModel.goalInfo.activityCalories);
					maxValue = nil;
				}
				/*
				// TODO Should this be using the BMR goal as a MIN if they're trying to gain weight?
				minValue = nil;
				maxValue = self.dataModel.goalInfo.bmrGoal;
				*/
			}
			NSString *valueText = nil;
			if (minValue && maxValue) {
				valueText = [NSString stringWithFormat:@"%@/%@-%@%@", currentValueString, minValue, maxValue, (nutrient.units ?: @"")];
			}
			else if (minValue) {
				valueText = [NSString stringWithFormat:@"%@/>%@%@", currentValueString, minValue, (nutrient.units ?: @"")];
			}
			else if (maxValue) {
				valueText = [NSString stringWithFormat:@"%@/<%@%@", currentValueString, maxValue, (nutrient.units ?: @"")];
			}
			else {
				// No values
				valueText = currentValueString;
			}

			// Default to the happy smiley color, then check to see if it needs to be changed
			UIColor *valueColor = [UIColor BL_happySmileyColor];

			// Adjust color based upon pro-rated meal time values
			BLLogTime latestLoggedTime = [self.dataModel latestLoggedTime];
			if (latestLoggedTime != LogTimeNone) {
				NSDictionary *proRated = [self.dataModel.proRatedByLogTime objectAtIndex:latestLoggedTime];
				NSDictionary *minNutrition = [proRated valueForKey:@"minNutrition"];
				NSDictionary *maxNutrition = [proRated valueForKey:@"maxNutrition"];
				NSNumber *prMinValue = [minNutrition valueForKey:nutrient.name];
				NSNumber *prMaxValue = [maxNutrition valueForKey:nutrient.name];
				if ((prMinValue && ([currentValue doubleValue] < [prMinValue doubleValue]))
						|| (prMaxValue && ([currentValue doubleValue] > [prMaxValue doubleValue]))) {
					valueColor = [UIColor BL_sadSmileyColor];
				}
			}

			NSMutableAttributedString *valueTextAttributed = [[NSMutableAttributedString alloc] initWithString:valueText];
			[valueTextAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15]
				range:NSMakeRange(0, [valueTextAttributed length])];
			[valueTextAttributed addAttribute:NSForegroundColorAttributeName value:valueColor
				range:NSMakeRange(0, [currentValueString length])];

			ngCell.valueLabel.attributedText = valueTextAttributed;

			ngCell.currentValue = [currentValue doubleValue];
			ngCell.minGoalValue = minValue;
			ngCell.maxGoalValue = maxValue;

			ngCell.progressLine.backgroundColor = valueColor;

			[ngCell setNeedsUpdateConstraints];
			[ngCell updateConstraintsIfNeeded];
		}
		else {
			cell = [self.tableView dequeueReusableCellWithIdentifier:@"Nutrient"];
			cell.textLabel.text = nutrient.displayName;
			cell.detailTextLabel.text = [NSString stringWithFormat:@"%@%@", currentValueString, (nutrient.units ?: @"")];
		}
	}
	else {
		// TODO Show goals for log times that have a percent assigned
		NSDictionary *nutrition = [self.dataModel.nutritionByLogTime objectAtIndex:self.activeTab - 1];
		NSNumber *currentValue = [nutrition valueForKey:nutrient.name];
		NSString *currentValueString = [NSString stringWithFormat:@"%.*f", [nutrient.precision intValue], [currentValue doubleValue]];

		cell = [self.tableView dequeueReusableCellWithIdentifier:@"Nutrient"];
		cell.textLabel.text = nutrient.displayName;
		cell.detailTextLabel.text = [NSString stringWithFormat:@"%@%@", (currentValueString ?: @(0)), (nutrient.units ?: @"")];
	}

	return cell;
}

#pragma mark - responders
- (IBAction)doneSelected:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tabChanged:(BLLogTimeTabBar *)sender {
	self.activeTab = sender.selectedTab;
	[self.tableView reloadData];
}

@end
