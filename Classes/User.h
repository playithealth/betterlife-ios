//
//  User.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/22/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UserProfileMO.h"

@interface User : NSObject <NSCoding, NSCopying>

@property (strong, nonatomic) NSNumber *loginId;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *oauthAccessToken;
@property (strong, nonatomic) NSString *oauthSecret;
@property (weak, nonatomic, readonly) UserProfileMO *userProfile;

- (BOOL)loginIsValid;

+ (User *)currentUser;
+ (NSNumber *)loginId;
- (NSNumber*)loginId;
+ (NSNumber *)accountId;
- (NSNumber *)accountId;
- (NSString *)name;
- (NSString *)homeZip;
+ (NSString *)dataFilePath;
+ (void)clearCurrentUser;

@end
