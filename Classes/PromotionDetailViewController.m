//
//  PromotionDetailViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "PromotionDetailViewController.h"


@implementation PromotionDetailViewController
@synthesize label;
@synthesize message;

- (void)viewWillAppear:(BOOL)animated {
	label.text = message;
	[super viewWillAppear:animated];
}
- (void)viewDidUnload {
	self.label = nil;
	self.message = nil;
	[super viewDidUnload];
}

@end
