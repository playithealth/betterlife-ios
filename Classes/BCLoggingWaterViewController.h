//
//  BCLoggingWaterViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/24/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FoodLogMO.h"

@interface BCLoggingWaterViewController : UIViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSDate *logDate;
@property (assign, nonatomic) BLLogTime logTime;
@property (strong, nonatomic) FoodLogMO *foodLog;
@end

