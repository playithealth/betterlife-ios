//
//  BCAccountSettingsViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/12/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import "BCAccountSettingsViewController.h"

#import "BCAppDelegate.h"
#import "BCCustomFoodsViewController.h"
#import "BCPermissionsDataModel.h"
#import "BCUserProfileViewController.h"
#import "BCUserStoresViewController.h"
#import "BLOnboardNutritionGoalsViewController.h"
#import "HealthProfileViewController.h"
#import "BCRecipeBoxViewController.h"
#import "SuggestionsViewController.h"
#import "UIColor+Additions.h"
#import "UIViewController+BLSidebarView.h"
#import "UserPermissionMO.h"
#import "UserProfileMO.h"

@interface BCAccountSettingsViewController ()

@end

@implementation BCAccountSettingsViewController

//static const NSInteger kRowProfile = 0;
static const NSInteger kRowGoals = 1;
static const NSInteger kRowNutritionGoals = 2;
static const NSInteger kRowHealthProfile = 3;
static const NSInteger kRowSuggestions = 4;
static const NSInteger kRowRecipes = 5;
static const NSInteger kRowStoreHistory = 6;
//static const NSInteger kRowCustomFoods = 7;
//static const NSInteger kNumberOfRows = 8;

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self BL_implementSidebar];
	
	UIView *backgroundView = [[UIView alloc] init];
	backgroundView.backgroundColor = [UIColor BC_lightGrayColor];
	self.tableView.backgroundView = backgroundView;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowHealthProfile"]) {
		HealthProfileViewController *viewController = segue.destinationViewController;
		viewController.managedObjectContext = self.managedObjectContext;
	}
	else if ([segue.identifier isEqualToString:@"ShowProfile"]) {
		BCUserProfileViewController *viewController = segue.destinationViewController;
		viewController.managedObjectContext = self.managedObjectContext;
	}
	else if ([segue.identifier isEqualToString:@"ShowMeals"]) {
		BCRecipeBoxViewController *viewController = segue.destinationViewController;
		viewController.managedObjectContext = self.managedObjectContext;
	}
	else if ([segue.identifier isEqualToString:@"ShowSuggestions"]) {
		SuggestionsViewController *viewController = segue.destinationViewController;
		viewController.managedObjectContext = self.managedObjectContext;
	}
	else if ([segue.identifier isEqualToString:@"ShowStores"]) {
		BCUserStoresViewController *viewController = segue.destinationViewController;
		viewController.managedObjectContext = self.managedObjectContext;
	}
	else if ([segue.identifier isEqualToString:@"ShowCustomFoods"]) {
		BCCustomFoodsViewController *viewController = segue.destinationViewController;
		viewController.managedObjectContext = self.managedObjectContext;
	}
}

#pragma mark - local methods
- (IBAction)gotoLogin:(id)sender
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate gotoLogin];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44.0f;

	// hide the specific rows if the user doesn't have that permission
	if ((indexPath.row == kRowGoals)
			&& ![BCPermissionsDataModel userHasPermission:kPermissionFoodlog]
			&& ![BCPermissionsDataModel userHasPermission:kPermissionActivitylog]
			&& ![BCPermissionsDataModel userHasPermission:kPermissionMealplan]
			&& ![BCPermissionsDataModel userHasPermission:kPermissionActivityplan]) {
		rowHeight = 0.0f;
	}	
	if (indexPath.row == kRowNutritionGoals && ![BCPermissionsDataModel userHasPermission:kPermissionGoals]) {
		rowHeight = 0.0f;
	}
	else if (indexPath.row == kRowHealthProfile && ![BCPermissionsDataModel userHasPermission:kPermissionHealthprefs]) {
		rowHeight = 0.0f;
	}	
	else if (indexPath.row == kRowSuggestions && ![BCPermissionsDataModel userHasPermission:kPermissionShop]) {
		rowHeight = 0.0f;
	}	
	else if (indexPath.row == kRowRecipes) {
		rowHeight = 0.0f;
	}	
	else if (indexPath.row == kRowStoreHistory && ![BCPermissionsDataModel userHasPermission:kPermissionShop]) {
		rowHeight = 0.0f;
	}	
	return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	id viewController = nil;

	if (indexPath.row == kRowNutritionGoals) {
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
		viewController = [storyboard instantiateViewControllerWithIdentifier:@"Onboard Nutrition"];
	}
	else if (indexPath.row == kRowGoals) {
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
		viewController = [storyboard instantiateViewControllerWithIdentifier:@"Onboard Demographics"];
	}

	if (viewController) {
		if ([viewController respondsToSelector:@selector(setManagedObjectContext:)]) {
			[viewController setManagedObjectContext:self.managedObjectContext];
		}

		[self.navigationController pushViewController:viewController animated:YES];
	}
}

@end
