//
//  BCSearchViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 8/30/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCSearchViewDelegate.h"

@interface BCSearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) id<BCSearchViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *backLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (assign, nonatomic) NSInteger morePage;
@property (strong, nonatomic) UIToolbar *keyboardToolbar;
@property (strong, nonatomic) NSString *searchTerm;
@property (assign, nonatomic) BOOL showKeyboardOnView;

- (IBAction)cancelButtonTapped:(id)sender;
- (IBAction)searchTextFieldEditingChanged:(id)sender;
- (void)fetchInitialResults;
- (void)fetchLocalResults;
- (void)resetSearch;
- (void)showBackView:(BOOL)show withLabelText:(NSString *)labelText;
- (IBAction)backButtonTapped:(id)sender;
- (void)resignKeyboard:(id)sender;

- (void)insertRowsFromArray:(NSArray *)fromArray toArray:(NSMutableArray *)toArray inSection:(NSInteger)section more:(BOOL)more;

@end
