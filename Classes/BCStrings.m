//
//  BCStrings.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/1/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "BCStrings.h"


// update statuses
NSString * const kStatusOk = @"OK";
NSString * const kStatusPost = @"POST";
NSString * const kStatusPut = @"PUT";
NSString * const kStatusDelete = @"DELETE";

// return value
NSString * const kSuccess = @"success";
NSString * const kErrors = @"errors";

// common columns
NSString * const kAccountId = @"account_id";
NSString * const kCloudId = @"cloud_id";
NSString * const kDeleted = @"deleted";
NSString * const kUpdatedOn = @"updated_on";
NSString * const kCreatedOn = @"created_on";
NSString * const kUserId = @"user_id";
NSString * const kStatus = @"status";
NSString * const kRating = @"rating";
NSString * const kRatingCount = @"rating_count";
NSString * const kMore = @"more";
NSString * const kOffset = @"offset";
NSString * const kWebRecipeImageURL = @"image_url";
NSString * const kItemTypeProduct = @"product";
NSString * const kItemTypeRecipe = @"recipe";
NSString * const kItemTypeMenu = @"menu";
NSString * const kItemTypeWater = @"water";
NSString * const kItemTypeCalories = @"calories";
NSString * const kItemTypeOther = @"other";
NSString * const kSource = @"source";
NSString * const kType = @"type";
NSString * const kReadOn = @"read_on";
NSString * const kTitle = @"title";
NSString * const kUnits = @"units";
NSString * const kDescription = @"description";

// login
NSString * const kEmail = @"email";
NSString * const kPassword = @"password";
NSString * const kLoginZip = @"zip";
NSString * const kLoginUserId = @"login_id";
NSString * const kLoginAccountId = @"account_id";
NSString * const kLoginName = @"name";
NSString * const kLoginOAuthAccessToken = @"oauth_access_token";
NSString * const kLoginOAuthSecret = @"oauth_secret";
NSString * const kLoginPaidCustomer = @"paidcustomer";

// store
NSString * const kId = @"id";
NSString * const kDefaultStore = @"default_store";
NSString * const kEntityId = @"entity_id";
NSString * const kName = @"name";
NSString * const kStore = @"store_name";
NSString * const kAddress = @"address";
NSString * const kAddress2 = @"address2";
NSString * const kCity = @"city";
NSString * const kState = @"state";
NSString * const kZip = @"zip";
NSString * const kPhone = @"phone";
NSString * const kLatitude = @"latitude";
NSString * const kLongitude = @"longitude";
NSString * const kBuyerCompassClient = @"buyercompassclient";

// shoppinglist
NSString * const kProductCategory = @"product_category";
NSString * const kCategoryId = @"category_id";
NSString * const kProductId = @"product_id";
NSString * const kQuantity = @"quantity";
NSString * const kPrivate = @"private";
NSString * const kBarcode = @"barcode";
NSString * const kPurchased = @"purchased";
NSString * const kImageId = @"image_id";
NSString * const kPromoAvailable = @"promo_available";
NSString * const kPromoSelectedCount = @"selected_promo_count";
NSString * const kLifestyleCount = @"lifestyle_count";
NSString * const kAllergyCount = @"allergy_count";
NSString * const kIngredientCount = @"ingredient_count";

// category sort
NSString * const kStoreCategoryId = @"category_id";
NSString * const kStoreCategorySortOrder = @"sort_order";

// recommendation
NSString * const kIsRecommendation = @"is_recommendation";
NSString * const kRecommendationDays = @"days";
NSString * const kRecommendationHidden = @"hidden";

// products search
NSString * const kGenericName = @"generic_name";
NSString * const kProductCategoryName = @"product_category_name";
NSString * const kProductCategoryId = @"product_category_id";
NSString * const kProductResults = @"results";
NSString * const kProductBrands = @"search_brands";
NSString * const kProductCategories = @"search_categories";

// purchase history
NSString * const kPurchaseHx = @"purchasehistory";
NSString * const kPurchaseHxPurchasedOn = @"purchased_on";

// Pantry
NSString * const kSourceId = @"source_id";
NSString * const kItemName = @"item";
NSString * const kPantryItemCategory = @"category";
NSString * const kPantryItemPurchasedOn = @"purchased_on";

// inbox
NSString * const kMessages = @"messages";
NSString * const kMessageId = @"message_id";
NSString * const kMessageDeliveryId = @"delivery_id";
NSString * const kMessageSubject = @"subject";
NSString * const kMessageSummary = @"summary";
NSString * const kMessageBody = @"body";
NSString * const kMessageText = @"text";
NSString * const kMessageIsPrivate = @"private";
NSString * const kMessageIsRead = @"isread";
NSString * const kMessageReceiverDeleted = @"receiverRm";
NSString * const kMessageSenderDeleted = @"senderRm";
NSString * const kMessageUserName = @"user_name";
NSString * const kMessageSenderId = @"from_user_id";
NSString * const kMessageToAccountId = @"to_account_id";
NSString * const kMessageToLoginId = @"to_user_id";
NSString * const kMessageReplyId = @"reply_id";
NSString * const kMessageDeliveryList = @"delivery_list";
NSString * const kMessageToUsers = @"to_users";
NSString * const kMessageDeliveries = @"deliveries";
NSString * const kAction = @"action";
NSString * const kMarkRead = @"markread";
NSString * const kMarkDeleted = @"markdeleted";
NSString * const kMarkUnread = @"markunread";
// evaluations
NSString * const kEvaluationComment = @"eval_comment";
NSString * const kEvaluationUserName = @"name";
NSString * const kEvaluationCategory = @"category";

// Facebook
NSString * const kFacebookAppId = @"134330253268368";
NSString * const kFacebookApiKey = @"2fde52df2b80833edde5e25cbeeea522";
NSString * const kFacebookApiSecret = @"34b6045887567e330de79836c0661ea4";
NSString * const kFacebookAccessToken = @"fb_access_token";

// OAuth
NSString * const kMobileId = @"mobile_id";
NSString * const kDeviceName = @"device_name";
NSString * const kOAuthConsumerKey = @"oauth_consumer_key";
NSString * const kOAuthConsumerSecret = @"oauth_consumer_secret";
NSString * const kOAuthConsumerHost = @"oauth_consumer_host";
NSString * const kOAuthAccessToken = @"oauth_access_token";
NSString * const kOAuthSecret = @"oauth_secret";
NSString * const kOAuthNOnce = @"oauth_nonce";
NSString * const kOAuthSignature = @"oauth_signature";
NSString * const kOAuthToken = @"oauth_token";
NSString * const kOAuthTimeStamp = @"oauth_timestamp";
NSString * const kOAuthVersion = @"oauth_version";
NSString * const kOAuthSignatureMethod = @"oauth_signature_method";

// single use tokens
NSString * const kSingleUseTokenKey = @"su_id";
NSString * const kSingleUseTokenAgeKey = @"su_age";

NSString * const kUpdatingDevice = @"updating_device";

// promotions
NSString * const kPromotionName = @"name";
NSString * const kDisclaimer = @"disclaimer";
NSString * const kDiscount = @"discount";
NSString * const kPromotionDescription = @"description";
NSString * const kAccepted = @"accepted";
NSString * const kPromotionShoppingListId = @"shoppinglists_id";
NSString * const kPromotionDelete = @"delete";
NSString * const kPromotionId = @"promotion_id";

// meals, ahem... Recipes
NSString * const kRecipes = @"recipes";
NSString * const kRecipeAdvisorId = @"advisor_id";
NSString * const kRecipeAdvisorName = @"advisor_name";
NSString * const kRecipeId = @"recipe_id";
NSString * const kRecipeURL = @"url";
NSString * const kRecipeName = @"name";
NSString * const kRecipeImageId = @"image_id";
NSString * const kRecipeImageURL = @"image_url";
NSString * const kRecipeSource = @"source";
NSString * const kRecipeInstructions = @"instructions";
NSString * const kRecipeServes = @"serves";
NSString * const kRecipeCookTime = @"cooktime";
NSString * const kRecipeCookTimeUnits = @"cooktime_units";
NSString * const kRecipePrepTime = @"preptime";
NSString * const kRecipeIngredients = @"ingredients";
NSString * const kRecipeExternalId = @"external_id";
NSString * const kRecipeIsOwner = @"is_owner";
NSString * const kRecipeTags = @"tags";

NSString * const kValueCooktimeMinutes = @"Min";
NSString * const kValueCooktimeHours = @"Hours";

// meal ingredients
NSString * const kAddToShoppingList = @"slitem";
NSString * const kMeasure = @"measure";
NSString * const kMeasureUnits = @"unit_name";
NSString * const kMeasureUnitsId = @"unit_id";
NSString * const kShoppingListText = @"sltext";
NSString * const kShoppingListQuantity = @"slquantity";
NSString * const kShoppingListCategoryId = @"slcategory_id";
NSString * const kIngredientNotes = @"notes";
NSString * const kOriginalIngredientText = @"original_ingr_text";

// activity plan
NSString * const kAPDay = @"day";
NSString * const kAPActivity = @"activity";
NSString * const kAPFactor = @"factor";
NSString * const kAPDistance = @"distance";
NSString * const kAPTime = @"time";
NSString * const kAPExerciseRoutinesID = @"exercise_routines_id";

// meal planner
NSString * const kMealPlannerId = @"mealplan_id";
NSString * const kMealPlannerDay = @"day";
NSString * const kMealPlannerTime = @"logtime";
NSString * const kMPShoppingListItems = @"shopping_list_items";
NSString * const kMPShoppingListItemQuantity = @"quantity";
NSString * const kMPShoppingListItemSLId = @"shoppinglist_id";
NSString * const kMPShoppingListItemIngredientId = @"mealrecipe_id";
NSString * const kMPShoppingListItemDateAdded = @"date_added";
NSString * const kMPMenuId = @"menu_id";
NSString * const kMPRecipeId = @"recipe_id";
NSString * const kMPProductId = @"product_id";
NSString * const kMPItemType = @"item_type";
NSString * const kMPTagID = @"mealplan_tag_id";
NSString * const kMPServingSize = @"mealplan_serving_size";
NSString * const kMPServingSizeUOM = @"mealplan_serving_size_uom";
NSString * const kMPServingSizeUOMAbbreviation = @"mealplan_serving_size_uom_abbrev";

// Advisor Meal Plan Tags
NSString * const kMPExternalId = @"external_id";
NSString * const kMPNutrients = @"nutrients";
NSString * const kMPTagItemType = @"tag_type";
NSString * const kMPTagServingsString = @"servings_string";
NSString * const kMPTagServings = @"servings";
NSString * const kMPTagServingsUnit = @"units";
NSString * const kMPTagServingSize = @"serving_size";
NSString * const kMPTagServingSizeUOM = @"serving_size_uom";

// food log
NSString * const kFoodLogs = @"foodlogs";
NSString * const kFoodLogId = @"foodlog_id";
NSString * const kFoodLogImageId = @"image_id";
NSString * const kFoodLogRecipeId = @"recipe_id";
NSString * const kFoodLogProductId = @"product_id";
NSString * const kFoodLogMenuId = @"menu_id";
NSString * const kFoodLogDay = @"day";
NSString * const kFoodLogLogTime = @"logtime";
NSString * const kFoodLogMultiplier = @"multiplier";
NSString * const kFoodLogName = @"name";
NSString * const kFoodLogUpdatedOn = @"updated_on";
NSString * const kFoodLogDeleted = @"deleted";
NSString * const kFoodLogUpdatingDevice = @"updating_device";
NSString * const kFoodLogServingSize = @"serving_size";
NSString * const kFoodLogNutritionFactor = @"nutrition_factor";
NSString * const kFoodLogServingsUnits = @"units";
NSString * const kFoodLogItemType = @"item_type";
NSString * const kFoodLogItemTypeProduct = @"product";
NSString * const kFoodLogItemTypeRecipe = @"recipe";
NSString * const kFoodLogItemTypeMenu = @"menu";
NSString * const kFoodLogItemTypeWater = @"water";
NSString * const kFoodLogItemTypeCalories = @"calories";
NSString * const kFoodLogItemTypeOther = @"other";

// logging note
NSString * const kLoggingNoteDay = @"day";
NSString * const kLoggingNoteType = @"note_type";
NSString * const kLoggingNoteTypeFood = @"food";
NSString * const kLoggingNoteTypeExercise = @"exercise";
NSString * const kLoggingNoteNote = @"note";
NSString * const kLoggingNoteSource = @"source";

// nutrition info
NSString * const kNutritionId = @"nutrition_id";
NSString * const kNutritionCalories = @"calories";
NSString * const kNutritionCaloriesFromFat = @"calories_from_fat";
NSString * const kNutritionSaturatedFatCalories = @"saturated_fat_calories";
NSString * const kNutritionTotalFat = @"total_fat";
NSString * const kNutritionSaturatedFat = @"saturated_fat";
NSString * const kNutritionPolyunsaturatedFat = @"polyunsaturated_fat";
NSString * const kNutritionMonounsaturatedFat = @"monounsaturated_fat";
NSString * const kNutritionTransFat = @"trans_fat";
NSString * const kNutritionCholesterol = @"cholesterol";
NSString * const kNutritionSodium = @"sodium";
NSString * const kNutritionPotassium = @"potassium";
NSString * const kNutritionTotalCarbohydrates = @"total_carbohydrate";
NSString * const kNutritionDietaryFiber = @"dietary_fiber";
NSString * const kNutritionSolubleFiber = @"soluble_fiber";
NSString * const kNutritionInsolubleFiber = @"insoluble_fiber";
NSString * const kNutritionSugars = @"sugars";
NSString * const kNutritionSugarsAlcohol = @"sugar_alcohol";
NSString * const kNutritionOtherCarbohydrates = @"other_carbohydrate";
NSString * const kNutritionProtein = @"protein";
NSString * const kNutritionVitaminAPercent = @"vitamin_a_pct";
NSString * const kNutritionVitaminCPercent = @"vitamin_c_pct";
NSString * const kNutritionCalciumPercent = @"calcium_pct";
NSString * const kNutritionIronPercent = @"iron_pct";

NSString * const kNutrition = @"nutrition";
NSString * const kNutritionMasterId = @"master_id";
NSString * const kNutritionName = @"name";
NSString * const kNutritionQuantity = @"quantity";
NSString * const kNutritionUnitOfMeasure = @"uom";
NSString * const kNutritionPercent = @"pct";

// training log common
NSString * const kTrainingLogDay = @"day";

// cardio log
NSString * const kCardioLogId = @"cardio_id";
NSString * const kCardioLogExercise = @"exercise";
NSString * const kCardioLogTime = @"time";
NSString * const kCardioLogDistance = @"distance";
NSString * const kCardioLogResistance = @"resistance";
NSString * const kCardioLogIntensity = @"intensity";
NSString * const kCardioLogIntensityLow = @"Low";
NSString * const kCardioLogIntensityMedium = @"Medium";
NSString * const kCardioLogIntensityHigh = @"High";
NSString * const kCardioLogHeartrate = @"heartrate";
NSString * const kCardioLogEase = @"ease";
NSString * const kCardioLogEaseEasy = @"Easy";
NSString * const kCardioLogEaseMedium = @"Medium";
NSString * const kCardioLogEaseHard = @"Hard";
NSString * const kCardioLogCalories = @"calories";

// assessment log
NSString * const kAssessmentLogId = @"assessment_id";
NSString * const kAssessmentLogAge = @"age";
NSString * const kAssessmentLogSex = @"sex";
NSString * const kAssessmentLogHeight = @"height";
NSString * const kAssessmentLogWeight = @"weight";
NSString * const kAssessmentLogBodyFatPercent = @"body_fat_pct";
NSString * const kAssessmentLogNeck = @"neck";
NSString * const kAssessmentLogChest = @"chest";
NSString * const kAssessmentLogWaist = @"waist";
NSString * const kAssessmentLogStomach = @"stomach";
NSString * const kAssessmentLogHips = @"hips";
NSString * const kAssessmentLogRightBicep = @"right_bicep";
NSString * const kAssessmentLogLeftBicep = @"left_bicep";
NSString * const kAssessmentLogRightThigh = @"right_thigh";
NSString * const kAssessmentLogLeftThigh = @"left_thigh";
NSString * const kAssessmentLogActivityLevel = @"activity_level";
NSString * const kAssessmentLogCrunches = @"crunches";
NSString * const kAssessmentLogPushups = @"pushups";
NSString * const kAssessmentLogPullups = @"pullups";
NSString * const kAssessmentLogSquats = @"squats";
NSString * const kAssessmentLogMileRun = @"mile_run";

// activity log
NSString * const kActivityRecordActivity = @"activity"; ///< Subkey of the dictionary
NSString * const kActivityId = @"activity_id";
NSString * const kActivityBasedOnId = @"based_on_id";
NSString * const kActivityForceFactor = @"force_factor";
NSString * const kActivityIsTimedActivity = @"timed_activity";
NSString * const kActivityLink = @"link";
NSString * const kActivityNotes = @"notes";
NSString * const kActivityRoutineId = @"exercise_routines_id";
NSString * const kActivitySets = @"sets";
NSString * const kActivityRecordFactor = @"factor";
NSString * const kActivityRecordTimed = @"timed_activity";
NSString * const kActivityRecordLink = @"link";
NSString * const kActivityRecordNotes = @"notes";
NSString * const kActivityRecordTargetDate = @"target_date";
NSString * const kActivityRecordSets = @"sets";
NSString * const kActivityRecordWorkoutId = @"activity_entry_id";
NSString * const kActivityRecordUpdatedOn = @"updated_on_ts";

// Activity Sets
NSString * const kActivitySetTypeId = @"activity_type_id";
NSString * const kActivitySetSequenceNumber = @"set_number";
NSString * const kActivitySetZone = @"zone";
NSString * const kActivitySetMiles = @"miles";
NSString * const kActivitySetMPH = @"mph";
NSString * const kActivitySetPounds = @"pounds";
NSString * const kActivitySetReps = @"reps";
NSString * const kActivitySetSeconds = @"seconds";
NSString * const kActivitySetWatts = @"watts";

// Old activity log stuff
NSString * const kActivityLogExerciseRoutinesId = @"exercise_routines_id";
NSString * const kActivityLogTime = @"time";
NSString * const kActivityLogWeight = @"weight";
NSString * const kActivityLogCalories = @"calories";
NSString * const kActivityLogDistance = @"distance";

// Tracking
NSString * const kTrackingID = @"tracking_id";
NSString * const kTrackingValue1 = @"track_value1";
NSString * const kTrackingValue2 = @"track_value2";
NSString * const kTrackingValue3 = @"track_value3";
NSString * const kTrackingValue4 = @"track_value4";
NSString * const kTrackingValue5 = @"track_value5";
NSString * const kTrackingValue6 = @"track_value6";
NSString * const kTrackingValues = @"track_values";
NSString * const kTrackingType = @"track_type";
NSString * const kTrackingDate = @"track_date";
NSString * const kTrackingBodyFat = @"body_fat";
NSString * const kTrackingWeight = @"weight";
NSString * const kTrackingDiastolic = @"diastolic";
NSString * const kTrackingSystolic = @"systolic";
NSString * const kTrackingGlucose = @"glucose";
NSString * const kTrackingHA1c = @"hA1c";
NSString * const kTrackingTypeId = @"trackType";
NSString * const kTrackingTypeView = @"view";
NSString * const kTrackingTypeReadOnly = @"readOnly";
NSString * const kTrackingTypeSortOrder = @"sortOrder"; ///< Not currently being passed from the cloud
NSString * const kTrackingTypeValues = @"values";
NSString * const kTrackingValueField = @"field";
NSString * const kTrackingValueDefault = @"default";
NSString * const kTrackingValueDataType = @"dataType";
NSString * const kTrackingValueUnitAbbrev = @"unitAbbrev";
NSString * const kTrackingValueRangeMin = @"rangeMin";
NSString * const kTrackingValueRangeMax = @"rangeMax";
NSString * const kTrackingValuePrecision = @"precision";
NSString * const kTrackingValueOptional = @"optional";

// user profile
NSString * const kUserProfileName = @"name";
NSString * const kUserProfileEmail = @"email";
NSString * const kUserProfileZipCode = @"zipcode";
NSString * const kUserProfileActivityLevel = @"activity_level";
NSString * const kUserProfileBirthday = @"birthday";
NSString * const kUserProfileBirthdayMonth = @"month";
NSString * const kUserProfileBirthdayDay = @"day";
NSString * const kUserProfileBirthdayYear = @"year";
NSString * const kUserProfileGender = @"gender";
NSString * const kUserProfileHeight = @"height";
NSString * const kUserProfileGoalWeight = @"goal_weight";
NSString * const kUserProfileCurrentWeight = @"current_weight";
NSString * const kUserProfileGoalCaloriesMin = @"calories_min";
NSString * const kUserProfileGoalCaloriesMax = @"calories_max";
NSString * const kUserProfileGoalProteinMin = @"protein_min";
NSString * const kUserProfileGoalProteinMax = @"protein_max";
NSString * const kUserProfileGoalFatMin = @"fat_min";
NSString * const kUserProfileGoalFatMax = @"fat_max";
NSString * const kUserProfileGoalSatFatMin = @"satfat_min";
NSString * const kUserProfileGoalSatFatMax = @"satfat_max";
NSString * const kUserProfileGoalCarbsMin = @"carbs_min";
NSString * const kUserProfileGoalCarbsMax = @"carbs_max";
NSString * const kUserProfileGoalSodiumMin = @"sodium_min";
NSString * const kUserProfileGoalSodiumMax = @"sodium_max";
NSString * const kUserProfileGoalCholesterolMin = @"cholesterol_min";
NSString * const kUserProfileGoalCholesterolMax = @"cholesterol_max";
NSString * const kUserProfileGoalNutritionEffort = @"nutrition_effort";
NSString * const kUserProfileOnboardingComplete = @"onboard_complete";
NSString * const kUserProfileMyZone = @"myzone";
NSString * const kUserProfileSkin = @"skin";
NSString * const kUserProfileSkinKosama = @"kosama-skin";
NSString * const kUserProfileCustomNutritionTimeframe = @"custom_nutrition_timeframe";
NSString * const kUserProfileCustomTrackingTimeframe = @"custom_tracking_timeframe";
NSString * const kUserProfileAlwaysShowNutrients = @"always_show_nutrients";
NSString * const kUserProfileUseBMR = @"use_bmr";
NSString * const kNutritionMaxData = @"max_nutrition_data";
NSString * const kNutritionMinData = @"min_nutrition_data";


// strength log
NSString * const kStrengthLogId = @"strength_id";
NSString * const kStrengthLogName = @"name";
NSString * const kStrengthLogFocus = @"focus";
NSString * const kStrengthLogEquipment = @"equipment";
NSString * const kStrengthLogSets = @"sets";
NSString * const kStrengthLogSetWeight = @"weight";
NSString * const kStrengthLogSetReps = @"reps";

// settings
NSString * const kShoppingListSortSetting = @"shopping_list_sort";
NSString * const kPantrySortSetting = @"pantry_sort";
NSString * const kAutoRefreshEnabledSetting = @"enabled_auto_refresh";
NSString * const kAutoRefreshIntervalSetting = @"interval_auto_refresh";
NSString * const kCurrentNavigationLocation = @"current_view";
NSString * const kSettingWelcomeViewHasBeenShown = @"welcome_shown";
NSString * const kSettingShowLoggingPredictions = @"logging_predictions";
NSString * const kSettingDBRefreshBuild = @"db_refresh_build"; ///< The most recent build that wiped out the db and re-synced it

NSString * const kHealthFilters = @"healthfilters";

// Health Profile filter - Allergies
NSString * const kHealthFilterAllergies = @"allergies";
NSString * const kHealthFilterAllergyCount = @"allergy_count";
NSString * const kHealthFilterAllergyEggs = @"allergy_eggs";
NSString * const kHealthFilterAllergyFish = @"allergy_fish";
NSString * const kHealthFilterAllergyMilk = @"allergy_milk";
NSString * const kHealthFilterAllergyPeanuts = @"allergy_peanuts";
NSString * const kHealthFilterAllergyShellfish = @"allergy_shellfish";
NSString * const kHealthFilterAllergySesameSeeds = @"allergy_sesame";
NSString * const kHealthFilterAllergySoy = @"allergy_soy";
NSString * const kHealthFilterAllergyTreeNuts = @"allergy_nuts";
NSString * const kHealthFilterAllergyWheat = @"allergy_wheat";

// Health Profile filter - "Lifestyle"
NSString * const kHealthFilterLifestyles = @"lifestyle";
NSString * const kHealthFilterLifestyleRequireAll = @"required_lifestyle";
NSString * const kHealthFilterLifestyleAHAHeartHealthy = @"lifestyle_heart";
NSString * const kHealthFilterLifestyleCalcium = @"lifestyle_calcium";
NSString * const kHealthFilterLifestyleCount = @"lifestyle_count";
NSString * const kHealthFilterLifestyleFatFree = @"lifestyle_fatfree";
NSString * const kHealthFilterLifestyleHighFiber = @"lifestyle_fiber";
NSString * const kHealthFilterLifestyleHighProtein = @"lifestyle_protein";
NSString * const kHealthFilterLifestyleKosher = @"lifestyle_kosher";
NSString * const kHealthFilterLifestyleLowCal = @"lifestyle_lowcal";
NSString * const kHealthFilterLifestyleLowCarb = @"lifestyle_lowcarb";
NSString * const kHealthFilterLifestyleLowSaturatedFat = @"lifestyle_lowsatfat";
NSString * const kHealthFilterLifestyleLowSodium = @"lifestyle_sodium";
NSString * const kHealthFilterLifestyleUSDAOrganic = @"lifestyle_organic";
NSString * const kHealthFilterLifestyleVegan = @"lifestyle_vegan";
NSString * const kHealthFilterLifestyleVegetarian = @"lifestyle_vegetarian";
NSString * const kHealthFilterLifestyleVitaminA = @"lifestyle_vita";
NSString * const kHealthFilterLifestyleVitaminC = @"lifestyle_vitc";

// Health Profile filter - custom ingredient warnings
NSString * const kHealthFilterIngredientWarnings = @"ingredient_warnings";

// Health Profile filter - Maximums
NSString * const kHealthFilterMaximums = @"max";
NSString * const kHealthFilterMaxCalories = @"max_calories";
NSString * const kHealthFilterMaxSaturatedFat = @"max_satfat";
NSString * const kHealthFilterMaxSodium = @"max_sodium";

NSString * const kNotificationKeyObjectID = @"ObjectID";

// Restaurant Chains & Menu Items
NSString * const kNutritionUrl = @"nutrition_url";
NSString * const kMenuItems = @"menu_items";
NSString * const kMenuItemName = @"name";
NSString * const kMenuRestaurantName = @"restaurant_name";
NSString * const kMenuCategory = @"category";
NSString * const kMenuServingSize = @"serving_size";
NSString * const kMenuCaloriesFromFat = @"calories_from_fat";
NSString * const kMenuSaturatedFatCalories = @"saturated_fat_calories";
NSString * const kMenuTotalFat = @"total_fat";
NSString * const kMenuSaturatedFat = @"saturated_fat";
NSString * const kMenuPolyUnsaturatedFat = @"polyunsaturated_fat";
NSString * const kMenuMonoUnsaturatedFat = @"monounsaturated_fat";
NSString * const kMenuTransFat = @"trans_fat";
NSString * const kMenuCholesterol = @"cholesterol";
NSString * const kMenuSodium = @"sodium";
NSString * const kMenuPotassium = @"potassium";
NSString * const kMenuTotalCarbohydrates = @"total_carbohydrate";
NSString * const kMenuDietaryFiber = @"dietary_fiber";
NSString * const kMenuSolubleFiber = @"soluble_fiber";
NSString * const kMenuInsolubleFiber = @"insoluble_fiber";
NSString * const kMenuSugar = @"sugars";
NSString * const kMenuSugarAlcohol = @"sugar_alcohol";
NSString * const kMenuOtherCarbohydrates = @"other_carbohydrate";
NSString * const kMenuProtein = @"protein";
NSString * const kMenuVitaminA = @"vitamin_a";
NSString * const kMenuVitaminC = @"vitamin_c";
NSString * const kMenuCalcium = @"calcium";
NSString * const kMenuIron = @"iron";

// CustomFood
NSString * const kCFUpdatedOn = @"post_date";
NSString * const kCFServingSize = @"serving_size_text";
NSString * const kCFServingSizeUnit = @"serving_size_uom";
NSString * const kCFServingsPerContainer = @"servings_per_container";
NSString * const kCFCalories = @"calories";
NSString * const kCFCaloriesFromFat = @"calories_from_fat";
NSString * const kCFSaturatedFatCalories = @"saturated_fat_calories";
NSString * const kCFFat = @"total_fat";
NSString * const kCFCFSaturatedFat = @"saturated_fat";
NSString * const kCFPolyunsaturatedFat = @"polyunsaturated_fat";
NSString * const kCFMonounsaturatedFat = @"monounsaturated_fat";
NSString * const kCFTransFat = @"trans_fat";
NSString * const kCFCholesterol = @"cholesterol";
NSString * const kCFSodium = @"sodium";
NSString * const kCFPotassium = @"potassium";
NSString * const kCFCarbohydrates = @"total_carbohydrate";
NSString * const kCFFiber = @"dietary_fiber";
NSString * const kCFSolubleFiber = @"soluble_fiber";
NSString * const kCFInsolubleFiber = @"insoluble_fiber";
NSString * const kCFSugar = @"sugars";
NSString * const kCFSugarAlcohol = @"sugar_alcohol";
NSString * const kCFOtherCarbohydrates = @"other_carbohydrate";
NSString * const kCFProtein = @"protein";
NSString * const kCFVitaminA = @"vitamin_a_pct";
NSString * const kCFVitaminC = @"vitamin_c_pct";
NSString * const kCFCalcium = @"calcium_pct";
NSString * const kCFIron = @"iron_pct";

// Advisor & AdvisorGroup
NSString * const kAdvisorId = @"advisor_id";
NSString * const kAdvisorCanMessage = @"message";
NSString * const kAdvisorCanRecommendMeals = @"recommend_meals";
NSString * const kAdvisorCanRecommendMealPlans = @"recommend_mealplans";
NSString * const kAdvisorCanRecommendExercise = @"recommend_exercise";
NSString * const kAdvisorCanViewNutrition = @"view_nutrition";
NSString * const kAdvisorCanViewExercise = @"view_exercise";
NSString * const kAdvisorCanViewMeals = @"view_meals";
NSString * const kAdvisorCanViewShoppingList = @"view_shoppinglist";
NSString * const kAdvisorCanViewPantry = @"view_pantry";
NSString * const kAdvisorCanAddToShoppingList = @"add_shoppinglist";
NSString * const kAdvisorCanDeleteFromShoppingList = @"del_shoppinglist";
NSString * const kAdvisorConfirmation = @"confirmation";
NSString * const kAdvisorGroups = @"groups";
NSString * const kAdvisorGroupId = @"advisor_group_id";
NSString * const kAdvisorName = @"advisor_name";

// Advisor Plans aka Health Coach Plans - Common keys
NSString * const kAdvisorPlanName = @"name";
NSString * const kAdvisorPlanDescription = @"description";
NSString * const kAdvisorPlanIsRepeating = @"is_repeating";
NSString * const kAdvisorPlanPhases = @"phases";

NSString * const kAdvisorPlanPhaseDuration = @"duration";
NSString * const kAdvisorPlanPhaseNumber = @"phase";
NSString * const kAdvisorPlanPhaseEntries = @"entries";

NSString * const kAdvisorPlanStartDate = @"start_date";
NSString * const kAdvisorPlanStopDate = @"stop_date";

// Advisor Meal Plans aka Health Coach Plans
NSString * const kAdvisorMealPlanId = @"advisor_mealplan_id";
NSString * const kAdvisorMealPlanAdvisorId = @"advisor_id";
NSString * const kAdvisorMealPlanSugaryDrinks = @"sugary_drinks";
NSString * const kAdvisorMealPlanFruitsVeggies = @"fruits_veggies";

NSString * const kAdvisorMealPlanPhaseBMR = @"bmr";
NSString * const kAdvisorMealPlanPhaseAutoRatio = @"auto_ratio";
NSString * const kAdvisorMealPlanPhaseProteinRatio = @"ratio_protein";
NSString * const kAdvisorMealPlanPhaseTotalCarbohydrateRatio = @"ratio_total_fat";
NSString * const kAdvisorMealPlanPhaseTotalFatRatio = @"ratio_total_carbohydrate";
NSString * const kAdvisorMealPlanPhaseNutritionMaxID = @"max_nutrition_id";
NSString * const kAdvisorMealPlanPhaseNutritionMinID = @"min_nutrition_id";
NSString * const kAdvisorMealPlanPhaseNutritionMaxData = @"max_nutrition_data";
NSString * const kAdvisorMealPlanPhaseNutritionMinData = @"min_nutrition_data";

NSString * const kAdvisorMealPlanPhaseEntryID = @"advisor_mealplan_entry_id";
NSString * const kAdvisorMealPlanPhaseEntryLogtime = @"logtime";
NSString * const kAdvisorMealPlanPhaseEntryLogtimeNumber = @"logtime_num";
NSString * const kAdvisorMealPlanPhaseEntryNutritionPercent = @"nutrition_percent";
NSString * const kAdvisorMealPlanPhaseEntryUnplannedMealOption = @"unplanned_meal_option";

NSString * const kAdvisorMealPlanPhaseEntryTags = @"tags";
NSString * const kAdvisorMealPlanPhaseEntryTagID = @"advisor_mealplan_tag_id";
NSString * const kAdvisorMealPlanPhaseEntryTagName = @"tag";
NSString * const kAdvisorMealPlanPhaseEntryTagServingTargetQuantity = @"serving_target_qty";
NSString * const kAdvisorMealPlanPhaseEntryTagServingTargetUnit = @"serving_target";

// Advisor Activity Plans aka Health Coach Plans
NSString * const kAdvisorActivityPlan = @"plan";

NSString * const kAdvisorActivityPlanWorkout = @"workout";
NSString * const kAdvisorActivityPlanWorkoutNumberToPerform = @"perform";
NSString * const kAdvisorActivityPlanWorkoutPerformOperator = @"perform_op";
NSString * const kAdvisorActivityPlanWorkoutTime = @"do_when";
NSString * const kAdvisorActivityPlanWorkoutActivities = @"activities";

NSString * const kAdvisorActivityPlanWorkoutActivitySetTypeId = @"activity_type_id";
NSString * const kAdvisorActivityPlanWorkoutActivitySetZone = @"zone";
NSString * const kAdvisorActivityPlanWorkoutActivitySetMiles = @"miles";
NSString * const kAdvisorActivityPlanWorkoutActivitySetMPH = @"mph";
NSString * const kAdvisorActivityPlanWorkoutActivitySetPounds = @"pounds";
NSString * const kAdvisorActivityPlanWorkoutActivitySetSeconds = @"seconds";
NSString * const kAdvisorActivityPlanWorkoutActivitySetSequenceNumber = @"set_number";
NSString * const kAdvisorActivityPlanWorkoutActivitySetWatts = @"watts";

NSString * const kAdvisorActivityPlanId = @"activityplan_id";

// unified search
NSString * const kUnifiedSearchAccountID = @"account_id";
NSString * const kUnifiedSearchAdvisorID = @"advisor_id";
NSString * const kUnifiedSearchBarcode = @"barcode";
NSString * const kUnifiedSearchCookTime = @"cooktime";
NSString * const kUnifiedSearchCookTimeUnits = @"cooktime_units";
NSString * const kUnifiedSearchImageID = @"image_id";
NSString * const kUnifiedSearchImageURL = @"imageurl";
NSString * const kUnifiedSearchIsOwner = @"is_owner";
NSString * const kUnifiedSearchItemID = @"item_id";
NSString * const kUnifiedSearchItemType = @"item_type";
NSString * const kUnifiedSearchItemTypeMenuItem = @"menu";
NSString * const kUnifiedSearchItemTypeProduct = @"product";
NSString * const kUnifiedSearchItemTypeRecipe = @"recipe";
NSString * const kUnifiedSearchItems = @"items";
NSString * const kUnifiedSearchName = @"name";
NSString * const kUnifiedSearchNutritionID = @"nutrition_id";
NSString * const kUnifiedSearchPrepTime = @"preptime";
NSString * const kUnifiedSearchSections = @"sections";
NSString * const kUnifiedSearchServes = @"serves";
NSString * const kUnifiedSearchServingSizeText = @"serving_size_text";
NSString * const kUnifiedSearchServingSizeUnit = @"serving_size_uom";
NSString * const kUnifiedSearchShowMore = @"showmore";
NSString * const kUnifiedSearchType = @"type";
NSString * const kUnifiedSearchURL = @"url";

// Sync Check
NSString * const kSyncToken = @"sync_token";
NSString * const kUpdatedTables = @"updated_tables";
NSString * const kSyncActivityLog = @"logged_activities";
NSString * const kSyncActivityPlan = @"activity_planners";
NSString * const kSyncAdvisorActivityPlans = @"activity_plans";
NSString * const kSyncAdvisorGroupUsers = @"advisor_group_users";
NSString * const kSyncAdvisorMealPlans = @"advisor_mealplans";
NSString * const kSyncAdvisorUsers = @"advisor_users";
NSString * const kSyncCategories = @"product_categories";
NSString * const kSyncCategorySorting = @"product_category_sorting";
NSString * const kSyncCoachInvites = @"coach_invites";
NSString * const kSyncCommunities = @"communities";
NSString * const kSyncExerciseActivities = @"exercise_activities";
NSString * const kSyncExerciseAssessment = @"exercise_assessments";
NSString * const kSyncExerciseRoutines = @"exerciseroutines";
NSString * const kSyncExerciseStrength = @"exercise_strength";
NSString * const kSyncFoodLog = @"foodlog";
NSString * const kSyncLogNotes = @"logging_notes";
NSString * const kSyncMealPlan = @"mealplanners";
NSString * const kSyncMessages = @"messages";
NSString * const kSyncNutrients = @"nutrients";
NSString * const kSyncPantry = @"pantries";
NSString * const kSyncPermissions = @"permissions";
NSString * const kSyncProducts = @"products";
NSString * const kSyncPurchaseHx = @"purchasehx"; // This one actually doesn't come down from syncCheck
NSString * const kSyncRecipes = @"meals";
NSString * const kSyncShoppingSuggestions = @"shoppinglist_hide_suggestions";
NSString * const kSyncShopping = @"shoppinglists";
NSString * const kSyncThreadMessages = @"threadmessages";
NSString * const kSyncThreads = @"threads";
NSString * const kSyncTracking = @"tracking";
NSString * const kSyncTrackingType = @"trackingmeta";
NSString * const kSyncUnits = @"units";
NSString * const kSyncUserGroups = @"user_groups";
NSString * const kSyncUserHealthFilters = @"user_health_filters";
NSString * const kSyncUserStores = @"user_stores";
NSString * const kSyncUsers = @"users";

// Permissions
NSString * const kPermission = @"permissions";
NSString * const kPermissionShop = @"shop";
NSString * const kPermissionPantry = @"pantry";
NSString * const kPermissionMealplan = @"mealplan";
NSString * const kPermissionActivityplan = @"activityplan";
NSString * const kPermissionFoodlog = @"foodlog";
NSString * const kPermissionActivitylog = @"activitylog";
NSString * const kPermissionRecipes = @"recipes";
NSString * const kPermissionDashboard = @"dashboard";
NSString * const kPermissionTracking = @"tracking";
NSString * const kPermissionGoals = @"goals";
NSString * const kPermissionHealthprefs = @"healthprefs";

// Evaluation (Rating)
NSString * const kEvalUser = @"user";
NSString * const kEvalUserCategory = @"category";
NSString * const kEvalUserCategoryGeneral = @"General";
NSString * const kEvalUserRating = @"rating";
NSString * const kEvalAggregate = @"aggregate";
NSString * const kEvalAggregateCount = @"count";
NSString * const kEvalAggregateAverage = @"average";
NSString * const kEvalItemId = @"eval_item_id";
NSString * const kEvalItemTypeId = @"eval_item_type_id";
NSUInteger const kEvalItemTypeProduct = 11;
NSUInteger const kEvalItemTypeRecipe = 14;
NSUInteger const kEvalItemTypeMenuItem = 15;
NSUInteger const kEvalItemTypeStore = 4;

// Product warnings
NSString * const kProductWarningsAllergies = @"allergies";
NSString * const kProductWarningsIngredients = @"ingredients";
NSString * const kProductWarningsLifestyle = @"lifestyle";

// Product info
NSString * const kProductInfoAllergies = @"allergies";
NSString * const kProductInfoIngredients = @"ingredients";
NSString * const kProductInfoWarnings = @"warnings";

// Images
NSString * const kImageSizeSmall = @"small";
NSString * const kImageSizeMedium = @"medium";
NSString * const kImageSizeLarge = @"large";

// Nutrients
NSString * const kNutrientName = @"name";
NSString * const kNutrientTitle = @"title";
NSString * const kNutrientPrecision = @"precision";
NSString * const kNutrientUnits = @"units";
NSString * const kNutrientIsSubNutrient = @"sublist";
NSString * const kNutrientSortOrder = @"sort_order";

// Units
NSString * const kUnitAbbreviation = @"abbrev";
NSString * const kUnitSingular = @"singular";
NSString * const kUnitPlural = @"plural";
NSString * const kUnitSortOrder = @"sort_order";

// Coach/Advisor invitations
NSString * const kInviteCode = @"code";
NSString * const kEntityName = @"entity_name";
NSString * const kFirstTimeInvites = @"firstTimeInvites";


// Community
NSString * const kCommunityMember = @"member";
NSString * const kCommunityVisibility = @"visibility";
NSString * const kCommunityId = @"community_id";
NSString * const kCommunityOwnerName = @"owner_name";
NSString * const kCommunityOwnerId = @"owner_id";
NSString * const kCommunityUpdatedOn = @"community_updated_on";
NSString * const kCommunityType = @"community_type";
NSString * const kCommunityTypeCoach = @"coach";
NSString * const kCommunityUsePhoto = @"use_photo";
NSString * const kCommunityStatusNone = @"none";
NSString * const kCommunityStatusReviewed = @"reviewed";
NSString * const kCommunityStatusAccepted = @"accepted";
NSString * const kCommunityStatusDeclined = @"declined";
NSString * const kCommunityStatusLeft = @"left";

// Message Threads
NSString * const kThreadId = @"thread_id";
NSString * const kThreadType = @"thread_type";
NSString * const kThreadUpdatedOn = @"thread_updated_on";

// Thread Messages
NSString * const kSenderName = @"sender_name";

// Sync Update Human Readable Messages
NSString * const kSyncUpdateUserStatusNotification = @"SyncUpdateUserStatusNotification";
NSString * const kSyncUpdateHumanReadableStatusKey = @"HumanReadable";

NSString * const kSyncUpdateHumanReadable_ShoppingList = @"SyncUpdateHumanReadableShoppingList";
NSString * const kSyncUpdateHumanReadable_Messages = @"SyncUpdateHumanReadableMessages";
NSString * const kSyncUpdateHumanReadable_Stores = @"SyncUpdateHumanReadableStores";
NSString * const kSyncUpdateHumanReadable_PurchaseHistory = @"SyncUpdateHumanReadablePurchaseHistory";
NSString * const kSyncUpdateHumanReadable_ProductCategories = @"SyncUpdateHumanReadableProductCategories";
NSString * const kSyncUpdateHumanReadable_StoreCategories = @"SyncUpdateHumanReadableStoreCategories";
NSString * const kSyncUpdateHumanReadable_Pantry = @"SyncUpdateHumanReadablePantry";
NSString * const kSyncUpdateHumanReadable_Tokens = @"SyncUpdateHumanReadableTokens";
NSString * const kSyncUpdateHumanReadable_Recipes = @"SyncUpdateHumanReadableRecipes";
NSString * const kSyncUpdateHumanReadable_MealPlanner = @"SyncUpdateHumanReadableMealPlanner";
NSString * const kSyncUpdateHumanReadable_ActivityPlans = @"SyncUpdateHumanReadableActivityPlans";
NSString * const kSyncUpdateHumanReadable_HealthChoices = @"SyncUpdateHumanReadableHealthChoices";
NSString * const kSyncUpdateHumanReadable_Recommendations = @"SyncUpdateHumanReadableRecommendations";
NSString * const kSyncUpdateHumanReadable_FoodLog = @"SyncUpdateHumanReadableFoodLog";
NSString * const kSyncUpdateHumanReadable_FoodLogNotes = @"SyncUpdateHumanReadableFoodLogNotes";
NSString * const kSyncUpdateHumanReadable_MealPredictions = @"SyncUpdateHumanReadableMealPredictions";
NSString * const kSyncUpdateHumanReadable_Activities = @"SyncUpdateHumanReadableActivities";
NSString * const kSyncUpdateHumanReadable_TrainingAssessment = @"SyncUpdateHumanReadableTrainingAssessment";
NSString * const kSyncUpdateHumanReadable_Strengths = @"SyncUpdateHumanReadableStrengths";
NSString * const kSyncUpdateHumanReadable_Tracking = @"SyncUpdateHumanReadableTracking";
NSString * const kSyncUpdateHumanReadable_TrackingTypes = @"SyncUpdateHumanReadableTrackingTypes";
NSString * const kSyncUpdateHumanReadable_CustomFoods = @"SyncUpdateHumanReadableCustomFoods";
NSString * const kSyncUpdateHumanReadable_UserProfile = @"SyncUpdateHumanReadableUserProfile";
NSString * const kSyncUpdateHumanReadable_UserPermissions = @"SyncUpdateHumanReadableUserPermissions";
NSString * const kSyncUpdateHumanReadable_Advisors = @"SyncUpdateHumanReadableAdvisors";
NSString * const kSyncUpdateHumanReadable_MealPlans = @"SyncUpdateHumanReadableMealPlans";
NSString * const kSyncUpdateHumanReadable_MealPlanTimeline = @"SyncUpdateHumanReadableMealPlanTimeline";
NSString * const kSyncUpdateHumanReadable_ExerciseRoutines = @"SyncUpdateHumanReadableExerciseRoutines";
NSString * const kSyncUpdateHumanReadable_Nutrients = @"SyncUpdateHumanReadableNutrients";
NSString * const kSyncUpdateHumanReadable_MeasurementUnits = @"SyncUpdateHumanReadableMeasurementUnits";
NSString * const kSyncUpdateHumanReadable_CoachInvites = @"SyncUpdateHumanReadableCoachInvites";
NSString * const kSyncUpdateHumanReadable_Communities = @"SyncUpdateHumanReadableCommunities";
NSString * const kSyncUpdateHumanReadable_CommunityThreads = @"SyncUpdateHumanReadableCommunityThreads";
NSString * const kSyncUpdateHumanReadable_CommunityThreadMessages = @"SyncUpdateHumanReadableCommunityThreadMessages";
NSString * const kSyncUpdateHumanReadable_ActivityPlanTimeline = @"SyncUpdateHumanReadableActivityPlanTimeline";
NSString * const kSyncUpdateHumanReadable_ActivityLog = @"SyncUpdateHumanReadableActivityLog";
