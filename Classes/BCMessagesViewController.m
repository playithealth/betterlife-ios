//
//  BCMessagesViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "BCMessagesViewController.h"

#import "BCObjectManager.h"
#import "InboxMessageCell.h"
#import "InboxMessageViewController.h"
#import "InboxMessage.h"
#import "NSDateFormatter+Additions.h"
#import "User.h"

@interface BCMessagesViewController () 
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end


@implementation BCMessagesViewController
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize customCell;

#pragma mark - Lifecycle
- (void)viewDidLoad 
{
    [super viewDidLoad];

	self.navigationController.toolbarHidden = YES;

	[self refresh:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[BCObjectManager syncCheck:SendOnly];

    [super viewWillDisappear:animated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowMessage"]) {
		InboxMessageViewController *detailView = segue.destinationViewController;
	
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		InboxMessage *message = [self.fetchedResultsController objectAtIndexPath:indexPath];
		message.isRead = [NSNumber numberWithInt:1];
		message.status = kStatusPut;

		// Save the context.
		NSError *error = nil;
		if (![message.managedObjectContext save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}

		NSUInteger messageCount = [self.fetchedResultsController.fetchedObjects count];

		detailView.navigationItem.title = [NSString stringWithFormat:@"Message %ld of %lu", (long)[indexPath row] + 1, (unsigned long)messageCount];
		detailView.message = message;
	}
}

#pragma mark - BCTableViewController overrides
- (void)reloadBCTableViewData
{
	// delete the cache and force a table reload
	self.fetchedResultsController = nil;
	[self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sections = [self.fetchedResultsController sections];
    NSUInteger count = 0;
    if ([sections count]) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        count = [sectionInfo numberOfObjects];
    }
    return count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellId = @"MessageCell";

	InboxMessageCell *cell = (InboxMessageCell *)[self.tableView dequeueReusableCellWithIdentifier:CellId];

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (void)configureCell:(InboxMessageCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	InboxMessage *message = [self.fetchedResultsController objectAtIndexPath:indexPath];

	if (message.sender) {
		cell.fromLabel.text = message.sender;
	}
	else {
		cell.fromLabel.text = @"BettrLife";
	}

	cell.subjectLabel.text = message.subject;
	cell.summaryLabel.text = message.summary;
	cell.dateLabel.text = [NSDateFormatter relativeStringFromDate:[NSDate dateWithTimeIntervalSince1970:[message.createdOn doubleValue]]];
	
	if ([message.isRead isEqualToNumber:[NSNumber numberWithInteger:0]]) {
		cell.fromLabel.font = [UIFont boldSystemFontOfSize:14];
		cell.subjectLabel.font = [UIFont boldSystemFontOfSize:14];
		cell.statusImage.image = [UIImage imageNamed:@"envelope-green.png"];
	}
	else {
		cell.fromLabel.font = [UIFont systemFontOfSize:14];
		cell.subjectLabel.font = [UIFont systemFontOfSize:14];
		cell.statusImage.image = [UIImage imageNamed:@"envelope-grey.png"];
	}
	//cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		InboxMessage *message = [self.fetchedResultsController objectAtIndexPath:indexPath];

		// mark this item for deletion
		message.isRemoved = [NSNumber numberWithBool:YES];
		message.status = kStatusPut;

		// Save the context.
		NSError *error = nil;
		if (![message.managedObjectContext save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 80.0f;
	return rowHeight;
}

#pragma mark - Fetched results controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

    // specify that we are fetching the Inbox items
    NSEntityDescription *entity = [NSEntityDescription 
		entityForName:@"InboxMessage" 
		inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
	// filter for the user
	User *thisUser = [User currentUser];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:
			@"((accountId = %@ AND isPrivate = NO) OR loginId = %@) "
				"AND (status <> %@) "
				"AND (isRemoved = NO)", 
			thisUser.accountId, thisUser.loginId, kStatusPost];
	
    // sort by item name
    NSSortDescriptor *sortByDate = [NSSortDescriptor 
		sortDescriptorWithKey:@"createdOn" ascending:NO];
    NSSortDescriptor *sortBySubject = [NSSortDescriptor
		sortDescriptorWithKey:@"subject" ascending:YES];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:
		sortByDate, sortBySubject, nil]];
    
    // create and init the fetched results controller
    NSFetchedResultsController *aFetchedResultsController = 
		[[NSFetchedResultsController alloc] 
		initWithFetchRequest:fetchRequest 
		managedObjectContext:self.managedObjectContext 
		sectionNameKeyPath:nil 
		cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    

	NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error]) {
	    DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    
    return _fetchedResultsController;
}    
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
	//QuietLog(@"%s", __FUNCTION__);
    [self.tableView beginUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller 
didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
	//QuietLog(@"%s", __FUNCTION__);
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
				withRowAnimation:UITableViewRowAnimationLeft];
			break;

		case NSFetchedResultsChangeDelete:
			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
				withRowAnimation:UITableViewRowAnimationRight];
			break;
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
	}
}
- (void)controller:(NSFetchedResultsController *)controller
didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath
forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
	//QuietLog(@"%s", __FUNCTION__);
	UITableView *thisTableView = self.tableView;

	switch(type) {
		case NSFetchedResultsChangeInsert:
			[thisTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
				withRowAnimation:UITableViewRowAnimationLeft];
			break;

		case NSFetchedResultsChangeDelete:
			[thisTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
				withRowAnimation:UITableViewRowAnimationRight];
			break;

		case NSFetchedResultsChangeUpdate:
			[self configureCell:[thisTableView cellForRowAtIndexPath:indexPath]
				atIndexPath:indexPath];
			break;

		case NSFetchedResultsChangeMove:
			[thisTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
				withRowAnimation:UITableViewRowAnimationLeft];
			[thisTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
				withRowAnimation:UITableViewRowAnimationRight];
			break;
	}
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	//QuietLog(@"%s", __FUNCTION__);
    [self.tableView endUpdates];
}

@end
