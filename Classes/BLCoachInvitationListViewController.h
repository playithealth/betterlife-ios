//
//  BLCoachInvitationListViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 7/25/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCTableViewController.h"

@interface BLCoachInvitationListViewController : BCTableViewController
@property (strong, nonatomic) NSArray *invitations;
@end
