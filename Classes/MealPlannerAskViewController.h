//
//  MealPlannerAskViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/7/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MealPlannerAskViewDelegate;

@interface MealPlannerAskViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *askItems;
@property (strong, nonatomic) NSMutableArray *yesItems;
@property (unsafe_unretained, nonatomic) id<MealPlannerAskViewDelegate> delegate;

@end

@protocol MealPlannerAskViewDelegate <NSObject>
@required
- (void)mealPlannerAskView:(MealPlannerAskViewController *)mealPlannerAskView didFinish:(BOOL)done;
@end
