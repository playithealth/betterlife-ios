//
//  BCLoggingNoteViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 12/6/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import "BCLoggingNoteViewController.h"

#import "LoggingNoteMO.h"

@interface BCLoggingNoteViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewBottomConstraint;

@property (assign, nonatomic) CGRect oldRect;
@property (strong, nonatomic) NSTimer *caretVisibilityTimer;

@end

@implementation BCLoggingNoteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.textView.delegate = self;

	[self registerForKeyboardNotifications];

	self.textView.text = self.loggingNote.note;

	[self.textView becomeFirstResponder];
}

#pragma mark - keyboard
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
            selector:@selector(keyboardWasShown:)
            name:UIKeyboardDidShowNotification object:nil];
 
   [[NSNotificationCenter defaultCenter] addObserver:self
             selector:@selector(keyboardWillBeHidden:)
             name:UIKeyboardWillHideNotification object:nil];
 
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
 
	self.textViewBottomConstraint.constant = kbSize.height;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
	self.textViewBottomConstraint.constant = 0;
}

- (void)scrollCaretToVisible
{
    //This is where the cursor is at.
    CGRect caretRect = [self.textView caretRectForPosition:self.textView.selectedTextRange.end];

    if (CGRectEqualToRect(caretRect, self.oldRect)) {
        return;
	}

    self.oldRect = caretRect;

    //This is the visible rect of the textview.
    CGRect visibleRect = self.textView.bounds;
    visibleRect.size.height -= (self.textView.contentInset.top + self.textView.contentInset.bottom);
    visibleRect.origin.y = self.textView.contentOffset.y;

    //We will scroll only if the caret falls outside of the visible rect.
    if (!CGRectContainsRect(visibleRect, caretRect)) {
        CGPoint newOffset = self.textView.contentOffset;

        newOffset.y = MAX((caretRect.origin.y + caretRect.size.height) - visibleRect.size.height + 10, 0);

        [self.textView setContentOffset:newOffset animated:YES];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{    
    [self.caretVisibilityTimer invalidate];
    self.caretVisibilityTimer = nil;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{ 
    self.oldRect = [self.textView caretRectForPosition:self.textView.selectedTextRange.end];
    self.caretVisibilityTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(scrollCaretToVisible) userInfo:nil repeats:YES];
}

@end
