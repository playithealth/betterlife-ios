//
//  BCDebug.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 7/6/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "BCDebug.h"

@implementation BCDebugFormatter

- (id)init
{
	if ((self = [super init])) {
		threadUnsafeDateFormatter = [[NSDateFormatter alloc] init];
		[threadUnsafeDateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
		[threadUnsafeDateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss:SSS"];
	}
	return self;
}

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage
{
	// return [NSString stringWithFormat:@"%@ | %s @ %i | %@", [logMessage fileName], logMessage->function, logMessage->lineNumber, logMessage->logMsg];

    NSString *dateAndTime = [threadUnsafeDateFormatter stringFromDate:(logMessage->timestamp)];
    
	NSString *logPrefix = nil;
	NSString *logSuffix = nil;
    switch (logMessage->logFlag) {
        case LOG_FLAG_FATAL:
			logPrefix = [NSString stringWithFormat:@"Error: %@, %s @ %i | ", 
				[logMessage fileName], logMessage->function, logMessage->lineNumber];
			logSuffix = @"\n";
			break;
        case LOG_FLAG_ERROR:
			logPrefix = [NSString stringWithFormat:@"Error: %@, %s @ %i | ", 
				[logMessage fileName], logMessage->function, logMessage->lineNumber];
			logSuffix = @"\n";
			break;
        case LOG_FLAG_WARN:
			logPrefix = [NSString stringWithFormat:@"Warning: %@, %s @ %i | ", 
				[logMessage fileName], logMessage->function, logMessage->lineNumber];
			logSuffix = @"\n";
			break;
        case LOG_FLAG_DEBUG:
			logPrefix = [NSString stringWithFormat:@"Debug: %@, %s @ %i | ", 
				[logMessage fileName], logMessage->function, logMessage->lineNumber];
			logSuffix = @" **";
			break;
        case LOG_FLAG_INFO:
			logPrefix = @"I ";
			logSuffix = @"";
			break;
        case LOG_FLAG_VERBOSE:
			logPrefix = @"V ";
			logSuffix = @"";
			break;
        default:
			logPrefix = @"";
			logSuffix = @"";
			break;
    }
    
    return [NSString stringWithFormat:@"%@ %@%@%@", dateAndTime, logPrefix, logMessage->logMsg, logSuffix];	
}

- (void)didAddToLogger:(id <DDLogger>)logger
{
    loggerCount++;
    NSAssert(loggerCount <= 1, @"This logger isn't thread-safe");
}

- (void)willRemoveFromLogger:(id <DDLogger>)logger
{
    loggerCount--;
}


@end
