//
//  ShoppingListItemCategoryCell.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 9/6/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingListItemCategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

@end
