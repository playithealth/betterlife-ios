//
//  MealPlannerAskViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/7/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "MealPlannerAskViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "BCTableViewController.h"
#import "RecipeMO.h"
#import "MealIngredient.h"
#import "UIImage+Additions.h"
#import "UIColor+Additions.h"

static const NSInteger kSectionHeaderHeight = 32;

@interface MealPlannerAskViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation MealPlannerAskViewController

- (id)initWithCoder:(NSCoder *)aCoder
{
    self = [super initWithCoder:aCoder];
    if (self) {
        _askItems = [[NSMutableArray alloc] init];
        _yesItems = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rows = 0;
    // Return the number of rows in the section.
	switch (section) {
		case 0:
			rows = [self.askItems count];
			break;
		case 1:
			rows = [self.yesItems count];
			break;
	}
	return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView 
heightForHeaderInSection:(NSInteger)section
{
	return kSectionHeaderHeight;
}

- (UIView *)tableView:(UITableView *)theTableView 
viewForHeaderInSection:(NSInteger)section
{
	NSString *headerText = nil;
	switch (section) {
		case 0:
			headerText = @"Select items to add them to your shopping list";
			break;
		case 1:
			headerText = @"yes items";
			break;
	}

	return [BCTableViewController customViewForHeaderWithTitle:headerText];
}

- (void)tableView:(UITableView *)theTableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	MealIngredient *ingredient = [self.askItems objectAtIndex:[indexPath row]];

	if (!ingredient.answer || ![ingredient.answer boolValue]) {
		ingredient.answer = @YES;
	}
	else {
		ingredient.answer = @NO;
	}

	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];

	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Local methods
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	MealIngredient *ingredient = nil;

	UIImage *onImage = [UIImage imageNamed:@"shop-btn.png"];
	UIImage *offImage = [onImage BC_imageTemplateWithColor:[UIColor lightGrayColor]];

	switch ([indexPath section]) {
		case 0:
			ingredient = [self.askItems objectAtIndex:[indexPath row]];

			if (!ingredient.answer || ![ingredient.answer boolValue]) {
				cell.imageView.image = offImage;
			}
			else {
				cell.imageView.image = onImage;
			}

			break;
		case 1:
			ingredient = [self.yesItems objectAtIndex:[indexPath row]];
			break;
	}

	cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@",
		[ingredient shoppingListQuantity], [ingredient shoppingListText]];
	//cell.detailTextLabel.text = [NSString stringWithFormat: @"%@ - %@", ingredient.meal.name, ingredient.name];
	cell.detailTextLabel.text = [ingredient description];
}

- (IBAction)cancel:(id)sender
{
	if (self.delegate != nil 
			&& [self.delegate conformsToProtocol:@protocol(MealPlannerAskViewDelegate)]) {
		[self.delegate mealPlannerAskView:self didFinish:NO];
	}
}

- (IBAction)done:(id)sender
{
	if (self.delegate != nil 
			&& [self.delegate conformsToProtocol:@protocol(MealPlannerAskViewDelegate)]) {
		[self.delegate mealPlannerAskView:self didFinish:YES];
	}
}

@end
