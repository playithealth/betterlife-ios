//
//  BLOnboardLifestyleViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 3/29/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLOnboardLifestyleViewController.h"

#import "BLHeaderFooterMultiline.h"
#import "BLOnboardWelcomeViewController.h"
#import "HealthChoiceMO.h"
#import "User.h"
#import "UserHealthChoice.h"

@interface BLOnboardLifestyleViewController ()
@property (strong, nonatomic) NSArray *healthChoices;
@end

@implementation BLOnboardLifestyleViewController

static const NSInteger kSectionLifestyle = 0;
static const NSInteger kSectionAllCriteria = 1;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

	[self.tableView registerClass:[BLHeaderFooterMultiline class] forHeaderFooterViewReuseIdentifier:@"Multiline"];

	self.title = [NSString stringWithFormat:@"Welcome: %ld of %ld",
		(long)[[self.navigationController viewControllers] count] - 1, (long)[self.viewStack count] - 1];

	self.healthChoices = [HealthChoiceMO MR_findByAttribute:HealthChoiceMOAttributes.choiceType
		withValue:[NSNumber numberWithInteger:(self.isLifestyle ? 1 : 0)] andOrderBy:HealthChoiceMOAttributes.sortOrder ascending:YES
		inContext:self.managedObjectContext];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rows = 0;

	if (section == 0) {
		if (self.isLifestyle) {
			rows = [self.healthChoices count] - 1;
		}
		else {
			rows = [self.healthChoices count];
		}
	}
	else {
		if (self.isLifestyle) {
			rows = 1;
		}
		else {
			rows = 0;
		}
	}

	return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	// Special case for lifestyle, as we don't want row zero, which is required_lifestyle, to show up in section zero
	NSInteger dataRow = (![indexPath section] && self.isLifestyle ? [indexPath row] + 1 : [indexPath row]);

	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Lifestyle"];
	if (!cell.accessoryView) {
		UISwitch *lifestyleSwitch = [[UISwitch alloc] init];
		[lifestyleSwitch addTarget:self action:@selector(lifestyleValueChanged:) forControlEvents:UIControlEventValueChanged];
		cell.accessoryView = lifestyleSwitch;
	}
	cell.accessoryView.tag = dataRow; // For easier handline when the value changes

	HealthChoiceMO *choice = [self.healthChoices objectAtIndex:dataRow];

	UserHealthChoice *userChoice = nil;
	NSSet *userChoices = [[choice userHealthChoices] filteredSetUsingPredicate:
		[NSPredicate predicateWithFormat:@"%K = %@", 
		UserHealthChoiceAttributes.loginId, [[User currentUser] loginId]]];

	userChoice = [userChoices anyObject];
	UISwitch *lifestyleSwitch = (UISwitch *)cell.accessoryView;
	if (!userChoice || !userChoice.value) {
		lifestyleSwitch.on = NO;
	}
	else {
		lifestyleSwitch.on = YES;
	}

	if ([[choice detailText] length]) {
		cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)", [choice name], [choice detailText]];
	}
	else {
		// Special case for lifestyle section 1 require all
		if (([indexPath section] == 1) && ([indexPath row] == 0)) {
			cell.textLabel.text = @"Only show icon when all met";
		}
		else {
			cell.textLabel.text = [choice name];
		}
	}

    return cell;
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat height = UITableViewAutomaticDimension;
	switch (section) {
		case kSectionLifestyle:
			height = 84;
			break;
		case kSectionAllCriteria:
			if (self.isLifestyle) {
				height = 0;
			}
			else {
				height = 704;
			}
			break;
	}
	return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	CGFloat height = UITableViewAutomaticDimension;
	switch (section) {
		case kSectionLifestyle:
			if (self.isLifestyle) {
				height = 60;
			}
			else {
				height = 114;
			}
			break;
		case kSectionAllCriteria:
			height = 0;
			break;
	}
	return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *headerView = nil;

	switch (section) {
		case kSectionLifestyle:
			headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
			headerView.multilineLabel.text = @"All of this information is optional, but the more you enter, "
				"the more we can do to help you manage your health.";
			if (self.isLifestyle) {
				headerView.normalLabel.text = @"SHOW LIFESTYLE ASSISTANCE";
				headerView.headerImageView.image = [UIImage imageNamed:@"leaf"];
			}
			else {
				headerView.normalLabel.text = @"SHOW ALLERGY WARNINGS";
				headerView.headerImageView.image = [UIImage imageNamed:@"alert"];
			}
			break;
		case kSectionAllCriteria:
			if (!self.isLifestyle) {
				headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
				headerView.multilineLabel.text =
					@"While we work to ensure that product information is correct, on occasion manufacturers may alter their "
					"ingredient lists. Actual product packaging and materials may contain more and/or different information than "
					"that shown on our Web site. For these reasons you should not rely on the information presented, but should "
					"always read labels, warnings, and directions before using or consuming a product. For additional information "
					"about a product, please contact the manufacturer. Content on this site is for reference purposes and is not "
					"intended to substitute for advice given by a physician, pharmacist, or other licensed health-care professional. "
					"You should not use this information as self-diagnosis or for treating a health problem or disease. "
					"Contact your health-care provider immediately if you suspect that you have a medical problem. "
					"Information and statements regarding dietary supplements have not been evaluated by the Food and Drug "
					"Administration and are not intended to diagnose, treat, cure, or prevent any disease or health condition. "
					"Neither BettrLife Corporation, its Content provider(s), nor product manufacturers assume any liability for "
					"inaccuracies, misstatements, or omissions. BETTRLIFE CORPORATION PROVIDES NO WARRANTY, EXPRESS OR IMPLIED, "
					"REGARDING THE INFORMATION PROVIDED. INDIVIDUALS WHO HAVE ALLERGIES OR OTHER REACTIONS TO FOODS OR OTHER "
					"SUBSTANCES DESCRIBED ON THE WEBSITE SHOULD NOT RELY ON THE INFORMATION PROVIDED AS BEING ACCURATE OR COMPLETE "
					"AND SHOULD CONDUCT THEIR OWN INVESTIGATION REGARDING THE INFORMATION PRESENTED.";
			}
			break;
	}

	return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	BLHeaderFooterMultiline *footerView = nil;
	switch (section) {
		case kSectionLifestyle:
			footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Multiline"];
			if (self.isLifestyle) {
				footerView.multilineLabel.text = @"A healthy choice icon will be shown for selected lifestyle choices "
					"during product searches and in product info.";
			}
			else {
				footerView.multilineLabel.text = @"A warning icon will be shown for selected allergies during "
					"product searches and in product info.\n\n"
					"Custom ingredient warnings can be set in the Account section of this app.";
			}
			break;
		case kSectionAllCriteria:
			break;
	}

	return footerView;
}

#pragma mark - local
- (IBAction)lifestyleValueChanged:(UISwitch *)sender
{
	HealthChoiceMO *choice = [self.healthChoices objectAtIndex:sender.tag];
	if (choice) {
		UserHealthChoice *userChoice = nil;
		NSSet *userChoices = [[choice userHealthChoices] filteredSetUsingPredicate:
			[NSPredicate predicateWithFormat:@"%K = %@", 
			UserHealthChoiceAttributes.loginId, [[User currentUser] loginId]]];
		userChoice = [userChoices anyObject];

		userChoice.value = (sender.on ? @0 : nil);

		userChoice.status = kStatusPut;

		[self.managedObjectContext BL_save];
	}
}

#pragma mark - responders
- (IBAction)nextViewController:(id)sender {
	if (self.delegate) {
		[self.delegate nextView];
	}
}

@end
