//
//  BCLoggingSearchMainViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/9/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCDetailViewDelegate.h"

typedef enum {
	BCLoggingTypeFood = 0,
	BCLoggingTypeActivity = 1,
	BCLoggingTypeStrength = 2,
} BCLoggingType;

@interface BCLoggingSearchMainViewController : UIViewController <BCDetailViewDelegate>

@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSString *headerText;
@property (strong, nonatomic) NSDate *logDate;
@property (strong, nonatomic) NSNumber *logTime;
@property (assign, nonatomic) BCLoggingType loggingType;

@end
