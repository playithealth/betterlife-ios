//
//  BCEditCustomFoodViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 12/16/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomFoodMO;
@protocol BCDetailViewDelegate;

@interface BCEditCustomFoodViewController : UIViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) CustomFoodMO *customFood;
@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;
@end
