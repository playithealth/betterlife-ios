//
//  BLOnboardViewDelegate.h
//  BettrLife
//
//  Created by Greg Goodrich on 9/16/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

@protocol BLOnboardDelegate;

@protocol BLOnboardViewDelegate <NSObject>
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *viewStack;
@property (weak, nonatomic) id <BLOnboardDelegate> delegate;
@end
