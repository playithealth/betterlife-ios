//
//  BLLogTimeTabBar.m
//  BettrLife
//
//  Created by Greg Goodrich on 4/29/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLLogTimeTabBar.h"

#import "UIColor+Additions.h"

@interface BLLogTimeTabBar ()
@property (strong, nonatomic) NSArray *tabNames;
@property (strong, nonatomic) NSMutableArray *tabs;
@end

@implementation BLLogTimeTabBar

- (id)initWithCoder:(NSCoder *)decoder
{
	self = [super initWithCoder:decoder];
    if (self) {
        // Custom initialization
		self.tabNames = @[ @"Total", @"Bkf", @"Mor", @"Lun", @"Aft", @"Din", @"Eve" ];
		self.tabs = [NSMutableArray array];

		id priorTabView = [NSNull null];
		for (NSInteger idx = 0; idx < [self.tabNames count]; idx++) {
			// Construct the view for each tab
//			UIView *tabView = [[UIView alloc] init];
			UIButton *tabView = [UIButton buttonWithType:UIButtonTypeCustom];
			tabView.tag = idx; // For tracking which button is pressed
			[tabView addTarget:self action:@selector(tabTapped:) forControlEvents:UIControlEventTouchUpInside];
			[tabView setTitleColor:[UIColor BL_linkColor] forState:UIControlStateNormal];
			[tabView setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
			tabView.backgroundColor = [UIColor BL_tabColor];
			[tabView setTitle:[self.tabNames objectAtIndex:idx] forState:UIControlStateNormal];
			tabView.titleLabel.font = [UIFont systemFontOfSize:15];
			tabView.translatesAutoresizingMaskIntoConstraints = NO;
			[self addSubview:tabView];
//			UILabel *tabLabel = [[UILabel alloc] init];
//			tabLabel.translatesAutoresizingMaskIntoConstraints = NO;
//			tabLabel.text = [self.tabNames objectAtIndex:idx];
//			[tabView addSubview:tabLabel];
			[self.tabs addObject:tabView];

			// TODO In order to run properly in landscape mode, the 50 and 45 widths would need to be adapted to be dynamic somehow
			NSDictionary *bindingsDict = NSDictionaryOfVariableBindings(tabView, priorTabView);
			// Deal with the horizontal constraints
			if (idx == 0) {
				tabView.selected = YES;
				tabView.backgroundColor = [UIColor BL_linkColor];
				[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[tabView(50)]"
					options:0 metrics:nil views:bindingsDict]];
			}
			else if (idx == [self.tabNames count] - 1) {
				[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[tabView(45)]-0-|"
					options:0 metrics:nil views:bindingsDict]];
			}
			else {
				[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[priorTabView]-0-[tabView(45)]"
					options:0 metrics:nil views:bindingsDict]];
			}

			// Deal with the vertical constraints
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[tabView]-0-|"
				options:0 metrics:nil views:bindingsDict]];

//			[tabView addConstraint:[NSLayoutConstraint constraintWithItem:tabLabel attribute:NSLayoutAttributeCenterX
//				relatedBy:NSLayoutRelationEqual toItem:tabView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
//			[tabView addConstraint:[NSLayoutConstraint constraintWithItem:tabLabel attribute:NSLayoutAttributeCenterY
//				relatedBy:NSLayoutRelationEqual toItem:tabView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];

			priorTabView = tabView;
		}

		CGFloat	separatorHeight = 1.0 / [[UIScreen mainScreen] scale];
		UIView *border = [[UIView alloc]
			initWithFrame:CGRectMake(0, 0, self.bounds.size.width, separatorHeight)];
		border.backgroundColor = [UIColor BL_darkLineColor];
		[self addSubview:border];
		border = [[UIView alloc]
			initWithFrame:CGRectMake(0, self.bounds.size.height - separatorHeight, self.bounds.size.width, separatorHeight)];
		border.backgroundColor = [UIColor BL_lightLineColor];
		[self addSubview:border];

	}
	return self;
}

- (void)tabTapped:(UIButton *)tabButton
{
	if (self.selectedTab != tabButton.tag) {
		// First, set the old selection back to normal
		UIButton *oldSelection = [self.tabs objectAtIndex:self.selectedTab];
		oldSelection.selected = NO;
		oldSelection.backgroundColor = [UIColor BC_lightGrayColor];

		// Now, 'select' the new selection
		tabButton.selected = YES;
		tabButton.backgroundColor = [UIColor BL_linkColor];

		self.selectedTab = tabButton.tag;
		[self sendActionsForControlEvents:UIControlEventValueChanged];
	}

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
