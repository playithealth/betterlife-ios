//
//  TrainingActivityViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 2/22/12.
//  Copyright (c) 2012 BettrLife Corporation. All rights reserved.
//

#import "TrainingActivityViewController.h"

#import "BCPopupView.h"
#import "BCTextFieldCell.h"
#import "ExerciseRoutineMO.h"
#import "GenericValueDisplay.h"
#import "NumberValue.h"
#import "TrackingMO.h"
#import "UIColor+Additions.h"
#import "User.h"

static const NSInteger kTagWeightPopupView = 1;
static const NSInteger kTagTimePopupView = 2;
static const NSInteger kTagActivityPopupView = 3;
static const NSInteger kTagWeightPickerView = 4;
static const NSInteger kTagTimePickerView = 5;

static const NSInteger kScrollInset = 300;

@interface TrainingActivityViewController () <UIPickerViewDelegate, UIPickerViewDataSource, BCPopupViewDelegate>
@property (strong, nonatomic) NSArray *rowLabels;
@property (strong, nonatomic) NSArray *rowKeys;
@property (assign, nonatomic) BOOL changedLog;
@property (strong, nonatomic) UITextField *editingTextField;
@property (strong, nonatomic) NSIndexPath *editingIndexPath;
@property (strong, nonatomic) NSMutableArray *cells;
@property (strong, nonatomic) NSArray *exerciseRoutines;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)showWeightPopup;
- (void)showTimePopup;
- (void)showActivityPopup;
- (NSInteger)calculateCalories;
@end

@implementation TrainingActivityViewController
{
	NSInteger _hours;
	NSInteger _minutes;
	NSInteger _seconds;
}
@synthesize tableView;
@synthesize trainingActivity;
@synthesize delegate;
@synthesize rowLabels;
@synthesize rowKeys;
@synthesize changedLog;
@synthesize editingTextField;
@synthesize editingIndexPath;
@synthesize cells;
@synthesize exerciseRoutines;

- (id)initWithLog:(TrainingActivityMO *)log
{
    self = [self init];
    if (self) {
		self.trainingActivity = log;

		self.rowLabels = [NSArray arrayWithObjects:
			@"Weight",
			@"Time",
			@"Activity",
			@"Calories",
			nil];

		self.rowKeys = [NSArray arrayWithObjects:
			@"weight",
			@"time",
			@"activity",
			@"calories",
			nil];
		
		self.changedLog = NO;
	}

    return self;
}

- (void)loadView
{
	UITableView *aTableView = [[UITableView alloc] initWithFrame:CGRectZero 
		style:UITableViewStyleGrouped];
	aTableView.delegate = self;
	aTableView.dataSource = self;
	self.view = aTableView;
	self.tableView = aTableView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.tableView.backgroundColor = [UIColor colorWithPatternImage:
		[UIImage imageNamed:@"background.png"]];

	self.cells = [NSMutableArray arrayWithCapacity:[self.rowLabels count]];

	// create the cells
	// current weight
	UITableViewCell *aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:nil];
	[self.cells addObject:aCell];

	// time
	aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:nil];
	[self.cells addObject:aCell];

	// activity
	aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:nil];
	[self.cells addObject:aCell];

	// calories expended
	aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:nil];
	[self.cells addObject:aCell];

	// load the exercise routines
	self.exerciseRoutines = [ExerciseRoutineMO MR_findAllSortedBy:ExerciseRoutineMOAttributes.activity ascending:YES
		inContext:self.managedObjectContext];

	if (![self.trainingActivity.weight integerValue]) {
		self.trainingActivity.weight = [self getLastWeightTrack];
	}
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

	self.title = @"Activity Training";
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillDisappear:(BOOL)animated
{
	// notify delegate
	if (self.changedLog) {
		if (delegate && [delegate conformsToProtocol:@protocol(TrainingActivityViewDelegate)]) {
			[delegate trainingActivityView:self didUpdateLog:self.trainingActivity];
		}
	}

	self.changedLog = NO;

    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView 
numberOfRowsInSection:(NSInteger)section
{
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.cells objectAtIndex:indexPath.row];
    
	// configure cell
	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.editingIndexPath = indexPath;

	if (indexPath.row == 0) {
		[self showWeightPopup];
	}
	else if (indexPath.row == 1) {
		[self showTimePopup];
	}
	else if (indexPath.row == 2) {
		[self showActivityPopup];
	}

	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Local Methods
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	cell.textLabel.text = [self.rowLabels objectAtIndex:indexPath.row];
	id <GenericValueDisplay, NSObject> rowValue = [self.trainingActivity 
		valueForKey:[self.rowKeys objectAtIndex:indexPath.row]];
	cell.detailTextLabel.text = [rowValue genericValueDisplay];
}

- (NSInteger)calculateCalories
{
	NSInteger calories = 0;
	if (self.trainingActivity.exerciseRoutine && [self.trainingActivity.weight integerValue] > 0) {
		double calculatedCalories = [self.trainingActivity.exerciseRoutine.factor doubleValue] * [self.trainingActivity.timeInMinutes doubleValue] * [self.trainingActivity.weight doubleValue];
		calories = calculatedCalories;
	}

	//QuietLog(@"%s: %.2f", __FUNCTION__, calories);

	return calories;
}

- (void)showWeightPopup
{
	self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, kScrollInset, 0.0);

	[self.tableView scrollToRowAtIndexPath:self.editingIndexPath
		atScrollPosition:UITableViewScrollPositionMiddle animated:YES];

	UIPickerView *aPickerView = [[UIPickerView alloc] init];
	aPickerView.dataSource = self;
	aPickerView.delegate = self;
	aPickerView.showsSelectionIndicator = YES;
	aPickerView.tag = kTagWeightPickerView;

	if (self.trainingActivity.weight) {
		NSInteger firstComponentIndex = [self.trainingActivity.weight integerValue];
		NSInteger secondComponentIndex = ([self.trainingActivity.weight doubleValue] * 10) - (firstComponentIndex * 10);

		[aPickerView selectRow:firstComponentIndex inComponent:0 animated:NO];
		[aPickerView selectRow:secondComponentIndex inComponent:1 animated:NO];
	}

	BCPopupView *aPopupView = [[BCPopupView alloc] 
		initWithView:aPickerView
		caretPosition:CGPointMake(160, 120)
		title:@"Enter your current weight:"
		delegate:self];
	aPopupView.tag = kTagWeightPopupView;

	UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	doneButton.frame = CGRectMake(0, 0, 80, 37);
	[doneButton setTitle:@"Done" forState:UIControlStateNormal];
	[aPopupView addSubview:doneButton];
	doneButton.center = CGPointMake(160, 420);

	UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]
		initWithTarget:aPopupView action:@selector(handleDoneButton:)];
	[doneButton addGestureRecognizer:recognizer];

	[self.navigationController.view addSubview:aPopupView];
}

- (void)showTimePopup
{
	self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, kScrollInset, 0.0);

	[self.tableView scrollToRowAtIndexPath:self.editingIndexPath
		atScrollPosition:UITableViewScrollPositionMiddle animated:YES];

	UIPickerView *aPickerView = [[UIPickerView alloc] init];
	aPickerView.dataSource = self;
	aPickerView.delegate = self;
	aPickerView.showsSelectionIndicator = YES;
	aPickerView.tag = kTagTimePickerView;

	if (self.trainingActivity.time) {
		NSArray *timeComponents = [self.trainingActivity.time componentsSeparatedByString:@":"];
		
		if ([timeComponents count] == 3) {
			NSInteger firstComponentIndex = [[timeComponents objectAtIndex:0] integerValue];
			NSInteger secondComponentIndex = [[timeComponents objectAtIndex:1] integerValue];
			NSInteger thirdComponentIndex = [[timeComponents objectAtIndex:2] integerValue];

			[aPickerView selectRow:firstComponentIndex inComponent:0 animated:NO];
			[aPickerView selectRow:secondComponentIndex inComponent:1 animated:NO];
			[aPickerView selectRow:thirdComponentIndex inComponent:2 animated:NO];
		}
	}

	BCPopupView *aPopupView = [[BCPopupView alloc] 
		initWithView:aPickerView
		caretPosition:CGPointMake(160, 120)
		title:@"Enter time spent:"
		delegate:self];
	aPopupView.tag = kTagTimePopupView;

	UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	doneButton.frame = CGRectMake(0, 0, 80, 37);
	[doneButton setTitle:@"Done" forState:UIControlStateNormal];
	[aPopupView addSubview:doneButton];
	doneButton.center = CGPointMake(160, 420);

	UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]
		initWithTarget:aPopupView action:@selector(handleDoneButton:)];
	[doneButton addGestureRecognizer:recognizer];

	[self.navigationController.view addSubview:aPopupView];
}

- (void)showActivityPopup
{
	self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, kScrollInset, 0.0);

	[self.tableView scrollToRowAtIndexPath:self.editingIndexPath
		atScrollPosition:UITableViewScrollPositionMiddle animated:YES];

	BCPopupView *aPopupView = [[BCPopupView alloc] 
		initWithTableViewStyle:UITableViewStylePlain
		caretPosition:CGPointMake(160, 120)
		title:@"Choose an activity:"
		delegate:self
		rowTitles:[exerciseRoutines valueForKeyPath:ExerciseRoutineMOAttributes.activity]];
	aPopupView.tag = kTagActivityPopupView;

	[self.navigationController.view addSubview:aPopupView];
}

- (id)getLastWeightTrack
{
	User *thisUser = [User currentUser];

	// fetch the assessments ordered by date
	NSError *error = nil;
	NSSortDescriptor *sortByDate = [NSSortDescriptor 
		sortDescriptorWithKey:@"date" ascending:NO];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:
		@"loginId = %@ AND status <> %@ AND trackingType = %d",
		thisUser.loginId, kStatusDelete, TrackingTypes.weight];
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	request.entity = [TrackingMO entityInManagedObjectContext:self.managedObjectContext];
	request.predicate = predicate;
	request.sortDescriptors = [NSArray arrayWithObject:sortByDate];
	request.fetchLimit = 1;

	NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];

	if (![results count]) {
		return nil;
	}

	TrackingMO *weightTrack = [results objectAtIndex:0];
    NSDictionary *trackingDict = weightTrack.valueDictionary;
	NSNumber *weight = [[trackingDict objectForKey:@"weight"] numberValueDecimal];

	return weight;
}

- (IBAction)handleDoneButton:(id)sender
{
	
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	NSInteger numberOfComponents = 0;
	if (pickerView.tag == kTagWeightPickerView) {
		numberOfComponents = 2;
	}
	else if (pickerView.tag == kTagTimePickerView) {
		numberOfComponents = 3;
	}
	return numberOfComponents;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	NSInteger numberOfRows = 0;
	if (pickerView.tag == kTagWeightPickerView) {
		if (component == 0) {
			numberOfRows = 1000;
		}
		else {
			numberOfRows = 10;
		}
	}
	else if (pickerView.tag == kTagTimePickerView) {
		if (component == 0) {
			numberOfRows = 25;
		}
		else {
			numberOfRows = 61;
		}
	}
	return numberOfRows;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *title = nil;
	if (pickerView.tag == kTagWeightPickerView) {
		if (component == 0) {
			title = [NSString stringWithFormat:@"%ld", (long)row];
		}
		else {
			title = [NSString stringWithFormat:@".%ld", (long)row];
		}
	}
	else if (pickerView.tag == kTagTimePickerView) {
		if (component == 0) {
			title = [NSString stringWithFormat:@"%ld", (long)row];
		}
		else {
			title = [NSString stringWithFormat:@"%02ld", (long)row];
		}
	}
	return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	//QuietLog(@"row %d component %d", row, component);
}

#pragma mark - BCPopupViewDelegate
- (void)popupView:(BCPopupView *)inPopup
didDismissWithSelectedIndexPath:(NSIndexPath *)indexPath
{
	self.tableView.contentInset = UIEdgeInsetsZero;

	// bail if the user cancelled the popup
	if (!indexPath) {
		return;
	}

	if (inPopup.tag == kTagActivityPopupView) {
		//QuietLog(@"selected activity %d", indexPath.row);
		
		ExerciseRoutineMO *selectedRoutine = [self.exerciseRoutines objectAtIndex:indexPath.row];

		self.trainingActivity.exerciseRoutine = selectedRoutine;
		self.trainingActivity.activity = selectedRoutine.activity;
		self.trainingActivity.factor = selectedRoutine.factor;
		self.trainingActivity.calories = [NSNumber numberWithInteger:[self calculateCalories]];

		self.changedLog = YES;
	}

	[self.tableView reloadData];
}

- (void)popupView:(BCPopupView *)inPopup didDismissWithInfo:(NSDictionary *)userInfo
{
	if (inPopup.tag == kTagWeightPopupView) {
		UIPickerView *pickerView = (UIPickerView *)inPopup.customView;
		NSInteger firstIndex = [pickerView selectedRowInComponent:0];
		NSInteger secondIndex = [pickerView selectedRowInComponent:1];
		self.trainingActivity.weight = [NSNumber numberWithDouble:firstIndex + ((double)secondIndex / 10)];
		self.changedLog = YES;
	}
	else if (inPopup.tag == kTagTimePopupView) {
		UIPickerView *pickerView = (UIPickerView *)inPopup.customView;
		_hours = [pickerView selectedRowInComponent:0];
		_minutes = [pickerView selectedRowInComponent:1];
		_seconds = [pickerView selectedRowInComponent:2];
		self.trainingActivity.time = [NSString stringWithFormat:@"%ld:%02ld:%02ld", (long)_hours, (long)_minutes, (long)_seconds];
		self.changedLog = YES;
	}

	self.trainingActivity.calories = [NSNumber numberWithInteger:[self calculateCalories]];

	[self.tableView reloadData];
}

@end
