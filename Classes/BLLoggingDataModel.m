//
//  BLLoggingDataModel.m
//  BettrLife
//
//  Created by Greg Goodrich on 04/22/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLLoggingDataModel.h"

#import "ActivityLogMO.h"
#import "ActivityPlanMO.h"
#import "AdvisorActivityPlanMO.h"
#import "AdvisorActivityPlanPhaseMO.h"
#import "AdvisorActivityPlanWorkoutMO.h"
#import "AdvisorActivityPlanWorkoutActivityMO.h"
#import "AdvisorMealPlanEntryMO.h"
#import "AdvisorMealPlanMO.h"
#import "FoodLogMO.h"
#import "LoggingNoteMO.h"
#import "MealPlannerDay.h"
#import "MealPredictionMO.h"
#import "NSCalendar+Additions.h"
#import "NutrientMO.h"
#import "NutritionMO.h"
#import "User.h"
#import "UserProfileMO.h"

@interface BLLoggingDataModel ()
@property (strong, nonatomic) NSCalendar *calendar;
@property (strong, nonatomic) UserProfileMO *userProfileMO;
@end

@implementation BLLoggingDataModel

static const NSInteger kMaxPredictions = 3;

- (id)initWithMOC:(NSManagedObjectContext *)moc
{
	self = [super init];
	if (self) {
		self.managedObjectContext = moc;
		self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

		// This is important, as nutrient sync uses a delete strategy, so these can just go away if they re-sync
		[self reloadNutrients:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadNutrients:) name:[NutrientMO entityName] object:nil];

		self.userProfileMO = [UserProfileMO getCurrentUserProfileInMOC:self.managedObjectContext];

		self.foodLogsByLogTime = [[NSMutableArray alloc] initWithCapacity:6];
		self.nutritionByLogTime = [[NSMutableArray alloc] initWithCapacity:6];
		[self setDayOffset:0];
	}

	return self;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:[NutrientMO entityName] object:nil];
}

- (void)setDayOffset:(NSInteger)dayOffset
{
	_dayOffset = dayOffset;

	NSDateComponents *components = [self.calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
		fromDate:[NSDate date]];

	components.hour = 0;
	components.minute = 0;
	components.second = 0;

	NSDate *todayStart = [self.calendar dateFromComponents:components];

	NSDateComponents *offset = [[NSDateComponents alloc] init];
	offset.day = _dayOffset;
	_startOfLogDate = [self.calendar dateByAddingComponents:offset toDate:todayStart options:0];

	components.hour = 23;
	components.minute = 59;
	components.second = 59;

	NSDate *todayEnd = [self.calendar dateFromComponents:components];

	_endOfLogDate = [self.calendar dateByAddingComponents:offset toDate:todayEnd options:0];

	// Load the goal info for the day
	self.goalInfo = [self.userProfileMO goalInfoForDate:_startOfLogDate];

	// NOTE: There isn't always a meal plan entry for each logtime, so need to adapt for that
	NSMutableDictionary *mpPercentagesByLogTime = [@{
			@0 : [NSNull null],
			@1 : [NSNull null],
			@2 : [NSNull null],
			@3 : [NSNull null],
			@4 : [NSNull null],
			@5 : [NSNull null]}
		mutableCopy];
	for (AdvisorMealPlanEntryMO *entry in self.goalInfo.mealPlanEntries) {
		if (entry.nutritionPercent) {
			[mpPercentagesByLogTime setObject:entry.nutritionPercent forKey:entry.logTime];
		}
	}

	double unassigned = 0;
	double totalPercent = 0;
	for (NSInteger idx = 0; idx < 6; idx++) {
		NSNumber *percent = [mpPercentagesByLogTime objectForKey:@(idx)];
		if (percent == (id)[NSNull null]) {
			// We don't currently prorate snacks individually, so ignore them if they're unassigned
			if ((idx != LogTimeMorningSnack) && (idx != LogTimeAfternoonSnack) && (idx != LogTimeEveningSnack)) {
				unassigned++;
			}
		}
		else {
			totalPercent += [percent integerValue];
		}
	}
	double allocPercent = (totalPercent < 100 ? (100 - totalPercent) / unassigned : 0);
	// Now loop through the percentages again, this time assigning prorated percents that are cumulative
	totalPercent = 0.0;
	NSDictionary *minNutrition = self.goalInfo.minNutrition;
	NSDictionary *maxNutrition = self.goalInfo.maxNutrition;
	self.proRatedByLogTime = [[NSMutableArray alloc] init];
	for (NSInteger idx = 0; idx < 6; idx++) {
		NSNumber *percent = [mpPercentagesByLogTime objectForKey:@(idx)];
		if (percent == (id)[NSNull null]) {
			if ((idx != LogTimeMorningSnack) && (idx != LogTimeAfternoonSnack) && (idx != LogTimeEveningSnack)) {
				totalPercent += allocPercent;
			}
		}
		else {
			totalPercent += [percent integerValue];
		}

		// Calculate the prorated goal values
		NSMutableDictionary *proRatedMin = [[NSMutableDictionary alloc] init];
		NSMutableDictionary *proRatedMax = [[NSMutableDictionary alloc] init];
		double margin = (idx < LogTimeDinner ? 10 : 0); // Give a 10% range +- for breakfast/lunch
		double adjustedMinPercent = (totalPercent - margin > 0 ? (totalPercent - margin) / 100.0 : 0);
		double adjustedMaxPercent = (totalPercent + margin) / 100.0;
		for (NSString *nutrientName in self.goalInfo.goalNutrients) {
			if ([minNutrition valueForKey:nutrientName]) {
				[proRatedMin setValue:@(adjustedMinPercent * [[minNutrition valueForKey:nutrientName] doubleValue])
					forKey:nutrientName];
			}
			if ([maxNutrition valueForKey:nutrientName]) {
				[proRatedMax setValue:@(adjustedMaxPercent * [[maxNutrition valueForKey:nutrientName] doubleValue])
					forKey:nutrientName];
			}
		}
		[self.proRatedByLogTime addObject:@{@"minNutrition" : proRatedMin, @"maxNutrition" : proRatedMax}];
	}
}

- (BLSmiley)complianceThroughLogTime:(BLLogTime)logTime
{
	BLSmiley smiley = SmileyHappy;
	if (logTime != LogTimeNone) {
		NSDictionary *proRated = [self.proRatedByLogTime objectAtIndex:logTime];
		NSDictionary *minNutrition = [proRated valueForKey:@"minNutrition"];
		NSDictionary *maxNutrition = [proRated valueForKey:@"maxNutrition"];

		// Calculate the nutrition through the given log time
		NSArray *subArray = [self.nutritionByLogTime subarrayWithRange:NSMakeRange(0, logTime + 1)];
		NSMutableDictionary *nutritionThruLogTime = [[NSMutableDictionary alloc] init];
		for (NutrientMO *nutrient in self.nutrients) {
			NSString *keyPath = [NSString stringWithFormat:@"@sum.%@", nutrient.name];
			NSNumber *totalValue = [subArray valueForKeyPath:keyPath];
			[nutritionThruLogTime setValue:totalValue forKey:nutrient.name];
		}

		double compliantNutrients = 0;
		for (NSString *nutrientName in self.goalInfo.goalNutrients) {
			NSNumber *minValue = [minNutrition valueForKey:nutrientName];
			NSNumber *maxValue = [maxNutrition valueForKey:nutrientName];
			NSNumber *value = [nutritionThruLogTime valueForKey:nutrientName];
			// If we're within the min and max value range of this nutrient, then this nutrient is compliant
			if ((!minValue || ([value doubleValue] >= [minValue doubleValue]))
					&& (!maxValue || ([value doubleValue] <= [maxValue doubleValue]))) {
				compliantNutrients++;
			}
		}

		double totalNutrients = [self.goalInfo.goalNutrients count];
		double percentNutrients = compliantNutrients / totalNutrients;
		if ((percentNutrients >= 0.5) && (percentNutrients < 1.0)) {
			smiley = SmileyMeh;
		}
		else if (percentNutrients < 0.5) {
			smiley = SmileySad;
		}
	}

	return smiley;
}

- (BLSmiley)currentCompliance
{
	return [self complianceThroughLogTime:self.latestLoggedTime];
}

- (void)updateTotalNutrition
{
	// Calculate total nutrition
	if (self.nutritionByLogTime) {
		NSMutableDictionary *totalNutrition = [[NSMutableDictionary alloc] init];
		for (NutrientMO *nutrient in self.nutrients) {
			NSString *keyPath = [NSString stringWithFormat:@"@sum.%@", nutrient.name];
			NSNumber *totalValue = [self.nutritionByLogTime valueForKeyPath:keyPath];
			if (totalValue) {
				[totalNutrition setValue:totalValue forKey:nutrient.name];
			}
		}

		self.totalNutrition = totalNutrition;
	}
}

- (NSDictionary *)updateNutritionForLogTime:(NSNumber *)logTime
{
	NSMutableDictionary *logTimeNutrition = [[NSMutableDictionary alloc] init];

	NSArray *logItems = [self.foodLogsByLogTime objectAtIndex:[logTime integerValue]];
	for (id logItem in logItems) {
		if ([logItem isKindOfClass:[FoodLogMO class]]) {
			// First, update the latest logged time - Don't use LogTimeNone, as it doesn't seem to work to have a negative in MAX()
			FoodLogMO *foodLog = logItem;
			//calories += foodLog.nutrition.caloriesValue * foodLog.nutritionFactorValue * foodLog.servingsValue;
			for (NutrientMO *nutrient in self.nutrients) {
				double value = [[foodLog.nutrition valueForKey:nutrient.name] doubleValue] * foodLog.nutritionFactorValue * foodLog.servingsValue;
				[logTimeNutrition setValue:@([[logTimeNutrition valueForKey:nutrient.name] doubleValue] + value) forKey:nutrient.name];
			}
		}
	}

	return logTimeNutrition;
}

- (BLLogTime)latestLoggedTime
{
	// Iterate backwards over the foodLogsByLogTime array for efficiency, and stop when we find any FoodLogMO object
	for (NSArray *foodLogs in [self.foodLogsByLogTime reverseObjectEnumerator]) {
		for (id foodLog in foodLogs) {
			if ([foodLog isKindOfClass:[FoodLogMO class]]) {
				return [[foodLog logTime] integerValue];
			}
		}
	}

	return LogTimeNone;
}

// UNUSED!!!???
- (void)loadLoggingData
{
	// Load food logs
	for (NSInteger logTime = 0; logTime < 6; logTime++) {
		[self loadFoodLogsForLogTime:@(logTime)];
	}

	// Load water logs
	[self loadWaterLogs];

	// Load activity logs
	[self loadActivityLogs];

	// Load food notes
	[self loadLoggingNotesOfType:kLoggingNoteTypeFood];

	// Load activity notes
	[self loadLoggingNotesOfType:kLoggingNoteTypeExercise];

}

- (NSMutableArray *)loadFoodLogsForLogTime:(NSNumber *)logTime
{
	// Load up all logged items
	NSMutableArray *existingLogs = [[FoodLogMO findLogsForLogTime:logTime after:self.startOfLogDate before:self.endOfLogDate
		inContext:self.managedObjectContext] mutableCopy];

	BOOL hasLoggedItems = ([existingLogs count] ? YES : NO);
	NSMutableArray *predictions = [[NSMutableArray alloc] init];

	// Load up all the planned items
	NSMutableArray *mealPlans = [[MealPlannerDay MR_findAllSortedBy:@"date,sortOrder" ascending:YES 
		withPredicate:[NSPredicate predicateWithFormat:@"%K = %@ AND date BETWEEN {%@, %@} AND status <> %@ AND accountId = %@", 
			MealPlannerDayAttributes.logTime, logTime, self.startOfLogDate, self.endOfLogDate, kStatusDelete, [[User currentUser] accountId]]
		inContext:self.managedObjectContext] mutableCopy];

	// Match up planned items with logged items
	for (MealPlannerDay *mpd in mealPlans) {
		BOOL found = NO;
		for (FoodLogMO *foodLog in existingLogs) {
			if (([mpd.itemType isEqualToString:foodLog.itemType]) && ([[mpd itemId] integerValue] == [[foodLog itemId] integerValue])) {
				found = YES;
				break;
			}
		}
		if (!found) {
			// Didn't find a matching log item, so add to existingLogs
			[existingLogs addObject:mpd];
		}
	}

	// Get the advisor meal plan tags for this day, if they have any
	// TODO Should we be limiting how far back in the past this works, like the predictions do?
	AdvisorMealPlanEntryMO *planEntry = nil;
	if (self.advisorMealPlanMO) {
		planEntry = [self.advisorMealPlanMO getMealPlanEntryForDate:self.startOfLogDate atLogTime:logTime];

		// Match up tags with existingItems, and add the ones that don't match
		for (AdvisorMealPlanTagMO *tag in planEntry.tags) {
			// Search for a planned item for this tag
			// NOTE: Could possibly iterate over existingLogs looking for this, but for now, just use tag.foodLogs
			// NOTE: This should only ever return one item in the set, hence using anyObject
			BOOL found = NO;
			for (id <BLFoodItem> item in existingLogs) {
				// These should all conform to our protocol, as at this point, they should either be a
				// FoodLogMO or MealPlannerDay object
				if ([item respondsToSelector:@selector(tag)] && ([item tag] == tag)) {
					found = YES;
					break;
				}
			}
			if (!found) {
				// Since we didn't find a matching entry thus far, add this Tag to the log
				[existingLogs addObject:tag];
			}
		}
	}

	// Show predictions only if the user has not turned the feature off
	BOOL showPredictions = [[NSUserDefaults standardUserDefaults] boolForKey:kSettingShowLoggingPredictions];

	// Show predictions only if date is not more than 7 days ago
	showPredictions = showPredictions && (self.dayOffset > -7);

	// Don't show predictions on snacks
	showPredictions = showPredictions && ([logTime integerValue] != LogTimeMorningSnack) && ([logTime integerValue] != LogTimeAfternoonSnack)
		&& ([logTime integerValue] != LogTimeEveningSnack);

	// Don't show predictions on any logTime that has coach tags
	showPredictions = showPredictions && ![planEntry.tags count];

	// Don't show predictions if 'none' was selected for this logTime
	showPredictions = showPredictions && ![planEntry.unplannedMealOption isEqualToString:@"none"];

	if (showPredictions) {
		// Determine if we need to show entrees or sides. We only show sides when we have actual items logged, not just
		// planned or meal plan tag entries
		if (hasLoggedItems) {
			// Show sides - need to iterate over our logged item until we run out of them or get at least 3 items to show
			// Get the count value up front for iterating, we don't want to include added prediction rows in iterating
			NSUInteger count = [existingLogs count];
			int idx = 0;
			while (([existingLogs count] < 3) && (idx < count)) {
			///if (showPredictions && ([existingLogs count] < 3)) {

				// NOTE: Whatever is in existingLogs needs to respond to itemType and itemId!
				id entree = existingLogs[idx];
				// Only show predictions based upon actual logged items
				if ([entree isKindOfClass:[FoodLogMO class]]) {
					MealPredictionMO *matchingPrediction = [MealPredictionMO MR_findFirstWithPredicate:[NSPredicate
						predicateWithFormat:@"entree == nil AND %K == %@ AND %K == %@ AND %K == %@ AND %K == %@",
							MealPredictionMOAttributes.login_id, [[User currentUser] loginId],
							MealPredictionMOAttributes.logTime, logTime, 
							MealPredictionMOAttributes.item_type, [entree itemType],
							MealPredictionMOAttributes.item_id, [entree itemId]]
						inContext:self.managedObjectContext];
					if (matchingPrediction) {
						NSArray *sidePredictions = [matchingPrediction.sides sortedArrayUsingDescriptors:@[
							[NSSortDescriptor sortDescriptorWithKey:MealPredictionMOAttributes.usageCount ascending:NO],
							[NSSortDescriptor sortDescriptorWithKey:MealPredictionMOAttributes.name ascending:YES]
							]];
						NSMutableArray *predictionsToShow = [NSMutableArray array];
						for (MealPredictionMO *side in sidePredictions) {
							BOOL (^TestPredictionToFoodLog)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
								id <BLFoodItem> log = obj;
								// Ensure that the log item has an itemId, as a brand new custom food will not yet
								if ([side.item_type isEqualToString:log.itemType] && log.itemId && [side.item_id isEqualToNumber:log.itemId]) {
									*stop = YES;
									return YES;
								}
								return NO;
							};

							BOOL (^TestPredictionToPrediction)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
								MealPredictionMO *prediction = obj;
								if ([side.item_type isEqualToString:prediction.item_type]
										&& [side.item_id isEqualToNumber:prediction.item_id]) {
									*stop = YES;
									return YES;
								}
								return NO;
							};

							// make sure that the item isn't a dupe
							NSUInteger foodLogIndex = [existingLogs indexOfObjectPassingTest:TestPredictionToFoodLog];
							NSUInteger predictionsIndex = [predictionsToShow indexOfObjectPassingTest:TestPredictionToPrediction];
							if (foodLogIndex == NSNotFound && predictionsIndex == NSNotFound) {
								[predictionsToShow addObject:side];
							}
						}
						[predictions addObjectsFromArray:predictionsToShow];
					}
				}
				idx++;
			}
		}
		else {
			// Show entrees
			NSArray *entreePredictions = [MealPredictionMO MR_findAllSortedBy:MealPredictionMOAttributes.usageCount
				ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"entree == nil AND %K == %@ AND %K == %@",
					MealPredictionMOAttributes.login_id, [[User currentUser] loginId],
					MealPredictionMOAttributes.logTime, logTime]
				inContext:self.managedObjectContext];

			NSMutableArray *predictionsToShow = [NSMutableArray array];
			for (MealPredictionMO *entree in entreePredictions) {
				BOOL (^TestPredictionToFoodLog)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					id <BLFoodItem> log = obj;
					if ([entree.item_type isEqualToString:log.itemType] && [entree.item_id isEqualToNumber:log.itemId]) {
						*stop = YES;
						return YES;
					}
					return NO;
				};

				BOOL (^TestPredictionToPrediction)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
					MealPredictionMO *prediction = obj;
					if ([entree.item_type isEqualToString:prediction.item_type] 
							&& [entree.item_id isEqualToNumber:prediction.item_id]) {
						*stop = YES;
						return YES;
					}
					return NO;
				};

				// make sure that the item isn't a dupe
				NSUInteger foodLogIndex = [existingLogs indexOfObjectPassingTest:TestPredictionToFoodLog];
				NSUInteger predictionsIndex = [predictionsToShow indexOfObjectPassingTest:TestPredictionToPrediction];
				if (foodLogIndex == NSNotFound && predictionsIndex == NSNotFound) {
					[predictionsToShow addObject:entree];
				}
			}
			predictions = predictionsToShow;
		}

		if (existingLogs.count < kMaxPredictions) { // jic
			NSUInteger itemsToAdd = kMaxPredictions - existingLogs.count;
			itemsToAdd = MIN(predictions.count, itemsToAdd);
			[existingLogs addObjectsFromArray:[predictions subarrayWithRange:NSMakeRange(0, itemsToAdd)]];
		}
	}

	self.foodLogsByLogTime[[logTime integerValue]] = existingLogs;
	self.nutritionByLogTime[[logTime integerValue]] = [self updateNutritionForLogTime:logTime];
	[self updateTotalNutrition];

	return existingLogs;
}

- (NSMutableArray *)loadWaterLogs
{
	self.waterLogs = [NSMutableArray array];
	FoodLogMO *waterLog = [FoodLogMO MR_findFirstWithPredicate:[NSPredicate
		predicateWithFormat:@"%K = %@ AND %K <> %@ AND %K = %@ AND %K BETWEEN {%@, %@}",
			FoodLogMOAttributes.loginId, [[User currentUser] loginId],
			FoodLogMOAttributes.status, kStatusDelete,
			FoodLogMOAttributes.itemType, @"water",
			FoodLogMOAttributes.date, self.startOfLogDate, self.endOfLogDate]
		inContext:self.managedObjectContext];

	// Needs to be an array, but we only allow one water log per day
	if (waterLog) {
		[self.waterLogs addObject:waterLog];
	}

	return self.waterLogs;
}

- (NSMutableArray *)loadSugaryDrinkLogs
{
	self.sugaryDrinkLogs = [NSMutableArray array];
	FoodLogMO *log = [FoodLogMO MR_findFirstWithPredicate:[NSPredicate
		predicateWithFormat:@"%K = %@ AND %K <> %@ AND %K = %@ AND %K BETWEEN {%@, %@}",
			FoodLogMOAttributes.loginId, [[User currentUser] loginId],
			FoodLogMOAttributes.status, kStatusDelete,
			FoodLogMOAttributes.logTime, @(LogTimeSugaryDrinks),
			FoodLogMOAttributes.date, self.startOfLogDate, self.endOfLogDate]
		inContext:self.managedObjectContext];

	// Needs to be an array, but we only allow one water log per day
	if (log) {
		[self.sugaryDrinkLogs addObject:log];
	}

	return self.sugaryDrinkLogs;
}

- (NSMutableArray *)loadFruitVeggieLogs
{
	self.fruitVeggieLogs = [NSMutableArray array];
	FoodLogMO *log = [FoodLogMO MR_findFirstWithPredicate:[NSPredicate
		predicateWithFormat:@"%K = %@ AND %K <> %@ AND %K = %@ AND %K BETWEEN {%@, %@}",
			FoodLogMOAttributes.loginId, [[User currentUser] loginId],
			FoodLogMOAttributes.status, kStatusDelete,
			FoodLogMOAttributes.logTime, @(LogTimeFruitsVeggies),
			FoodLogMOAttributes.date, self.startOfLogDate, self.endOfLogDate]
		inContext:self.managedObjectContext];

	// Needs to be an array, but we only allow one fruit & veggie log per day
	if (log) {
		[self.fruitVeggieLogs addObject:log];
	}

	return self.fruitVeggieLogs;
}

- (NSMutableArray *)loadActivityLogs
{
	NSMutableArray *existingLogs = [NSMutableArray array];
	NSArray *loggedActivities = [ActivityLogMO MR_findAllWithPredicate:[NSPredicate
		predicateWithFormat:@"%K = %@ AND %K <> %@ AND %K BETWEEN {%@, %@}",
			ActivityMOAttributes.loginId, [[User currentUser] loginId],
			ActivityRecordMOAttributes.status, kStatusDelete,
			ActivityRecordMOAttributes.targetDate, self.startOfLogDate, self.endOfLogDate]
		inContext:self.managedObjectContext];

	#pragma message "TODO Match up planned items with logged items"
/*
	// Load up the planned activities
	NSArray *plannedActivities = [ActivityPlanMO activityPlansForLoginId:[User loginId] forDate:self.startOfLogDate
		inContext:self.managedObjectContext];

	for (ActivityPlanMO *plan in plannedActivities) {
		BOOL found = NO;
		for (TrainingActivityMO *activity in existingLogs) {
			if (([plan.itemType isEqualToString:activity.itemType]) && ([[plan itemId] integerValue] == [[activity itemId] integerValue])) {
				found = YES;
				break;
			}
		}
		if (!found) {
			// Didn't find a matching log item, so add to existingLogs
			[existingLogs addObject:plan];
		}
	}
*/

	#pragma message "TODO Load up the coach activity plan"
	// Get the advisor activity plan workouts for this day, if they have any
	AdvisorActivityPlanPhaseMO *planPhase = nil;
	if (self.advisorActivityPlanMO) {
		planPhase = [self.advisorActivityPlanMO getPhaseForDate:self.startOfLogDate];

		// Match up workout activities with existingLogs
		NSArray *sortedWorkouts = [planPhase.workouts sortedArrayUsingDescriptors:@[[NSSortDescriptor
			sortDescriptorWithKey:AdvisorActivityPlanWorkoutMOAttributes.sortOrder ascending:YES]]];
		for (AdvisorActivityPlanWorkoutMO *workout in sortedWorkouts) {
			[existingLogs addObject:workout];
			for (AdvisorActivityPlanWorkoutActivityMO *activity in workout.activities) {
				// Default to adding the workout activity to existing logs
				ActivityMO *activityToAdd = activity;
				// While adding the workout activities, see if there is already a logged activity for this, if so, use it instead
				for (ActivityLogMO *loggedActivity in loggedActivities) {
					if ([loggedActivity.linkedWorkout isEqual:workout] && [loggedActivity.basedOnActivity isEqual:activity]) {
						// TODO Remove the activity from loggedActivities so that it isn't added below

						// Add this logged activity to existing logs instead of the workout activity
						activityToAdd = loggedActivity;
						break;
					}
				}
				[existingLogs addObject:activityToAdd];
			}

			// TODO Somehow deal with the workout activities
		}
	}

	#pragma message "TODO Match up coach plan items with existing items (planned or logged)"

	self.activityLogs = existingLogs;

	return self.activityLogs;
}

- (NSMutableArray *)loadFoodNotes
{
	self.foodNotes = [self loadLoggingNotesOfType:kLoggingNoteTypeFood];

	return self.foodNotes;
}

- (NSMutableArray *)loadActivityNotes
{
	self.activityNotes = [self loadLoggingNotesOfType:kLoggingNoteTypeExercise];

	return self.activityNotes;
}

- (NSMutableArray *)loadLoggingNotesOfType:(NSString *)noteType
{
	NSMutableArray *loggingNotes = [NSMutableArray array];
	LoggingNoteMO *loggingNote = [LoggingNoteMO MR_findFirstWithPredicate:[NSPredicate
		predicateWithFormat:@"%K = %@ AND %K <> %@ AND %K = %@ AND %K BETWEEN {%@, %@}",
			LoggingNoteMOAttributes.loginId, [[User currentUser] loginId],
			LoggingNoteMOAttributes.status, kStatusDelete,
			LoggingNoteMOAttributes.noteType, noteType,
			LoggingNoteMOAttributes.date, self.startOfLogDate, self.endOfLogDate]
		inContext:self.managedObjectContext];

	// Needs to be an array, but we only allow one note per day
	if (loggingNote) {
		[loggingNotes addObject:loggingNote];
	}
	return loggingNotes;
}

- (void)reloadNutrients:(NSNotification *)notification
{
	self.nutrients = [NutrientMO MR_findAllSortedBy:NutrientMOAttributes.sortOrder ascending:YES
		inContext:self.managedObjectContext];
}

@end
