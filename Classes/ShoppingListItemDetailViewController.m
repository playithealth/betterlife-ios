//
//  ShoppingListItemDetailViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 8/31/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import "ShoppingListItemDetailViewController.h"

#import "BCTableViewCell.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "CategoryMO.h"
#import "BCCategorySelectViewController.h"
#import "GTMHTTPFetcherAdditions.h"
#import "GenericValueDisplay.h"
#import "NumberValue.h"
#import "ShoppingListItemCaloriesCell.h"
#import "ShoppingListItemCategoryCell.h"
#import "ShoppingListItemDetailCell.h"
#import "ShoppingListItemNutrientCell.h"
#import "ShoppingListItemQuantityCell.h"
#import "UIColor+Additions.h"
#import "UIImage+Additions.h"
#import "User.h"
#import "UserImageMO.h"

@interface ShoppingListItemDetailViewController () <BCCategorySelectDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, 
	UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *nutrients;
@property (strong, nonatomic) NSArray *allergies;
@property (strong, nonatomic) NSArray *lifestyles;
@property (strong, nonatomic) NSArray *ingredientWarnings;
@property (strong, nonatomic) UIImage *productImage;

- (IBAction)incrementButtonTapped:(id)sender;
- (IBAction)decrementButtonTapped:(id)sender;
- (IBAction)saveButtonTapped:(id)sender;

@end

@implementation ShoppingListItemDetailViewController

static const NSInteger kSectionInfo = 0;
static const NSInteger kSectionQuantity = 1;
static const NSInteger kSectionCategory = 2;
static const NSInteger kSectionAlerts = 3;
static const NSInteger kSectionCalories = 4;
static const NSInteger kSectionNutrients = 5;

static const NSInteger kTagQuantityBox = 10001;

- (void)viewDidLoad
{
	[super viewDidLoad];

	if (self.shoppingListItem.managedObjectContext) {
		self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save"
			style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonTapped:)];
	}
	else {
		self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add"
			style:UIBarButtonItemStyleDone target:self action:@selector(saveButtonTapped:)];
	}

	// fetch the nutrition data and warnings
	NSURL *url = nil;
	if ([self.shoppingListItem.productId integerValue] > 0) {
		url = [BCUrlFactory productsURLNutritionForProductId:self.shoppingListItem.productId];

		GTMHTTPFetcher *nutritionFetcher = [GTMHTTPFetcher signedFetcherWithURL:url];
		[nutritionFetcher beginFetchWithCompletionHandler:
			^(NSData * retrievedData, NSError * error) {
				if (!error) {
					NSArray *nutrientsArray = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

					[self updateShoppingListNutrition:nutrientsArray];
				}
				else {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:nutritionFetcher response:nil];
				}
			}];

		url = [BCUrlFactory productsURLWarningsForProductId:self.shoppingListItem.productId];

		GTMHTTPFetcher *warningsFetcher = [GTMHTTPFetcher signedFetcherWithURL:url];
		[warningsFetcher beginFetchWithCompletionHandler:
			^(NSData * retrievedData, NSError * error) {
				if (!error) {
					NSDictionary *resultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

					self.lifestyles = [resultsDict objectForKey:@"lifestyle"];
					self.allergies = [resultsDict objectForKey:@"allergies"];
					self.ingredientWarnings = [resultsDict objectForKey:@"ingredients"];

					NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionAlerts];
					UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
					[self configureCell:cell atIndexPath:indexPath];

					indexPath = [NSIndexPath indexPathForRow:1 inSection:kSectionAlerts];
					cell = [self.tableView cellForRowAtIndexPath:indexPath];
					[self configureCell:cell atIndexPath:indexPath];
				}
				else {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:nutritionFetcher response:nil];
				}
			}];
	}

	if ([self.shoppingListItem.imageId integerValue] > 0) {
		url = [BCUrlFactory imageURLForImage:self.shoppingListItem.imageId imageSize:@"medium"];
		GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher signedFetcherWithURL:url];
		[imageFetcher beginFetchWithCompletionHandler:
			^(NSData * retrievedData, NSError * error) {
				if (!error) {
					UIImage *tempImage = [[UIImage alloc] initWithData:retrievedData];
					self.productImage = tempImage;

					NSIndexPath *infoIndexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionInfo];
					[self configureCell:[self.tableView cellForRowAtIndexPath:infoIndexPath] atIndexPath:infoIndexPath];
				}
				else {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:imageFetcher retrievedData:retrievedData];
				}
			}];
	}
	else if ([self.shoppingListItem.productId integerValue] > 0) {
		url = [BCUrlFactory imageURLForProductId:self.shoppingListItem.productId imageSize:@"medium"];
		GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher signedFetcherWithURL:url];
		[imageFetcher beginFetchWithCompletionHandler:
			^(NSData * retrievedData, NSError * error) {
				if (!error) {
					UIImage *tempImage = [[UIImage alloc] initWithData:retrievedData];
					self.productImage = tempImage;

					NSIndexPath *infoIndexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionInfo];
					[self configureCell:[self.tableView cellForRowAtIndexPath:infoIndexPath] atIndexPath:infoIndexPath];
				}
				else {
					[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:imageFetcher retrievedData:retrievedData];
				}
			}];
	}
}

- (void)viewDidUnload
{
	[self setToolbar:nil];
	[super viewDidUnload];
	// Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"SelectCategory"]) {
		BCCategorySelectViewController *categoryView = segue.destinationViewController;
		categoryView.managedObjectContext = self.managedObjectContext;
		categoryView.delegate = self;
	}
}

#pragma mark - local
- (IBAction)incrementButtonTapped:(id)sender
{
	if ([self.shoppingListItem.isRecommendation boolValue]) {
		self.shoppingListItem.isRecommendation = [NSNumber numberWithBool:NO];
		self.shoppingListItem.quantity = [NSNumber numberWithInteger:1];
	}
	else {
		self.shoppingListItem.quantity = [NSNumber numberWithInteger:
			[self.shoppingListItem.quantity integerValue] + 1];
	}

	[self.shoppingListItem markForSync];

	[self.managedObjectContext BL_save];

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionQuantity];
	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
}

- (IBAction)decrementButtonTapped:(id)sender
{
	if ([self.shoppingListItem.isRecommendation boolValue]) {
		self.shoppingListItem.isRecommendation = [NSNumber numberWithBool:NO];
		self.shoppingListItem.quantity = [NSNumber numberWithInteger:1];
	}
	// if this has a remainder, just chop that off
	else if ([self.shoppingListItem.quantity doubleValue] - [self.shoppingListItem.quantity integerValue] > 0) {
		self.shoppingListItem.quantity = [NSNumber numberWithInteger:
			[self.shoppingListItem.quantity integerValue]];
	}
	// otherwise step down one as long as it's above 1
	else if ([self.shoppingListItem.quantity integerValue] > 1) {
		self.shoppingListItem.quantity = [NSNumber numberWithInteger:
			[self.shoppingListItem.quantity integerValue] - 1];
	}
	else {
		return;
	}

	[self.shoppingListItem markForSync];

	[self.managedObjectContext BL_save];

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionQuantity];
	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
}

- (IBAction)saveButtonTapped:(id)sender
{
	[self.delegate detailView:self didUpdateItem:self.shoppingListItem];
}

- (void)productImageTapped:(UITapGestureRecognizer *)recognizer
{
	UIActionSheet *actionSheet = [[UIActionSheet alloc]
		initWithTitle:@"Choose a source" 
		delegate:self
		cancelButtonTitle:@"Cancel" 
		destructiveButtonTitle:nil
		otherButtonTitles:@"Take a new picture", @"Choose from library", nil];
	[actionSheet showInView:self.view];
}

- (BOOL)isSubItem:(NSNumber *)masterId
{
	NSArray *subItems = @[ @5, @7545, @6, @7, @12, @13, @14, @15, @16, @17 ];

	BOOL (^SameMasterId)(id, NSUInteger, BOOL *) = ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
		NSNumber *inMaster = (NSNumber *)obj;
		return ([inMaster isEqualToNumber:masterId]);
	};

	NSUInteger indexOfNutrient = [subItems indexOfObjectPassingTest:SameMasterId];

	return (indexOfNutrient != NSNotFound);
}

- (void)updateShoppingListNutrition:(NSArray *)nutrientsArray
{
	self.nutrients = [[NSMutableArray alloc] init];

//	NSArray *nutritionKeys = @[
//			kNutritionCalories,
//			kNutritionCaloriesFromFat,
//			kNutritionSaturatedFatCalories,
//			kNutritionTotalFat,
//			kNutritionSaturatedFat,
//			kNutritionTransFat,
//			kNutritionPolyunsaturatedFat,
//			kNutritionMonounsaturatedFat,
//			kNutritionCholesterol,
//			kNutritionSodium,
//			kNutritionPotassium,
//			kNutritionTotalCarbohydrates,
//			kNutritionDietaryFiber,
//			kNutritionSolubleFiber,
//			kNutritionInsolubleFiber,
//			kNutritionSugars,
//			kNutritionSugarsAlcohol,
//			kNutritionOtherCarbohydrates,
//			kNutritionProtein,
//			kNutritionVitaminAPercent,
//			kNutritionVitaminCPercent,
//			kNutritionCalciumPercent,
//			kNutritionIronPercent];

	// example of a nutrient
	// [{"master_id":"1","type":"0","name":"Calories","quantity":"130","uom":null,"pct":null,"serving_size_text":"1","serving_size_uom":"tortilla"},

	// step 1 : sort the array
	NSArray *sortedArray = [nutrientsArray sortedArrayUsingComparator:^NSComparisonResult (id obj1, id obj2) {
		NSArray *sortOrder = @[ @"1", @"2", @"3", @"4", @"5", @"7545", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22" ];

		NSNumber *masterId1 = [obj1 valueForKey:kNutritionMasterId];
		NSNumber *masterId2 = [obj2 valueForKey:kNutritionMasterId];

		NSInteger index1 = [sortOrder indexOfObject:masterId1];
		NSInteger index2 = [sortOrder indexOfObject:masterId2];

		if (index1 < index2) {
			return NSOrderedAscending;
		}
		else if (index1 > index2) {
			return NSOrderedDescending;
		}
		else if (index1 == NSNotFound && index2 == NSNotFound) {
			return [masterId1 compare:masterId2];
		}
		else {
			return NSOrderedSame;
		}
	}];

	for (NSDictionary *nutrient in sortedArray) {
		if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 1
				|| [[nutrient objectForKey:kNutritionMasterId] integerValue] == 1000208) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.calories];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 2) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.caloriesFromFat];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 3) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.saturatedFatCalories];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 4) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.totalFat];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 5) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.saturatedFat];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 7545) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.transFat];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 6) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.polyunsaturatedFat];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 7) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.monounsaturatedFat];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 8) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.cholesterol];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 9) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.sodium];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 10) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.potassium];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 11) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.totalCarbohydrates];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 12) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.dietaryFiber];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 13) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.solubleFiber];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 14) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.insolubleFiber];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 15) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.sugars];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 16) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.sugarsAlcohol];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 17) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.otherCarbohydrates];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 18) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.protein];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 19) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.vitaminAPercent];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 20) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.vitaminCPercent];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 21) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.calciumPercent];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 22) {
			[self.shoppingListItem setValue:[[nutrient objectForKey:kNutritionQuantity] numberValueDecimal] forKey:ShoppingListItemAttributes.ironPercent];
			[self.nutrients addObject:nutrient];
		}
		else if ([[nutrient objectForKey:kNutritionMasterId] integerValue] == 20567) {
			// ignoring this - 20567 is serving size in grams
		}
		else {
			[self.nutrients addObject:nutrient];
		}
	}

	[self.tableView reloadData];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	switch (indexPath.section) {
		case kSectionInfo:
		{
			ShoppingListItemDetailCell *sliCell = (ShoppingListItemDetailCell *)cell;
			sliCell.nameLabel.text = self.shoppingListItem.name;
			sliCell.productImageView.image = self.productImage;

			break;
		}
		case kSectionQuantity:
		{
			ShoppingListItemQuantityCell *quantityCell = (ShoppingListItemQuantityCell *)cell;
			quantityCell.quantityLabel.text = [self.shoppingListItem.quantity stringValue];
			break;
		}
		case kSectionCategory:
		{
			ShoppingListItemCategoryCell *categoryCell = (ShoppingListItemCategoryCell *)cell;
			categoryCell.categoryLabel.text = self.shoppingListItem.category.name;
			break;
		}
		case kSectionAlerts:
		{
			BCTableViewCell *bcCell = (BCTableViewCell *)cell;

			if (indexPath.row == 0) {
				bcCell.bcDetailTextLabel.text = [self.lifestyles componentsJoinedByString:@","];
				// if the allergies section is below this one, use the top graphic
				if ([self.allergies count] || [self.ingredientWarnings count]) {
					bcCell.bcImageView.image = [[UIImage imageNamed:@"resizable-box-top"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
				}
				else {
					bcCell.bcImageView.image = [[UIImage imageNamed:@"resizable-box"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
				}
			}
			else if (indexPath.row == 1) {
				bcCell.bcDetailTextLabel.text = [[self.allergies arrayByAddingObjectsFromArray:self.ingredientWarnings] componentsJoinedByString:@","];
				// if the lifestyles section is above, use the bottom graphic
				if ([self.lifestyles count] > 0) {
					bcCell.bcImageView.image = [[UIImage imageNamed:@"resizable-box-bottom"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
				}
				else {
					bcCell.bcImageView.image = [[UIImage imageNamed:@"resizable-box"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
				}
			}
			break;
		}
		case kSectionCalories:
		{
			ShoppingListItemCaloriesCell *caloriesCell = (ShoppingListItemCaloriesCell *)cell;
			caloriesCell.caloriesLabel.text = [self.shoppingListItem.calories stringValue];
			caloriesCell.caloriesFromFatLabel.text = [self.shoppingListItem.caloriesFromFat stringValue];
			break;
		}
		case kSectionNutrients:
		{
			ShoppingListItemNutrientCell *nutrientCell = (ShoppingListItemNutrientCell *)cell;

			NSDictionary *nutrientData = [self.nutrients objectAtIndex:indexPath.row];
			NSString *nutrientName = [nutrientData valueForKey:kNutritionName];
			NSNumber *masterId = [[nutrientData valueForKey:kNutritionMasterId] numberValueDecimal];
			
			// indent the sub items and make text a lighter gray
			NSInteger indent = 20;
			if ([self isSubItem:masterId]) {
				nutrientCell.nameLabel.textColor = [UIColor BC_mediumDarkGrayColor];
				indent = 30;
			}
			else {
				nutrientCell.nameLabel.textColor = [UIColor blackColor];
			}
			nutrientCell.quantityLabel.textColor = [UIColor BC_mediumLightGrayColor];

			nutrientCell.nameLabel.text = nutrientName;
			CGSize nameLabelSize = [nutrientName sizeWithFont:[UIFont systemFontOfSize:17]];
			CGRect nameFrame = nutrientCell.nameLabel.frame;
			nutrientCell.nameLabel.frame = CGRectMake(indent, nameFrame.origin.y, nameLabelSize.width, nameFrame.size.height);

			CGRect quantityFrame = nutrientCell.quantityLabel.frame;
			nutrientCell.quantityLabel.frame = CGRectMake(indent + nameLabelSize.width + 4, quantityFrame.origin.y, quantityFrame.size.width, quantityFrame.size.height);

			if ((NSNull *)[nutrientData valueForKey:kNutritionQuantity] != [NSNull null]) {
				nutrientCell.quantityLabel.text = [NSString stringWithFormat:@"%@ %@", [nutrientData valueForKey:kNutritionQuantity], [nutrientData valueForKey:kNutritionUnitOfMeasure]];
			}
			else {
				nutrientCell.quantityLabel.text = nil;
			}

			if ((NSNull *)[nutrientData valueForKey:kNutritionPercent] != [NSNull null]) {
				nutrientCell.percentLabel.text = [NSString stringWithFormat:@"%@%%", [nutrientData valueForKey:kNutritionPercent]];
			}
			else {
				nutrientCell.percentLabel.text = nil;
			}

			break;
		}
	}
}

#pragma mark - Image Handling
- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType 
{ 
	NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType]; 
	if ([UIImagePickerController isSourceTypeAvailable:sourceType] 
			&& [mediaTypes count] > 0) { 
		NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType]; 
		UIImagePickerController *picker = [[UIImagePickerController alloc] init]; 
		picker.mediaTypes = mediaTypes;
		picker.delegate = self;
		picker.allowsEditing = YES;
		picker.sourceType = sourceType;
		[self presentViewController:picker animated:YES completion:nil];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] 
			initWithTitle:@"Error accessing media"
			message:@"Device doesn't support that media source."
			delegate:self 
			cancelButtonTitle:@"OK"
			otherButtonTitles:nil];
		[alert show];
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// info, quantity, category, alerts, calories, nutrients
	return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;

	switch (section) {
		case kSectionInfo:
		case kSectionQuantity:
		case kSectionCategory:
			numberOfRows = 1;
			break;
		case kSectionCalories:
			numberOfRows = [self.nutrients count] ? 1 : 0;
			break;
		case kSectionAlerts:
		{
			numberOfRows = 0;
			if ([self.lifestyles count] > 0) {
				numberOfRows++;
			}
			if ([self.allergies count] || [self.ingredientWarnings count]) {
				numberOfRows++;
			}
			break;
		}
		case kSectionNutrients:
			numberOfRows = [self.nutrients count];
			break;
	}
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;

	static NSString *detailCell = @"DetailCell";
	static NSString *quantityCell = @"QuantityCell";
	static NSString *categoryCell = @"CategoryCell";
	static NSString *caloriesCell = @"CaloriesCell";
	static NSString *nutrientCell = @"NutrientCell";
	static NSString *lifestyleCellId = @"LifestyleCell";
	static NSString *alertsCellId = @"AlertsCell";

	if (indexPath.section == kSectionInfo) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:detailCell];

		ShoppingListItemDetailCell *sliCell = (ShoppingListItemDetailCell *)cell;
		UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(productImageTapped:)];
		[sliCell.productImageView addGestureRecognizer:recognizer];
	}
	else if (indexPath.section == kSectionQuantity) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:quantityCell];

		UIImageView *boxImageView = (UIImageView*)[cell viewWithTag:kTagQuantityBox];
		boxImageView.image = [[UIImage imageNamed:@"resizable-box"]
			resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
	}
	else if (indexPath.section == kSectionCategory) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:categoryCell];
	}
	else if (indexPath.section == kSectionAlerts) {
		if (indexPath.row == 0) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:lifestyleCellId];
		}
		else if (indexPath.row == 1) {
			cell = [self.tableView dequeueReusableCellWithIdentifier:alertsCellId];
		}
	}
	else if (indexPath.section == kSectionCalories) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:caloriesCell];
	}
	else if (indexPath.section == kSectionNutrients) {
		cell = [self.tableView dequeueReusableCellWithIdentifier:nutrientCell];

		if (indexPath.row == [self.nutrients count] - 1) {
			UIImageView *boxImageView = (UIImageView*)[cell viewWithTag:kTagQuantityBox];
			boxImageView.image = [[UIImage imageNamed:@"resizable-box-bottom"]
				resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 5, 5)];
		}
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat heightForRow = 44.0;

	switch (indexPath.section) {
		case kSectionInfo:
			heightForRow = 120.0;
			break;
		case kSectionQuantity:
		case kSectionCategory:
			heightForRow = 44.0;
			break;
		case kSectionAlerts:
			heightForRow = 60.0;
			break;
		case kSectionCalories:
			heightForRow = 78.0;
			break;
		case kSectionNutrients:
			heightForRow = 30.0;
			break;
	}
	return heightForRow;
}

#pragma mark - CategorySelectViewDelegate
- (void)categorySelectView:(BCCategorySelectViewController *)categorySelectView didSelectCategory:(CategoryMO *)category
{
	self.shoppingListItem.category = category;

	[self.shoppingListItem markForSync];

	[self.managedObjectContext BL_save];

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionCategory];
	[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];

	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UIImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info 
{
	UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
	self.productImage = [chosenImage BC_imageWithTargetSize:CGSizeMake(70, 70)];

	NSManagedObjectContext *context = self.managedObjectContext;
	User *thisUser = [User currentUser];

	// create a 200x200 size image and save it to be uploaded
	UIImage *bigImage = [chosenImage BC_imageWithTargetSize:CGSizeMake(200, 200)];
	UserImageMO *customImage = [UserImageMO MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"%K = %d AND %K = %@ AND %K = %@",
			UserImageMOAttributes.imageType, kUserImageTypeProduct, UserImageMOAttributes.accountId, thisUser.accountId,
			UserImageMOAttributes.imageId, self.shoppingListItem.productId]
		inContext:context];
	if (!customImage) {
		customImage = [UserImageMO insertInManagedObjectContext:context];
		customImage.accountId = thisUser.accountId;
		customImage.loginId = thisUser.loginId;
		customImage.imageType = @(kUserImageTypeProduct);
		customImage.imageId = self.shoppingListItem.productId;
	}
	[customImage setUserImageWithUIImage:bigImage];
	[customImage markForSync];
	[context BL_save];

	NSIndexPath *infoIndexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionInfo];
	[self configureCell:[self.tableView cellForRowAtIndexPath:infoIndexPath] atIndexPath:infoIndexPath];

	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker 
{ 
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex != [actionSheet cancelButtonIndex]) {
		switch (buttonIndex) {
			case 0:
				[self getMediaFromSource:UIImagePickerControllerSourceTypeCamera];
				break;
			case 1:	
			default:
				[self getMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
				break;
		}		
	}
}

@end
