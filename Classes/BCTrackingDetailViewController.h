//
//  BCTrackingDetailViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 2/28/13.
//  Copyright (c) 2013 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCDetailViewDelegate.h"

@class TrackingTypeMO;

@interface BCTrackingDetailViewController : UIViewController

@property (weak, nonatomic) id<BCDetailViewDelegate>delegate;
@property (strong, nonatomic) TrackingTypeMO *trackingTypeMO;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (assign, nonatomic) NSInteger scopeIndex;

@end
