//
//  BLSyncEntity.h
//  BettrLife
//
//  Created by Greg Goodrich on 10/14/14.
//  Copyright (c) 2014 BettrLife Corporation All rights reserved.
//

@protocol BLSyncEntity <NSObject>
+ (id)uniqueKeyFromDict:(NSDictionary *)updateDict; ///< The id for this update dict that uniquely identifies this item, usually the cloudId

+ (NSString *)syncName; ///< The name that the cloud identifies with this entity for syncCheck
+ (NSString *)statusString; ///< The non-localized string that describes what is being synced
+ (NSURL *)urlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit;
+ (NSPredicate *)userPredicate; ///< This returns the appropriate predicate for finding objects owned by the current user (login or account)
+ (void)setWithUpdatesArray:(NSArray *)updates forLogin:(NSNumber *)loginId inMOC:(NSManagedObjectContext *)inMOC;
+ (id)insertInManagedObjectContext:(NSManagedObjectContext *)context;
- (void)setWithUpdateDictionary:(NSDictionary *)updateDict;

@optional
@property (strong, nonatomic) NSNumber *loginId;
@property (strong, nonatomic, readonly) id cloudId; ///< This should return whatever value uniquely defines this object, usually cloudId
@property (strong, nonatomic, readonly) NSNumber *updatedOn; ///< This is used to update SyncUpdate with the last updatedOn
@end

@protocol BLPagingSyncEntity <BLSyncEntity>
+ (NSString *)updatesSubkey; ///< Used for paging syncs, if the cloud response is a dict, we need this to find the array of updates within
@end
