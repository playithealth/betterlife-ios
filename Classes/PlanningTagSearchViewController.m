//
//  PlanningTagSearchViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/21/13.
//  Copyright (c) 2013 BettrLife Corporation. All rights reserved.
//

#import "PlanningTagSearchViewController.h"

#import "AdvisorMealPlanEntryMO.h"
#import "AdvisorMealPlanPhaseMO.h"
#import "AdvisorMealPlanMO.h"
#import "BCAppDelegate.h"
#import "BCHealthCoachPlanNutritionViewController.h"
#import "BCImageCache.h"
#import "BCTableViewController.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "FoodLogDetailViewController.h"
#import "GTMHTTPFetcherAdditions.h"
#import "MealPlannerDay.h"
#import "MealPlannerDetailViewController.h"
#import "NSArray+NestedArrays.h"
#import "NSDictionary+NumberValue.h"
#import "UIColor+Additions.h"
#import "UITableView+DownloadImage.h"

static const CGFloat kSectionHeaderHeight = 22;

enum { SectionCommonBit = 0x1, SectionMenuBit = 0x2, SectionRecipeBit = 0x4, SectionProductBit = 0x8, SectionAllBit = 0xf };
enum { SectionCommon, SectionMenu, SectionRecipe, SectionProduct };

@interface MealPlanTagCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *servingsLabel;
@property (weak, nonatomic) IBOutlet UILabel *servingSizeLabel;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIView *nutritionColorView;
@property (weak, nonatomic) IBOutlet UIButton *nutritionButton;
@end
@implementation MealPlanTagCell
@end

@interface PlanningTagSearchViewController () <BCDetailViewDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UINavigationItem *tagNavigationItem;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) NSString *searchTerm;
@property (strong, nonatomic) NSMutableDictionary *imageFetchersInProgress;

@property (strong, nonatomic) NSArray *sectionItems;
@property (strong, nonatomic) NSArray *sectionInfo;
// NOTE: I decided that for now we will not zoom, but leaving code here in case I change my mind
@property (assign, nonatomic) BOOL isZoomedToSection;
@property (assign, nonatomic) NSUInteger zoomedSection;

@property (strong, nonatomic) id selectedItem;

@end

@implementation PlanningTagSearchViewController

#pragma mark - view lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.imageFetchersInProgress = [NSMutableDictionary dictionary];
        self.searchTerm = @"";
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tagNavigationItem.title = self.tag.name;
    
	// Need to animate this, as without the animation, the contentOffset is set too early, and ends up getting overwritten
	[UIView animateWithDuration:0.4 animations:^{
		self.tableView.contentOffset = CGPointMake(0, CGRectGetHeight(self.tableView.tableHeaderView.frame));
	}];

	[self sendFirstSearchRequest];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
	[self.tableView cancelAllImageTrackers];

	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowNutrition"]) {
		BCHealthCoachPlanNutritionViewController *view = segue.destinationViewController;
		NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
		NSDictionary *item = [[self itemsForSection:indexPath.section] objectAtIndex:indexPath.row];
		view.itemNutrition = item[@"nutrients"];
		view.minNutrition = self.tag.entry.phase.minNutrition;
		view.maxNutrition = self.tag.entry.phase.maxNutrition;
	}
}

- (IBAction)planNutritionViewDone:(UIStoryboardSegue *)segue
{
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (self.isZoomedToSection ? 1 : [self.sectionItems count]);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [[self itemsForSection:section] count] + ([[[self infoForSection:section] objectForKey:kMore] boolValue] ? 1 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *normalCellId = @"TagCell";
    static NSString *moreCellId = @"ShowMoreCell";

	NSArray *items = [self itemsForSection:indexPath.section];
    UITableViewCell *cell = nil;
	if (indexPath.row < [items count]) {
		// Normal cell
		cell = [self.tableView dequeueReusableCellWithIdentifier:normalCellId];

		NSDictionary *item = [items objectAtIndex:indexPath.row];

		MealPlanTagCell *tagCell = (MealPlanTagCell *)cell;
		tagCell.nameLabel.text = item[kName];

		NSString *servingsText = [NSString stringWithFormat:@"%@ servings", item[kMPTagServings]];
		if ([tagCell.servingsLabel respondsToSelector:@selector(setAttributedText:)]) {
			NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:servingsText];
			[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor BC_mediumLightGreenColor]
				range:NSMakeRange(0, [[item[kMPTagServings] description] length])];
			tagCell.servingsLabel.attributedText = attributedString;
		}
		else {
			tagCell.servingsLabel.text = servingsText;
		}

		if ((NSNull *)item[kMPNutrients][kNutritionCalories] != [NSNull null]) {
			NSString *caloriesText = [NSString stringWithFormat:@"%@ calories", item[kMPNutrients][kNutritionCalories]];
			if ([tagCell.servingSizeLabel respondsToSelector:@selector(setAttributedText:)]) {
				NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:caloriesText];
				[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor BC_mediumLightGreenColor]
					range:NSMakeRange(0, [item[kMPNutrients][kNutritionCalories] length])];
				tagCell.servingSizeLabel.attributedText = attributedString;
			}
			else {
				tagCell.servingSizeLabel.text = caloriesText;
			}
		}

		tagCell.itemImageView.image = [[BCImageCache sharedCache] imageWithImageId:[item BC_numberOrNilForKey:kImageId]
			withItemType:item[kMPTagItemType] withSize:kImageSizeMedium];

		tagCell.nutritionColorView.backgroundColor = [UIColor BC_mediumLightGreenColor];
		[tagCell.nutritionButton setImage:[UIImage imageNamed:@"nutrition-complete"] forState:UIControlStateNormal];
	}
	else {
		// More cell
		cell = [self.tableView dequeueReusableCellWithIdentifier:moreCellId];
	}

    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 0.0f;
	NSArray *items = [self itemsForSection:section];
	if ([items count] > 0) {
		headerHeight = kSectionHeaderHeight;
	}
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section
{
	return [BCTableViewController customViewForHeaderWithTitle:[[self infoForSection:section] valueForKey:kTitle]];
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSArray *items = [self itemsForSection:indexPath.section];
	if (indexPath.row < [items count]) {
		// Normal cell selected
		NSDictionary *item = [[self itemsForSection:indexPath.section] objectAtIndex:indexPath.row];
		MealPlannerDay *newMP = nil;
		if (item) {
			newMP = [MealPlannerDay insertMealPlannerDayWithTag:self.tag forDate:self.logDate
				withAdvisorMealPlanTagDictionary:item inContext:self.managedObjectContext];
		}

		self.selection = newMP;
		[self performSegueWithIdentifier:@"ItemSelected" sender:self];
	}
	else {
		// Show More cell selected
		[self loadMoreResultsForSection:indexPath.section];
		/*
		if (!self.isZoomedToSection) {
			self.isZoomedToSection = YES;
			self.zoomedSection = indexPath.section;
			[self.tableView reloadData];
			[self loadMoreResults];
		}
		[self showBackView:YES withLabelText:@"Back"];
		*/
	}
}

#pragma mark - local methods

- (NSMutableDictionary *)infoForSection:(NSUInteger)section
{
	return [self.sectionInfo objectAtIndex:(self.isZoomedToSection ? self.zoomedSection : section)];
}

- (NSMutableArray *)itemsForSection:(NSUInteger)section
{
	return [self.sectionItems objectAtIndex:(self.isZoomedToSection ? self.zoomedSection : section)];
}

- (void)sendFirstSearchRequest
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	NSURL *searchURL = [BCUrlFactory urlForMealPlanId:self.tag.entry.phase.plan.cloudId tagId:self.tag.cloudId sections:@(SectionAllBit)
		offset:@0 limit:@3 searchText:self.searchTerm];
    
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
	
	[appDelegate setNetworkActivityIndicatorVisible:YES];
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:nil];
		}
		else {
			// fetch succeeded
			NSDictionary *searchResultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];

			NSMutableDictionary *commonDict = [[searchResultsDict objectForKey:@"common"] mutableCopy];
			[commonDict setValue:@"common" forKey:@"section"];
			NSMutableDictionary *menuDict = [[searchResultsDict objectForKey:kItemTypeMenu] mutableCopy];
			[menuDict setValue:kItemTypeMenu forKey:@"section"];
			NSMutableDictionary *productDict = [[searchResultsDict objectForKey:kItemTypeProduct] mutableCopy];
			[productDict setValue:kItemTypeProduct forKey:@"section"];
			NSMutableDictionary *recipeDict = [[searchResultsDict objectForKey:kItemTypeRecipe] mutableCopy];
			[recipeDict setValue:kItemTypeRecipe forKey:@"section"];

			self.sectionItems = @[
				[[commonDict objectForKey:@"items"] mutableCopy],
				[[menuDict objectForKey:@"items"] mutableCopy],
				[[productDict objectForKey:@"items"] mutableCopy],
				[[recipeDict objectForKey:@"items"] mutableCopy],
			];

			self.sectionInfo = @[
				commonDict,
				menuDict,
				productDict,
				recipeDict,
			];

			// Ensure that the image trackers don't try to update indexPaths until after we refresh
			[self.tableView clearTrackerIndexPaths];

			[self.tableView reloadData];
			[self loadImagesForOnscreenRows];
		}

		[self.searchTextField resignFirstResponder];
	}];
}

- (void)loadMoreResultsForSection:(NSUInteger)section
{
	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	NSMutableDictionary *sectionInfo = [self infoForSection:section];
	NSURL *searchURL = [BCUrlFactory urlForMealPlanId:self.tag.entry.phase.plan.cloudId tagId:self.tag.cloudId
		sections:[sectionInfo BC_numberForKey:@"bitmask"] offset:[sectionInfo BC_numberForKey:kOffset] limit:@10
		searchText:self.searchTerm];
    
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:searchURL];
	
	[appDelegate setNetworkActivityIndicatorVisible:YES];
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];
		if (error) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher response:nil];
		}
		else {
			// Ensure that the image trackers don't try to update indexPaths until after we refresh
			[self.tableView clearTrackerIndexPaths];

			// fetch succeeded
			NSDictionary *searchResultsDict = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
			// There should only be one entry in the dictionary this time
			NSDictionary *newSectionInfo = [searchResultsDict objectForKey:[sectionInfo valueForKey:@"section"]];

			// Update the section info
			[sectionInfo setObject:[newSectionInfo objectForKey:kMore] forKey:kMore];
			[sectionInfo setObject:[newSectionInfo objectForKey:kOffset] forKey:kOffset];

			// Update the items
			NSMutableArray *items = [self itemsForSection:section];
			NSArray *newItems = [newSectionInfo objectForKey:@"items"];
			NSInteger newRowsCount = [newItems count];
			NSInteger existingRowsCount = [items count];
			[items addObjectsFromArray:newItems];

			NSMutableArray *indexPathsToInsert = [NSMutableArray array];
			for (NSInteger rowIndex = 0; rowIndex < newRowsCount; rowIndex++) {
				[indexPathsToInsert addObject:[NSIndexPath indexPathForRow:existingRowsCount + rowIndex inSection:section]];
			}
			[self.tableView beginUpdates];
			[self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationTop];
			if (![[sectionInfo BC_numberForKey:kMore] boolValue]) {
				[self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:existingRowsCount inSection:section]]
					withRowAnimation:UITableViewRowAnimationBottom];
			}
			[self.tableView endUpdates];

			[self loadImagesForOnscreenRows];
		}

		[self.searchTextField resignFirstResponder];
	}];
}

- (IBAction)infoButtonTapped:(id)sender {
	CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
	NSDictionary *item = [[self itemsForSection:indexPath.section] objectAtIndex:indexPath.row];
    
	NSManagedObjectContext *scratchContext = [NSManagedObjectContext MR_contextWithParent:self.managedObjectContext];
	AdvisorMealPlanTagMO *tag = (id)[scratchContext objectWithID:[self.tag objectID]];
	MealPlannerDay *newMP = [MealPlannerDay insertMealPlannerDayWithTag:tag forDate:self.logDate
                                       withAdvisorMealPlanTagDictionary:item inContext:scratchContext];
    
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Logging" bundle:nil];
	FoodLogDetailViewController *detailView = [storyboard instantiateViewControllerWithIdentifier:@"FoodLogDetailMain"];
	detailView.delegate = self;
	detailView.managedObjectContext = scratchContext;
	detailView.item = newMP;
	detailView.detailViewMode = (self.loggingMode ? ItemDetailViewModeCoachTagLogging: ItemDetailViewModeCoachTagPlanning);
    
	[self presentViewController:detailView animated:YES completion:nil];
}

- (void)loadImagesForOnscreenRows
{
	NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];

	for (NSIndexPath *indexPath in visiblePaths) {

		NSArray *items = [self itemsForSection:indexPath.section];
		if (indexPath.row < [items count]) {
			NSDictionary *item = [items objectAtIndex:indexPath.row];
			NSNumber *imageId = [item BC_numberOrNilForKey:kImageId];
			if ([imageId integerValue]) {
				UIImage *image = [[BCImageCache sharedCache] imageWithImageId:imageId size:kImageSizeMedium];

				if (!image) {
					[self.tableView downloadImageWithImageId:imageId forIndexPath:indexPath withSize:kImageSizeMedium];
				}
			}
		}
	}
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate) {
		[self loadImagesForOnscreenRows];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self loadImagesForOnscreenRows];
}

#pragma mark - BCDetailViewDelegate
- (void)view:(id)viewController didUpdateObject:(id)object
{
	DDLogInfo(@"Detail view - view:%@ didUpdateObject:%@", viewController, object);

	if ([viewController isKindOfClass:[FoodLogDetailViewController class]]) {
		// If there is an object, then don't animate, as the delegate will likely animate as it dismisses us, but
		// if there isn't an object, then go ahead and animate, as the delegate will not dismiss us.
		[self dismissViewControllerAnimated:(object ? NO : YES) completion:^{
			// Treat a save from the detail view as an add in this view
			if (object) {
				self.selection = object;
				[self performSegueWithIdentifier:@"ItemSelected" sender:self];
			}
		}];
	}
}

#pragma mark - UISearchBarDelegate
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[searchBar resignFirstResponder];
	[UIView animateWithDuration:0.4 animations:^{
		self.tableView.contentOffset = CGPointMake(0, CGRectGetHeight(self.tableView.tableHeaderView.frame));
	}];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	[searchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
	if (![searchBar.text isEqualToString:self.searchTerm]) {
		self.searchTerm = searchBar.text;
		[self sendFirstSearchRequest];
    }
}

@end
