//
//  QuantityView.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 10/22/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QuantityView;

@protocol QuantityViewDelegate <NSObject>
@required
- (void)quantityView:(QuantityView *)quantityView finishedWithQuantity:(NSNumber *)quantity;
@end

@interface QuantityView : UIViewController {
}

@property (unsafe_unretained) id<QuantityViewDelegate> delegate;
@property (nonatomic, strong) IBOutlet UITextField *textField;
@property (nonatomic, strong) NSNumber *startingQuantity;

- (IBAction)numberClicked:(id)sender;
- (IBAction)decimalClicked;
- (IBAction)backspaceClicked;
//- (IBAction)save;


@end
