//
//  BLHealthCoachPlanViewController.m
//  BettrLife
//
//  Created by Greg Goodrich on 12/5/14.
//  Copyright (c) 2014 BettrLife Corporation. All rights reserved.
//

#import "BLHealthCoachPlanViewController.h"

#import "AdvisorMealPlanMO.h"
#import "AdvisorMealPlanEntryMO.h"
#import "AdvisorMealPlanPhaseMO.h"
#import "AdvisorMealPlanTagMO.h"
#import "AdvisorMealPlanTimelineMO.h"
#import "BCAppDelegate.h"
#import "BCImageCache.h"
#import "BCUrlFactory.h"
#import "BCUtilities.h"
#import "GTMHTTPFetcherAdditions.h"
#import "NSArray+NestedArrays.h"
#import "UIColor+Additions.h"
#import "UITableView+DownloadImage.h"

@interface BLPlanNameCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *planLabel;
@property (weak, nonatomic) IBOutlet UILabel *planStartLabel;
@end
@implementation BLPlanNameCell
@end
@interface BLPlanCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *planLabel;
@property (weak, nonatomic) IBOutlet UIImageView *planImageView;
@end
@implementation BLPlanCell
@end
@interface BLPlanPhaseControlCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *currentPhaseLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@end
@implementation BLPlanPhaseControlCell
@end

@interface BLHealthCoachPlanViewController ()
@property (strong, nonatomic) NSArray *headerTitles;
@property (assign, nonatomic) BOOL currentPlan;
@property (assign, nonatomic) NSInteger currentPhaseIndex;
@property (strong, nonatomic) NSMutableArray *entriesByLogTime;
@property (strong, nonatomic) NSArray *sortedPhases;
@property (strong, nonatomic) NSAttributedString *scheduleAttributedString;
@property (assign, nonatomic) double planDescriptionVerticalMargins;
@property (assign, nonatomic) double scheduleDescriptionVerticalMargins;
@property (strong, nonatomic) UIImage *coachImage;
@end

typedef NS_ENUM(NSUInteger, PlanDetailViewSections) {
    SectionInfo,
    SectionPhaseControl, // Put this in its own section so that it is easier to refresh when the phase arrows are pressed (configPhaseView)
    SectionDetailsStart,
};

typedef NS_ENUM(NSUInteger, PlanDetailsRows) {
    RowPlanName,
    RowCoachName,
    RowPlanDescription,
    RowScheduleTitle,
    RowScheduleDescription,
    RowPhaseTitle,
    RowPhaseDescription,
};

@implementation BLHealthCoachPlanViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
		self.currentPhaseIndex = 0;

		// The first two blank titles are for the info section and phase control section
		self.headerTitles = @[@"", @"", @"Breakfast", @"Morning Snack", @"Lunch", @"Afternoon Snack", @"Dinner", @"Evening Snack"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
	// They have looked at this, so it's not new anymore
	if ([self.plan.isNew boolValue]) {
		self.plan.isNew = @NO;
		[self.plan.managedObjectContext BL_save];
	}

	AdvisorMealPlanMO *currentPlan = [AdvisorMealPlanMO getMealPlanForDate:[NSDate date] inContext:self.plan.managedObjectContext];
	self.currentPlan = (self.plan == currentPlan);
	if (self.currentPlan) {
		self.navigationItem.rightBarButtonItem.title = @"Stop Plan";
		//[nameCell.startPlanButton setTitleColor:[UIColor BC_redColor] forState:UIControlStateNormal];
	}
	else {
		self.navigationItem.rightBarButtonItem.title = @"Start Plan";
		//[nameCell.startPlanButton setTitleColor:[UIColor BC_mediumDarkGreenColor] forState:UIControlStateNormal];
	}

	[self.tableView downloadImageWithCoachId:self.plan.advisorId
		forIndexPath:[NSIndexPath indexPathForRow:RowCoachName inSection:SectionInfo] withSize:kImageSizeMedium];

	// Want to receive notifications regarding plan timelines changing
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(planTimelineChanged:)
		name:[AdvisorMealPlanTimelineMO entityName] object:nil];
	// Want to receive notifications regarding plan changes
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePlanData:)
		name:[AdvisorMealPlanMO entityName] object:nil];

	NSInteger totalDays = 0;
	for (AdvisorMealPlanPhaseMO *phase in self.plan.phases) {
		totalDays += phase.durationInDaysValue;
	}

	// Build the attributed string for the schedule description
	NSString *startString = @"Meal plans can be split into different time periods and each period can "
		"be one or more days. This meal plan has ";
	NSString *phasesString = [NSString stringWithFormat:@"%lu phases", (unsigned long)[self.plan.phases count]];
	NSMutableString *scheduleString = [NSMutableString stringWithCapacity:[startString length]];
	[scheduleString appendFormat:@"%@%@ and lasts a total of ", startString, phasesString];
	NSUInteger phasesStart = [startString length];
	NSUInteger daysStart = [scheduleString length];
	NSString *daysString = [NSString stringWithFormat:@"%ld days", (long)totalDays];
	[scheduleString appendFormat:@"%@.%@", daysString,
		(self.plan.isRepeating ? @" When you reach the end of this plan, it will automatically start over." : @"")];

	NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:scheduleString];
	[attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14]
		range:NSMakeRange(0, [scheduleString length])];
	[attributedString setAttributes:@{
			NSForegroundColorAttributeName : [UIColor BC_mediumDarkGreenColor],
			NSFontAttributeName : [UIFont boldSystemFontOfSize:14]
		}
		range:NSMakeRange(phasesStart, [phasesString length])];
	[attributedString setAttributes:@{
			NSForegroundColorAttributeName : [UIColor BC_mediumDarkGreenColor],
			NSFontAttributeName : [UIFont boldSystemFontOfSize:14]
		}
		range:NSMakeRange(daysStart, [daysString length])];
	self.scheduleAttributedString = attributedString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.headerTitles count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSInteger rows = 0;
	if (section == SectionInfo) {
		rows = 7; // NOTE: This needs to match the count of items in the enum PlanDetailsRows
	}
	else if (section == SectionPhaseControl) {
		rows = 1;
	}
	else {
		// Always have at least one row per detail section, but could have more if there are multiple tags
		rows = [self.entriesByLogTime BC_countOfNestedArrayAtIndex:section];
	}
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if ((section == SectionInfo) || (section == SectionPhaseControl)) {
		return 0;
	}
	else {
		return UITableViewAutomaticDimension;
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if ((section == SectionInfo) || (section == SectionPhaseControl)) {
		return nil;
	}
	else {
		return [self.headerTitles objectAtIndex:section];
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    // Configure the cell...
	if (indexPath.section == SectionInfo) {
		switch (indexPath.row) {
			case RowPlanName: {
				cell = [tableView dequeueReusableCellWithIdentifier:@"PlanName" forIndexPath:indexPath];
				break;
			}
			case RowCoachName: {
				cell = [tableView dequeueReusableCellWithIdentifier:@"CoachName" forIndexPath:indexPath];
				break;
			}
			case RowPlanDescription: {
				cell = [tableView dequeueReusableCellWithIdentifier:@"PlanDescription" forIndexPath:indexPath];
				break;
			}
			case RowScheduleTitle: {
				cell = [tableView dequeueReusableCellWithIdentifier:@"DescriptionTitle" forIndexPath:indexPath];
				break;
			}
			case RowScheduleDescription: {
				cell = [tableView dequeueReusableCellWithIdentifier:@"ScheduleDescription" forIndexPath:indexPath];
				break;
			}
			case RowPhaseTitle: {
				cell = [tableView dequeueReusableCellWithIdentifier:@"DescriptionTitle" forIndexPath:indexPath];
				break;
			}
			case RowPhaseDescription: {
				cell = [tableView dequeueReusableCellWithIdentifier:@"PhaseDescription" forIndexPath:indexPath];
				break;
			}
		}
	}
	else if (indexPath.section == SectionPhaseControl) {
		cell = [tableView dequeueReusableCellWithIdentifier:@"PhaseControl" forIndexPath:indexPath];
	}
	else {
		// Details Sections
		id item = [self.entriesByLogTime BC_nestedObjectAtIndexPath:indexPath];
		if ([item isKindOfClass:[AdvisorMealPlanEntryMO class]]) {
			AdvisorMealPlanEntryMO *entry = item;
			if ([[entry unplannedMealOption] isEqualToString:@"none"]) {
				cell = [tableView dequeueReusableCellWithIdentifier:@"noneCell"];
			}
			else {
				cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
			}
		}
		else {
			// Must be a tag
			cell = [tableView dequeueReusableCellWithIdentifier:@"tagCell"];
		}
	}
    
	[self configureCell:cell atIndexPath:indexPath];

    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell...
	if (indexPath.section == SectionInfo) {
		switch (indexPath.row) {
			case RowPlanName: {
				cell.textLabel.text = self.plan.planName;
				cell.detailTextLabel.text = [self.plan getMealPlanDetailText];
				[cell layoutIfNeeded];
				break;
			}
			case RowCoachName: {
				BLPlanCell *planCell = (BLPlanCell *)cell;
				NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:
					[NSString stringWithFormat:@"%@ assigned you this plan.", self.plan.advisorName]];
				[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor BC_blueColor]
					range:NSMakeRange(0, [self.plan.advisorName length])];
				planCell.planLabel.attributedText = attributedString;
				id image = [[BCImageCache sharedCache] imageWithCoachId:self.plan.advisorId size:kImageSizeMedium];
				if (image && (image != [NSNull null])) {
					planCell.planImageView.image = image;
				}
				break;
			}
			case RowPlanDescription: {
				BLPlanCell *planCell = (BLPlanCell *)cell;
				planCell.planLabel.text = self.plan.planDescription;
				break;
			}
			case RowScheduleTitle: {
				BLPlanCell *planCell = (BLPlanCell *)cell;
				planCell.planLabel.text = @"Meal Plan Schedule";
				break;
			}
			case RowScheduleDescription: {
				BLPlanCell *planCell = (BLPlanCell *)cell;
				planCell.planLabel.attributedText = self.scheduleAttributedString;
				break;
			}
			case RowPhaseTitle: {
				BLPlanCell *planCell = (BLPlanCell *)cell;
				planCell.planLabel.text = @"Meal Plan Phases";
				break;
			}
			case RowPhaseDescription: {
				break;
			}
		}
	}
	else if (indexPath.section == SectionPhaseControl) {
		BLPlanPhaseControlCell *controlCell = (BLPlanPhaseControlCell *)cell;
		AdvisorMealPlanPhaseMO *phase = [self.sortedPhases objectAtIndex:self.currentPhaseIndex];
		controlCell.currentPhaseLabel.text = [NSString stringWithFormat:@"%ld of %lu", (long)self.currentPhaseIndex + 1,
			(unsigned long)[self.plan.phases count]];
		controlCell.daysLabel.text = [NSString stringWithFormat:@"%@ days", phase.durationInDays];
	}
	else {
		// Details Sections
		id item = [self.entriesByLogTime BC_nestedObjectAtIndexPath:indexPath];
		if ([item isKindOfClass:[AdvisorMealPlanEntryMO class]]) {
			AdvisorMealPlanEntryMO *entry = item;
			if ([[entry unplannedMealOption] isEqualToString:@"none"]) {
				BLPlanCell *planCell = (BLPlanCell *)cell;
				planCell.planLabel.text = @"You should not eat anything for this meal.";
			}
			else {
				// No config needed for 'any' cell
			}
		}
		else {
			AdvisorMealPlanTagMO *tag = item;
			BLPlanCell *planCell = (BLPlanCell *)cell;
			planCell.planLabel.text = [tag name];
		}
	}
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = UITableViewAutomaticDimension;

	if (indexPath.section == SectionInfo) {
		switch (indexPath.row) {
			case RowPlanName:
				height = 50;
			case RowCoachName:
				break;
			case RowPlanDescription: {
				BLPlanCell *planCell = (BLPlanCell *)[tableView dequeueReusableCellWithIdentifier:@"PlanDescription"];
				[self configureCell:planCell atIndexPath:indexPath];
				[planCell.planLabel sizeToFit];

				// Find the vertical margins of the description label so that we can adjust the cell's height properly
				if (!self.planDescriptionVerticalMargins) {
					double topMargin = 0;
					double bottomMargin = 0;
					for (NSLayoutConstraint *constraint in planCell.contentView.constraints) {
						if (([constraint.firstItem isEqual:planCell.planLabel] && (constraint.firstAttribute == NSLayoutAttributeTop))
								|| ([constraint.secondItem isEqual:planCell.planLabel]
								&& (constraint.secondAttribute == NSLayoutAttributeTop))) {
							topMargin = constraint.constant;
						}
						else if (([constraint.firstItem isEqual:planCell.planLabel]
								&& (constraint.firstAttribute == NSLayoutAttributeBottom))
								|| ([constraint.secondItem isEqual:planCell.planLabel]
								&& (constraint.secondAttribute == NSLayoutAttributeBottom))) {
							bottomMargin = constraint.constant;
						}
					}

					self.planDescriptionVerticalMargins = topMargin + bottomMargin;
				}

				height = planCell.planLabel.frame.size.height + self.planDescriptionVerticalMargins;
				break;
			}
			case RowScheduleTitle:
			case RowPhaseTitle:
				height = 22;
				break;
			case RowScheduleDescription: {
				BLPlanCell *planCell = [tableView dequeueReusableCellWithIdentifier:@"ScheduleDescription"];
				[self configureCell:planCell atIndexPath:indexPath];
				[planCell.planLabel sizeToFit];

				// Find the vertical margins of the description label so that we can adjust the cell's height properly
				if (!self.scheduleDescriptionVerticalMargins) {
					double topMargin = 0;
					double bottomMargin = 0;
					for (NSLayoutConstraint *constraint in planCell.contentView.constraints) {
						if (([constraint.firstItem isEqual:planCell.planLabel] && (constraint.firstAttribute == NSLayoutAttributeTop))
								|| ([constraint.secondItem isEqual:planCell.planLabel]
								&& (constraint.secondAttribute == NSLayoutAttributeTop))) {
							topMargin = constraint.constant;
						}
						else if (([constraint.firstItem isEqual:planCell.planLabel]
								&& (constraint.firstAttribute == NSLayoutAttributeBottom))
								|| ([constraint.secondItem isEqual:planCell.planLabel]
								&& (constraint.secondAttribute == NSLayoutAttributeBottom))) {
							bottomMargin = constraint.constant;
						}
					}

					self.scheduleDescriptionVerticalMargins = topMargin + bottomMargin;
				}
				height = planCell.planLabel.frame.size.height + self.scheduleDescriptionVerticalMargins;
				break;
			}
			case RowPhaseDescription:
				height = 40;
				break;
		}
	}
	else {
		// Standard height for the details rows and phase control row
	}
	return height;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - local
- (void)updatePlanData:(NSNotification *)notification
{
	[self.tableView reloadData];
}

- (NSArray *)entriesByLogTime
{
	if (_entriesByLogTime) {
		return _entriesByLogTime;
	}

	/*
	 * Initialize our storage (multi-dimensional array, top level is each week day or meal time)
	 */
	_entriesByLogTime = [[NSMutableArray alloc] init];
	// Due to having two additional sections, make sure they're accounted for in the data so that indexPaths into this work
	[_entriesByLogTime addObject:[NSNull null]];
	[_entriesByLogTime addObject:[NSNull null]];
	for (int idx = 0; idx < [self.headerTitles count]; idx++) {
		[_entriesByLogTime addObject:[[NSMutableArray alloc] init]];
	}

	AdvisorMealPlanPhaseMO *phase = [self.sortedPhases objectAtIndex:self.currentPhaseIndex];
	NSArray *sortedEntries = [phase.entries sortedArrayUsingDescriptors:
		@[[NSSortDescriptor sortDescriptorWithKey:AdvisorMealPlanEntryMOAttributes.logTime ascending:YES]]];
	for (AdvisorMealPlanEntryMO *entry in sortedEntries) {
		if ([entry.tags count]) {
			NSArray *sortedTags = [entry.tags sortedArrayUsingDescriptors:
				@[[NSSortDescriptor sortDescriptorWithKey:AdvisorMealPlanTagMOAttributes.cloudId ascending:YES]]];
			[_entriesByLogTime setObject:sortedTags atIndexedSubscript:[entry.logTime integerValue] + SectionDetailsStart];
		}
		else {
			[[_entriesByLogTime objectAtIndex:[entry.logTime integerValue] + SectionDetailsStart] addObject:entry];
		}
	}

	return _entriesByLogTime;
}

- (NSArray *)sortedPhases
{
	if (_sortedPhases) {
		return _sortedPhases;
	}

	self.sortedPhases = [self.plan.phases sortedArrayUsingDescriptors:
		@[[NSSortDescriptor sortDescriptorWithKey:AdvisorPlanPhaseMOAttributes.phaseNumber ascending:YES]]];

	return _sortedPhases;
}

- (void)configPhaseView
{
	self.entriesByLogTime = nil;
	[self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(SectionPhaseControl, [self.headerTitles count] - 1)]
		withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)updateStartStopButton:(BOOL)currentPlan
{
	// TODO Maybe just put this code where this method is called, instead of having this method
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:RowPlanName inSection:SectionInfo]]
		withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)planTimelineChanged:(NSNotification *)notification
{
	// TODO Maybe just put this code where this method is called, instead of having this method
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:RowPlanName inSection:SectionInfo]]
		withRowAnimation:UITableViewRowAnimationAutomatic];

	if (self.currentPlan) {
		self.navigationItem.rightBarButtonItem.title = @"Stop Plan";
		//[nameCell.startPlanButton setTitleColor:[UIColor BC_redColor] forState:UIControlStateNormal];
	}
	else {
		self.navigationItem.rightBarButtonItem.title = @"Start Plan";
		//[nameCell.startPlanButton setTitleColor:[UIColor BC_mediumDarkGreenColor] forState:UIControlStateNormal];
	}
}

#pragma mark - responders
- (IBAction)startStopTapped:(id)sender 
{
	self.currentPlan = !self.currentPlan;

	NSString *httpMethod = (self.currentPlan ? kStatusPut : kStatusDelete);

	NSURL *url = [BCUrlFactory advisorMealPlanTimelineURL];
	NSDictionary *jsonDict = [NSDictionary dictionaryWithObject:self.plan.cloudId forKey:kAdvisorMealPlanId];
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil];

	BCAppDelegate *appDelegate = (BCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate setNetworkActivityIndicatorVisible:YES];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:httpMethod postData:jsonData];
	[fetcher beginFetchWithCompletionHandler: ^(NSData *retrievedData, NSError * error) {
		[appDelegate setNetworkActivityIndicatorVisible:NO];
		if (error != nil) {
			[BCUtilities logHttpError:error inFunctionNamed:__FUNCTION__ withFetcher:fetcher retrievedData:retrievedData];
		}
		else {
            NSArray *updates = [NSJSONSerialization JSONObjectWithData:retrievedData options:0 error:nil];
            [AdvisorMealPlanTimelineMO setWithUpdatesArray:updates forLogin:self.plan.loginId inMOC:self.plan.managedObjectContext];

			[[NSNotificationCenter defaultCenter]
				postNotificationName:[AdvisorMealPlanTimelineMO entityName]
				object:self userInfo:nil];
		}
	}];

	[self updateStartStopButton:self.currentPlan];
}

- (IBAction)previousPhaseTapped:(id)sender 
{
	self.currentPhaseIndex--;
	if (self.currentPhaseIndex < 0) {
		self.currentPhaseIndex = [self.plan.phases count] - 1;
	}
	[self configPhaseView];
}

- (IBAction)nextPhaseTapped:(id)sender 
{
	self.currentPhaseIndex++;
	if (self.currentPhaseIndex > [self.plan.phases count] - 1) {
		self.currentPhaseIndex = 0;
	}
	[self configPhaseView];
}

@end
