//
//  OAuthRequestParameter.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 3/28/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "OAuthRequestParameter.h"

#import "NSString+BCAdditions.h"

@implementation OAuthRequestParameter
@synthesize name;
@synthesize value;

+ (NSArray *)parametersFromDictionary:(NSDictionary *)inDictionary {
	NSMutableArray *parameterArray = [[NSMutableArray alloc] init];
	
	for (NSString *aKey in [inDictionary allKeys]) {
		OAuthRequestParameter *aURLParameter = [[OAuthRequestParameter alloc] init];
		aURLParameter.name = aKey;
		aURLParameter.value = [inDictionary objectForKey:aKey];
		
		[parameterArray addObject:aURLParameter];
	}
	
	return parameterArray;
}
+ (NSString *)parameterStringForParameters:(NSArray *)inParameters {
	NSMutableString *queryString = [[NSMutableString alloc] init];
	int i = 0;
	NSInteger parameterCount = [inParameters count];
	OAuthRequestParameter *aParameter = nil;
	
	for (; i < parameterCount; i++) {
		aParameter = [inParameters objectAtIndex:i];
		[queryString appendString:[aParameter URLEncodedParameterString]];
		
		if (i < parameterCount - 1) {
			[queryString appendString:@"&"];
		}
	}
	
	return queryString;
}
+ (NSArray *)parametersFromString:(NSString *)inString {
	NSMutableArray *foundParameters = [NSMutableArray arrayWithCapacity:10];
	if ([inString length] > 0) {
		NSScanner *parameterScanner = [[NSScanner alloc] initWithString:inString];
		NSString *name = nil;
		NSString *value = nil;
		OAuthRequestParameter *currentParameter = nil;

		while (![parameterScanner isAtEnd]) {
			name = nil;
			value = nil;

			[parameterScanner scanUpToString:@"=" intoString:&name];
			[parameterScanner scanString:@"=" intoString:NULL];
			[parameterScanner scanUpToString:@"&" intoString:&value];
			[parameterScanner scanString:@"&" intoString:NULL];		

			currentParameter = [[OAuthRequestParameter alloc] init];
			currentParameter.name = name;
			currentParameter.value = [value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

			[foundParameters addObject:currentParameter];

		}

	}
	return foundParameters;
}
- (NSString *)URLEncodedParameterString {
	return [NSString stringWithFormat:@"%@=%@", [self.name BC_escapeURIReservedCharacters],
		self.value ? [self.value BC_escapeURIReservedCharacters] : @""];
}
- (NSComparisonResult)compare:(id)inObject {
	NSComparisonResult result = [self.name compare:[(OAuthRequestParameter *)inObject name]];
	
	if (result == NSOrderedSame) {
		result = [self.value compare:[(OAuthRequestParameter *)inObject value]];
	}
								 
	return result;
}
- (NSString *)description {
	return [NSString stringWithFormat:@"<%@: %p %@>", NSStringFromClass([self class]), self, [self URLEncodedParameterString]];
}

@end
