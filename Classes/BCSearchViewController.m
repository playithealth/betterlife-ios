//
//  BCSearchViewController.m
//  BuyerCompass
//
//  Created by Sef Tarbell on 8/30/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import "BCSearchViewController.h"

static const NSInteger kSearchPageSize = 10;

@interface BCSearchViewController ()
@end

@implementation BCSearchViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.searchTerm = @"";
		self.showKeyboardOnView = YES; ///< Default this for now, since that is the original behavior
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	if (!self.keyboardToolbar) {
		self.keyboardToolbar = [[UIToolbar alloc] initWithFrame:
			CGRectMake(0, 0, self.view.bounds.size.width, 35)];
		self.keyboardToolbar.barStyle = UIBarStyleBlack;
		self.keyboardToolbar.translucent = YES;

		UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]
			initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
			target:nil action:nil];

		UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
			initWithTitle:@"Done" style:UIBarButtonItemStyleDone
			target:self action:@selector(resignKeyboard:)];

		self.keyboardToolbar.items = @[flexibleSpace, doneButton];
	}

	self.searchTextField.inputAccessoryView = self.keyboardToolbar;
}

- (void)viewWillAppear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:YES animated:NO];

	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{    
	[self fetchInitialResults];
	[self.tableView reloadData];

	if (self.showKeyboardOnView) {
		[self.searchTextField becomeFirstResponder];
	}

	[super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:NO animated:YES];

	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)cancelButtonTapped:(id)sender
{
    [self.delegate searchView:self didFinish:YES withSelection:nil];
}

- (void)fetchInitialResults
{
	// this should be overridden
}

- (void)fetchLocalResults
{
	// this should be overridden
}

- (void)resetSearch
{
	// this should be overridden
}

- (void)showBackView:(BOOL)show withLabelText:(NSString *)labelText
{
	self.searchTextField.text = nil;

	self.backButton.hidden = !show;
	self.backLabel.hidden = !show;

	self.backLabel.text = labelText;

	self.headerImageView.hidden = show;
	self.cancelButton.hidden = show;
	self.searchTextField.hidden = show;
}

- (IBAction)backButtonTapped:(id)sender
{
	[self showBackView:NO withLabelText:nil];

	self.searchTextField.text = self.searchTerm;
	
	[self resetSearch];

	[self fetchLocalResults];
}

- (void)resignKeyboard:(id)sender
{
	[self.searchTextField resignFirstResponder];
}

#pragma mark - UITableDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self resetSearch];
	[self.tableView reloadData];
    
	if ([self.searchTextField.text length] > 0) {
		self.searchTerm = self.searchTextField.text;
		[self.searchTextField resignFirstResponder];

		[self fetchLocalResults];
		/*
		 * THIS SHOULD BE OVERRIDDEN
		 *  /# send any api requests here #/
		 */
    }

    return YES;
}

- (IBAction)searchTextFieldEditingChanged:(UITextField *)sender 
{
	self.searchTerm = [sender.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	
	[self resetSearch];
	[self fetchLocalResults];
	[self.tableView reloadData];
}

#pragma mark - "Load More" handling
- (void)insertRowsFromArray:(NSArray *)fromArray toArray:(NSMutableArray *)toArray inSection:(NSInteger)section more:(BOOL)more
{
	NSInteger newRowsCount = [fromArray count];
	NSInteger existingRowsCount = [toArray count];

	if (newRowsCount > kSearchPageSize) {
		newRowsCount = kSearchPageSize;
		fromArray = [fromArray subarrayWithRange:NSMakeRange(0, newRowsCount)];
	}

	[toArray addObjectsFromArray:fromArray];

	NSMutableArray *indexPathsToInsert = [NSMutableArray array];
	for (NSInteger rowIndex = 0; rowIndex < newRowsCount; rowIndex++) {
		[indexPathsToInsert addObject:[NSIndexPath indexPathForRow:existingRowsCount + rowIndex inSection:section]];
	}
	[self.tableView beginUpdates];
	[self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationTop];
	if (!more) {
		[self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:existingRowsCount inSection:section]] withRowAnimation:UITableViewRowAnimationBottom];
	}
	[self.tableView endUpdates];
}

@end
