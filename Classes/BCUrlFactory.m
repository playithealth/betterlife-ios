//
//  BCUrlFactory.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/2/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "BCUrlFactory.h"

#import "CategoryMO.h"
#import "CoreLocation/CoreLocation.h"
#import "NSString+BCAdditions.h"

@implementation BCUrlFactory

+ (NSString *)apiRootURLString
{
#if TARGET_IPHONE_SIMULATOR
	// for testing ssl on the simulator:
	//return @"https://www.bettrlife.com:4431/";
	//return @"https://api.bettrlifeapp.com/";
	//return @"https://testing.bettrlife.com:4431/";
	return @"http://api.bettrlife.dev/";

#else
	//return @"https://testing.bettrlife.com:4431/";
	//return @"https://www.bettrlife.com:4431/";
	return @"https://api.bettrlifeapp.com/";
#endif
}

+ (NSString *)apiVersionString
{
	return @"4";
}

#pragma mark - oauth
+ (NSString *)oauthConsumerURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"oauthconsumer.php"];
}

+ (NSURL *)oauthConsumerURL
{
	return [NSURL URLWithString:[self oauthConsumerURLString]];
}

#pragma mark - login
+ (NSString *)loginURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"login.php"];
}

+ (NSURL *)loginURL
{
	return [NSURL URLWithString:[self loginURLString]];
}

#pragma mark - inbox
+ (NSString *)inboxURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"messagelist.php"];
}

+ (NSURL *)inboxURL
{
	return [NSURL URLWithString:[self inboxURLString]];
}

+ (NSURL *)messagesUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self inboxURLString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSString *)messageURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"messagedetail.php"];
}

+ (NSURL *)messageUrlForMessage:(NSUInteger)messageId
{
	NSString *serviceURLString = [[self messageURLString]
		stringByAppendingFormat:@"?message_id=%lu", (unsigned long)messageId];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - community
+ (NSString *)communityURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"community.php"];
}

+ (NSURL *)communityURL
{
	return [NSURL URLWithString:[self communityURLString]];
}

+ (NSURL *)communityUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self communityURLString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - message threads and thread messages
+ (NSString *)threadsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"threads.php"];
}

+ (NSURL *)threadsURL
{
	return [NSURL URLWithString:[self threadsURLString]];
}

+ (NSURL *)threadsUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self threadsURLString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSString *)threadMessagesURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"threadmessages.php"];
}

+ (NSURL *)threadMessagesURL
{
	return [NSURL URLWithString:[self threadMessagesURLString]];
}

+ (NSURL *)threadMessagesUrlNewerThan:(NSNumber *)timestamp emptyThreads:(NSString *)emptyThreads
{
	NSString *serviceURLString = [[self threadMessagesURLString]
		stringByAppendingFormat:@"?newer_than=%@&empty_thread_ids=%@", timestamp, emptyThreads];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - shopping list
+ (NSString *)shoppingListURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"shoppinglist.php"];
}

+ (NSURL *)shoppingListURL
{
	return [NSURL URLWithString:[self shoppingListURLString]];
}

+ (NSURL *)shoppingListUrlNewerThan:(NSNumber *)timestamp
{
	// TODO not sure if we should send the mobile id here
	NSString *serviceURLString = [[self shoppingListURLString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - evaluate
+ (NSString *)evaluateURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"evaluate.php"];
}

+ (NSURL *)evaluateURL
{
	return [NSURL URLWithString:[self evaluateURLString]];
}

+ (NSURL *)latestEvaluationsURLForStoreId:(NSNumber *)storeId
{
	NSString *serviceURLString = [[self evaluateURLString]
		stringByAppendingFormat:@"?item_id=%@&type_id=4&latest=1", storeId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)userEvaluationsURLForStoreId:(NSNumber *)storeId
{
	NSString *serviceURLString = [[self evaluateURLString]
		stringByAppendingFormat:@"?item_id=%@&type_id=4", storeId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)evaluationsForItemId:(NSNumber *)itemId withTypeId:(NSNumber *)itemType
{
	NSString *serviceURLString = [[self evaluateURLString]
		stringByAppendingFormat:@"?item_id=%@&type_id=%@&aggregate_and_user=1", itemId, itemType];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)evaluationsForItemId:(NSNumber *)itemId withTypeName:(NSString *)itemType
{
	NSUInteger typeId = 0;
	if ([itemType isEqualToString:@"product"]) {
		typeId = kEvalItemTypeProduct;
	}
	else if ([itemType isEqualToString:@"recipe"]) {
		typeId = kEvalItemTypeRecipe;
	}
	else if ([itemType isEqualToString:@"menu"]) {
		typeId = kEvalItemTypeMenuItem;
	}

	return [BCUrlFactory evaluationsForItemId:itemId withTypeId:@(typeId)];
}

#pragma mark - purchase history
+ (NSString *)purchaseHistoryURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"purchasehistory.php"];
}

+ (NSURL *)purchaseHistoryURL
{
	return [NSURL URLWithString:[self purchaseHistoryURLString]];
}

+ (NSURL *)purchaseHistoryUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self purchaseHistoryURLString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)purchaseHistoryWithRecommendations:(NSNumber *)timestamp;
{
	NSString *serviceURLString = [[self purchaseHistoryURLString]
		stringByAppendingFormat:@"?newer_than=%@&with_recommendations=1",
		timestamp];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)purchaseHistoryURLForProductId:(NSNumber *)productId limit:(NSNumber *)limit
{
	NSString *serviceURLString = [[self purchaseHistoryURLString]
		stringByAppendingFormat:@"?product_id=%@&limit=%@", productId, limit];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)purchaseHistoryURLForProductNamed:(NSString *)item limit:(NSNumber *)limit
{
	NSString *serviceURLString = [[self purchaseHistoryURLString]
		stringByAppendingFormat:@"?item=%@&limit=%@",
		[item BC_escapeURIReservedCharacters], limit];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - entities
+ (NSString *)getUrlEntities
{
	return [NSString stringWithFormat:@"http://%@/entities.php", [self apiRootURLString]];
}

+ (NSString *)entitiesURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"entities.php"];
}

+ (NSURL *)entitiesURL
{
	return [NSURL URLWithString:[self entitiesURLString]];
}

+ (NSURL *)amenitiesURLForStoreId:(NSNumber *)storeId
{
	NSString *serviceURLString = [[self entitiesURLString]
		stringByAppendingFormat:@"?storeamenities=1&store_id=%@", storeId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)userStoresUrl
{
	NSString *serviceURLString = [[self entitiesURLString]
		stringByAppendingString:@"?favstores=1"];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)searchStoresURLByZip:(NSString *)zip name:(NSString *)name showRows:(NSUInteger)rows rowOffset:(NSUInteger)offset
{
	NSString *serviceURLString = [[self entitiesURLString] stringByAppendingFormat:@"?list=1&zip=%@&name=%@&rows=%lu&offset=%lu",
		zip, [name BC_escapeURIReservedCharacters], (unsigned long)rows, (unsigned long)offset];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)searchStoresURLByLocation:(CLLocation *)location name:(NSString *)name showRows:(NSUInteger)rows rowOffset:(NSUInteger)offset
{
	NSString *serviceURLString = [[self entitiesURLString] stringByAppendingFormat: @"?list=1&latitude=%g&longitude=%g&name=%@&rows=%lu&offset=%lu",
		location.coordinate.latitude, location.coordinate.longitude, [name BC_escapeURIReservedCharacters], (unsigned long)rows, (unsigned long)offset];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - quicklists
+ (NSString *)quickListURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"quicklist.php"];
}

+ (NSURL *)quickListURL
{
	return [NSURL URLWithString:[self quickListURLString]];
}

+ (NSURL *)quickListsUrl
{
	NSString *serviceURLString = [[self quickListURLString]
		stringByAppendingString:@"?list=1"];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)quickListItemsUrlForList:(NSUInteger)listId
{
	NSString *serviceURLString = [[self quickListURLString]
		stringByAppendingFormat:@"?qlitems=%lu", (unsigned long)listId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)quickListsAndItemsUrl
{
	NSString *serviceURLString = [[self quickListURLString]
		stringByAppendingString:@"?both=1"];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - products
+ (NSString *)productsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"products.php"];
}

+ (NSURL *)productsURL
{
	return [NSURL URLWithString:[self productsURLString]];
}

+ (NSURL *)productsURLInfoForProductId:(NSNumber *)productId
{
	NSString *serviceURLString = [[self productsURLString]
		stringByAppendingFormat:@"?productinfo=1&product_id=%@", productId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)productsURLServingsForProductId:(NSNumber *)productId
{
	NSString *serviceURLString = [[self productsURLString]
		stringByAppendingFormat:@"?servings=1&product_id=%@", productId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)productsURLWarningsForProductId:(NSNumber *)productId
{
	NSString *serviceURLString = [[self productsURLString]
		stringByAppendingFormat:@"?product_warnings=1&product_id=%@", productId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)productsURLNutritionForProductId:(NSNumber *)productId
{
	NSString *serviceURLString = [[self productsURLString]
		stringByAppendingFormat:@"?product_nutrition=1&product_id=%@", productId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)productSearchURLForSearchTerm:(NSString *)searchTerm
	showRows:(NSUInteger)rows rowOffset:(NSUInteger)offset
	filters:(NSDictionary *)filters showOnlyFood:(BOOL)foodOnly
{
	NSMutableString *serviceURLString = [NSMutableString stringWithString:[self productsURLString]];
	[serviceURLString appendFormat:@"?search=%@&rows=%lu&offset=%lu",
		[searchTerm BC_escapeURIReservedCharacters], (unsigned long)rows, (unsigned long)offset];

	if (foodOnly) {
		[serviceURLString appendString:@"&foodonly=1"];
	}

	if (filters) {
		NSArray *categories = [filters objectForKey:@"categories"];
		if (categories) {
			NSArray *categoryIds = [categories valueForKeyPath:kCategoryId];
			[serviceURLString appendFormat:@"&categories=%@", [categoryIds componentsJoinedByString:@","]];
		}

		NSArray *brands = [filters objectForKey:@"brands"];
		if (brands) {
			NSArray *brandIds = [brands valueForKeyPath:@"brand_id"];
			[serviceURLString appendFormat:@"&brands=%@", [brandIds componentsJoinedByString:@","]];
		}

		NSArray *lifestyles = [filters objectForKey:@"lifestyles"];
		if (lifestyles) {
			NSArray *lifestyleNames = [lifestyles valueForKeyPath:@"filter_name"];
			[serviceURLString appendFormat:@"&lifestyles=%@", [lifestyleNames componentsJoinedByString:@","]];
		}

		NSArray *ratings = [filters objectForKey:@"ratings"];
		if (ratings) {
			NSArray *ratingNumbers = [ratings valueForKeyPath:@"rating"];
			[serviceURLString appendFormat:@"&ratings=%@", [ratingNumbers componentsJoinedByString:@","]];
		}
	}

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)productSearchURLForUPC:(NSString *)upc
{
	NSString *serviceURLString = [[self productsURLString]
		stringByAppendingFormat:@"?barcode=%@", upc];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)productSearchURLForBarcode:(NSString *)barcode andType:(NSString *)type
{
	NSString *serviceURLString = [[self productsURLString]
		stringByAppendingFormat:@"?barcode=%@&type=%@", barcode, type];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)productSpeculateCategoryForFreetextItem:(NSString *)freetextItem
{
	NSString *serviceURLString = [[self productsURLString]
		stringByAppendingFormat:@"?speculate_category=1&item=%@",
		[freetextItem BC_escapeURIReservedCharacters]];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - universal search
+ (NSString *)searchURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"search.php"];
}

+ (NSURL *)searchUrlForSearchTerm:(NSString *)searchTerm searchSections:(NSArray *)sections showRows:(NSUInteger)rows showOnlyFood:(BOOL)foodOnly;
{
	NSMutableString *serviceURLString = [NSMutableString stringWithString:[self searchURLString]];

	[serviceURLString appendFormat:@"?search=%@&sections=%@&rows=%lu", [searchTerm BC_escapeURIReservedCharacters],
		[sections componentsJoinedByString:@","], (unsigned long)rows];

	if (foodOnly) {
		[serviceURLString appendString:@"&foodonly=1"];
	}

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - smart search
+ (NSString *)smartSearchURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"smartsearch.php"];
}

+ (NSURL *)smartSearchURLForSearchTerm:(NSString *)searchTerm searchSections:(NSArray *)sections offset:(NSInteger)offset rows:(NSInteger)rows
{
	NSMutableString *serviceURLString = [NSMutableString stringWithString:[self smartSearchURLString]];

	if (sections) {
		[serviceURLString appendFormat:@"?searchtype=search&search=%@&sections=%@&rows=%ld&offset=%ld", [searchTerm BC_escapeURIReservedCharacters],
			[sections componentsJoinedByString:@","], (long)rows, (long)offset];
	}
	else {
		[serviceURLString appendFormat:@"?searchtype=search&search=%@&rows=%ld&offset=%ld", [searchTerm BC_escapeURIReservedCharacters], (long)rows, (long)offset];
	}

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)unifiedSmartSearchURLForSearchTerm:(NSString *)searchTerm offset:(NSInteger)offset rows:(NSInteger)rows
{
	NSMutableString *serviceURLString = [NSMutableString stringWithString:[self smartSearchURLString]];

	[serviceURLString appendFormat:@"?searchtype=search&search=%@&sections=unified&rows=%ld&offset=%ld", [searchTerm BC_escapeURIReservedCharacters], (long)rows, (long)offset];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)activitySmartSearchURLForSearchTerm:(NSString *)searchTerm offset:(NSInteger)offset rows:(NSInteger)rows
{
	NSMutableString *serviceURLString = [NSMutableString stringWithString:[self smartSearchURLString]];

	[serviceURLString appendFormat:@"?searchtype=search&search=%@&sections=activity&rows=%ld&offset=%ld",
		[searchTerm BC_escapeURIReservedCharacters], (long)rows, (long)offset];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - product recommendations
+ (NSString *)productRecommendationsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"recommendations.php"];
}

+ (NSURL *)productRecommendationsURLShowHidden:(BOOL)showHidden
{
	NSMutableString *serviceURLString = [[NSMutableString alloc]
		initWithString:[self productRecommendationsURLString]];
	if (showHidden) {
		[serviceURLString appendString:@"?show_hidden=1"];
	}

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - categories
+ (NSString *)categoriesURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"categories.php"];
}

+ (NSURL *)categoriesURL
{
	return [NSURL URLWithString:[self categoriesURLString]];
}

#pragma mark - store categories
+ (NSString *)storeCategoriesUrlString
{
	return [[self apiRootURLString] stringByAppendingString:@"storecategories.php"];
}

+ (NSURL *)storeCategoriesUrl
{
	return [NSURL URLWithString:[self storeCategoriesUrlString]];
}

+ (NSURL *)storeCategoriesUrlForStoreId:(NSNumber *)storeId
{
	NSString *serviceURLString = [[self storeCategoriesUrlString]
		stringByAppendingFormat:@"?entity_id=%@", storeId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSString *)checkInUrlString
{
	return [[self apiRootURLString] stringByAppendingString:@"checkin.php"];
}

+ (NSURL *)checkInUrl
{
	return [NSURL URLWithString:[self checkInUrlString]];
}

#pragma mark - Pantry
+ (NSString *)pantryURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"pantry.php"];
}

+ (NSURL *)pantryURL
{
	return [NSURL URLWithString:[self pantryURLString]];
}

+ (NSURL *)pantryUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self pantryURLString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Single Use IDs
+ (NSString *)singleUseURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"singleuse.php"];
}

+ (NSURL *)singleUseURL
{
	return [NSURL URLWithString:[self singleUseURLString]];
}

#pragma mark - Promotions
+ (NSString *)promotionsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"promotions.php"];
}

+ (NSURL *)promotionsURL
{
	return [NSURL URLWithString:[self promotionsURLString]];
}

+ (NSURL *)promotionsSummariesForProductURL:(NSNumber *)productId
{
	NSString *serviceURLString = [[self promotionsURLString]
		stringByAppendingFormat:@"?promo_summary_for_product=1&product_id=%@", productId];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Nutrition
+ (NSString *)nutritionURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"nutrition.php"];
}

+ (NSURL *)nutritionURL
{
	return [NSURL URLWithString:[self nutritionURLString]];
}

+ (NSURL *)nutritionForProductURL:(NSNumber *)productId
{
	NSString *serviceURLString = [[self nutritionURLString] stringByAppendingFormat:@"?product_id=%@", productId];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Meals
+ (NSString *)mealsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"meals.php"];
}

+ (NSURL *)mealsURL
{
	return [NSURL URLWithString:[self mealsURLString]];
}

+ (NSURL *)mealsUrlNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit
{
	NSString *serviceURLString = [[self mealsURLString]
		stringByAppendingFormat:@"?newer_than=%@&offset=%lu&limit=%lu", timestamp, (long)offset, (long)limit];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - HealthProfile
+ (NSString *)healthProfileURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"healthprofile.php"];
}

+ (NSURL *)healthProfileURL
{
	return [NSURL URLWithString:[self healthProfileURLString]];
}

#pragma mark - FoodLog
+ (NSString *)foodLogURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"foodlog.php"];
}

+ (NSURL *)foodLogURL
{
	return [NSURL URLWithString:[self foodLogURLString]];
}

+ (NSURL *)foodLogURLstart:(NSNumber *)start end:(NSNumber *)end
{
	NSString *serviceURLString = [[self foodLogURLString]
		stringByAppendingFormat:@"?start=%@&end=%@", start, end];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)foodLogURLNewerThan:(NSNumber *)timestamp withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit
{
	NSString *serviceURLString = [[self foodLogURLString]
		stringByAppendingFormat:@"?newer_than=%@&offset=%lu&limit=%lu", timestamp, (long)offset, (long)limit];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - LoggingNotes
+ (NSString *)loggingNotesURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"loggingnotes.php"];
}

+ (NSURL *)loggingNotesURL
{
	return [NSURL URLWithString:[self loggingNotesURLString]];
}

+ (NSURL *)loggingNotesURLNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self loggingNotesURLString] stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - MealPredictions (part of FoodLog)
+ (NSString *)mealPredictionsURLString
{
	return [[BCUrlFactory apiRootURLString] stringByAppendingString:@"mealpredictions.php"];
}

+ (NSURL *)mealPredictionsURL
{
	NSString *serviceURLString = [BCUrlFactory mealPredictionsURLString];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - MealSidePredictions (part of FoodLog)
+ (NSString *)mealSidePredictionsURLString
{
	return [[BCUrlFactory apiRootURLString] stringByAppendingString:@"mealsidepredictions.php"];
}

+ (NSURL *)mealSidePredictionsUrlForItemID:(NSNumber *)itemID itemType:(NSString *)itemType showDetails:(BOOL)showDetails 
{
	NSString *serviceURLString = [[self mealSidePredictionsURLString] stringByAppendingFormat:@"?item_id=%@&item_type=%@", itemID, itemType];

	if (!showDetails) {
		serviceURLString = [serviceURLString stringByAppendingString:@"&nodetails=1"];
	}

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - ActivityLog & ActivityPlan
+ (NSString *)activityRecordUrlString
{
	return [[self apiRootURLString] stringByAppendingString:@"activityrecord.php"];
}

+ (NSURL *)activityLogURL
{
	return [NSURL URLWithString:[self activityRecordUrlString]];
}

+ (NSURL *)activityLogUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self activityRecordUrlString]
		stringByAppendingFormat:@"?used_by=logger&newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)activityPlanURL
{
	return [NSURL URLWithString:[self activityRecordUrlString]];
}

+ (NSURL *)activityPlanUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self activityRecordUrlString]
		stringByAppendingFormat:@"?used_by=planner&newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

// TODO These probably need to go away, as activityLog likely replaces their use!
#pragma mark - trainingActivity
+ (NSString *)trainingActivityUrlString
{
	return [[self apiRootURLString] stringByAppendingString:@"trainingactivity.php"];
}

+ (NSURL *)trainingActivityURL
{
	return [NSURL URLWithString:[self trainingActivityUrlString]];
}

+ (NSURL *)trainingActivityUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self trainingActivityUrlString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - trainingAssessment
+ (NSString *)trainingAssessmentUrlString
{
	return [[self apiRootURLString] stringByAppendingString:@"trainingassessment.php"];
}

+ (NSURL *)trainingAssessmentURL
{
	return [NSURL URLWithString:[self trainingAssessmentUrlString]];
}

+ (NSURL *)trainingAssessmentUrlStart:(NSNumber *)start end:(NSNumber *)end
{
	NSString *serviceURLString = [[self trainingAssessmentUrlString]
		stringByAppendingFormat:@"?start=%@&end=%@", start, end];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)trainingAssessmentUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self trainingAssessmentUrlString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - trainingCardio
+ (NSString *)trainingCardioUrlString
{
	return [[self apiRootURLString] stringByAppendingString:@"trainingcardio.php"];
}

+ (NSURL *)trainingCardioURL
{
	return [NSURL URLWithString:[self trainingCardioUrlString]];
}

+ (NSURL *)trainingCardioUrlStart:(NSNumber *)start end:(NSNumber *)end
{
	NSString *serviceURLString = [[self trainingCardioUrlString]
		stringByAppendingFormat:@"?start=%@&end=%@", start, end];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)trainingCardioUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self trainingCardioUrlString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - trainingStrength
+ (NSString *)trainingStrengthUrlString
{
	return [[self apiRootURLString] stringByAppendingString:@"trainingstrength.php"];
}

+ (NSURL *)trainingStrengthURL
{
	return [NSURL URLWithString:[self trainingStrengthUrlString]];
}

+ (NSURL *)trainingStrengthUrlStart:(NSNumber *)start end:(NSNumber *)end
{
	NSString *serviceURLString = [[self trainingStrengthUrlString]
		stringByAppendingFormat:@"?start=%@&end=%@", start, end];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)trainingStrengthUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self trainingStrengthUrlString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - tracking
+ (NSString *)trackingUrlString
{
	return [[self apiRootURLString] stringByAppendingString:@"tracking.php"];
}

+ (NSURL *)trackingURL
{
	return [NSURL URLWithString:[self trackingUrlString]];
}

+ (NSURL *)trackingUrlNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self trackingUrlString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSString *)trackingTypeUrlString
{
	return [[self apiRootURLString] stringByAppendingString:@"trackingmeta.php"];
}

+ (NSURL *)trackingTypeURL
{
	return [NSURL URLWithString:[self trackingTypeUrlString]];
}

#pragma mark - Advisor Meal Plan
+ (NSString *)advisorMealPlanURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"advisormealplans.php"];
}

+ (NSURL *)advisorMealPlansURL
{
	NSString *serviceURLString = [self advisorMealPlanURLString];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Advisor Meal Plan Timeline
+ (NSString *)advisorMealPlanTimelineURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"advisormealplantimeline.php"];
}

// GET to get the current timeline, PUT to start a plan, DELETE to stop a plan
+ (NSURL *)advisorMealPlanTimelineURL
{
	NSString *serviceURLString = [self advisorMealPlanTimelineURLString];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Advisor Meal Plan Tags
+ (NSString *)advisorMealPlanTagsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"advisormealplantags.php"];
}

+ (NSURL *)urlForMealPlanId:(NSNumber *)planId tagId:(NSNumber *)tagId sections:(NSNumber *)sections offset:(NSNumber *)offset limit:(NSNumber *)limit searchText:(NSString *)searchText
{
	NSString *serviceURLString = [[self advisorMealPlanTagsURLString]
		stringByAppendingFormat:@"?plan_id=%@&tag_id=%@&sections=%@&offset=%@&limit=%@&searchtext=%@", planId, tagId, sections, offset,
		limit, searchText];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Recipes, MealPlanner
+ (NSString *)mealPlannerURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"mealplanner.php"];
}

+ (NSURL *)mealPlannerURL
{
	return [NSURL URLWithString:[self mealPlannerURLString]];
}

+ (NSURL *)mealPlannerURLNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self mealPlannerURLString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSString *)googleRecipeSearchURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"recipefinder.php"];
}

+ (NSURL *)googleRecipeSearchForSearchTerm:(NSString *)searchTerms page:(NSUInteger)page includeDetails:(BOOL)includeDetails;
{
	NSString *serviceURLString = [[self googleRecipeSearchURLString]
		stringByAppendingFormat:@"?searchterms=%@&page=%lu&include_details=%d",
		[searchTerms BC_escapeURIReservedCharacters], (unsigned long)page, includeDetails];
	return [NSURL URLWithString:serviceURLString];
}

+ (NSString *)recipeDetailsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"recipedetails.php"];
}

+ (NSURL *)recipeDetailsURLForRecipeId:(id)recipeId
{
	NSString *serviceURLString = [[self recipeDetailsURLString] stringByAppendingFormat:@"?recipe_id=%@", recipeId];
	return [NSURL URLWithString:serviceURLString];
}

+ (NSString *)publicRecipesURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"publicrecipes.php"];
}

+ (NSURL *)publicRecipesForSearchTerm:(NSString *)searchTerm showRows:(NSUInteger)rows rowOffset:(NSUInteger)offset
{
	NSString *serviceURLString = [[self publicRecipesURLString] stringByAppendingFormat:@"?search=%@&rows=%lu&offset=%lu", 
		[searchTerm BC_escapeURIReservedCharacters], (unsigned long)rows, (unsigned long)offset];
	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Account
+ (NSString *)accountURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"account.php"];
}

+ (NSURL *)accountURL
{
	return [NSURL URLWithString:[self accountURLString]];
}

#pragma mark - UserProfile
+ (NSString *)userProfileURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"userinfo.php"];
}

+ (NSURL *)userProfileURL
{
	return [NSURL URLWithString:[self userProfileURLString]];
}

#pragma mark - UserPermissions
+ (NSString *)userPermissionsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"permissions.php"];
}

+ (NSURL *)userPermissionsURL
{
	return [NSURL URLWithString:[self userPermissionsURLString]];
}

#pragma mark - images
+ (NSString *)imageURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"getimage.php"];
}

+ (NSURL *)imageURL
{
	return [NSURL URLWithString:[self imageURLString]];
}

+ (NSURL *)imageURLForImage:(NSNumber *)imageId imageSize:(NSString *)size
{
	NSString *serviceURLString = [[self imageURLString]
		stringByAppendingFormat:@"?image_id=%@&image_size=%@", imageId, size];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)imageURLForProductId:(NSNumber *)productId imageSize:(NSString *)size
{
	NSString *serviceURLString = [[self imageURLString]
		stringByAppendingFormat:@"?product_id=%@&image_size=%@", productId, size];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)imageURLForCoachId:(NSNumber *)coachId imageSize:(NSString *)size
{
	NSString *serviceURLString = [[self imageURLString]
		stringByAppendingFormat:@"?advisor_id=%@&image_size=%@", coachId, size];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)imageURLForUserId:(NSNumber *)userId inCommunity:(NSNumber *)communityId imageSize:(NSString *)size
{
	NSString *serviceURLString = [[self imageURLString]
		stringByAppendingFormat:@"?user_id=%@&community_id=%@&image_size=%@", userId, communityId, size];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - CustomFood
+ (NSString *)customFoodURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"customfood.php"];
}

+ (NSURL *)customFoodURL
{
	return [NSURL URLWithString:[self customFoodURLString]];
}

+ (NSURL *)customFoodURLNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self customFoodURLString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Restaurant Chain
+ (NSString *)restaurantChainURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"restaurantchains.php"];
}

+ (NSURL *)restaurantChainURLNewerThan:(NSNumber *)timestamp
{
	NSString *serviceURLString = [[self restaurantChainURLString]
		stringByAppendingFormat:@"?newer_than=%@", timestamp];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)restaurantChainURLSearch:(NSString *)search
{
	NSString *serviceURLString = [[self restaurantChainURLString]
		stringByAppendingFormat:@"?search=%@", [search BC_escapeURIReservedCharacters]];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Restaurant Menu Items
+ (NSString *)restaurantMenuItemURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"restaurantmenuitems.php"];
}

+ (NSURL *)menuItemsURLForRestaurant:(NSNumber *)restaurantId
{
	NSString *serviceURLString = [[self restaurantMenuItemURLString]
		stringByAppendingFormat:@"?restaurant_id=%@", restaurantId];

	return [NSURL URLWithString:serviceURLString];
}

+ (NSURL *)menuItemsSearchURLForSearchTerm:(NSString *)searchTerm
	limit:(NSUInteger)limit offset:(NSUInteger)offset
{
	NSString *serviceURLString = [[self restaurantMenuItemURLString]
		stringByAppendingFormat:@"?search=%@&limit=%lu&offset=%lu", [searchTerm BC_escapeURIReservedCharacters], (unsigned long)limit, (unsigned long)offset];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Advisors
+ (NSString *)advisorsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"useradvisors.php"];
}

+ (NSURL *)advisorsURL
{
	return [NSURL URLWithString:[self advisorsURLString]];
}

#pragma mark - Exercise Routines
+ (NSString *)exerciseRoutinesURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"exerciseroutines.php"];
}

+ (NSURL *)exerciseRoutinesURL
{
	return [NSURL URLWithString:[self exerciseRoutinesURLString]];
}

#pragma mark - Nutrients
+ (NSString *)nutrientsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"nutrients.php"];
}

+ (NSURL *)nutrientsURL
{
	return [NSURL URLWithString:[self nutrientsURLString]];
}

#pragma mark - Units
+ (NSString *)unitsURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"units.php"];
}

+ (NSURL *)unitsURL
{
	return [NSURL URLWithString:[self unitsURLString]];
}

#pragma mark - Coach Invites
+ (NSString *)coachInvitesURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"coachinvites.php"];
}

+ (NSURL *)coachInvitesURL
{
	return [NSURL URLWithString:[self coachInvitesURLString]];
}

#pragma mark - Advisor Activity Plan Timeline
+ (NSString *)activityPlanTimelineURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"activityplantimeline.php"];
}

// GET to get the current timeline, PUT to start a plan, DELETE to stop a plan
+ (NSURL *)activityPlanTimelineURL
{
	NSString *serviceURLString = [self activityPlanTimelineURLString];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Advisor Activity Plan
+ (NSString *)advisorActivityPlanURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"advisoractivityplans.php"];
}

+ (NSURL *)advisorActivityPlansURL
{
	NSString *serviceURLString = [self advisorActivityPlanURLString];

	return [NSURL URLWithString:serviceURLString];
}

#pragma mark - Sync Check
+ (NSString *)syncCheckURLString
{
	return [[self apiRootURLString] stringByAppendingString:@"synccheck.php"];
}

+ (NSURL *)syncCheckURLWithToken:(NSNumber *)syncToken
{
	NSString *serviceURLString = [[self syncCheckURLString]
		stringByAppendingFormat:@"?sync_token=%@", syncToken];

	return [NSURL URLWithString:serviceURLString];
}

@end
