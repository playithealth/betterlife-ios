//
//  PromotionDetailViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/27/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PromotionDetailViewController : UIViewController { }

@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) NSString *message;

@end
