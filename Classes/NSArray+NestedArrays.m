//
//  NSArray+NestedArrays.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/3/10.
//  Copyright 2010 BettrLife Corporation. All rights reserved.
//

#import "NSArray+NestedArrays.h"

@implementation NSArray (NestedArrays)
- (id)BC_nestedObjectAtIndexPath:(NSIndexPath *)indexPath
{
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSArray *subArray = [self objectAtIndex:section];

	if (![subArray isKindOfClass:[NSArray class]]) {
		return nil;
	}

	if (row >= [subArray count]) {
		return nil;
	}

	return [subArray objectAtIndex:row];
}

- (NSInteger)BC_countOfNestedArrayAtIndex:(NSUInteger)section
{
	NSArray *subArray = [self objectAtIndex:section];
	return [subArray count];
}

- (NSInteger)BC_countOfNestedArrays
{
	NSInteger count = 0;
	for (id subArray in self) {
		if ([subArray isKindOfClass:[NSArray class]]) {
			count += [(NSArray*)subArray count];
		}
	}
	return count;
}

- (void)BC_removeObjectAtIndexPath:(NSIndexPath *)indexPath
{
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSMutableArray *subArray = [self objectAtIndex:section];

	if (![subArray isKindOfClass:[NSMutableArray class]]) {
		return;
	}

	if (row >= [subArray count]) {
		return;
	}

	return [subArray removeObjectAtIndex:row];
}

- (void)BC_replaceObjectAtIndexPath:(NSIndexPath *)indexPath withObject:(id)object
{
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	NSMutableArray *subArray = [self objectAtIndex:section];

	if (![subArray isKindOfClass:[NSMutableArray class]]) {
		return;
	}

	if (row >= [subArray count]) {
		return;
	}

	[subArray replaceObjectAtIndex:row withObject:object];
}

@end
