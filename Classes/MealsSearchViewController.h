//
//  UNUSED!?
//  MealsSearchViewController.h
//  BuyerCompass
//
//  Created by Sef Tarbell on 10/4/12.
//  Copyright (c) 2012 BuyerCompass, LLC. All rights reserved.
//

#import "BCSearchViewController.h"

@interface MealsSearchViewController : BCSearchViewController

@property (strong, nonatomic) NSDate *planDate;
@property (strong, nonatomic) NSNumber *planTime;

@end
