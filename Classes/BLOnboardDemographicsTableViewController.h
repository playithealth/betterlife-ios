//
//  BLOnboardDemographicsTableViewController.h
//  BettrLife
//
//  Created by Greg Goodrich on 3/22/14.
//  Copyright (c) 2014 BettrLife Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCDetailViewDelegate.h"
#import "BLOnboardViewDelegate.h"

@protocol BLOnboardDelegate;

@interface BLOnboardDemographicsTableViewController : UITableViewController <BCDetailViewDelegate, BLOnboardViewDelegate>
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *viewStack;
@property (weak, nonatomic) id<BLOnboardDelegate> delegate;
@end
