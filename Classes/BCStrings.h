//
//  BCStrings.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 1/25/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

// update statuses
extern NSString * const kStatusOk;
extern NSString * const kStatusPost;
extern NSString * const kStatusPut;
extern NSString * const kStatusDelete;

// return value
extern NSString * const kSuccess;
extern NSString * const kErrors;

// common columns
extern NSString * const kAccountId;
extern NSString * const kCloudId;
extern NSString * const kDeleted;
extern NSString * const kUpdatedOn;
extern NSString * const kCreatedOn;
extern NSString * const kUserId;
extern NSString * const kStatus;
extern NSString * const kRating;
extern NSString * const kRatingCount;
extern NSString * const kMore;
extern NSString * const kOffset;
extern NSString * const kWebRecipeImageURL;
extern NSString * const kItemTypeProduct;
extern NSString * const kItemTypeRecipe;
extern NSString * const kItemTypeMenu;
extern NSString * const kItemTypeWater;
extern NSString * const kItemTypeCalories;
extern NSString * const kItemTypeOther;
extern NSString * const kSource;
extern NSString * const kType;
extern NSString * const kReadOn;
extern NSString * const kTitle;
extern NSString * const kUnits;
extern NSString * const kDescription;

// login
extern NSString * const kEmail;
extern NSString * const kPassword;
extern NSString * const kLoginZip;
extern NSString * const kLoginUserId;
extern NSString * const kLoginAccountId;
extern NSString * const kLoginName;
extern NSString * const kLoginOAuthAccessToken;
extern NSString * const kLoginOAuthSecret;
extern NSString * const kLoginPaidCustomer;

// store
extern NSString * const kId;
extern NSString * const kDefaultStore;
extern NSString * const kEntityId;
extern NSString * const kName;
extern NSString * const kStore;
extern NSString * const kAddress;
extern NSString * const kAddress2;
extern NSString * const kCity;
extern NSString * const kState;
extern NSString * const kZip;
extern NSString * const kPhone;
extern NSString * const kLatitude;
extern NSString * const kLongitude;
extern NSString * const kBuyerCompassClient;

// shoppinglist
extern NSString * const kProductCategory;
extern NSString * const kCategoryId;
extern NSString * const kProductId;
extern NSString * const kQuantity;
extern NSString * const kPrivate;
extern NSString * const kBarcode;
extern NSString * const kPurchased;
extern NSString * const kImageId;
extern NSString * const kPromoAvailable;
extern NSString * const kPromoSelectedCount;
extern NSString * const kLifestyleCount;
extern NSString * const kAllergyCount;
extern NSString * const kIngredientCount;

// category sort
extern NSString * const kStoreCategoryId;
extern NSString * const kStoreCategorySortOrder;

// recommendations
extern NSString * const kIsRecommendation;
extern NSString * const kRecommendationDays;
extern NSString * const kRecommendationHidden;

// products search
extern NSString * const kGenericName;
extern NSString * const kProductCategoryName;
extern NSString * const kProductCategoryId;
extern NSString * const kProductResults;
extern NSString * const kProductBrands;
extern NSString * const kProductCategories;

// purchase history
extern NSString * const kPurchaseHx;
extern NSString * const kPurchaseHxPurchasedOn;

// Pantry
extern NSString * const kSourceId;
extern NSString * const kItemName;
extern NSString * const kPantryItemCategory;
extern NSString * const kPantryItemPurchasedOn;

// inbox
extern NSString * const kMessages;
extern NSString * const kMessageId;
extern NSString * const kMessageDeliveryId;
extern NSString * const kMessageSubject;
extern NSString * const kMessageSummary;
extern NSString * const kMessageBody;
extern NSString * const kMessageText;
extern NSString * const kMessageIsPrivate;
extern NSString * const kMessageIsRead;
extern NSString * const kMessageReceiverDeleted;
extern NSString * const kMessageSenderDeleted;
extern NSString * const kMessageUserName;
extern NSString * const kMessageSenderId;
extern NSString * const kMessageToAccountId;
extern NSString * const kMessageToLoginId;
extern NSString * const kMessageReplyId;
extern NSString * const kMessageDeliveryList;
extern NSString * const kMessageToUsers;
extern NSString * const kMessageDeliveries;
extern NSString * const kAction;
extern NSString * const kMarkRead;
extern NSString * const kMarkDeleted;
extern NSString * const kMarkUnread;

// evaluations
extern NSString * const kEvaluationComment;
extern NSString * const kEvaluationUserName;
extern NSString * const kEvaluationCategory;

// Facebook
extern NSString * const kFacebookAppId;
extern NSString * const kFacebookApiKey;
extern NSString * const kFacebookApiSecret;
extern NSString * const kFacebookAccessToken;

// OAuth
extern NSString * const kMobileId;
extern NSString * const kDeviceName;
extern NSString * const kOAuthConsumerKey;
extern NSString * const kOAuthConsumerSecret;
extern NSString * const kOAuthConsumerHost;
extern NSString * const kOAuthAccessToken;
extern NSString * const kOAuthSecret;
extern NSString * const kOAuthNOnce;
extern NSString * const kOAuthSignature;
extern NSString * const kOAuthToken;
extern NSString * const kOAuthTimeStamp;
extern NSString * const kOAuthVersion;
extern NSString * const kOAuthSignatureMethod;

// single use IDs
extern NSString * const kSingleUseTokenKey;
extern NSString * const kSingleUseTokenAgeKey;

// used to pass the updates in shopping list
extern NSString * const kUpdatingDevice;

// promotions
extern NSString * const kPromotionName;
extern NSString * const kDisclaimer;
extern NSString * const kDiscount;
extern NSString * const kPromotionDescription;
extern NSString * const kAccepted;
extern NSString * const kPromotionShoppingListId;
extern NSString * const kPromotionDelete;
extern NSString * const kPromotionId;

// meals
extern NSString * const kRecipes;
extern NSString * const kMealAdvisorId;
extern NSString * const kRecipeAdvisorId;
extern NSString * const kRecipeAdvisorName;
extern NSString * const kRecipeId;
extern NSString * const kRecipeURL;
extern NSString * const kRecipeName;
extern NSString * const kRecipeImageId;
extern NSString * const kRecipeImageURL;
extern NSString * const kRecipeSource;
extern NSString * const kRecipeInstructions;
extern NSString * const kRecipeServes;
extern NSString * const kRecipeCookTime;
extern NSString * const kRecipeCookTimeUnits;
extern NSString * const kRecipePrepTime;
extern NSString * const kRecipeIngredients;
extern NSString * const kRecipeExternalId;
extern NSString * const kRecipeIsOwner;
extern NSString * const kRecipeTags;

extern NSString * const kValueCooktimeMinutes;
extern NSString * const kValueCooktimeHours;

// meal ingredients
extern NSString * const kAddToShoppingList;
extern NSString * const kMeasure;
extern NSString * const kMeasureUnits;
extern NSString * const kMeasureUnitsId;
extern NSString * const kShoppingListText;
extern NSString * const kShoppingListQuantity;
extern NSString * const kShoppingListCategoryId;
extern NSString * const kIngredientNotes;
extern NSString * const kOriginalIngredientText;

// activity plan
extern NSString * const kAPDay;
extern NSString * const kAPActivity;
extern NSString * const kAPFactor;
extern NSString * const kAPDistance;
extern NSString * const kAPTime;
extern NSString * const kAPExerciseRoutinesID;

// meal planner
extern NSString * const kMealPlannerId;
extern NSString * const kMealPlannerDay;
extern NSString * const kMealPlannerTime;
extern NSString * const kMPShoppingListItems;
extern NSString * const kMPShoppingListItemQuantity;
extern NSString * const kMPShoppingListItemSLId;
extern NSString * const kMPShoppingListItemIngredientId;
extern NSString * const kMPShoppingListItemDateAdded;
extern NSString * const kMPMenuId;
extern NSString * const kMPRecipeId;
extern NSString * const kMPProductId;
extern NSString * const kMPItemType;
extern NSString * const kMPTagID;
extern NSString * const kMPServingSize;
extern NSString * const kMPServingSizeUOM;
extern NSString * const kMPServingSizeUOMAbbreviation;

// Advisor Meal Plan Tags
extern NSString * const kMPExternalId;
extern NSString * const kMPNutrients;
extern NSString * const kMPTagItemType;
extern NSString * const kMPTagServingsString;
extern NSString * const kMPTagServings;
extern NSString * const kMPTagServingsUnit;
extern NSString * const kMPTagServingSize;
extern NSString * const kMPTagServingSizeUOM;

// food log
extern NSString * const kFoodLogs;
extern NSString * const kFoodLogId;
extern NSString * const kFoodLogImageId;
extern NSString * const kFoodLogRecipeId;
extern NSString * const kFoodLogProductId;
extern NSString * const kFoodLogMenuId;
extern NSString * const kFoodLogDay;
extern NSString * const kFoodLogLogTime;
extern NSString * const kFoodLogMultiplier;
extern NSString * const kFoodLogName;
extern NSString * const kFoodLogUpdatedOn;
extern NSString * const kFoodLogDeleted;
extern NSString * const kFoodLogUpdatingDevice;
extern NSString * const kFoodLogServingSize;
extern NSString * const kFoodLogNutritionFactor;
extern NSString * const kFoodLogServingsUnits;
extern NSString * const kFoodLogItemType;
extern NSString * const kFoodLogItemTypeProduct;
extern NSString * const kFoodLogItemTypeRecipe;
extern NSString * const kFoodLogItemTypeMenu;
extern NSString * const kFoodLogItemTypeWater;
extern NSString * const kFoodLogItemTypeCalories;
extern NSString * const kFoodLogItemTypeOther;

// logging note
extern NSString * const kLoggingNoteDay;
extern NSString * const kLoggingNoteType;
extern NSString * const kLoggingNoteTypeFood;
extern NSString * const kLoggingNoteTypeExercise;
extern NSString * const kLoggingNoteNote;
extern NSString * const kLoggingNoteSource;

extern NSString * const kNutritionId;
extern NSString * const kNutritionCalories;
extern NSString * const kNutritionCaloriesFromFat;
extern NSString * const kNutritionSaturatedFatCalories;
extern NSString * const kNutritionTotalFat;
extern NSString * const kNutritionSaturatedFat;
extern NSString * const kNutritionPolyunsaturatedFat;
extern NSString * const kNutritionMonounsaturatedFat;
extern NSString * const kNutritionTransFat;
extern NSString * const kNutritionCholesterol;
extern NSString * const kNutritionSodium;
extern NSString * const kNutritionPotassium;
extern NSString * const kNutritionTotalCarbohydrates;
extern NSString * const kNutritionDietaryFiber;
extern NSString * const kNutritionSolubleFiber;
extern NSString * const kNutritionInsolubleFiber;
extern NSString * const kNutritionSugars;
extern NSString * const kNutritionSugarsAlcohol;
extern NSString * const kNutritionOtherCarbohydrates;
extern NSString * const kNutritionProtein;
extern NSString * const kNutritionVitaminAPercent;
extern NSString * const kNutritionVitaminCPercent;
extern NSString * const kNutritionCalciumPercent;
extern NSString * const kNutritionIronPercent;

extern NSString * const kNutrition;
extern NSString * const kNutritionName;
extern NSString * const kNutritionMasterId;
extern NSString * const kNutritionQuantity;
extern NSString * const kNutritionUnitOfMeasure;
extern NSString * const kNutritionPercent;

// training log common
extern NSString * const kTrainingLogDay;

// cardio log
extern NSString * const kCardioLogId;
extern NSString * const kCardioLogExercise;
extern NSString * const kCardioLogTime;
extern NSString * const kCardioLogDistance;
extern NSString * const kCardioLogResistance;
extern NSString * const kCardioLogIntensity;
extern NSString * const kCardioLogIntensityLow;
extern NSString * const kCardioLogIntensityMedium;
extern NSString * const kCardioLogIntensityHigh;
extern NSString * const kCardioLogHeartrate;
extern NSString * const kCardioLogEase;
extern NSString * const kCardioLogEaseEasy;
extern NSString * const kCardioLogEaseMedium;
extern NSString * const kCardioLogEaseHard;
extern NSString * const kCardioLogCalories;

// activity log
extern NSString * const kActivityRecordActivity; ///< Subkey of the dictionary
extern NSString * const kActivityId;
extern NSString * const kActivityBasedOnId;
extern NSString * const kActivityForceFactor;
extern NSString * const kActivityIsTimedActivity;
extern NSString * const kActivityLink;
extern NSString * const kActivityNotes;
extern NSString * const kActivityRoutineId;
extern NSString * const kActivitySets;
extern NSString * const kActivityRecordFactor;
extern NSString * const kActivityRecordTimed;
extern NSString * const kActivityRecordLink;
extern NSString * const kActivityRecordNotes;
extern NSString * const kActivityRecordTargetDate;
extern NSString * const kActivityRecordSets;
extern NSString * const kActivityRecordWorkoutId;
extern NSString * const kActivityRecordUpdatedOn;

// Activity Sets
extern NSString * const kActivitySetTypeId;
extern NSString * const kActivitySetSequenceNumber;
extern NSString * const kActivitySetZone;
extern NSString * const kActivitySetMiles;
extern NSString * const kActivitySetMPH;
extern NSString * const kActivitySetPounds;
extern NSString * const kActivitySetReps;
extern NSString * const kActivitySetSeconds;
extern NSString * const kActivitySetWatts;

// Old activity log stuff
extern NSString * const kActivityLogExerciseRoutinesId;
extern NSString * const kActivityLogTime;
extern NSString * const kActivityLogWeight;
extern NSString * const kActivityLogCalories;
extern NSString * const kActivityLogDistance;

// Tracking
extern NSString * const kTrackingID;
extern NSString * const kTrackingValue1;
extern NSString * const kTrackingValue2;
extern NSString * const kTrackingValue3;
extern NSString * const kTrackingValue4;
extern NSString * const kTrackingValue5;
extern NSString * const kTrackingValue6;
extern NSString * const kTrackingValues;
extern NSString * const kTrackingType;
extern NSString * const kTrackingDate;
extern NSString * const kTrackingBodyFat;
extern NSString * const kTrackingWeight;
extern NSString * const kTrackingDiastolic;
extern NSString * const kTrackingSystolic;
extern NSString * const kTrackingGlucose;
extern NSString * const kTrackingHA1c;
extern NSString * const kTrackingTypeId;
extern NSString * const kTrackingTypeView;
extern NSString * const kTrackingTypeReadOnly;
extern NSString * const kTrackingTypeSortOrder; ///< Not currently being passed from the cloud
extern NSString * const kTrackingTypeValues;
extern NSString * const kTrackingValueField;
extern NSString * const kTrackingValueDefault;
extern NSString * const kTrackingValueDataType;
extern NSString * const kTrackingValueUnitAbbrev;
extern NSString * const kTrackingValueRangeMin;
extern NSString * const kTrackingValueRangeMax;
extern NSString * const kTrackingValuePrecision;
extern NSString * const kTrackingValueOptional;

// assessment log
extern NSString * const kAssessmentLogId;
extern NSString * const kAssessmentLogAge;
extern NSString * const kAssessmentLogSex;
extern NSString * const kAssessmentLogHeight;
extern NSString * const kAssessmentLogWeight;
extern NSString * const kAssessmentLogBodyFatPercent;
extern NSString * const kAssessmentLogNeck;
extern NSString * const kAssessmentLogChest;
extern NSString * const kAssessmentLogWaist;
extern NSString * const kAssessmentLogStomach;
extern NSString * const kAssessmentLogHips;
extern NSString * const kAssessmentLogRightBicep;
extern NSString * const kAssessmentLogLeftBicep;
extern NSString * const kAssessmentLogRightThigh;
extern NSString * const kAssessmentLogLeftThigh;
extern NSString * const kAssessmentLogActivityLevel;
extern NSString * const kAssessmentLogCrunches;
extern NSString * const kAssessmentLogPushups;
extern NSString * const kAssessmentLogPullups;
extern NSString * const kAssessmentLogSquats;
extern NSString * const kAssessmentLogMileRun;

// user profile
extern NSString * const kUserProfileName;
extern NSString * const kUserProfileEmail;
extern NSString * const kUserProfileZipCode;
extern NSString * const kUserProfileActivityLevel;
extern NSString * const kUserProfileBirthday;
extern NSString * const kUserProfileBirthdayMonth;
extern NSString * const kUserProfileBirthdayDay;
extern NSString * const kUserProfileBirthdayYear;
extern NSString * const kUserProfileGender;
extern NSString * const kUserProfileHeight;
extern NSString * const kUserProfileGoalWeight;
extern NSString * const kUserProfileCurrentWeight;
extern NSString * const kUserProfileGoalCaloriesMin;
extern NSString * const kUserProfileGoalCaloriesMax;
extern NSString * const kUserProfileGoalProteinMin;
extern NSString * const kUserProfileGoalProteinMax;
extern NSString * const kUserProfileGoalFatMin;
extern NSString * const kUserProfileGoalFatMax;
extern NSString * const kUserProfileGoalSatFatMin;
extern NSString * const kUserProfileGoalSatFatMax;
extern NSString * const kUserProfileGoalCarbsMin;
extern NSString * const kUserProfileGoalCarbsMax;
extern NSString * const kUserProfileGoalSodiumMin;
extern NSString * const kUserProfileGoalSodiumMax;
extern NSString * const kUserProfileGoalCholesterolMin;
extern NSString * const kUserProfileGoalCholesterolMax;
extern NSString * const kUserProfileGoalNutritionEffort;
extern NSString * const kUserProfileOnboardingComplete;
extern NSString * const kUserProfileMyZone;
extern NSString * const kUserProfileSkin;
extern NSString * const kUserProfileSkinKosama;
extern NSString * const kUserProfileCustomNutritionTimeframe;
extern NSString * const kUserProfileCustomTrackingTimeframe;
extern NSString * const kUserProfileAlwaysShowNutrients;
extern NSString * const kUserProfileUseBMR;
extern NSString * const kNutritionMaxData;
extern NSString * const kNutritionMinData;

// strength log
extern NSString * const kStrengthLogId;
extern NSString * const kStrengthLogName;
extern NSString * const kStrengthLogFocus;
extern NSString * const kStrengthLogEquipment;
extern NSString * const kStrengthLogSets;
extern NSString * const kStrengthLogSetWeight;
extern NSString * const kStrengthLogSetReps;

// settings
extern NSString * const kShoppingListSortSetting;
extern NSString * const kPantrySortSetting;
extern NSString * const kAutoRefreshEnabledSetting;
extern NSString * const kAutoRefreshIntervalSetting;
extern NSString * const kCurrentNavigationLocation;
extern NSString * const kSettingWelcomeViewHasBeenShown;
extern NSString * const kSettingShowLoggingPredictions;
extern NSString * const kSettingDBRefreshBuild;

extern NSString * const kHealthFilters;

// Health Profile filter - Allergies
extern NSString * const kHealthFilterAllergies;
extern NSString * const kHealthFilterAllergyCount;
extern NSString * const kHealthFilterAllergyEggs;
extern NSString * const kHealthFilterAllergyFish;
extern NSString * const kHealthFilterAllergyMilk;
extern NSString * const kHealthFilterAllergyPeanuts;
extern NSString * const kHealthFilterAllergyShellfish;
extern NSString * const kHealthFilterAllergySesameSeeds;
extern NSString * const kHealthFilterAllergySoy;
extern NSString * const kHealthFilterAllergyTreeNuts;
extern NSString * const kHealthFilterAllergyWheat;

// Health Profile filter - "Lifestyle"
extern NSString * const kHealthFilterLifestyles;
extern NSString * const kHealthFilterLifestyleRequireAll;
extern NSString * const kHealthFilterLifestyleAHAHeartHealthy;
extern NSString * const kHealthFilterLifestyleCalcium;
extern NSString * const kHealthFilterLifestyleCount;
extern NSString * const kHealthFilterLifestyleFatFree;
extern NSString * const kHealthFilterLifestyleHighFiber;
extern NSString * const kHealthFilterLifestyleHighProtein;
extern NSString * const kHealthFilterLifestyleKosher;
extern NSString * const kHealthFilterLifestyleLowCal;
extern NSString * const kHealthFilterLifestyleLowCarb;
extern NSString * const kHealthFilterLifestyleLowSaturatedFat;
extern NSString * const kHealthFilterLifestyleLowSodium;
extern NSString * const kHealthFilterLifestyleUSDAOrganic;
extern NSString * const kHealthFilterLifestyleVegan;
extern NSString * const kHealthFilterLifestyleVegetarian;
extern NSString * const kHealthFilterLifestyleVitaminA;
extern NSString * const kHealthFilterLifestyleVitaminC;

// Health Profile filter - custom ingredient warnings
extern NSString * const kHealthFilterIngredientWarnings;

// Health Profile filter - Maximums
extern NSString * const kHealthFilterMaximums;
extern NSString * const kHealthFilterMaxCalories;
extern NSString * const kHealthFilterMaxSaturatedFat;
extern NSString * const kHealthFilterMaxSodium;

extern NSString * const kNotificationKeyObjectID;

// Restaurant Chains & Menu Items
extern NSString * const kNutritionUrl;
extern NSString * const kMenuItems;
extern NSString * const kMenuItemName;
extern NSString * const kMenuRestaurantName;
extern NSString * const kMenuCategory;
extern NSString * const kMenuServingSize;
extern NSString * const kMenuCaloriesFromFat;
extern NSString * const kMenuSaturatedFatCalories;
extern NSString * const kMenuTotalFat;
extern NSString * const kMenuSaturatedFat;
extern NSString * const kMenuPolyUnsaturatedFat;
extern NSString * const kMenuMonoUnsaturatedFat;
extern NSString * const kMenuTransFat;
extern NSString * const kMenuCholesterol;
extern NSString * const kMenuSodium;
extern NSString * const kMenuPotassium;
extern NSString * const kMenuTotalCarbohydrates;
extern NSString * const kMenuDietaryFiber;
extern NSString * const kMenuSolubleFiber;
extern NSString * const kMenuInsolubleFiber;
extern NSString * const kMenuSugar;
extern NSString * const kMenuSugarAlcohol;
extern NSString * const kMenuOtherCarbohydrates;
extern NSString * const kMenuProtein;
extern NSString * const kMenuVitaminA;
extern NSString * const kMenuVitaminC;
extern NSString * const kMenuCalcium;
extern NSString * const kMenuIron;

// CustomFood
extern NSString * const kCFUpdatedOn;
extern NSString * const kCFServingSize;
extern NSString * const kCFServingSizeUnit;
extern NSString * const kCFServingsPerContainer;
extern NSString * const kCFCalories;
extern NSString * const kCFCaloriesFromFat;
extern NSString * const kCFSaturatedFatCalories;
extern NSString * const kCFFat;
extern NSString * const kCFCFSaturatedFat;
extern NSString * const kCFPolyunsaturatedFat;
extern NSString * const kCFMonounsaturatedFat;
extern NSString * const kCFTransFat;
extern NSString * const kCFCholesterol;
extern NSString * const kCFSodium;
extern NSString * const kCFPotassium;
extern NSString * const kCFCarbohydrates;
extern NSString * const kCFFiber;
extern NSString * const kCFSolubleFiber;
extern NSString * const kCFInsolubleFiber;
extern NSString * const kCFSugar;
extern NSString * const kCFSugarAlcohol;
extern NSString * const kCFOtherCarbohydrates;
extern NSString * const kCFProtein;
extern NSString * const kCFVitaminA;
extern NSString * const kCFVitaminC;
extern NSString * const kCFCalcium;
extern NSString * const kCFIron;

// Advisor & AdvisorGroup
extern NSString * const kAdvisorId;
extern NSString * const kAdvisorCanMessage;
extern NSString * const kAdvisorCanRecommendMeals;
extern NSString * const kAdvisorCanRecommendMealPlans;
extern NSString * const kAdvisorCanRecommendExercise;
extern NSString * const kAdvisorCanViewNutrition;
extern NSString * const kAdvisorCanViewExercise;
extern NSString * const kAdvisorCanViewMeals;
extern NSString * const kAdvisorCanViewShoppingList;
extern NSString * const kAdvisorCanViewPantry;
extern NSString * const kAdvisorCanAddToShoppingList;
extern NSString * const kAdvisorCanDeleteFromShoppingList;
extern NSString * const kAdvisorConfirmation;
extern NSString * const kAdvisorGroups;
extern NSString * const kAdvisorGroupId;
extern NSString * const kAdvisorName;

// Advisor Plans aka Health Coach Plans - Common keys
extern NSString * const kAdvisorPlanName;
extern NSString * const kAdvisorPlanDescription;
extern NSString * const kAdvisorPlanIsRepeating;
extern NSString * const kAdvisorPlanPhases;

extern NSString * const kAdvisorPlanPhaseDuration;
extern NSString * const kAdvisorPlanPhaseNumber;
extern NSString * const kAdvisorPlanPhaseEntries;

extern NSString * const kAdvisorPlanStartDate;
extern NSString * const kAdvisorPlanStopDate;

// Advisor Meal Plans aka Health Coach Plans
extern NSString * const kAdvisorMealPlanId;
extern NSString * const kAdvisorMealPlanCancelDate;
extern NSString * const kAdvisorMealPlanAdvisorId;
extern NSString * const kAdvisorMealPlanId;
extern NSString * const kAdvisorMealPlanSugaryDrinks;
extern NSString * const kAdvisorMealPlanFruitsVeggies;

extern NSString * const kAdvisorMealPlanPhaseBMR;
extern NSString * const kAdvisorMealPlanPhaseAutoRatio;
extern NSString * const kAdvisorMealPlanPhaseProteinRatio;
extern NSString * const kAdvisorMealPlanPhaseTotalCarbohydrateRatio;
extern NSString * const kAdvisorMealPlanPhaseTotalFatRatio;

extern NSString * const kAdvisorMealPlanPhaseEntryID;
extern NSString * const kAdvisorMealPlanPhaseEntryLogtime;
extern NSString * const kAdvisorMealPlanPhaseEntryLogtimeNumber;
extern NSString * const kAdvisorMealPlanPhaseEntryNutritionPercent;
extern NSString * const kAdvisorMealPlanPhaseEntryUnplannedMealOption;
extern NSString * const kAdvisorMealPlanPhaseNutritionMaxID;
extern NSString * const kAdvisorMealPlanPhaseNutritionMinID;
extern NSString * const kAdvisorMealPlanPhaseNutritionMaxData;
extern NSString * const kAdvisorMealPlanPhaseNutritionMinData;

extern NSString * const kAdvisorMealPlanPhaseEntryTags;
extern NSString * const kAdvisorMealPlanPhaseEntryTagID;
extern NSString * const kAdvisorMealPlanPhaseEntryTagName;
extern NSString * const kAdvisorMealPlanPhaseEntryTagServingTargetQuantity;
extern NSString * const kAdvisorMealPlanPhaseEntryTagServingTargetUnit;

// Advisor Activity Plans aka Health Coach Plans
extern NSString * const kAdvisorActivityPlan;

extern NSString * const kAdvisorActivityPlanWorkout;
extern NSString * const kAdvisorActivityPlanWorkoutNumberToPerform;
extern NSString * const kAdvisorActivityPlanWorkoutPerformOperator;
extern NSString * const kAdvisorActivityPlanWorkoutTime;
extern NSString * const kAdvisorActivityPlanWorkoutActivities;

extern NSString * const kAdvisorActivityPlanWorkoutActivitySetTypeId;
extern NSString * const kAdvisorActivityPlanWorkoutActivitySetZone;
extern NSString * const kAdvisorActivityPlanWorkoutActivitySetMiles;
extern NSString * const kAdvisorActivityPlanWorkoutActivitySetMPH;
extern NSString * const kAdvisorActivityPlanWorkoutActivitySetPounds;
extern NSString * const kAdvisorActivityPlanWorkoutActivitySetSeconds;
extern NSString * const kAdvisorActivityPlanWorkoutActivitySetSequenceNumber;
extern NSString * const kAdvisorActivityPlanWorkoutActivitySetWatts;

extern NSString * const kAdvisorActivityPlanId;

// unified search
extern NSString * const kUnifiedSearchAccountID;
extern NSString * const kUnifiedSearchAdvisorID;
extern NSString * const kUnifiedSearchBarcode;
extern NSString * const kUnifiedSearchCookTime;
extern NSString * const kUnifiedSearchCookTimeUnits;
extern NSString * const kUnifiedSearchImageID;
extern NSString * const kUnifiedSearchImageURL;
extern NSString * const kUnifiedSearchIsOwner;
extern NSString * const kUnifiedSearchItemID;
extern NSString * const kUnifiedSearchItemType;
extern NSString * const kUnifiedSearchItemTypeMenuItem;
extern NSString * const kUnifiedSearchItemTypeProduct;
extern NSString * const kUnifiedSearchItemTypeRecipe;
extern NSString * const kUnifiedSearchItems;
extern NSString * const kUnifiedSearchName;
extern NSString * const kUnifiedSearchNutritionID;
extern NSString * const kUnifiedSearchPrepTime;
extern NSString * const kUnifiedSearchSections;
extern NSString * const kUnifiedSearchServes;
extern NSString * const kUnifiedSearchServingSizeText;
extern NSString * const kUnifiedSearchServingSizeUnit;
extern NSString * const kUnifiedSearchShowMore;
extern NSString * const kUnifiedSearchType;
extern NSString * const kUnifiedSearchURL;

// Sync Check
extern NSString * const kSyncToken;
extern NSString * const kUpdatedTables;
extern NSString * const kSyncActivityLog;
extern NSString * const kSyncActivityPlan;
extern NSString * const kSyncAdvisorActivityPlans;
extern NSString * const kSyncAdvisorGroupUsers;
extern NSString * const kSyncAdvisorMealPlans;
extern NSString * const kSyncAdvisorUsers;
extern NSString * const kSyncCategories;
extern NSString * const kSyncCategorySorting;
extern NSString * const kSyncCoachInvites;
extern NSString * const kSyncCommunities;
extern NSString * const kSyncExerciseActivities;
extern NSString * const kSyncExerciseAssessment;
extern NSString * const kSyncExerciseRoutines;
extern NSString * const kSyncExerciseStrength;
extern NSString * const kSyncFoodLog;
extern NSString * const kSyncLogNotes;
extern NSString * const kSyncMealPlan;
extern NSString * const kSyncMessages;
extern NSString * const kSyncNutrients;
extern NSString * const kSyncPantry;
extern NSString * const kSyncPermissions;
extern NSString * const kSyncProducts;
extern NSString * const kSyncPurchaseHx; // This one actually doesn't come down from syncCheck
extern NSString * const kSyncRecipes;
extern NSString * const kSyncShoppingSuggestions;
extern NSString * const kSyncShopping;
extern NSString * const kSyncThreadMessages;
extern NSString * const kSyncThreads;
extern NSString * const kSyncTracking;
extern NSString * const kSyncTrackingType;
extern NSString * const kSyncUnits;
extern NSString * const kSyncUserGroups;
extern NSString * const kSyncUserHealthFilters;
extern NSString * const kSyncUserStores;
extern NSString * const kSyncUsers;

// Permissions
extern NSString * const kPermission;
extern NSString * const kPermissionShop;
extern NSString * const kPermissionPantry;
extern NSString * const kPermissionMealplan;
extern NSString * const kPermissionActivityplan;
extern NSString * const kPermissionFoodlog;
extern NSString * const kPermissionActivitylog;
extern NSString * const kPermissionRecipes;
extern NSString * const kPermissionDashboard;
extern NSString * const kPermissionTracking;
extern NSString * const kPermissionGoals;
extern NSString * const kPermissionHealthprefs;

// enums that need to be seen in multiple places
typedef NS_ENUM(NSInteger, ShoppingListSortMode) {
	ShoppingListSortByCategory,
	ShoppingListSortByName,
	ShoppingListSortByStoreCategory
};

// Evaluation (Rating)
extern NSString * const kEvalUser;
extern NSString * const kEvalUserCategory;
extern NSString * const kEvalUserCategoryGeneral;
extern NSString * const kEvalUserRating;
extern NSString * const kEvalAggregate;
extern NSString * const kEvalAggregateCount;
extern NSString * const kEvalAggregateAverage;
extern NSString * const kEvalItemId;
extern NSString * const kEvalItemTypeId;
extern NSUInteger const kEvalItemTypeProduct;
extern NSUInteger const kEvalItemTypeRecipe;
extern NSUInteger const kEvalItemTypeMenuItem;
extern NSUInteger const kEvalItemTypeStore;

// Product warnings
extern NSString * const kProductWarningsAllergies;
extern NSString * const kProductWarningsIngredients;
extern NSString * const kProductWarningsLifestyle;

// Product info
extern NSString * const kProductInfoAllergies;
extern NSString * const kProductInfoIngredients;
extern NSString * const kProductInfoWarnings;

// Images
extern NSString * const kImageSizeSmall;
extern NSString * const kImageSizeMedium;
extern NSString * const kImageSizeLarge;

// Nutrients
extern NSString * const kNutrientName;
extern NSString * const kNutrientTitle;
extern NSString * const kNutrientPrecision;
extern NSString * const kNutrientUnits;
extern NSString * const kNutrientIsSubNutrient;
extern NSString * const kNutrientSortOrder;

// Units
extern NSString * const kUnitAbbreviation;
extern NSString * const kUnitSingular;
extern NSString * const kUnitPlural;
extern NSString * const kUnitSortOrder;

// Coach/Advisor invitations
extern NSString * const kInviteCode;
extern NSString * const kEntityName;
extern NSString * const kFirstTimeInvites;

// Community
extern NSString * const kCommunityMember;
extern NSString * const kCommunityVisibility;
extern NSString * const kCommunityId;
extern NSString * const kCommunityOwnerName;
extern NSString * const kCommunityOwnerId;
extern NSString * const kCommunityUpdatedOn;
extern NSString * const kCommunityType;
extern NSString * const kCommunityTypeCoach;
extern NSString * const kCommunityUsePhoto;
extern NSString * const kCommunityStatusNone;
extern NSString * const kCommunityStatusReviewed;
extern NSString * const kCommunityStatusAccepted;
extern NSString * const kCommunityStatusDeclined;
extern NSString * const kCommunityStatusLeft;

// Message Threads
extern NSString * const kThreadId;
extern NSString * const kThreadType;
extern NSString * const kThreadUpdatedOn;

// Thread Messages
extern NSString * const kSenderName;

// Sync Update Human Readable Messages
extern NSString * const kSyncUpdateUserStatusNotification;
extern NSString * const kSyncUpdateHumanReadableStatusKey;

extern NSString * const kSyncUpdateHumanReadable_ShoppingList;
extern NSString * const kSyncUpdateHumanReadable_Messages;
extern NSString * const kSyncUpdateHumanReadable_Stores;
extern NSString * const kSyncUpdateHumanReadable_PurchaseHistory;
extern NSString * const kSyncUpdateHumanReadable_ProductCategories;
extern NSString * const kSyncUpdateHumanReadable_StoreCategories;
extern NSString * const kSyncUpdateHumanReadable_Pantry;
extern NSString * const kSyncUpdateHumanReadable_Tokens;
extern NSString * const kSyncUpdateHumanReadable_Recipes;
extern NSString * const kSyncUpdateHumanReadable_MealPlanner;
extern NSString * const kSyncUpdateHumanReadable_ActivityPlans;
extern NSString * const kSyncUpdateHumanReadable_HealthChoices;
extern NSString * const kSyncUpdateHumanReadable_Recommendations;
extern NSString * const kSyncUpdateHumanReadable_FoodLog;
extern NSString * const kSyncUpdateHumanReadable_FoodLogNotes;
extern NSString * const kSyncUpdateHumanReadable_MealPredictions;
extern NSString * const kSyncUpdateHumanReadable_Activities;
extern NSString * const kSyncUpdateHumanReadable_TrainingAssessment;
extern NSString * const kSyncUpdateHumanReadable_Strengths;
extern NSString * const kSyncUpdateHumanReadable_Tracking;
extern NSString * const kSyncUpdateHumanReadable_TrackingTypes;
extern NSString * const kSyncUpdateHumanReadable_CustomFoods;
extern NSString * const kSyncUpdateHumanReadable_UserProfile;
extern NSString * const kSyncUpdateHumanReadable_UserPermissions;
extern NSString * const kSyncUpdateHumanReadable_Advisors;
extern NSString * const kSyncUpdateHumanReadable_MealPlans;
extern NSString * const kSyncUpdateHumanReadable_MealPlanTimeline;
extern NSString * const kSyncUpdateHumanReadable_ExerciseRoutines;
extern NSString * const kSyncUpdateHumanReadable_Nutrients;
extern NSString * const kSyncUpdateHumanReadable_MeasurementUnits;
extern NSString * const kSyncUpdateHumanReadable_CoachInvites;
extern NSString * const kSyncUpdateHumanReadable_Communities;
extern NSString * const kSyncUpdateHumanReadable_CommunityThreads;
extern NSString * const kSyncUpdateHumanReadable_CommunityThreadMessages;
extern NSString * const kSyncUpdateHumanReadable_ActivityPlanTimeline;
extern NSString * const kSyncUpdateHumanReadable_ActivityLog;
