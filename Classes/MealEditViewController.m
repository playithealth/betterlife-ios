//
//  MealEditViewController.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 8/30/11.
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import "MealEditViewController.h"

#import "BCObjectManager.h"
#import "BCUtilities.h"
#import "GenericValueDisplay.h"
#import "MealIngredient.h"
#import "MealIngredientEditView.h"
#import "NSArray+NestedArrays.h"
#import "NumberValue.h"
#import "UIColor+Additions.h"

const NSInteger kTagTextField = 10001;
const NSInteger kTagRating = 10002;
const NSInteger kTagCooktimeUnits = 10003;
const NSInteger kTagMEVSectionLabelTag = 10004;
const NSInteger kTagMEVLeftArrowDown = 10005;
const NSInteger kTagMEVRightArrowDown = 10006;
const NSInteger kTagMEVLeftArrowUp = 10007;
const NSInteger kTagMEVRightArrowUp = 10008;

const NSInteger kCellWidth = 320;
const NSInteger kCellPadding = 10;
const NSInteger kHackExtraSize = 10;
const NSInteger kStockInstructionsCellHeight = 150;

const NSInteger kSectionOverall = 0;
const NSInteger kSectionIngredients = 1;
const NSInteger kSectionInstructions = 2;

const NSInteger kRowName = 0;
const NSInteger kRowRating = 1;
const NSInteger kRowServes = 2;
const NSInteger kRowCalories = 3;
const NSInteger kRowCookTime = 4;
const NSInteger kRowProtein = 5;
const NSInteger kRowCarbohydrates = 6;
const NSInteger kRowFat = 7;
const NSInteger kRowSaturatedFat = 8;
const NSInteger kRowSodium = 9;
const NSInteger kRowCholesterol = 10;
const NSInteger kRowCaloriesFromFat = 11;

const NSInteger kSegmentMinutes = 0;
const NSInteger kSegmentHours = 1;

@interface MealEditViewController () <UITextFieldDelegate>

@property (nonatomic, strong) UITableViewCell *nameCell;
@property (nonatomic, strong) UITableViewCell *ratingCell;
@property (nonatomic, strong) UITableViewCell *servesCell;
@property (nonatomic, strong) UITableViewCell *caloriesCell;
@property (nonatomic, strong) UITableViewCell *cookTimeCell;
@property (nonatomic, strong) UITableViewCell *proteinCell;
@property (nonatomic, strong) UITableViewCell *carbsCell;
@property (nonatomic, strong) UITableViewCell *fatCell;
@property (nonatomic, strong) UITableViewCell *satfatCell;
@property (nonatomic, strong) UITableViewCell *sodiumCell;
@property (nonatomic, strong) UITableViewCell *cholesterolCell;
@property (nonatomic, strong) UITableViewCell *caloriesFromFatCell;
@property (nonatomic, strong) UITableViewCell *addIngredientCell;
@property (nonatomic, strong) UITableViewCell *instructionsCell;
@property (strong, nonatomic) NSArray *rowCells;
@property (strong, nonatomic) UIToolbar *keyboardToolbar;
@property (strong, nonatomic) UITextField *editingTextField;
@property (assign, nonatomic) BOOL changedMeal;
@property (strong, nonatomic) NSArray *ingredients;
@property (strong, nonatomic) BCMoreLessFooter *footerView;
@property (strong, nonatomic) NSIndexPath *editingIndexPath;

- (void)cooktimeChanged:(id)sender;
- (UITableViewCell *)createCellWithLabelText:(NSString *)labelText textFieldKeyboardType:(UIKeyboardType)keyboardType;
- (UITableViewCell *)createCooktimeCellWithText:(NSString *)labelText;
- (UITableViewCell *)createRatingCellWithText:(NSString *)labelText;
- (void)saveTextFieldValue:(UITextField *)textField;
- (void)keyboardDidShowNotification:(NSNotification *)aNotification;
- (void)keyboardWillHideNotification:(NSNotification *)aNotification;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation MealEditViewController

#pragma mark - memory management
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.changedMeal = NO;
		self.footerView = nil;
	}
	return self;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];

	if (self.recipe) {
		self.ingredients = [[self.recipe valueForKey:@"ingredients"] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES]]];
	}

	if (!self.keyboardToolbar) {
		self.keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
		self.keyboardToolbar.barStyle = UIBarStyleBlack;
		self.keyboardToolbar.translucent = YES;

		UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
		UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboard:)];

		[self.keyboardToolbar setItems:@[flexibleSpace, doneButton] animated:NO];
	}

	self.nameCell = [self createCellWithLabelText:@"Name:" textFieldKeyboardType:UIKeyboardTypeDefault];
	self.ratingCell = [self createRatingCellWithText:@"Rating:"];
	self.servesCell = [self createCellWithLabelText:@"Serves:" textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
	self.caloriesCell = [self createCellWithLabelText:@"Calories:" textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
	self.cookTimeCell = [self createCooktimeCellWithText:@"Cook Time:"];
	self.proteinCell = [self createCellWithLabelText:@"Protein:" textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
	self.carbsCell = [self createCellWithLabelText:@"Carbohydrates:" textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
	self.fatCell = [self createCellWithLabelText:@"Fat:" textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
	self.satfatCell = [self createCellWithLabelText:@"Saturated Fat:" textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
	self.sodiumCell = [self createCellWithLabelText:@"Sodium:" textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
	self.cholesterolCell = [self createCellWithLabelText:@"Cholesterol:" textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
	self.caloriesFromFatCell = [self createCellWithLabelText:@"Calories From Fat:" textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];

	self.rowCells = @[self.nameCell, self.ratingCell, self.servesCell,
		self.cookTimeCell, self.caloriesCell, self.caloriesFromFatCell,
		self.fatCell, self.satfatCell, self.cholesterolCell, self.sodiumCell,
		self.carbsCell, self.proteinCell];

	UIView *backgroundView = [[UIView alloc] init];
	backgroundView.backgroundColor = [UIColor BC_lightGrayColor];
	self.tableView.backgroundView = backgroundView;

	// Allows the more/less section footer to cuddle up to the section
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewDidUnload
{
	[self setTableView:nil];
	[super viewDidUnload];
	// TODO Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];

	self.title = @"Edit Recipe";

	// Register for notification when the keyboard will be shown
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardDidShowNotification:)
		name:UIKeyboardDidShowNotification
		object:nil];

	// Register for notification when the keyboard will be hidden
	[[NSNotificationCenter defaultCenter] addObserver:self
		selector:@selector(keyboardWillHideNotification:)
		name:UIKeyboardWillHideNotification
		object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
	if (self.editingTextField != nil) {
		[self saveTextFieldValue:self.editingTextField];
		self.editingTextField = nil;
	}

	// notify delegate
	if (self.changedMeal) {
		if (self.delegate
				&& [self.delegate conformsToProtocol:@protocol(MealEditViewDelegate)]) {
			[self.delegate mealEditView:self didUpdateMeal:self.recipe];
		}
		// Post a notification that we've updated the recipe
		[[NSNotificationCenter defaultCenter] postNotificationName:[RecipeMO entityName]
			object:self
			userInfo:@{ kNotificationKeyObjectID : [self.recipe objectID] }];
		// Sync the change out to the cloud
		[BCObjectManager syncCheck:SendOnly];
	}
	self.changedMeal = NO;

	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

	[super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// Return YES for supported orientations
	return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - local methods
- (void)cooktimeChanged:(id)sender
{
	self.changedMeal = YES;

	switch ([sender selectedSegmentIndex]) {
		case kSegmentMinutes:
			self.recipe.cookTimeUnit = kValueCooktimeMinutes;
			break;
		case kSegmentHours:
			self.recipe.cookTimeUnit = kValueCooktimeHours;
			break;
		case UISegmentedControlNoSegment:
		default:
			self.recipe.cookTimeUnit = nil;
			break;
	}
}

- (UITableViewCell *)createCellWithLabelText:(NSString *)labelText textFieldKeyboardType:(UIKeyboardType)keyboardType
{
	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];

	cell.selectionStyle = UITableViewCellSelectionStyleNone;

	UILabel *aLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 25)];
	aLabel.adjustsFontSizeToFitWidth = YES;
	aLabel.textAlignment = NSTextAlignmentRight;
	aLabel.font = [UIFont boldSystemFontOfSize:14];
	aLabel.text = labelText;
	aLabel.backgroundColor = [UIColor BC_groupedTableViewCellBackgroundColor];
	// aLabel.backgroundColor = [UIColor clearColor];
	[cell.contentView addSubview:aLabel];

	UITextField *aTextField = [[UITextField alloc] initWithFrame:CGRectMake(115, 12, 175, 25)];
	aTextField.enabled = NO;
	aTextField.tag = kTagTextField;
	[aTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[aTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
	aTextField.keyboardType = keyboardType;

	aTextField.inputAccessoryView = self.keyboardToolbar;
	[cell.contentView addSubview:aTextField];

	return cell;
}

- (UITableViewCell *)createCooktimeCellWithText:(NSString *)labelText
{
	UITableViewCell *cell = [self createCellWithLabelText:labelText
		textFieldKeyboardType:UIKeyboardTypeNumbersAndPunctuation];

	UITextField *aTextField = (UITextField *)[cell viewWithTag:kTagTextField];

	aTextField.frame = CGRectMake(115, 12, 75, 25);

	UISegmentedControl *cooktimeUnits = [[UISegmentedControl alloc]
		initWithFrame:CGRectMake(190, 8, 100, 26)];
	[cooktimeUnits insertSegmentWithTitle:@"Min" atIndex:kSegmentMinutes animated:NO];
	[cooktimeUnits insertSegmentWithTitle:@"Hr" atIndex:kSegmentHours animated:NO];
	cooktimeUnits.tag = kTagCooktimeUnits;
	[cooktimeUnits addTarget:self action:@selector(cooktimeChanged:)
		forControlEvents:UIControlEventValueChanged];
	[cell.contentView addSubview:cooktimeUnits];

	return cell;
}

- (UITableViewCell *)createRatingCellWithText:(NSString *)labelText
{
	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
		reuseIdentifier:nil];

	UILabel *aLabel = [[UILabel alloc]
		initWithFrame:CGRectMake(10, 10, 100, 25)];

	aLabel.adjustsFontSizeToFitWidth = YES;
	aLabel.textAlignment = NSTextAlignmentRight;
	aLabel.font = [UIFont boldSystemFontOfSize:14];
	aLabel.text = labelText;
	aLabel.backgroundColor = [UIColor BC_groupedTableViewCellBackgroundColor];
	// aLabel.backgroundColor = [UIColor clearColor];
	[cell.contentView addSubview:aLabel];

	StarRatingControl *ratingControl = [[StarRatingControl alloc]
		initWithFrame:CGRectMake(120, 6, 160, 32)
		animateStars:YES
		emptyStarImage:[UIImage imageNamed:@"star-large-03.png"]
		fullStarImage:[UIImage imageNamed:@"star-large-01.png"]
		delegate:self];
	ratingControl.rating = 0;
	ratingControl.tag = kTagRating;
	[cell.contentView addSubview:ratingControl];

	return cell;
}

- (void)resignKeyboard:(id)sender
{
	self.editingTextField.enabled = NO;
	[self.editingTextField resignFirstResponder];
	self.editingTextField = nil;
}

- (void)saveTextFieldValue:(UITextField *)textField
{
	// Get the cell in which the textfield is embedded
	id textFieldSuper = textField;
	while (textFieldSuper && ![textFieldSuper isKindOfClass:[UITableViewCell class]]) {
		textFieldSuper = [textFieldSuper superview];
	}
	NSInteger idx = [self.rowCells indexOfObject:textFieldSuper];

	switch (idx) {
		case kRowName:
			if (![self.recipe.name isEqualToString:textField.text]) {
				self.recipe.name = textField.text;
				// Clear the relationship to the original 'recipe' since they've changed it
				self.recipe.externalId = nil;
			}
			break;
		case kRowCalories:
			self.recipe.calories = [textField.text numberValueDecimal];
			break;
		case kRowServes:
			self.recipe.serves = [textField.text numberValueDecimal];
			break;
		case kRowCookTime:
			self.recipe.cookTime = [textField.text numberValueDecimal];
			break;
		case kRowProtein:
			self.recipe.protein = [textField.text numberValueDecimal];
			break;
		case kRowCarbohydrates:
			self.recipe.carbs = [textField.text numberValueDecimal];
			break;
		case kRowFat:
			self.recipe.fat = [textField.text numberValueDecimal];
			break;
		case kRowSaturatedFat:
			self.recipe.satfat = [textField.text numberValueDecimal];
			break;
		case kRowSodium:
			self.recipe.sodium = [textField.text numberValueDecimal];
			break;
		case kRowCholesterol:
			self.recipe.cholesterol = [textField.text numberValueDecimal];
			break;
		case kRowCaloriesFromFat:
			self.recipe.caloriesFromFat = [textField.text numberValueDecimal];
			break;
		default:
			break;
	}
	textField.enabled = NO;
	self.changedMeal = YES;
}

- (void)moreLessFooterTapped:(BCMoreLessFooter *)footer
{
	NSArray *extraRows = @[
		[NSIndexPath indexPathForRow:kRowProtein inSection:kSectionOverall],
		[NSIndexPath indexPathForRow:kRowCarbohydrates inSection:kSectionOverall],
		[NSIndexPath indexPathForRow:kRowFat inSection:kSectionOverall],
		[NSIndexPath indexPathForRow:kRowSaturatedFat inSection:kSectionOverall],
		[NSIndexPath indexPathForRow:kRowSodium inSection:kSectionOverall],
		[NSIndexPath indexPathForRow:kRowCholesterol inSection:kSectionOverall],
		[NSIndexPath indexPathForRow:kRowCaloriesFromFat inSection:kSectionOverall]];

	if ([self.footerView isCollapsed]) {
		// Show less
		[self.tableView beginUpdates];
		[self.tableView deleteRowsAtIndexPaths:extraRows withRowAnimation:UITableViewRowAnimationTop];
		[self.tableView endUpdates];
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:kRowCookTime inSection:kSectionOverall]
			atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}
	else {
		// Show more
		[self.tableView beginUpdates];
		[self.tableView insertRowsAtIndexPaths:extraRows withRowAnimation:UITableViewRowAnimationBottom];
		[self.tableView endUpdates];
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:kRowCaloriesFromFat inSection:kSectionOverall]
			atScrollPosition:UITableViewScrollPositionBottom animated:YES];
	}

}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString *theTitle = nil;

	switch (section) {
		case kSectionOverall:
			break;
		case kSectionIngredients:
			theTitle = @"Ingredients";
			break;
		case kSectionInstructions:
			theTitle = @"Instructions";
			break;
		default:
			break;
	}

	return theTitle;
}

- (UIView *)tableView:(UITableView *)theTableView
viewForFooterInSection:(NSInteger)section
{
	UIImageView *sectionFooterView = nil;

	if (section == kSectionOverall) {
		if (!self.footerView) {
			self.footerView = [[BCMoreLessFooter alloc] initWithDelegate:self];
		}

		sectionFooterView = self.footerView;
	}

	return sectionFooterView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rows = 0;

	switch (section) {
		case kSectionOverall:
			if ([self.footerView isExpanded]) {
				rows = [self.rowCells count];
			}
			else {
				rows = 5;
			}
			break;
		case kSectionIngredients:
			rows = [self.ingredients count] + 1;
			break;
		case kSectionInstructions:
			rows = 1;
			break;
		default:
			break;
	}

	return rows;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView
cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;

	switch ([indexPath section]) {
		case kSectionOverall:
			{
				cell = [self.rowCells objectAtIndex:indexPath.row];
				break;
			}
		case kSectionIngredients:
			if ([indexPath row] < [self.ingredients count]) {
				// Ingredient
				static NSString *ingredientCellId = @"IngredientCell";
				cell = [self.tableView dequeueReusableCellWithIdentifier:ingredientCellId];

				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				cell.textLabel.numberOfLines = 2;
			}
			else {
				// Add ingredient row
				static NSString *addIngredientCellId = @"AddIngredientCell";
				cell = [self.tableView dequeueReusableCellWithIdentifier:addIngredientCellId];
			}
			break;
		case kSectionInstructions:
			{
				UITextView *textView = nil;
				if (!self.instructionsCell) {
					self.instructionsCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
						reuseIdentifier:nil];
					self.instructionsCell.selectionStyle = UITableViewCellSelectionStyleNone;

					// establish the size needed for the cell, and do this one time only, as this
					// cell is editable, and if done multiple times, the textView will grow larger than
					// the cell
					CGSize size = [self.recipe.instructions sizeWithFont:[UIFont systemFontOfSize:14]
						constrainedToSize:CGSizeMake(300 - 16, 10000)];

					textView = [[UITextView alloc]
						initWithFrame:CGRectMake(0, kCellPadding, 300,
								MAX(kStockInstructionsCellHeight, size.height + kHackExtraSize))];
					textView.delegate = self;
					textView.font = [UIFont systemFontOfSize:14];
					textView.tag = kTagTextField;
					// This appears to be the default table cell grouped background color
					textView.backgroundColor = [UIColor BC_groupedTableViewCellBackgroundColor];

					[self.instructionsCell.contentView addSubview:textView];
				}

				cell = self.instructionsCell;
				break;
			}
		default:
			break;
	}

	[self configureCell:cell atIndexPath:indexPath];

	return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	if ([indexPath section] == kSectionOverall) {
		id <GenericValueDisplay, NSObject> rowValue = nil;
		switch ([indexPath row]) {
			case kRowName:
				{
					rowValue = self.recipe.name;
					break;
				}
			case kRowRating:
				{
					StarRatingControl *ratingControl = (StarRatingControl *)[cell viewWithTag:kTagRating];
					ratingControl.rating = [self.recipe.rating integerValue];
				}
				break;
			case kRowCalories:
				{
					rowValue = self.recipe.calories;
					break;
				}
			case kRowServes:
				{
					rowValue = self.recipe.serves;
					break;
				}
			case kRowCookTime:
				{
					rowValue = self.recipe.cookTime;
					UISegmentedControl *cooktimeUnits = (UISegmentedControl *)[cell
						viewWithTag:kTagCooktimeUnits];
					if ([self.recipe.cookTimeUnit isEqualToString:kValueCooktimeMinutes]) {
						[cooktimeUnits setSelectedSegmentIndex:0];
					}
					else if ([self.recipe.cookTimeUnit isEqualToString:kValueCooktimeHours]) {
						[cooktimeUnits setSelectedSegmentIndex:1];
					}
					else {
						[cooktimeUnits setSelectedSegmentIndex:UISegmentedControlNoSegment];
					}
					break;
				}
			case kRowProtein:
				{
					rowValue = self.recipe.protein;
					break;
				}
			case kRowCarbohydrates:
				{
					rowValue = self.recipe.carbs;
					break;
				}
			case kRowFat:
				{
					rowValue = self.recipe.fat;
					break;
				}
			case kRowSaturatedFat:
				{
					rowValue = self.recipe.satfat;
					break;
				}
			case kRowSodium:
				{
					rowValue = self.recipe.sodium;
					break;
				}
			case kRowCholesterol:
				{
					rowValue = self.recipe.cholesterol;
					break;
				}
			case kRowCaloriesFromFat:
				{
					rowValue = self.recipe.caloriesFromFat;
					break;
				}
			default:
				break;
		}

		if (rowValue) {
			UITextField *aTextField = (UITextField *)[cell viewWithTag:kTagTextField];
			aTextField.text = [rowValue genericValueDisplay];
		}
	}
	else if ([indexPath section] == kSectionIngredients) {
		if ([indexPath row] < [self.ingredients count]) {
			MealIngredient *ingredient = [self.ingredients objectAtIndex:[indexPath row]];
			cell.textLabel.text = [ingredient description];
			cell.imageView.contentMode = UIViewContentModeCenter;
			if ([ingredient.addToShoppingList integerValue] == 0) {
				cell.imageView.image = [UIImage imageNamed:@"white"];
			}
			else if ([ingredient.addToShoppingList integerValue] == 1) {
				cell.imageView.image = [UIImage imageNamed:@"shop-always-small.png"];
			}
			else if ([ingredient.addToShoppingList integerValue] == 2) {
				cell.imageView.image = [UIImage imageNamed:@"shop-ask-small.png"];
			}
		}
	}
	else if ([indexPath section] == kSectionInstructions) {
		UITextView *textView = (UITextView *)[cell viewWithTag:kTagTextField];
		textView.text = self.recipe.instructions;
	}
}

- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Allow deleting ingredients
	if ([indexPath section] == kSectionIngredients) {
		return YES;
	}
	else {
		return NO;
	}
}

- (void)tableView:(UITableView *)inTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		// Delete the managed object for the given index path
		MealIngredient *ingredient = [self.ingredients objectAtIndex:[indexPath row]];
		[ingredient.managedObjectContext deleteObject:ingredient];
		NSError *error;
		if (![ingredient.managedObjectContext save:&error]) {
			DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
		}
		self.changedMeal = YES;
		self.ingredients = [self.recipe.ingredients
			sortedArrayUsingDescriptors:@[[NSSortDescriptor
			sortDescriptorWithKey:@"sortOrder" ascending:YES]]];

		// Delete the row from the data source
		[self.tableView deleteRowsAtIndexPaths:@[indexPath]
			withRowAnimation:UITableViewRowAnimationFade];
	}
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section
{
	if (section == kSectionOverall) {
		return 40.0;
	}
	else {
		return 0.0;
	}
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 0.0f;

	switch ([indexPath section]) {
		case kSectionInstructions:
			{
				CGSize size = { 0 };
				if ([self.recipe.instructions length]) {
					size = [self.recipe.instructions sizeWithFont:[UIFont systemFontOfSize:14]
						constrainedToSize:CGSizeMake(kCellWidth - (2 * kCellPadding) - 16, 10000)];
				}
				rowHeight = MAX(kStockInstructionsCellHeight, size.height + kHackExtraSize) + (2 * kCellPadding);
				break;
			}
		case kSectionOverall:
		case kSectionIngredients:
		default:
			rowHeight = 44.0f;
			break;
	}
	return rowHeight;
}

- (NSIndexPath *)tableView:(UITableView *)tableView
willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	switch ([indexPath section]) {
		// allow selection conditionally
		case kSectionOverall:
			{
				if ([indexPath row] == kRowRating) {
					return nil;
				}
			}
			break;
			// allow selection always
		case kSectionInstructions:
		case kSectionIngredients:
			break;
			// allow selection never
		default:
			return nil;
			break;
	}
	return indexPath;
}
- (void)tableView:(UITableView *)theTableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.editingIndexPath = indexPath;

	if (self.editingTextField) {
		self.editingTextField.enabled = NO;
		self.editingTextField = nil;
	}

	switch ([indexPath section]) {
		case kSectionOverall:
			{
				UITableViewCell *cell = [self.rowCells objectAtIndex:indexPath.row];
				UITextField *aTextField = (UITextField *)[cell viewWithTag:kTagTextField];
				self.editingTextField = aTextField;
				self.editingTextField.enabled = YES;
				self.editingTextField.delegate = self;
				[self.editingTextField becomeFirstResponder];
				[self.tableView deselectRowAtIndexPath:indexPath animated:YES];

				break;
			}
		case kSectionIngredients:
			{
				MealIngredient *ingredient = nil;

				if ([indexPath row] < [self.ingredients count]) {
					// Edit existing ingredient
					ingredient = [self.ingredients objectAtIndex:[indexPath row]];
				}
				else {
					// Add and edit a new ingredient
					ingredient = [MealIngredient
						insertInManagedObjectContext:self.recipe.managedObjectContext];
					ingredient.recipe = self.recipe;
					ingredient.sortOrder = [NSNumber numberWithInteger:
						([[self.recipe.ingredients valueForKeyPath:@"@max.sortOrder"] integerValue] + 1)];

					self.ingredients = [self.recipe.ingredients
						sortedArrayUsingDescriptors:@[[NSSortDescriptor
						sortDescriptorWithKey:@"sortOrder" ascending:YES]]];
					[self.tableView reloadData];

					[self.tableView selectRowAtIndexPath:indexPath animated:NO
						scrollPosition:UITableViewScrollPositionNone];
				}

				MealIngredientEditView *ingredientEditView = [[MealIngredientEditView alloc] initWithNibName:nil bundle:nil];
				ingredientEditView.ingredient = ingredient;
				ingredientEditView.delegate = self;
				[self.navigationController pushViewController:ingredientEditView animated:YES];
				break;
			}
		case kSectionInstructions:
			break;
		default:
			break;
	}
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self.editingTextField resignFirstResponder];

	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[self saveTextFieldValue:textField];

	self.editingTextField = nil;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
	if (![self.recipe.instructions isEqualToString:textView.text]) {
		self.recipe.instructions = textView.text;
		self.changedMeal = YES;
		// Clear the relationship to the original 'recipe' since they've changed it
		self.recipe.externalId = nil;
	}
}

#pragma mark - StarRatingControlDelegate
- (void)starRatingControl:(StarRatingControl *)control didUpdateRating:(NSUInteger)rating fromRating:(NSUInteger)oldRating
{
	self.recipe.rating = [NSNumber numberWithInteger:rating];
	self.changedMeal = YES;
}

#pragma mark - Notifications for keyboard hide/show
- (void)keyboardDidShowNotification:(NSNotification *)aNotification
{
	NSDictionary *info = [aNotification userInfo];
	CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);

	self.tableView.contentInset = contentInsets;
	self.tableView.scrollIndicatorInsets = contentInsets;

	[self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)keyboardWillHideNotification:(NSNotification *)aNotification
{
	self.tableView.contentInset = UIEdgeInsetsZero;
	self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

#pragma mark - MealIngredientEditViewDelegate
- (void)mealIngredientEditView:(MealIngredientEditView *)mealIngredientEditView
didUpdateIngredient:(MealIngredient *)ingredient
{
	self.changedMeal = YES;

	NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	if (cell) {
		[self configureCell:cell atIndexPath:indexPath];
	}
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
