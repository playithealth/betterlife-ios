//
//  Store.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 4/27/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryMO.h"
#import "UserStoreMO.h"


@interface Store : NSObject <NSCopying>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSNumber *storeId;
@property (strong, nonatomic) UserStoreMO *userStore;
@property (strong, nonatomic) NSMutableArray *storeCategories;
@property (strong, nonatomic) NSMutableDictionary *storeCategoriesDict;
@property (assign, nonatomic) BOOL buyerCompassClient;

+ (Store *)currentStore;
+ (void)clearCurrentStore;
+ (void)setCurrentStoreWithDictionary:(NSDictionary *)inStore inMOC:(NSManagedObjectContext *)moc;
+ (void)setCurrentStoreWithUserStore:(UserStoreMO *)inStore inMOC:(NSManagedObjectContext *)moc;

- (BOOL)isUserStore;
- (BOOL)isStoreSet;
- (BOOL)hasOrderedCategories;
- (StoreCategory *)storeCategoryForCategory:(CategoryMO *)category;
- (void)storeCheckIn:(NSManagedObjectContext *)moc;
- (NSString *)displayString;

@end
