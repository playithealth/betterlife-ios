//
//  SubtitleRatingCell.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 9/2/2011
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StarRatingControl;

@interface SubtitleRatingCell : UITableViewCell

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UILabel *detailLabel;
@property (strong, nonatomic) StarRatingControl *ratingControl;
@property (strong, nonatomic) UIButton *accessoryButton;

- (void)setAccessoryType:(UITableViewCellAccessoryType)accessoryType;
@end
