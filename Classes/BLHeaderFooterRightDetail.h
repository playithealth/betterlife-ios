//
//  BLHeaderFooterRightDetail.h
//  BettrLife
//
//  Created by Greg Goodrich on 1/07/15.
//  Copyright (c) 2015 BettrLife Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLHeaderFooterRightDetail : UITableViewHeaderFooterView
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UILabel *valueLabel;
@end
