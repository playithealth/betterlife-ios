//
//  MealPlannerDetailViewController.h
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 11/02/2012
//  Copyright (c) 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MealPlannerDay;
@protocol BCDetailViewDelegate;

@interface MealPlannerDetailViewController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext; ///< This is just retaining the MOC, in case it is a scratch context
@property (strong, nonatomic) MealPlannerDay *mpd;
@property (assign, nonatomic) BOOL showAddButton;
@property (weak, nonatomic) id<BCDetailViewDelegate> delegate;

@end
