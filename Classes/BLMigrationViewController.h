//
//  BLMigrationViewController.h
//  BuyerCompass
//
//  Created by Greg Goodrich on 3/25/14.
//  Copyright (c) 2014 BuyerCompass, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLMigrationViewController : UIViewController
- (void)showUpdateNotificationsFrom:(id)notifyingObject;
@end
