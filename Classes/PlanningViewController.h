//
//  PlanningViewController.h
//  BettrLife Corporation
//
//  Created by Greg Goodrich on 8/15/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCViewController.h"
#import "MealPlannerAskViewController.h"

@interface PlanningViewController : BCViewController <MealPlannerAskViewDelegate>

@end
