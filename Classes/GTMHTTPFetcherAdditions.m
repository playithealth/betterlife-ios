//
//  GTMHTTPFetcherAdditions.m
//  BettrLife Corporation
//
//  Created by Sef Tarbell on 6/20/11.
//  Copyright 2011 BettrLife Corporation. All rights reserved.
//

#import "GTMHTTPFetcherAdditions.h"

#import "BCUrlFactory.h"
#import "BPXLUUIDHandler.h"
#import "OAuthConsumer.h"
#import "OAuthUtilities.h"
#import "User.h"

static NSString *const kCustomHeaderPlatform = @"platform";
static NSString *const kCustomHeaderVersion = @"version";
static NSString *const kCustomHeaderUpdatingDevice = @"x-updating-device";
static NSString *const kCustomHeaderDeviceTimeZone = @"device-timezone";
static NSString *const kPlatform = @"iphone";
static NSString *const kCustomHeaderAPIVersion = @"bettrlife-api-version";

@implementation GTMHTTPFetcher (GTMHTTPFetcherAdditions)

+ (GTMHTTPFetcher *)signedFetcherWithURL:(NSURL *)url
{
	return [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:@"GET" body:nil];
}

+ (GTMHTTPFetcher *)signedFetcherWithURL:(NSURL *)url httpMethod:(NSString *)method postData:(NSData *)postData
{
	User *thisUser = [User currentUser];
	OAuthConsumer *thisConsumer = [OAuthConsumer currentOAuthConsumer];
	if (![thisUser loginIsValid] || ![thisConsumer consumerIsValid]) {
		return nil;
	}

	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
	[request setHTTPMethod:method];
	if (postData) {
		NSString *contentLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
		[request setValue:contentLength forHTTPHeaderField:@"Content-Length"];
		[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
		[request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
		[request setHTTPBody:postData];
	}
	[request setValue:kPlatform forHTTPHeaderField:kCustomHeaderPlatform];
	[request setValue:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] forHTTPHeaderField:kCustomHeaderVersion];
	[request setValue:[BPXLUUIDHandler UUID] forHTTPHeaderField:kCustomHeaderUpdatingDevice];
	[request setValue:[[NSTimeZone localTimeZone] name] forHTTPHeaderField:kCustomHeaderDeviceTimeZone];
	[request setValue:[BCUrlFactory apiVersionString] forHTTPHeaderField:kCustomHeaderAPIVersion];

	[OAuthUtilities signRequest:request withConsumer:thisConsumer andUser:thisUser];
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher fetcherWithRequest:request];

	return fetcher;
}

+ (GTMHTTPFetcher *)signedFetcherWithURL:(NSURL *)url httpMethod:(NSString *)method body:(NSString *)body
{
	NSData *postData = nil;
	if (body) {
		postData = [body dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
	}

	return [GTMHTTPFetcher signedFetcherWithURL:url httpMethod:method postData:postData];
}

+ (GTMHTTPFetcher *)fetcherWithURL:(NSURL *)url httpMethod:(NSString *)method postData:(NSData *)postData
{
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];

	[request setHTTPMethod:method];
	if (postData) {
		NSString *contentLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
		[request setValue:contentLength forHTTPHeaderField:@"Content-Length"];
		[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
		[request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
		[request setHTTPBody:postData];
	}
	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher fetcherWithRequest:request];

	return fetcher;
}

+ (GTMHTTPFetcher *)fetcherWithURL:(NSURL *)url httpMethod:(NSString *)method body:(NSString *)body
{
	NSData *postData = nil;
	if (body) {
		postData = [body dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
	}

	return [GTMHTTPFetcher fetcherWithURL:url httpMethod:method postData:postData];
}

+ (GTMHTTPFetcher *)oauthConsumerFetcher
{
	NSDictionary *oauthRequestData = [NSDictionary dictionaryWithObjectsAndKeys:
		[BPXLUUIDHandler UUID], kMobileId,
		nil];

	GTMHTTPFetcher *oauthConsumerFetcher = [GTMHTTPFetcher fetcherWithURL:[BCUrlFactory oauthConsumerURL] httpMethod:kPost
		postData:[NSJSONSerialization dataWithJSONObject:oauthRequestData options:kNilOptions error:nil]];

	return oauthConsumerFetcher;
}

+ (GTMHTTPFetcher *)signedFetcherWithLoginCredentials:(NSDictionary *)credentials
{
	NSAssert(credentials != nil, @"Cannot create login fetcher without login credentials");
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:credentials options:kNilOptions error:nil];

	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[BCUrlFactory loginURL]];
	[request setHTTPMethod:kPost];

	[request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
	[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPBody:jsonData];

	[request setValue:kPlatform forHTTPHeaderField:kCustomHeaderPlatform];
	[request setValue:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] forHTTPHeaderField:kCustomHeaderVersion];
	[request setValue:[BPXLUUIDHandler UUID] forHTTPHeaderField:kCustomHeaderUpdatingDevice];
	[request setValue:[[NSTimeZone localTimeZone] name] forHTTPHeaderField:kCustomHeaderDeviceTimeZone];
	[request setValue:[BCUrlFactory apiVersionString] forHTTPHeaderField:kCustomHeaderAPIVersion];
	[OAuthUtilities signRequest:request withConsumer:[OAuthConsumer currentOAuthConsumer] andUser:nil];

	GTMHTTPFetcher *fetcher = [GTMHTTPFetcher fetcherWithRequest:request];

	return fetcher;
}

@end
